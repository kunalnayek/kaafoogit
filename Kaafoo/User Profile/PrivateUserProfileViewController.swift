//
//  PrivateUserProfileViewController.swift
//  Kaafoo
//
//  Created by admin on 05/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class PrivateUserProfileViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBAction func hideMessageBox(_ sender: UIButton) {
        
        self.MessageView.isHidden = true
    }
    
    @IBOutlet weak var MessageBox: MessageBox!
    
    @IBOutlet weak var hideMessageBox: UIButton!
    
    @IBOutlet weak var hideMessageBtnOutlet: UIButton!
    
    @IBOutlet weak var MessageView: UIView!
    
    var userID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var pageHeader: UIView!
    
    @IBOutlet weak var headerProfileLBL: UILabel!
    
    @IBAction func closePrivateUserScreen(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var viewOfLocationImage: UIView!
    
    @IBOutlet weak var locationImageview: UIImageView!
    
    @IBOutlet weak var locationLBL: UILabel!
    
    //@IBOutlet weak var privateUserProfileTableview: UITableView!
    
    @IBOutlet weak var privateUserProfileTableView: UITableView!
    
    
    var sellerID : String! = "default"
    
    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var privateUserDataDictionary : NSMutableDictionary!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var clickedOnFeedback : Bool = false
    
    var headerLabelArray = [UILabel]()
    
    var feedbackLabelArray = [UILabel]()
    
    var listingType : String! = "P"
    
    var feedbackType : String! = "ALL"
    
    var feedbackTypePressed : Int!
    
    var headerSelected : Int! = 0
    
    var feedbackSelected : Int! = 0
    
    var aboutClicked : Bool! = true
    
    var profileStatusStringArray = [String]()
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        getUserProfileData()
        
        dispatchGroup.notify(queue: .main) {
            
            self.userName.text = "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["name"]!)"
            
            self.locationLBL.text = "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["location"]!)"
            
            
            
        }
        
        // Adding a sticky view effect in the tableview
        self.privateUserProfileTableView.estimatedSectionHeaderHeight = (80/568)*self.FullHeight //changed
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.privateUserProfileTableView.tableHeaderView = self.topView
        
        
        //give location background a round corner
        self.viewOfLocationImage.layer.cornerRadius = self.viewOfLocationImage.frame.size.height / 2
        
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        privateUserProfileTableView.estimatedRowHeight = 300
        
        privateUserProfileTableView.rowHeight = UITableView.automaticDimension
        
    }
    
     //MARK:- // MessageView data handling
    func setMessageBox()
    {
        self.MessageBox.MessageTextView.text = ""
        
        self.MessageBox.SendBtnOutlet.addTarget(self, action: #selector(self.sendUserMessage(sender:)), for: .touchUpInside)
        
        self.MessageBox.CancelBtnOutlet.addTarget(self, action: #selector(self.hideMessageView(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //Message Box Cancel Button Selector Method
    
    @objc func hideMessageView(sender : UIButton)
    {
        self.MessageView.isHidden = true
    }
    
    //MARK:- //Send Message API
    @objc func sendUserMessage(sender : UIButton)
    {
        if self.MessageBox.MessageTextView.text == ""
        {
            self.ShowAlertMessage(title: "Warning", message: "Message can not be empty!")
        }
        else
        {
            let parameters = "user_id=\(userID ?? "")&seller_id=\(sellerID ?? "")&message=\(self.MessageBox.MessageTextView.text ?? "")"
            
            self.CallAPI(urlString: "app_friend_send_message", param: parameters, completion: {
                
                self.globalDispatchgroup.leave()
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    self.MessageBox.MessageTextView.text = ""
                    
                    self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                }
                
            })
        }
    }
    
    
    // MARK:- // Delegate Functions
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if self.scrollBegin > self.scrollEnd
        {
            
        }
        else
        {
            
//            print("next start : ",self.nextStart )
            
            if (self.nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getUserProfileData()
            }
            
        }
        
    }
    
    // MARK:- // Tableview Delegates
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if tableView == privateUserProfileTableView
        {
            
            let headerOfTable : UIView!
            
            if clickedOnFeedback == true  // User Tapped On Reviews
            {
                
                headerLabelArray.removeAll()
                
                feedbackLabelArray.removeAll()
                
                headerOfTable = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (80/568)*self.FullHeight))
                
                headerOfTable.backgroundColor = UIColor(red:242/255, green:242/255, blue:242/255, alpha: 1)
                
                createHeaderOfTableview(headerView: headerOfTable)
                
                createViewForFeedbackClick(parentView: headerOfTable, xOrigin: 0, yOrigin: (40/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: ""), buttonTag: 0)
                
                createViewForFeedbackClick(parentView: headerOfTable, xOrigin: (self.FullWidth/3), yOrigin: (40/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Selling", comment: ""), buttonTag: 1)
                
                createViewForFeedbackClick(parentView: headerOfTable, xOrigin: 2*(self.FullWidth/3), yOrigin: (40/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Buying", comment: ""), buttonTag: 2)
                //
                //                self.headerLabelArray[headerSelected].textColor = UIColor.green
                //                self.feedbackLabelArray[feedbackSelected].textColor = UIColor.red
                //
                
            }
            else
            {
                
                headerLabelArray.removeAll()
                
                feedbackLabelArray.removeAll()
                
                headerOfTable = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (40/568)*self.FullHeight))
                
                headerOfTable.backgroundColor = UIColor(red:242/255, green:242/255, blue:242/255, alpha: 1)
                
                createHeaderOfTableview(headerView: headerOfTable)
                //
                //                self.headerLabelArray[0].textColor = UIColor.green
                //
                
            }
            
            self.headerLabelArray[self.headerSelected].textColor = UIColor.green
            
            return headerOfTable
            
        }
        else
        {
            return nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if clickedOnFeedback == true
        {
            return (80/568)*self.FullHeight
        }
        else
        {
            return (40/568)*self.FullHeight
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if listingType.elementsEqual("P")
        {
            if aboutClicked == true // About
            {
                return 1
            }
            else // Listing
            {
                return recordsArray.count
            }
        }
        else  // Reviews
        {
            return recordsArray.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if listingType.elementsEqual("P")
        {
            
            if self.aboutClicked == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "listings") as! PUListingTVCTableViewCell
                
                cell.listingImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"))
                
                cell.listingTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
                
                cell.listingtype.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
                
                cell.closesInLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)"
                
                cell.addresslabel.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
                
                //cell.listedonlabel.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)"
                
               
                
                
                cell.listingPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                
                
                
                cell.addToWatchlistButton.tag = indexPath.row
                
                cell.addToWatchlistButton.setTitle("", for: .normal)
                
                cell.addToWatchlistButton.backgroundColor = .clear
                //cell.addToWatchlistButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: ""), for: .normal)
                
                if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"] ?? "")".elementsEqual("1")
                {
                    cell.addToWatchlistButton.setImage(UIImage(named: "heart_red"), for: .normal)
                    
                    cell.addToWatchlistButton.addTarget(self, action: #selector(self.WatchlistedBtnTarget(sender:)), for: .touchUpInside)
                }
                else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"] ?? "")".elementsEqual("0")
                {
                    cell.addToWatchlistButton.setImage(UIImage(named: "heart"), for: .normal)
                    
                    cell.addToWatchlistButton.addTarget(self, action: #selector(self.WatchlistBtnTarget(sender:)), for: .touchUpInside)
                }
                else
                {
                    cell.addToWatchlistButton.setImage(UIImage(named: "heart"), for: .normal)
                    
                }
                
                cell.addToWatchlistButton.layer.cornerRadius = cell.addToWatchlistButton.frame.size.height / 2
                
                //set font
                cell.listingTitle.font = UIFont(name: cell.listingTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.closesInLBL.font = UIFont(name: cell.closesInLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                
                cell.listingPrice.font = UIFont(name: cell.listingPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.addToWatchlistButton.titleLabel?.font = UIFont(name: (cell.addToWatchlistButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
                
                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                //
                cell.productView.layer.cornerRadius = 8
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "about") as! PUAboutTVCTableViewCell
                
                cell.sendMessageButton.addTarget(self, action: #selector(self.sendMessage(sender:)), for: .touchUpInside)
                
                cell.qrImage.sd_setImage(with: URL(string: "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["qr_code"]!)"))
                
                cell.profileName.text = "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["name"]!)"
                
                cell.accountNo.text = "Account No. " + "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["account_no"]!)"
                
                cell.memberSince.text = "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["member_since"]!)"
                
                cell.views.text = "Views : " + "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["total_view"]!)"
                
                if "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["bonus_status"]!)".elementsEqual("1")
                {
                    cell.bonusView.isHidden = false
                    
                    cell.bonusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bonus", comment: "")
                    
                    self.profileStatusStringArray.append("B")
                }
                else
                {
                    cell.bonusView.isHidden = true
                }
                
                if "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["verified_status"]!)".elementsEqual("V")
                {
                    cell.verifiedView.isHidden = false
                    
                    cell.verifiedLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verified", comment: "")
                    
                    self.profileStatusStringArray.append("V")
                }
                else
                {
                    cell.verifiedView.isHidden = true
                }
                
                if "\((self.privateUserDataDictionary["seller_info"] as! NSDictionary)["marrof_status"]!)".elementsEqual("1")
                {
                    cell.maroofView.isHidden = false
                    
                    cell.maroofLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "maroof", comment: "")
                    
                    self.profileStatusStringArray.append("M")
                }
                else
                {
                    cell.maroofView.isHidden = true
                }
                
                if profileStatusStringArray.count > 0
                {
                    cell.profileStatusImageView.isHidden = false
                }
                else
                {
                    cell.profileStatusImageView.isHidden = true
                }
                
                //set font
                cell.profileName.font = UIFont(name: cell.profileName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
                
                cell.accountNo.font = UIFont(name: cell.accountNo.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                
                cell.memberSince.font = UIFont(name: cell.memberSince.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                
                cell.views.font = UIFont(name: cell.views.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                
                cell.sendMessageButton.titleLabel?.font = UIFont(name: (cell.sendMessageButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
                
                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
            }
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "feedback") as! PUFeedbackTVCTableViewCell
            
            cell.reviewsNameLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["username"]!)"
            
            cell.reviewsDateLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["datetime"]!)"
            
            cell.reviewsCommentLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["comment"]!)"
            
            cell.totalRatingLBL.text = " (" + "\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"]!)" + "          ) "
            
            cell.ratingNoLBL.text = "  \((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)"
            
            
            
            //set font
            cell.reviewsNameLBL.font = UIFont(name: cell.reviewsNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            cell.reviewsDateLBL.font = UIFont(name: cell.reviewsDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            cell.reviewsCommentLBL.font = UIFont(name: cell.reviewsCommentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            cell.totalRatingLBL.font = UIFont(name: cell.totalRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            cell.ratingNoLBL.font = UIFont(name: cell.ratingNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            
            //
            cell.cellView.layer.cornerRadius = 8
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            
            return cell
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.listingType.elementsEqual("P")
        {
            if self.aboutClicked == false
            {
                 if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("9")
           {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
            {
                let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController

                navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else
            {
                
            }
           }
                //MarketPlace Product Details View Controller
                
           else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("10")
           {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
            {
                let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController

                navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else
            {
                
            }
           }
                //Daily Rental Checking
                
           else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("15")
           {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
            {
                let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyrental") as! NewDailyRentalProductDetailsViewController

                nav.DailyRentalProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                self.navigationController?.pushViewController(nav, animated: true)
            }
            else
            {
                
            }
            
           }
                //Real Estate Checking
                
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("999")
            {
                if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                {
                    let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateDetailsVC") as! NewRealEstateDetailsViewController
                    
                    nav.productID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                    
                    self.navigationController?.pushViewController(nav, animated: true)
                }
                else
                {
                    
                }
            }
            
            //Food Court Checking
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("13")
            {
                if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                {
                    let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController
                    
                    navigate.sellerID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else
                {
                    
                }
            }
                //Services Checking
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("11")
            {
                if "\((recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                {
                    let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
                    
                    obj.productID = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                    
                    obj.SID = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
                    
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                else
                {
                   // Can Not Navigate
                }
                
            }
                //Happening Checking
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("14")
            {
                if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                {
                    let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController
                    
                    navigate.eventID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else
                {
                    
                }
                
            }
                //Jobs Checking
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("12")
            {
                if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                {
                    let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobdetails") as! NewJobDetailsViewController

                    navigate.JobId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else
                {
                    
                }
            }
            }
        }
        
    }
    
    
    //MARK:- //Add To Watchlist API Fire
    
    @objc func WatchlistBtnTarget(sender : UIButton)
    {
        
        if (self.userID?.elementsEqual(""))!
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
                
                self.startValue = 0
                
                self.recordsArray.removeAllObjects()
                
                self.getUserProfileData()
                
            })
        }
        
    }
    
    //MARK:- //Remove From Watchlist API Fire
    
    @objc func WatchlistedBtnTarget(sender : UIButton)
    {
        if (self.userID?.elementsEqual(""))!
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
                
                self.startValue = 0
                
                self.recordsArray.removeAllObjects()
                
                self.getUserProfileData()
                
            })
        }
    }
    
    
    // MARK: - // JSON POST Method to get User Profile Data
    
    func getUserProfileData()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
            
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_private_user_profile")!   //change the url
        
        print("Business User Profile Data URL ------",url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if sellerID.elementsEqual("default")
        {
            sellerID = userID
        }
        
        if clickedOnFeedback == true
        {
            
            parameters = "seller_id=\(sellerID!)&login_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&start=&type=\(feedbackType!)&category_id=&child_id=&listing_type=\(listingType!)"
            
        }
        else
        {
            
            parameters = "seller_id=\(sellerID!)&login_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&start=&type=&category_id=&child_id=&listing_type=\(listingType!)"
            
        }
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                SVProgressHUD.dismiss()
                return
            }
            
            guard let data = data else {
                SVProgressHUD.dismiss()
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Private User Profile Response: " , json)
                    
                    if self.listingType.elementsEqual("P")
                    {
                        
                        self.privateUserDataDictionary = json["info_array"] as? NSMutableDictionary
                        
                        print(self.privateUserDataDictionary)
                        
                        for i in 0..<((self.privateUserDataDictionary)["product_info"] as! NSArray).count
                            
                        {
                            
                            let tempDict = ((self.privateUserDataDictionary)["product_info"] as! NSArray)[i] as! NSDictionary
                            
                            self.recordsArray.add(tempDict as! NSMutableDictionary)
                            
                        }
                        self.nextStart = "\(json["next_start"]!)"
                        
                        self.startValue = self.startValue + ((self.privateUserDataDictionary)["product_info"] as! NSArray).count
                        
                        print("Next Start Value : " , self.startValue)
                        
                    }
                    else if self.listingType.elementsEqual("R")
                    {
                        
                        if ((json["info_array"] as! NSDictionary)["review_section"] as! NSArray).count == 0
                        {
                            
                        }
                        else
                        {
                            let ReviewArray = ((json["info_array"] as! NSDictionary)["review_section"] as! NSArray)
                            
                            for i in 0..<ReviewArray.count
                                
                                                  
                                                       
                                                   {
                                                       
                                                       let tempDict = ReviewArray[i] as! NSMutableDictionary
                                                       
                                                    self.recordsArray.add(tempDict)
                                                       
                                                   }
                                                   
                                                   self.nextStart = "\(json["next_start"]!)"
                                                   
                                                   self.startValue = self.startValue + ((json["info_array"] as! NSDictionary)["review_section"] as! NSArray).count
                                                   
                                                   print("Next Start Value : " , self.startValue)
                        }
                        
                       
                        
                    }
                    
                    
                    DispatchQueue.main.async {
                        
                        self.privateUserProfileTableView.delegate = self
                        self.privateUserProfileTableView.dataSource = self
                        self.privateUserProfileTableView.reloadData()
                        
                        SVProgressHUD.dismiss()
                        
                        self.dispatchGroup.leave()
                    }
                    
                    
                }
                
            } catch let error {
                SVProgressHUD.dismiss()
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Function Declaring the creation of Tableview Section Header while clickOnReviews == false
    
    func createHeaderOfTableview(headerView: UIView)
    {
        
        createViewForTableviewHeaderSection(parentView: headerView, xOrigin: 0, yOrigin: 0, width: (90/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "about", comment: ""), buttonTag: 0)
        
        createViewForTableviewHeaderSection(parentView: headerView, xOrigin: (90/320)*self.FullWidth, yOrigin: 0, width: (90/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "listings", comment: ""), buttonTag: 1)
        
        createViewForTableviewHeaderSection(parentView: headerView, xOrigin: (180/320)*self.FullWidth, yOrigin: 0, width: (140/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountFeedback", comment: ""), buttonTag: 2)
        
        
    }
    
    // MARK:- // Function Declaring when user taps on "Listing" , "Reviews" , "Company Profile" , "Contact"
    
    func createViewForTableviewHeaderSection(parentView: AnyObject, xOrigin: CGFloat, yOrigin: CGFloat, width: CGFloat, height: CGFloat, title: String, buttonTag: Int)
    {
        
        let tempView = UIView(frame: CGRect(x: xOrigin, y: yOrigin, width: width, height: height))
        
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempLabel.textAlignment = .center
        
        tempLabel.text = title
        
        tempView.addSubview(tempLabel)
        
        headerLabelArray.append(tempLabel)
        
        let tempButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempButton.tag = buttonTag
        
        tempButton.addTarget(self, action: #selector(PrivateUserProfileViewController.headerSectionClicks), for: .touchUpInside)
        
        tempView.addSubview(tempButton)
        
        let separatorView = UIView(frame: CGRect(x: width - 1, y: (5/568)*self.FullHeight, width: 1, height: height - (10/568)*self.FullHeight))
        
        separatorView.backgroundColor = UIColor.black
        
        tempView.addSubview(separatorView)
        
        parentView.addSubview(tempView)
        
    }
    
    
    // MARK:- // Function Declaring when user Taps on "Reviews" of TableView Section Header
    
    func createViewForFeedbackClick(parentView: AnyObject, xOrigin: CGFloat, yOrigin: CGFloat, width: CGFloat, height: CGFloat, title: String, buttonTag: Int)
    {
        
        let tempView = UIView(frame: CGRect(x: xOrigin, y: yOrigin, width: width, height: height))
        
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempLabel.textAlignment = .center
        
        tempLabel.text = title
        
        feedbackLabelArray.append(tempLabel)
        
        tempView.addSubview(tempLabel)
        
        let tempButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempButton.tag = buttonTag
        
        tempButton.addTarget(self, action: #selector(PrivateUserProfileViewController.feedbackClick), for: .touchUpInside)
        
        tempView.addSubview(tempButton)
        
        let separatorView = UIView(frame: CGRect(x: width - 1, y: (5/568)*self.FullHeight, width: 1, height: height - (10/568)*self.FullHeight))
        
        separatorView.backgroundColor = UIColor.black
        
        tempView.addSubview(separatorView)
        
        parentView.addSubview(tempView)
        
    }
    
    
    
    // MARK: - // Tableview SEction Header Click
    
    @objc func headerSectionClicks(_ sender: UIButton) {
        
        if sender.tag == 0  // About
        {
            self.aboutClicked = true
            
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("About is Working")
            
            listingType = "P"
            
            headerSelected = 0
            
            clickedOnFeedback = false
            
            getUserProfileData()
            
        }
        else if sender.tag == 1  // Listing
        {
            self.aboutClicked = false
            
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Listing is Working")
            
            listingType = "P"
            
            headerSelected = 1
            
            clickedOnFeedback = false
            
            getUserProfileData()
            
        }
        else if sender.tag == 2  // reviews
        {
            
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Account Feedback is Working")
            
            listingType = "R"
            
            headerSelected = 2
            
            clickedOnFeedback = true
            
            getUserProfileData()
            
            dispatchGroup.notify(queue: .main) {
                
                if self.feedbackSelected != 0
                {
                    self.feedbackLabelArray[self.feedbackSelected].textColor = UIColor.red
                }
                else
                {
                    self.feedbackLabelArray[0].textColor = UIColor.red
                }
            }
            
            
            
        }
        
        if self.headerLabelArray.count != 0
        {
            dispatchGroup.notify(queue: .main, execute: {
                for i in 0..<self.headerLabelArray.count
                {
                    self.headerLabelArray[i].textColor = UIColor.black
                }
                self.headerLabelArray[self.headerSelected].textColor = UIColor.green
            })
        }
        
    }
    
    
    
    // MARK: - // Review Click
    
    @objc func feedbackClick(_ sender: UIButton) {
        
        
        
        if sender.tag == 0
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("All is Working")
            
            feedbackType = "ALL"
            
            feedbackSelected = 0
            
            getUserProfileData()
            
        }
        else if sender.tag == 1
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Selling is Working")
            
            feedbackType = "SELL"
            
            feedbackSelected = 1
            
            getUserProfileData()
            
        }
        else if sender.tag == 2
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Buying is Working")
            
            feedbackType = "BUY"
            
            feedbackSelected = 2
            
            getUserProfileData()
            
        }
        
        
        if self.headerLabelArray.count != 0
        {
            dispatchGroup.notify(queue: .main, execute: {
                
                for i in 0..<self.feedbackLabelArray.count
                {
                    self.feedbackLabelArray[i].textColor = UIColor.black
                }
                self.headerLabelArray[self.headerSelected].textColor = UIColor.green
                
                self.feedbackLabelArray[self.feedbackSelected].textColor = UIColor.red
            })
        }
    }
    
    
    
    // MARK:- // Give Shadow to a UIView
    
    func castShadow(view: UIView)
    {
        let shadowSize : CGFloat = 10.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,y: -shadowSize / 2,width: view.frame.size.width + shadowSize,height: view.frame.size.height + shadowSize))
        
        self.view.layer.masksToBounds = false
        
        self.view.layer.shadowColor = UIColor.black.cgColor
        
        self.view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        self.view.layer.shadowOpacity = 0.5
        
        self.view.layer.shadowPath = shadowPath.cgPath
    }
    
    //MARK:- //Send Message Button Target
    
    @objc func sendMessage(sender : UIButton)
    {
        self.setMessageBox()
        
        self.MessageView.isHidden = false
        
        self.view.bringSubviewToFront(self.MessageView)
    }
    
}

class PUAboutTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var profileStatusImageView: UIView!
    @IBOutlet weak var profileInformationView: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var accountNo: UILabel!
    @IBOutlet weak var memberSince: UILabel!
    @IBOutlet weak var views: UILabel!
    
    @IBOutlet weak var sendMessageButton: UIButton!
    
    @IBOutlet weak var bonusView: UIView!
    @IBOutlet weak var profileStatusStackView: UIStackView!
    @IBOutlet weak var bonusImageview: UIImageView!
    @IBOutlet weak var bonusLBL: UILabel!
    
    @IBOutlet weak var maroofView: UIView!
    @IBOutlet weak var maroofImageview: UIImageView!
    @IBOutlet weak var maroofLBL: UILabel!
    
    @IBOutlet weak var verifiedView: UIView!
    @IBOutlet weak var verifiedImageview: UIImageView!
    @IBOutlet weak var verifiedLBL: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class PUListingTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var listingImageview: UIImageView!
    @IBOutlet weak var listingTitle: UILabel!
    @IBOutlet weak var closesInLBL: UILabel!
    
    @IBOutlet weak var listingPrice: UILabel!
    @IBOutlet weak var addToWatchlistButton: UIButton!
    
    @IBOutlet weak var productView: UIView!
    
    @IBOutlet weak var listedonlabel: UILabel!
    
    @IBOutlet weak var addresslabel: UILabel!
    
    @IBOutlet weak var listingtype: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class PUFeedbackTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var reviewsNameLBL: UILabel!
    @IBOutlet weak var reviewsDateLBL: UILabel!
    @IBOutlet weak var reviewsCommentLBL: UILabel!
    
    @IBOutlet weak var totalRatingLBL: UILabel!
    @IBOutlet weak var ratingNoLBL: UILabel!
    @IBOutlet weak var ratingNoStarImageview: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



