//
//  BusinessUserProfileViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 29/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleMaps
import GooglePlaces
import Cosmos

class BusinessUserProfileViewController: GlobalViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    /*
     9-Marketplace
     11-Services
     12-Jobs
     13-Food Court
     14-Happening
     15-Daily Rental
     16-Holiday Accommodatin
     */
    
    // Global Font Applied
    
    
   // @IBOutlet weak var SearchBar: UISearchBar!
    
    var SearchBar = UISearchBar()
    
    var searchText : String! = ""
    
    var SortsearchKey : String! = ""
    
   
    
    var SelectedCategoryID : String! = ""
    
    var selleRID : String! = ""
    
    var SellerId : String! = ""
    
    var MainCategoryArray = ["9","15","16","9","13","999","11","14"]
    
    var SelectedPickerIDInt : Int! = 0
    
    var button = UIButton()
    
    var dataLBL = UILabel()
    
    var userID = UserDefaults.standard.string(forKey: "userID")
    
    var SelectedPickerInt : Int! = 0
    
    var serviceselectedfrompicker : Int! = 0
    
    var SellerInfoDict : NSDictionary!
    
//     var mainCategoryDict = [["key" : "" , "value" : "All"],["key" : "F" , "value" : "Food Court"],["key" : "S" , "value" : "Service"],["key" : "R" , "value" : "Real Estate"],["key" : "M" , "value" : "Marketplace"],["key" : "H" , "value" : "Holiday Accommodation"]]
    
    
    @IBOutlet weak var Messagebox: MessageBox!
    
    @IBOutlet weak var MessageView: UIView!
    
    @IBOutlet weak var hideMessageViewBtnOutlet: UIButton!
    
    @IBAction func hideMessageBtn(_ sender: UIButton) {
        
        self.MessageView.isHidden = true
        
        self.view.sendSubviewToBack(self.MessageView)
    }
    
    @IBOutlet weak var sampleLBL: UILabel!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var pageHeader: UIView!
    
    @IBOutlet weak var profileTitleLBL: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var imageScroll: UIScrollView!
    
    @IBOutlet weak var imagePageControl: UIPageControl!
    
    @IBOutlet weak var businessUserTableview: UITableView!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var categoryView: UIView!
    
    var availabilityTimeView : UIView!
    
    var contactCellHeight : CGFloat!
    
    var profileStatusStringArray = [String]()
    
    
    @IBAction func closeCategoryView(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.categoryView)
            
            self.categoryView.isHidden = true
        }
        
    }
    
    
    
    var headerScrollview : UIScrollView!
    
    var headerScrollPoint : CGPoint!
    
    var sellerID : String! = "default"
    
    var categoryDataLBLArray = [UILabel]()
    
    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var businessUserDataDictionary : NSMutableDictionary!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var clickedOnReviews : Bool = false
    
    var headerLabelArray = [UILabel]()
    
    var listingType : String! = "P"
    
    var reviewType : String! = "ALL"
    
    var reviewTypePressed : Int!
    
    var categoryArray = ["ALL"]
    
    var dropDownView : UIView!
    
    var marker = GMSMarker()
    
    var reviewSelected : Int! = 0
    
    var aboutClicked : Bool! = true
    
    var mainCategoryArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Rental", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Holiday Accommodation", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Deals", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Food Court", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Real Estate", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Services", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "happening", comment: "")]
    
    var ShowPickerView : UIView = {
        
        let showpickerview = UIView()
        
        showpickerview.translatesAutoresizingMaskIntoConstraints = false
        
        showpickerview.backgroundColor = UIColor(red: 65/255, green: 65/255, blue: 65/255, alpha: 1.0)
        
        return showpickerview
    }()
    
    var hidePickerBtn : UIButton = {
        
        let hidepickerbtn = UIButton()
        
        hidepickerbtn.translatesAutoresizingMaskIntoConstraints = false
        
        hidepickerbtn.backgroundColor = .clear
        
        return hidepickerbtn
    }()
    
    var DataPicker : UIPickerView = {
        
        let datapicker = UIPickerView()
        
        datapicker.translatesAutoresizingMaskIntoConstraints = false
        
        datapicker.backgroundColor = .white
        
        return datapicker
        
    }()
    
    var toolBar : UIToolbar = {
        
        let toolbar = UIToolbar()
        
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        
        return toolbar
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        set_font()
        
       
        
        getUserProfileData()
        
        SearchBar.delegate = self
        
       // getData()
        
        dispatchGroup.notify(queue: .main) {
            
            self.profileImage.backgroundColor = UIColor.white
            
            self.profileImage.sd_setImage(with: URL(string: "\((self.businessUserDataDictionary["seller_info"] as! NSDictionary)["logo"]!)"))
            
            // MARK:- // ImageScroll
            
            let tempArray = ((self.businessUserDataDictionary["seller_info"] as! NSDictionary)["slider_banner"] as! NSArray)
            
            print("temp Array is : " , tempArray)
            
            self.imagePageControl.numberOfPages = tempArray.count
            
            print("pagecontrol no of pages : ",self.imagePageControl.numberOfPages )
            
            for i in 0..<tempArray.count
            {
                self.tempFrame.origin.x = self.imageScroll.frame.size.width * CGFloat(i)
                
                self.tempFrame.size = self.imageScroll.frame.size
                
                let imageview = UIImageView(frame: self.tempFrame)
                
                imageview.sd_setImage(with: URL(string: "\((tempArray[i] as! NSDictionary)["slider_img"]!)"))
                
                imageview.contentMode = .scaleAspectFill
                
                imageview.clipsToBounds = true
                
                self.imageScroll.addSubview(imageview)
            }
            self.imageScroll.contentSize = CGSize(width: (self.imageScroll.frame.size.width * CGFloat(tempArray.count)), height: self.imageScroll.frame.size.height)
            
            
            self.imageScroll.delegate = self
            
            
            /////////////
            
            
            
            // Adding a sticky view effect in the tableview
            
            
            self.businessUserTableview.estimatedSectionHeaderHeight = (80/568)*self.FullHeight
//            self.mainScroll.contentInsetAdjustmentBehavior = .automatic
            self.automaticallyAdjustsScrollViewInsets = false
            
            self.businessUserTableview.tableHeaderView = self.topView
            
            self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
            
            self.SetConfiguration()
            
        }
        
    }
    
    func SetConfiguration()
    {
        self.view.addSubview(self.ShowPickerView)
        
        self.ShowPickerView.anchor(top: self.view.topAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.ShowPickerView.addSubview(self.hidePickerBtn)
        
        self.hidePickerBtn.anchor(top: self.ShowPickerView.topAnchor, leading: self.ShowPickerView.leadingAnchor, bottom: self.ShowPickerView.bottomAnchor, trailing: self.ShowPickerView.trailingAnchor)
        
        self.hidePickerBtn.addTarget(self, action: #selector(self.hidePickerTarget(sender:)), for: .touchUpInside)
        
        self.ShowPickerView.addSubview(self.DataPicker)
        
        self.DataPicker.anchor(top: nil, leading: self.ShowPickerView.leadingAnchor, bottom: self.ShowPickerView.bottomAnchor, trailing: self.ShowPickerView.trailingAnchor)
        
        self.DataPicker.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        self.DataPicker.delegate = self
        
        self.DataPicker.dataSource = self
        
        self.DataPicker.reloadAllComponents()
        
        self.CreateToolbar()
        
        self.ShowPickerView.isHidden = true
    }
    
    //MARK:- //Create Toolbar for Pickerview in Swift
    
       func CreateToolbar()
       {
           // ToolBar
           
           toolBar.barStyle = .default
           
           toolBar.isTranslucent = true
           
           toolBar.tintColor = .white
           
           toolBar.backgroundColor = .black
           
           toolBar.barTintColor = .black
           
           toolBar.sizeToFit()
           
           let doneButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneClick))
           
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           
           let cancelButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: ""), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelClick))
           
           toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
           
           toolBar.isUserInteractionEnabled = true
           
           self.ShowPickerView.addSubview(toolBar)
           
           toolBar.translatesAutoresizingMaskIntoConstraints = false
           
           toolBar.leadingAnchor.constraint(equalTo: self.ShowPickerView.leadingAnchor, constant: 0).isActive = true
           
           toolBar.trailingAnchor.constraint(equalTo: self.ShowPickerView.trailingAnchor, constant: 0).isActive = true
           
           toolBar.bottomAnchor.constraint(equalTo: self.DataPicker.topAnchor).isActive = true
           
           toolBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
       }
    
    override func viewWillAppear(_ animated: Bool) {
        
        businessUserTableview.estimatedRowHeight = 300
        
        businessUserTableview.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.dispatchGroup.notify(queue: .main) {
            
            self.categoryDataLBLArray[0].text = self.categoryArray[0]
            
        }
        
    }
    
    @objc func hidePickerTarget(sender : UIButton)
    {
        self.ShowPickerView.isHidden = true
        
        self.view.bringSubviewToFront(self.ShowPickerView)
    }
    
    @objc func doneClick()
    {
        
        
         print("maincategoryarray--------",self.mainCategoryArray)
        
        
        
        self.ShowPickerView.isHidden = true
        
        if (self.SelectedPickerInt == 6)
        {
        
          let bookATime = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "servicelisting") as! ServiceListingViewController
           
           self.navigationController?.pushViewController(bookATime, animated: true)
            
            
        }
        
        
        //self.button.setTitle("\(self.mainCategoryArray[SelectedPickerInt])", for: .normal)
        
        //self.dataLBL.text = "\(self.mainCategoryArray[SelectedPickerInt])"
        
        
       // self.button.backgroundColor = .red
        
        self.SelectedCategoryID = "\(self.MainCategoryArray[SelectedPickerIDInt])"
        
//         self.SortsearchKey = "\((self.mainCategoryDict[SelectedPickerIDInt] as NSDictionary)["key"] ?? "")"
//
//        print("SortsearchKey----",self.SortsearchKey)
        
        self.startValue = 0
        
        self.categoryArray.removeAll()

        self.recordsArray.removeAllObjects()

        self.getUserProfileData()
    }
    
    @objc func cancelClick()
    {
        self.ShowPickerView.isHidden = true
    }
    
    @objc func watchlistBtnTarget(sender : UIButton)
    {
        print("Watchlist")
        
        if (self.userID?.elementsEqual(""))!
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
                
                print("watchlist")
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getUserProfileData()
                
            })
        }
    }
    
    @objc func watchlistedBtnTarget(sender : UIButton)
    {
        print("Watchlisted")
        
        if (self.userID?.elementsEqual(""))!
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
                
                print("watchlisted")
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getUserProfileData()

            })
        }
    }
   
    
    //MARK:- //Set MessageBox Details
    
    func setMessageBoxDetails()
    {
        self.Messagebox.MessageTextView.text = ""
        
        self.Messagebox.SendBtnOutlet.addTarget(self, action: #selector(sendMessage(sender:)), for: .touchUpInside)
        
        self.Messagebox.CancelBtnOutlet.addTarget(self, action: #selector(self.hideMessageBox(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //Hide MessageBox View
    
    @objc func hideMessageBox(sender : UIButton)
    {
        self.MessageView.isHidden = true
        
        self.view.sendSubviewToBack(self.MessageView)
    }
    
    @objc func sendMessage(sender : UIButton)
    {
        if self.Messagebox.MessageTextView.text.elementsEqual("")
        {
            self.ShowAlertMessage(title: "Alert", message: "Message can not be blank")
        }
        else
        {
            let parameters = "user_id=\(userID ?? "")&seller_id=\(sellerID ?? "")&message=\(self.Messagebox.MessageTextView.text ?? "")"
            
            self.CallAPI(urlString: "app_friend_send_message", param: parameters, completion: {
                
                self.globalDispatchgroup.leave()
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    self.Messagebox.MessageTextView.text = ""
                    
                    self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                }
            })
        }
    }
    
    
    
    @IBOutlet weak var pageCrossButtonOutlet: UIButton!
    
    @IBAction func tapOnPageCross(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == imageScroll
        {
            
        }
        else
        {
            scrollBegin = scrollView.contentOffset.y
        }
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == imageScroll
        {
            
        }
            
        else
        {
            scrollEnd = scrollView.contentOffset.y
            
            headerScrollPoint = CGPoint(x: scrollEnd, y: 0)
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == imageScroll
        {
            
            let pageNumber = imageScroll.contentOffset.x / imageScroll.frame.size.width
            
            imagePageControl.currentPage = Int(pageNumber)
        }
            
        else
        {
            if self.scrollBegin > self.scrollEnd
            {
                
            }
            else
            {
                
//                print("next start : ",self.nextStart )
                
                if (self.nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        
                        SVProgressHUD.dismiss()
                    }
                    
                    
                }
                else
                {
                    self.getUserProfileData()
                }
                
            }
            
            
            
            
        }
        
        
    }
    
    
    // MARK:- // Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if tableView == businessUserTableview
        {
            
            let headerOfTable : UIView!
            
            if clickedOnReviews == true  // User Tapped On Reviews
            {
                headerLabelArray.removeAll()
                
                headerOfTable = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (120/568)*self.FullHeight))
                
                headerOfTable.backgroundColor = UIColor(red:242/255, green:242/255, blue:242/255, alpha: 1)
                
                createHeaderOfTableview(headerView: headerOfTable)
                
                createViewForReviewClick(parentView: headerOfTable, xOrigin: 0, yOrigin: (80/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: ""), buttonTag: 0)
                
                createViewForReviewClick(parentView: headerOfTable, xOrigin: (self.FullWidth/3), yOrigin: (80/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "selling", comment: ""), buttonTag: 1)
                
                createViewForReviewClick(parentView: headerOfTable, xOrigin: 2*(self.FullWidth/3), yOrigin: (80/568)*self.FullHeight, width: (self.FullWidth/3), height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "buying", comment: ""), buttonTag: 2)
                
                
                self.headerLabelArray[reviewSelected].textColor = UIColor.red
                
                
                
            }
            else
            {
                
                headerOfTable = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (80/568)*self.FullHeight))
                
                headerOfTable.backgroundColor = UIColor(red:242/255, green:242/255, blue:242/255, alpha: 1)
                
                createHeaderOfTableview(headerView: headerOfTable)
                
            }
            
            ///////////
            
            headerOfTable.layer.borderWidth = 2
            headerOfTable.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
            
            
            
            return headerOfTable
            
        }
        else
        {
            return nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == businessUserTableview
        {
            
            if clickedOnReviews == true
            {
                return (120/568)*self.FullHeight
            }
            else
            {
                return (80/568)*self.FullHeight
            }
            
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == businessUserTableview
        {
            
            if listingType.elementsEqual("P")  // Product Listing
            {
                if aboutClicked == true
                {
                    return 1
                }
                else
                {
                    return recordsArray.count
                }
            }
            else if listingType.elementsEqual("R") // Reviews Listing
            {
                return recordsArray.count
            }
            else if listingType.elementsEqual("CP") // Company Profile
            {
                return recordsArray.count
            }
            else  // Contact
            {
                return recordsArray.count + 1
            }
        }
        else
        {
            return categoryArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if tableView == businessUserTableview
        {
            
            if listingType.elementsEqual("R")  // Review Listing
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! BUReviewsTVCTableViewCell
                
                cell.reviewsNameLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["username"]!)"
                
                cell.reviewsDateLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["datetime"]!)"
                
                cell.reviewsCommentLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["comment"]!)"
                
                
                cell.ratingNoLBL.text =  "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)"
                
                cell.totalRatingLBL.text = "(" + "\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"]!)"
                
                if "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)".elementsEqual("1")
                {
                   
                    cell.ratingview.backgroundColor = UIColor(red: 246, green: 15, blue: 49)
                }
                
                else if "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)".elementsEqual("2")
                {
                    
                   
                    cell.ratingview.backgroundColor = UIColor(red: 242, green: 144, blue: 0)
                }
                
                else
                {
                    
                   
                    cell.ratingview.backgroundColor = UIColor(red: 0, green: 119, blue: 0)
                }
                
                
                
                //set font
                cell.reviewsNameLBL.font = UIFont(name: cell.reviewsNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                 cell.totalRatingLBL.font = UIFont(name: cell.totalRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                 cell.ratingNoLBL.font = UIFont(name: cell.ratingNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.reviewsDateLBL.font = UIFont(name: cell.reviewsDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.reviewsCommentLBL.font = UIFont(name: cell.reviewsCommentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                ///////////
                cell.cellView.layer.cornerRadius = 8
                
                
                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
            }
            else if listingType.elementsEqual("CP")  // Company Profile Listing
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! BUReviewsTVCTableViewCell
                
                cell.reviewsNameLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["title"]!)"
                
                cell.reviewsDateLBL.text = ""
                
                cell.reviewsCommentLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["description"]!)"
                
                
                //set font
                cell.reviewsNameLBL.font = UIFont(name: cell.reviewsNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.reviewsDateLBL.font = UIFont(name: cell.reviewsDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                cell.reviewsCommentLBL.font = UIFont(name: cell.reviewsCommentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
                ///////////
                cell.cellView.layer.cornerRadius = 8
                
                
                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                return cell
            }
            else if listingType.elementsEqual("P")  // Product Listing
            {
                
                if aboutClicked == true
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell") as! BUAboutTVCTableViewCell
                    
                    cell.sendMessageButton.addTarget(self, action: #selector(self.ShowMessageView(sender:)), for: .touchUpInside)
                    
                    cell.qrImage.sd_setImage(with: URL(string: "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["qr_code"]!)"))
                    
                    cell.profileName.text = "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["display_name"]!)"
                    
                    cell.accountNo.text = "Account No. " + "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["account_no"]!)"
                    
                    cell.memberSince.text = "Member Since:" + "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["reg_date"]!)"
                    
                    cell.views.text = "Views : " + "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["total_view"]!)"
                    
                     //cell.totalfeedback.text = "Total Feedback : " + "\(self.SellerInfoDict["total_feedback"] ?? "")"
                    
                    cell.totalfeedback.text = "Total Feedback : " + "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["total_feedback"]!)"
                    
                    //cell.outof5.text = "\(self.SellerInfoDict["total_review"] ?? "")" + " Out Of 5 Stars"
                    
                    cell.outof5.text = "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["rating"]!)" + " Out Of 5 Stars"
                    
                    if "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["bonus_status"]!)".elementsEqual("1")
                    {
                        cell.bonusView.isHidden = false
                        //cell.bonusImageview.sd_setImage(with: URL(string: "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["bonus_status_logo"]!)"))
                        cell.bonusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bonus", comment: "")
                        
                        self.profileStatusStringArray.append("B")
                    }
                    else
                    {
                        cell.bonusView.isHidden = true
                    }
                    
                    if "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["verified_status"]!)".elementsEqual("V")
                    {
                        cell.verifiedView.isHidden = false
                        //cell.verifiedImageview.sd_setImage(with: URL(string: "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["verified_status_logo"]!)"))
                        cell.verifiedLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verified", comment: "")
                        
                        self.profileStatusStringArray.append("V")
                    }
                    else
                    {
                        cell.verifiedView.isHidden = true
                    }
                    
                    if "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["marrof_status"]!)".elementsEqual("1")
                    {
                        cell.maroofView.isHidden = false
                        //cell.maroofImageview.sd_setImage(with: URL(string: "\((businessUserDataDictionary["seller_info"] as! NSDictionary)["marrof_status_logo"]!)"))
                        cell.maroofLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "maroof", comment: "")
                        
                        self.profileStatusStringArray.append("M")
                    }
                    else
                    {
                        cell.maroofView.isHidden = true
                    }
                    
                    if profileStatusStringArray.count > 0
                    {
                        cell.profileStatusImageView.isHidden = false
                    }
                    else
                    {
                        cell.profileStatusImageView.isHidden = true
                    }
                    
                    
                    //
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    return cell
                }
                else
                {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! BUListingsTVCTableViewCell
                    
                    
                    if langID.elementsEqual("AR")
                           {
                               if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "RightUsed")
                                   cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                                   cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)

                               }
                               else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "RightNew")
                                   cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                                    cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
                               }
                               else
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "")
                                   cell.ProductCondition.text = ""
                                    cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
                               }
                           }
                           else
                           {
                               if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "LeftUsed")
                                   cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                                    cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
                               }
                               else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "LeftNew")
                                   cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                                    cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
                               }
                               else
                               {
                                   cell.ProductConditionimage.image = UIImage(named: "")
                                   cell.ProductCondition.text = ""
                                   cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
                               }
                           }
                    
                    
                    
                    
                    cell.listingImageview.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"))
                    
                    cell.listingTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
                   // cell.listingType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)"
                    cell.listingType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)"
                    
                     cell.type.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)"
                     //cell.type.textColor = UIColor.seaGreen()
                    
//                    if "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)".elementsEqual("")
//                    {
//                        cell.closesInLBL.isHidden = true
//                    }
//                    else
//                    {
                      //  cell.closesInLBL.isHidden = false
                    cell.closesInLBL.text =  "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)"
             //       }
                    
//                    if "\((recordsArray[indexPath.row] as! NSDictionary)["expire_time"]!)".elementsEqual("")
//                    {
//                        cell.listedOnLBL.isHidden = true
//                    }
//                    else
//                    {
                       // cell.listedOnLBL.isHidden = false
                        cell.listedOnLBL.text =  "\((recordsArray[indexPath.row] as! NSDictionary)["expire_time"]!)"
                  //  }
                    
                    cell.addressLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
                    cell.addressLBL.numberOfLines = 0
                    cell.addressLBL.lineBreakMode = NSLineBreakMode.byWordWrapping
                    cell.addressLBL.sizeToFit()
                    
                    
                    
                    
                    cell.listingPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                    
                    cell.addToWatchlistButton.layer.cornerRadius = cell.addToWatchlistButton.frame.size.height / 2
                    
                    // Price and Reserved Price Checking

                               if  "\((recordsArray[indexPath.row] as! NSDictionary)["special_offeer"]!)".elementsEqual("0")
                               {
                                  
                               
                                cell.listingPrice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                                           cell.listingPrice.textColor = UIColor.seaGreen()
                                           cell.reverseprice.text = ""
                                
                               }
                               else if  "\((recordsArray[indexPath.row] as! NSDictionary)["special_offeer"]!)".elementsEqual("1")
                               
                               {
                                
                                cell.reverseprice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
                                           cell.reverseprice.textColor = UIColor.red
                                          // let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                                          // cell.listingPrice.attributedText = txt.strikeThroughone()
                                          // cell.listingPrice.textColor = UIColor.seaGreen()
                                let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
                                 cell.reverseprice.attributedText = txt.strikeThroughone()

                               }
                    else
                               {
                                print("nothing")
                    }
                    
                    
                    //set font
                    cell.listingTitle.font = UIFont(name: cell.listingTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    cell.listingType.font = UIFont(name: cell.listingType.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
                    cell.closesInLBL.font = UIFont(name: cell.closesInLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                    cell.listedOnLBL.font = UIFont(name: cell.listedOnLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
                    cell.addressLBL.font = UIFont(name: cell.addressLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    cell.listingPrice.font = UIFont(name: cell.listingPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    cell.reverseprice.font = UIFont(name: cell.reverseprice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    cell.addToWatchlistButton.titleLabel?.font = UIFont(name: (cell.addToWatchlistButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
                    
                    //
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    //
                    cell.productView.layer.cornerRadius = 8
                    
                    cell.addToWatchlistButton.tag = indexPath.row
                    
                    cell.addToWatchlistButton.backgroundColor = .clear
                    
                    cell.addToWatchlistButton.setTitle("", for: .normal)
                    
                    if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"] ?? "")".elementsEqual("0")
                    {
                        cell.addToWatchlistButton.setImage(UIImage(named: "heart"), for: .normal)
                        
                        cell.addToWatchlistButton.addTarget(self, action: #selector(self.watchlistBtnTarget(sender:)), for: .touchUpInside)
                    }
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"] ?? "")".elementsEqual("1")
                    {
                        cell.addToWatchlistButton.setImage(UIImage(named: "heart_red"), for: .normal)
                        
                        cell.addToWatchlistButton.addTarget(self, action: #selector(self.watchlistedBtnTarget(sender:)), for: .touchUpInside)
                    }
                    else
                    {
                        cell.addToWatchlistButton.setImage(UIImage(named: "heart"), for: .normal)
                        
                        cell.addToWatchlistButton.addTarget(self, action: #selector(self.watchlistBtnTarget(sender:)), for: .touchUpInside)
                    }
                    
                 //cell.addToWatchlistButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: ""), for: .normal)
                    
                    return cell
                }
                
            }
            else  if listingType.elementsEqual("C")// contact listing
            {
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell") as! BUContactsTVCTableViewCell
                     print(recordsArray)
                     print(businessUserDataDictionary)
                     cell.contactName.text = "\(businessUserDataDictionary["website"]!)"

                     cell.contactNo.text = "\(businessUserDataDictionary["mobile_no"]!)"
                     cell.email.text = "\(businessUserDataDictionary["email"]!)"
                    
                     
                     
//                     cell.contactPersonalINfoView.frame = CGRect(x: 0, y: 0, width: (300/320)*self.FullWidth, height: (100/568)*self.FullHeight)
//
//                     cell.addressView.frame = CGRect(x: 0, y: (110/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (160/568)*self.FullHeight)
//
//
//                     let availabilityView = UIView(frame: CGRect(x: 0, y: cell.addressView.frame.origin.y + cell.addressView.frame.size.height, width: self.FullWidth, height: 1))
//
//                     let titleLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (280/320)*self.FullWidth, height: (25/568)*self.FullHeight))
//
//                     titleLBL.text = "Service Available Time"
//
//                     availabilityView.addSubview(titleLBL)
//
//                     var tempOrigin = titleLBL.frame.origin.y + titleLBL.frame.size.height + (5/568)*self.FullHeight
//
//                     for i in 0..<(businessUserDataDictionary["service_available"] as! NSArray).count
//                     {
//                         let dayName = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: tempOrigin, width: (80/320)*self.FullWidth, height: (15/568)*self.FullHeight))
//
//                         let availableTime = UILabel(frame: CGRect(x: (90/320)*self.FullWidth, y: tempOrigin, width: (190/320)*self.FullWidth, height: (15/568)*self.FullHeight))
//
//                         dayName.text = "\(((businessUserDataDictionary["service_available"] as! NSArray)[i] as! NSDictionary)["day"]!)"
//                         availableTime.text = "\(((businessUserDataDictionary["service_available"] as! NSArray)[i] as! NSDictionary)["time"]!)"
//
//                         availabilityView.addSubview(dayName)
//                         availabilityView.addSubview(availableTime)
//
//                         tempOrigin = tempOrigin + (15/568)*self.FullHeight
//
//                         print("AvailabilityVIew Height",(availabilityView.frame.size.height))
//                         print("AvailabilityVIewOrigin",(availabilityView.frame.origin.y))
//                     }
//                     cell.addSubview(availabilityView)
                     print(businessUserDataDictionary)
                     print("\(((businessUserDataDictionary["map_address"] as! NSArray)[0] as! NSDictionary)["lat"]!)")
                     print("\(((businessUserDataDictionary["map_address"] as! NSArray)[0] as! NSDictionary)["long"]!)")
                     
                     self.showLOcation(mapView: cell.addressMap, addressLat: "\(((businessUserDataDictionary["map_address"] as! NSArray)[0] as! NSDictionary)["lat"] ?? "")", addressLong: "\(((businessUserDataDictionary["map_address"] as! NSArray)[0] as! NSDictionary)["long"] ?? "")")
                     
                     //
                     cell.selectionStyle = UITableViewCell.SelectionStyle.none
                     
                     return cell

                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell2") as! BUContactsTVCTableViewCell2
                    print(recordsArray)
                    cell.name.text = "\((recordsArray[indexPath.row - 1] as! NSDictionary)["store_name"] ?? "")"
                    cell.mapImg.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row - 1] as! NSDictionary)["address_image"]!)"))
                    cell.address.text = "\((recordsArray[indexPath.row - 1] as! NSDictionary)["address"] ?? "")"
                    cell.webImg.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row - 1] as! NSDictionary)["website_url_image"]!)"))
                    cell.webAddress.text = "\((recordsArray[indexPath.row - 1] as! NSDictionary)["website_url"] ?? "")"
                    cell.phImg.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row - 1] as! NSDictionary)["landline_number_image"]!)"))
                    cell.phNo.text = "\((recordsArray[indexPath.row - 1] as! NSDictionary)["landline_number"] ?? "")"
                    cell.dayOpen.text = "\((((recordsArray[indexPath.row - 1] as! NSDictionary)["service_available_time_new"] as! NSArray)[0] as! NSDictionary)["day"] ?? "")"
                    cell.time.text = "\((((recordsArray[indexPath.row - 1] as! NSDictionary)["service_available_time_new"] as! NSArray)[0] as! NSDictionary)["time"] ?? "")"
                    return cell
                }
                                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "listingCell") as! BUListingsTVCTableViewCell
                
                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                
                return cell
            }
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell")
            
            cell?.textLabel?.text = "\(categoryArray[indexPath.row])"
            
            return cell!
        }
        
    }
    
    
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //
    //        let cell = cell as! BUListingsTVCTableViewCell
    //
    //        cell.productView.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (180/568)*self.FullHeight)
    //        cell.listingImageview.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (80/320)*self.FullWidth, height: (80/568)*self.FullHeight)
    //        cell.listingTitle.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //        cell.listingType.frame = CGRect(x: (230/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (60/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //        cell.closesInLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (190/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //        cell.listedOnLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (55/568)*self.FullHeight, width: (190/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //        cell.addressPlaceholderImageview.frame = CGRect(x: (100/320)*self.FullWidth, y: (75/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //        cell.addressLBL.frame = CGRect(x: (125/320)*self.FullWidth, y: (75/568)*self.FullHeight, width: (165/320)*self.FullWidth, height: (30/568)*self.FullHeight)
    //        cell.addToWatchlistButton.frame = CGRect(x: (190/320)*self.FullWidth, y: (125/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (35/568)*self.FullHeight)
    //        cell.productView.frame = CGRect(x: (10/320)*self.FullWidth, y: (135/568)*self.FullHeight, width: (80/320)*self.FullWidth, height: (20/568)*self.FullHeight)
    //
    //    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        if tableView == businessUserTableview
    //        {
    //
    //            if listingType.elementsEqual("P")  // product listing
    //            {
    //                return (190/568)*self.FullHeight
    //            }
    //            else if listingType.elementsEqual("R")  // review listing
    //            {
    //                return (80/568)*self.FullHeight
    //            }
    //            else if listingType.elementsEqual("CP") // complany profile listing
    //            {
    //                return (80/568)*self.FullHeight
    //            }
    //            else //  contact listing
    //            {
    //                return (400/568)*self.FullHeight
    //                //return contactCellHeight
    ////                let tempHEight = ((305/568)*self.FullHeight + (((15/568)*self.FullHeight) * CGFloat((businessUserDataDictionary["service_available"] as! NSArray).count)))
    ////                print("tempheight",tempHEight)
    ////                return tempHEight
    //            }
    //        }
    //        else
    //        {
    //            return (40/568)*self.FullHeight
    //        }
    //
    //    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == businessUserTableview
        {
            if sellerID.elementsEqual("default")
            {
                sellerID = userID
            }
            if self.listingType.elementsEqual("P")
            {
                if self.aboutClicked == false
                {
                    //Daily Deals Product View Controller
                    
                    if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("9")
                   {
                    if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                    {
                        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController

                        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                        self.navigationController?.pushViewController(navigate, animated: true)
                    }
                    else
                    {
                        
                    }
                   }
                        //MarketPlace Product Details View Controller
                        
                   else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("10")
                   {
                    if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                    {
                        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController

                        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                        self.navigationController?.pushViewController(navigate, animated: true)
                    }
                    else
                    {
                        
                    }
                   }
                        //Daily Rental Checking
                        
                   else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("15")
                   {
                    if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                    {
                        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyrental") as! NewDailyRentalProductDetailsViewController
                        
                       nav.DailyRentalProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                       self.navigationController?.pushViewController(nav, animated: true)
                    }
                    else
                    {
                        
                    }
                    
                   }
                        //Real Estate Checking
                        
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("999")
                    {
                        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                        {
                            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateDetailsVC") as! NewRealEstateDetailsViewController
                            
                            nav.productID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                            
                            self.navigationController?.pushViewController(nav, animated: true)
                        }
                        else
                        {
                            
                        }
                        
                    }
                    
                    //Food Court Checking
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("13")
                    {
                        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                        {
                            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController
                            
                            navigate.sellerID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["seller_id"] ?? "\(sellerID!)")"
                            
                            self.navigationController?.pushViewController(navigate, animated: true)
                        }
                        else
                        {
                            
                        }
                    }
                       // Services Checking
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("11")
                    {
                        
                      if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"] ?? "")".elementsEqual("")
                      {
                        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
                        
                        obj.productID =   "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                        
                        obj.SID = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_id"] ?? "\(sellerID!)")"
                        
                        self.navigationController?.pushViewController(obj, animated: true)
                      }
                        else
                      {
                        
                        }
                      
                    }
                        //Happening Checking
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("14")
                    {
                        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                        {
                            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController
                            
                            navigate.eventID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
                            
                            self.navigationController?.pushViewController(navigate, animated: true)
                        }
                        else
                        {
                            
                        }
                        
                    }
                        //Jobs Checking
                    else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["category_id"] ?? "")".elementsEqual("12")
                    {
                        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)".elementsEqual("")
                        {
                            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobdetails") as! NewJobDetailsViewController

                            navigate.JobId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"

                            self.navigationController?.pushViewController(navigate, animated: true)
                        }
                        else
                        {
                            
                        }
                    }
                    
                }

            }
            else
            {
                //do nothing
            }
        }
        else
        {
            print("categoryArray--------",categoryArray)
            
            categoryDataLBLArray[0].text = "\(categoryArray[indexPath.row])"
            
            UIView.animate(withDuration: 0.5)
            {
                self.view.sendSubviewToBack(self.categoryView)
                
                self.categoryView.isHidden = true
            }
            
        }
        
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to get User Profile Data
    
    func getUserProfileData()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
            
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_business_user_details")!   //change the url
        
        print("Business User Profile Data URL ------",url)
        
        var parameters : String = ""
        
        let myCountry = UserDefaults.standard.string(forKey: "myCountryName")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if sellerID.elementsEqual("default")
        {
            sellerID = userID
        }
        
        if clickedOnReviews == true
        {
            
            parameters = "seller_id=\(sellerID!)&login_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&start=&type=\(reviewType!)&category_id=\(self.SelectedCategoryID!)&child_id=&listing_type=\(listingType!)&mycountry_name=\(myCountry!)"
            
            
        }
        else
        {
            
            //https://kaafoo.com/appcontrol/app_business_user_details?seller_id=19&start_value=0&per_load=10&search=&login_id=19&category_id=9&child_id=
            
            parameters = "seller_id=\(sellerID!)&login_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&start=&type=&category_id=\(self.SelectedCategoryID!)&child_id=&listing_type=\(listingType!)&search=\(searchText ?? "")&mycountry_name=\(myCountry!)"
            
        
            
        }
        
        print("Parameters are : " , parameters)
        
        print("Let Url..",url)
        
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
//        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Business User Profile Response------: " , json)
                    
                    if self.listingType.elementsEqual("P")
                    {
                         print("maincategoryarray--------",self.mainCategoryArray)
                        
                        self.businessUserDataDictionary = json["info_array"] as? NSMutableDictionary
                        
                        print("businessUserDataDictionary----------",self.businessUserDataDictionary)
                        
                        self.categoryArray.removeAll()
                        self.categoryArray.append(LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: ""))
                        
                        for i in 0..<((self.businessUserDataDictionary)["category_info"] as! NSArray).count
                        {
                            
                       self.categoryArray.append("\(((self.businessUserDataDictionary["category_info"] as! NSArray)[i] as! NSDictionary)["name"]!)")
                            
                        }
                        
                        for i in 0..<((self.businessUserDataDictionary)["product_info"] as! NSArray).count
                            
                        {
                            
                            let tempDict = ((self.businessUserDataDictionary)["product_info"] as! NSArray)[i] as! NSDictionary
                            
                            self.recordsArray.add(tempDict as! NSMutableDictionary)
                            
                        }
                        self.nextStart = "\(json["next_start"] ?? "")"
                        
                        print("nextstart------",self.nextStart)
                        
                        self.startValue = self.startValue + ((self.businessUserDataDictionary)["product_info"] as! NSArray).count
                        
                        print("Next Start Value : " , self.startValue)
                        
                    }
                    else if self.listingType.elementsEqual("R")
                    {
                        
                        self.businessUserDataDictionary = json["info_array"] as? NSMutableDictionary
                        
                        if ((self.businessUserDataDictionary)["review_section"] as! NSArray).count > 0 {
                            
                            for i in 0..<((self.businessUserDataDictionary)["review_section"] as! NSArray).count
                                
                            {
                                
                                let tempDict = ((self.businessUserDataDictionary)["review_section"] as! NSArray)[i] as! NSDictionary
                                
                                self.recordsArray.add(tempDict as! NSMutableDictionary)
                                
                                
                            }
                            
                            self.nextStart = "\(json["next_start"]!)"
                            
                            self.startValue = self.startValue + ((self.businessUserDataDictionary)["review_section"] as! NSArray).count
                            
                            print("Next Start Value : " , self.startValue)
                            
                        }
                        
                    }
                    else if self.listingType.elementsEqual("CP")
                    {
                        
                        let tempArray = (json["info_array"] as! NSDictionary)["title_description"] as! NSArray
                        
                        for i in 0..<tempArray.count
                            
                        {
                            
                            let tempDict = tempArray[i] as! NSDictionary
                            
                            self.recordsArray.add(tempDict as! NSMutableDictionary)
                            
                            
                        }
                        
                        self.nextStart = ""
                        
                        self.startValue = 0
                        
                        print("Next Start Value : " , self.startValue)
                        
                    }
                    else if self.listingType.elementsEqual("C")
                    {
                        
                        
                        self.businessUserDataDictionary = json["info_array"] as? NSMutableDictionary
                        
                        for i in 0..<(self.businessUserDataDictionary["more_info"] as! NSArray).count

                        {

                        let tempDict = (self.businessUserDataDictionary["more_info"] as! NSArray)[i] as! NSDictionary

                        self.recordsArray.add(tempDict as! NSMutableDictionary)
                                           
                        print("recordsArray--------",self.recordsArray)
                                           

                        }
                        
                        self.nextStart = ""
                        
                        self.startValue = 0
                        
                        print("Next Start Value : " , self.startValue)
                        
                    }
                    
                    
                    
                    DispatchQueue.main.async {
                        
                        self.businessUserTableview.delegate = self
                        self.businessUserTableview.dataSource = self
                        self.businessUserTableview.reloadData()
                        
                        // Showing text in tableviewHeaderforsection 0
                        
                        SVProgressHUD.dismiss()
                        
                        self.dispatchGroup.leave()
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    func getData()
       {
           print("SellerID------",sellerID ?? "")
        
        
           let langID = UserDefaults.standard.string(forKey: "langID")
           
           let parameters = "login_id=\(userID!)&lang_id=\(langID!)&seller_id=\(sellerID!)"
           
           self.CallAPI(urlString: "app_service_user_details_main", param: parameters, completion: {
               
               self.globalDispatchgroup.leave()
               
               SVProgressHUD.dismiss()
               
               let infoArrayDict = self.globalJson["info_array"] as! NSDictionary
            
                print("infoArrayDict---------",infoArrayDict)
               
               self.SellerInfoDict = (infoArrayDict["seller_info"] as! NSDictionary)
            
            print("SellerInfoDict---------",self.SellerInfoDict)
            
               
               self.globalDispatchgroup.notify(queue: .main, execute: {
                   
                   DispatchQueue.main.async {
                       
                     //  self.setData()
                   }
                   
                   self.view.isHidden = false
               })
           })
       }
       
  func setData()
    {
        headerView.headerViewTitle.text = "\(self.SellerInfoDict["businessname"] ?? "")"
        
        let bannerImageURL = "\(self.SellerInfoDict["banner"] ?? "")"
        
        let logoImageURL = "\(self.SellerInfoDict["logo"] ?? "")"
        
        let QRImageURL = "\(self.SellerInfoDict["qrcode_img"] ?? "")"
        
//        self.BannerImageView.sd_setImage(with: URL(string: bannerImageURL))
//
//        self.ProfileImageView.sd_setImage(with: URL(string: logoImageURL))
//
//        self.QRImageView.sd_setImage(with: URL(string: QRImageURL))
//
//        self.AccountNumber.text = "Account Number : " + "\(self.SellerInfoDict["account_no"] ?? "")"
//
//        self.MemberSince.text = "Member Since : " + "\(self.SellerInfoDict["member_since"] ?? "")"
//
//        self.TotalViews.text = "Views : " + "\(self.SellerInfoDict["total_view"] ?? "")"
//
//        self.ServiceName.text = "\(self.SellerInfoDict["businessname"] ?? "")"
//
//        self.totalFeedback.text = "Total Feedback : " + "\(self.SellerInfoDict["total_feedback"] ?? "")"
//
//        self.totalRating.text = "\(self.SellerInfoDict["total_review"] ?? "")" + " Out Of 5 Stars"
        
    }
    
    // MARK: - // Tableview SEction Header Click
    
    @objc func headerSectionClicks(_ sender: UIButton) {
        
        if sender.tag == 0  // About
        {
            
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("About is Working")
            
            listingType = "P"
            
            clickedOnReviews = false
            
            aboutClicked = true
            
            getUserProfileData()
            
        }
        else if sender.tag == 1  // Listing
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Listing is Working")
            
            listingType = "P"
            
            clickedOnReviews = false
            
            aboutClicked = false
            
            getUserProfileData()
            
        }
        else if sender.tag == 2  // reviews
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Reviews is Working")
            
            listingType = "R"
            
            clickedOnReviews = true
            
            getUserProfileData()
            
            if self.headerLabelArray.count != 0
            {
                dispatchGroup.notify(queue: .main, execute: {
                    for i in 0..<self.headerLabelArray.count
                    {
                        self.headerLabelArray[i].textColor = UIColor.black
                    }
                    self.headerLabelArray[0].textColor = UIColor.red
                })
            }
            
        }
        else if sender.tag == 3  // company profile
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Company Profile is Working")
            
            listingType = "CP"
            
            clickedOnReviews = false
            
            getUserProfileData()
        }
        else if sender.tag == 4  // contacts
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Contact is Working")
            
            listingType = "C"
            
            clickedOnReviews = false
            
            getUserProfileData()
        }
        
    }
    
    // MARK: - // Review Click
    
    @objc func reviewClick(_ sender: UIButton) {
        
        for i in 0..<self.headerLabelArray.count
        {
            self.headerLabelArray[i].textColor = UIColor.black
        }
        self.headerLabelArray[sender.tag].textColor = UIColor.red
        
        
        if sender.tag == 0
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("All is Working")
            
            reviewType = "ALL"
            
            reviewSelected = 0
            
            getUserProfileData()
            
            dispatchGroup.notify(queue: .main, execute: {
                
                print("Header LBL Array-----",self.headerLabelArray)
                
                print("sender.tag-----",sender.tag)
            })
            
        }
        else if sender.tag == 1
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Selling is Working")
            
            reviewType = "SELL"
            
            reviewSelected = 1
            
            getUserProfileData()
            
            dispatchGroup.notify(queue: .main, execute: {
                
                print("Header LBL Array-----",self.headerLabelArray)
                
                print("sender.tag-----",sender.tag)
                
                self.headerLabelArray[sender.tag].textColor = UIColor.red
            })
            
        }
        else if sender.tag == 2
        {
            recordsArray.removeAllObjects()
            
            self.startValue = 0
            
            print("Buying is Working")
            
            reviewType = "BUY"
            
            reviewSelected = 2
            
            getUserProfileData()
            
            dispatchGroup.notify(queue: .main, execute: {
                
                print("Header LBL Array-----",self.headerLabelArray)
                
                print("sender.tag-----",sender.tag)
                
                self.headerLabelArray[sender.tag].textColor = UIColor.red
            })
            
        }
        
        
        
        
    }
    
    
    
    // MARK:- // Function Declaring the creation of Tableview Section Header while clickOnReviews == false
    
    func createHeaderOfTableview(headerView: UIView)
    {
        
        let separatorview = UIView(frame: CGRect(x: 0, y: (40/568)*self.FullHeight, width: self.FullWidth, height: (1/568)*self.FullHeight))
        
        separatorview.backgroundColor = UIColor.lightGray
        
        headerView.addSubview(separatorview)
        
        headerScrollview = UIScrollView(frame: CGRect(x: 0, y: (40/568)*self.FullHeight, width: self.FullWidth, height: (40/568)*self.FullHeight))
        
        //headerScrollview.backgroundColor = .red
        
        headerScrollview.contentSize = CGSize(width: (550/320)*self.FullWidth, height: (40/568)*self.FullHeight)
        
        headerScrollview.isScrollEnabled = true
        
        headerScrollview.isUserInteractionEnabled = true
        
        headerScrollview.clipsToBounds = true
        
        headerScrollview.showsHorizontalScrollIndicator = false
        
        headerScrollview.showsVerticalScrollIndicator = false
        
        headerScrollview.bounces = false
        
        headerView.addSubview(headerScrollview)
        
        createViewForTableviewHeaderSection(parentView: headerScrollview, xOrigin: 0, yOrigin: 0, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "about", comment: ""), buttonTag: 0)
        
        createViewForTableviewHeaderSection(parentView: headerScrollview, xOrigin: 100, yOrigin: 0, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "listings", comment: ""), buttonTag: 1)
        
        createViewForTableviewHeaderSection(parentView: headerScrollview, xOrigin: (200/320)*self.FullWidth, yOrigin: 0, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "review", comment: ""), buttonTag: 2)
        
        createViewForTableviewHeaderSection(parentView: headerScrollview, xOrigin: (300/320)*self.FullWidth, yOrigin: 0, width: (150/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "company_profile", comment: ""), buttonTag: 3)
        
        createViewForTableviewHeaderSection(parentView: headerScrollview, xOrigin: (450/320)*self.FullWidth, yOrigin: 0, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight, title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: ""), buttonTag: 4)
        
        createViewWithTableView(parentView: headerView, xOrigin: 0, yOrigin: 0)
        
    }
    
    
    // MARK:- // Function Declaring when user taps on "Listing" , "Reviews" , "Company Profile" , "Contact"
    
    func createViewForTableviewHeaderSection(parentView: AnyObject, xOrigin: CGFloat, yOrigin: CGFloat, width: CGFloat, height: CGFloat, title: String, buttonTag: Int)
    {
        
        let tempView = UIView(frame: CGRect(x: xOrigin, y: yOrigin, width: width, height: height))
        
        //tempView.backgroundColor = .yellow
        
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempLabel.textAlignment = .center
        
        tempLabel.font = UIFont(name: sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        tempLabel.text = title
        
        tempView.addSubview(tempLabel)
        
        let tempButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempButton.tag = buttonTag
        
        tempButton.addTarget(self, action: #selector(BusinessUserProfileViewController.headerSectionClicks), for: .touchUpInside)
        
        tempView.addSubview(tempButton)
        
        let separatorView = UIView(frame: CGRect(x: width - 1, y: (5/568)*self.FullHeight, width: 1, height: height - (5/568)*self.FullHeight))
        
        separatorView.backgroundColor = UIColor.black
        
        tempView.addSubview(separatorView)
        
        parentView.addSubview(tempView)
        
    }
    
    
    // MARK:- // Function Declaring when user Taps on "Reviews" of TableView Section Header
    
    func createViewForReviewClick(parentView: AnyObject, xOrigin: CGFloat, yOrigin: CGFloat, width: CGFloat, height: CGFloat, title: String, buttonTag: Int)
    {
        
        let tempView = UIView(frame: CGRect(x: xOrigin, y: yOrigin, width: width, height: height))
        
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempLabel.textAlignment = .center
        
        tempLabel.font = UIFont(name: sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        tempLabel.text = title
        
        headerLabelArray.append(tempLabel)
        
        tempView.addSubview(tempLabel)
        
        let tempButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        tempButton.tag = buttonTag
        
        tempButton.addTarget(self, action: #selector(BusinessUserProfileViewController.reviewClick), for: .touchUpInside)
        
        tempView.addSubview(tempButton)
        
        let separatorView = UIView(frame: CGRect(x: width - 1, y: (5/568)*self.FullHeight, width: 1, height: height - (5/568)*self.FullHeight))
        
        separatorView.backgroundColor = UIColor.black
        
        tempView.addSubview(separatorView)
        
        parentView.addSubview(tempView)
        
    }
    
    
    // MARK:- // Defining the Function to create a View with TableView
    
    
    func createViewWithTableView(parentView: UIView,xOrigin: CGFloat,yOrigin: CGFloat)
    {
        
       // parentView.backgroundColor = .green
        
      
        self.SearchBar = UISearchBar(frame: CGRect(x: (0/320)*self.FullWidth, y: 0, width: self.FullWidth, height: 50))
        
        SearchBar.setShowsCancelButton(true, animated: true)
     
        self.SearchBar.isHidden = true
               
       
        
        // initiating Data Label
        self.dataLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (140/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        
        self.dataLBL.textColor = UIColor.black
        
       // self.dataLBL.backgroundColor = .green
        
        self.dataLBL.font = UIFont(name: sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        self.dataLBL.textAlignment = .center
        
        self.dataLBL.text = "\(self.mainCategoryArray[SelectedPickerInt])"
        
        categoryDataLBLArray.removeAll()
        
        categoryDataLBLArray.append(self.dataLBL)
        
       // self.categoryDataLBLArray[0].text = self.categoryArray[0]
        
        // initiating drop down Button
        self.button = UIButton(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (200/320)*FullWidth, height: (40/568)*FullHeight))
        
        self.button.setTitleColor(UIColor.black, for: .normal)
        
        self.button.addTarget(self, action: #selector(clickOnDropDown), for: .touchUpInside)
        
        
        // initiating drop down image
        let dropDownImage = UIImageView(frame: CGRect(x: (180/320)*self.FullWidth, y: 0, width: (50/320)*self.FullWidth, height: (40/320)*self.FullWidth))
        
        dropDownImage.image = UIImage(named: "Triangle")
        
       // dropDownImage.backgroundColor = .blue
        
        dropDownImage.contentMode = .center
        
        // adding subviews
        parentView.addSubview(self.dataLBL)
        
        parentView.addSubview(button)
        
        parentView.addSubview(dropDownImage)
        
        // separatorviewOne
        let separatorViewOne = UIView(frame: CGRect(x: (219/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (1/320)*self.FullWidth, height: (30/568)*self.FullHeight))
        
        separatorViewOne.backgroundColor = UIColor.black
        
        parentView.addSubview(separatorViewOne)
        
        //search imageview
        let searchImageview = UIImageView(frame: CGRect(x: (220/320)*self.FullWidth, y: 0, width: (50/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        
        searchImageview.image = UIImage(named: "magnifyingGlass")
        
        searchImageview.contentMode = .center
               
        parentView.addSubview(searchImageview)
               
        
         let searchbutton = UIButton(frame: CGRect(x: (220/320)*self.FullWidth, y: 0, width: (50/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        
        searchbutton.backgroundColor = .clear
        
       
         searchbutton.addTarget(self, action: #selector(searchclicked), for: .touchUpInside)
         parentView.addSubview(searchbutton)
        
       
        
        // separatorviewTwo
        let separatorViewTwo = UIView(frame: CGRect(x: (269/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (1/320)*self.FullWidth, height: (30/568)*self.FullHeight))
        
        separatorViewTwo.backgroundColor = UIColor.black
        
        parentView.addSubview(separatorViewTwo)
        
        
        // map Imageview
//        let mapImageview = UIImageView(frame: CGRect(x: (270/320)*self.FullWidth, y: 0, width: (50/320)*self.FullWidth, height: (40/568)*self.FullHeight))
//
//        mapImageview.image = UIImage(named: "placeholder")
//
//        mapImageview.contentMode = .center
//
//        mapImageview.clipsToBounds = true
//
//        parentView.addSubview(mapImageview)
        
        var displaymapbutton = UIButton()
        
        displaymapbutton = UIButton(frame: CGRect(x: (280/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        
        displaymapbutton.setImage(UIImage(named: "Map-1"), for: .normal)
       
        displaymapbutton.addTarget(self, action: #selector(clickOndisplaymapbutton), for: .touchUpInside)
        
         parentView.addSubview(displaymapbutton)
        
         parentView.addSubview(SearchBar)
        
        SearchBar.delegate = self
        
    }
    
    @objc func clickOndisplaymapbutton(sender: UIButton!){
           
    let contact = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "contactVC") as! ContactViewController
        
        contact.sellerid = sellerID
           
            self.navigationController?.pushViewController(contact, animated: true)
    }
    
    @objc func searchclicked(sender: UIButton!){
    
        print("yes clicked")
        
        self.SearchBar.delegate = self
        
       self.SearchBar.isHidden = false
        
       self.hideKeyboardWhenTappedAround()
        
        //self.SearchBar.resignFirstResponder()
        
    
    }
    
    
    
    // MARK:- // Function defining Drop down click Action
    
    @objc func clickOnDropDown(sender: UIButton!){
        
//        if categoryView.isHidden == true
//        {
//            UIView.animate(withDuration: 0.5)
//            {
//
//                // initiating drop down tableview
//                let dropDownTableView = UITableView(frame: CGRect(x: 0, y: 0, width: (220/320)*self.FullWidth, height: (368/568)*self.FullHeight))
//
//                dropDownTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
//
//                dropDownTableView.isUserInteractionEnabled = true
//                dropDownTableView.isScrollEnabled = true
//                dropDownTableView.separatorStyle = .none
//
//                dropDownTableView.clipsToBounds = true
//
//
//                dropDownTableView.delegate = self
//                dropDownTableView.dataSource = self
//                dropDownTableView.reloadData()
//
//                self.dropDownView = UIView(frame: CGRect(x: (50/320)*self.FullWidth, y: (100/568)*self.FullHeight, width: (220/320)*self.FullWidth, height: (368/568)*self.FullHeight))
//
//                // adding subviews
//                self.dropDownView.addSubview(dropDownTableView)
//                self.categoryView.addSubview(self.dropDownView)
//
//
//                // show view
//                self.view.bringSubviewToFront(self.categoryView)
//                self.categoryView.isHidden = false
//
//            }
//        }
        
        self.ShowPickerView.isHidden = false
        
        self.DataPicker.selectRow(0, inComponent: 0, animated: false)
        
        self.view.bringSubviewToFront(self.ShowPickerView)
        
        
        recordsArray.removeAllObjects()
        
        self.startValue = 0
        
        print("Listing is Working")
        
        listingType = "P"
        
        clickedOnReviews = false
        
        aboutClicked = false
        
        getUserProfileData()
        
        
        
      
        
    }
    
    
    
    
    // MARK:- // Give Shadow to a UIView
    
    func castShadow(view: UIView)
    {
        let shadowSize : CGFloat = 10.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,y: -shadowSize / 2,width: view.frame.size.width + shadowSize,height: view.frame.size.height + shadowSize))
        
        self.view.layer.masksToBounds = false
        
        self.view.layer.shadowColor = UIColor.black.cgColor
        
        self.view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        self.view.layer.shadowOpacity = 0.5
        
        self.view.layer.shadowPath = shadowPath.cgPath
    }
    
    
    // MARK:- // Functions to show location
    
    
    func showLOcation(mapView: GMSMapView, addressLat: String, addressLong: String)
    {
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 12.0)
        
        // Creates a marker in the center of the map.
        //let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        
        marker.title = "Selected Address"
        
        marker.snippet = "Description"
        
        marker.map = mapView
        
        mapView.animate(to: camera)
        
        marker.isDraggable = true
    }
    
    @objc func ShowMessageView(sender : UIButton)
    {
        self.MessageView.isHidden = false
        
        self.setMessageBoxDetails()
        
        self.view.bringSubviewToFront(self.MessageView)
    }
    
    
    
    // MARK:- // SEt Font
    
    func set_font()
    {
        
        //profileTitleLBL.font = UIFont(name: profileTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
    }
    
    
}

extension BusinessUserProfileViewController : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.mainCategoryArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(self.mainCategoryArray[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        print("maincategoryarray--------",self.mainCategoryArray)
        
        self.SelectedPickerInt = row
        
        
        
        
        self.SelectedPickerIDInt = row
        
        
        //self.recordsArray.removeAllObjects()
        
        //self.getUserProfileData()
    }
}


class BUAboutTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var profileStatusImageView: UIView!
    @IBOutlet weak var profileInformationView: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var accountNo: UILabel!
    @IBOutlet weak var memberSince: UILabel!
    @IBOutlet weak var views: UILabel!
    
    @IBOutlet weak var sendMessageButton: UIButton!
    
    @IBOutlet weak var bonusView: UIView!
    @IBOutlet weak var profileStatusStackView: UIStackView!
    @IBOutlet weak var bonusImageview: UIImageView!
    @IBOutlet weak var bonusLBL: UILabel!
    
    @IBOutlet weak var maroofView: UIView!
    @IBOutlet weak var maroofImageview: UIImageView!
    @IBOutlet weak var maroofLBL: UILabel!
    
    @IBOutlet weak var verifiedView: UIView!
    @IBOutlet weak var verifiedImageview: UIImageView!
    @IBOutlet weak var verifiedLBL: UILabel!
    
    @IBOutlet weak var totalfeedback: UILabel!
    
    @IBOutlet weak var starview: CosmosView!
    
    @IBOutlet weak var outof5: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class BUListingsTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ProductConditionimage: UIImageView!
    
    //@IBOutlet weak var ProductCondition: UILabel!
    
    @IBOutlet weak var listingImageview: UIImageView!
    @IBOutlet weak var listingTitle: UILabel!
    @IBOutlet weak var listingType: UILabel!
    @IBOutlet weak var closesInLBL: UILabel!
    @IBOutlet weak var listedOnLBL: UILabel!
    @IBOutlet weak var addressPlaceholderImageview: UIImageView!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var listingPrice: UILabel!
    @IBOutlet weak var addToWatchlistButton: UIButton!
    
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var productView: UIView!
    
    @IBOutlet weak var reverseprice: UILabel!
    @IBOutlet weak var ProductConditionImage: UIImageView!
    
    @IBOutlet weak var ProductCondition: UILabel!
    
    //@IBOutlet weak var reviewcount: UILabel!
    
    @IBOutlet weak var starview: CosmosView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class BUReviewsTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var reviewsNameLBL: UILabel!
    @IBOutlet weak var reviewsDateLBL: UILabel!
    @IBOutlet weak var reviewsCommentLBL: UILabel!
    
    @IBOutlet weak var totalRatingLBL: UILabel!
    @IBOutlet weak var ratingNoLBL: UILabel!
    @IBOutlet weak var ratingNoStarImageview: UIImageView!
    
    @IBOutlet weak var ratingview: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class BUContactsTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellVIew: UIView!
    @IBOutlet weak var contactPersonalINfoView: UIView!
    
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressMap: GMSMapView!
    
    
    @IBOutlet weak var userInformationLBL: UILabel!
    @IBOutlet weak var contactNameLBL: UILabel!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactNoLBL: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var email: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class BUContactsTVCTableViewCell2: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var mapImg: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var webImg: UIImageView!
    @IBOutlet weak var webAddress: UILabel!
    @IBOutlet weak var phImg: UIImageView!
    @IBOutlet weak var phNo: UILabel!
    @IBOutlet weak var dayOpen: UILabel!
    @IBOutlet weak var time: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

// MARK:- //Strike Out Text
extension String {
    func strikeThroughone() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}


extension BusinessUserProfileViewController : UISearchBarDelegate
{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("Writing is in progress")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.SearchBar.searchTextField.text = ""
        
        self.SearchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("Search is in Progress")
        
        self.searchText = self.SearchBar.searchTextField.text
        
        self.recordsArray.removeAllObjects()
        
        self.getUserProfileData()
    }
}


extension BusinessUserProfileViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BusinessUserProfileViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
        self.SearchBar.isHidden = true
    }
}
