//
//  ChatListViewController.swift
//  Kaafoo
//
//  Created by esolz on 09/11/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ChatListViewController: GlobalViewController,UIScrollViewDelegate {

    @IBOutlet weak var ListTableView: UITableView!
    
    var userListArray : NSMutableArray! = NSMutableArray()
    
    var StartFrom : Int! = 0
    
    var PerPage : String! = "100"
    
    var Searchvalue : String! = ""
    
    var nextStart : String! = ""
    
    var ScrollBegin : CGFloat!
    
    var ScrollEnd : CGFloat!
    
    @IBOutlet weak var ViewSearch: UIView!
    
    @IBOutlet weak var hideSearchViewBtnOutlet: UIButton!
    
    @IBOutlet weak var SearchBox: UISearchBar!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.SearchBox.delegate = self
        
        self.getListing()
        
        self.ListTableView.separatorStyle = .none
        
        self.headerView.tapSearch.addTarget(self, action: #selector(self.showSearchView(sender:)), for: .touchUpInside)
        
        self.hideSearchViewBtnOutlet.addTarget(self, action: #selector(self.setSearchHidden(sender:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    //MAR:- //View Will Appear Method
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        SVProgressHUD.dismiss()
    }
    
    //MARK:- //SearchView Hide Button Selector Method
    
    @objc func setSearchHidden(sender : UIButton)
    {
        self.ViewSearch.isHidden = true
        
        self.view.sendSubviewToBack(self.ViewSearch)
    }
    
    //MARK:- //Showing Search Bar
    
    @objc func showSearchView(sender : UIButton)
    {
       print("ShowSearch")
        
        self.Searchvalue = ""
        
        self.SearchBox.text = ""
        
        self.ViewSearch.isHidden = false
        
        self.view.bringSubviewToFront(self.ViewSearch)
    }
    
    
    
    func getListing()
       {
           let UserID = UserDefaults.standard.string(forKey: "userID")
        
           let LangID = UserDefaults.standard.string(forKey: "langID")
        
           let parameters = "user_id=\(UserID!)&lang_id=\(LangID!)&start_from=\(StartFrom!)&per_page=\(PerPage!)&search_value=\(Searchvalue ?? "")"
        
           self.CallAPI(urlString: "app_user_chat_listing", param: parameters, completion: {
            
               self.globalDispatchgroup.leave()
            
               let infoArray  = self.globalJson["info_array"] as! NSArray
            
               for i in 0..<infoArray.count
               {
                   let tempdict = infoArray[i] as! NSDictionary
                
                   self.userListArray.add(tempdict)
               }
               self.nextStart = "\(self.globalJson["next_start"] ?? "")"
            
               self.StartFrom = self.StartFrom + self.userListArray.count
            
               self.globalDispatchgroup.notify(queue: .main, execute: {
                
                   DispatchQueue.main.async {
                    
                       self.ListTableView.delegate = self
                    
                       self.ListTableView.dataSource = self
                    
                       self.ListTableView.reloadData()
                    
                       SVProgressHUD.dismiss()
                   }
               })
           })
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - // Scrollview Delegates

         func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            
            if scrollView == self.ListTableView
            {
                ScrollBegin = scrollView.contentOffset.y
            }
            else
            {
                
            }
             
         }
         func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            
            if scrollView == self.ListTableView
            {
                ScrollEnd = scrollView.contentOffset.y
            }
            else
            {
                
            }
             
         }
         func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
            if scrollView == self.ListTableView
            {
                if ScrollBegin > ScrollEnd
                {

                }
                else
                {
                    if (nextStart).isEmpty
                    {
                        DispatchQueue.main.async {
                            
                        SVProgressHUD.dismiss()
                    }
                    }
                    else
                    {
                        self.getListing()
                    }
                }
            }
            else
            {
                
            }
             
         }

}
class ChatListTVC : UITableViewCell
{
    @IBOutlet weak var tableViewContent: UIView!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userDescription: UILabel!
    
    @IBOutlet weak var ShadowView: ShadowView!
    
    @IBOutlet weak var userStatus: UIImageView!
    
    
    @IBOutlet weak var datetimeLBL: UILabel!
    
}

extension ChatListViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.userListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ChatListTVC
        
        cell.userName.text = "\((self.userListArray[indexPath.row] as! NSDictionary)["receiver_name"] ?? "")"
        
         cell.datetimeLBL.text = "\((self.userListArray[indexPath.row] as! NSDictionary)["last_msg_send_time"] ?? "")"
        
        cell.userDescription.text = "\((self.userListArray[indexPath.row] as! NSDictionary)["last_message"] ?? "")"
        
        let imageURL = "\((self.userListArray[indexPath.row] as! NSDictionary)["receiver_image"] ?? "")"
        
        cell.userImage.sd_setImage(with: URL(string: imageURL))
        
        if "\((self.userListArray[indexPath.row] as! NSDictionary)["online_status"] ?? "")".elementsEqual("1")
        {
            cell.userStatus.image = UIImage(named: "Active green")
        }
        else
        {
            cell.userStatus.image = UIImage(named: "Oval")
        }
        cell.userImage.layer.cornerRadius = (cell.userImage.frame.size.height)/2
        
        cell.userImage.layer.masksToBounds = true
        
        cell.ShadowView.layer.cornerRadius = 5.0
        
        cell.tableViewContent.layer.cornerRadius = 5.0
        
        cell.tableViewContent.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       //do nothing
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatmessage") as! ChatMessageViewController
        
        navigate.SenderId = "\((self.userListArray[indexPath.row] as! NSDictionary)["receiverid"] ?? "")"
         navigate.receivername = "\((self.userListArray[indexPath.row] as! NSDictionary)["receiver_name"] ?? "")"
        
//        if "\((self.userListArray[indexPath.row] as! NSDictionary)["block_status"] ?? "")".elementsEqual("0")
//        {
//            navigate.ChatInputView.isHidden = true
//        }
//        else
//        {
//            navigate.ChatInputView.isHidden = false
//        }
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}


extension ChatListViewController : UISearchBarDelegate
{
    //MARK:- //Searchbar Delegate Methods
       
       func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
           print("cancelled")
        
           self.ViewSearch.isHidden = true
        
           self.view.sendSubviewToBack(self.ViewSearch)
       }
       
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
           self.Searchvalue = "\(self.SearchBox.text ?? "")"
       }
       
       func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("searchvalue",Searchvalue ?? "")
        
        self.view.endEditing(true)
        
        self.userListArray.removeAllObjects()
        
        self.StartFrom = 0
        
        self.getListing()
        
        self.ViewSearch.isHidden = true
        
        self.view.sendSubviewToBack(self.ViewSearch)
        
       }
       
       func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
           self.SearchBox.text = ""
        
           self.Searchvalue = ""
       }
    
}
