//
//  HelpViewController.swift
//  Kaafoo
//
//  Created by priya on 03/06/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class HelpViewController: GlobalViewController {
    
    
    @IBOutlet weak var localview: UIView!
    @IBOutlet weak var webview: UIWebView!
    
    @IBOutlet weak var closebtn: UIButton!
    @IBOutlet weak var titlelbl: UILabel!
    
    
     let langid = UserDefaults.standard.string(forKey: "langID")
    
     var datalist : NSMutableArray! = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        HelpAPI()
        
    }
    
    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        localview.isHidden = true
        webview.isHidden = true
    }
    
    
    func HelpAPI()
    {
       
        
        let parameters = "lang_id=\(self.langid ?? "")"
        
        self.CallAPI(urlString: "app_help_info", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.datalist = (self.globalJson["info_array"] as! NSMutableArray)

            print("datalist----------",self.datalist)
            
          
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
               
                
            })
            
            DispatchQueue.main.async {
                
                   SVProgressHUD.dismiss()
                
            }
            
        })
    }
    
   
    
    @IBAction func registrationClicked(_ sender: Any)
    {
        
        localview.isHidden = false
        webview.isHidden = false
        titlelbl.text = "Registering"
        let langID = UserDefaults.standard.string(forKey: "langID")
                     
        let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Registering&lang_id"+langID!)
                        
        let requestObj = URLRequest(url: url!)
                         
        self.webview.loadRequest(requestObj)
    }
    
    @IBAction func problemloginClicked(_ sender: Any)
    {
        
        localview.isHidden = false
        webview.isHidden = false
        titlelbl.text = "Problems logging in"
        let langID = UserDefaults.standard.string(forKey: "langID")
                 
        let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Problems-logging-in&"+langID!)
                    
        let requestObj = URLRequest(url: url!)
                     
        self.webview.loadRequest(requestObj)
    }
    

    @IBAction func paymentClicked(_ sender: Any) {
        
        localview.isHidden = false
        webview.isHidden = false
        titlelbl.text = "Payments & your account"
        let langID = UserDefaults.standard.string(forKey: "langID")
                            
                           
        let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                             
                            
        let requestObj = URLRequest(url: url!)
                             
        self.webview.loadRequest(requestObj)
        
    }
    
    @IBAction func businessmemberClicked(_ sender: Any)
    {
        
        localview.isHidden = false
        webview.isHidden = false
        titlelbl.text = "Business memberships"
        let langID = UserDefaults.standard.string(forKey: "langID")
                            
                           
        let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                             
                          
                             
        let requestObj = URLRequest(url: url!)
                             
        self.webview.loadRequest(requestObj)
    }
    
    @IBAction func findingitemsClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func buyerprotectionClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func biddingClicked(_ sender: Any) {
        
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func feesandfeecalClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func managingClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
    @IBAction func autobillingClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func myproductClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
    @IBAction func problemswithbuyerClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func termsandconditionClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func codeofconductClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func feedbackpolicyClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    @IBAction func humanrightsactClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
    @IBAction func buyerprotectionpolicyClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
   
    @IBAction func testhelpClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
    @IBAction func abcdClicked(_ sender: Any) {
        
        localview.isHidden = false
               webview.isHidden = false
               titlelbl.text = "Business memberships"
               let langID = UserDefaults.standard.string(forKey: "langID")
                                   
                                  
               let url = URL (string: "https://kaafoo.com/appcontrol/app_help_url_info?alias_url=Payments-&-your-account&"+langID!)
                                    
                                 
                                    
               let requestObj = URLRequest(url: url!)
                                    
               self.webview.loadRequest(requestObj)
    }
    
    
}
