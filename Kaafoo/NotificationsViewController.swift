//
//  NotificationsViewController.swift
//  Kaafoo
//
//  Created by esolz on 27/09/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Crashlytics


class NotificationsViewController: GlobalViewController {
    
    var ListingArray : NSMutableArray! = NSMutableArray()
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ListingtableView: UITableView!
    
    var ScrollBegin : CGFloat!
    
    var ScrollEnd : CGFloat!
    
    var StartValue : Int! = 0
    
    var PerLoad : String! = "10"
    
    var nextStart : String! = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.ListingtableView.separatorStyle = .none
        
//        self.headerView.crossButton.addTarget(self, action: #selector(self.Crash(sender:)), for: .touchUpInside)
        
        if userID?.isEmpty == true
        {
            let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:"Ok", comment: ""), style: .default, handler: {
                UIAlertAction in
                
                     let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                
                    self.navigationController?.pushViewController(navigate, animated: true)
            })
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
             self.getListing()
        }
        // Do any additional setup after loading the view.
    }
    
    

    func getListing()
    {
        let parameters = "lang_id=\(LangID!)&user_id=\(userID!)&start_value=\(StartValue ?? 0)&per_load=\(PerLoad ?? "")"
        
        self.CallAPI(urlString: "notification", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                    let infoDict = self.globalJson["notification_list"] as! NSDictionary
                
                    let InfoArray = infoDict["notification"] as! NSArray
                
                    for i in 0..<InfoArray.count
                    {
                        let tempdict = InfoArray[i] as! NSDictionary
                        
                        self.ListingArray.add(tempdict)
                    }
                     self.nextStart = "\(self.globalJson["next_start"]!)"
                
                     self.StartValue = self.StartValue + self.ListingArray.count
                
                    SVProgressHUD.dismiss()
            })
            DispatchQueue.main.async {
                
                self.ListingtableView.delegate = self
                
                self.ListingtableView.dataSource = self
                
                self.ListingtableView.reloadData()
            }
        })
    }
    
    @objc func DeleteNotification(sender : UIButton)
    {
        let adminID = "\((self.ListingArray[sender.tag] as! NSDictionary)["admin_notification_id"] ?? "")"
        
        let NotificationID = "\((self.ListingArray[sender.tag] as! NSDictionary)["id"] ?? "")"
        
        let parameters = "notification_id=\(NotificationID)&admin_notification_id=\(adminID)&lang_id=\(LangID!)&user_id=\(userID!)"
        
        self.CallAPI(urlString: "remove-notification", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            DispatchQueue.main.async {
                
                SVProgressHUD.dismiss()
                
                self.ListingArray.removeAllObjects()
                
                self.StartValue = 0
                
                self.getListing()
                
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(self.globalJson["message"] ?? "")")
            }
        })
    }
    
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func Crash(sender : UIButton)
    {
        Crashlytics.sharedInstance().crash()
    }

}
class NotificationListingsTVC : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var MessageBody: UILabel!
    
    @IBOutlet weak var NotificationTitle: UILabel!
    
    @IBOutlet weak var NotificationTiming: UILabel!
    
    @IBOutlet weak var NotificationDeleteBtnOutlet: UIButton!
}

extension NotificationsViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.ListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifications") as! NotificationListingsTVC
        
        cell.MessageBody.text =  "\((self.ListingArray[indexPath.row] as! NSDictionary)["name"] ?? "")"
        
        cell.NotificationTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "") + " : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
        
        cell.NotificationTiming.text = "\((self.ListingArray[indexPath.row] as! NSDictionary)["listed_time"] ?? "")"
        
        cell.NotificationDeleteBtnOutlet.tag = indexPath.row
        
        cell.NotificationDeleteBtnOutlet.addTarget(self, action: #selector(DeleteNotification(sender:)), for: .touchUpInside)
        
        cell.layoutIfNeeded()
        
        cell.ContentView.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.ContentView.layer.borderWidth = 1.0
        
        cell.ContentView.layer.cornerRadius = 5.0
        
        cell.ContentView.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        
        //Font
        
        cell.MessageBody.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 16.0)))
        
        cell.NotificationTitle.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 16.0)))
        
        cell.NotificationTiming.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 16.0)))
        
        //
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    // MARK: - // Scrollview Delegates

      func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
          ScrollBegin = scrollView.contentOffset.y
      }
      func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
          ScrollEnd = scrollView.contentOffset.y
      }
      func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
          if ScrollBegin > ScrollEnd
          {

          }
          else
          {
              if (nextStart).isEmpty
              {
                  DispatchQueue.main.async {
                    
                  SVProgressHUD.dismiss()
              }
              }
              else
              {
                  self.getListing()
              }
          }
      }
}
