//
//  NewProductDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 5/29/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class NewProductDetailsViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout {
   
    //Outlet For ShareingClass
    
    @IBOutlet weak var paymentviewheightcons: NSLayoutConstraint!
    @IBOutlet weak var paymenttitleviewheightconstraints: NSLayoutConstraint!
    @IBOutlet weak var kaafoologoimg: UIImageView!
    @IBOutlet weak var dailydealsLbl: UILabel!
    @IBOutlet weak var dailydealsimage: UIImageView!
    @IBOutlet weak var sendbuttonoutlet: UIButton!
    
    @IBOutlet weak var viewaccountoutlet: UIButton!
    var WatchlistStatus : Bool! = false
    
    var followunfollowButton : Bool! = false
    
    @IBOutlet weak var MessageView: UIView!
    
    @IBOutlet weak var hideMessageViewBtn: UIButton!
    
    @IBAction func hideMessageBtn(_ sender: UIButton) {
        
        self.MessageView.isHidden = true
        
        self.view.sendSubviewToBack(self.MessageView)
        
    }
    
    @IBOutlet weak var MessageBox: MessageBox!
    
    @IBOutlet weak var disCountedPrice: UILabel!
    
    @IBOutlet weak var ShareView: UIView!
    
    @IBAction func HideShareView(_ sender: UIButton) {
        
        self.ShareView.isHidden = true
        
    }
    @IBOutlet weak var SharingClass: SharingClass!
    
    @IBOutlet weak var AccountTypeLbl: UILabel!
    
    @IBOutlet weak var VerifiedImagesView: UIView!
    
    @IBAction func BiggerImageCrossBtn(_ sender: UIButton)
    {
        self.BiggerImageView.isHidden = true
    }

    @IBOutlet weak var BiggerImageCollectionView: UICollectionView!
    
    @IBOutlet weak var BiggerImageView: UIView!
    
    @IBOutlet weak var OtherSellerListingCollectionView: UICollectionView!
    
    @IBOutlet weak var AccountStatusImage: UIImageView!
    
    @IBOutlet weak var ButtonStackView: UIStackView!
    
    @IBOutlet weak var BreadCrumpBtnOutlet: UIButton!

    @IBOutlet weak var BuyNowOutlet: UIButton!

    @IBAction func BreadCrumpBtn(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController
        
        navigate.typeID = "\(self.ProductInfo["breadcrumb_bar_final_id"]!)"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }

    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var loadStr : String! = ""
    
    @IBOutlet weak var BidTableviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ReportLbl: UILabel!
    
    @IBOutlet weak var MainScroll: UIScrollView!
    
    @IBOutlet weak var TimeDateView: UIView!
    
    @IBOutlet weak var ImageCollectionView: UICollectionView!
    
    @IBOutlet weak var ImagePageControl: UIPageControl!
    
    @IBOutlet weak var ProductName: UILabel!
    
    @IBOutlet weak var ProductAddress: UILabel!
    
    @IBOutlet weak var ProductFeaturesTableview: UITableView!
    
    @IBOutlet weak var BreadCrumpView: UIView!
    
    @IBOutlet weak var DescriptionTitleView: UIView!
    
    @IBOutlet weak var DescriptionView: UIView!
    
    @IBOutlet weak var NoDescLabel: UILabel!
    
    @IBOutlet weak var QuestionAnswerTitleView: UIView!
    
    @IBOutlet weak var QNATableview: UITableView!
    
    @IBOutlet weak var AllQnaView: UIView!
    
    @IBOutlet weak var NoQNALabel: UILabel!
    
    @IBOutlet weak var ReviiewRatingTitleView: UIView!
    
    @IBOutlet weak var RatingsView: UIView!
    
    @IBOutlet weak var ReviewsTableView: UITableView!
    
    @IBOutlet weak var AllReviewsBtnView: UIView!
    
    @IBOutlet weak var PostedByTitleView: UIView!
    
    @IBOutlet weak var PostedByView: UIView!
    
    @IBOutlet weak var LocationTitleView: UIView!
    
    @IBOutlet weak var LocationView: UIView!
    
    @IBOutlet weak var ShippingOptionTitleView: UIView!
    
    @IBOutlet weak var ShippingOptionView: UIView!
    
    @IBOutlet weak var PaymentTitleView: UIView!
    
    @IBOutlet weak var PaymentView: UIView!
    
    @IBOutlet weak var SellerOtherListingTitleView: UIView!
    
    @IBOutlet weak var SellerListingCollectionView: UICollectionView!
    
    @IBOutlet weak var NoSellerPrductView: UIView!
    
    @IBOutlet weak var WatchlistBtnOutlet: UIButton!
    
    @IBOutlet weak var WatchlistedBtnOutlet: UIButton!
    
    @IBOutlet weak var ProductFeaturesTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SellerListingCollectionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SellerListingStackView: UIStackView!
    
    @IBOutlet weak var ShippingCompanyName: UILabel!
    
    @IBOutlet weak var ShippingCompanystackView: UIStackView!
    
    @IBOutlet weak var LocationStackView: UIStackView!
    
    @IBOutlet weak var LocationName: UILabel!
    
    @IBAction func AllReviewRatingBtn(_ sender: UIButton) {
        
        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductreview") as! NewProductDetailsAllReviewsViewController
        
        obj.productID = ProductID
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBOutlet weak var AllReviewRatingBtnOutlet: UIButton!
    
    @IBOutlet weak var ReviewsTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ReviewsTableStackView: UIStackView!
    
    @IBOutlet weak var RatingStackView: UIStackView!
    
    @IBOutlet weak var DescriptionStackView: UIStackView!
    
    @IBOutlet weak var DescriptionString: UILabel!
    
    @IBOutlet weak var TotalDescription: UILabel!
    
    @IBOutlet weak var BreadCrump: UILabel!
    
    @IBOutlet weak var ShowTotalDescriptionBtnOutlet: UIButton!
    
    @IBOutlet weak var NoDescriptionLabel: UILabel!
    
    @IBOutlet weak var QuestionAnswerStackView: UIStackView!
    
    @IBOutlet weak var QuestionAnswerTableStackView: UIStackView!
    
    @IBOutlet weak var AllQNABtnOutlet: UIButton!
    
    @IBAction func AllQnaBtn(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateAllQnaVC") as! NewRealEstateAllQNAViewController
        
        navigate.productID = self.ProductID
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    @IBOutlet weak var NoQNALbl: UILabel!
    
    @IBOutlet weak var ReviewRatingStackView: UIStackView!
    
    @IBOutlet weak var NoProductFoundLbl: UILabel!
    
    @IBOutlet weak var QnaTableviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SellerName: UILabel!
    
    @IBOutlet weak var PositiveFeedbackLabel: UILabel!
    
    @IBOutlet weak var memberSince: UILabel!
    
    @IBOutlet weak var RatingStar: UILabel!
    
    @IBOutlet weak var ReportView: UIView!
    
    @IBAction func ReportBtn(_ sender: UIButton) {
        
        self.view.bringSubviewToFront(ReportBlurView)
        
        self.ReportBlurView.isHidden = false
    }
    
    @IBOutlet weak var ReportBtnOutlet: UIButton!
    
    @IBOutlet weak var ProductSellerImage: UIImageView!
    
    @IBOutlet weak var FiveStarReviews: UILabel!
    
    @IBOutlet weak var FourStarReviews: UILabel!
    
    @IBOutlet weak var ThreestarReviews: UILabel!
    
    @IBOutlet weak var TwoStarReviews: UILabel!
    
    @IBOutlet weak var FirstStarReviews: UILabel!
    
    @IBOutlet weak var DescriptionStringHeightConstraint: NSLayoutConstraint!
    
    @IBAction func ShowFullDescriptionBtn(_ sender: UIButton) {
        
        if DescHeightCheck == false
        {
            //            self.DescriptionStringHeightConstraint.constant = 20
            self.ShowTotalDescriptionBtnOutlet.isHidden = true
        }
        else
        {
            self.ShowTotalDescriptionBtnOutlet.isHidden = false
            //            self.DescriptionString.removeConstraint(self.DescriptionString?.heightAnchor)
            self.ShowTotalDescriptionBtnOutlet.setTitle("Hide Description", for: .normal)
            
        }
    }
    
    @IBOutlet weak var ListedTime: UILabel!
    
    @IBAction func AddToCartBtn(_ sender: UIButton) {
    }
    
    @IBAction func BuyNowBtn(_ sender: UIButton) {
        
        self.CheckProduct()
    }
    @IBOutlet weak var ClosingTime: UILabel!
    
    @IBOutlet weak var LocationOutlet: UILabel!
    
    @IBOutlet weak var ContactOutlet: UILabel!
    
    @IBOutlet weak var MemberOutlet: UILabel!
    
    @IBOutlet weak var QuantityOutlet: UILabel!
    
    @IBOutlet weak var WatchlistOutlet: UILabel!
    
    @IBOutlet weak var ViewsOutlet: UILabel!
    
    @IBAction func WatchlistedBtn(_ sender: UIButton) {
        
        if  self.userID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Login and Try again later")
        }
        else
        {
            self.removeFromWatchList()
            
            let alert = UIAlertController(title: "Removed", message: "Succcessfully Removed from Watchlist", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
            
            self.WatchlistedBtnOutlet.isHidden = true
            
            self.WatchlistBtnOutlet.isHidden = false
            
            self.WatchlistStatus = false
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateWatchlist"), object: nil)
        }
        
    }
    @IBAction func WatchlistBtn(_ sender: UIButton) {
        
        if userID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Login and Try again later")
        }
        else
        {
            self.addToWatchList()
            
            let alert = UIAlertController(title: "Added", message: "Succcessfully Added to Watchlist", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
            
            self.WatchlistedBtnOutlet.isHidden = false
            
            self.WatchlistBtnOutlet.isHidden = true
            
            self.WatchlistStatus = true
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateWatchlist"), object: nil)
        }
        
    }
    
    @IBOutlet weak var FirstProgressView: UIProgressView!
    
    @IBOutlet weak var FifthProgressView: UIProgressView!
    
    @IBOutlet weak var SecondProgressView: UIProgressView!
    
    @IBAction func SendMessageBtn(_ sender: UIButton) {
        
        if userID == nil
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Login and try again later")
        }
        else
        {
            self.MessageView.isHidden = false
            
            self.setMessageDetails()
            
            self.view.bringSubviewToFront(self.MessageView)
        }
    }
    
    func setMessageDetails()
    {
        self.MessageBox.MessageTextView.text = ""
        
        self.MessageBox.SendBtnOutlet.addTarget(self, action: #selector(self.MessageSend(sender:)), for: .touchUpInside)
        
        self.MessageBox.CancelBtnOutlet.addTarget(self, action: #selector(self.hideMessageBox(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //Hide MessageView Selector method
    
    @objc func hideMessageBox(sender : UIButton)
    {
        self.MessageView.isHidden = true
    }
    
    @IBAction func ViewAccountBtn(_ sender: UIButton) {
        
        if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Business") ||  self.langID!.elementsEqual("AR")
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            
            nav.sellerID = "\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)"
            
            self.navigationController?.pushViewController(nav, animated: true)
            
        }
        else if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Private") ||  self.langID!.elementsEqual("AR")
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            
            nav.sellerID = "\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)"
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            
        }
    }
    @IBOutlet weak var ForthProgressView: UIProgressView!
    
    @IBOutlet weak var ThirdProgressView: UIProgressView!
    
    var HeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var NoReviewsLabel: UILabel!
    
    @IBOutlet weak var BuyNowView: UIView!
    
    @IBOutlet weak var BidStackView: UIStackView!
    
    @IBOutlet weak var BidView: UIView!
    
    @IBOutlet weak var CurrentBidLabel: UILabel!
    
    @IBOutlet weak var CurrentBidOutlet: UILabel!
    
    @IBOutlet weak var TotalBidOutlet: UILabel!
    
    @IBOutlet weak var NextBidTextField: UITextField!
    
    @IBOutlet weak var PlaceBidOutlet: UIButton!
    
    @IBAction func PlaceBidBtn(_ sender: UIButton) {
        
        if CheckUncheckBtnOutlet.currentImage == UIImage(named: "checkBoxEmpty")
        {
            ShowAlertMessage(title: "Warning", message: "Please Check Terms & Conditions")
        }
        else
        {
            if NextBidTextField.text?.isEmpty == true
            {
                ShowAlertMessage(title: "Warning", message: "Please Enter Bidding amount")
            }
            else
            {
                self.PlaceBid()
            }
        }
    }
    @IBOutlet weak var CheckUncheckBtnOutlet: UIButton!
    
    @IBOutlet weak var TermsAndConditionBtnOutlet: UIButton!
    
    @IBAction func TermsAndConditionBtn(_ sender: UIButton) {
        
        self.SetWebView()
        
        self.BlurWebView.isHidden = false
    }
    @IBAction func HideBidBtn(_ sender: UIButton) {
        
        if self.BidTableview.isHidden == false
        {
            self.BidTableview.isHidden = true
            
            self.BidTableviewHeightConstraint.constant = 0
            
            self.HideBtnOutlet.setTitle("Show Bid History", for: .normal)
        }
        else
        {
            self.BidTableview.isHidden = false
            
            self.setTableviewHeight(tableview: self.BidTableview, heightConstraint: self.BidTableviewHeightConstraint)
            
            self.HideBtnOutlet.setTitle("Hide Bid History", for: .normal)
        }
        
    }
    @IBOutlet weak var HideBtnOutlet: UIButton!
    
    @IBOutlet weak var BidTableview: UITableView!
    
    @IBOutlet weak var ImageBlurView: UIView!
    
    @IBOutlet weak var DetailImageView: UIImageView!
    
    @IBAction func ImageCrossBtn(_ sender: UIButton) {
        
        self.ImageBlurView.isHidden = true
    }
    @IBOutlet weak var ReportBlurView: UIView!
    
    
  
   
    @IBOutlet weak var ProductTypeOutlet: UILabel!
    
    @IBOutlet weak var ProductPriceOutlet: UILabel!
    
    @IBOutlet weak var BlurWebView: UIView!
    
    @IBOutlet weak var WebView: UIWebView!
    
    @IBAction func CrossWebView(_ sender: UIButton) {
        self.BlurWebView.isHidden = true
    }
    
    @IBOutlet weak var AddRemoveCartWallet: UIButton!
    @IBAction func CheckUncheckBtn(_ sender: UIButton) {
        
        if CheckUncheckBtnOutlet.currentImage == UIImage(named: "checkBoxEmpty")
        {
            self.CheckUncheckBtnOutlet.setImage(UIImage(named: "checkBoxFilled"), for: .normal)
        }
        else
        {
            self.CheckUncheckBtnOutlet.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
        }
    }
    
   
    
   
    @IBAction func FollowBtn(_ sender: UIButton) {
        
        followunfollowButton = true
        self.AccountFavourite()
    }
    
    
    @IBOutlet weak var FollowBtnOutlet: UIButton!
    
    @IBAction func UnfollowBtn(_ sender: Any)
    {
         followunfollowButton = false
        self.AccountremoveFavourite()
    }
    
    @IBOutlet weak var unfollowbtnoutlet: UIButton!
    
    
    
    @IBOutlet weak var UnfollowBtnOutlet: UIButton!
    
    @IBOutlet weak var TermsConditionHeading: UILabel!
    @IBOutlet weak var PostedByLocationStackView: UIStackView!
    
    
    
    var DescHeightCheck : Bool! = false
    
    var ProductID : String! = ""
    
    var StartValue : Int! = 0
    
    var PerLoad : String! = "10"
    
    var height : CGFloat = 0
    
    var ProductInfo : NSDictionary!
    
    var ProductOptionArray : NSMutableArray! = NSMutableArray()
    
    var QuestionAnswerArray : NSMutableArray! = NSMutableArray()
    
    var SellerOtherListingArray : NSMutableArray! = NSMutableArray()
    
    var ProductImageArray : NSMutableArray! = NSMutableArray()
    
    var ReviewArray : NSMutableArray! = NSMutableArray()
    
    var ReviewPercent : NSMutableArray! = NSMutableArray()
    
    var SellerDetails : NSDictionary!


    //Static Labels Outlets
    @IBOutlet weak var QuantityAvailableLbl: UILabel!
    
    @IBOutlet weak var NextBidAmountLbl: UILabel!
    
    @IBOutlet weak var DetailsLbl: UILabel!
    
    @IBOutlet weak var BrowseCategoryLbl: UILabel!
    
    @IBOutlet weak var QuestionAndAnswersLbl: UILabel!
    
    @IBOutlet weak var ReviewAndRatingLbl: UILabel!
    
    @IBOutlet weak var PostedByLbl: UILabel!
    
    @IBOutlet weak var ShippingOptionLbl: UILabel!
    
    @IBOutlet weak var LocationLbl: UILabel!
    
    @IBOutlet weak var PaymentOptionLbl: UILabel!
    
    @IBOutlet weak var SellerOtherListingLbl: UILabel!

    
    
    //MARK:- Report Class Variable Declaration
    
    @IBOutlet weak var ReportClass: Report!
    
    @IBAction func hideReportViewBtn(_ sender: UIButton) {
        
        self.ReportBlurView.isHidden = true
    }
    @IBOutlet weak var hideOutLet: UIButton!
    
    
    var customView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewaccountoutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)
        self.sendbuttonoutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
        
        self.FollowBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Follow", comment: ""), for: .normal)
        
         self.unfollowbtnoutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Unfollow", comment: ""), for: .normal)
        
        self.ShareView.isHidden = true
        self.BidStackView.isHidden  = true
        
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight))
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        
        //headerView.ShareBtnOutlet.addTarget(self, action: #selector(ShareTapped(sender:)), for: .touchUpInside)
        
        headerView.ShareBtnOutlet.addTarget(self, action: #selector(ShareDetails(sender:)), for: .touchUpInside)
        
        self.DynamicLanguage()
        
        self.SellerListingCollectionView.isHidden = true
        
        self.BiggerImageCollectionView.backgroundColor = .clear
        
        self.DescriptionString.translatesAutoresizingMaskIntoConstraints = false
        // Do any additional setup after loading the view.
        self.SetShareClass()
        
        self.setReportClass()
        
         self.ProductDetails()
        
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        
//        self.view.isHidden = true
//        
//    }
//    
    
    @objc func ShareDetails(sender : UIButton)
    {
        let text = "This is the text...."
        
        let image = UIImage(named: "Active green")
        
        let myWebsite = NSURL(string:"https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile")
        
        let shareAll = [text,image!, myWebsite] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK:- Report Send Button Outlet Connection
    @objc func SendReportAPI(sender : UIButton)
    {
        self.reportValidation()
    }
    
    //MARK:- //Set Share Class
    
    func SetShareClass()
    {
        self.SharingClass.VC = NewProductDetailsViewController()
    }
    
    //MARK:- //set Report Class
    
    func setReportClass()
    {
        self.ReportClass.showReportView = self.ReportBlurView
        
        self.ReportClass.SendBtnOutlet.addTarget(self, action: #selector(SendReportAPI(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //Objective C Method for Share Button
    
    @objc func ShareTapped(sender : UIButton)
    {
        self.ShareView.isHidden = false
    }

    //MARK:- Dynamic language
    func DynamicLanguage()
    {
        self.HideBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "hide_bid_history", comment: ""), for: .normal)
        
        self.CurrentBidLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "current_bid", comment: "")
        self.TermsAndConditionBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "terms_and_conditions", comment: ""), for: .normal)
        self.PlaceBidOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "place_bid", comment: ""), for: .normal)
    self.ShowTotalDescriptionBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "show_total_description", comment: ""), for: .normal)
//        self.ReportBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "report", comment: ""), for: .normal)
        self.ReportLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "report", comment: "")
        self.AllQNABtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_all_questions_and_answers", comment: ""), for: .normal)
        
self.AllReviewRatingBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAllReview", comment: ""), for: .normal)
        
       self.NoQNALbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "there_are_no_question_answer", comment: "")
        
       self.NoReviewsLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "noReviewFound", comment: "")
        
       self.NoDescLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_description_found", comment: "")
        
//       self.ReportBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "report", comment: ""), for: .normal)
       self.BuyNowOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "buy_now", comment: ""), for: .normal)
        self.AddRemoveCartWallet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_to_cart", comment: ""), for: .normal)
        
        self.TotalDescription.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
        
        self.SellerOtherListingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sellerOtherListing", comment: "")
        
        self.PaymentOptionLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Posted By", comment: "")
        
        self.LocationLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")
        
        
        
        self.ShippingOptionLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingOption", comment: "")
        
        self.PostedByLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")
        
        self.ReviewAndRatingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviewAndRating", comment: "")
        
        self.QuestionAndAnswersLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questionsAndAnswers", comment: "")
        
        self.BrowseCategoryLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "browseCategory", comment: "")
        
        self.DetailsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
        
        self.NextBidAmountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "next_bid_amount", comment: "")
        
        self.QuantityAvailableLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "quantity_available", comment: "")
        
        self.NoProductFoundLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "noProductFound", comment: "")
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.globalDispatchgroup.notify(queue: .main, execute: {
            
            self.ProductFeaturesTableview.estimatedRowHeight = 300
            
            self.ProductFeaturesTableview.rowHeight = UITableView.automaticDimension
            
            self.setTableviewHeight(tableview: self.ProductFeaturesTableview, heightConstraint: self.ProductFeaturesTableViewHeightConstraint)
            
            self.QNATableview.estimatedRowHeight = 300
            
            self.QNATableview.rowHeight = UITableView.automaticDimension
            
            self.setTableviewHeight(tableview: self.QNATableview, heightConstraint: self.QnaTableviewHeightConstraint)
            
            self.ReviewsTableView.estimatedRowHeight = 300
            
            self.ReviewsTableView.rowHeight = UITableView.automaticDimension
            
            self.setTableviewHeight(tableview: self.ReviewsTableView, heightConstraint: self.ReviewsTableHeightConstraint)
            
            self.setTableviewHeight(tableview: self.BidTableview, heightConstraint: self.BidTableviewHeightConstraint)
            
            self.BidTableview.estimatedRowHeight = 300
            
            self.BidTableview.rowHeight = UITableView.automaticDimension
        })
        //self.SetTxtView()
    }
    
   
       
    
    //MARK:- //Selector Method for Sending Message
    
    @objc func MessageSend(sender : UIButton)
    {
         let userID = UserDefaults.standard.string(forKey: "userID")
        
        if self.MessageBox.MessageTextView.text.elementsEqual("")
        {
            self.ShowAlertMessage(title: "Alert", message: "Message can not be empty")
        }
          
        else  if userID!.elementsEqual("\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)")
        {
            self.ShowAlertMessage(title: "Warning", message: "You can not send Message to Your own.")
            
        }
       
        else
        {
            SendMessage()
        }
    }
    
    // MARK: - // JSON POST Method to get Product Details
    
    func ProductDetails()
    {
        globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_product_details")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if self.langID!.elementsEqual("AR")
        {
            
            self.AccountStatusImage.image = UIImage(named: "button-2")
            
            
        }
            
        else
        {
            self.AccountStatusImage.image = UIImage(named: "button-1")
        }
        
        parameters = "product_id=\(ProductID!)&user_id=\(userID!)&lang_id=\(langID!)&start_value=\(StartValue!)&per_load=\(PerLoad!)"
        
        
        print("Product Details URL : ", url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    if (json["response"] as! Bool) == true
                    {
                        print("Json",json)
                        self.ProductInfo = json["product_info"] as? NSDictionary
                        
                        self.SharingClass.ProductLink = "\(self.ProductInfo["web_url"]!)"
                        
                        self.ProductOptionArray = self.ProductInfo["product_option"] as? NSMutableArray
                        
                        self.QuestionAnswerArray = self.ProductInfo["question_answer"] as? NSMutableArray
                        
                        self.ProductImageArray = self.ProductInfo["product_image"] as? NSMutableArray
                        
                        self.SellerOtherListingArray = self.ProductInfo["selelr_others_listing"] as? NSMutableArray
                         print("SellerOtherListingArray----------",self.SellerOtherListingArray)
                        
                        
                        self.ReviewArray = ((self.ProductInfo["review_rating"] as! NSDictionary)["review_list"] as! NSMutableArray)
                        
                        self.ReviewPercent = ((self.ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSMutableArray)
                        
                        self.SellerDetails = self.ProductInfo["seller_details"] as? NSDictionary
                        print("sellerdetials----------",self.SellerDetails)
                        
                       
                        
                        DispatchQueue.main.async {
                            
                            
                            self.ImageCollectionView.delegate = self
                            
                            self.ImageCollectionView.dataSource = self
                            
                            self.ImageCollectionView.reloadData()
                            
                            self.PageControl()
                            
                            self.FetchData()
                            
                            self.SetReviewStar()
                            
                            self.SetReviewsSection()
                            //                            self.FetchWebViewData()
                            
                            self.ProductFeaturesTableview.delegate = self
                            
                            self.ProductFeaturesTableview.dataSource = self
                            
                            self.ProductFeaturesTableview.reloadData()
                            
                            self.globalDispatchgroup.leave()
                            
                            self.SetSellerListingView()
                            
                            self.SetDescription()
                            
                            self.SetQuestionAnswer()
                            
                            self.SetBidSection()
                            
                            self.SetwatchlistChecking()
                            
                            self.SetDailyDealsChecking()
                            
                            self.AccountImageCheck()
                            
                            self.SellerListingCollectionView.delegate = self
                            
                            self.SellerListingCollectionView.dataSource = self
                            
                            self.SellerListingCollectionView.reloadData()
                            
                            self.QNATableview.delegate = self
                            
                            self.QNATableview.dataSource = self
                            
                            self.QNATableview.reloadData()
                            
                            self.ReviewsTableView.delegate = self
                            
                            self.ReviewsTableView.dataSource = self
                            
                            self.ReviewsTableView.reloadData()
                            
                            
                            self.OtherSellerListingCollectionView.delegate = self
                            
                            self.OtherSellerListingCollectionView.dataSource = self
                            
                            self.OtherSellerListingCollectionView.reloadData()
                            
                            self.BiggerImageCollectionView.delegate = self
                            
                            self.BiggerImageCollectionView.dataSource = self
                            
                            self.BiggerImageCollectionView.reloadData()
                            
                            if "\(self.ProductInfo["product_type"]!)".elementsEqual("Auction")
                            {
                                self.BidTableview.delegate = self
                                
                                self.BidTableview.dataSource = self
                                
                                self.BidTableview.reloadData()
                            }
                            else
                            {
                                //Do Nothing
                            }
                            
                            if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_type_status"]!)".elementsEqual("B")
                            
                            {
                            self.AccountTypeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_account", comment: "")
                            }
                            else
                            {
                              self.AccountTypeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "private_account", comment: "")
                            }
                            
                            
                            self.customView.removeFromSuperview()
                            SVProgressHUD.dismiss()
                            
                            self.view.isHidden = false
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.customView.removeFromSuperview()
                            SVProgressHUD.dismiss()
                            
                            self.globalDispatchgroup.leave()
                            
                            
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    //MARK:- //CollectionView Delegate and DataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == ImageCollectionView
        {
            return self.ProductImageArray.count
        }
        else if collectionView == self.SellerListingCollectionView
        {
            return self.SellerOtherListingArray.count
        }
        else if collectionView == OtherSellerListingCollectionView
        {
            return self.SellerOtherListingArray.count
        }
        else if collectionView == BiggerImageCollectionView
        {
            return self.ProductImageArray.count
        }
        else
        {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ImageCollectionView
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! ImageListingCollectionViewCell
            
            let path = "\(self.ProductImageArray[indexPath.row])"
            
            obj.imageView.sd_setImage(with: URL(string: path))
            
            obj.backgroundColor = .white
            
            obj.imageView.contentMode = .redraw
            
            return obj
        }
        else if collectionView == self.SellerListingCollectionView
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "sellerlisting", for: indexPath) as! SellerListingCollectionViewCell
            
            let path = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["photo"]!)"
            
            obj.SellerListingImageView.sd_setImage(with: URL(string: path))
            
            obj.SellerListingProductName.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            
            return obj
        }
        else if collectionView == self.OtherSellerListingCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "marketplacelisting", for: indexPath) as! MarketPlaceListingCollectionViewCell
            
            let path = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["photo"]!)"
            
            cell.Product_image.sd_setImage(with: URL(string: path))
            
            cell.Product_name.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            
            cell.Listing_time.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["listing_time"]!)"
            
            cell.Expire_time.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["expiry_time"]!)"
            
            cell.ProductSellerName.text = "Business:" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["seller_name"]!)"
            
            cell.Product_address.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["address"]!)"
            
            cell.Product_type.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_type"]!)"
            
            
            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["seller_name"]!)".elementsEqual("Admin")
            {
                cell.Product_StatusView.isHidden = false
                
                cell.Product_SellerView.isHidden = true
            }
            else
            {
                cell.Product_StatusView.isHidden = true
                
                cell.Product_SellerView.isHidden = false
                
            }
            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["seller_name"]!)".elementsEqual("Admin")
            {
                cell.Verified_image.isHidden = false
            }
            else
            {
                cell.Verified_image.isHidden = true
            }
            
            cell.watchlistBtnOutlet.tag = indexPath.row
            
            cell.WatchlistedBtnOutlet.tag = indexPath.row
            
            //            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
            //            {
            //                cell.watchlistBtnOutlet.isHidden = true
            //                cell.WatchlistedBtnOutlet.isHidden = false
            ////                cell.WatchlistedBtnOutlet.addTarget(self, action: #selector(Removefromwatchlist(sender:)), for: .touchUpInside)
            //            }
            //            else if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
            //            {
            //                cell.watchlistBtnOutlet.isHidden = false
            //                cell.WatchlistedBtnOutlet.isHidden = true
            ////                cell.watchlistBtnOutlet.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
            //            }
            //            else
            //            {
            //                cell.watchlistBtnOutlet.isHidden = false
            //                cell.WatchlistedBtnOutlet.isHidden = true
            //            }
            //
            
            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["seller_id"]!)".elementsEqual("0")
            {
                print("show dailydealsicon ")
                
                cell.Product_StatusView.isHidden = false
                 cell.Product_SellerView.isHidden = true
            }
            else
            {
                cell.Product_SellerView.isHidden = false
                 cell.Product_StatusView.isHidden = true
            }
            
            
            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("1") && "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_type"]!)".elementsEqual("Buy Now")
            {
                cell.Product_Price.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
                
                cell.Product_Price.textColor = UIColor.red
                
                let txt = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                
                cell.Product_discounted_price.attributedText = txt.strikeThrough()
                
                cell.Product_discounted_price.textColor = UIColor.seaGreen()
                
            }
            else if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("0") && "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_type"]!)".elementsEqual("Buy Now")
            {
                cell.Product_Price.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                
                cell.Product_Price.textColor = UIColor.seaGreen()
                
                cell.Product_discounted_price.text = ""
            }
            else if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_type"]!)".elementsEqual("Asking Price")
            {
                cell.Product_Price.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                
                cell.Product_Price.textColor = UIColor.seaGreen()
                
                cell.Product_discounted_price.text = ""
                
            }
            else
            {
                cell.Product_Price.text = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                
                cell.Product_Price.textColor = UIColor.seaGreen()
                
                cell.Product_discounted_price.text = ""
            }
            
            //            if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
            //            {
            //                cell.ProductConditionImage.image = UIImage(named: "Used")
            //            }
            //            else if "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
            //            {
            //                cell.ProductConditionImage.image = UIImage(named: "New")
            //            }
            //            else
            //            {
            //                cell.ProductConditionImage.image = UIImage(named: "")
            //            }
            
            cell.Product_name.textColor = UIColor.brown1()
            
            cell.Listing_time.textColor = UIColor.seaGreen()
            
            cell.Expire_time.textColor = UIColor.red
            
            cell.ProductSellerName.textColor = UIColor.brown1()
            
            cell.Product_address.textColor = UIColor.brown1()
            
            return cell
        }
        else if collectionView == BiggerImageCollectionView
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "bigger", for: indexPath) as! BiggerImageCollectionViewCell
            
            let path = "\(self.ProductImageArray[indexPath.row])"
            
            obj.BiggerImage.sd_setImage(with: URL(string: path))
            
            return obj
        }
        else
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) 
            return obj
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ImageCollectionView
        {
            return CGSize(width: UIScreen.main.bounds.width, height: 179)
        }
        else if collectionView == OtherSellerListingCollectionView
        {
            let padding : CGFloat! = 4
            
            let collectionViewSize = self.OtherSellerListingCollectionView.frame.size.width - padding
            
            return CGSize(width: collectionViewSize/2, height: 273)
        }
        else if collectionView == BiggerImageCollectionView
        {
            let collectionviewsize = self.BiggerImageCollectionView.frame.size.width
            
            return CGSize(width: collectionviewsize, height: 542)
        }
        else
        {
            let padding : CGFloat! = 4
            
            let collectionViewSize = self.SellerListingCollectionView.frame.size.width - padding
            
            return CGSize(width: collectionViewSize/2, height: 128)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == BiggerImageCollectionView
        {
          return 0
        }
        else
        {
          return 2
        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ImageCollectionView
        {
            self.BiggerImageView.isHidden = false
            
            self.view.bringSubviewToFront(self.BiggerImageView)
            
            let toIndex : IndexPath = IndexPath(row: indexPath.item, section: 0)
            
            self.BiggerImageCollectionView.scrollToItem(at: toIndex, at: .left, animated: true)
            
        }
        else if collectionView == BiggerImageCollectionView
        {
            print("Do nothing")
        }
        else if collectionView == OtherSellerListingCollectionView
        {
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
            
            navigate.ProductID = "\((self.SellerOtherListingArray[indexPath.row] as! NSDictionary)["product_id"]!)"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            print("Do Nothing")
        }
    }
    //MARK:- //Tableview Delegate and Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == ProductFeaturesTableview
        {
            return self.ProductOptionArray.count
        }
        else if tableView == QNATableview
        {
            if self.QuestionAnswerArray.count >= 4
            {
                return 4
            }
            else
            {
                return self.QuestionAnswerArray.count
            }
        }
        else if tableView == ReviewsTableView
        {
            if self.ReviewArray.count >= 4
            {
                return 4
            }
            else
            {
                return self.ReviewArray.count
            }
            
        }
        else if tableView == BidTableview
        {
            return ((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSArray).count
        }
        else
        {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ProductFeaturesTableview
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "feature") as! ProductFeaturesTableViewCell
            
            obj.OptionValue.text = "\((self.ProductOptionArray[indexPath.row] as! NSDictionary)["option_label"]!)"
            
            obj.DetailValue.text = "\((self.ProductOptionArray[indexPath.row] as! NSDictionary)["option_value"]!)"
            
            //Font
            
            obj.OptionValue.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.DetailValue.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            //
            
            obj.selectionStyle = .none
            
            return obj
        }
        else if tableView == QNATableview
        {
            let nav = tableView.dequeueReusableCell(withIdentifier: "qnacell") as! QuestionandAnswerTableViewCell
            
            nav.QuestionLbl.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["question"]!)"
            
            if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["name"]!)" == "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["qusn_user"]!)"
            {
                nav.ReplyBtnOutlet.isHidden = false
            }
            else
            {
                nav.ReplyBtnOutlet.isHidden = true
            }
            
            if "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["answer"]!)".elementsEqual("")
            {
                nav.AnswerLabel.text = "Not Answered yet"
            }
            else
            {
                nav.AnswerLabel.text =  "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["answer"]!)"
            }
            nav.QuestionAskerName.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["qusn_user"]!)" + "(" + "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["total_rating"]!)" + ")"
            
            nav.QuestionAskerTime.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["question_date"]!)"
            
            nav.AnsweringTime.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["reply_date"]!)"
            
            //Font
            
            nav.QuestionLbl.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            nav.AnswerLabel.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            nav.QuestionAskerName.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            nav.QuestionAskerTime.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            nav.AnsweringTime.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            //
            
            
            nav.selectionStyle = .none
            
            return nav
        }
        else if tableView == ReviewsTableView
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "newreview") as! NewProductListingReviewTableViewCell
            
            obj.RatingNo.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["rating_no"]!)"
            
            obj.ReviewerName.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["name"]!)"
            
            obj.ReviewDate.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["date"]!)"
            
            obj.ReviewDetails.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["comment"]!)"
            
            obj.selectionStyle = .none
            
            
            //Font
            
            obj.RatingNo.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.ReviewerName.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.ReviewDate.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.ReviewDetails.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            //
            
            return obj
        }
        else if tableView == BidTableview
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "newproduct") as! NewProductListingTableviewCell
            
            obj.BiddingDate.text = "\((((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSMutableArray)[indexPath.row] as! NSDictionary)["date"]!)"
            
            obj.BidderName.text = "\((((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSMutableArray)[indexPath.row] as! NSDictionary)["name"]!)"
            
            obj.BiddingAmount.text = "\((((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSMutableArray)[indexPath.row] as! NSDictionary)["price"]!)"
            
            obj.BiddingTime.text = "\((((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSMutableArray)[indexPath.row] as! NSDictionary)["time"]!)"
            
            obj.selectionStyle = .none
            
            //Font
            
            obj.BiddingDate.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.BiddingTime.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.BiddingAmount.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            obj.BidderName.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
            
            //
            
            return obj
        }
        else
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
            
            return obj
        }
    }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return UITableView.automaticDimension
        }
    //MARK:- //Page Control
    func PageControl()
    {
        if self.ProductImageArray.count <= 1
        {
            self.ImagePageControl.isHidden = false
            
            self.ImagePageControl.numberOfPages = self.ProductImageArray.count
        }
        else
        {
            self.ImagePageControl.isHidden = false
            
            self.ImagePageControl.numberOfPages = self.ProductImageArray.count
        }
    }
    func AccountImageCheck()
    {
        self.FollowBtnOutlet.backgroundColor = .LeafGreen()

        if langID.elementsEqual("AR")
        {
            if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Business")
            {
                self.AccountStatusImage.image = UIImage(named: "button-2")
                
                self.AccountTypeLbl.text = "Business Account"
                
                //         self.VerifiedImagesView.isHidden = false
            }
            else if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Private")
            {
                self.AccountStatusImage.image = UIImage(named: "button-2")
                
                self.AccountTypeLbl.text = "Private Account"

                //         self.VerifiedImagesView.isHidden = true
            }
            else
            {
                self.AccountStatusImage.image = UIImage(named: "button-2")
                //        self.VerifiedImagesView.isHidden = false
            }
        }
        else
        {
            if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Business")
            {
                self.AccountStatusImage.image = UIImage(named: "button-1")
                
                self.AccountTypeLbl.text = "Business Account"
                //         self.VerifiedImagesView.isHidden = false
            }
            else if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["User_type"]!)".elementsEqual("Private")
            {
                self.AccountStatusImage.image = UIImage(named: "button-1")
                
                self.AccountTypeLbl.text = "Private Account"

                //         self.VerifiedImagesView.isHidden = true
            }
            else
            {
                self.AccountStatusImage.image = UIImage(named: "button-1")
                //        self.VerifiedImagesView.isHidden = false
            }
        }

    }
    //MARK:- Fetch Static Data
    func FetchData()
    {
        self.ProductName.text = "\(self.ProductInfo["product_name"]!)"
        
        if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["location"]!)".elementsEqual("")
        {
               self.ProductAddress.text = "null"
        }
        else
        {
                self.ProductAddress.text = "\((self.ProductInfo["seller_details"]! as! NSDictionary)["location"]!)"
        }
        
        let BreadCrumpString = "\(self.ProductInfo["breadcrumb_bar"]!)"

        let BRStr = BreadCrumpString.replacingOccurrences(of: ",", with: "/", options: .literal, range: nil) + "/"
        
        let BreadCrumbArray = BRStr.components(separatedBy: "/")
        print("Array",BreadCrumbArray)
        

        let reversedNames : [String] = Array(BreadCrumbArray.reversed())

        print("ReversedArray",reversedNames)

        if langID.elementsEqual("AR")
        {
           let str = reversedNames.joined(separator: "/")
            print("String",str)
            
           self.BreadCrump.halfTextColorChange(fullText: ( "\(self.ProductInfo["product_name"]!)" + str), changeText: "\(self.ProductInfo["product_name"]!)", color: .black())
            
            self.BreadCrumpBtnOutlet.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
        }
        else
        {
          self.BreadCrump.halfTextColorChange(fullText: (BRStr + "\(self.ProductInfo["product_name"]!)" ), changeText: "\(self.ProductInfo["product_name"]!)", color: .black())
        }






        
//        self.BreadCrump.halfTextColorChange(fullText: (BRStr + "\(self.ProductInfo["product_name"]!)" ), changeText: "\(self.ProductInfo["product_name"]!)", color: .black())


         print("ProductName==","\(self.ProductInfo["product_name"]!)")
        if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["location"]!)".elementsEqual("")
        {
            self.LocationName.text = "null"
        }
        else
        {
            self.LocationName.text = "\((self.ProductInfo["seller_details"]! as! NSDictionary)["location"]!)"
        }
        self.SellerName.text = "\(self.SellerDetails["name"]!)"
        
         if "\(self.SellerDetails["favourite_status"]!)".elementsEqual("0")
         {
            self.FollowBtnOutlet.isHidden = false
            self.unfollowbtnoutlet.isHidden = true
        }
        else
         {
            self.FollowBtnOutlet.isHidden = true
            self.unfollowbtnoutlet.isHidden = false
        }
        
        let path = "\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_image"]!)"
        
        self.ProductSellerImage.sd_setImage(with: URL(string: path))
        
        self.MemberOutlet.text = "Member Since :" + " " + "\((self.ProductInfo["seller_details"]! as! NSDictionary)["member_since"]!)"
        
        self.ContactOutlet.text = "Contact:" + " " + "\((self.ProductInfo["seller_details"]! as! NSDictionary)["mobile_no"]!)"
        
        self.LocationOutlet.text = "Location :" + " " + "\((self.ProductInfo["seller_details"]! as! NSDictionary)["location"]!)"
        
        self.ListedTime.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed", comment: "") + " : " + "\(self.ProductInfo["listed_time"]!)" + " ago"

        self.ClosingTime.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Closing", comment: "") + " : " + "\(self.ProductInfo["expire_time"]!)"

        self.ViewsOutlet.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "views", comment: "") + ":" + "\(self.ProductInfo["count_views"]!)"

        self.WatchlistOutlet.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: "") + " :" + "\(self.ProductInfo["count_watchlist"]!)"

        self.QuantityOutlet.text = "\(self.ProductInfo["quantity"]!)"
        
        if "\((self.ProductInfo["shipping_option"]! as! NSDictionary)["ship_content"]!)".elementsEqual("")
        {
            self.ShippingCompanyName.text = "Store does not allow pick-ups"
        }
        else
        {
            self.ShippingCompanyName.text  = "\((self.ProductInfo["shipping_option"]! as! NSDictionary)["ship_content"]!)"
        }
        
        
        
        self.ProductTypeOutlet.text = "\(self.ProductInfo["product_type"]!)"
        
//        if "\(self.ProductInfo["reserve_price"] ?? "")".elementsEqual("")
//        {
           self.ProductPriceOutlet.text = "\(self.ProductInfo["currency_symbol"]!)" + "\(self.ProductInfo["start_price"]!)"
//
            self.disCountedPrice.isHidden = true
//        }
//        else
//        {
//            self.ProductPriceOutlet.text = "\(self.ProductInfo["currency_symbol"]!)" + "\(self.ProductInfo["reserve_price"]!)"
//
//            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(self.ProductInfo["currency_symbol"]!)" + "\(self.ProductInfo["start_price"]!)")
//
//            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
//
//            self.disCountedPrice.attributedText = attributeString
//        }
        
    }
    func SetDescription()
    {
        if "\(self.ProductInfo["product_description"]!)".elementsEqual("")
        {
            self.NoDescriptionLabel.isHidden = false
            
            self.DescriptionString.isHidden = true
        }
        else
        {
            self.NoDescriptionLabel.isHidden = true
            
            self.DescriptionString.isHidden = false
            
            self.DescriptionString.text = "\(self.ProductInfo["product_description"]!)".trimmingCharacters(in: .whitespacesAndNewlines)
            
            let descriptionHeight = "\(self.ProductInfo["product_description"] ?? "")".heightWithConstrainedWidth(width: self.FullWidth - 16, font: UIFont(name: self.DescriptionString.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))!)
            
            print("Desc Height",descriptionHeight)
            
            
            //        if descriptionHeight >= 40
            //        {
            //           self.DescriptionString.translatesAutoresizingMaskIntoConstraints = false
            //          self.HeightConstraint =  self.DescriptionString.heightAnchor.constraint(equalToConstant: 50)
            //           self.DescriptionString.addConstraint(self.HeightConstraint)
            //           self.ShowTotalDescriptionBtnOutlet.isHidden = false
            //
            //        }
            //        else
            //        {
            //            self.DescriptionString.translatesAutoresizingMaskIntoConstraints = false
            //            self.DescHeightCheck = false
            //            self.HeightConstraint = self.DescriptionString.heightAnchor.constraint(equalToConstant: descriptionHeight)
            //            self.DescriptionString.addConstraint(self.HeightConstraint)
            //            self.ShowTotalDescriptionBtnOutlet.isHidden = true
            //        }
            
            
        }
    }
    
    func CheckDescriptionString()
    {
        var LineCount: Int = 0
        let TextSize = CGSize(width: self.DescriptionString.frame.size.width, height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.DescriptionString.sizeThatFits(TextSize).height))
        let charSize: Int = lroundf(Float(self.DescriptionString.font.lineHeight))
        LineCount = rHeight / charSize
        print(String(format: "No of lines: %i", LineCount))
    }
    func SetSellerListingView()
    {
        if self.SellerOtherListingArray.count == 0
        {
            self.OtherSellerListingCollectionView.isHidden = true
            
            self.NoSellerPrductView.isHidden = false
        }
        else
        {
            self.OtherSellerListingCollectionView.isHidden = false
            
            self.NoSellerPrductView.isHidden = true
        }
    }
    func SetQuestionAnswer()
    {
        if self.QuestionAnswerArray.count == 0
        {
            self.QNATableview.isHidden = true
            
            self.NoQNALabel.isHidden = false
            
            self.AllQnaView.isHidden = true
            
        }
        else if self.QuestionAnswerArray.count < 4
        {
            self.QNATableview.isHidden = false
            
            self.NoQNALabel.isHidden = true
            
            self.AllQnaView.isHidden = true
        }
        else
        {
            self.QNATableview.isHidden = false
            
            self.NoQNALabel.isHidden = true
            
            self.AllQnaView.isHidden = false
        }
    }
    func SetReviewsSection()
    {
        if self.ReviewArray.count == 0
        {
            self.ReviewsTableView.isHidden = true
            
            self.NoReviewsLabel.isHidden = false
            
            self.AllReviewsBtnView.isHidden = true
            
        }
        else if self.ReviewArray.count < 4
        {
            self.ReviewsTableView.isHidden = false
            
            self.NoReviewsLabel.isHidden = true
            
            self.AllReviewsBtnView.isHidden = true
        }
        else
        {
            self.ReviewsTableView.isHidden = false
            
            self.NoReviewsLabel.isHidden = true
            
            self.AllReviewsBtnView.isHidden = false
        }
    }
    func SetBidSection()
    {
        if "\(self.ProductInfo["product_type"]!)".elementsEqual("Buy Now")
        {
            self.BuyNowView.isHidden = false
            
            self.BidStackView.isHidden = true
        }
        else if "\(self.ProductInfo["product_type"]!)".elementsEqual("Auction")
        {
            self.BuyNowView.isHidden = true
            
            self.BidStackView.isHidden = false
            
            self.SetBidTableviewSection()
        }
        else if "\(self.ProductInfo["product_type"]!)".elementsEqual("Asking Price")
        {
            self.BuyNowView.isHidden = true
            
            self.BidStackView.isHidden = true
        }
        else
        {
            self.BuyNowView.isHidden = false
            
            self.BidStackView.isHidden = true
        }
    }
    func SetDailyDealsChecking()
    {
        if "\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)".elementsEqual("0")
        {
            //           self.PostedByLocationStackView.isHidden = true
            self.PostedByTitleView.isHidden = true
            
            self.PostedByView.isHidden = true
            
            self.LocationTitleView.isHidden = true
            
            self.LocationStackView.isHidden = true
            
            self.PaymentTitleView.isHidden = false

            self.PaymentView.isHidden = false
            
           
            
        }
        else
        {
            //            self.PostedByLocationStackView.isHidden = false
            self.PostedByTitleView.isHidden = false
            
            self.PostedByView.isHidden = false
            
            self.LocationTitleView.isHidden = false
            
            self.LocationStackView.isHidden = false
            
             self.paymenttitleviewheightconstraints.constant = 0
            
            self.paymentviewheightcons.constant = 0
            
            self.PaymentTitleView.isHidden = true
            
            self.PaymentView.isHidden = true
        }
    }
    func SetReviewStar()
    {
        if ReviewPercent.count>0
        {
            self.FiveStarReviews.text = "\((self.ReviewPercent[4] as! NSDictionary)["count"]!)"
            
            self.FourStarReviews.text = "\((self.ReviewPercent[3] as! NSDictionary)["count"]!)"
            
            self.ThreestarReviews.text =  "\((self.ReviewPercent[2] as! NSDictionary)["count"]!)"
            
            self.TwoStarReviews.text =  "\((self.ReviewPercent[1] as! NSDictionary)["count"]!)"
            
            self.FirstStarReviews.text =  "\((self.ReviewPercent[0] as! NSDictionary)["count"]!)"
            
            let progress1 : Float = Float("\((((ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSArray)[0] as! NSDictionary)["rating"] ?? "")")!
            
            FirstProgressView.setProgress(progress1/100.0, animated: false)
            
            let progress2 : Float = Float("\((((ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSArray)[1] as! NSDictionary)["rating"] ?? "")")!
            
            SecondProgressView.setProgress(progress2/100.0, animated: false)
            
            let progress3 : Float = Float("\((((ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSArray)[2] as! NSDictionary)["rating"] ?? "")")!
            
            ThirdProgressView.setProgress(progress3/100.0, animated: false)
            
            let progress4 : Float = Float("\((((ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSArray)[3] as! NSDictionary)["rating"] ?? "")")!
            
            ForthProgressView.setProgress(progress4/100.0, animated: false)
            
            let progress5 : Float = Float("\((((ProductInfo["review_rating"] as! NSDictionary)["review_percent"] as! NSArray)[4] as! NSDictionary)["rating"] ?? "")")!
            
            FifthProgressView.setProgress(progress5/100.0, animated: false)
        }
        else
        {
            self.FiveStarReviews.text = "0"
            
            self.FourStarReviews.text = "0"
            
            self.ThreestarReviews.text = "0"
            
            self.TwoStarReviews.text =  "0"
            
            self.FirstStarReviews.text = "0"
            
            FirstProgressView.setProgress(0/100.0, animated: false)
            
            SecondProgressView.setProgress(0/100.0, animated: false)
            
            ThirdProgressView.setProgress(0/100.0, animated: false)
            
            ForthProgressView.setProgress(0/100.0, animated: false)
            
            FifthProgressView.setProgress(0/100.0, animated: false)
        }
    }
    
    //MARK:- //Watchlist Button Checking
    func SetwatchlistChecking()
    {
        if "\(self.ProductInfo["watchlist_status"]!)".elementsEqual("0")
        {
            self.WatchlistBtnOutlet.isHidden = false
            
            self.WatchlistedBtnOutlet.isHidden = true
        }
        else if "\(self.ProductInfo["watchlist_status"]!)".elementsEqual("1")
        {
            self.WatchlistBtnOutlet.isHidden = true
            
            self.WatchlistedBtnOutlet.isHidden = false
        }
        else
        {
            self.WatchlistBtnOutlet.isHidden = false
            
            self.WatchlistedBtnOutlet.isHidden = true
        }
    }
    //MARK:- //SetBid Section Checking
    
    
    func SetBidTableviewSection()
    {
//        if "\((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_count_status"]!)".elementsEqual("")
//        {
//            self.CurrentBidOutlet.text = "0"
//        }
//        else
//        {
//            self.CurrentBidOutlet.text = "\((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_count_status"]!)"
//        }
        
        self.CurrentBidOutlet.text = "\(self.ProductInfo["bid_price"] ?? "")"
        
        if "\((self.ProductInfo["bid_content"]! as! NSDictionary)["count_bid"]!)".elementsEqual("")
        {
            self.TotalBidOutlet.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_bids", comment: "") + " :" + "0"
        }
        else
        {
            self.TotalBidOutlet.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_bids", comment: "") + " :" + "\((self.ProductInfo["bid_content"]! as! NSDictionary)["count_bid"]!)"
        }
        if ((self.ProductInfo["bid_content"]! as! NSDictionary)["bid_history"]! as! NSArray).count == 0
        {
            self.BidTableview.isHidden = true
            //           self.BidTableviewHeightConstraint.constant = 0
        }
        else
        {
            self.BidTableview.isHidden = false
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setTableviewHeight(tableview: UITableView, heightConstraint: NSLayoutConstraint)
    {
        var setheight: CGFloat  = 0
        tableview.frame.size.height = 3000
        
        for cell in tableview.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
        print("height",setheight)
    }
    func CalCulateLines(yourLabel : UILabel!)
    {
        var lineCount: Int = 0
        let textSize = CGSize(width: yourLabel.frame.size.width, height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(yourLabel.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(yourLabel.font.lineHeight))
        lineCount = rHeight / charSize
        print(String(format: "No of lines: %i", lineCount))
    }
    //MARK:-//Place Bid Functionality
    func PlaceBid()
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_add_to_bid")!   //change the url
        
        print("Place Bid URL : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(ProductID!)&bid_price=\(NextBidTextField.text!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Place Bid Api Fire" , json)
                    
                    let Msg = "\(json["message"]!)"
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        self.customView.removeFromSuperview()
                        SVProgressHUD.dismiss()
                        
                        self.ShowAlertMessage(title: "Warning", message: Msg)
                        
                        self.NextBidTextField.text = ""
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    // MARK:- // Check Product
    
    func CheckProduct()
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show()
            
        }
        
        let url = URL(string: GLOBALAPI + "app_add_to_cart")!   //change the url
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&product_id=\(ProductID!)&Lang_id=\(langID!)&qnty=\("1")"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Add To Cart API Fire " , json)
                    
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "checkout") as! CheckOutViewController
                                
                                self.navigationController?.pushViewController(nav, animated: true)
                                
                            })
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                        
                    }
                        
                        
                    else
                    {
                        
                        if "\(json["confirmstatus"]!)".elementsEqual("1")
                        {
                            
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                            }
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                let alert = UIAlertController(title: "Confirm", message: "You have tried to add products from different seller. If you still want to proceed please delete existent cart products", preferredStyle: .alert)
                                
                                let ok = UIAlertAction(title: "OK", style: .default) {
                                    UIAlertAction in
                                    
                                    self.globalDispatchgroup.notify(queue: .main, execute: {
                                        
                                        
                                        //MARK:- Remove All Cart API Fire
                                        
                                        self.removeCarts {
                                            
                                            self.Checkcart()
                                            
                                        }
                                        
                                    })
                                }
                                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                
                                alert.addAction(ok)
                                alert.addAction(cancel)
                                
                                self.present(alert, animated: true, completion: nil )
                                
                            })
                        }
                        else
                        {
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    // MARK:- // Remove Carts
    
    func removeCarts(completion : @escaping () -> ())
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_remove_cart_by_userid")!   //change the url
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove All Item From Cart API " , json)
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                        SVProgressHUD.dismiss()
                        self.customView.removeFromSuperview()
                        self.globalDispatchgroup.notify(queue: .main, execute: {
                            
                            completion()
                            
                        })
                        
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    // MARK:- // Check Cart
    
    func Checkcart()
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show()
            
        }
        
        let url = URL(string: GLOBALAPI + "app_add_to_cart")!   //change the url
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&product_id=\(ProductID!)&Lang_id=\(langID!)&qnty=\("1")"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Add To Cart API Fire " , json)
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                //let nav = self.storyboard?.instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
                                let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
                                
                                nav.LanguageID = self.langID as NSString?
                                
                                nav.ProductID = self.ProductID as? NSString
                                
                                self.navigationController?.pushViewController(nav, animated: true)
                                
                            })
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                        
                    }
                        
                        
                    else
                    {
                        
                        if "\(json["confirmstatus"]!)".elementsEqual("1")
                        {
                            
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                            }
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                let alert = UIAlertController(title: "Confirm", message: "You have tried to add products from different seller. If you still want to proceed please delete existent cart products", preferredStyle: .alert)
                                
                                let ok = UIAlertAction(title: "OK", style: .default) {
                                    UIAlertAction in
                                    self.globalDispatchgroup.notify(queue: .main, execute: {
                                        
                                        self.removeCarts {
                                            
                                            self.Checkcart()
                                            
                                        }
                                        
                                    })
                                }
                                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                
                                alert.addAction(ok)
                                
                                alert.addAction(cancel)
                                
                                self.present(alert, animated: true, completion: nil )
                                
                            })
                        }
                        else
                        {
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    // MARK: - // JSON POST Method to add Data to WatchList
    
    func addToWatchList()
        
    {
        if self.userID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Alert", message: "Please log in and try again later")
        }
        else
        {
            self.globalDispatchgroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
            
            let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url
            
            print("addtoWatchList URL : ", url)
            
            var parameters : String = ""
            
            let userID = UserDefaults.standard.string(forKey: "userID")
            
            parameters = "user_id=\(userID!)&add_product_id=\(ProductID!)"
            
            print("Parameters are : " , parameters)
            
            let session = URLSession.shared
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)
                
                
            }
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        
                        print("add to watchlist Response: " , json)
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                        
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            
            task.resume()
        }
        
    }
    
    // MARK: - // JSON POST Method to remove Data from WatchList
    
    func removeFromWatchList()
        
    {
        if self.userID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Alert", message: "Please log in and try again later")
        }
        else
        {
            self.globalDispatchgroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
            
            let url = URL(string: GLOBALAPI + "app_remove_watchlist")!   //change the url
            
            print("remove from watchlist URL : ", url)
            
            var parameters : String = ""
            
            let userID = UserDefaults.standard.string(forKey: "userID")
            
            parameters = "user_id=\(userID!)&remove_product_id=\(ProductID!)"
            
            print("Parameters are : " , parameters)
            
            let session = URLSession.shared
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)
                
                
            }
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("add to watchlist Response: " , json)
                        
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                        
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            
            task.resume()
            
        }
    }
    
    //MARK:- //Show Terms and Condition In WebView
    //    func FetchWebViewData()
    //    {
    //        self.globalDispatchgroup.enter()
    //
    //        DispatchQueue.main.async {
    //            SVProgressHUD.show()
    //        }
    //
    //
    //
    //        let url = URL(string: GLOBALAPI + "app_kaafoo_terms_condition_page")!   //change the url
    //
    //        print("Place Bid URL : ", url)
    //
    //        var parameters : String = ""
    //
    //
    //        parameters = "page_name=auction_terms_condition"
    //
    //        print("Parameters are : " , parameters)
    //
    //        let session = URLSession.shared
    //
    //        var request = URLRequest(url: url)
    //        request.httpMethod = "POST" //set http method as POST
    //
    //        do {
    //            request.httpBody = parameters.data(using: String.Encoding.utf8)
    //
    //
    //        } catch let error {
    //            print(error.localizedDescription)
    //        }
    //
    //        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
    //
    //            guard error == nil else {
    //                return
    //            }
    //
    //            guard let data = data else {
    //                return
    //            }
    //
    //            do {
    //
    //                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
    //                    print("Place Bid Api Fire" , json)
    //
    //                   if "\(json["response"]! as! Bool)".elementsEqual("true")
    //                   {
    //                     let decodeString = "\((json["info_array"] as! NSDictionary)["content"]!)"
    //                    DispatchQueue.main.async {
    //                        self.TermsConditionHeading.text = "\((json["info_array"] as! NSDictionary)["title"]!)"
    //                        self.loadStr = String(htmlEncodedString: decodeString)
    //                        self.SetWebView()
    //                    }
    //                    }
    //                    else
    //                   {
    //
    //                    }
    //
    //
    //                    DispatchQueue.main.async {
    //
    //                        self.globalDispatchgroup.leave()
    //
    //                        SVProgressHUD.dismiss()
    //
    //                    }
    //
    //                }
    //
    //            } catch let error {
    //                print(error.localizedDescription)
    //            }
    //        })
    //
    //        task.resume()
    //    }
    //MARK:- // Send Message API Fire
    func SendMessage()
    {
//        self.globalDispatchgroup.enter()
//
//        DispatchQueue.main.async {
//            SVProgressHUD.show()
//        }
//
//        let url = URL(string: GLOBALAPI + "app_kaafoo_terms_condition_page")!   //change the url
//
//        print("Place Bid URL : ", url)
//
//        var parameters : String = ""
//        //        let langID = UserDefaults.standard.string(forKey: "langID")
//        //        let userID = UserDefaults.standard.string(forKey: "userID")
//
//        parameters = "user_id=\(userID!)&seller_id=\("\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)")&message=\(self.MessageBox.MessageTextView.text ?? "")"
//
//        print("Parameters are : " , parameters)
//
//        let session = URLSession.shared
//
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST" //set http method as POST
//
//        do {
//            request.httpBody = parameters.data(using: String.Encoding.utf8)
//
//
//        }
        
//        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//
//            guard error == nil else {
//                return
//            }
//
//            guard let data = data else {
//                return
//            }
//
//            do {
                
//                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
//                    print("Place Bid Api Fire" , json)
//
//                    DispatchQueue.main.async {
//
//                        self.globalDispatchgroup.leave()
//
//                        SVProgressHUD.dismiss()
//                        self.customView.removeFromSuperview()
//
//                    }
//
//                    self.globalDispatchgroup.notify(queue: .main, execute: {
//                        if "\(json["response"]! as! Bool)".elementsEqual("true")
//                        {
//                            self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
//                        }
//                        else
//                        {
//                            self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
//                        }
//
//                    })
//                }
//
//            } catch let error {
//                print(error.localizedDescription)
//            }
//        })
//
//        task.resume()
        
        
       
                         var parameters : String = ""
                                   let langID = UserDefaults.standard.string(forKey: "langID")
                                   let userID = UserDefaults.standard.string(forKey: "userID")
                   parameters = "user_id=\(userID!)&seller_id=\("\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)")&message=\(self.MessageBox.MessageTextView.text ?? "")&lang_id=\(langID!)"
                   self.CallAPI(urlString: "app_friend_send_message", param: parameters, completion: {
                       self.globalDispatchgroup.leave()
                       
                       self.globalDispatchgroup.notify(queue: .main, execute: {
                           SVProgressHUD.dismiss()
                           self.MessageView.removeFromSuperview()
                         self.ShowAlertMessage(title: "Alert", message: "Message sent Successfully")
                       })
                   })
             
        
        
    }
    //MARK:- //Webview Data Fetching
    func SetWebView()
    {
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let url = URL (string: "https://kaafoo.com/appcontrol/app_kaafoo_terms_condition_page?page_name=kaafoo_bonus"+"&"+langID!)
        
        let requestObj = URLRequest(url: url!)
        
        self.WebView.loadRequest(requestObj)
    }
    
    //MARK:- //Account add to Favourite
    func AccountFavourite()
    {
        
        
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url
        
        print("Place Bid URL : ", url)
        
        var parameters : String = ""
        //        let langID = UserDefaults.standard.string(forKey: "langID")
        //        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&buss_seller_id=\("\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)")&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Place Bid Api Fire" , json)
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                        SVProgressHUD.dismiss()
                        self.customView.removeFromSuperview()
                        
                    }
                    
                    self.globalDispatchgroup.notify(queue: .main, execute:
                        {
                            if "\(json["response"]! as! Bool)".elementsEqual("true")
                            {
                                self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                
                                self.FollowBtnOutlet.isHidden = true
                                self.unfollowbtnoutlet.isHidden = false
                                
                            }
                            else
                            {
                                self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                
//                                if(self.followunfollowButton == true)
//                                {
//                                    self.FollowBtnOutlet.isHidden = true
//                                    self.unfollowbtnoutlet.isHidden = false
//
//
//                                }
//                                else
//                                {
//                                    self.FollowBtnOutlet.isHidden = false
//                                    self.unfollowbtnoutlet.isHidden = true
//                                }
//
//                                 self.followunfollowButton = false
                            }
                    })
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    
    //MARK:- //Account remove to Favourite
       func AccountremoveFavourite()
        
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url
        
        print("Place Bid URL : ", url)
        
        var parameters : String = ""
        //        let langID = UserDefaults.standard.string(forKey: "langID")
        //        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&buss_seller_id=\("\((self.ProductInfo["seller_details"]! as! NSDictionary)["user_id"]!)")&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Place Bid Api Fire" , json)
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                        SVProgressHUD.dismiss()
                        self.customView.removeFromSuperview()
                        
                    }
                    
                    self.globalDispatchgroup.notify(queue: .main, execute:
                        {
                            if "\(json["response"]! as! Bool)".elementsEqual("true")
                            {
                                self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                self.FollowBtnOutlet.isHidden = false
                                self.unfollowbtnoutlet.isHidden = true
                            }
                            else
                            {
                                self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                
//                                if(self.followunfollowButton == false)
//                                {
//                                    self.FollowBtnOutlet.isHidden = false
//                                    self.unfollowbtnoutlet.isHidden = true
//
//
//                                }
//                                else
//                                {
//                                    self.FollowBtnOutlet.isHidden = true
//                                    self.unfollowbtnoutlet.isHidden = false
//                                }
//
//                                 self.followunfollowButton = true
                            }
                    })
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    
    //Dynamic Language Change
    func SetLanguageChange()
    {

    }
    
    //MARK:- //
    func reportValidation()
    {
        if self.ReportClass.ReportTxtView.text.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Report text can not be blank")
        }
        else
        {
            self.reportAPI()
        }
    }
    
    //MARK:- // Set Report API
    
    func reportAPI()
    {
        let params = "user_id=\(userID!)&product_id=\(ProductID!)&report_msg=\(self.ReportClass.ReportTxtView.text ?? "")&lang_id=\(langID!)"
        
        self.CallAPI(urlString: "app_report_product", param: params, completion: {
            if "\(self.globalJson["response"] as! Bool)".elementsEqual("true")
            {
                self.globalDispatchgroup.leave()
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                    
                    self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["message"] ?? "")")
                    
                    self.ReportBlurView.isHidden = true
                }
                
            }
            else
            {
                self.globalDispatchgroup.leave()
                
                DispatchQueue.main.async {
                    
                  SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                    
                  self.ReportBlurView.isHidden = true
                }

            }
        })
    }
}

//MARK:- //Number of lines in Uilabel Calculation
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
// MARK:- // Scrollview Delegates
extension NewProductDetailsViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        ImagePageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        ImagePageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
//MARK:- //Bid History Tableview Cell Class
class NewProductListingTableviewCell : UITableViewCell
{
    
    @IBOutlet weak var BiddingDate: UILabel!
    
    @IBOutlet weak var Separator: UIView!
    
    @IBOutlet weak var BiddingAmount: UILabel!
    
    @IBOutlet weak var BidderName: UILabel!
    
    @IBOutlet weak var BiddingTime: UILabel!
    
}
//MARK:- //HTML Decode For String
extension String {
    
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString.string)
    }
    
}
// MARK:- // String Extension to Decode HTML Content
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
//MARK:- //Image Listing CollectionViewCell
class ImageListingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
//MARK:- //Product Features TableView Cell Class
class ProductFeaturesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var OptionValue: UILabel!
    
    @IBOutlet weak var DetailValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
//MARK:- Question and Answer Tableviewcell
class QuestionandAnswerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var QuestionLbl: UILabel!
    
    @IBOutlet weak var QuestionAskerName: UILabel!
    
    @IBOutlet weak var QuestionAskerTime: UILabel!
    
    @IBOutlet weak var AnswerLabel: UILabel!
    
    @IBOutlet weak var AnsweringTime: UILabel!
    
    @IBOutlet weak var ReplyBtnOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
//MARK:- //Seller Other Listing CollectionViewCell
class SellerListingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var SellerListingImageView: UIImageView!
    
    @IBOutlet weak var SellerListingProductName: UILabel!
    
}
//MARK:- Product Reviews TableviewCell
class NewProductListingReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ReviewDate: UILabel!
    
    @IBOutlet weak var ReviewDetails: UILabel!
    
    @IBOutlet weak var RatingNo: UILabel!
    
    @IBOutlet weak var ReviewerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
//MARK:- //Other Seller Listing CollectionViewCell

class BiggerImageCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var BiggerImage: UIImageView!
}


