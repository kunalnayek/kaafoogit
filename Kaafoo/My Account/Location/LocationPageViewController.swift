//
//  LocationPageViewController.swift
//  Kaafoo
//
//  Created by admin on 02/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SVProgressHUD
import GoogleMaps
import GooglePlaces

class LocationPageViewController: GlobalViewController,UITextViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate {

    @IBOutlet weak var locationEnterView: UIView!
    @IBOutlet weak var locationTXTView: UITextField!
    
    @IBOutlet weak var locationMapView: GMSMapView!
    @IBOutlet weak var listOfLocationsView: UIView!
    @IBOutlet weak var listOfLocationsTableView: UITableView!
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    
    
    var selectedAddress : String!
    
    var addressLat : String!
    
    var addressLong : String!
    
    var resultsArray : NSArray = NSArray()
    
    var annotation = MKPointAnnotation()
    
    var searchQuery : String!
    
    let dispatchGroup = DispatchGroup()
    
    var marker = GMSMarker()
    
    var updatedAddress : String!
    
    var addressID : String!
    
    var city : String!
    var state : String!
    var country : String!
    
    var newAddress : Int!
    
    var userType : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        //locationMapView.frame.origin.y = locationEnterView.frame.origin.y + locationEnterView.frame.size.height
        
        locationTXTView.delegate = self
        
        locationTXTView.text = selectedAddress!
        
        showLOcation()
        
        locationTXTView.inputView = UIView.init(frame: CGRect.zero)
        locationTXTView.inputAccessoryView = UIView.init(frame: CGRect.zero)
        
//        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
//        self.locationMapView.addGestureRecognizer(longPressRecognizer)


    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Submit Button
    
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func tapOnSubmit(_ sender: Any) {
        
        if newAddress == 1
        {
            SubmitNewAddress()
        }
        else
        {
            if selectedAddress == locationTXTView.text
            {
                print("No Changes in Address")
            }
            else
            {
                SubmitEditedAddress()
            }
            
            
        }
        
        
//        if userType.elementsEqual("P")
//        {
//            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pEditAccountPThreeVC") as! PEditAccountPThreeViewController
//
//            navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as! NSMutableDictionary
//
//            navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as! NSMutableDictionary
//
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else
//        {
//            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPOneVC") as! BEditAccountPOneViewController
//
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPOneVC") as! BEditAccountPOneViewController

        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Textview Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == locationTXTView
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            
            // Specify the place data types to return.
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue))!
            autocompleteController.placeFields = fields
            
            // Specify a filter.
            let filter = GMSAutocompleteFilter()
            filter.type = .address
            autocompleteController.autocompleteFilter = filter
            
            // Display the autocomplete view controller.
            present(autocompleteController, animated: true, completion: nil)
        }
        
        locationTXTView.text = ""
//        locationTXTView.becomeFirstResponder()
//
//        UIView.animate(withDuration: 0.3)
//        {
//
//            self.locationMapView.frame.origin.y = self.listOfLocationsView.frame.origin.y + self.listOfLocationsView.frame.size.height
//
//            self.locationMapView.frame.size.height = self.locationMapView.frame.size.height - self.listOfLocationsView.frame.size.height
//
//            self.listOfLocationsView.isHidden = false
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        dispatchGroup.enter()
        
        let writenTxt = locationTXTView.text
        let textChanging = string
        
        searchQuery = writenTxt! + textChanging
        
        dispatchGroup.leave()
        
        dispatchGroup.notify(queue: .main) {
            
            self.fireGoogleSearch()
            
        }
        
        return true
    
    }
    
    
    // MARK:- // Mapview Delegate Methods
    
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        
        let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: 12.0)
        self.locationMapView.animate(to: camera)
        
        print("Started Dragging")
        
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        let lat = marker.position.latitude
        let long = marker.position.longitude
        
        print("Stopped Dragging")
        let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: 12.0)
        locationMapView.animate(to: camera)
        
        addressLat = "\(lat)"
        addressLong = "\(long)"
        
//        print("Lat :" , addressLat , "Long :" , addressLong)
        
        getAddressFromLatLon(pdblLatitude: addressLat, withLongitude: addressLong)
        
    }
    
    
    
    // MARK:- // Tableview Delegates
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listPlacesCell") as! locationPageTVCTableViewCell
        
        let tempDict = self.resultsArray[indexPath.row] as! NSDictionary
        cell.cellLbl.text = "\(tempDict["description"]!)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tempDict = self.resultsArray[indexPath.row] as! NSDictionary
        searchQuery = "\(tempDict["place_id"]!)"
        
        updatedAddress = "\(tempDict["description"]!)"
        
        locationTXTView.text = updatedAddress
        
        fireGoogleSearchTwo()
        
        listOfLocationsView.isHidden = true
        
        locationTXTView.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3)
        {
            self.locationMapView.frame.origin.y = self.locationEnterView.frame.origin.y + self.locationEnterView.frame.size.height
            
            self.locationMapView.frame.size.height = self.listOfLocationsView.frame.size.height + self.locationMapView.frame.size.height
        }
        
    }
    
    
    
    
    // MARK: - // JSON Get Method to get Google Autocomplete Search Results
    
    
    func fireGoogleSearch()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
       
        
        
        var strGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchQuery!)&language=en&key= AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("Google Search URL is : ", strGoogleApi)
        
        if let url = NSURL(string: strGoogleApi)
            
        {
            
            if let data = try? Data(contentsOf:url as URL)
                
            {
                
                do{
                    
                    let jsonDict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
                    
                    print("Search Result Response : " , jsonDict)
                    
                    resultsArray = jsonDict["predictions"] as! NSArray
                    
                    DispatchQueue.main.async {
                        
                        self.listOfLocationsTableView.delegate = self
                        self.listOfLocationsTableView.dataSource = self
                        self.listOfLocationsTableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                    
                    
                }
                    
                catch
                    
                {
                    
                    print("Error")
                    
                }
                
            }
            
        }
        
    }
    
    
    
    // MARK: - // Get Method to get Lat Long of Selected Address
    
    
    func fireGoogleSearchTwo()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        var strGoogleApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(searchQuery!)&key= AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("Google Search Two URL is : ", strGoogleApi)
        
        if let url = NSURL(string: strGoogleApi)
            
        {
            
            if let data = try? Data(contentsOf:url as URL)
                
            {
                
                do{
                    
                    let jsonDict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
                    
                    print("Search Result Two Response : " , jsonDict)
                    
                    if "\(jsonDict["status"]!)".elementsEqual("OVER_QUERY_LIMIT")
                    {
                        
                    }
                    else
                    {
                        let resultsDictionary = jsonDict["result"] as! NSDictionary
                        
                        addressLat = "\(((resultsDictionary["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lat"]!)"
                        addressLong = "\(((resultsDictionary["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lng"]!)"
                        
                        city = "\(((resultsDictionary["address_components"] as! NSArray)[1] as! NSDictionary)["long_name"]!)"
                        state = "\(((resultsDictionary["address_components"] as! NSArray)[2] as! NSDictionary)["long_name"]!)"
                        country = "\(((resultsDictionary["address_components"] as! NSArray)[3] as! NSDictionary)["long_name"]!)"
                    }
                    
                    
                    DispatchQueue.main.async {
                        self.showLOcation()
                        SVProgressHUD.dismiss()
                    }
                    
                    
                }
                    
                catch
                    
                {
                    
                    print("Error")
                    
                }
                
            }
            
        }
        
    }
    
    
    
    
    
    
//    func handleLongPress(recognizer: UILongPressGestureRecognizer)
//    {
//        if (recognizer.state == UIGestureRecognizerState.began)
//        {
//            let longPressPoint = recognizer.location(in: self.locationMapView);
//            let coordinate = locationMapView.projection.coordinate(for: longPressPoint )
//            //Now you have Coordinate of map add marker on that location
//            let marker = GMSMarker(position: coordinate)
//            marker.opacity = 0.6
//            marker.position = coordinate
//            marker.title = "Current Location"
//            marker.snippet = ""
//            marker.map = locationMapView
//        }
//    }
    
    
    
    
    
    // MARK: - // JSON POST Method to Submit Edited Address
    
    func SubmitEditedAddress()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_address_save")!   //change the url
        
        print("Submit Edited Address URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&buss_id=\(addressID!)&buss_address=\(locationTXTView.text!)&lat=\(addressLat!)&long=\(addressLong!)&state=\(state!)&country=\(country!)&city=\(city!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Submit Address Response: " , json)
                    
                    self.dispatchGroup.leave()
                    
                    DispatchQueue.main.async {
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to Submit New Address
    
    func SubmitNewAddress()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_address_save")!   //change the url
        
        print("Submit New Address URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&buss_address=\(locationTXTView.text!)&lat=\(addressLat!)&long=\(addressLong!)&state=\(state!)&country=\(country!)&city=\(city!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Submit Address Response: " , json)
                    
                    self.dispatchGroup.leave()
                    
                    DispatchQueue.main.async {
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }


    
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Functions to show location
    
    
    func showLOcation()
    {
        
        print("Address Lat----",addressLat)
        print("Address Long----",addressLong)
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat)!, longitude: Double(addressLong)!, zoom: 12.0)
        
        // Creates a marker in the center of the map.
        //let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat)!, longitude: Double(addressLong)!)
        marker.title = "Selected Address"
        marker.snippet = "Description"
        marker.map = self.locationMapView
        
        self.locationMapView.animate(to: camera)
        
        marker.isDraggable = true
        
        locationMapView.delegate = self
        
        
        //        locationMapkit.removeAnnotation(annotation)
        //
        //        var location = CLLocationCoordinate2DMake(Double(addressLat)!, Double(addressLong)!)
        //
        //        var span = MKCoordinateSpanMake(0.2, 0.2)
        //
        //        var region = MKCoordinateRegion(center: location, span: span)
        //
        //        locationMapkit.setRegion(region, animated: true)
        //
        //
        //        annotation.coordinate = location
        //        annotation.title = "Selected Address"
        //        annotation.subtitle = selectedAddress!
        //
        //        locationMapkit.addAnnotation(annotation)
    }
    
    
  
    // MARK:- // Reverse Geocoding to get address from Entered Lat Long
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country!)
                    print(pm.locality!)
                    print(pm.subLocality!)
                    print(pm.thoroughfare!)
                    print(pm.postalCode!)
                    print(pm.subThoroughfare!)
                    print(pm.administrativeArea!)
                    var addressString : String = ""
                    if pm.subThoroughfare != nil {    // Street No
                        addressString = addressString + pm.subThoroughfare! + ", "
                    }
                    if pm.thoroughfare != nil {    //Street Name
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subLocality != nil {   //Area
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {     //City
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.administrativeArea != nil {   //State
                        addressString = addressString + pm.administrativeArea! + ", "
                    }
                    if pm.country != nil {    //Country
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {   //ZipCode
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    self.updatedAddress = addressString
                    print(self.updatedAddress)
                    self.locationTXTView.text = self.updatedAddress
                }
        })
        
    }
    
  
   
    
    
    
}

extension LocationPageViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        dispatchGroup.enter()
        
        self.locationTXTView.text = "\(place.name!)"
        
//        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        
        self.searchQuery = "\(place.placeID!)"
        
        dispatchGroup.leave()
        
        dispatchGroup.notify(queue: .main, execute: {
            self.fireGoogleSearchTwo()
        })
        
//        print("Place attributions: \(place.attributions)")
//        print("Latitude", place.formattedAddress)
//        print("Longitude",place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
