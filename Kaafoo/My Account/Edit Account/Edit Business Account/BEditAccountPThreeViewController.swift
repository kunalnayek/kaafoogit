//
//  BEditAccountPThreeViewController.swift
//  Kaafoo
//
//  Created by admin on 01/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class BEditAccountPThreeViewController: GlobalViewController {
    
    @IBOutlet weak var editlbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var newPasswordLbl: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var newPasswordTXT: UITextField!
    @IBOutlet weak var showHidePasswordImageView: UIImageView!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    
    
    var checkshow_email : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.setLanguageStrings()
        
        self.emailSwitchOutlet.isOn = true
        self.editedUserInfoDictionary["checkshow_email"] = "0"
        
        set_font()
        
        emailTXT.text = "\(userInfoDictionary["emailid"] ?? "")"
        
        let currentPassword = UserDefaults.standard.string(forKey: "lastLoggedPassword")
        
        newPasswordTXT.text = currentPassword
        
        newPasswordTXT.isSecureTextEntry = true
        
        showHidePasswordImageView.image = UIImage(named: "view")
        
        
        
    }
    
    @IBOutlet weak var emailSwitchOutlet: UISwitch!
    @IBAction func emailSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true)
        {
            checkshow_email = "1"
            emailTXT.isHidden = false
        }
        else
        {
            checkshow_email = "0"
            emailTXT.isHidden = true
        }
        
    }
    
    
    @IBOutlet weak var showHidePasswordOutlet: UIButton!
    @IBAction func showHidePassword(_ sender: Any) {
        
        if newPasswordTXT.isSecureTextEntry == true
        {
            newPasswordTXT.isSecureTextEntry = false
            showHidePasswordImageView.image = UIImage(named: "hide")
        }
        else
        {
            newPasswordTXT.isSecureTextEntry = true
            showHidePasswordImageView.image = UIImage(named: "view")
        }
        
    }
    
    
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func tapOnNext(_ sender: Any) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPFourVC") as! BEditAccountPFourViewController
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        addDataInArray()
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        //setAttributedTitle(toLabel: self.editlbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editlbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        emailLbl.font = UIFont(name: emailLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        emailTXT.font = UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordLbl.font = UIFont(name: newPasswordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordTXT.font = UIFont(name: (newPasswordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
        
    }
    
    
    func validateWithAlerts()
    {
        if (emailTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Email Empty", message: "Please Enter Email Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (newPasswordTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "New Password Empty", message: "Please Enter New Password", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        
    }
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
       
        self.editedUserInfoDictionary["email"] = self.emailTXT.text
        self.editedUserInfoDictionary["new_password"] = self.newPasswordTXT.text
        
    }
    
    
    
    
    // MARK:- // Set Language Strings
    
    func setLanguageStrings()
    {
        
        
        setAttributedTitle(toLabel: self.editlbl, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit", comment: ""), boldTextFont: UIFont(name: self.editlbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "account", comment: ""))
        
        
        self.editAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: "")
        
        self.emailLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email_address", comment: "")
        
        self.newPasswordLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new_password", comment: "")
        
        self.nextButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "next", comment: ""), for: .normal)
        
        
    }
    

    
}
