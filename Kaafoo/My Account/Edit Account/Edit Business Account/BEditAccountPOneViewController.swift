//
//  BEditAccountViewController.swift
//  Kaafoo
//
//  Created by admin on 01/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BEditAccountPOneViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var businessNameLbl: UILabel!
    @IBOutlet weak var displayNameLbl: UILabel!
    @IBOutlet weak var businessAddressLbl: UILabel!
    
    @IBOutlet weak var businessNameTXT: UITextField!
    @IBOutlet weak var displayNameTXT: UITextField!
    @IBOutlet weak var businessAddressTableView: UITableView!
    @IBOutlet weak var addMoreAndNextView: UIView!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var businessAddressTableHeightCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var businessAddressTitleView: UIView!
    
    
    let dispatchGroup = DispatchGroup()
    
    var userInfoDictionary : NSMutableDictionary!
    
    var userBusinessAddressArray : NSMutableArray! = NSMutableArray()
    
    var addressAdded : Bool = false
    
    var newAddressArrayCount : Int!
    
    var businessID : String!
    
    var checkshow_add : String!
    
    var TableHeight : CGFloat!
    
    var addMoreCellCount : Int! = 0
    
    
    
    var editedUserInfoDictionary : NSMutableDictionary! = ["business_name" : "" , "display_name" : "" , "business_address" : [] , "website" : "" , "landline" : "" , "mobile" : "" , "email" : "" , "checkshow_add" : "" , "chckshow_mobile" : "" , "chckshow_email" : ""]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.setLanguageStrings()
        self.set_font()
       
        dispatchGroup.enter()
        
        getAccountInfo()
        
        dispatchGroup.leave()
        
        dispatchGroup.notify(queue: .main) {
            
            
            self.editedUserInfoDictionary["checkshow_add"] = "0"
            
            self.businessNameTXT.text = "\(self.userInfoDictionary["business_name"] ?? "")"
            
            self.displayNameTXT.text = "\(self.userInfoDictionary["display_name"] ?? "")"
            
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.addMoreOutlet.layer.cornerRadius = self.addMoreOutlet.frame.size.height / 2
        
        self.businessAddressTableView.estimatedRowHeight = 500
        self.businessAddressTableView.rowHeight = UITableView.automaticDimension
        
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.dispatchGroup.notify(queue: .main) {
            
             self.setTableviewHeight(tableview: self.businessAddressTableView, heightConstraint: self.businessAddressTableHeightCOnstraint)
        }
        
       
        
    }
    
    
    
    // MARK:- // Buttons
    
    
    @IBOutlet weak var businessAddressSwitchOutlet: UISwitch!
    @IBAction func businessAddressSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true)
        {
            checkshow_add = "1"
            
            self.businessAddressTableHeightCOnstraint.constant = self.TableHeight
            
        }
        else
        {
            checkshow_add = "0"
            
            self.businessAddressTableHeightCOnstraint.constant = 0
            
        }
        
    }
    
    
    @IBOutlet weak var addMoreOutlet: UIButton!
    @IBAction func tapOnAddMore(_ sender: Any) {
        
        //addressAdded = true
        
        self.addMoreCellCount = addMoreCellCount + 1
        
        businessAddressTableView.reloadData()
        
        self.TableHeight = self.TableHeight + 60
        
        self.businessAddressTableHeightCOnstraint.constant = self.TableHeight
        
        
    }
    
    
    @IBOutlet weak var nextOutlet: UIButton!
    @IBAction func tapOnNext(_ sender: Any) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPTwoVC") as! BEditAccountPTwoViewController
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        addDataInArray()
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        //setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.businessNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
       
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        businessNameLbl.font = UIFont(name: businessNameLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        businessNameTXT.font = UIFont(name: (businessNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        displayNameLbl.font = UIFont(name: displayNameLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        displayNameTXT.font = UIFont(name: (displayNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        businessAddressLbl.font = UIFont(name: businessAddressLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        
       
        
    }
    
    
    // MARK:- // Set Position
    
    func set_position()
    {
        businessAddressTableView.reloadData()
        
        let cellHeight = ((60.0/568.0)*FullHeight)
        
        let tableviewHeight = cellHeight * (CGFloat(userBusinessAddressArray.count))
        
        businessAddressTableView.contentSize = CGSize(width: businessAddressTableView.frame.size.width, height: tableviewHeight)
        
        businessAddressTableView.frame.size.height = tableviewHeight
        
        addMoreAndNextView.frame.origin.y = businessAddressTableView.frame.origin.y + tableviewHeight + ((30.0/568.0)*FullHeight)
        
        mainScroll.contentSize = CGSize(width: mainScroll.frame.size.width, height: (addMoreAndNextView.frame.origin.y + addMoreAndNextView.frame.size.height) + ((60/568)*FullHeight))
        
    }
    
    
    // MARK:- // Set Position Two
    
    func set_positiontwo()
    {
        let cellHeight = ((60.0/568.0)*FullHeight)
        
        let tableviewHeight = cellHeight
        
        businessAddressTableView.contentSize = CGSize(width: businessAddressTableView.frame.size.width, height: tableviewHeight)
        
        businessAddressTableView.frame.size.height = tableviewHeight
        
        addMoreAndNextView.frame.origin.y = businessAddressTableView.frame.origin.y + tableviewHeight + ((20.0/568.0)*FullHeight)
        
        mainScroll.contentSize = CGSize(width: mainScroll.frame.size.width, height: (addMoreAndNextView.frame.origin.y + addMoreAndNextView.frame.size.height + ((20.0/568.0)*FullHeight)))
        
    }
    
    
    // MARK:- // Validate with Alerts
    
    
    func validateWithAlerts()
    {
        if (businessNameTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Business Name Empty", message: "Please Enter Business Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (displayNameTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Display Name Empty", message: "Please Enter Display Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (userBusinessAddressArray.count == 0)
            {
                let alert = UIAlertController(title: "Business Address Empty", message: "Atleast add one Business Address", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
        }
    }
    
    
    // MARK:- // Remove address
    
    
    @objc func remove(sender: UIButton)
    {

        let tempDictionary = userBusinessAddressArray[sender.tag] as! NSDictionary
        
        businessID = "\(tempDictionary["addrass_id"]!)"
        
        self.removeBusinessAddress {
            
            self.getAccountInfo()
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                self.setTableviewHeight(tableview: self.businessAddressTableView, heightConstraint: self.businessAddressTableHeightCOnstraint)
                
            })
            
        }
        
    }
    
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        self.editedUserInfoDictionary["business_name"] = self.businessNameTXT.text
        self.editedUserInfoDictionary["display_name"] = self.displayNameTXT.text
        self.editedUserInfoDictionary["business_address"] = self.userBusinessAddressArray
        
        
    }
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.userBusinessAddressArray.count + self.addMoreCellCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BEditAccountPOneTVCTableViewCell
        
        if indexPath.row < userBusinessAddressArray.count
        {
            let tempDict = userBusinessAddressArray[indexPath.row] as! NSMutableDictionary
            
            cell.addressLbl.text = "\(tempDict["address"]!)" + "," + "\(tempDict["city"]!)" + "," + "\(tempDict["state"]!)" + "," + "\(tempDict["country"]!)"
             cell.addressLbl.textColor = UIColor.black
            
            cell.removeButton.tag = indexPath.row
            
            cell.removeButton.addTarget(self, action: #selector(BEditAccountPOneViewController.remove(sender:)), for: .touchUpInside)
            
            cell.removeImageView.isHidden = false
            cell.removeButton.isHidden = false
            
            return cell
        }
        else
        {
            cell.addressLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "addNewAddress", comment: "")
            cell.addressLbl.textColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
            
            cell.removeImageView.isHidden = true
            cell.removeButton.isHidden = true
            
            return cell
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row < self.userBusinessAddressArray.count
        {
            let tempDict = userBusinessAddressArray[indexPath.row] as! NSMutableDictionary
            
//            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "locationPageVC") as! LocationPageViewController
            
              let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "locationPageVC") as! LocationPageViewController
            
            navigate.addressLat = "\(tempDict["lat"]!)"
            navigate.addressLong = "\(tempDict["long"]!)"
            navigate.selectedAddress = "\(tempDict["address"]!)" + "," + "\(tempDict["city"]!)" + "," + "\(tempDict["state"]!)" + "," + "\(tempDict["country"]!)"
            navigate.addressID = "\(tempDict["addrass_id"]!)"
            
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
//            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "locationPageVC") as! LocationPageViewController
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "locationPageVC") as! LocationPageViewController
            
            navigate.addressLat = "0.00"
            navigate.addressLong = "0.00"
            navigate.selectedAddress = "Enter New Address"
            navigate.newAddress = 1
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }

    }
    
    
    
    
    // MARK: - // JSON POST Method to get Account Info Data
    
    func getAccountInfo()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_edit_account_details")!   //change the url
        
        print("get account info URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }

        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("get account info Response: " , json)
                    
                    self.userInfoDictionary = json["info_array"] as? NSMutableDictionary

                    self.userBusinessAddressArray = self.userInfoDictionary["business_address"] as? NSMutableArray
                    
                    DispatchQueue.main.async {
                 
                        self.dispatchGroup.leave()
                        
                        self.businessAddressTableView.delegate = self
                        self.businessAddressTableView.dataSource = self
                        self.businessAddressTableView.reloadData()
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to remove Business Address
    
    func removeBusinessAddress(completion : @escaping () -> ())
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string:  GLOBALAPI + "app_remove_address")!   //change the url
        
        print("Remove address URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&remove_id=\(businessID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
      
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove Address Response: " , json)
                    
                    DispatchQueue.main.async {
                     
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                        
                        completion()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Set Tableview Height
    
    func setTableviewHeight(tableview: UITableView,heightConstraint: NSLayoutConstraint)
    {
        self.TableHeight = 0
        
        heightConstraint.constant = 5000
        
        print("count---",tableview.visibleCells.count)
        
        for cell in tableview.visibleCells {
            print("test--",cell.bounds.height)
            TableHeight += cell.bounds.height
        }
        
        heightConstraint.constant = TableHeight
        
//        print("tableHEight=----- ", TableHeight)
    }
    
    
    // MARK:- // Set Language Strings
    
    func setLanguageStrings()
    {
        setAttributedTitle(toLabel: self.editLbl, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit", comment: ""), boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.businessNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "account", comment: ""))
        
        
        self.editAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: "")
        
        self.businessNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_name", comment: "")
        
        self.displayNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "display_name", comment: "")
        
        self.businessAddressLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_address", comment: "")
        
        self.addMoreOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_more", comment: ""), for: .normal)
        
        self.nextOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "next", comment: ""), for: .normal)
    }
    
    


}
