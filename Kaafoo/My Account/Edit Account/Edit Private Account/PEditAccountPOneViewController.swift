//
//  PEditAccountViewController.swift
//  Kaafoo
//
//  Created by admin on 01/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class PEditAccountPOneViewController: GlobalViewController {
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var lastNameLbl: UILabel!
    @IBOutlet weak var displayNameLbl: UILabel!
    @IBOutlet weak var firstNameTXT: UITextField!
    @IBOutlet weak var lastNameTXT: UITextField!
    @IBOutlet weak var displayNameTXT: UITextField!
    
    let dispatchGroup = DispatchGroup()
    
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary! = ["first_name" : "" , "last_name" : "" , "display_name" : "" , "gender" : "" , "dob" : "" , "landline" : "" , "mobile" : "" , "address" : "" , "nearby_town" : "" , "email" : "" , "new_password" : "" , "pchckshow_add" : "" , "pchckshow_add" : "" , "chckshow_pemail" : ""]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        dispatchGroup.enter()
        
        getAccountInfo()
        
        dispatchGroup.leave()
        
        dispatchGroup.notify(queue: .main) {
            
            self.set_font()
            
            self.firstNameTXT.text = "\(self.userInfoDictionary["fname"]!)"
            
            self.lastNameTXT.text = "\(self.userInfoDictionary["lname"]!)"
            
            self.displayNameTXT.text = "\(self.userInfoDictionary["display_name"]!)"
            
        }
       
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Next Button
    
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButton(_ sender: Any) {
        
        addDataInArray()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pEditAccountPTwoVC") as! PEditAccountPTwoViewController
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        self.editedUserInfoDictionary["first_name"] = self.firstNameTXT.text
        self.editedUserInfoDictionary["last_name"] = self.lastNameTXT.text
        self.editedUserInfoDictionary["display_name"] = self.displayNameTXT.text
    }
    
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.firstNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        firstNameLbl.font = UIFont(name: firstNameLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        lastNameLbl.font = UIFont(name: lastNameLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        displayNameLbl.font = UIFont(name: displayNameLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        firstNameTXT.font = UIFont(name: (firstNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        lastNameTXT.font = UIFont(name: (lastNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        displayNameTXT.font = UIFont(name: (displayNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to get Account Info Data
    
    func getAccountInfo()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_edit_account_details")!   //change the url
        
        print("get account info URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("get account info Response: " , json)
                    

                    self.userInfoDictionary = json["info_array"] as? NSMutableDictionary

                    self.dispatchGroup.leave()
                    
                    DispatchQueue.main.async {
 
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    


}
