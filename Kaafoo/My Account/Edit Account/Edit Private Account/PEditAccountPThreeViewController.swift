//
//  PEditAccountPThreeViewController.swift
//  Kaafoo
//
//  Created by admin on 03/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class PEditAccountPThreeViewController: GlobalViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var nearbyTownLbl: UILabel!
    @IBOutlet weak var mobileNoTXT: UITextField!
    @IBOutlet weak var addressTXT: UITextField!
    @IBOutlet weak var nearbyTownTXT: UITextField!
    
    
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        addressTXT.inputView = UIView.init(frame: CGRect.zero)
        addressTXT.inputAccessoryView = UIView.init(frame: CGRect.zero)
        
        self.addressTXT.delegate = self
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
            
            self.set_font()
            
            self.mobileNoTXT.text = "\(self.userInfoDictionary["mobilenum"] ?? "")"
        
            self.addressTXT.text = "\(self.userInfoDictionary["address"] ?? "")"
            
//            self.addressTXT.text = "\((self.userInfoDictionary["private_address"] as! NSDictionary)["private_adds"]!)"
        
            self.nearbyTownTXT.text = "\(self.userInfoDictionary["address"] ?? "")"
        
        

    }
    
    //MARK:- //TextField Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == addressTXT
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            
            // Specify the place data types to return.
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue))!
            autocompleteController.placeFields = fields
            
            // Specify a filter.
            let filter = GMSAutocompleteFilter()
            filter.type = .address
            autocompleteController.autocompleteFilter = filter
            
            // Display the autocomplete view controller.
            present(autocompleteController, animated: true, completion: nil)
        }
        else
        {
            //do nothing
        }
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Next Button
    
    
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButton(_ sender: Any) {
        
        addDataInArray()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pEditAccountPFourVC") as! PEditAccountPFourViewController
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Edit Address
    
    
//    @IBOutlet weak var editAddressOutlet: UIButton!
//    @IBAction func editAddressTap(_ sender: Any) {
//
//        let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "locationPageVC") as! LocationPageViewController
//
//        navigate.userType = "P"
//        navigate.addressLat = "0.00"
//        navigate.addressLong = "0.00"
//        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
//        navigate.selectedAddress = addressTXT.text
//        self.navigationController?.pushViewController(navigate, animated: true)
//
//    }
    
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        self.editedUserInfoDictionary["mobile"] = self.mobileNoTXT.text
        self.editedUserInfoDictionary["address"] = self.addressTXT.text
        self.editedUserInfoDictionary["nearby_town"] = self.addressTXT.text
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        mobileNoLbl.font = UIFont(name: mobileNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        addressLbl.font = UIFont(name: addressLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        nearbyTownLbl.font = UIFont(name: nearbyTownLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        mobileNoTXT.font = UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        addressTXT.font = UIFont(name: (addressTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        nearbyTownTXT.font = UIFont(name: (nearbyTownTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
    }
    
    
    

    
}

extension PEditAccountPThreeViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        self.addressTXT.text = "\(place.name!)"
        
        self.editedUserInfoDictionary["nearby_town"] = "\(place.name!)"
        
//        print("Dictionary",userInfoDictionary!)
//        print("EditedUserInfoDict",editedUserInfoDictionary!)
        
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
