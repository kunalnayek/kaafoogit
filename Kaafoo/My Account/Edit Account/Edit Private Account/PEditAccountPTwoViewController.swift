//
//  PEditAccountPTwoViewController.swift
//  Kaafoo
//
//  Created by admin on 03/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class PEditAccountPTwoViewController: GlobalViewController {
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var maleLbl: UILabel!
    @IBOutlet weak var maleImage: UIImageView!
    @IBOutlet weak var femaleLbl: UILabel!
    @IBOutlet weak var femaleImage: UIImageView!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var landLineLbl: UILabel!
    @IBOutlet weak var dobTXT: UITextField!
    @IBOutlet weak var landLineNoTXT: UITextField!
    @IBOutlet weak var dobPicker: UIDatePicker!
    @IBOutlet weak var dobView: UIView!
    
    
    
    var Gender : String!
    
    var click : Int = 0
    
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.set_font()
        
        if "\(self.userInfoDictionary["gender"]!)".elementsEqual("M")
        {
            maleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
            
            femaleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
            
            maleImage.image = UIImage(named : "002-male")
            femaleImage.image = UIImage(named : "001-female copy")
            
            Gender = "M"
            
//            print("Gender is : ", Gender)
        }
        else if "\(self.userInfoDictionary["gender"]!)".elementsEqual("F")
        {
            femaleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
            
            maleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
            
            maleImage.image = UIImage(named : "002-male copy")
            femaleImage.image = UIImage(named : "001-female")
            
            Gender = "F"
            
//            print("Gender is : ", Gender)
        }
        
        self.dobTXT.text = "\(self.userInfoDictionary["dob"]!)"
        
        self.landLineNoTXT.text = "\(self.userInfoDictionary["landline"]!)"
        
        

    }
    
    // MARK:- // Buttons
    
    // MARK:- // Next Button
    
    
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButton(_ sender: Any) {
        
        addDataInArray()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pEditAccountPThreeVC") as! PEditAccountPThreeViewController
        
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Click on Male
    
    @IBAction func clickOnMale(_ sender: UIButton) {
        
        maleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
        
        femaleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
        
        maleImage.image = UIImage(named : "002-male")
        femaleImage.image = UIImage(named : "001-female copy")
        
        Gender = "M"
        
//        print("Gender is : ", Gender)
        
    }
    
    
    // MARK:- // Click on Female
    
    @IBAction func clickOnFemale(_ sender: UIButton) {
        
        femaleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
        
        maleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
        
        maleImage.image = UIImage(named : "002-male copy")
        femaleImage.image = UIImage(named : "001-female")
        
        Gender = "F"
        
//        print("Gender is : ", Gender)
        
    }
    
    
    // MARK:- // Click On Date of Birth
    
    @IBAction func clickOnDob(_ sender: UIButton) {
        
        
        
        if click == 0
        {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {
                    
                    self.dobView.isHidden = false
                    self.click = 1
                    
//                    self.dobLbl.frame.origin.y = ((270/568)*self.FullHeight)
//                    self.dobLbl.font = UIFont(name: (self.dobLbl.font?.fontName)!,size: 11)
//                    self.dobLbl.font = UIFont(name: self.dobLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
                    
//                    if (self.landLineNoTXT.text?.isEmpty)!
//                    {
//                        self.landLineGetBackInPosition()
//
//                    }
                    
            }, completion: { (finished: Bool) in})
            
        }
        else
        {
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {
                    
                    
                    
                    let dateformat = DateFormatter()
                    
                    dateformat.dateFormat = "yyyy-MM-dd"
                    
                    self.dobTXT.text = dateformat.string(from: self.dobPicker.date)
                    
                    self.dobView.isHidden = true
                    
                    self.click = 0
                    
            }, completion: { (finished: Bool) in})
            
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        self.editedUserInfoDictionary["gender"] = Gender
        self.editedUserInfoDictionary["dob"] = self.dobTXT.text
        self.editedUserInfoDictionary["landline"] = self.landLineNoTXT.text
    }
    
    
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.dobTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        genderLbl.font = UIFont(name: genderLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        dobLbl.font = UIFont(name: dobLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        landLineLbl.font = UIFont(name: landLineLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        dobTXT.font = UIFont(name: (dobTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        landLineNoTXT.font = UIFont(name: (landLineNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
    }
    
    
    

   

}
