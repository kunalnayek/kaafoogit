//
//  PEditAccountPFourViewController.swift
//  Kaafoo
//
//  Created by admin on 03/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class PEditAccountPFourViewController: GlobalViewController {
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var newPasswordLbl: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var newPasswordTXT: UITextField!
    @IBOutlet weak var passwordWarningLbl: UILabel!
    
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    

    let dispatchGroup = DispatchGroup()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        set_font()
        
        emailTXT.text = "\(userInfoDictionary["emailid"]!)"
        
        let currentPassword = UserDefaults.standard.string(forKey: "lastLoggedPassword")
        
        newPasswordTXT.text = currentPassword
        
        newPasswordTXT.isSecureTextEntry = true
        
    }
    
    // MARK:- // Buttons
    
    // MARK:- // Submit Button
    
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: Any) {
        
        
        
        dispatchGroup.enter()
        
        addDataInArray()

//        print("Edited User Info Dictionary : ", editedUserInfoDictionary)
        
        self.saveInfo()
        
        dispatchGroup.notify(queue: .main) {
            
            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }
        
        
    }
    
    
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
       
        self.editedUserInfoDictionary["email"] = self.emailTXT.text
        self.editedUserInfoDictionary["new_password"] = self.newPasswordTXT.text
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.newPasswordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        emailLbl.font = UIFont(name: emailLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        emailTXT.font = UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordLbl.font = UIFont(name: newPasswordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordTXT.font = UIFont(name: (newPasswordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        passwordWarningLbl.font = UIFont(name: (passwordWarningLbl.font?.fontName)!, size: CGFloat(Get_fontSize(size: 10)))
        
        
    }
    
    
    // MARK: - // JSON POST Method to Save Account Info
    
    
    func saveInfo()
        
    {
        
        dispatchGroup.enter()
        
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_edit_account_save")!   //change the url
        
        print("save account info URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&f_name=\(editedUserInfoDictionary["first_name"]!)&l_name=\(editedUserInfoDictionary["last_name"]!)&display_name=\(editedUserInfoDictionary["display_name"]!)&near_by_loc=\(editedUserInfoDictionary["nearby_town"]!)&privt_gender=\(editedUserInfoDictionary["gender"]!)&dob=\(editedUserInfoDictionary["dob"]!)&plandline_no=\(editedUserInfoDictionary["landline"]!)&pmobile_no=\(editedUserInfoDictionary["mobile"]!)&pnew_pass=\(editedUserInfoDictionary["new_password"]!)&pchckshow_add=1&chckshow_pmobile=1&chckshow_pemail=1"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                print("test : ",data)
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String : Any]
                    print("Submit Account info Response: " , json)
                
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                        self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                    }
                    
                
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    

    
    

    

}
