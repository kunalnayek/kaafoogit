//
//  VerificationFileViewController.swift
//  Kaafoo
//
//  Created by admin on 25/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class VerificationFileViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var scrollStackview: UIStackView!
    @IBOutlet weak var staticHeaderView: UIView!
    @IBOutlet weak var staticHeaderLBL: UILabel!
    @IBOutlet weak var verifiedByNormalTitleview: UIView!
    @IBOutlet weak var normalCheckboxImageview: UIImageView!
    @IBOutlet weak var verifiedByNormalLBL: UILabel!
    @IBOutlet weak var verifiedByNormalView: UIView!
    @IBOutlet weak var verifiedByNormalCollectionview: UICollectionView!
    @IBOutlet weak var verifiedByMaroofTitleview: UIView!
    @IBOutlet weak var maroofCheckboxImageview: UIImageView!
    @IBOutlet weak var verifiedByMaroofLBL: UILabel!
    @IBOutlet weak var verifiedByMaroofView: UIView!
    @IBOutlet weak var maroofLinkView: UIView!
    @IBOutlet weak var maroofLinkLBL: UILabel!
    @IBOutlet weak var maroofLinkTXT: UITextField!
    @IBOutlet weak var maroofMessageView: UIView!
    @IBOutlet weak var maroofMessageLBL: UILabel!
    @IBOutlet weak var maroofMessageTXT: UITextField!
    @IBOutlet weak var viewOfVerifiedMyMaroofCollectionview: UIView!
    @IBOutlet weak var verifiedByMaroofCollectionview: UICollectionView!
    
    let dispatchGroup = DispatchGroup()
    
    var normalUploadSelected : Bool! = true
    var maroofUploadSelected : Bool! = true
    
    var verificationFileDetailsDictionary : NSDictionary!
    
    var normalUploadImageArray  = [UIImage]()
    var maroofUploadImageArray  = [UIImage]()
    
    var typeArray = [String]()
    
    var uploadButtonTag : Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getVerificationFileDetails()

        self.changeTickImageview(type: "normal")
        self.changeTickImageview(type: "maroof")
        
        typeArray.append("1")
        
        self.dispatchGroup.notify(queue: .main) {
            
            let tempWidth = (self.verifiedByNormalCollectionview.frame.size.width - 20)/3
            let tempLayout = self.verifiedByNormalCollectionview.collectionViewLayout as! UICollectionViewFlowLayout
            tempLayout.itemSize = CGSize(width: tempWidth, height: tempWidth)
            
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Tap Normal Upload Checkbox
    
    @IBOutlet weak var normalUploadCheckboxButtonOutlet: UIButton!
    @IBAction func tapNormalUploadCheckbox(_ sender: UIButton) {
        
        self.normalUploadSelected = !self.normalUploadSelected
        
//        print("Normal Upload Selected---",self.normalUploadSelected)
        
        if normalUploadSelected == true
        {
            self.verifiedByNormalView.isHidden = false
        }
        else
        {
            self.verifiedByNormalView.isHidden = true
        }
        
        self.changeTickImageview(type: "normal")
        
    }
    
    
    // MARK:- // Tap Maroof upload Checkbox
    
    @IBOutlet weak var maroofUploadCheckboxButtonOutlet: UIButton!
    @IBAction func tapMaroofUploadCheckbox(_ sender: UIButton) {
        
        self.maroofUploadSelected = !self.maroofUploadSelected
        
        if maroofUploadSelected == true
        {
            self.verifiedByMaroofView.isHidden = false
        }
        else
        {
            self.verifiedByMaroofView.isHidden = true
        }
        
        self.changeTickImageview(type: "maroof")
        
    }
    
    
    
    
    // MARK:- // Normal Upload Button
    
    @IBOutlet weak var normalUploadButtonOutlet: UIButton!
    @IBAction func normalUploadButton(_ sender: UIButton) {
        
        self.uploadButtonTag = 0
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Upload Files", message: "Choose a Type", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Files", style: .default, handler: { (action:UIAlertAction) in
            
            //            imagePickerController.sourceType = .camera
            //            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    // MARK:- // Maroof Upload Button
    
    @IBOutlet weak var maroofUploadButtonOutlet: UIButton!
    @IBAction func maroofUploadButton(_ sender: UIButton) {
        
        self.uploadButtonTag = 1
        /*
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Upload Files", message: "Choose a Type", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Files", style: .default, handler: { (action:UIAlertAction) in
            
            //            imagePickerController.sourceType = .camera
            //            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        */
        
        self.uploadImages()
        
    }
    
    
    // MARK:- // Change Image based on Selection
    
    func changeTickImageview(type : String)
    {
        if type.elementsEqual("normal")
        {
            if normalUploadSelected == true
            {
                self.normalCheckboxImageview.image = UIImage(named: "checkBoxFilled")
            }
            else
            {
                self.normalCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
            }
        }
        else
        {
            if maroofUploadSelected == true
            {
                self.maroofCheckboxImageview.image = UIImage(named: "checkBoxFilled")
            }
            else
            {
                self.maroofCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
            }
        }
        
        
    }
    
    
    
    
    // MARK:- // Delegate Functions
    
    //a MARK:- // Collectionview Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == verifiedByNormalCollectionview
        {
            
            return (verificationFileDetailsDictionary["normal_verify_files"] as! NSArray).count + normalUploadImageArray.count
        }
        else
        {
            return (verificationFileDetailsDictionary["maroof_verify_files"] as! NSArray).count + maroofUploadImageArray.count
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == verifiedByNormalCollectionview
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "normal", for: indexPath) as! VFNormalUploadCollectionViewCell
            
            let tempCount = (verificationFileDetailsDictionary["normal_verify_files"] as! NSArray).count
            
            if indexPath.row < tempCount
            {
                cell.cellImage.sd_setImage(with: URL(string: "\(((((self.verificationFileDetailsDictionary["normal_verify_files"] as! NSArray)[indexPath.row] as! NSDictionary)["normal_files_details"] as! NSArray)[0] as! NSDictionary)["files"] ?? "")"))
            }
            else
            {
                cell.cellImage.image = normalUploadImageArray[indexPath.row - tempCount]
            }
            
            return cell
            
        }
        else
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "maroof", for: indexPath) as! VFMaroofUploadCollectionViewCell
            
            let tempCount = (verificationFileDetailsDictionary["maroof_verify_files"] as! NSArray).count
            
            if indexPath.row < tempCount
            {
                cell.cellImage.sd_setImage(with: URL(string: "\(((((self.verificationFileDetailsDictionary["maroof_verify_files"] as! NSArray)[indexPath.row] as! NSDictionary)["marrof_files_details"] as! NSArray)[0] as! NSDictionary)["files"] ?? "")"))
            }
            else
            {
                cell.cellImage.image = maroofUploadImageArray[indexPath.row - tempCount]
            }
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10//(10/568)*self.FullHeight
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0//(10/568)*self.FullHeight
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let bounds = self.verifiedByNormalCollectionview.bounds
        let heightVal = self.verifiedByNormalCollectionview.frame.size.width / 3
        
        return CGSize(width: heightVal - 10, height: heightVal - 10)
        
    }
    
    
    
    
    
    // MARK:- // Imagepicker Delegates
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if uploadButtonTag == 0
        {
            
            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
            
            print("Done")
            
            self.normalUploadImageArray.append(image)
            self.verifiedByNormalCollectionview.reloadData()
            
            picker.dismiss(animated: true, completion: nil)
            
        }
        else
        {
            
            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
            
            print("Done")
            
            self.maroofUploadImageArray.append(image)
            self.verifiedByMaroofCollectionview.reloadData()
            
            picker.dismiss(animated: true, completion: nil)
            
        }
        
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    // MARK: - // JSON POST Method to get Verification File Details
    
    func getVerificationFileDetails()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_verification_doc_details")!       //change the url
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "lang_id=\(langID ?? "")&user_id=\(userID ?? "")"
        
        print("Parameters are : " , parameters)
        
        print("JOB Details URL : ", "\(url)" + "?" + parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Get Verification FIle Details Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {

                        self.verificationFileDetailsDictionary = json["info_array"] as? NSDictionary


                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.verifiedByNormalCollectionview.delegate = self
                            self.verifiedByNormalCollectionview.dataSource = self
                            self.verifiedByNormalCollectionview.reloadData()

                            self.verifiedByMaroofCollectionview.delegate = self
                            self.verifiedByMaroofCollectionview.dataSource = self
                            self.verifiedByMaroofCollectionview.reloadData()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Json Post Method to Add Images in an Array uploaded in Gallery
    
    
    
    func uploadImages()
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let myUrl = NSURL(string: GLOBALAPI + "app_verification_doc_file_save")!   //change the url
        
        print("Image Upload Url : ", myUrl)
        
        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"
        
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        
        // User "authentication":
        let parameters = ["user_id" : "\(userID!)" , "lang_id" : "\(langID!)" , "verified_type[]" : "\(JSONStringify(value: typeArray as AnyObject))"]
        
        print("Parameters : ", parameters)
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")
        
        var imageDataArray = [Data]()
        
        //print("Image Array : ", imagesToUpload)
        
        for i in 0..<normalUploadImageArray.count
        {
            let imageData = normalUploadImageArray[i].jpegData(compressionQuality: 1)
            imageDataArray.append(imageData!)
        }
        
        
        
        if(imageDataArray.count==0)  {
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            
            return; }
        
        print(imageDataArray)
        
        request.httpBody = createBodyWithParametersAndImages(parameters: parameters, filePathKey: "verify_file[]", imageDataKey: imageDataArray, boundary: boundary) as Data
        
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as? NSDictionary
                
                print(json!)
                
                self.dispatchGroup.leave()
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    self.ShowAlertMessage(title: "Alert", message: "\(json!["message"] ?? "")")
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
        
        
    }
    
    
    
    
    
    
    
    
    
    // MARK:- // Creating HTTP Body with Parameters while Sending an Array of Images
    
    
    func createBodyWithParametersAndImages(parameters: [String: Any]?, filePathKey: String, imageDataKey: [Data], boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        for index in 0..<imageDataKey.count {
            let data = imageDataKey[index]
            
            let filename = "image\(index).jpeg"
            let mimetype = "image/jpeg"
            
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.appendString(string: "\r\n")
            
        }
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    
    
    // MARK:- // Generating Boundary String for Multipart Form Data
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    
                    return string as String
                }
            }catch {
                
                print("error")
                //Access error here
            }
            
        }
        return ""
        
    }


}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
