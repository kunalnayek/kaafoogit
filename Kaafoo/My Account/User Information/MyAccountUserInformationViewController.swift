//
//  MyAccountUserInformationViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 26/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyAccountUserInformationViewController: GlobalViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    
    @IBOutlet weak var timezoneView: UIView!
    @IBOutlet weak var timezoneTitleLBL: UILabel!
    @IBOutlet weak var timezone: UILabel!
    
    @IBOutlet weak var timezoneButtonOutlet: UIButton!
    @IBAction func tapTimezoneButton(_ sender: UIButton) {
        
        self.firstLoad = false
        
        self.timezoneClicked = true
        self.startTimeClicked = false
        self.endTimeClicked = false
        
        self.userInformationPickerview.reloadAllComponents()
        
        UIView.animate(withDuration: 0.5) {
            
            self.viewOfPickerview.isHidden = false
            self.view.bringSubviewToFront(self.viewOfPickerview)
        }
        
    }
    
    
    @IBOutlet weak var paymentTypeStackview: UIStackView!
    @IBOutlet weak var paymentTypeListView: UIView!
    @IBOutlet weak var paymentTypeTitleLBL: UILabel!
    @IBOutlet weak var paymentTypeTableview: UITableView!
    
    
    @IBOutlet weak var bankInformationView: UIView!
    @IBOutlet weak var bankNameLBL: UILabel!
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var accountNumberLBL: UILabel!
    @IBOutlet weak var accountNumber: UITextField!
    
    @IBOutlet weak var bookingAvailabilityTitleLBL: UILabel!
    @IBOutlet weak var bookingAvailabilityView: UIView!
    @IBOutlet weak var bookingAvailabilityTableview: UITableView!
    
    @IBOutlet weak var bonusSectionView: UIView!
    
    
    
    
    
    
    
    
    @IBOutlet weak var viewOfPickerview: UIView!
    @IBOutlet weak var userInformationPickerview: UIPickerView!
    
    
    
    
    
    
    
    @IBOutlet weak var paymentTypeTableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bookingAvailabilityTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewOfPickerviewTopConstraint: NSLayoutConstraint!
    
    
    
    
    
    
    
    var height: CGFloat = 0
    
    
    var userInformationAllDataDicitonary : NSDictionary!
    
    let dispatchGroup = DispatchGroup()
    
    var timezoneArray : NSArray!
    
    var paymentTypeDataArray : NSArray!
    
    var paymentTypeButtonsArray = [UIButton]()
    
    var bookingCheckboxButtonsArray = [UIButton]()
    
    var startTimeButtonsArray = [UIButton]()
    
    var endTimeButtonsArray = [UIButton]()
    
    var timezoneClicked : Bool! = false
    
    var firstLoad : Bool! = true
    
    var bookingAvailabilityDataArray : NSArray!
    
    var localTimeArray = ["00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00",]
    
    var startTimeClicked : Bool! = false
    var endTimeClicked : Bool! = false
    var startTimeIndex : Int!
    var endTimeIndex : Int!
    
    
    var startTimeString : String!
    var endTimeString : String!
    var dayPositionString : String!
    var paymentTypeString : String!
    var timeZoneID : String!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.getUserInformationDetails()
        
        self.dispatchGroup.notify(queue: .main) {
            
            
            self.createPaymentType()
            
            self.setTimeZoneOnLoad()
            
            
        }

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.dispatchGroup.notify(queue: .main) {
            
            self.paymentTypeTableview.estimatedRowHeight = 100
            self.paymentTypeTableview.rowHeight = UITableView.automaticDimension
            
            self.setTableviewHeight()
           
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    
    
    
    
    
    
    
    // MARK: - // JSON POST Method to get Edit Account Info Data
    
    func getUserInformationDetails()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        let url = URL(string: GLOBALAPI + "app_user_information_details")!       //change the url
        
        print("User Information Details URl : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "lang_id=\(langID!)&user_id=\(userID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }

        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Edit Account Info Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {
                        self.userInformationAllDataDicitonary = json["info_array"] as? NSDictionary
                        
                        self.timezoneArray = self.userInformationAllDataDicitonary["timezone"] as? NSArray
                        
                        self.paymentTypeDataArray = self.userInformationAllDataDicitonary["payment_type"] as? NSArray
                        
                        self.bookingAvailabilityDataArray = self.userInformationAllDataDicitonary["booking_availables"] as? NSArray
                        
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.timezoneClicked = true
                            
                            self.userInformationPickerview.delegate = self
                            self.userInformationPickerview.dataSource = self
                            self.userInformationPickerview.reloadAllComponents()
                            
                            
                            
                            self.bookingAvailabilityTableview.delegate = self
                            self.bookingAvailabilityTableview.dataSource = self
                            self.bookingAvailabilityTableview.reloadData()
                            
                            
                            SVProgressHUD.dismiss()
                            
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                            
                        }
                    }
                    
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
                
            }
        })
        
        task.resume()
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to save All Data
    
    func saveAllData()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_user_information_save")!       //change the url
        
        print("Save User Information URl : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        
        parameters = "lang_id=\(langID ?? "")&user_id=\(userID ?? "")&timezone_id=\(timeZoneID ?? "")&payment_type=\(paymentTypeString ?? "")&bank_name=\(bankName.text ?? "")&acc_no=\(accountNumber.text ?? "")&booking_open_time=\(startTimeString ?? "")&booking_close_time=\(endTimeString ?? "")&booking_day_pos=\(dayPositionString ?? "")"
        
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
                
                return
            }
            
            do {
                
                
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary {
                    
                    print("Save User Information Response: " , json)
                    
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    
                }
            }
        })
        
        task.resume()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Pickerview Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.timezoneClicked == true
        {
            return self.timezoneArray.count
        }
        else
        {
            return self.localTimeArray.count
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.timezoneClicked == true
        {
            return "\((self.timezoneArray[row] as! NSDictionary)["time_name"] ?? "")"
        }
        else
        {
            return self.localTimeArray[row]
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.timezoneClicked == true
        {
            timezone.text = "\((self.timezoneArray[row] as! NSDictionary)["time_name"] ?? "")"
            
            self.timeZoneID = "\((self.timezoneArray[row] as! NSDictionary)["id"] ?? "")"
            
            UIView.animate(withDuration: 0.5) {
                
                self.viewOfPickerview.isHidden = true
                self.view.sendSubviewToBack(self.viewOfPickerview)
                
            }
        }
        else if self.startTimeClicked == true
        {
           
            print("start---",self.startTimeButtonsArray)
            self.startTimeButtonsArray[startTimeIndex].setTitle(self.localTimeArray[row], for: .normal)
            
            UIView.animate(withDuration: 0.5) {
                
                self.viewOfPickerview.isHidden = true
                self.view.sendSubviewToBack(self.viewOfPickerview)
                
            }
        }
        else
        {
            self.endTimeButtonsArray[endTimeIndex].setTitle(self.localTimeArray[row], for: .normal)
            
            UIView.animate(withDuration: 0.5) {
                
                self.viewOfPickerview.isHidden = true
                self.view.sendSubviewToBack(self.viewOfPickerview)
                
            }
        }
        
    }
    
    
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.paymentTypeTableview
        {
            return self.paymentTypeDataArray.count
        }
        else
        {
            return self.bookingAvailabilityDataArray.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.paymentTypeTableview
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! PaymentTypeTVCTableViewCell
            
            
            if firstLoad == true
            {
                if "\((self.paymentTypeDataArray[indexPath.row] as! NSDictionary)["status"] ?? "")".elementsEqual("1")
                {
                    cell.checkBoxImageview.image = UIImage(named: "checkBoxFilled")
                    cell.cellButton.isSelected = true
                }
                else
                {
                    cell.checkBoxImageview.image = UIImage(named: "checkBoxEmpty")
                    cell.cellButton.isSelected = false
                }
                
            }
            else
            {
                if cell.cellButton.isSelected == true
                {
                    cell.checkBoxImageview.image = UIImage(named: "checkBoxFilled")
                }
                else
                {
                    cell.checkBoxImageview.image = UIImage(named: "checkBoxEmpty")
                }
            }
            
            
            cell.cellLBL.text = "\((self.paymentTypeDataArray[indexPath.row] as! NSDictionary)["type"] ?? "")"
            
            self.paymentTypeButtonsArray.append(cell.cellButton)
            
            
            cell.cellButton.tag = indexPath.item
            
            cell.cellButton.addTarget(self, action: #selector(MyAccountUserInformationViewController.setSelection(sender:)), for: .touchUpInside)
            
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BookingAvailabilityTVCTableViewCell
            
            cell.dayLBL.text = "\((self.bookingAvailabilityDataArray[indexPath.row] as! NSDictionary)["day"] ?? "")"
            
            
            cell.startTime.titleLabel?.font = UIFont(name: (cell.startTime.titleLabel?.font.fontName)!, size: 14)
            cell.endTime.titleLabel?.font = UIFont(name: (cell.endTime.titleLabel?.font.fontName)!, size: 14)
            
            
            
            
            if firstLoad == true
            {
                
                if "\((self.bookingAvailabilityDataArray[indexPath.row] as! NSDictionary)["check_status"] ?? "")".elementsEqual("1")
                {
                    cell.checkboxButton.isSelected = true
                }
                else
                {
                    cell.checkboxButton.isSelected = false
                }
                
                
            }
            
            
            
            if cell.checkboxButton.isSelected == true
            {
                cell.startTime.isUserInteractionEnabled = true
                cell.startTime.titleLabel?.textColor = UIColor.black
                cell.endTime.titleLabel?.textColor = UIColor.black
            }
            else
            {
                cell.startTime.isUserInteractionEnabled = false
                cell.startTime.titleLabel?.textColor = UIColor.darkGray
                cell.endTime.titleLabel?.textColor = UIColor.darkGray
            }


            

            
            cell.startTime.setTitle("\((self.bookingAvailabilityDataArray[indexPath.row] as! NSDictionary)["open_start"] ?? "")", for: .normal)
            
            cell.endTime.setTitle("\((self.bookingAvailabilityDataArray[indexPath.row] as! NSDictionary)["open_close"] ?? "")", for: .normal)
            
            if self.bookingCheckboxButtonsArray.contains(cell.checkboxButton)
            {
                
            }
            else
            {
                self.bookingCheckboxButtonsArray.append(cell.checkboxButton)
                self.startTimeButtonsArray.append(cell.startTime)
                self.endTimeButtonsArray.append(cell.endTime)
            }
            
            
            
            
            cell.checkboxButton.tag = indexPath.item
            cell.startTime.tag = indexPath.item
            cell.endTime.tag = indexPath.item
            
            cell.checkboxButton.addTarget(self, action: #selector(MyAccountUserInformationViewController.bookingCheckbox(sender:)), for: .touchUpInside)
            cell.startTime.addTarget(self, action: #selector(MyAccountUserInformationViewController.openStartTime(sender:)), for: .touchUpInside)
            cell.endTime.addTarget(self, action: #selector(MyAccountUserInformationViewController.openEndTime(sender:)), for: .touchUpInside)
            
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == bookingAvailabilityTableview
        {
//            let cell = cell as! BookingAvailabilityTVCTableViewCell
            
            
            
            
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Payment Collectionview Button Action
    
    @objc func setSelection(sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        self.paymentTypeTableview.reloadData()
        
        if "\((self.paymentTypeDataArray[sender.tag] as! NSDictionary)["code"] ?? "")".elementsEqual("B")
        {
            if sender.isSelected == true
            {
                self.bankInformationView.isHidden = false
            }
            else
            {
                self.bankInformationView.isHidden = true
            }
        }
    }
    
    
    
    
    // MARK:- // Booking Checkbox
    
    @objc func bookingCheckbox(sender: UIButton)
    {
        
        self.firstLoad = false
        
        self.bookingCheckboxButtonsArray[sender.tag].isSelected = !self.bookingCheckboxButtonsArray[sender.tag].isSelected
        
        self.bookingCheckboxButtonsArray.removeAll()
        self.bookingAvailabilityTableview.reloadData()
        
        
    }
    
    
    
    // MARK:- // Booking Start Time Button Action
    
    @objc func openStartTime(sender: UIButton)
    {
        
        self.firstLoad = false
        
        self.timezoneClicked = false
        self.startTimeClicked = true
        self.endTimeClicked = false
        
        self.userInformationPickerview.reloadAllComponents()
        
        self.startTimeIndex = sender.tag
        
        UIView.animate(withDuration: 0.5) {
            
            self.viewOfPickerview.isHidden = false
            self.view.bringSubviewToFront(self.viewOfPickerview)
        }
    }
    
    
    
    // MARK:- // Booking End Time Button Action
    
    @objc func openEndTime(sender: UIButton)
    {
        
        self.firstLoad = false
        
        self.timezoneClicked = false
        self.startTimeClicked = false
        self.endTimeClicked = true
        
        self.userInformationPickerview.reloadAllComponents()
        
        self.endTimeIndex = sender.tag
        
        UIView.animate(withDuration: 0.5) {
            
            self.viewOfPickerview.isHidden = false
            self.view.bringSubviewToFront(self.viewOfPickerview)
        }
    }
    
    
    
    
    
    
    // MARK:- // Create Payment Type
    
    func createPaymentType()
    {
        self.paymentTypeTableview.delegate = self
        self.paymentTypeTableview.dataSource = self
        self.paymentTypeTableview.reloadData()
        
        self.paymentTypeTableviewHeightConstraint.constant = (self.paymentTypeTableview.visibleCells[0].bounds.height * CGFloat(self.paymentTypeDataArray.count))
        
        for i in 0..<self.paymentTypeDataArray.count
        {
            if "\((self.paymentTypeDataArray[i] as! NSDictionary)["code"] ?? "")".elementsEqual("B")
            {
                if "\((self.paymentTypeDataArray[i] as! NSDictionary)["status"] ?? "")".elementsEqual("1")
                {
                    self.bankInformationView.isHidden = false
                    
                    self.bankName.text = "\((self.paymentTypeDataArray[i] as! NSDictionary)["bank_name"] ?? "")"
                    self.accountNumber.text = "\((self.paymentTypeDataArray[i] as! NSDictionary)["account_number"] ?? "")"
                }
                else
                {
                    self.bankInformationView.isHidden = true
                }
            }
        }
    }
    
    
    // MARK:- // Set Time Zone on load
    
    func setTimeZoneOnLoad()
    {
        for i in 0..<(self.userInformationAllDataDicitonary["timezone"] as! NSArray).count
        {
            if "\(((self.userInformationAllDataDicitonary["timezone"] as! NSArray)[i] as! NSDictionary)["time_checked_status"] ?? "")".elementsEqual("1")
            {
                self.timezone.text = "\(((self.userInformationAllDataDicitonary["timezone"] as! NSArray)[i] as! NSDictionary)["time_name"] ?? "")"
                
                self.timeZoneID = "\(((self.userInformationAllDataDicitonary["timezone"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")"
            }
        }
        
        
    }
    
    
    func setTableviewHeight()
    {
        
        self.height = 0
        
        // Product Tableview
        self.bookingAvailabilityTableview.frame.size.height = 3000
        
        for cell in self.bookingAvailabilityTableview.visibleCells {
            self.height += cell.bounds.height
        }
        
        self.bookingAvailabilityTableHeightConstraint.constant = self.height
        
        
    }
    
    
    
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func saveButton(_ sender: UIButton) {
        
        var tempStartArray = [String]()
        var tempEndArray = [String]()
        var tempDayPositionArray = [String]()
        
        for i in 0..<self.bookingCheckboxButtonsArray.count
        {
            if self.bookingCheckboxButtonsArray[i].isSelected == true
            {
                tempStartArray.append((self.startTimeButtonsArray[i].titleLabel?.text!)!)
                tempEndArray.append((self.endTimeButtonsArray[i].titleLabel?.text!)!)
                tempDayPositionArray.append("\(i)")
            }
        }
        
        
        self.startTimeString = tempStartArray.joined(separator: ",")
        self.endTimeString = tempEndArray.joined(separator: ",")
        self.dayPositionString = tempDayPositionArray.joined(separator: ",")
        
        var tempPaymentTypeArray = [String]()
        
        for i in 0..<self.paymentTypeButtonsArray.count
        {
            if self.paymentTypeButtonsArray[i].isSelected == true
            {
                tempPaymentTypeArray.append("\((self.paymentTypeDataArray[i] as! NSDictionary)["code"] ?? "")")
            }
        }
        
        self.paymentTypeString = tempPaymentTypeArray.joined(separator: ",")
        
        
        
        self.saveAllData()
        
        self.dispatchGroup.notify(queue: .main) {
            
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            
            self.navigationController?.pushViewController(nav, animated: true)
            
        }
        
        
        
    }
    
    
    
}
