//
//  TextAdvertisementTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 26/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class TextAdvertisementTVCTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var mobileLBL: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var editImageview: UIImageView!
    
    @IBOutlet weak var editAdvertisementButton: UIButton!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
