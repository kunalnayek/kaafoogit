//
//  RestaurantFeaturesViewController.swift
//  Kaafoo
//
//  Created by admin on 25/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestaurantFeaturesViewController: GlobalViewController {

    var allDataDictionary : NSDictionary!

    let dispatchGroup = DispatchGroup()

    var featureImageviewArray = [UIImageView]()

    var selectedID : String!

    var totalExtraFees : Float!


    var mainScroll : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        //scrollView.backgroundColor = .green
        return scrollView
    }()


    var scrollContentview : UIView = {
        let scrollcontentview = UIView()
        scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        //scrollcontentview.backgroundColor = .blue
        return scrollcontentview
    }()


    var allFeaturesView : UIView = {
        let allfeaturesview = UIView()
        allfeaturesview.translatesAutoresizingMaskIntoConstraints = false
        //allfeaturesview.backgroundColor = .red
        return allfeaturesview
    }()

    var saveButton : UIButton = {
        let savebutton = UIButton()
        savebutton.translatesAutoresizingMaskIntoConstraints = false
        savebutton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
        savebutton.addTarget(self, action: #selector(RestaurantFeaturesViewController.save(sender:)), for: .touchUpInside)

        savebutton.backgroundColor = UIColor(red:255/255, green:202/255, blue:0/255, alpha: 1)
        savebutton.setTitleColor(.white, for: .normal)

        return savebutton
    }()



    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.saveButton.titleLabel?.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))

        self.getRestaurantFeatures()

        self.dispatchGroup.notify(queue: .main) {

            self.view.addSubview(self.saveButton)
            self.view.addSubview(self.mainScroll)
            self.mainScroll.addSubview(self.scrollContentview)
            self.scrollContentview.addSubview(self.allFeaturesView)

            self.setupLayout()

        }





    }


    // MARK:- // Setup Layout

    func setupLayout() {

        self.saveButton.anchor(top: nil, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor)

        mainScroll.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor, bottom: self.saveButton.topAnchor, trailing: self.view.trailingAnchor)

        self.scrollContentview.anchor(top: self.mainScroll.topAnchor, leading: self.mainScroll.leadingAnchor, bottom: self.mainScroll.bottomAnchor, trailing: self.mainScroll.trailingAnchor)
        self.scrollContentview.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true

        self.allFeaturesView.anchor(top: self.scrollContentview.topAnchor, leading: self.scrollContentview.leadingAnchor, bottom: self.scrollContentview.bottomAnchor, trailing: self.scrollContentview.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))




        let tempCount = (self.allDataDictionary["info_array"] as! NSArray).count

        for i in 0..<tempCount
        {

            let tempView : UIView = {
                let tempview = UIView()
                tempview.translatesAutoresizingMaskIntoConstraints = false
                //tempview.backgroundColor = .yellow
                return tempview
            }()

            self.allFeaturesView.addSubview(tempView)


            if i == 0
            {
                tempView.anchor(top: self.allFeaturesView.topAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: nil, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }
            else if i == (tempCount - 1)
            {
                tempView.anchor(top: self.allFeaturesView.subviews[i-1].bottomAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: self.allFeaturesView.bottomAnchor, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }
            else
            {
                tempView.anchor(top: self.allFeaturesView.subviews[i-1].bottomAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: nil, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }

            createSingleFeatureView(dataDictionary: (self.allDataDictionary["info_array"] as! NSArray)[i] as! NSDictionary, index: i)

        }



        // Selecting preselected feature
        for i in 0..<(self.allDataDictionary["info_array"] as! NSArray).count {
            if "\(((self.allDataDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["status"] ?? "")".elementsEqual("1") {
                self.featureImageviewArray[i].image = UIImage(named: "radioSelected")
            }
            else {
                self.featureImageviewArray[i].image = UIImage(named: "radioUnselected")
            }
        }



    }



    // MARK:- // Create Single Feature View

    func createSingleFeatureView(dataDictionary : NSDictionary , index : Int) {

        let featureBTN : UIButton = {
            let featurebtn = UIButton()
            featurebtn.translatesAutoresizingMaskIntoConstraints = false
            featurebtn.addTarget(self, action: #selector(RestaurantFeaturesViewController.selectFeature(sender:)), for: .touchUpInside)
            return featurebtn
        }()

        featureBTN.tag = index

        let radioImage : UIImageView = {
            let radioimage = UIImageView()
            radioimage.translatesAutoresizingMaskIntoConstraints = false
            radioimage.image = UIImage(named: "radioUnselected")
            return radioimage
        }()

        self.featureImageviewArray.append(radioImage)

        let titleLBL : UILabel = {
            let titlelbl = UILabel()
            titlelbl.translatesAutoresizingMaskIntoConstraints = false
            titlelbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
            titlelbl.textColor = .darkGray
            titlelbl.textAlignment = .natural
            titlelbl.numberOfLines = 0

            let langID = UserDefaults.standard.string(forKey: "langID")

            if (langID?.elementsEqual("AR"))! {
                titlelbl.text = "\(dataDictionary["price"] ?? "")" + "\(dataDictionary["currency"] ?? "")" + " ,\(dataDictionary["title"] ?? "")"
            }
            else {
                titlelbl.text = "\(dataDictionary["title"] ?? "")" + ", \(dataDictionary["currency"] ?? "")" + "\(dataDictionary["price"] ?? "")"
            }



            return titlelbl
        }()

        let separatorView : UIView = {
            let separatorview = UIView()
            separatorview.translatesAutoresizingMaskIntoConstraints = false
            separatorview.backgroundColor = .lightGray
            return separatorview
        }()

        let optionsView : UIView = {
            let optionsview = UIView()
            optionsview.translatesAutoresizingMaskIntoConstraints = false
            //optionsview.backgroundColor = .cyan
            return optionsview
        }()



        let tempView = self.allFeaturesView.subviews[index]


        tempView.addSubview(titleLBL)
        tempView.addSubview(separatorView)
        tempView.addSubview(radioImage)
        tempView.addSubview(optionsView)
        tempView.addSubview(featureBTN)

        featureBTN.anchor(top: tempView.topAnchor, leading: tempView.leadingAnchor, bottom: tempView.bottomAnchor, trailing: tempView.trailingAnchor)

        radioImage.anchor(top: tempView.topAnchor, leading: tempView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 10, bottom: 0, right: 0), size: .init(width: 20, height: 20))

        titleLBL.anchor(top: radioImage.topAnchor, leading: radioImage.trailingAnchor, bottom: nil, trailing: tempView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 10))

        separatorView.anchor(top: nil, leading: tempView.leadingAnchor, bottom: tempView.bottomAnchor, trailing: tempView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 1))



        optionsView.anchor(top: radioImage.bottomAnchor, leading: titleLBL.leadingAnchor, bottom: tempView.bottomAnchor, trailing: tempView.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 10, right: 10))


        let tempcount = (dataDictionary["content"] as! NSArray).count



        for i in 0..<tempcount {

            let optionLBL : UILabel = {
                let optionlbl = UILabel()
                optionlbl.translatesAutoresizingMaskIntoConstraints = false
                optionlbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
                optionlbl.textColor = .black
                optionlbl.textAlignment = .natural
                optionlbl.numberOfLines = 0

                let tempString = "\(((dataDictionary["content"] as! NSArray)[i] as! NSDictionary)["content"] ?? "")"

                optionlbl.text = tempString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

                return optionlbl
            }()

            optionsView.addSubview(optionLBL)

            if tempcount == 1 {
                optionLBL.anchor(top: optionsView.topAnchor, leading: optionsView.leadingAnchor, bottom: optionsView.bottomAnchor, trailing: optionsView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 5))
            }
            else {
                if i == 0 {
                    optionLBL.anchor(top: optionsView.topAnchor, leading: optionsView.leadingAnchor, bottom: nil, trailing: optionsView.trailingAnchor)
                }
                else if i == tempcount - 1 {
                    optionLBL.anchor(top: optionsView.subviews[i-1].bottomAnchor, leading: optionsView.leadingAnchor, bottom: optionsView.bottomAnchor, trailing: optionsView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 0))
                }
                else {
                    optionLBL.anchor(top: optionsView.subviews[i-1].bottomAnchor, leading: optionsView.leadingAnchor, bottom: nil, trailing: optionsView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
                }
            }

        }

    }


    // MARK:- // Select Feature Button Action

    @objc func selectFeature(sender : UIButton) {

        for i in 0..<self.featureImageviewArray.count {
            self.featureImageviewArray[i].image = UIImage(named: "radioUnselected")
        }
        self.featureImageviewArray[sender.tag].image = UIImage(named: "radioSelected")

        print(sender.tag)

        self.selectedID = "\(((self.allDataDictionary["info_array"] as! NSArray)[sender.tag] as! NSDictionary)["id"] ?? "")"



    }



    // MARK:- // Save Button

    @objc func save(sender : UIButton) {

        self.saveData()

    }














    // MARK: - // JSON POST Method to get Restaurant Features

    func getRestaurantFeatures()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }

        let url = URL(string: GLOBALAPI + "app_restaurent_features")!       //change the url

        print("Features Listing URL : ", url)

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "lang_id=\(langID!)&user_id=\(userID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Load Response: " , json)

                    if (json["response"] as! Bool) == true {

                        self.allDataDictionary = json 

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
        })

        task.resume()



    }





    // MARK: - // JSON POST Method to get Third Category Table Data

    func saveData()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }

        let url = URL(string: GLOBALAPI + "app_restaurent_features_save")!       //change the url

        print("Features Listing URL : ", url)

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&user_feature_id=\(selectedID ?? "")"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Save Response: " , json)

                    if (json["response"] as! Bool) == true {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()

                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController

                            self.navigationController?.pushViewController(navigate, animated: true)
                        }
                    }
                    else {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
        })

        task.resume()



    }


}
