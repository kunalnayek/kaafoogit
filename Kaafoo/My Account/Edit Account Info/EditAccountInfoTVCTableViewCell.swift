//
//  EditAccountInfoTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 19/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class EditAccountInfoTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellStackview: UIStackView!
    @IBOutlet weak var cellTitleLBL: UILabel!
    @IBOutlet weak var cellTitle: UITextField!
    
    @IBOutlet weak var cellDescriptionLBL: UILabel!
    @IBOutlet weak var cellDescription: UITextField!
    
    
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
