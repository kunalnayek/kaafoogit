//
//  Report.swift
//  Kaafoo
//
//  Created by esolz on 22/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation
import UIKit

class Report : UIView
{
    @IBOutlet var ContentView: UIView!
    @IBOutlet weak var ReportLbl: UILabel!
    @IBOutlet weak var SeparatorView: UIView!
    @IBOutlet weak var ReportTxtView: UITextView!
    @IBOutlet weak var SendBtnOutlet: UIButton!
    
    var showReportView : UIView! = UIView()
    @IBOutlet weak var CrossBtnOutlet: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("Report", owner: self, options: nil)
        addSubview(ContentView)
        
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.setLayer()
        CrossBtnOutlet.addTarget(self, action: #selector(HideReport(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //Set Layer For Report TextView
    func setLayer()
    {
        self.ReportTxtView.layer.borderColor = UIColor.black.cgColor
        self.ReportTxtView.layer.borderWidth = 1.5
    }
    
    //MARK:- //Report Hide Button Objective C Target Method
    
    @objc func HideReport(sender : UIButton)
    {
        self.showReportView.isHidden = true
    }
    
    
    
}
