//
//  GlobalViewController.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Alamofire
import PusherSwift
import SVProgressHUD

//struct Connectivity
//{
//    static let sharedInstance = NetworkReachabilityManager()!
//    static var isConnectedToInternet:Bool
//    {
//        return self.sharedInstance.isReachable
//    }
//}

class GlobalViewController: UIViewController, Side_menu_delegate , sort_view_delegate,PusherDelegate,SideButtonDelegate{
    
    let USERID = UserDefaults.standard.string(forKey: "userID")
    
   
    
    var globalJson : NSDictionary!
    
    let documentInteractionController = UIDocumentInteractionController()

    var CatID : String! = ""
    var busssellerID : String! = ""
    
     let CreateBtn1 = UIButton()
     let CreateBtn = UIButton()
     let CustomView = UIView()

    @IBOutlet var headerView: HeaderView!

//    var timer : Timer!

    @IBOutlet var footerView: FooterView!

    @IBOutlet var sideMenuView: SideMenuView!

    @IBOutlet weak var searchView: SearchView!

    @IBOutlet weak var searchViewTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var searchViewBottomConstraint: NSLayoutConstraint!

//    func StartTimer()
//    {
//        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(eventWith(timer:)), userInfo: nil, repeats: true)
//    }
//
//    @objc func eventWith(timer: Timer!) {
//        //        let info = timer.userInfo as Any
//        print("Nothing")
//    }

    let globalDispatchgroup = DispatchGroup()

    @IBOutlet weak var headerSortView: SortView!

    @IBOutlet weak var filterView: FilterView!

    var FullHeight = (UIScreen.main.bounds.size.height)
    
    var FullWidth = (UIScreen.main.bounds.size.width)
    
    var sortTapped : Bool! = false
    
    var sortTapGesture = UITapGestureRecognizer()
    
    var searchOptionOneCommaSeparatedString : String! = ""
    
    var searchOptionTwoCommaSeparatedString : String! = ""
    
    var filterRating : String! = ""
    
    var filterDeliveryType : String! = ""
    
    var maxFilterPrice : String! = ""
    
    var maxFilterDistance : String! = ""
    
    var typeFilter : String! = ""
    
    var filterFilter : String! = ""
    
    var genderFilter : String! = ""
    
    var roleFilter : String! = ""
    
    var jobcatid : String! = ""
    
    var ProductOnMapArray = [MapListing]()

    static let Isiphone6 = (UIScreen.main.bounds.size.height==667) ? true : false
    
    static let Isiphone6Plus = (UIScreen.main.bounds.size.height==736) ? true : false
    
    static let IsiphoneX = (UIScreen.main.bounds.size.height==812) ? true : false
    
    static let IsiphoneEleven = (UIScreen.main.bounds.size.height==896) ? true : false

    //let GLOBALAPI = "http://esolz.co.in/lab6/kaafoo/appcontrol/"
    
   // let GLOBALAPI = "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/"
    
    let GLOBALAPI = "https://kaafoo.com/appcontrol/"


    var storyboardName = UserDefaults.standard.string(forKey: "storyboard")

    var langID : String! = ""
    
    //var countryID : String! = ""
    
    var countryName : String! = ""
    
    var timer = Timer()
    
   // var pusher: Pusher! = nil

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // UserDefaults.standard.set("99", forKey: "countryID")
        
        documentInteractionController.delegate = self
        //        self.StartTimer()
        timer.invalidate()
        
        if UserDefaults.standard.string(forKey: "langID") != nil
        {
            langID = UserDefaults.standard.string(forKey: "langID")
        }
        else
        {
            UserDefaults.standard.set("EN", forKey: "langID")
            
            langID = UserDefaults.standard.string(forKey: "langID")
        }

        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            // do some tasks..
        }
        else
        {
            let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "network_is_not_available", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_try_again_later", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)

        }
        

        // MARK:- // LatestHomeViewController ViewdidLoad

        if (self.topViewController()?.isKind(of: LatestHomeViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "home", comment: "")


            //footerView.isHidden = true

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.headerViewTitle.isHidden = true
            
            headerView.HeaderImageView.isHidden = false

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButton.isHidden = false
            
            headerView.dropDownButtonImage.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = false

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            headerView.dropDownButton.addTarget(self, action: #selector(GlobalViewController.cartClick), for: .touchUpInside)

            if langID!.elementsEqual("AR")
            {
                self.sideMenuView.frame = CGRect(x: self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            else
            {
                self.sideMenuView.frame = CGRect(x: -self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            
            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            
            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)
            
            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)



        }

            // MARK:- // CategoryViewController ViewdidLoad

            // MARK:- // CategoryViewController ViewdidLoad

        else if (self.topViewController()?.isKind(of: CategoryViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            footerView.isHidden = false

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.crossButtonAR.isHidden = true

            // headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)

            headerView.tapSearch.addTarget(self, action: #selector(GlobalViewController.tapHeaderSearch), for: .touchUpInside)

            searchView.closeSearchviewButton.addTarget(self, action: #selector(GlobalViewController.closeHeaderSearch), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)



        }

            // MARK:- // WatchList ViewdidLoad

        else if (self.topViewController()?.isKind(of: WatchListViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "watchlist", comment: "")

            footerView.isHidden = false

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)



        }


            //            // MARK:- // ProductDetails ViewdidLoad
            //
            //        else if (self.topViewController()?.isKind(of: ProductDetailsViewController.self))!
            //        {
            //
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            //
            //            headerView.sideMenuButtonImage.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //            headerView.sideMenuButtonAR.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            //headerView.crossButtonAR.isHidden = false
            //
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //
            //            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //
            //
            //
            //        }

            //            // MARK:- // Product Details All Reviews ViewdidLoad
            //
            //        else if (self.topViewController()?.isKind(of: ProductDetailsAllReviewsViewController.self))!
            //        {
            //
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: "")
            //
            //            headerView.sideMenuButtonImage.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //            headerView.sideMenuButtonAR.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            //headerView.crossButtonAR.isHidden = false
            //
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //
            //            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //
            //
            //
            //        }

            // MARK:- // New Product Details ViewdidLoad

        else if (self.topViewController()?.isKind(of: NewProductDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.ShareBtnOutlet.isHidden = false

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }


            // MARK:- // Recently Viewed ViewdidLoad

        else if (self.topViewController()?.isKind(of: RecentlyViewedViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "recently_viewed", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // My Favourites ViewdidLoad

        else if (self.topViewController()?.isKind(of: MyFavouritesViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_favourites", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }
            
            // Kaafoo bonus viewcontroller
            
            else if (self.topViewController()?.isKind(of: KaafooBonusViewController.self))!
                   {
                       headerView.headerViewTitle.isHidden = false
                       headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Kaafoo Bonus", comment: "")
                     
                     
                       headerView.HeaderImageView.isHidden = true
                       headerView.crossButton.isHidden = false
                       headerView.crossButtonAR.isHidden = true
                       headerView.sideMenuButton.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.sideMenuButtonImage.isHidden = true
                       headerView.headerViewAdditionalImageOne.isHidden = true
                       headerView.headerVIewAdditionalImageTwo.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.dropDownButtonImage.isHidden = true
                       headerView.dropDownButton.isHidden = true
                       
                       headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                      
                      
                       
                   }
            
            // Terms and conditions viewcontroller
                       
                       else if (self.topViewController()?.isKind(of: TermsandconditionsViewController.self))!
                              {
                                  headerView.headerViewTitle.isHidden = false
                                  headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Terms and Conditions", comment: "")
                                
                                
                                  headerView.HeaderImageView.isHidden = true
                                  headerView.crossButton.isHidden = false
                                  headerView.crossButtonAR.isHidden = true
                                  headerView.sideMenuButton.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.sideMenuButtonImage.isHidden = true
                                  headerView.headerViewAdditionalImageOne.isHidden = true
                                  headerView.headerVIewAdditionalImageTwo.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.dropDownButtonImage.isHidden = true
                                  headerView.dropDownButton.isHidden = true
                                  
                                  headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                 
                                 
                                  
                              }
            
            
            // About Us viewcontroller
                                  
                                  else if (self.topViewController()?.isKind(of: AboutusViewController.self))!
                                         {
                                             headerView.headerViewTitle.isHidden = false
                                             headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "About Us", comment: "")
                                           
                                           
                                             headerView.HeaderImageView.isHidden = true
                                             headerView.crossButton.isHidden = false
                                             headerView.crossButtonAR.isHidden = true
                                             headerView.sideMenuButton.isHidden = true
                                             headerView.sideMenuButtonAR.isHidden = true
                                             headerView.sideMenuButtonImage.isHidden = true
                                             headerView.headerViewAdditionalImageOne.isHidden = true
                                             headerView.headerVIewAdditionalImageTwo.isHidden = true
                                             headerView.sideMenuButtonAR.isHidden = true
                                             headerView.dropDownButtonImage.isHidden = true
                                             headerView.dropDownButton.isHidden = true
                                             
                                             headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                            
                                            
                                             
                                         }
            
            
            
            
            
            // privacy policy viewcontroller
                       
                       else if (self.topViewController()?.isKind(of: PrivacypolicyViewController.self))!
                              {
                                  headerView.headerViewTitle.isHidden = false
                                  headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Privacy Policy", comment: "")
                                
                                
                                  headerView.HeaderImageView.isHidden = true
                                  headerView.crossButton.isHidden = false
                                  headerView.crossButtonAR.isHidden = true
                                  headerView.sideMenuButton.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.sideMenuButtonImage.isHidden = true
                                  headerView.headerViewAdditionalImageOne.isHidden = true
                                  headerView.headerVIewAdditionalImageTwo.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.dropDownButtonImage.isHidden = true
                                  headerView.dropDownButton.isHidden = true
                                  
                                  headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                 
                                 
                                  
                              }
            
            // ContactUs viewcontroller
                       
                       else if (self.topViewController()?.isKind(of: ContactUsViewController.self))!
                              {
                                  headerView.headerViewTitle.isHidden = false
                                  headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: "")
                                
                                
                                  headerView.HeaderImageView.isHidden = true
                                  headerView.crossButton.isHidden = false
                                  headerView.crossButtonAR.isHidden = true
                                  headerView.sideMenuButton.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.sideMenuButtonImage.isHidden = true
                                  headerView.headerViewAdditionalImageOne.isHidden = true
                                  headerView.headerVIewAdditionalImageTwo.isHidden = true
                                  headerView.sideMenuButtonAR.isHidden = true
                                  headerView.dropDownButtonImage.isHidden = true
                                  headerView.dropDownButton.isHidden = true
                                  
                                  headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                 
                                 
                                  
                              }
            
            // Help viewcontroller
            
            else if (self.topViewController()?.isKind(of: HelpViewController.self))!
                   {
                      headerView.headerViewTitle.isHidden = false
                         headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Help", comment: "")
                       
                       
                         headerView.HeaderImageView.isHidden = true
                         headerView.crossButton.isHidden = false
                         headerView.crossButtonAR.isHidden = true
                         headerView.sideMenuButton.isHidden = true
                         headerView.sideMenuButtonAR.isHidden = true
                         headerView.sideMenuButtonImage.isHidden = true
                         headerView.headerViewAdditionalImageOne.isHidden = true
                         headerView.headerVIewAdditionalImageTwo.isHidden = true
                         headerView.sideMenuButtonAR.isHidden = true
                         headerView.dropDownButtonImage.isHidden = true
                         headerView.dropDownButton.isHidden = true
                         
                         headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                      
                      
                       
                   }
            
            
            
            // Report viewcontroller
            
            else if (self.topViewController()?.isKind(of: ReportViewController.self))!
                   {
                       headerView.headerViewTitle.isHidden = false
                       headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Report", comment: "")
                     
                     
                       headerView.HeaderImageView.isHidden = true
                       headerView.crossButton.isHidden = false
                       headerView.crossButtonAR.isHidden = true
                       headerView.sideMenuButton.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.sideMenuButtonImage.isHidden = true
                       headerView.headerViewAdditionalImageOne.isHidden = true
                       headerView.headerVIewAdditionalImageTwo.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.dropDownButtonImage.isHidden = true
                       headerView.dropDownButton.isHidden = true
                       
                       headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                      
                      
                       
                   }
            
           
            


            // MARK:- // Items Won ViewdidLoad

        else if (self.topViewController()?.isKind(of: ItemsWonViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_I_won", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Items Lost ViewdidLoad

        else if (self.topViewController()?.isKind(of: ItemsLostViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_lost", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Items I am Selling ViewdidLoad

        else if (self.topViewController()?.isKind(of: ItemsIAmSellingViewController.self))!
        {

          //  headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Items I am Selling", comment: "")
            
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Product Listing", comment: "")


            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }
            
            // MARK:- // RoomListing ViewdidLoad

                  else if (self.topViewController()?.isKind(of: RoomListingViewController.self))!
                  {

                   
                      headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Room List", comment: "")


                   // headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Product Listing", comment: "")


                      footerView.isHidden = true

                      headerView.crossImage.isHidden = true
                      headerView.crossButton.isHidden = true
                      headerView.crossButtonAR.isHidden = true

                      headerView.headerViewAdditionalImageOne.isHidden = true
                      headerView.headerVIewAdditionalImageTwo.isHidden = true

                      headerView.dropDownButtonImage.isHidden = true
                      headerView.dropDownButton.isHidden = true

                      headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                      headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                      sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



                  }




            // MARK:- // Items SOld ViewdidLoad

        else if (self.topViewController()?.isKind(of: ItemsSoldViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sold_items", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Unsold Items ViewdidLoad

        else if (self.topViewController()?.isKind(of: UnsoldItemsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "unsold_items", comment: "")

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Edit Business Account Page One ViewdidLoad

        else if (self.topViewController()?.isKind(of: BEditAccountPOneViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            footerView.isHidden = true

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Edit Business Account Page Two ViewdidLoad

        else if (self.topViewController()?.isKind(of: BEditAccountPTwoViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Edit Business Account Page Three ViewdidLoad

        else if (self.topViewController()?.isKind(of: BEditAccountPThreeViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Edit Business Account Page Four ViewdidLoad

        else if (self.topViewController()?.isKind(of: BEditAccountPFourViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }



            // MARK:- // Location Page ViewdidLoad

        else if (self.topViewController()?.isKind(of: LocationPageViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_address", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Edit Private Account Page One ViewdidLoad

        else if (self.topViewController()?.isKind(of: PEditAccountPOneViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Edit Private Account Page Two ViewdidLoad

        else if (self.topViewController()?.isKind(of: PEditAccountPTwoViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Edit Private Account Page Three ViewdidLoad

        else if (self.topViewController()?.isKind(of: PEditAccountPThreeViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Edit Private Account Page Four ViewdidLoad

        else if (self.topViewController()?.isKind(of: PEditAccountPFourViewController.self))!
        {

            headerView.headerViewTitle.text = ""

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // List An Item ViewdidLoad

        else if (self.topViewController()?.isKind(of: NewPostAdViewController.self))!
        {

            headerView.headerViewTitle.text = "Post Ad"

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }

            // MARK:- // Edit Account Info ViewdidLoad

        else if (self.topViewController()?.isKind(of: EditAccountInfoViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account_info", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // User Information ViewdidLoad

        else if (self.topViewController()?.isKind(of: MyAccountUserInformationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "User_information", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Add Advertisement ViewdidLoad

        else if (self.topViewController()?.isKind(of: AddAdvertisementViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_advertisement", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Restaurant Feature ViewdidLoad

        else if (self.topViewController()?.isKind(of: RestaurantFeaturesViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Restaurant_feature", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Service Feature ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceFeaturesViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service_feature", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Text Advertisement ViewdidLoad

        else if (self.topViewController()?.isKind(of: TextAdvertisementViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Text Advertisement", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // File Verification ViewdidLoad

        else if (self.topViewController()?.isKind(of: FileVerificationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verification_file_upload", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Verification File ViewdidLoad

        else if (self.topViewController()?.isKind(of: VerificationFileViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verification_file_upload", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Past Order ViewdidLoad

        else if (self.topViewController()?.isKind(of: PastOrderViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "buy_now_invoices", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Order Detail ViewdidLoad

        else if (self.topViewController()?.isKind(of: OrderDetailViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_detail", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }
            
            // MARK:- // Kaafoo Bonus ViewdidLoad

               else if (self.topViewController()?.isKind(of: KaafooBonusVC.self))!
               {

                   //headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_detail", comment: "")
                   headerView.headerViewTitle.text = "Kaafoo Bonus"

                   headerView.crossImage.isHidden = true
                   headerView.crossButton.isHidden = true

                   headerView.headerViewAdditionalImageOne.isHidden = true
                   headerView.headerVIewAdditionalImageTwo.isHidden = true

                   headerView.dropDownButtonImage.isHidden = true
                   headerView.dropDownButton.isHidden = true

                   headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                   headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                   sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



               }
            
            // MARK:- // Users holiday Services ViewdidLoad

             else if (self.topViewController()?.isKind(of: UsersHolidayServiceVC.self))!
             {

//                 headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "", comment: "")
                
                headerView.headerViewTitle.text = "Users Holiday Service"

                 headerView.sideMenuButtonImage.isHidden = true
                 headerView.sideMenuButton.isHidden = true
                 headerView.sideMenuButtonAR.isHidden = true

                 headerView.dropDownButtonImage.isHidden = true
                 headerView.dropDownButton.isHidden = true

                 headerView.headerViewAdditionalImageOne.isHidden = true
                 headerView.headerVIewAdditionalImageTwo.isHidden = true

                 headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

                 headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



             }
            
             // MARK:- // Product Query QNAVC ViewdidLoad

                         else if (self.topViewController()?.isKind(of: ProductQueryQNAVC.self))!
                         {
                            
                            headerView.headerViewTitle.text = "Product Query"

                             headerView.sideMenuButtonImage.isHidden = true
                             headerView.sideMenuButton.isHidden = true
                             headerView.sideMenuButtonAR.isHidden = true

                             headerView.dropDownButtonImage.isHidden = true
                             headerView.dropDownButton.isHidden = true

                             headerView.headerViewAdditionalImageOne.isHidden = true
                             headerView.headerVIewAdditionalImageTwo.isHidden = true

                             headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

                             headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



                         }
            
           
            
            // MARK:- // Job Applied Users List ViewDidLoad

            else if (self.topViewController()?.isKind(of: JobAppliedUsersVC.self))!
            {
               
                headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "users_list", comment: "")

                headerView.sideMenuButtonImage.isHidden = true
                headerView.sideMenuButton.isHidden = true
                headerView.sideMenuButtonAR.isHidden = true

                headerView.dropDownButtonImage.isHidden = true
                headerView.dropDownButton.isHidden = true

                headerView.headerViewAdditionalImageOne.isHidden = true
                headerView.headerVIewAdditionalImageTwo.isHidden = true

                headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

                headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



            }



            // MARK:- // Booking Information ViewdidLoad

        else if (self.topViewController()?.isKind(of: BookingInformationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingInformation", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Booking Information Detail ViewdidLoad

        else if (self.topViewController()?.isKind(of: BookingInformationDetailViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingInformation", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }

            // MARK:- // Service Information ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceInformationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceInformation", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)


        }

            // MARK:- // Service Information Detail ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceInformationDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceInformation", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Food Court Information ViewdidLoad

        else if (self.topViewController()?.isKind(of: FoodCourtInformationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodCourtInformation", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)


        }


            // MARK:- // Food Court Information Detail ViewdidLoad

        else if (self.topViewController()?.isKind(of: FoodCourtInformationDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodCourtInformation", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Booking Item ViewdidLoad

        else if (self.topViewController()?.isKind(of: BookingItemViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingItem", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Service Item ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceItemViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service_invoices", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Services Main Screen ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServicesMainViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = false

            headerView.tapSearch.isHidden = false

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.tapSearch.addTarget(self, action: #selector(GlobalViewController.tapHeaderSearch), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // Service Booking Profile ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceBookViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")
            //
            //            footerView.isHidden = true
            //
            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }




            // MARK:- // Service Booking Details ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceBookDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")
            //
            //            footerView.isHidden = true
            //
            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Service Booking Information ViewdidLoad

        else if (self.topViewController()?.isKind(of: serviceBookingInformationViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingInformation", comment: "")

            //footerView.isHidden = true

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true


            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Food Court Main Screen ViewdidLoad

        else if (self.topViewController()?.isKind(of: FoodCourtMainViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodCourt", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.tapSearch.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)


        }
            // MARK:- // Daily Rental ViewdidLoad

        else if (self.topViewController()?.isKind(of: DailyRentalSearchViewController.self))!
        {

            headerView.headerViewTitle.text = ""//LocalizationSystem.sharedInstance.localizedStringForKey(key: "dailyRentals", comment: "")
            headerView.HeaderImageView.isHidden = false


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }


            // MARK:- // Daily Rental Listing ViewdidLoad

        else if (self.topViewController()?.isKind(of: DailyRentalListingViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rentalSearchListing", comment: "")

            headerView.HeaderImageView.isHidden = false
            headerView.headerViewTitle.isHidden = true


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.tapSort.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            //headerView.tapSort.isHidden = false

//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.tapSort.addTarget(self, action: #selector(GlobalViewController.tapHeaderSort), for: .touchUpInside)
//
//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
            
            
            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            
            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)

        }


//            // MARK:- // Daily Rental Product Details ViewdidLoad
//
//        else if (self.topViewController()?.isKind(of: DailyRentalProductDetailsViewController.self))!
//        {
//
//            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
//
//            headerView.sideMenuButtonImage.isHidden = true
//            headerView.sideMenuButton.isHidden = true
//            headerView.sideMenuButtonAR.isHidden = true
//
//            headerView.dropDownButtonImage.isHidden = true
//            headerView.dropDownButton.isHidden = true
//
//            headerView.headerViewAdditionalImageOne.isHidden = true
//            headerView.headerVIewAdditionalImageTwo.isHidden = true
//
//            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
//
//            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
//
//
//
//        }


            // MARK:- // Daily Rental Booking ViewdidLoad

        else if (self.topViewController()?.isKind(of: DailyRentalBookingViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dailyrentalbooking", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // QR Scan ViewdidLoad

        else if (self.topViewController()?.isKind(of: QRScanViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "scanning", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            // MARK:- // QR Scan Info Main ViewdidLoad

        else if (self.topViewController()?.isKind(of: QRScanInfoMainViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "scanning", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }

            // MARK:- // Give Bonus ViewdidLoad

        else if (self.topViewController()?.isKind(of: GiveBonusViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "givebonus", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Pay with Bonus ViewdidLoad

        else if (self.topViewController()?.isKind(of: PayWithBonusViewController.self))!
        {

            //  headerView.headerViewTitle.text = "Pay with Bonus"
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "paywithbonus", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            // MARK:- // Code ID ViewdidLoad

        else if (self.topViewController()?.isKind(of: CodeIDViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "paywithbonus", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }


            //            // MARK:- // Holiday Accomodation Listing ViewdidLoad
            //
            //        else if (self.topViewController()?.isKind(of: HolidayListingViewController.self))!
            //        {
            //
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "hotels", comment: "")
            //
            //
            //            headerView.crossImage.isHidden = true
            //            headerView.crossButton.isHidden = true
            //
            //            headerView.crossButtonAR.isHidden = true
            //
            //            headerView.dropDownButton.isHidden = true
            //            headerView.dropDownButtonImage.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = false
            //
            //            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            //
            //            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            //
            //            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
            //
            //        }


            // MARK:- // Business User Profile ViewdidLoad

        else if (self.topViewController()?.isKind(of: BusinessUserProfileViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }

            // MARK:- // Private User Profile ViewdidLoad

        else if (self.topViewController()?.isKind(of: PrivateUserProfileViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "profile", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }

            // MARK:- // Job First Search Screen ViewdidLoad

        else if (self.topViewController()?.isKind(of: JobFirstSearchScreenViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Jobs", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }

       

            
            // MARK:- // Job Listing ViewdidLoad
            
        else if (self.topViewController()?.isKind(of: NewJobListingViewController.self))!
        {
            
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Jobs", comment: "")
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false
            
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true
            
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
            
            
            
        }
            


      

            // MARK:- // Post Ad ViewdidLoad

        else if (self.topViewController()?.isKind(of: PostAdViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Post Ad", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

        }


            // MARK:- // Daily Deals ViewController ViewdidLoad

        else if (self.topViewController()?.isKind(of: DailyDealsListingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            
            headerView.HeaderImageView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.crossButtonAR.setImage(UIImage(named: "filter"), for: .normal)

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.isHidden = false

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)

            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)


        }


            //            // MARK:- // Daily Deals Details ViewController ViewdidLoad
            //
            //
            //
            //        else if (self.topViewController()?.isKind(of: DailyDealsDetailsViewController.self))!
            //        {
            //
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "daily_deald_details", comment: "")
            //
            //
            //
            //            headerView.crossImage.isHidden = true
            //            headerView.crossButton.isHidden = false
            //            headerView.crossButtonAR.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //        }



            //MARK:- ReviewList ViewController


        else if(self.topViewController()?.isKind(of: ReviewListViewController.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }


            //MARK:- QUESTION AND ANSWER ViewController


            //        else if(self.topViewController()?.isKind(of: QuestionAnswerViewController.self))!
            //        {
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questions_and_answers", comment: "")
            //
            //            headerView.crossImage.isHidden = true
            //            headerView.crossButton.isHidden = false
            //            headerView.crossButtonAR.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //        }


        else if(self.topViewController()?.isKind(of: AddToCartViewController.self))!
        {
            //headerView.headerViewTitle.text = "My Cart"
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_cart", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
            //MARK:- Check Out ViewController ViewDidLoad
            
        else if(self.topViewController()?.isKind(of: CheckOutViewController.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "check_out", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
            //MARK:- Order Placed ViewController ViewDidLoad
            
        else if(self.topViewController()?.isKind(of: OrderPlacedViewController.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_detail", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }


            //            //MARK:- Real Estate Listing ViewController ViewDidLoad
            //        else if(self.topViewController()?.isKind(of: RealEstateListingViewController.self))!
            //        {
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "realestate", comment: "")
            //            headerView.crossImage.isHidden = true
            //            headerView.crossButton.isHidden = false
            //            headerView.crossButtonAR.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //
            //
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //        }


            //            //MARK:- Real Estate Listing Detail ViewController
            //        else if (self.topViewController()?.isKind(of: RealListingDetailViewController.self))!
            //        {
            //
            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            //
            //
            //            headerView.crossImage.isHidden = true
            //            headerView.crossButton.isHidden = false
            //            headerView.crossButtonAR.isHidden = true
            //            headerView.sideMenuButton.isHidden = true
            //
            //            headerView.headerViewAdditionalImageOne.isHidden = true
            //            headerView.headerVIewAdditionalImageTwo.isHidden = true
            //
            //            headerView.dropDownButtonImage.isHidden = true
            //            headerView.dropDownButton.isHidden = true
            //            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            //        }

            // MARK:- // USer Job LIsting ViewController
        else if (self.topViewController()?.isKind(of: JobPostListViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "userjoblisting", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = false

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
//            // MARK:- // Happening Listing ViewController
//        else if (self.topViewController()?.isKind(of: HappeningListingViewController.self))!
//        {
//
//            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "events", comment: "")
//
//            // footerView.isHidden = true
//
//            headerView.crossImage.isHidden = true
//            headerView.crossButton.isHidden = true
//            headerView.headerViewAdditionalImageOne.isHidden = true
//            headerView.headerVIewAdditionalImageTwo.isHidden = true
//            headerView.sideMenuButtonAR.isHidden = true
//            headerView.crossButtonAR.isHidden = true
//
//            headerView.dropDownButtonImage.isHidden = true
//            headerView.dropDownButton.isHidden = true
//
//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
//        }


            // MARK:- // Happening Details ViewController
        else if (self.topViewController()?.isKind(of: HappeningDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventDetails", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }





            // MARK:- // Food Listing ViewController


        else if (self.topViewController()?.isKind(of: FoodListViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodlist", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }
            // MARK:- // Food Listing ViewdidLoad

        else if (self.topViewController()?.isKind(of: FoodListingViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "restaurantDetails", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.tapSearch.isHidden = false

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            //headerView.tapSearch.addTarget(self, action: #selector(GlobalViewController.tapHeaderSearch), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)



        }
            //MARK:- Bons Request ViewController
        else if (self.topViewController()?.isKind(of: BonusRequestViewController.self))!
        {

            //            headerView.headerViewTitle.text = "Bonus Request"

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_detail", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }



            // MARK:- //Holiday Booking List Buying Section ViewController

        else if (self.topViewController()?.isKind(of: HolidaybookingViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidayBooking", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }


            // MARK:- //Event booking Listing Under Buying Section

        else if (self.topViewController()?.isKind(of: EventBookingListingViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventBooking", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }


            // MARK:- //Event booking Listing Details Under Buying Section

        else if (self.topViewController()?.isKind(of: EventBookingListingDetailsViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventbookinginformation", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }


            // MARK:- // Holiday Booking Seller Listing Under Selling section ==Change==

        else if (self.topViewController()?.isKind(of: HolidayBookingSellingListingViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidaybookinginformation", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }


            // MARK:- // Event Booking Seller Listing Under Selling Section

        else if (self.topViewController()?.isKind(of: EventBookingSellerListingViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventbookinginformation", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }

            //MARK:- //Event Booking Seller Listing Details ViewController

        else if (self.topViewController()?.isKind(of: EventBookingSellerListingDetailsViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventbookinginformation", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }

            //NaerBy Location View Controller
        else if (self.topViewController()?.isKind(of: NearByViewController.self))!

        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = false
            headerView.sideMenuButtonAR.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }

            //MARK:- Classified ViewController
            // MARK:- // WatchList ViewdidLoad

        else if (self.topViewController()?.isKind(of: ClassifiedViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "classifiedlist", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }

            //MARK:- MarketPlaceListing ViewController

        else if (self.topViewController()?.isKind(of: MarketPlaceListingViewController.self))!
        {

            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "classifiedlist", comment: "")
            //            headerView.headerViewTitle.text = "MarketPlace"
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.tapSearch.isHidden = false


            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.isHidden = false

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)

            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)

            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)

        }


//            //MARK:- //Hotel Listing Details VieController
//        else if (self.topViewController()?.isKind(of: HotelListingDetailViewController.self))!
//        {
//
//
//            headerView.headerViewTitle.isHidden = true
//            headerView.HeaderImageView.isHidden = false
//            headerView.crossImage.isHidden = true
//            headerView.crossButton.isHidden = true
//
//            headerView.crossButtonAR.isHidden = true
//
//            headerView.dropDownButtonImage.isHidden = true
//            headerView.dropDownButton.isHidden = true
//
//            headerView.headerViewAdditionalImageOne.isHidden = true
//
//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
//        }
            // MARK:- // Services Main Latest Screen ViewdidLoad

        else if (self.topViewController()?.isKind(of: ServiceMainLatestViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")

            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = false

            headerView.tapSearch.isHidden = false

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

//            headerView.tapSearch.addTarget(self, action: #selector(GlobalViewController.tapHeaderSearch), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
            
            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            
            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)



        }
            //MARK:- //New Holiday List View Controller
        else if (self.topViewController()?.isKind(of: NewHolidayListingViewController.self))!
        {

            //            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")

            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            footerView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.tapSearch.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.tapSearch.addTarget(self, action: #selector(GlobalViewController.tapHeaderSearch), for: .touchUpInside)
//
//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)

            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)


        }



            //MARK:- //Image Search Result ViewController
        else if (self.topViewController()?.isKind(of: ImageSearchResultViewController.self))!
        {
            //headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
            //MARK:- //New Real Estate ViewController
        else if (self.topViewController()?.isKind(of: NewRealEstateListingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //            headerView.tapSearch.isHidden = false
            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.isHidden = false

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)

            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)


        }

            // MARK:- // Restaurant Listing Screen ViewdidLoad

        else if (self.topViewController()?.isKind(of: RestaurantListingViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodCourt", comment: "")


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true


            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false


            headerView.headerViewAdditionalImageOne.isHidden = false
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = false

            headerView.tapSearch.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)


            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)

            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)



        }

            // MARK:- // New Real Estate Details ViewdidLoad

        else if (self.topViewController()?.isKind(of: NewRealEstateDetailsViewController.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")

            headerView.sideMenuButtonImage.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            //headerView.crossButtonAR.isHidden = false

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)



        }


            //MARK:- New Real Estate All QUESTION AND ANSWER ViewController


        else if(self.topViewController()?.isKind(of: NewRealEstateAllQNAViewController.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questions_and_answers", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }



            //MARK:- New Product Details All Reviews ViewController


        else if(self.topViewController()?.isKind(of: NewProductDetailsAllReviewsViewController.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: "")

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }

        else if(self.topViewController()?.isKind(of: HappeningSearchViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            //            headerView.crossButtonAR.isHidden = false
            //
            //            headerView.crossButtonAR.setImage(UIImage(named: "filter"), for: .normal)

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }
        else if(self.topViewController()?.isKind(of: RealEstateViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            //            headerView.crossButtonAR.isHidden = false
            //
            //            headerView.crossButtonAR.setImage(UIImage(named: "filter"), for: .normal)

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }
        else if(self.topViewController()?.isKind(of: HolidayAccomodationSearchViewController.self))!
        {

            headerView.headerViewTitle.isHidden = true//LocalizationSystem.sharedInstance.localizedStringForKey(key: "dailyRentals", comment: "")
            headerView.HeaderImageView.isHidden = false


            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

        }
            //MARK:- //New Food Listing ViewController
        else if(self.topViewController()?.isKind(of: NewFoodListingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }

        //MARK:- New Holiday Details ViewController
        else if(self.topViewController()?.isKind(of: NewHolidayDetailsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = true

//            headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            
            
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            
            
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }

        //MARK:- Holiday Room Details ViewController
            
        else if(self.topViewController()?.isKind(of: HolidayRoomDetailsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = true

//            headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            
            
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

//            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
//
//            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
        }
        else if(self.topViewController()?.isKind(of: HolidayRoomDetailsAdvancedPageViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = true

            //            headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }

        else if(self.topViewController()?.isKind(of: NewDailyRentalProductDetailsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = true

            //            headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }

            //MARK:- NewHolidayRoomBooking ViewController

        else if(self.topViewController()?.isKind(of:NewHolidayRoomBookingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = true

            //            headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }

            //MARK:- //NewTextAdvertisementViewController

        else if(self.topViewController()?.isKind(of:NewTextAdvertisementViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
//            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
            headerView.HeaderImageView.isHidden = false

            // headerView.crossImage.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)

            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
        //MARK:- //New Food Court Add to Cart ViewController
            
            
        else if(self.topViewController()?.isKind(of:NewFoodCartViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = "Features"
            headerView.HeaderImageView.isHidden = true
            
    
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //New Happening Listing ViewController
            
            
        else if(self.topViewController()?.isKind(of:NewHappeningListingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = false
            
            
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            
            //MARK:- //Latest News ViewController
            
            
        else if(self.topViewController()?.isKind(of:LatestNewsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = false
            
            
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //Product Query Listing ViewController
            
            
        else if(self.topViewController()?.isKind(of:ProductQueryListingViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.HeaderImageView.isHidden = false

            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
            
            headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
            
            // MARK:- // NearbyVC ViewdidLoad

    else if (self.topViewController()?.isKind(of: NearByVC.self))!
    {
                                                  
          headerView.headerViewTitle.isHidden = true
          headerView.HeaderImageView.isHidden = false

          headerView.crossImage.isHidden = true
          headerView.crossButton.isHidden = true
          headerView.headerViewAdditionalImageOne.isHidden = true
          headerView.headerVIewAdditionalImageTwo.isHidden = true
          headerView.sideMenuButtonAR.isHidden = true
          headerView.dropDownButtonImage.isHidden = true
          headerView.dropDownButton.isHidden = true

          headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

          headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

          sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
          
          headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)


   }
            
            //MARK:- //Job Query Listing ViewController
                   
                   
               else if(self.topViewController()?.isKind(of:JobQueryVC.self))!
               {
                   headerView.headerViewTitle.isHidden = true
                   headerView.HeaderImageView.isHidden = false

                   headerView.crossImage.isHidden = true
                   headerView.crossButton.isHidden = true
                   headerView.headerViewAdditionalImageOne.isHidden = true
                   headerView.headerVIewAdditionalImageTwo.isHidden = true
                   headerView.sideMenuButtonAR.isHidden = true
                   headerView.dropDownButtonImage.isHidden = true
                   headerView.dropDownButton.isHidden = true

                   headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                   headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

                   sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
                   
                   headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
               }
            
            //MARK:- //Job Query QNA Listing ViewController
                
                
            else if(self.topViewController()?.isKind(of:JobQueryQNAVC.self))!
            {
                headerView.headerViewTitle.isHidden = true
                headerView.headerViewTitle.text = ""
                headerView.HeaderImageView.isHidden = false
                headerView.crossButton.isHidden = false
                headerView.crossButtonAR.isHidden = true
                headerView.sideMenuButton.isHidden = true
                headerView.sideMenuButtonAR.isHidden = true
                headerView.sideMenuButtonImage.isHidden = true
                headerView.headerViewAdditionalImageOne.isHidden = true
                headerView.headerVIewAdditionalImageTwo.isHidden = true
                headerView.sideMenuButtonAR.isHidden = true
                headerView.dropDownButtonImage.isHidden = true
                headerView.dropDownButton.isHidden = true
                
                headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            }
            
            
            else if(self.topViewController()?.isKind(of:FoodOrderItemsVC.self))!
                   {
                       headerView.headerViewTitle.isHidden = true
                       headerView.headerViewTitle.text = ""
                       headerView.HeaderImageView.isHidden = false
                       
                       
                       headerView.crossButton.isHidden = false
                       headerView.crossButtonAR.isHidden = true
                       headerView.sideMenuButton.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.sideMenuButtonImage.isHidden = true
                       headerView.headerViewAdditionalImageOne.isHidden = true
                       headerView.headerVIewAdditionalImageTwo.isHidden = true
                       headerView.sideMenuButtonAR.isHidden = true
                       headerView.dropDownButtonImage.isHidden = true
                       headerView.dropDownButton.isHidden = true
                       
                       headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                       
                       //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                   }
        
            //MARK:- //New Job details ViewController
            
            
        else if(self.topViewController()?.isKind(of:NewJobDetailsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = true
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = false
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)


            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
       //MARK:- //Notification Listing ViewController
            
            
        else if(self.topViewController()?.isKind(of:NotificationsViewController.self))!
        {
             headerView.headerViewTitle.isHidden = true
             headerView.HeaderImageView.isHidden = false

             headerView.crossImage.isHidden = true
             headerView.crossButton.isHidden = true
             headerView.headerViewAdditionalImageOne.isHidden = true
             headerView.headerVIewAdditionalImageTwo.isHidden = true
             headerView.sideMenuButtonAR.isHidden = true
             headerView.dropDownButtonImage.isHidden = true
             headerView.dropDownButton.isHidden = true

             headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

             headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

             sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
                       
             headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //New Service Profile ViewController
            
            
        else if(self.topViewController()?.isKind(of:NewServiceProfileVC.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
                                  
                       footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
                                  
                       footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
                       footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)
            
             footerView.clickAdd.addTarget(self, action: #selector(GlobalViewController.AdPostClick), for: .touchUpInside)
        }
        
            //MARK:- //Book A Time  ViewController
            
            
        else if(self.topViewController()?.isKind(of:BookATimeViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //Reviews ViewController
            
            
        else if(self.topViewController()?.isKind(of:ReviewsViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //Contact ViewController
            
            
        else if(self.topViewController()?.isKind(of:ContactViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = ""
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
            //MARK:- //ServiceListingViewController
            
            
        else if(self.topViewController()?.isKind(of:ServiceListingViewController.self))!
       
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "service", comment: "")
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
        //MARK:- //CreditChargingViewController
                
                
            else if(self.topViewController()?.isKind(of:CreditChargingViewController.self))!
            {
                headerView.headerViewTitle.isHidden = false
                headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Wallet", comment: "")
                headerView.HeaderImageView.isHidden = true
                headerView.crossButton.isHidden = false
                headerView.crossButtonAR.isHidden = true
                headerView.sideMenuButton.isHidden = true
                headerView.sideMenuButtonAR.isHidden = true
                headerView.sideMenuButtonImage.isHidden = true
                headerView.headerViewAdditionalImageOne.isHidden = true
                headerView.headerVIewAdditionalImageTwo.isHidden = true
                headerView.sideMenuButtonAR.isHidden = true
                headerView.dropDownButtonImage.isHidden = true
                headerView.dropDownButton.isHidden = true
                
                headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                
                //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            }
            
        //MARK:- //RechargeWalletViewController
            
            
        else if(self.topViewController()?.isKind(of:RechargeWalletViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Wallet", comment: "")
            headerView.HeaderImageView.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
            //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
        }
        
        //MARK:- //ChatViewController
                  
                  
              else if(self.topViewController()?.isKind(of:ChatViewController.self))!
              {
                  headerView.headerViewTitle.isHidden = false
                  headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: "")
                  headerView.HeaderImageView.isHidden = true
                  headerView.crossButton.isHidden = false
                  headerView.crossButtonAR.isHidden = true
                  headerView.sideMenuButton.isHidden = true
                  headerView.sideMenuButtonAR.isHidden = true
                  headerView.sideMenuButtonImage.isHidden = true
                  headerView.headerViewAdditionalImageOne.isHidden = true
                  headerView.headerVIewAdditionalImageTwo.isHidden = true
                  headerView.sideMenuButtonAR.isHidden = true
                  headerView.dropDownButtonImage.isHidden = true
                  headerView.dropDownButton.isHidden = true
                  
                  headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                  
                  //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
              }
        
        //MARK:- //ChatUsersListViewController
                         
                         
                     else if(self.topViewController()?.isKind(of:ChatUserListViewController.self))!
                     {
                         headerView.headerViewTitle.isHidden = false
                         headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: "")
                         headerView.HeaderImageView.isHidden = true
                         headerView.crossButton.isHidden = false
                         headerView.crossButtonAR.isHidden = true
                         headerView.sideMenuButton.isHidden = true
                         headerView.sideMenuButtonAR.isHidden = true
                         headerView.sideMenuButtonImage.isHidden = true
                         headerView.headerViewAdditionalImageOne.isHidden = true
                         headerView.headerVIewAdditionalImageTwo.isHidden = true
                         headerView.sideMenuButtonAR.isHidden = true
                         headerView.dropDownButtonImage.isHidden = true
                         headerView.dropDownButton.isHidden = true
                         
                         headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                         
                         //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                     }
               //MARK:- //ChatDetailsViewController
                                       
                                       
                                   else if(self.topViewController()?.isKind(of:ChatDetailsViewController.self))!
                                   {
                                       headerView.headerViewTitle.isHidden = false
                                       headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: "")
                                       headerView.HeaderImageView.isHidden = true
                                       headerView.crossButton.isHidden = false
                                       headerView.crossButtonAR.isHidden = true
                                       headerView.sideMenuButton.isHidden = true
                                       headerView.sideMenuButtonAR.isHidden = true
                                       headerView.sideMenuButtonImage.isHidden = true
                                       headerView.headerViewAdditionalImageOne.isHidden = true
                                       headerView.headerVIewAdditionalImageTwo.isHidden = true
                                       headerView.sideMenuButtonAR.isHidden = true
                                       headerView.dropDownButtonImage.isHidden = true
                                       headerView.dropDownButton.isHidden = true
                                       
                                       headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                       
                                       //headerView.crossButtonAR.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                   }
        
            
            //MARK:- //Chat User List ViewController

        else if(self.topViewController()?.isKind(of:ChatListViewController.self))!
        {
            headerView.headerViewTitle.isHidden = false
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: "")
            headerView.HeaderImageView.isHidden = true
            headerView.tapSearch.isHidden = false
            headerView.crossButton.isHidden = true
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = false
            headerView.sideMenuButtonAR.isHidden = false
            headerView.sideMenuButtonImage.isHidden = true
            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = false
            headerView.sideMenuButtonAR.isHidden = true
            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
            
           
            
            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)
            
             sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)
            
            footerView.clickHome.addTarget(self, action: #selector(GlobalViewController.homeClick), for: .touchUpInside)
                       
            footerView.clickNotification.addTarget(self, action: #selector(GlobalViewController.notificationClick), for: .touchUpInside)
                       
            footerView.clickDailyDeals.addTarget(self, action: #selector(GlobalViewController.DailydealsClick), for: .touchUpInside)
            footerView.clickMessage.addTarget(self, action: #selector(GlobalViewController.ChatListingClick), for: .touchUpInside)

        }
        
        //MARK:- //ChatDetailsViewController
                                             
                                             
                                         else if(self.topViewController()?.isKind(of:ChatMessageViewController.self))!
                                         {
                                             headerView.headerViewTitle.isHidden = false
                                            // headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "revname", comment: "")
                                           // headerView.headerViewTitle.text = "\((UserDefaults.standard.object(forKey: "revname")) ?? "")"
                                           
                                             headerView.HeaderImageView.isHidden = true
                                             headerView.crossButton.isHidden = false
                                             headerView.crossButtonAR.isHidden = true
                                             headerView.sideMenuButton.isHidden = true
                                             headerView.sideMenuButtonAR.isHidden = true
                                             headerView.sideMenuButtonImage.isHidden = true
                                             headerView.headerViewAdditionalImageOne.isHidden = true
                                             headerView.headerVIewAdditionalImageTwo.isHidden = true
                                             headerView.sideMenuButtonAR.isHidden = true
                                             headerView.dropDownButtonImage.isHidden = true
                                             headerView.dropDownButton.isHidden = true
                                             
                                             //headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
                                            
                                            //crossClickandcallapi
                                                       
                                                        headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClickandcallapi), for: .touchUpInside)
                                             
                                            
                                            
                                         }
              
        //MARK : - AccountSearchListingVC
        
        else if(self.topViewController()?.isKind(of: AccountSearchListingVC.self))!
        {
            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "All Users List", comment: "")
            headerView.crossImage.isHidden = true
            headerView.crossButton.isHidden = false
            headerView.crossButtonAR.isHidden = true
            headerView.sideMenuButton.isHidden = true

            headerView.headerViewAdditionalImageOne.isHidden = true
            headerView.headerVIewAdditionalImageTwo.isHidden = true
             headerView.headerVIewAdditionalImageTwo.isHidden = false

            headerView.dropDownButtonImage.isHidden = true
            headerView.dropDownButton.isHidden = true
            headerView.crossButton.addTarget(self, action: #selector(GlobalViewController.crossClick), for: .touchUpInside)
           
        }
        
        // MARK:- // CommercialAdsListVC
        else if (self.topViewController()?.isKind(of: CommercialAdsListVC.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial ads list", comment: "")


            //footerView.isHidden = true

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.headerViewTitle.isHidden = false
            
            headerView.HeaderImageView.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButton.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            headerView.dropDownButton.addTarget(self, action: #selector(GlobalViewController.cartClick), for: .touchUpInside)

            if langID!.elementsEqual("AR")
            {
                self.sideMenuView.frame = CGRect(x: self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            else
            {
                self.sideMenuView.frame = CGRect(x: -self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            
           
        }
        
        
        // MARK:- // CommercialAds VC
               else if (self.topViewController()?.isKind(of: CommercialAdsVC.self))!
        
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial ads", comment: "")


            //footerView.isHidden = true

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.headerViewTitle.isHidden = false
            
            headerView.HeaderImageView.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButton.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            headerView.dropDownButton.addTarget(self, action: #selector(GlobalViewController.cartClick), for: .touchUpInside)

            if langID!.elementsEqual("AR")
            {
                self.sideMenuView.frame = CGRect(x: self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            else
            {
                self.sideMenuView.frame = CGRect(x: -self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            
           
        }
        
        // MARK:- // SocialMediaVC
        else if (self.topViewController()?.isKind(of: SocialMediaVC.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Social Media", comment: "")


            //footerView.isHidden = true

            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.headerViewTitle.isHidden = false
            
            headerView.HeaderImageView.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButton.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.headerViewAdditionalImageOne.isHidden = true

            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            headerView.dropDownButton.addTarget(self, action: #selector(GlobalViewController.cartClick), for: .touchUpInside)

            if langID!.elementsEqual("AR")
            {
                self.sideMenuView.frame = CGRect(x: self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            else
            {
                self.sideMenuView.frame = CGRect(x: -self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            
           
        }
        
        
        // MARK:- // BonusActivationVC ViewdidLoad

        if (self.topViewController()?.isKind(of: BonusActivationVC.self))!
        {

            headerView.headerViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Bonus activation", comment: "")


          
            headerView.crossImage.isHidden = true
            
            headerView.crossButton.isHidden = true

            headerView.headerViewTitle.isHidden = false
            
            headerView.HeaderImageView.isHidden = true

            headerView.crossButtonAR.isHidden = true

            headerView.dropDownButton.isHidden = true
            
            headerView.dropDownButtonImage.isHidden = true

            headerView.headerVIewAdditionalImageTwo.isHidden = true
            
            headerView.headerViewAdditionalImageOne.isHidden = true


            headerView.sideMenuButton.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            headerView.sideMenuButtonAR.addTarget(self, action: #selector(GlobalViewController.LeftMenuClick), for: .touchUpInside)

            sideMenuView.closeSideMenu.addTarget(self, action: #selector(GlobalViewController.LeftMenuClose), for: .touchUpInside)

            headerView.dropDownButton.addTarget(self, action: #selector(GlobalViewController.cartClick), for: .touchUpInside)

            if langID!.elementsEqual("AR")
            {
                self.sideMenuView.frame = CGRect(x: self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            else
            {
                self.sideMenuView.frame = CGRect(x: -self.FullWidth, y: 0, width: self.FullWidth, height: self.FullHeight)
            }
            
            


        }



        //let langID = UserDefaults.standard.string(forKey: "langID")


        if (langID?.elementsEqual("AR"))!
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft

            self.changeTextfieldAlignment(mainview: self.view, Alignment: .right)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight

            self.changeTextfieldAlignment(mainview: self.view, Alignment: .left)
        }
    }
    
    //MARK:- //View Will Appear Method
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Connectivity.isConnectedToInternet() {
            
            print("Yes! internet is available.")
            // do some tasks..
        }
        else
        {
            let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "network_is_not_available", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_try_again_later", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    //MARK:- //Objective C Method for Timer
    
    @objc func InternetChecking(sender : Timer)
    {
        if Connectivity.isConnectedToInternet()
        {
            //do nothing
        }
        else
        {
            if timer.isValid == true
            {
                self.customAlert(title: "Warning", message: "Please Check Your Internet Connection", completion: {
                    
                    self.timerInvalidate()
                    
                })
            }
            else
            {
                //Do Nothing
            }
        }
    }
    
    //MARK:- //Time Invalidate Function
    func timerInvalidate()
    {
        self.timer.invalidate()
    }
    
    // MARK: - // function TopViewController


    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }


    // MARK:- // Function for header Search

    @objc func tapHeaderSearch()
    {
        UIView.animate(withDuration: 0.5, animations: {

            self.view.bringSubviewToFront(self.searchView)

            //            self.searchView.frame = CGRect(x: 0, y:(70/568)*self.FullHeight, width: UIScreen.main.bounds.size.width, height: (498/568)*self.FullHeight)
            self.searchViewTopConstraint.constant = 70
            self.searchViewBottomConstraint.constant = 0

            self.searchView.searchBar.becomeFirstResponder()

        }, completion: nil)
    }


    // MARK:- // FUnction to Close HeaderSearch

    @objc func closeHeaderSearch() {
        UIView.animate(withDuration: 0.5, animations: {

            self.view.sendSubviewToBack(self.searchView)

            //            self.searchView.frame = CGRect(x: 0, y:(70/568)*self.FullHeight, width: UIScreen.main.bounds.size.width, height: (498/568)*self.FullHeight)
            self.searchViewTopConstraint.constant = self.FullHeight
            self.searchViewBottomConstraint.constant = -self.FullHeight

            self.searchView.searchBar.resignFirstResponder()

        }, completion: nil)
    }




    // MARK:- // Function for header Sort

    @objc func tapHeaderSort()
    {
        //self.headerSortView.Sort_delegate = self

        //
        self.sortTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.sortTapGestureHandler))
        self.headerSortView.addGestureRecognizer(self.sortTapGesture)

        if sortTapped == false
        {
            UIView.animate(withDuration: 0.2, animations: {

                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false

                self.filterView.frame = CGRect(x: 0, y: self.FullHeight - self.filterView.frame.size.height, width: UIScreen.main.bounds.size.width, height: self.filterView.frame.size.height)
               

                self.sortTapped = true

            }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, animations: {

                //self.view.bringSubview(toFront: self.filterView)
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true

                self.filterView.frame = CGRect(x: 0, y: self.FullHeight, width: UIScreen.main.bounds.size.width, height: self.filterView.frame.size.height)
                
                

                self.sortTapped = false

            }, completion: nil)
        }


    }

    // MARK: - Hide View of Pickerview when tapping outside
    @objc func sortTapGestureHandler() {
        UIView.animate(withDuration: 0.2)
        {
            self.view.sendSubviewToBack(self.headerSortView)

            self.headerSortView.frame = CGRect(x: 0, y: self.FullHeight, width: self.headerSortView.frame.size.width, height: self.headerSortView.frame.size.height)

            self.sortTapped = false

        }
    }

    // MARK:- // Navigate to sort Options page

    func navigateToSortOptions()
    {
        print("HI, its working")

        //        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        if (self.topViewController()?.isKind(of: DailyRentalListingViewController.self))!
        {
            /*
             //            let instance = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "dailyRentalListingVC") as! DailyRentalListingViewController

             //let tempArray = UserDefaults.standard.object(forKey: "sortArray")
             let sortingArray = UserDefaults.standard.array(forKey: "sortArray")! as NSArray

             for i in 0..<sortingArray.count
             {
             //var tempArray = [sortClass]()

             if let temp = NSKeyedUnarchiver.unarchiveObject(with: (sortingArray[i] as! NSDictionary)["value"] as! Data) as? [sortClass] {

             temp[i].key
             temp[i].value
             temp[i].isClicked
             }

             for j in 0..<((sortingArray[i] as! NSDictionary)["value"] as! [NSData]).count
             {
             //tempArray.append(filterDataSet(key: "\((((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray)[j] as! NSDictionary)["child_id"]!)", value: "\((((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray)[j] as! NSDictionary)["child_name"]!)", isclicked: false))
             if let loadedPerson = NSKeyedUnarchiver.unarchiveObjectWithData(loadedData) as? [Person] {
             loadedPerson[0].name   //"Leo"
             loadedPerson[0].age    //45
             }


             }

             print("SortArray------",tempArray)
             */
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

            //            let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

            let tempArray = UserDefaults.standard.object(forKey: "sortArray")

            navigate.sortArray = tempArray as? NSMutableArray

            self.navigationController?.pushViewController(navigate, animated: true)


        }

    }




    // MARK: - // Function for LeftMenuClick


    @objc func LeftMenuClick() {

        UIView.animate(withDuration: 0.3, animations: {

            self.sideMenuView.frame = CGRect(x: 0, y:0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

            let deviceLanguageID = UserDefaults.standard.string(forKey: "DeviceLanguageID")

            var isGuest = "1"
            
            if (UserDefaults.standard.string(forKey: "isGuest")?.isEmpty)!
            {
                isGuest = "1"
            }
            else
            {
                isGuest = UserDefaults.standard.string(forKey: "isGuest")!
            }
            
            if (isGuest.elementsEqual("1"))
            {
                self.sideMenuView.postAdButtonOutlet.isHidden = true
                
                self.sideMenuView.collapsibleTableview.isHidden = true
                
                self.sideMenuView.userView.isHidden = true
                
                self.sideMenuView.guestView.isHidden = false

                self.sideMenuView.loginAndRegisterView.isHidden = false
                
            

                if (deviceLanguageID?.elementsEqual("en-US"))!
                {
                    self.sideMenuView.guestImageview.image = UIImage(named: "logo")
                }
                else if (deviceLanguageID?.elementsEqual("en"))!
                {
                    self.sideMenuView.guestImageview.image = UIImage(named: "logo")
                }
                else if (deviceLanguageID?.elementsEqual("AR"))!
                {
                    self.sideMenuView.guestImageview.image = UIImage(named: "kaafoo_logo_arabic")
                }
                else
                {
                    self.sideMenuView.guestImageview.image = UIImage(named: "Kaafoo_text_change")
                }
            }
            else
            {
                self.sideMenuView.postAdButtonOutlet.isHidden = false
                
                self.sideMenuView.collapsibleTableview.isHidden = false
                
                self.sideMenuView.userView.isHidden = false
                
                self.sideMenuView.guestView.isHidden = true
                
                

                self.sideMenuView.loginAndRegisterView.isHidden = true

                let userDisplayName = UserDefaults.standard.string(forKey: "userDisplayName")
                
                let userImageURL = UserDefaults.standard.string(forKey: "userImageURL")

                print("user Display Name : ", userDisplayName!)

                self.sideMenuView.userDisplayName.text = userDisplayName
                
                self.sideMenuView.userImageview.sd_setImage(with: URL(string: userImageURL!))
                
                self.sideMenuView.userImageview.clipsToBounds = true
                
                self.sideMenuView.userImageview.layer.cornerRadius = self.sideMenuView.userImageview.frame.size.height / 2

                self.sideMenuView.tapOnProfile.addTarget(self, action: #selector(GlobalViewController.ProfileClick), for: .touchUpInside)
            }
            
            print("Array Count------",self.sideMenuView.allCategoriesArray.count)
            if self.sideMenuView.allCategoriesArray.count > 0
            
             {
                self.sideMenuView.sideMenuCategoryTableHeightConstraint.constant = (self.sideMenuView.sideMenuCategoryTable.visibleCells[0].bounds.height * CGFloat(self.sideMenuView.allCategoriesArray.count))
                
                self.sideMenuView.ForthTableViewHeightConstraint.constant = self.sideMenuView.ForthTableView.visibleCells[0].bounds.height * CGFloat(self.sideMenuView.ForthTableArray.count)

//                self.sideMenuView.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.sideMenuView.data.count - 2))
                if UserDefaults.standard.string(forKey: "isGuest") == "1"{
                   self.sideMenuView.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.sideMenuView.data.count - 2))
                }
                else{
                    self.sideMenuView.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.sideMenuView.data.count))
                }
            }
            

        }, completion: nil)

        self.sideMenuView.Side_delegate = self

        self.sideMenuView.SideBtnDelegate = self

        self.view.bringSubviewToFront(self.sideMenuView)


    }







    // MARK: - // Function for LeftMenuClose

    @objc func LeftMenuClose() {

        // RTL - Set Frames and Allignments for Arabic and English

        //let langID = UserDefaults.standard.string(forKey: "langID")

        if (langID?.elementsEqual("AR"))!
        {

            UIView.animate(withDuration: 0.3, animations: {


                self.sideMenuView.frame = CGRect(x: +UIScreen.main.bounds.size.width, y:0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            }, completion: nil)

        }
        else
        {

            UIView.animate(withDuration: 0.3, animations: {


                self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            }, completion: nil)

        }

    }


    // MARK:- // Function for CrossClick

    @objc func crossClick() {

        self.navigationController?.popViewController(animated: false)
        
      
        

    }
    
    @objc func crossClickandcallapi() {


        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatlist") as! ChatListViewController
                  self.navigationController?.pushViewController(navigate, animated: true)
        
      
               

       }
    
    //MARK:- //

    // MARK: - // Click on Profile Icon

    @objc func ProfileClick() {

        let userType = UserDefaults.standard.string(forKey: "userType")

        if (userType?.elementsEqual("B"))!
        {

            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            self.navigationController?.pushViewController(object, animated: true)

        }
        else
        {

            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            self.navigationController?.pushViewController(object, animated: true)

        }

    }




    // MARK:- // Footerview Home Button Click


    @objc func homeClick() {

        UIView.animate(withDuration: 0.3, animations: {

            //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            self.navigationController?.pushViewController(navigate, animated: true)

        }, completion: nil)

    }
    
    
    //MARK:- //FooterView Notification Button Click
    
    @objc func notificationClick()
    {
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "notifications") as! NotificationsViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }



    // MARK:- // Footerview Daily Deals Button Click

    @objc func DailydealsClick()
    {
        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailydeals") as! DailyDealsListingViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @objc func ChatListingClick()
    {
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatlist") as! ChatListViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }

    @objc func AdPostClick()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        print("userid=======",userID)
        
        if userID?.isEmpty == true
        {
            
            print("Not Login")
          
           
                       let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""), preferredStyle: .alert)
                       
                       let ok = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:"Ok", comment: ""), style: .default, handler:
                           nil
                       )
                       alert.addAction(ok)
                       
                       self.present(alert, animated: true, completion: nil)
                       
              
            
            
        }
            
        else
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "NewPostAdViewController") as! NewPostAdViewController
            navigate.isNewAdd = true
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }


    // MARK:- // Headerview Home Click

    //    @objc func HomeClick() {
    //
    //        let object = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! HomeViewController
    //        self.navigationController?.pushViewController(object, animated: true)
    //
    //    }


    // MARK:- // Cart Click

    @objc func cartClick()
    {
        //let storyboard = UserDefaults.standard.string(forKey: "storyboard")

        let obj = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController

        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    //MARK :- // SideMenu ForthTableView Click
    
    func forthTable_action(sender: NSInteger?) {
        if sender == 0
        {
            let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "preferenceVC") as! PreferenceViewController
            self.navigationController?.pushViewController(logInView, animated: true)
        }
        else if sender == 1
        {
            let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "preferenceVC") as! PreferenceViewController
            self.navigationController?.pushViewController(logInView, animated: true)
        }
            else if sender == 2
            {
                
            }
            else if sender == 3
            {
                print("kaafoo Bonus")
                          
                          let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "KaafooBonusViewController") as! KaafooBonusViewController
                                     self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            else if sender == 4
            {
                print("Contact us")
                               
                               let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                               self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if sender == 5
            {
                 print("Help")
                              
                              let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
                              self.navigationController?.pushViewController(navigate, animated: true)
               
            }
            else if sender == 6
            {
                 print("Report")
                               
                               let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                               self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if sender == 7
            {
               print("T & C")
                               
                               let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "TermsandconditionsViewController") as! TermsandconditionsViewController
                               self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if sender == 8
            {
                 print("Privacy policy")
                               
                               let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "PrivacypolicyViewController") as! PrivacypolicyViewController
                               self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if sender == 9
            {
                 print("About us")
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if sender == 10
            {
                  print("social media")

                
                
            }
            else if sender == 11
            {
                
                
                 print("facebook")
                
                guard let facebook = URL(string:   "https://www.facebook.com/fb") else { return }
                  UIApplication.shared.open(facebook)
              
            }
            else if sender == 12
            {
                 print("twitter")
                
                guard let twitter = URL(string:"https://twitter.com/tw") else { return }
                  UIApplication.shared.open(twitter)
              
            }
            else if sender == 13
            {
                 print("instagram")
                
                
                
                guard let instagram = URL(string:"https://www.instagram.com/kaafoo_official/?hl=en") else { return }
                  UIApplication.shared.open(instagram)
              
            }
            else if sender == 14
            {
                 print("youtube")
                
                guard let youtube = URL(string:"https://www.youtube.com/channel/UCxqx7h4P6RSRqC9m3UernLA/featured?view_as=public") else { return }
                  UIApplication.shared.open(youtube)
              
            }
        
            else if sender == 15
            
            {
                 print("snapchat")
                
                guard let snapchat = URL(string:"https://kaafoo.com/assets/uploads/deal_upload/Kaafoo_snapchat.png"
                ) else { return }
                UIApplication.shared.open(snapchat)


            }
            else if sender == 16
            {
                 print("share with friends")
                
                let textToShare = "Test"
                    
                       
                       if let myWebsite = NSURL(string: "https://www.google.com/") {
                           let objectsToShare: [Any] = [textToShare, myWebsite]
                           let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                           
                           activityVC.popoverPresentationController?.sourceView = self.view
                           self.present(activityVC, animated: true, completion: nil)
                       }
                
            }
            else if sender == 17
            {
                 print("rate")
                
                
               if let url = URL(string: "itms-apps://itunes.apple.com/app/id" + "com.kaafoo.Kaafoo" ),
                    UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.openURL(url)
                }else{
                    //Just check it on phone not simulator!
                    print("Can not open")
                }
                
            }
            else if sender == 18
            {
                 print("maroof")
                
                guard let maroof = URL(string: "https://www.kaafoo.com/en/") else { return }
                UIApplication.shared.open(maroof)
                
            }
            else if sender == 19
            {
                
                print("maroof")
                               
                               guard let maroof = URL(string: "https://www.kaafoo.com/en/") else { return }
                               UIApplication.shared.open(maroof)
                
            }
            
            else if sender == 20
        {
            print("kaafoo version")
                          
                           guard let kaafoo  = URL(string:  "https://www.kaafoo.com/en/") else { return }
                           UIApplication.shared.open(kaafoo )
        }
            
            else if sender == 21
                   {
                       print("kaafoo version")
                                     
                                      guard let kaafoo  = URL(string:  "https://www.kaafoo.com/en/") else { return }
                                      UIApplication.shared.open(kaafoo )
                   }
            
            else if sender == 22
                              {
                                  print("kaafoo version")
                                                
                                                 guard let kaafoo  = URL(string:  "https://www.kaafoo.com/en/") else { return }
                                                 UIApplication.shared.open(kaafoo )
                              }
           
                                                                                                                       
            
            else if sender == 23
        
            {
                
                //logout
                
               
                
                if (self.topViewController()?.isKind(of: LogInViewController.self))!
                {
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                    {

                            self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

                    }, completion: { (finished: Bool) in
                    })
                }
                else
                {

                    //UserDefaults.standard.removeObject(forKey: "userDisplayName")
                    let UserID = UserDefaults.standard.string(forKey: "userID")
                    let LangID = UserDefaults.standard.string(forKey: "langID")
                    let deviceType : String! = "I"
                    DispatchQueue.main.async {
                        let parameters = "user_id=\(UserID!)&device_type=\(deviceType!)&lang_id=\(LangID!)"
                        self.CallAPI(urlString: "app_logout_api", param: parameters, completion: {
                            self.globalDispatchgroup.leave()
                           // UserDefaults.standard.set("", forKey:"DeviceToken")
                            SVProgressHUD.dismiss()
                        })
                    }

                    let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

                    UserDefaults.standard.set("", forKey: "userID")

                    self.navigationController?.pushViewController(logInView, animated: true)

                }
            }
        
        else
        {
            //do nothing
        }
    }



    // MARK:- // Sidemenu Sections Click


    func action_method(sender: NSInteger?) {

        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        //        if (sender==0)  // Home
        //        {
        //            if (self.topViewController()?.isKind(of: LatestHomeViewController.self))!
        //            {
        //
        //                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
        //                    {
        //
        //                        self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        //
        //                }, completion: { (finished: Bool) in
        //
        //
        //                })
        //
        //            }
        //            else
        //            {
        //
        //                let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
        //
        //
        //                self.navigationController?.pushViewController(logInView, animated: true)
        //
        //            }
        //        }
        //        else
        if (sender==0) // My Account
        {

        }
        else if (sender==1)  // Buying
        {

        }
        else if (sender==17)
        {
            print("About kaafoo")
        }
        else if (sender==4) // Chat
        {
//            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "userjoblisting") as! UserJobListingViewController
//            self.navigationController?.pushViewController(nav, animated: true)
        }
//        else if (sender==16) //chat
//        {
//            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatlist") as! ChatListViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
            
        else if (sender==19)  // Select Language
        {
            //about kaafoo
            
            if (self.topViewController()?.isKind(of: LogInViewController.self))!
            {
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {

                        self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

                }, completion: { (finished: Bool) in
                })
            }
            else
            {

                //UserDefaults.standard.removeObject(forKey: "userDisplayName")
                let UserID = UserDefaults.standard.string(forKey: "userID")
                let LangID = UserDefaults.standard.string(forKey: "langID")
                let deviceType : String! = "I"
                DispatchQueue.main.async {
                    let parameters = "user_id=\(UserID!)&device_type=\(deviceType!)&lang_id=\(LangID!)"
                    self.CallAPI(urlString: "app_logout_api", param: parameters, completion: {
                        self.globalDispatchgroup.leave()
                       // UserDefaults.standard.set("", forKey:"DeviceToken")
                        SVProgressHUD.dismiss()
                    })
                }

                let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

                UserDefaults.standard.set("", forKey: "userID")

                self.navigationController?.pushViewController(logInView, animated: true)

            }
        }
        else if (sender==20)  // Select Language
        {
                
            print("kaafoo Bonus")
            
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "KaafooBonusViewController") as! KaafooBonusViewController
                       self.navigationController?.pushViewController(navigate, animated: true)
            
        }
            else if (sender==21)
            {
                    
                print("Contact us")
                
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else if (sender==22)
            {
                    
                print("Help")
                
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
                self.navigationController?.pushViewController(navigate, animated: true)
                
                
            }
            else if (sender==23)
            {
                    
                print("Report")
                
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            else if (sender==24)
            {
                    
                print("T & C")
                
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "TermsandconditionsViewController") as! TermsandconditionsViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            
            else if (sender==25)
            {
                    
                print("Privacy policy")
                
                let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "PrivacypolicyViewController") as! PrivacypolicyViewController
                self.navigationController?.pushViewController(navigate, animated: true)
                
            }
            else if (sender==26)
            {
                    
                print("About us")
            }
            
        else if (sender==36)
        {
            
            print("language")
                        if (self.topViewController()?.isKind(of: PreferenceViewController.self))!
                        {
            
                            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                                {
            
                                    self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            
                            }, completion: { (finished: Bool) in
            
            
                            })
            
                        }
                        else
                        {
            
                            let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "preferenceVC") as! PreferenceViewController
                            self.navigationController?.pushViewController(logInView, animated: true)
            
                        }
        }
        else if (sender==37)  // Select Country
        {
            print("country")
            
            if (self.topViewController()?.isKind(of: PreferenceViewController.self))!
            {

                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                    {

                        self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

                }, completion: { (finished: Bool) in


                })

            }
            else
            {

                let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "preferenceVC") as! PreferenceViewController
                self.navigationController?.pushViewController(logInView, animated: true)

            }
        }
        else if (sender==35)  // Logout
        {
            
            if (self.topViewController()?.isKind(of: LogInViewController.self))!
            {
                UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {

                        self.sideMenuView.frame = CGRect(x: -UIScreen.main.bounds.size.width, y:0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)

                }, completion: { (finished: Bool) in
                })
            }
            else
            {

                //UserDefaults.standard.removeObject(forKey: "userDisplayName")
                let UserID = UserDefaults.standard.string(forKey: "userID")
                let LangID = UserDefaults.standard.string(forKey: "langID")
                let deviceType : String! = "I"
                DispatchQueue.main.async {
                    let parameters = "user_id=\(UserID!)&device_type=\(deviceType!)&lang_id=\(LangID!)"
                    self.CallAPI(urlString: "app_logout_api", param: parameters, completion: {
                        self.globalDispatchgroup.leave()
                       // UserDefaults.standard.set("", forKey:"DeviceToken")
                        SVProgressHUD.dismiss()
                    })
                }

                let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

                UserDefaults.standard.set("", forKey: "userID")
                // UserDefaults.standard.set("", forKey: "notificationcount")

                self.navigationController?.pushViewController(logInView, animated: true)

            }
        }
    }


    // MARK:- // Sidemenu Delegate for Button clicks

    func sideMenuButtons(sender: NSInteger?) {
        
        if (sender==0)  // Login
        {
            
            //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
            
            let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
            
            self.navigationController?.pushViewController(logInView, animated: true)
            
            
        }
        else if (sender==1)  // Register
        {
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        else if (sender == 4) //Home Button
        {
            let obj = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if (sender==2)  // Snapchat
        {
            
        }
        else if (sender==3)  // Scan QR
        {
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "qrScanVC") as! QRScanViewController
            //            let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "qrScanInfoMainVC") as! QRScanInfoMainViewController
            //
            //            navigate.QRString = "19"
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        else if (sender == 999) //NearBy Section
        {
            //            let obj = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "nearby") as! NearByViewController
            //            self.navigationController?.pushViewController(obj, animated: true)
            
            let obj = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "nearby") as! NearByVC
            
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else   // Post Ad
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "NewPostAdViewController") as! NewPostAdViewController
            navigate.isNewAdd = true
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }

    // MARK:- // Sidemenu Delegate for MarketPlace Category DidSelect

    func categoryDidSelect(typeID : String , categoryID : String , categoryName : String ) {

        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        // let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "categoryVC") as! CategoryViewController

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController
        navigate.typeID = typeID
        //        navigate.categoryID = categoryID
        //        navigate.categoryName = categoryName
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    //FoodCourt page Select
    func foodCourtdidSelect() {
        //let obj = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }

    // MARK:- // Sidemenu Delegate for Services Category DIdselect

    func servicesDidselect(typeID: String, categoryName : String) {


        //        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicesMainVC") as! ServicesMainViewController
        //
        //        navigate.typeID = typeID
        //        navigate.categoryNameToShow = categoryName
        //
        //        self.navigationController?.pushViewController(navigate, animated: true)
        
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicemain") as! ServiceMainLatestViewController

        navigate.typeID = typeID
        navigate.categoryNameToShow = categoryName

        self.navigationController?.pushViewController(navigate, animated: true)

    }

    func NearBy()
    {
        let obj = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "nearby") as! NearByViewController
        self.navigationController?.pushViewController(obj, animated: true)
    }

    // MARK:- // Sidemenu Delegate for Daily Deals Category DIdselect

    func dailyDealsDidSelect() {

        //        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailydeals") as! DailyDealsListingViewController

        self.navigationController?.pushViewController(navigate, animated: true)
    }



    // MARK:- // Sidemenu Delegate for Food Court Category DIdselect

    func foodCourtDidSelect(typeID: String) {
        //let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController
        print("typeID",typeID)
        navigate.searchCategoryID = typeID

        self.navigationController?.pushViewController(navigate, animated: true)



    }

    func HappeningDidSelect()
    {
        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningsearch") as! HappeningSearchViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    func ClassifiedDidSelect()
    {
        
        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "textad") as! NewTextAdvertisementViewController
        self.navigationController?.pushViewController(nav, animated: true)
        
        
//        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "classified") as! ClassifiedViewController
//        self.navigationController?.pushViewController(nav, animated: true)

//        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "textad") as! NewTextAdvertisementViewController
//        self.navigationController?.pushViewController(nav, animated: true)
        
        
        /*
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        print("userid=======",userID)
        
        if userID?.isEmpty == true
        {
//            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
            
            let logInView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

           // UserDefaults.standard.set("", forKey: "userID")

            self.navigationController?.pushViewController(logInView, animated: true)
        }
        else
        {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "textAdvertisementVC") as! TextAdvertisementViewController

        self.navigationController?.pushViewController(navigate, animated: true)
        }
 
            */

    }


    // MARK:- // Sidemenu Delegate for Holiday Accomodation Didselect

    func holidayAccomodationDidSelect() {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "holidayAccomodationSearchVC") as! HolidayAccomodationSearchViewController

        //        self.navigationController?.present(navigate, animated: true, completion: nil)

        self.navigationController?.pushViewController(navigate, animated: true)

        //        if let vc = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "holidayAccomodationSearchVC") as? HolidayAccomodationSearchViewController
        //        {
        //            self.navigationController?.present(vc, animated: true, completion: nil)
        //        }
    }


    // MARK:- // Sidemenu Delegate for Daily Rental Didselect

    func dailyRentalDidSelect(productID: String) {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalSearchVC") as! DailyRentalSearchViewController

        navigate.productID = productID

        self.navigationController?.pushViewController(navigate, animated: true)

    }


    // MARK:- // Sidemenu Delegate for Job Didselect

    func jobDidSelect() {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobFirstSearchScreenVC") as! JobFirstSearchScreenViewController

        //navigate.typeID = typeID

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    //MARK:- Sidemenu DelegateFor Real EState Didselect
    func RealEstateDidSelect() {

        //        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        //        let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "realestate") as! RealEstateViewController
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "realestate") as! RealEstateViewController

        //        navigate.productID = productID

        self.navigationController?.pushViewController(navigate, animated: true)

    }

    @objc func MarketPlace(sender: UIButton) {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
   

    // MARK:- // My Account Section


    func myAccount_section(sender: NSInteger?) {
        

        if (sender==1)   // Edit Account
        {
            let userType = "\(UserDefaults.standard.string(forKey: "userType")!)"

            if userType.elementsEqual("B")
            {

                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPOneVC") as! BEditAccountPOneViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else
            {

                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pEditAccountPOneVC") as! PEditAccountPOneViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }

        }
            else if (sender==0)   // profile Account 
            {
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController

                self.navigationController?.pushViewController(navigate, animated: true)
            }
        else if (sender==2)   // Edit Account Info
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "editAccountInfoVC") as! EditAccountInfoViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==3)   // Verification File
        {
            //let navigate = UIStoryboard(name: "MyAccount", bundle: nil).instantiateViewController(withIdentifier: "fileVerificationVC") as! FileVerificationViewController
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "verificationFileVC") as! VerificationFileViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==4)   // User Information
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "myAccountUserInformationVC") as! MyAccountUserInformationViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
//        else if (sender==4)   // Text Advertisement
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "textAdvertisementVC") as! TextAdvertisementViewController
//
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
            else if (sender==5)   // Social media
            {
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "SocialMediaVC") as! SocialMediaVC

                self.navigationController?.pushViewController(navigate, animated: true)
            }
        else if (sender==15)   // Add Advertisement
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addAdvertisementVC") as! AddAdvertisementViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==6)   // Restaurant Feature
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "restaurantFeaturesVC") as! RestaurantFeaturesViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==7)   // Service Feature
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceFeaturesVC") as! ServiceFeaturesViewController

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==8) // Category Section
        {
            //Show tableView
        }
        
        

    }
    
     // MARK:- // BonusActivation Section
    
    func bonusActivation_section(sender: NSInteger?)
    {
        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "BonusActivationVC") as! BonusActivationVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    

    // MARK:- // Buying Section


    func buying_section(sender: NSInteger?) {


        if (sender==0)   // Watchlist
        {
            let watchListView = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "watchListVC") as! WatchListViewController
            self.navigationController?.pushViewController(watchListView, animated: true)
        }

        else if (sender==1)   // My Favourites List
        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsWonVC") as! ItemsWonViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "myFavouritesVC") as! MyFavouritesViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }

        else if (sender==2)     // Recently Viewed Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "recentlyViewedVC") as! RecentlyViewedViewController
            self.navigationController?.pushViewController(navigate, animated: true)
            
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsLostVC") as! ItemsLostViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
        }

//        else if (sender==3)    // My Favourites
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "myFavouritesVC") as! MyFavouritesViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==4)    // Recently Viewed
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "recentlyViewedVC") as! RecentlyViewedViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==5)    // Past Order
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pastOrderVC") as! PastOrderViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==6)    // Booking Information
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingInformationVC") as! BookingInformationViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==7)    // Service Information
//        {
//            let navigate = UIStoryboard(name : "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceInformationVC") as! ServiceInformationViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==8)    // Food Court Information
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtInformationVC") as! FoodCourtInformationViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==10)    // Holiday booking Information
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "holidaybooking") as! HolidaybookingViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }
//        else if (sender==9)  // Event Booking
//        {
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookinglisting") as! EventBookingListingViewController
//            self.navigationController?.pushViewController(navigate, animated: true)
//        }


    }
    
    
    //MARK:- //MarketPlace Section
    
    func myAccount_marketplace_Section(sender: NSInteger?) {
        
        if (sender==0) //Items I am Selling Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "9"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Items I Won Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsWonVC") as! ItemsWonViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) //Items I Lost Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsLostVC") as! ItemsLostViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==3) // Sold Items Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsSoldVC") as! ItemsSoldViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==4) //Unsold Items section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "unsoldItemsVC") as! UnsoldItemsViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==5) //Buy now Invoices section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "pastOrderVC") as! PastOrderViewController
            navigate.CateGoryID = "9"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            //do nothing
        }
    }
    
    //MARK:- //My Account Daily Rental Section
    
    func myAccount_dailyRental_Section(sender: NSInteger?) {
        
        if (sender==0) //Listed daily rental
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "15"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Booked daily rental Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingInformationVC") as! BookingInformationViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) //daily rental booking section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingItemVC") as! BookingItemViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            
        }
    }
    
    //MARK:- //My Account Holiday Accommodation Section
    
    func myAccount_HolidayAccommodation_Section(sender: NSInteger?) {
        
        if (sender==0)  //Listed Accommodation
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "16"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Holiday booking (Selling section)
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookinglistingseller") as! HolidayBookingSellingListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) //Holiday Booking Information (Buying section)
        {
           
        }
        else if (sender==3) //Users Holiday Features
        {
            
        }
        else
        {
           //do nothing
        }
    }
    
    //MARK:- //My Account Wallet Section
    
    func myAccount_account_wallet_Section(sender: NSInteger?)
    {
              if (sender==0) //Credit Charging Section
               {
                   let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "credit") as! CreditChargingViewController
                   self.navigationController?.pushViewController(navigate, animated: true)
               }
               else if (sender==1) //
               {
                   let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "bonusvc") as! KaafooBonusVC
                   self.navigationController?.pushViewController(navigate, animated: true)
               }
               else if (sender == 2)
              {
                
              }
        
    }

    
    //MARK:- //My Account Happening Section
    
    func myAccount_Happening_Section(sender: NSInteger?) {
        
        if (sender==0) //Listed Happening Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "14"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Event Booking Information
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "eventbookingsellerlisting") as! EventBookingSellerListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2)  //Booked Events Information
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookinglisting") as! EventBookingListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            //do nothing
        }
    }

    //MARK:- //My Account Food Court section
    
    func myAccount_FoodCourt_Section(sender: NSInteger?) {
        
        if (sender==0) //Food_list Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "13"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Food Court Orders
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtInformationVC") as! FoodCourtInformationViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) //Food Order Items
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "orderitems") as! FoodOrderItemsVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==3) //Users Restaurent Features
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "restaurantFeaturesVC") as! RestaurantFeaturesViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            //do nothing
        }
    }
    
    //MARK:- //My Account Commercial section
       
       func myAccount_Commercial_Section(sender: NSInteger?) {
           
           if (sender==0) //Commercial ads
           {
               let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "CommercialAdsVC") as! CommercialAdsVC
              
               self.navigationController?.pushViewController(navigate, animated: true)
           }
           else if (sender==1) //cmmercial ad list
           {
               let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "CommercialAdsListVC") as! CommercialAdsListVC
               self.navigationController?.pushViewController(navigate, animated: true)
           }
           
           else
           {
               //do nothing
           }
       }
       
    
    //MARK:- //My Account Service Section
    
    func myAccount_Service_Section(sender: NSInteger?) {
        
        if (sender==0)  //Listed service section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "11"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1) //Service Information
        {
            let navigate = UIStoryboard(name : "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceInformationVC") as! ServiceInformationViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) //Service Invoices
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceItemVC") as! ServiceItemViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==3) //Users Holiday Services
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "userholidayVC") as! UsersHolidayServiceVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            //do nothing
        }
    }
    
    //MARK:- My Account Real Estate Section
    
    func myAccount_Real_Estate_Section(sender: NSInteger?) {
        
        if (sender==0) //Listed Daily rental Section
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            navigate.CategoryId = "999"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    //MARK:- //Select All Category
    
    func SelectAllCategory() {
        MarketPlaceParameters.shared.setMarketPlaceCategory(selectcategory: "All")
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController
        navigate.typeID = ""
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    func Selectservicecategory() 
    {
         let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicemain") as! ServiceMainLatestViewController

        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    func Selectfoodcourtcategory()
    {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController
            
        navigate.searchCategoryID = ""

        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    func Selectclassifiedcategory()
    {
         let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "textad") as! NewTextAdvertisementViewController
               self.navigationController?.pushViewController(nav, animated: true)
    }
   
    
    
    //MARK:- //Text Advertisement Section
    
    func myAccount_Advertsiement_Section(sender: NSInteger?) {
        
        if (sender==0)
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "textAdvertisementVC") as! TextAdvertisementViewController
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1)
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addAdvertisementVC") as! AddAdvertisementViewController
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            
        }
    }
    
    //MARK:- //Latest News My Account Section
    
    func myAccount_Latest_news() {
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "latestnewsVC") as! LatestNewsViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    //MARK:- //My Account Query Section
    
    func myAccount_Query_Section(sender: NSInteger?) {
        if (sender==0)
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "productQuery") as! ProductQueryListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else  if (sender == 1)
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobquery") as! JobQueryVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    //MARK:- //chat Section
    
    func chat_Section(sender: NSInteger?) {
        
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatlist") as! ChatListViewController
            self.navigationController?.pushViewController(navigate, animated: true)
      
    }
    
    //MARK:- //My Account Notification Section
    
    func myAccount_notification() {
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "notifications") as! NotificationsViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }


    // MARK:- // Selling Section


    func selling_section(sender: NSInteger?) {

        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        if (sender==0) // List An Item
        {
            //            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdVC") as! PostAdViewController
            //            self.navigationController?.pushViewController(navigate, animated: true)

            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "NewPostAdViewController") as! NewPostAdViewController
            navigate.isNewAdd = true
            UserDefaults.standard.setValue(false, forKey: "autoLoad")

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==1)  // Items I am Selling
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsIAmSellingVC") as! ItemsIAmSellingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==2) // Sold Items
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "itemsSoldVC") as! ItemsSoldViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==3)  // Unsold Items
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "unsoldItemsVC") as! UnsoldItemsViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==4)  // Booking Item
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingItemVC") as! BookingItemViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==5)  // Service Item
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceItemVC") as! ServiceItemViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==6)  // Bonus Req
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bonusreq") as! BonusRequestViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }

        else if (sender==7)  // FoodList
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodlist") as! FoodListViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==8)  // Event Booking ViewController
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "eventbookingsellerlisting") as! EventBookingSellerListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if (sender==9)  // Holiday booking SellerListing ViewController
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookinglistingseller") as! HolidayBookingSellingListingViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }

        else
        {

        }

    }
    
    
    // MARK:- // Job Section
    
    func job_section(sender: NSInteger?) {
        
        if (sender==0) { // Job Post
            
        }
        else {  // Job Post List
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "jobPostListVC") as! JobPostListViewController
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        
    }
    

    // MARK:- // Function Get_Fontsize


    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size

        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }
        else if(GlobalViewController.IsiphoneX)
        {
            size1 += 3.0
        }

        return size1
    }



    // guest

    func isGuest() {


        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

        UserDefaults.standard.set("GUEST", forKey: "userDisplayName")

        self.navigationController?.pushViewController(navigate, animated: true)


    }

    // MARK:- // Function to set Attributed Title Bold and Normal

    func setAttributedTitle(toLabel: UILabel,boldText: String,boldTextFont: UIFont,normalTextFont: UIFont,normalText: String)
    {
        let boldText  = boldText
        let boldAttrs = [NSAttributedString.Key.font : boldTextFont]
        let normalAttrs = [NSAttributedString.Key.font : normalTextFont]

        let attributedString = NSMutableAttributedString(string:boldText, attributes:boldAttrs)

        attributedString.append(NSMutableAttributedString(string:normalText, attributes:normalAttrs))

        //assign it to a label:
        toLabel.attributedText = attributedString
    }


    // MARK:- // Show Toast Message


    func displayToastMessage(_ message : String) {

        let toastView = UILabel()
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.white
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 25
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        let window = UIApplication.shared.delegate?.window!
        window?.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)

        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 275)

        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==100)]-68-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }


    // MARK:- // ShadowView class

    class ShadowView: UIView {
        override var bounds: CGRect {
            didSet {
                setupShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
            }
        }

        private func setupShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
            self.layer.cornerRadius = 8
            layer.shadowColor = color.cgColor
            self.layer.shadowOffset = offSet
            self.layer.shadowRadius = radius
            self.layer.shadowOpacity = opacity
            self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
    }


    /*

     // MARK:- // SortView class

     class FilterView: UIView {
     override var bounds: CGRect {
     didSet {
     setupFilterView()
     }
     }

     private func setupFilterView() {

     self.backgroundColor = UIColor(red:65/255, green:65/255, blue:65/255, alpha: 0.85)

     //            self.layer.cornerRadius = 8
     //            layer.shadowColor = color.cgColor
     //            self.layer.shadowOffset = offSet
     //            self.layer.shadowRadius = radius
     //            self.layer.shadowOpacity = opacity
     //            self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
     //            self.layer.shouldRasterize = true
     //            self.layer.rasterizationScale = UIScreen.main.scale
     }
     }

     */


    // MARK:- // Create Sort OPtions VIew

    func createFilterView(filterArray: [String],toAddInView: UIView,fontLabel: UILabel)
    {
        var tempOrigin : CGFloat! = 0

        for i in 0..<filterArray.count
        {
            let tempView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))

            let tempLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (135/320)*self.FullWidth, height: (25/568)*self.FullHeight))

            tempLBL.text = filterArray[i]

            tempLBL.font = UIFont(name: fontLabel.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

            let tempBTN = UIButton(frame: CGRect(x: 0, y: 0, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))

            tempBTN.tag = i

            //tempBTN.addTarget(self, action: #selector(self.startSorting(sender:)), for: .touchUpInside)

            tempView.addSubview(tempLBL)
            tempView.addSubview(tempBTN)

            toAddInView.addSubview(tempView)

            tempOrigin = tempOrigin + (35/568)*self.FullHeight

        }

        toAddInView.clipsToBounds = true

        toAddInView.autoresizesSubviews = false

        toAddInView.frame.size.height = tempOrigin

        toAddInView.autoresizesSubviews = true

        toAddInView.layer.cornerRadius = (5/568)*self.FullHeight

    }

    // MARK:- // Validation Custom Alert Function

    func customAlert(title: String,message: String, completion : @escaping () -> ())
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in

            UIView.animate(withDuration: 0.5)
            {
                completion()
            }

        }

        //let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
       // alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )
    }

    // MARK:- // Change TextField Alignment

    func changeTextfieldAlignment(mainview: UIView, Alignment: NSTextAlignment)
    {
        for view in mainview.subviews
        {

            if view.isKind(of: UIView.self)
            {
                self.changeTextfieldAlignment(mainview: view, Alignment: Alignment)
            }

            if view.isKind(of: UITextField.self)
            {
                (view as! UITextField).textAlignment = Alignment
            }
        }
    }

    // MARK:- // Make Asterisk (*) beside mandatory labels

    func asterixText(withText: String) -> NSMutableAttributedString
    {
        let passwordAttriburedString = NSMutableAttributedString(string: withText)
        let asterix = NSAttributedString(string: "*", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        passwordAttriburedString.append(asterix)

        return passwordAttriburedString
    }

    func CreateFavouriteView(inview : UIView )
    {
       
        
        CustomView.translatesAutoresizingMaskIntoConstraints = false
        
        inview.addSubview(CustomView)
        
        CustomView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        CustomView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        CustomView.trailingAnchor.constraint(equalTo: inview.trailingAnchor, constant: -10).isActive = true
        
        if (self.topViewController()?.isKind(of: MarketPlaceListingViewController.self))!
        {
            CustomView.topAnchor.constraint(equalTo: inview.topAnchor, constant: 150).isActive = true
        }
        else
        {
            CustomView.topAnchor.constraint(equalTo: inview.topAnchor, constant: 120).isActive = true
        }
        CustomView.backgroundColor = .green
        
        CustomView.layer.cornerRadius = 10
        
        inview.bringSubviewToFront(CustomView)

        CustomView.isUserInteractionEnabled = true

       
        
        CreateBtn.translatesAutoresizingMaskIntoConstraints = false
        
        CustomView.addSubview(CreateBtn)
        
        CreateBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn.centerXAnchor.constraint(equalTo: CustomView.centerXAnchor, constant: 0).isActive = true
        
        CreateBtn.centerYAnchor.constraint(equalTo: CustomView.centerYAnchor, constant: 0).isActive = true
        
        CreateBtn.setImage(UIImage(named: "heart"), for: .normal)
        
        CreateBtn.titleLabel?.textColor = .white
        
        CreateBtn.isUserInteractionEnabled = true

        if (self.topViewController()?.isKind(of: DailyDealsListingViewController.self))!
        {
            self.CatID = "10"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: MarketPlaceListingViewController.self))!
        {
            self.CatID = "9"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: NewRealEstateListingViewController.self))!
        {
            self.CatID = "999"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: NewHolidayListingViewController.self))!
        {
            self.CatID = "16"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: RestaurantListingViewController.self))!
        {
            self.CatID = "13"
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: DailyRentalListingViewController.self))!
        {
            self.CatID = "15"
           
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: ServiceMainLatestViewController.self))!
        {
            self.CatID = "11"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: ClassifiedViewController.self))!
        {
            self.CatID = "1122"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: NewJobListingViewController.self))!
        {
            self.CatID = "12"
            
            CreateBtn.addTarget(self, action: #selector(AddTargetToFavourite(sender:)), for: .touchUpInside)
        }
    }
    func RemoveFavouriteView(inview : UIView )
    {
       
        
        CustomView.translatesAutoresizingMaskIntoConstraints = false
        
        inview.addSubview(CustomView)
        
        CustomView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        CustomView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        CustomView.trailingAnchor.constraint(equalTo: inview.trailingAnchor, constant: -10).isActive = true
        
        if (self.topViewController()?.isKind(of: MarketPlaceListingViewController.self))!
        {
            CustomView.topAnchor.constraint(equalTo: inview.topAnchor, constant: 150).isActive = true
        }
        else
        {
            CustomView.topAnchor.constraint(equalTo: inview.topAnchor, constant: 120).isActive = true
        }
        CustomView.backgroundColor = .green
        
        CustomView.layer.cornerRadius = 10
        
        inview.bringSubviewToFront(CustomView)

        CustomView.isUserInteractionEnabled = true

//        let CreateBtn = UIButton()
//
//        CreateBtn.translatesAutoresizingMaskIntoConstraints = false
//
//        CustomView.addSubview(CreateBtn)
//
//        CreateBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
//
//        CreateBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
//
//        CreateBtn.centerXAnchor.constraint(equalTo: CustomView.centerXAnchor, constant: 0).isActive = true
//
//        CreateBtn.centerYAnchor.constraint(equalTo: CustomView.centerYAnchor, constant: 0).isActive = true
//
//        CreateBtn.setImage(UIImage(named: "heart"), for: .normal)
//
//
//        CreateBtn.titleLabel?.textColor = .white
//
//        CreateBtn.isUserInteractionEnabled = true

       
        
        CreateBtn1.translatesAutoresizingMaskIntoConstraints = false
        
        CustomView.addSubview(CreateBtn1)
        
        CreateBtn1.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn1.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn1.centerXAnchor.constraint(equalTo: CustomView.centerXAnchor, constant: 0).isActive = true
        
        CreateBtn1.centerYAnchor.constraint(equalTo: CustomView.centerYAnchor, constant: 0).isActive = true
        
        CreateBtn1.setImage(UIImage(named: "redheart"), for: .normal)
        
        CreateBtn1.titleLabel?.textColor = .white
        
        CreateBtn1.isUserInteractionEnabled = true

        if (self.topViewController()?.isKind(of: DailyDealsListingViewController.self))!
        {
            self.CatID = "10"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: MarketPlaceListingViewController.self))!
        {
            self.CatID = "9"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: NewRealEstateListingViewController.self))!
        {
            self.CatID = "999"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: NewHolidayListingViewController.self))!
        {
            self.CatID = "16"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: RestaurantListingViewController.self))!
        {
            self.CatID = "13"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: DailyRentalListingViewController.self))!
        {
            self.CatID = "15"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: ServiceMainLatestViewController.self))!
        {
            self.CatID = "11"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }
        else if (self.topViewController()?.isKind(of: ClassifiedViewController.self))!
        {
            self.CatID = "1122"
            
            CreateBtn1.addTarget(self, action: #selector(RemoveTargetFromFavourite(sender:)), for: .touchUpInside)
        }

    }

    //MARK:- //Add Target For Favourite Button
    @objc func AddTargetToFavourite(sender: UIButton)
    {
        
        CreateBtn1.isHidden = false
        CreateBtn1.translatesAutoresizingMaskIntoConstraints = false
               
               CustomView.addSubview(CreateBtn1)
               
               CreateBtn1.widthAnchor.constraint(equalToConstant: 20).isActive = true
               
               CreateBtn1.heightAnchor.constraint(equalToConstant: 20).isActive = true
               
               CreateBtn1.centerXAnchor.constraint(equalTo: CustomView.centerXAnchor, constant: 0).isActive = true
               
               CreateBtn1.centerYAnchor.constraint(equalTo: CustomView.centerYAnchor, constant: 0).isActive = true
               
               CreateBtn1.setImage(UIImage(named: "redheart"), for: .normal)
               
               CreateBtn1.titleLabel?.textColor = .white
               
               CreateBtn1.isUserInteractionEnabled = true
        CreateBtn.isHidden = true
        
        if USERID?.isEmpty == true
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            self.globalDispatchgroup.enter()

            DispatchQueue.main.async {
                SVProgressHUD.show()
            }

            let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url

            print("Place Bid URL : ", url)

            var parameters : String = ""
            let langID = UserDefaults.standard.string(forKey: "langID")
            let userID = UserDefaults.standard.string(forKey: "userID")

            parameters = "user_id=\(userID!)&cat_id=\(CatID!)&lang_id=\(langID!)"

            print("Parameters are : " , parameters)

            let session = URLSession.shared

            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST

            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)


            }
            //        catch let error {
            //            print(error.localizedDescription)
            //        }

            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {
                    return
                }

                guard let data = data else {
                    return
                }

                do {

                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("Add To Category" , json)

                        DispatchQueue.main.async {

                            self.globalDispatchgroup.leave()

                            SVProgressHUD.dismiss()

                        }

                        self.globalDispatchgroup.notify(queue: .main, execute: {
                            if "\(json["response"]! as! Bool)".elementsEqual("true")
                            {
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                            }
                            else
                            {
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                            }

                        })
                    }

                } catch let error {
                    print(error.localizedDescription)
                }
            })

            task.resume()
        }
        
    }

    //MARK:- //Remove Target From Favourite Button
    @objc func RemoveTargetFromFavourite(sender: UIButton)
    {
        
        CreateBtn1.isHidden = true
        CreateBtn.isHidden = false
        CreateBtn.translatesAutoresizingMaskIntoConstraints = false
        
        CustomView.addSubview(CreateBtn)
        
        CreateBtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        CreateBtn.centerXAnchor.constraint(equalTo: CustomView.centerXAnchor, constant: 0).isActive = true
        
        CreateBtn.centerYAnchor.constraint(equalTo: CustomView.centerYAnchor, constant: 0).isActive = true
        
        CreateBtn.setImage(UIImage(named: "heart"), for: .normal)
        
        CreateBtn.titleLabel?.textColor = .white
        
        CreateBtn.isUserInteractionEnabled = true
        
        self.globalDispatchgroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url

        print("Place Bid URL : ", url)

        var parameters : String = ""
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&cat_id=\(CatID!)&lang_id=\(langID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }
        //        catch let error {
        //            print(error.localizedDescription)
        //        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove Category" , json)

                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()

                    }

                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        if "\(json["response"]! as! Bool)".elementsEqual("true")
                        {
                            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                        }
                        else
                        {
                            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                        }

                    })
                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()
    }

   /* //MARK:- //Add Target For Favourite Button DailyRental
       @objc func AddTargetToFavouriteDailyRental(sender: UIButton)
       {
           if USERID?.isEmpty == true
           {
               self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
           }
           else
           {
               self.globalDispatchgroup.enter()

               DispatchQueue.main.async {
                   SVProgressHUD.show()
               }

               let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url

               print("Place Bid URL : ", url)

               var parameters : String = ""
               let langID = UserDefaults.standard.string(forKey: "langID")
               let userID = UserDefaults.standard.string(forKey: "userID")

               parameters = "user_id=\(userID!)&buss_seller_id=\(busssellerID!)&lang_id=\(langID!)"

               print("Parameters are : " , parameters)

               let session = URLSession.shared

               var request = URLRequest(url: url)
               request.httpMethod = "POST" //set http method as POST

               do {
                   request.httpBody = parameters.data(using: String.Encoding.utf8)


               }
               //        catch let error {
               //            print(error.localizedDescription)
               //        }

               let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                   guard error == nil else {
                       return
                   }

                   guard let data = data else {
                       return
                   }

                   do {

                       if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                           print("Add To Category" , json)

                           DispatchQueue.main.async {

                               self.globalDispatchgroup.leave()

                               SVProgressHUD.dismiss()

                           }

                           self.globalDispatchgroup.notify(queue: .main, execute: {
                               if "\(json["response"]! as! Bool)".elementsEqual("true")
                               {
                                   self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                               }
                               else
                               {
                                   self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                               }

                           })
                       }

                   } catch let error {
                       print(error.localizedDescription)
                   }
               })

               task.resume()
           }
           
       }
    
    
    //MARK:- //Remove Target From Favourite ButtonDailyRental
       @objc func RemoveTargetFromFavouritedailyrental(sender: UIButton)
       {
           self.globalDispatchgroup.enter()

           DispatchQueue.main.async {
               SVProgressHUD.show()
           }

           let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url

           print("Place Bid URL : ", url)

           var parameters : String = ""
           let langID = UserDefaults.standard.string(forKey: "langID")
           let userID = UserDefaults.standard.string(forKey: "userID")

           parameters = "user_id=\(userID!)&buss_seller_id=\(busssellerID!)&lang_id=\(langID!)"

           print("Parameters are : " , parameters)

           let session = URLSession.shared

           var request = URLRequest(url: url)
           request.httpMethod = "POST" //set http method as POST

           do {
               request.httpBody = parameters.data(using: String.Encoding.utf8)


           }
           //        catch let error {
           //            print(error.localizedDescription)
           //        }

           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

               guard error == nil else {
                   return
               }

               guard let data = data else {
                   return
               }

               do {

                   if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                       print("Remove Category" , json)

                       DispatchQueue.main.async {

                           self.globalDispatchgroup.leave()

                           SVProgressHUD.dismiss()

                       }

                       self.globalDispatchgroup.notify(queue: .main, execute: {
                           if "\(json["response"]! as! Bool)".elementsEqual("true")
                           {
                               self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                           }
                           else
                           {
                               self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "\(json["message"]!)")
                           }

                       })
                   }

               } catch let error {
                   print(error.localizedDescription)
               }
           })

           task.resume()
       } */

    //MARK:- // Global APi Call

    func CallAPI(urlString : String,param : String, completion : @escaping () -> ())
    {

        
        
        //UIApplication.shared.beginIgnoringInteractionEvents()
        self.globalDispatchgroup.enter()


        SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }

        if Connectivity.isConnectedToInternet()
        {
            DispatchQueue.main.async {
            }

            let  urlstr = URL(string: GLOBALAPI + urlString)!   //change the url

            let parameters = param

            print("Parameters are : " , parameters)
            print("URL : ", "\(urlstr)" + "?" + parameters)


            let session = URLSession.shared

            var request = URLRequest(url: urlstr)
            request.httpMethod = "POST" //set http method as POST

            do {
                request.httpBody = param.data(using: String.Encoding.utf8)
            }

            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {

                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()
//                                                UIApplication.shared.endIgnoringInteractionEvents()

                    }

                    return
                }

                guard let data = data else {

                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()
//                                                UIApplication.shared.endIgnoringInteractionEvents()

                    }

                    return
                }

                do {

                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("JSon Response:========= " , json)

                        if "\(json["response"] as! Bool)".elementsEqual("true")
                        {
                            self.globalJson = json.copy() as? NSDictionary

                            completion()
                            
//                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                        else
                        {
                            print("Unsuccessful Block")

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()

                                SVProgressHUD.dismiss()
                                
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), comment: ""), message: "\(json["message"] ?? "")")
//                                UIApplication.shared.endIgnoringInteractionEvents()

                            }
                        }
                    }

                } catch let error {

                    print(error.localizedDescription)

                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()
                        

                    }
                }
            })
            task.resume()
            
            DispatchQueue.main.async {
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        else
        {
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                UIApplication.shared.endIgnoringInteractionEvents()

            }

            ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), comment: ""), message: "Network is not available!Please try again later")
        }

    }


    //MARK:- Global Add to Watchlist API method
    func GlobalAddtoWatchlist(ProductID : String, completion : @escaping () -> ())

    {
        if Connectivity.isConnectedToInternet()
        {
            self.globalDispatchgroup.enter()

            DispatchQueue.main.async {
                SVProgressHUD.show()
            }

            //            UIApplication.shared.beginIgnoringInteractionEvents()

            let userID = UserDefaults.standard.string(forKey: "userID")
            let langID = UserDefaults.standard.string(forKey: "langID")


            let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url

            var parameters : String = ""

            parameters = "user_id=\(userID!)&lang_id=\(langID!)&add_product_id=\(ProductID)"

            print("Save Service Booking URL is : ",url)
            print("Parameters are : " , parameters)

            let session = URLSession.shared

            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST

            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)


            }
            //            catch let error {
            //                print(error.localizedDescription)
            //            }

            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {
                    return
                }

                guard let data = data else {
                    return
                }

                do {

                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("Add to Watchlist Response: " , json)

                        if "\(json["response"]! as! Bool)".elementsEqual("true")
                        {

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()
                                SVProgressHUD.dismiss()
                                //                                UIApplication.shared.endIgnoringInteractionEvents()
                                //self.ShowAlertMessage(title: "Add to watchlist Response", message: "\(json["message"]!)")
                                
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_to_watchlist_response", comment: ""), message: "\(json["message"]!)")
                            }
                                    completion()
                        }
                        else
                        {

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()
                                SVProgressHUD.dismiss()
                                //                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_to_watchlist_response", comment: ""), message: "\(json["message"]!)")
                            }
                        }

                    }

                } catch let error {
                    print(error.localizedDescription)
                }
            })

            task.resume()

        }
        else
        {
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
                //                UIApplication.shared.endIgnoringInteractionEvents()
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "network_is_not_available", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_try_again_later", comment: ""))

            }
        }


    }
    //MARK:- Global Remove From Watchlist API method
    func GlobalRemoveFromWatchlist(ProductID : String, completion : @escaping () -> ())

    {
        if Connectivity.isConnectedToInternet()
        {
            self.globalDispatchgroup.enter()

            DispatchQueue.main.async {
                SVProgressHUD.show()
            }

            //            UIApplication.shared.beginIgnoringInteractionEvents()

            let userID = UserDefaults.standard.string(forKey: "userID")
            let langID = UserDefaults.standard.string(forKey: "langID")


            let url = URL(string: GLOBALAPI + "app_remove_watchlist")!   //change the url

            var parameters : String = ""

            parameters = "user_id=\(userID!)&lang_id=\(langID!)&remove_product_id=\(ProductID)"

            print("Remove From Watchlist URL is : ",url)
            print("Parameters are : " , parameters)

            let session = URLSession.shared

            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST

            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)


            }
            //            catch let error {
            //                print(error.localizedDescription)
            //            }

            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {
                    return
                }

                guard let data = data else {
                    return
                }

                do {

                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("Remove From Watchlist Response: " , json)

                        if "\(json["response"]! as! Bool)".elementsEqual("true")
                        {

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()
                                SVProgressHUD.dismiss()
                                //                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "remove_from_watchlist_response", comment: ""), message: "\(json["message"]!)")
                            }
                            completion()

                        }
                        else
                        {

                            DispatchQueue.main.async {

                                self.globalDispatchgroup.leave()
                                SVProgressHUD.dismiss()
                                //                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "remove_from_watchlist_response", comment: ""), message: "\(json["message"]!)")
                            }
                        }

                    }

                } catch let error {
                    print(error.localizedDescription)
                }
            })

            task.resume()

        }
        else
        {
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
                //                UIApplication.shared.endIgnoringInteractionEvents()
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "network_is_not_available", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_try_again_later", comment: ""))

            }
        }


    }
    //MARK:- //Show Custom Alert Function
    func ShowAlertMessage(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil )

    }

    //No Label Function Found
    func noDataFound(mainview : UIView)
    {
        let NoLbl = UILabel()
        
        mainview.addSubview(NoLbl)
        
        NoLbl.translatesAutoresizingMaskIntoConstraints = false
        
        NoLbl.text = "No Data Found"
        
        NoLbl.centerXAnchor.constraint(equalTo: mainview.centerXAnchor).isActive = true
        
        NoLbl.centerYAnchor.constraint(equalTo: mainview.centerYAnchor).isActive = true
    }
    
    
    
    // MARK:- // Check if a key is present in Userdefaults
    
    public func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    //MRK:- // Color Extension for RGB
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- Valid Email Checking
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    //MARK:- //Calculate Difference Bewteen Two Dates From Calendar
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        
        let calendar = Calendar.current
        
        let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
        
        return components.day!
    }
    
    
    func createMultiFontLabel( ColorOne : UIColor , ColorTwo : UIColor , textOne : String , textTwo : String , fontOneSize : CGFloat , fontTwoSize : CGFloat) ->  NSAttributedString
    {
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: fontOneSize), NSAttributedString.Key.foregroundColor : ColorOne]

        let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: fontTwoSize), NSAttributedString.Key.foregroundColor : ColorTwo]

        let attributedString1 = NSMutableAttributedString(string: "\(textOne)", attributes:attrs1)

        let attributedString2 = NSMutableAttributedString(string: "\(textTwo)", attributes:attrs2)

        attributedString1.append(attributedString2)
        
        return attributedString1
    }

}
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

extension UIView {

    func fillSuperview() {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor)
    }

    func anchorSize(to view: UIView) {
        
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }

    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        
        translatesAutoresizingMaskIntoConstraints = false

        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }

        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }

        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }

        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }

        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }

        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}
// MARK:- // ShadowView class
class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3)
        }
    }
    private func setupShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        
        self.layer.cornerRadius = 8
        
        layer.shadowColor = color.cgColor
        
        self.layer.shadowOffset = offSet
        
        self.layer.shadowRadius = radius
        
        self.layer.shadowOpacity = opacity
        
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
extension UIColor
{
    class func colorPrimary() -> UIColor
    {
        return UIColor(red: 63.0/255.0, green: 181.0/255.0, blue: 81.0/255.0, alpha:1.0)
    }

    class func colorPrimaryDark() -> UIColor
    {
        return UIColor(red: 48.0/255.0, green: 63.0/255.0, blue: 159.0/255.0, alpha:1.0)
    }
    class func colorAccent() -> UIColor
    {
        return UIColor(red: 255.0/255.0, green: 64.0/255.0, blue: 129.0/255.0, alpha:1.0)
    }
    class func white() -> UIColor
    {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha:1.0)
    }
    class func yellow1() -> UIColor
    {
        return UIColor(red: 255.0/255.0, green: 199.0/255.0, blue: 15.0/255.0, alpha:1.0)
    }
    class func brown1() -> UIColor
    {
        return UIColor(red: 35.0/255.0, green: 0/255.0, blue: 0/255.0, alpha:1.0)
    }
    class func seaGreen() -> UIColor
    {
        return UIColor(red: 46.0/255.0, green: 139.0/255.0, blue: 87.0/255.0, alpha:1.0)
    }
    class func darkgray() -> UIColor
    {
        return UIColor(red: 169.0/255.0, green: 169.0/255.0, blue: 169.0/255.0, alpha:1.0)
    }
    class func lightgray() -> UIColor
    {
        return UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha:1.0)
    }
    class func drawerBrownDark() -> UIColor
    {
        return UIColor(red: 198.0/255.0, green: 146.0/255.0, blue: 49.0/255.0, alpha:1.0)
    }
    class func drawerBrownCenter() -> UIColor
    {
        return UIColor(red: 195.0/255.0, green: 149.0/255.0, blue: 47.0/255.0, alpha:1.0)
    }
    class func drawerBrownLight() -> UIColor
    {
        return UIColor(red: 217.0/255.0, green: 183.0/255.0, blue: 95.0/255.0, alpha:1.0)
    }
    class func red() -> UIColor
    {
        return UIColor(red: 255.0/255.0, green: 0/255.0, blue: 0/255.0, alpha:1.0)
    }
    class func darkred() -> UIColor
    {
        return UIColor(red: 190.0/255.0, green: 4.0/255.0, blue: 4.0/255.0, alpha:1.0)
    }
    class func green() -> UIColor
    {
        return UIColor(red: 0.0/255.0, green: 230.0/255.0, blue: 115.0/255.0, alpha:1.0)
    }
    class func black() -> UIColor
    {
        return UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha:1.0)
    }
    class func blue() -> UIColor
    {
        return UIColor(red: 131.0/255.0, green: 149.0/255.0, blue: 245.0/255.0, alpha:1.0)
    }
    class func seablue() -> UIColor
    {
        return UIColor(red: 34.0/255.0, green: 138.0/255.0, blue: 207.0/255.0, alpha:1.0)
    }
    class func lightgraymore() -> UIColor
    {
        return UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha:1.0)
    }
    class func yellow2() -> UIColor
    {
        return UIColor(red: 255.0/255.0, green: 171.0/255.0, blue: 1.0/255.0, alpha:1.0)
    }
    class func c_green() -> UIColor
    {
        return UIColor(red: 86.0/255.0, green: 108.0/255.0, blue: 105.0/255.0, alpha:1.0)
    }
    class func morelightgray() -> UIColor
    {
        return UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha:1.0)
    }
    class func s_green() -> UIColor
    {
        return UIColor(red: 70.0/255.0, green: 199.0/255.0, blue: 111.0/255.0, alpha:1.0)
    }
    class func colorTextDark() -> UIColor
    {
        return UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha:1.0)
    }
    class func colorDarkGray() -> UIColor
    {
        return UIColor(red: 193.0/255.0, green: 191.0/255.0, blue: 191.0/255.0, alpha:1.0)
    }
    class func adpostNextBtnColor() -> UIColor
    {
        return UIColor(red: 250.0/255.0, green: 170.0/255.0, blue: 13.0/255.0, alpha:1.0)
    }
    class func ShadowDark() -> UIColor
    {
        return UIColor(red: 238.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha:1.0)
    }
    class func LeafGreen() -> UIColor
    {
        return UIColor(red: 29.0/255.0, green: 199.0/255.0, blue: 105.0/255.0, alpha:1.0)
    }
    class func HolidayGreen() -> UIColor
    {
        return UIColor(red: 0.0/255.0, green: 218.0/255.0, blue: 98.0/255.0, alpha:1.0)
    }
    class func DeepWhite() -> UIColor
    {
        return UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha:1.0)
    }


}
//MARK:- //Set UIButton Background Color In Swift
extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}
// MARK:- // Get Height/Width of a given String
extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
// MARK:- // Half Colored Text in a UILABEL
extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String , color : UIColor ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        self.attributedText = attribute
    }
}
//MARK:- // Add Corner Radius
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

//extension UINavigationController
//{
//    func navigateToPageUsingPush(storyboardname : String , Identifier : String , ViewControllerName : UINavigationController)
//    {
//        let nav = UIStoryboard(name: storyboardname, bundle: nil).instantiateViewController(withIdentifier: Identifier) as!
//    }
//}

//MARK:- //Remove Html Tags
extension String{
    var htmlStripped : String{
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}


//MARK:- UIcolor Picker Using HEXACode

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

//MARK:- Date Difference Extension Irrespective of 24 Hours .

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}

//MARK:- Get Current Time and Date

extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "dd/MM/yyyy"

        return dateFormatter.string(from: Date())

    }
    static func getCurrentTime() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "HH:mm:ss"

        return dateFormatter.string(from: Date())

    }
}

//MARK:- //URL Validation Using NSDataDetector

extension String {
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
}

//MARK:- //Phone Number Validation in Swift

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

//MARK :- //GlobalView Controller

extension GlobalViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
            } catch {
                print(error)
            }
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                SVProgressHUD.dismiss()
                self.share(url: tmpURL)
            }
            }.resume()
    }
}

extension GlobalViewController: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}


extension String {
  func replace(string:String, replacement:String) -> String {
      return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
      return self.replace(string: " ", replacement: "")
  }
}
