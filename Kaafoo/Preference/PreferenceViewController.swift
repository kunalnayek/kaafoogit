//
//  PreferenceViewController.swift
//  Kaafoo
//
//  Created by admin on 20/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation
import MapKit

class PreferenceViewController: GlobalViewController {
    
    let dispatchGroup = DispatchGroup()
    
    var MyCountryId : String! = ""
    
    var locationManager: CLLocationManager! = CLLocationManager()
    
    var Latitude : Double!
    
    var Longitude : Double!
    
    @IBOutlet weak var BackBtnOutlet: UIButton!
    
    @IBAction func BackBtn(_ sender: UIButton) {

    }
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var chooseCountryLBL: UILabel!
    
    @IBOutlet weak var countryLbl: UILabel!
    
    @IBOutlet weak var chooseLanguageLBL: UILabel!
    
    @IBOutlet weak var languageLbl: UILabel!
    
    @IBOutlet weak var countryView: UIView!
    
    @IBOutlet weak var languageView: UIView!
    
    @IBOutlet weak var countryTableView: UITableView!
    
    @IBOutlet weak var languageTableView: UITableView!
    
    
    @IBOutlet weak var viewOfCountryTable: UIView!
    
    @IBOutlet weak var viewOfCountryTableHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewOfLanguageTable: UIView!
    
    @IBOutlet weak var viewOfLanguageTableHeightConstraint: NSLayoutConstraint!

    var languageList = ["ARABIC","ENGLISH"]
    
    var allCountryListDictionary : NSDictionary!

    var countryID : String!

    var selectedCountry : String!
    
    var selectedLanguage : String!
    
    var tempArray : NSArray!
    
    
    
    // MARK:- // View Did Load
    var customView = UIView()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //-Custom View
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight))
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        
        self.dispatchGroup.enter()
        
       
        
//        self.countryTableView.delegate = self
//
//        self.countryTableView.dataSource = self
//
//        self.countryTableView.reloadData()
      
         self.getCountryData()
        
        self.dispatchGroup.leave()

        self.globalDispatchgroup.notify(queue: .main) {
            
            DispatchQueue.main.async {
                
                if CLLocationManager.locationServicesEnabled() == true
                {
                    self.locationManager.pausesLocationUpdatesAutomatically = false
                    
                    self.locationManager.requestWhenInUseAuthorization()
                    
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    
                    self.locationManager.startUpdatingLocation()
                    
                    self.locationManager.delegate = self
                }
                else
                {
                    print("Do Nothing")
                }
            }
            
            self.viewOfCountryTable.layer.cornerRadius = 8
            
            self.viewOfLanguageTable.layer.cornerRadius = 8
            
            self.setStaticString()
            
            self.selectedLanguage = UserDefaults.standard.string(forKey: "myLanguage")
            
            self.selectedCountry = UserDefaults.standard.string(forKey: "myCountryName")
            
            
            if self.selectedLanguage == nil
            {
                print("No Previously Selected Language")
                
                self.languageLbl.text = ""
            }
            else
            {
                self.languageLbl.text = self.selectedLanguage
            }
            
            self.languageTableView.delegate = self
            
            self.languageTableView.dataSource = self
            
            //self.saveOutlet.layer.cornerRadius = self.saveOutlet.frame.size.height / 2
            
            if self.selectedCountry == nil
            {
                print("No Previously Selected country")
                
                self.countryLbl.text = ""
                 
            }
            else
            {
                self.countryLbl.text = self.selectedCountry
            }
            
        }
        
        
    }
    
    
    // View will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.countryTableView.estimatedRowHeight = 100
        
        self.countryTableView.rowHeight = UITableView.automaticDimension
        
        self.languageTableView.estimatedRowHeight = 100
        
        self.languageTableView.rowHeight = UITableView.automaticDimension
        
    }
    
    
    // MARK:- // Set Country
    
    func setCountry() {
        
        self.selectedCountry = UserDefaults.standard.string(forKey: "myCountryName")
        self.countryID = UserDefaults.standard.string(forKey: "countryID")
        
        
        if self.selectedCountry  == nil
        {
            print("No Previously Selected Country")
            
            self.countryLbl.text = ""
            
            self.getAddressFromLatLong(pdblLatitude: "\(self.Latitude!)", withLongitude: "\(self.Longitude!)")
            
        }
        else
        {
            self.countryLbl.text = self.selectedCountry
            
            
        }
        
    }
    
    
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Save Button
    
    @IBOutlet weak var saveOutlet: UIButton!
    @IBAction func saveButton(_ sender: Any) {
        
        if (countryLbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "No Country Selected", message: "Please Select a Country to Continue", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            
            if (languageLbl.text?.isEmpty)!
            {
                let alert = UIAlertController(title: "No Language Selected", message: "Please Select a Language to Continue", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else
            {
                
                print("Country Label Text",countryLbl.text ?? "")
                
                UserDefaults.standard.set(countryLbl.text, forKey: "myCountryName")
                UserDefaults.standard.set(countryID, forKey: "countryID")
                
               
                
                let SelectedCountryName = UserDefaults.standard.string(forKey: "myCountryName")
                
                print("SelectedCountryName========",SelectedCountryName ?? "")
                
                
                    
                   if  SelectedCountryName == "KUWAIT"
                   {
                       UserDefaults.standard.set(245, forKey: "countryID")
                   }
                    else if  SelectedCountryName == "SAUDI ARABIA"
                              {
                                  UserDefaults.standard.set(187, forKey: "countryID")
                              }
                       else if  SelectedCountryName == "INDIA"
                                  {
                                      UserDefaults.standard.set(99, forKey: "countryID")
                                  }
                         else  if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "EGYPT"
                                      {
                                          UserDefaults.standard.set(63, forKey: "countryID")
                                      }
                    else if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "الكويت"
                              {
                                  UserDefaults.standard.set(245, forKey: "countryID")
                              }
                               else if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "المملكة العربية السعودية"
                                         {
                                             UserDefaults.standard.set(187, forKey: "countryID")
                                         }
                                  else if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "الهند"
                                             {
                                                 UserDefaults.standard.set(99, forKey: "countryID")
                                             }
                                    else  if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "مصر"
                                                 {
                                                     UserDefaults.standard.set(63, forKey: "countryID")
                                                 }
                    else if UserDefaults.standard.string(forKey: "countryID") != nil && SelectedCountryName == "السعودية"
                                                   {
                                                       UserDefaults.standard.set(187, forKey: "countryID")
                                                   }
                    
                   
                    else
                   {

                    UserDefaults.standard.set(187, forKey: "countryID")
                    }
                    
                
                
                UserDefaults.standard.set(languageLbl.text, forKey: "myLanguage")
                
                if (languageLbl.text?.elementsEqual("ARABIC"))!
                {
                    UserDefaults.standard.set("AR", forKey: "langID")
                    
                  
//
//                    UserDefaults.standard.set("Arabic", forKey: "storyboard")
//
                    LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
                }
                else
                {
                    
                    UserDefaults.standard.set("EN", forKey: "langID")
                    
                    
//                    UserDefaults.standard.set("Main", forKey: "storyboard")
//
                    LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                }
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                //navigate.MyCountryId = MyCountryId
                
                self.navigationController?.pushViewController(navigate, animated: true)
            }
        }
        
        UserDefaults.standard.synchronize()
    }
    
    
    
    
    // MARK:- // Country Drop Down Button
    
    @IBAction func countryDropDown(_ sender: Any) {
        
        if viewOfCountryTable.isHidden == true{
            
            self.viewOfCountryTable.isHidden = false
            
            self.viewOfCountryTableHeightConstraint.constant = 200
            
        }
        else {
            
            self.viewOfCountryTable.isHidden = true
            
            self.viewOfCountryTableHeightConstraint.constant = 0
            
        }
        
    }
    
    
    // MARK:- // Language Drop Down Button
    
    @IBAction func languageDropDown(_ sender: Any) {
       
        if viewOfLanguageTable.isHidden == true{
            
            self.viewOfLanguageTable.isHidden = false
            
            self.viewOfLanguageTableHeightConstraint.constant = 200
            
        }
        else {
            
            self.viewOfLanguageTable.isHidden = true
            
            self.viewOfLanguageTableHeightConstraint.constant = 0
            
        }
        
    }
    
    
    // MARK:- // Close Table view
    
    @IBAction func closeTableViews(_ sender: Any) {
        
        if self.viewOfCountryTable.isHidden == false
        {
            self.viewOfCountryTableHeightConstraint.constant = 0
        }
        
        if self.viewOfLanguageTable.isHidden == false
        {
            self.viewOfLanguageTableHeightConstraint.constant = 0
        }
        
    }
    
    
    // MARK:- // Set FOnt
    
    func set_font()
    {
        
        countryLbl.font = UIFont(name: (countryLbl.font.fontName), size: CGFloat(Get_fontSize(size: 17)))
        
        languageLbl.font = UIFont(name: (languageLbl.font.fontName), size: CGFloat(Get_fontSize(size: 17)))
        
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to get all Country List
    
    func getCountryData()
        
    {
        
        
        
        //countryLbl.text = ""
        //chooseLanguageLBL.text = ""
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "lang_id=\(langID ?? "")"
        
        self.CallAPI(urlString: "app_all_country_list", param: parameters) {
            
            self.allCountryListDictionary = self.globalJson.copy() as? NSDictionary
            
            print("allCountryListDictionary-----",self.allCountryListDictionary)
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                 for i in 0..<(self.allCountryListDictionary["info_array"] as! NSArray).count {
                    
                    let id = ("\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")")
                        
                    print("id----",id)
                
                    UserDefaults.standard.set(id, forKey:"ID")
                    
                    print("countryid----",UserDefaults.standard.set(id, forKey:"ID"))
                }
                
                // UserDefaults.standard.set(self.countryLbl.text, forKey: "countryID")
                
                self.countryTableView.delegate = self
                
                self.countryTableView.dataSource = self
                
                self.countryTableView.reloadData()
                
                SVProgressHUD.dismiss()
                self.customView.removeFromSuperview()
            }
            
        }
    }
    
    
    
    
    // MARK:- // Set Static Strings
    
    func setStaticString()
    {
        setAttributedTitle(toLabel: titleLBL, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "your", comment: ""), boldTextFont: UIFont(name: titleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (countryLbl.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "preference", comment: ""))
        
        chooseCountryLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")
        
        chooseLanguageLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_language", comment: "")
        
        saveOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
        
    }

    
    
    
    
    
    
    
    // MARK:- // Reverse Geocoding to get Country from Entered Lat Long
    
    
    func getAddressFromLatLong(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.country!)
                    
                    if pm.country != nil {    //Country
                        
                        for i in 0..<(self.allCountryListDictionary["info_array"] as! NSArray).count {
                            if "\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["country_name"]!)".elementsEqual((pm.country?.uppercased())!) {
                                self.countryLbl.text = pm.country?.uppercased()
                                
                                //UserDefaults.standard.set("\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["countryname_en"] ?? "")", forKey: "myCountryName")
                                
                                 UserDefaults.standard.set("\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")", forKey: "countryID")
                                
                                UserDefaults.standard.set("\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["currency_id"] ?? "")", forKey: "currencyID")
                                
                                UserDefaults.standard.set("\(((self.allCountryListDictionary["info_array"] as! NSArray)[i] as! NSDictionary)["currency_symbol"] ?? "")", forKey: "currencySymbol")
                                
                                break
                            }
                            else {
                                self.countryLbl.text = ""
                            }
                        }
                        
                        
                    }
                }
        })
        
    }
    
    
}



// MARK:- // Tableview Delegates

extension PreferenceViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == countryTableView
        {
            
            tempArray = allCountryListDictionary["info_array"] as! NSArray
            
            return tempArray.count
        }
        else
        {
            return languageList.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == countryTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "country")
            
            tempArray = allCountryListDictionary["info_array"] as! NSArray
            
            cell?.textLabel?.text = "\((tempArray[indexPath.row] as! NSDictionary)["country_name"]!)"
            
              UserDefaults.standard.set("\((tempArray[indexPath.row] as! NSDictionary)["id"] ?? "")", forKey: "countryID")
            
            countryID = "\((tempArray[indexPath.row] as! NSDictionary)["id"]!)"
            
            print("countryid-----",countryID)
            
            print("id========",UserDefaults.standard.set(countryID, forKey: "countryID"))
            
            UserDefaults.standard.set("\((tempArray[indexPath.row] as! NSDictionary)["currency_id"] ?? "")", forKey: "currencyID")
            
            UserDefaults.standard.set("\((tempArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")", forKey: "currencySymbol")
            
            
            
            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))
            
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "language")
            
            cell?.textLabel?.text = languageList[indexPath.row]
            
            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))
            
            return cell!
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == countryTableView
        {
            
            tempArray = allCountryListDictionary["info_array"] as! NSArray

            countryLbl.text = "\((tempArray[indexPath.row] as! NSDictionary)["country_name"]!)"
            
            UserDefaults.standard.set("\((tempArray[indexPath.row] as! NSDictionary)["countryname_en"] ?? "")", forKey: "myCountryName")
            
            countryID = "\((tempArray[indexPath.row] as! NSDictionary)["id"]!)"
            
           
            
            UserDefaults.standard.set(countryID, forKey: "countryID")
            
            

            self.viewOfCountryTable.isHidden = true
            
            self.viewOfCountryTableHeightConstraint.constant = 0
        }
            
        else
        {
            languageLbl.text = languageList[indexPath.row]
            
            self.viewOfLanguageTable.isHidden = true
            
            self.viewOfLanguageTableHeightConstraint.constant = 0
            
        }
        
        UserDefaults.standard.synchronize()
    }
    
}


// MARK:- // Location Manager Delegate Method

extension PreferenceViewController: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        
        Latitude = userLocation!.coordinate.latitude
        
        Longitude = userLocation!.coordinate.longitude
        
        locationManager.stopUpdatingLocation()
        
//        print("Latitude",Latitude)
//        print("Longitude",Longitude)
        
        self.setCountry()
    }
    
}
