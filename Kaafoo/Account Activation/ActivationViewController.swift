//
//  ActivationViewController.swift
//  Kaafoo
//
//  Created by admin on 23/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ActivationViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    @IBOutlet weak var activateLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var activationCodeLbl: UILabel!
    @IBOutlet weak var activationCodeTXT: UITextField!
    
    var newUserID : String! = ""
    let dispatchGroup = DispatchGroup()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAttributedTitle(toLabel: activateLbl, boldText: "ACTIVATE", boldTextFont: UIFont(name: activateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 28)))!, normalTextFont: UIFont(name: (activationCodeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 28)))!, normalText: " ACCOUNT")
        
        set_font()
        
        activationCodeTXT.delegate = self
        
        activateOutlet.layer.cornerRadius = activateOutlet.frame.size.height / 2
        
        resendCodeOutlet.layer.cornerRadius = resendCodeOutlet.frame.size.height / 2
        
        
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Activate Button
    
    @IBOutlet weak var activateOutlet: UIButton!
    @IBAction func clickOnActivate(_ sender: Any) {
        if activationCodeTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Activation Code Can not be blank")
        }
        else
        {
            self.activate()
        }
    }
    
    
    // MARK:- // REsend COde Button
    
    
    @IBOutlet weak var resendCodeOutlet: UIButton!
    @IBAction func resendCode(_ sender: Any) {
        
        resendCode()
        
    }
    
    
    @IBOutlet weak var clickOnActivationCodeOutlet: UIButton!
    @IBAction func clickOnActivationCode(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.activationCodeLbl.frame.origin.y = (self.activationCodeLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.activationCodeLbl.font = UIFont(name: (self.activationCodeLbl.font?.fontName)!,size: 11)
            self.activationCodeLbl.font = UIFont(name: self.activationCodeLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnActivationCodeOutlet.isUserInteractionEnabled = false
            self.activationCodeTXT.isUserInteractionEnabled = true
            self.activationCodeTXT.becomeFirstResponder()
            
            
        }
        
    }
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Activate Code get back in position
    
    
    func activationCodeGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.activationCodeLbl.frame.origin.y = self.activationCodeTXT.frame.origin.y
            self.activationCodeLbl.font = UIFont(name: (self.activationCodeLbl.font?.fontName)!,size: 16)
            self.activationCodeLbl.font = UIFont(name: self.activationCodeLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnActivationCodeOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK: - // JSON POST Method to submit SIGNUP Data
    
    func activate()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_user_account_activation")!   //change the url
        
        var parameters : String = ""
        
        parameters = "activation_code=\(activationCodeTXT.text!))"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Activation Response: " , json)
                    
                    if "\(json["response"]! as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
                            self.navigationController?.pushViewController(navigate, animated: true)
                            
                            
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Warning", message:"\(json["message"]!)")
                        }
                        
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to Resend Activation Code
    
    func resendCode()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_resend_activation_code")!   //change the url
        
        var parameters : String = ""
        
        parameters = "user_id=\(newUserID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Resend Code Response: " , json)
                    
                    if "\(json["response"]! as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Alert", message: "\(json["message"]!)")
                        }
                    }
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.ShowAlertMessage(title: "Alert", message: "\(json["message"]!)")
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Textfield Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        activationCodeTXT.resignFirstResponder()
        
        if textField == activationCodeTXT
        {
            self.activationCodeGetBackInPosition()
        }
        
        return true
        
    }
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        //        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        //        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        activationCodeLbl.font = UIFont(name: activationCodeLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        
        activationCodeTXT.font = UIFont(name: (activationCodeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
        activateOutlet.titleLabel?.font = UIFont(name: (activateOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        resendCodeOutlet.titleLabel?.font = UIFont(name: (resendCodeOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        
        
    }
    
    
}

