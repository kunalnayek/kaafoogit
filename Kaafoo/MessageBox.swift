//
//  MessageBox.swift
//  Kaafoo
//
//  Created by esolz on 28/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation

class MessageBox : UIView
{
    @IBOutlet var ContentView: UIView!
    
    @IBOutlet weak var HeadingLbl: UILabel!
    
    @IBOutlet weak var SeparatorView: UIView!
    
    @IBOutlet weak var MessageTextView: UITextView!
    
    @IBOutlet weak var SendBtnOutlet: UIButton!
    
    @IBOutlet weak var CancelBtnOutlet: UIButton!
    
    override init(frame: CGRect) {
           super.init(frame: frame)
       }
       
       required init?(coder aDecoder: NSCoder) {
        
           super.init(coder: aDecoder)
        
           Bundle.main.loadNibNamed("MessageBox", owner: self, options: nil)
        
           addSubview(ContentView)
           
           ContentView.frame = self.bounds
        
           ContentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.SetLayer()
    }
    
    func SetLayer()
    {
        self.MessageTextView.layer.borderWidth = 1.0
        
        self.MessageTextView.layer.borderColor = UIColor.black.cgColor
        
        self.MessageTextView.clipsToBounds = true
        
        self.MessageTextView.layer.cornerRadius = 5.0
    }
}
