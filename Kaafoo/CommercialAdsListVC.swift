//
//  CommercialAdsListVC.swift
//  Kaafoo
//
//  Created by priya on 20/07/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class CommercialAdsListVC: GlobalViewController {
    
    @IBOutlet weak var ListingTableView: UITableView!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    var ListingDictionary : NSDictionary!
    var nextStart : String! = ""
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    var PerLoad : Int! = 10
    var startValue = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ListingData()

       
    }
    

    // MARK:- // Get Listing

       
    
     func ListingData()
        {
            
             var parameters : String = ""
            
            let userID = UserDefaults.standard.string(forKey: "userID")
            let langID = UserDefaults.standard.string(forKey: "langID")
            
            parameters = "lang_id=\(langID!)&user_id=\(userID!)&start_value=\(startValue)&per_load=\(PerLoad!)"
            
            
            self.CallAPI(urlString: "app_add_commercial_adv_list", param: parameters, completion: {
                       
                       self.globalDispatchgroup.leave()
                       
                self.recordsArray = self.globalJson["info_array"]  as? NSMutableArray
                       print("recordarray======",self.recordsArray)
                      
                
                          self.nextStart = "\(self.globalJson["next_start"] ?? "")"
                
                       self.globalDispatchgroup.notify(queue: .main, execute: {
                           
                        DispatchQueue.main.async {
                       
                            self.ListingTableView.delegate = self
                            self.ListingTableView.dataSource = self
                            self.ListingTableView.reloadData()
                        
                         SVProgressHUD.dismiss()
                    }
                })

            })
        }

    }



// MARK : -  CommercialAdsListTableViewCell

class CommercialAdsListTableViewCell : UITableViewCell
{
    
    @IBOutlet weak var refundstatus: UILabel!
    @IBOutlet weak var invoicenumber: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var countryname: UILabel!
    
    @IBOutlet weak var emailid: UILabel!
    @IBOutlet weak var contactnumber: UILabel!
    
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var addreviewBTn: UIButton!
}

// MARK:- // Tableview Delegate Methods

extension CommercialAdsListVC: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let obj = tableView.dequeueReusableCell(withIdentifier: "CommercialAdsListTableViewCell") as! CommercialAdsListTableViewCell
        
        obj.countryname.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["name"]!)"
        obj.emailid.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["emailid"]!)"
        obj.contactnumber.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["contact_no"]!)"
       
        obj.viewBtn.backgroundColor = .s_green()
        obj.viewBtn.layer.cornerRadius =  15

        return obj
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }


}


