//
//  LogInViewController.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import PushNotifications

class LogInViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    var appDelegate: AppDelegate!
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var EmailTxtCentetXConstraint: NSLayoutConstraint!
    @IBOutlet weak var logLbl: UILabel!
    @IBOutlet weak var inLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var passwordTXT: UITextField!
    @IBOutlet weak var forgotPasswordLbl: UILabel!
    @IBOutlet weak var dontHaveAnAccountLbl: UILabel!
    @IBOutlet weak var continueAsLbl: UILabel!
    @IBOutlet weak var guessButtonOutlet: UIButton!
    
    let dispatchGroup = DispatchGroup()
    
    var signInResponseDict : NSMutableDictionary!
    
    // MARK: - // ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTXT.textContentType = .username
        self.passwordTXT.textContentType = .password
//        if UserDefaults.standard.string(forKey: "langID") == "AR"{
//            self.emailTXT.setLeftPaddingPoints(20)
//            self.emailTXT.setRightPaddingPoints(20)
//        }
        
        
        self.setStaticStrings()
        
        print("Bundle Identifier : ", Bundle.main.bundleIdentifier!)
        
        let lastLoggedEmail = UserDefaults.standard.string(forKey: "lastLoggedEmail")
        let lastLoggedPassword = UserDefaults.standard.string(forKey: "lastLoggedPassword")
        
        passwordTXT.isSecureTextEntry = true
        
        if lastLoggedEmail != nil
        {
            emailTXT.text = lastLoggedEmail!
            
            self.emailLbl.frame.origin.y = (self.emailLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 11)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            //self.emailLbl.backgroundColor = .red
            self.clickOnEmailOutlet.isUserInteractionEnabled = false
            self.emailTXT.isUserInteractionEnabled = true
            
//            if self.isValidEmail(emailStr: emailTXT.text ?? "") == true
//            {
//
                if lastLoggedPassword != nil
                {
                    passwordTXT.text = lastLoggedPassword
                    
                    self.passwordLbl.frame.origin.y = (self.passwordLbl.frame.origin.y - ((30/568)*self.FullHeight))
                    self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 11)
                    self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
                    self.clickOnPasswordOutlet.isUserInteractionEnabled = false
                    self.passwordTXT.isUserInteractionEnabled = true
                    
                }
//            }
//            else
//            {
//                self.ShowAlertMessage(title: "Warning", message: "Email is Not in Correct Format")
//            }
           
        }
        
        set_font()
        
       // logInButtonOutlet.layer.cornerRadius = logInButtonOutlet.frame.size.height / 2.0
        
        logInButtonOutlet.backgroundColor = UIColor(red: 255, green: 202, blue: 0)
        
        dontHaveAnAccountLbl.sizeToFit()
        signUpButtonOutlet.center = dontHaveAnAccountLbl.center
        signUpButtonOutlet.frame.origin.x = dontHaveAnAccountLbl.frame.origin.x + dontHaveAnAccountLbl.frame.size.width
        
        continueAsLbl.sizeToFit()
        guessButtonOutlet.center = continueAsLbl.center
        guessButtonOutlet.frame.origin.x = continueAsLbl.frame.origin.x + continueAsLbl.frame.size.width
        
        
        emailTXT.delegate = self
        passwordTXT.delegate = self
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.EmailTxtCentetXConstraint.constant += self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - // Buttons
    
    // MARK: - // Login Button
    
    
    @IBOutlet var logInButtonOutlet: UIButton!
    
    @IBAction func logInButton(_ sender: Any) {
        
        self.emailTXT.resignFirstResponder()
        self.passwordTXT.resignFirstResponder()
        
        self.validationCheck()
        
        UserDefaults.standard.set(emailTXT.text, forKey: "lastLoggedEmail")
        UserDefaults.standard.set(passwordTXT.text, forKey: "lastLoggedPassword")
       
        signIn()
        
//        appDelegate.beamsClient.setUserId("<USER_ID_GOES_HERE>", tokenProvider: tokenProvider, completion: { error in
//          guard error == nil else {
//              print(error.debugDescription)
//              return
//          }
//
//          print("Successfully authenticated with Pusher Beams")
//        })
        
        dispatchGroup.notify(queue: .main) {
        
            
        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
            
            
        if "\(self.signInResponseDict["response"]!)".elementsEqual("1")
            
        {
            BeamsParameters.shared.setBeamsToken(Token: "\((self.signInResponseDict["beamsToken"] as! NSDictionary)["token"] ?? "")")
            
            print("BeamsToken",BeamsParameters.shared.BeamsToken!)
            
            
         
            //let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! HomeViewController
            
            print("Storyboard Name----",storyboardName!)
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            
            ////
            let EmailID = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["basic_info"] as! NSArray)[0] as! NSDictionary)["emailid"]!))"
            
            let UserType = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["basic_info"] as! NSArray)[0] as! NSDictionary)["usertype"]!))"
            
            if UserType.elementsEqual("B")
                
            {
                
                let PhoneNo = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["business_info"] as! NSArray)[0] as! NSDictionary)["mobile_num"]!))"
                
                UserDefaults.standard.set(PhoneNo, forKey: "PhoneNo")
                
                let Name  = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["business_info"] as! NSArray)[0] as! NSDictionary)["displayname"]!))"
                
                //NAME SPLITING
                
                var components = Name.components(separatedBy: " ")
                
                if(components.count > 0)
                    
                {
                    
                    let FirstName = components.removeFirst()
                    
                    let LastName = components.joined(separator: " ")
                    
                    print("FIRSTNAME",FirstName)
                    
                    print("LASTNAME",LastName)
                    
                    UserDefaults.standard.set(FirstName, forKey: "FirstName")
                    
                    UserDefaults.standard.set(LastName, forKey: "LastName")
                    
                    print("FIRSTANDLASTBUsiness",FirstName,LastName)
                    
                }
                
            }
                
            else
                
            {
                
                let PhoneNo = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["mobilenum"]!))"
                
                UserDefaults.standard.set(PhoneNo, forKey: "PhoneNo")
                
                let FirstName  = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["fname"]!))"
                
                let LastName  = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["lname"]!))"
                
                UserDefaults.standard.set(FirstName, forKey: "FirstName")
                
                UserDefaults.standard.set(LastName, forKey: "LastName")
                
                print("FIRSTANDLASTPrivate",FirstName,LastName)
                
            }
            /////
            
            let userID = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["basic_info"] as! NSArray)[0] as! NSDictionary)["id"]!))"

            let langID = UserDefaults.standard.string(forKey: "langID")
            
            UserDefaults.standard.set(EmailID, forKey: "EmailID")

            UserDefaults.standard.set(userID, forKey: "userID")

            UserDefaults.standard.set("0", forKey: "isGuest")
            
            print("User ID is : ", userID)
            print("Lang ID is : ", langID!)
            print("Storyboard is : ",storyboardName!)
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if "\(self.signInResponseDict["message"]!)".elementsEqual("Email and password does not match")
        {
            let alert = UIAlertController(title: "Login Invalid", message: "Email and Password does not match.", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }

        else if "\(self.signInResponseDict["response"]!)".elementsEqual("0")
        {
//            if "\(self.signInResponseDict["message"]!)".elementsEqual("Users account is not active")
//            {
//                let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController

            let alert = UIAlertController(title: "Warning", message: "You have entered invalid password and email Id", preferredStyle: .alert)

            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
//
//                navigate.newUserID = "\(self.signInResponseDict["new_userid"]!)"
//
//                self.navigationController?.pushViewController(navigate, animated: true)
//            }
        }
            //self.authData()
      }
    }
    
    //Email Verification and Empty Fields Checking
    
    func validationCheck()
    {
        if self.emailTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Your Email Address")
        }
        else if isValidEmail(emailStr: self.emailTXT.text ?? "") == false
        {
            self.ShowAlertMessage(title: "Warning", message: "Your Email is not in Correct Format")
        }
        else if self.passwordTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Your Password")
        }
        else
        {
            //Do Nothing
        }
    }
    
   // MARK: - // SignUp Button
    
    
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    
    @IBAction func signUpButton(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK: - // Guest Button
    
    @IBAction func guessButton(_ sender: Any) {
        
//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
        self.navigationController?.pushViewController(navigate, animated: true)
        
        UserDefaults.standard.set("1", forKey: "isGuest")
        UserDefaults.standard.set("GUEST", forKey: "userDisplayName")
        UserDefaults.standard.set("", forKey: "userID")
        
        
    }
    
    
    // MARK: - // Forgot Password Button
    
    @IBOutlet weak var forgotPasswordOutlet: UIButton!
    @IBAction func forgotPassword(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "forgotPasswordVC") as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK: - // Click On Email Button
    
    
    @IBOutlet weak var clickOnEmailOutlet: UIButton!
    @IBAction func clickOnEmail(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = (self.emailLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 11)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = false
            self.emailTXT.isUserInteractionEnabled = true
            self.emailTXT.becomeFirstResponder()
            if (self.passwordTXT.text?.isEmpty)!
            {
                self.passwordGetBackInPosition()
                
            }
            
        }
        
    }
    
    
    // MARK: - // Click On password Button
    
    
    @IBOutlet weak var clickOnPasswordOutlet: UIButton!
    @IBAction func clickOnPassword(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = (self.passwordLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 11)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = false
            self.passwordTXT.isUserInteractionEnabled = true
            self.passwordTXT.becomeFirstResponder()
            if (self.emailTXT.text?.isEmpty)!
            {
                self.emailGetBackInPosition()
                
            }
            
        }
        
    }
    

    // MARK: - // Email Get back in position
    
    
    func emailGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = self.emailTXT.frame.origin.y
            
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 16)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    // MARK: - // Password Get back in position
    
    
    func passwordGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = self.passwordTXT.frame.origin.y
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 16)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK: - // Defined Functions
    
    // MARK: - // Set Font
    
    func set_font()
    {
        
        //logLbl.font = UIFont(name: logLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        //inLbl.font = UIFont(name: inLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        emailLbl.font = UIFont(name: emailLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        passwordLbl.font = UIFont(name: passwordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        emailTXT.font = UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        passwordTXT.font = UIFont(name: (passwordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        //forgotPasswordLbl.font = UIFont(name: forgotPasswordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dontHaveAnAccountLbl.font = UIFont(name: dontHaveAnAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        //        signUpButtonOutlet.font = UIFont(name: signUpButtonOutlet.font.fontName, size: CGFloat(Get_fontSize(size: 14)))!
        
    }
    
     //MARK:- //Auth Data
    
               func authData()
               {
                   let tokenProvider = BeamsTokenProvider(authURL: "https://a1dff960-4058-45ff-a842-24e43541dd58.pushnotifications.pusher.com") { () -> AuthData in
                       let sessionToken = BeamsParameters.shared.BeamsToken ?? ""
                       print("token",sessionToken)
                     let headers = ["Authorization": "Bearer \(sessionToken)"] // Headers your auth endpoint needs
                     let queryParams: [String: String] = [:] // URL query params your auth endpoint needs
                     return AuthData(headers: headers, queryParams: queryParams)
                    
                   }
    
                   
                  // print("userid",UserID)
                   appDelegate.beamsClient.setUserId("19", tokenProvider: tokenProvider, completion: { error in
                     guard error == nil else {
                         print(error.debugDescription)
                         return
                     }
                     print("Successfully authenticated with Pusher Beams")
                   })
               }
               
    
    
    
    // MARK: - // JSON POST Method to submit SignIN Data
    
    func signIn()
        
    {
        let loginType = "I"
        
        let deviceToken = UserDefaults.standard.string(forKey: "DeviceToken")
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_login")!   //change the url
        
        print("Login URL : ", url)
        
        var parameters : String = ""
        
       // self.emailTXT.backgroundColor = .blue
        
        parameters = "login_email=\(emailTXT.text ?? "")&login_password=\(passwordTXT.text ?? "")&login_type=\(loginType)&device_token=\(deviceToken!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
//                    print("Login Response: " , json)
                    
                    self.signInResponseDict = json.mutableCopy() as? NSMutableDictionary
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        self.dispatchGroup.leave()
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        
                        
                        UserDefaults.standard.set("\((self.signInResponseDict["info_array"] as! NSDictionary)["logo_img"]!)", forKey: "userImageURL")
                        
                        
                        let tempArray = ((json["info_array"] as! NSDictionary)["basic_info"] as! NSArray)
                        
                        UserDefaults.standard.set("\((tempArray[0] as! NSDictionary)["usertype"]!)", forKey: "userType")
                        
                        if "\((tempArray[0] as! NSDictionary)["usertype"]!)".elementsEqual("B")
                        {
                            
                            
                            let userDisplayName = "\((((json["info_array"] as! NSDictionary)["business_info"] as! NSArray)[0] as! NSDictionary)["displayname"]!)"
                            
                            UserDefaults.standard.set(userDisplayName, forKey: "userDisplayName")
                        }
                        else
                        {
                            let userDisplayName = "\((((json["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["display_name"]!)"
                            
                            UserDefaults.standard.set(userDisplayName, forKey: "userDisplayName")
                        }
                        
                        self.dispatchGroup.leave()
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                    

                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Set Static Strings
    
    func setStaticStrings()
    {
//        let langID = UserDefaults.standard.string(forKey: "langID")
//
//        if (langID?.elementsEqual("EN"))!
//        {
//
//            setAttributedTitle(toLabel: logLbl, boldText: "LOG", boldTextFont: UIFont(name: logLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " IN")
//
//        }
//        else
//        {
        
            setAttributedTitle(toLabel: logLbl, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "LOG", comment: ""), boldTextFont: UIFont(name: logLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "IN", comment: ""))
            
//        }
//
        emailLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "email", comment: "")
        passwordLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
        
        logInButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "login", comment: ""), for: .normal)

        forgotPasswordOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "forgot_password", comment: ""), for: .normal)
        //forgotPasswordLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "forgot_password", comment: "")
        
        dontHaveAnAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dont_have_an", comment: "")
        signUpButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sign_up", comment: ""), for: .normal)
        continueAsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "continue_as", comment: "")
        guessButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "guest", comment: ""), for: .normal)
        
    }
    
    
    
    
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        emailTXT.resignFirstResponder()
        passwordTXT.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if textField == emailTXT
//        {
//            emailTXT.text = ""
//        }
//        else if textField == passwordTXT
//        {
//            passwordTXT.text = ""
//        }
        
    }
    

}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
