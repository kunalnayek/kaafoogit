//
//  RealEstateViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 26/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import GooglePlaces


class RealEstateViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var realEstateTitleLBL: UILabel!
    @IBOutlet weak var realEstateSubtitleLBL: UILabel!
    @IBOutlet weak var whereDoYouWantToLive: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    
    var resultsArray : NSArray = NSArray()
    var searchQuery : String!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var LocationTextField: UITextField!
    var RealDate: String! = ""
    
    @IBOutlet weak var CalenderBlurView: UIView!
    @IBOutlet weak var ViewCalender: UIView!
    @IBOutlet var Calender_Lbl_View: UIView!
    @IBOutlet var LocationList: UITableView!
    @IBOutlet var LocationtableView: UIView!
    let dispatchGroup = DispatchGroup()
    @IBAction func Calender_btn(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3){
            self.ViewCalender.isHidden = false
        }
    }
    @IBOutlet var Calender_View: UIView!
    
    @IBOutlet var Date_View: UIView!
    @IBOutlet var Search_Outlet: UIButton!
    //MARK:- Navigate To Real Estate Listing page
    @IBAction func Search_btn(_ sender: UIButton) {
        let object = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newrealestate") as! NewRealEstateListingViewController
        object.RealDate = RealDate
        self.navigationController?.pushViewController(object, animated: true)
    }
    @IBOutlet var Calender_btn_View: UIView!
    @IBOutlet var datePicker: UIDatePicker!
    //MARK:- Cross Button Tap
    @IBAction func Cross_btn(_ sender: UIButton) {
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.LocationTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(DoneButton(sender:)))
        self.LocationTextField.text = ""
        self.LocationList.separatorStyle = .none
        if self.LocationTextField.resignFirstResponder() == true
        {
            self.LocationList.isHidden = true
            self.LocationtableView.isHidden = true
            //self.LocationtableView.frame.size.height = 1.0
            self.Date_View.isHidden = true
            self.Calender_Lbl_View.isHidden = false
        }
        else
        {
            //do nothing
        }
        
        self.tapGesture()
        self.ViewCalender.isHidden = true
        LocationTextField.delegate = self
        Search_Outlet.layer.cornerRadius = 5.0
        Search_Outlet.frame.origin.y = (322/568)*self.FullHeight
        Search_Outlet.frame.size.height = (44/568)*self.FullHeight
        Search_Outlet.frame.size.width = (280/320)*self.FullWidth
        
        self.changeLanguageStrings()
       // self.fireGoogleSearch()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- //Dismiss Keyboard
       
       @objc func dismissKeyboard() {
        view.endEditing(true)
        self.LocationtableView.isHidden = true
        self.Calender_Lbl_View.isHidden = false
        self.LocationList.isHidden = true
        self.LocationList.isHidden = true
        self.Date_View.isHidden = true
       }

    
    @objc func DoneButton(sender : UIButton)
    {
        self.LocationtableView.isHidden = true
        self.Calender_Lbl_View.isHidden = false
        self.LocationList.isHidden = true
        self.LocationList.isHidden = true
        self.Date_View.isHidden = true
    }
    
    
  //  func showDatePicker(){
        //Formate Date
   //     datePicker.datePickerMode = .date
        
        //ToolBar
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//
//
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: "donedatePicker")
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: "cancelDatePicker")
//        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
//        dateLbl.inputAccessoryView = toolbar
//        dateLbl.inputView = datePicker
        
//    }
    
    
    
    
    // MARK:- // Change Language Strings
    
    func changeLanguageStrings() {
        
        self.realEstateTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "realEstateCaps", comment: "")
        self.realEstateSubtitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "findYourBestProperty", comment: "")
        self.whereDoYouWantToLive.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "whereDoYouWantToLive", comment: "")
        self.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
    self.Search_Outlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: ""), for: .normal)
        
    }
    
    
    
    
    
    func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        //txtDatePicker.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func cancelDatePicker(){
        self.view.endEditing(true)
    }

    @IBAction func ok_btn(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3)
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.dateLbl.text = formatter.string(from: self.datePicker.date)
            self.RealDate = self.dateLbl.text
            self.ViewCalender.isHidden = true
        }
       }
    
    @IBAction func cancel_btn(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3){
            self.ViewCalender.isHidden = true
        }
    }
    
    @IBAction func FindLocationBtn(_ sender: UITextField) {
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self as! GMSAutocompleteViewControllerDelegate
//        present(autocompleteController, animated: true, completion: nil)
        
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        LocationTextField.text = ""
        self.resultsArray = []
        self.LocationList.reloadData()
        LocationTextField.becomeFirstResponder()
        self.Date_View.isHidden = false
        LocationtableView.isHidden = false
        self.LocationList.isHidden = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        LocationTextField.resignFirstResponder()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        dispatchGroup.enter()
        Calender_Lbl_View.isHidden = true
        let Txt1 = LocationTextField.text
        let Txt2 = string
        searchQuery = Txt1! + Txt2
        dispatchGroup.leave()
        dispatchGroup.notify(queue: .main) {
            self.LocationSearch()
        }
        return true
    }
    
    //MARK:- Tableview Number Of Rows Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }
    
    //MARK:- Tableview Cell For Row Method
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "listtableview", for: indexPath) 
        let tempDict = self.resultsArray[indexPath.row] as! NSDictionary
        obj.textLabel?.text = "\(tempDict["description"]!)"
        //obj.textLabel?.text = "Hello"
        return obj
    }
    //MARK:- TableView Height For Row Method
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (44/667)*self.FullHeight
    }
    //MARK:- TableView Did Select Method
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tempDict = self.resultsArray[indexPath.row] as! NSDictionary
        //searchQuery = "\(tempDict["description"]!)"
        self.LocationTextField.text = "\(tempDict["description"]!)"
//        self.LocationTextField.resignFirstResponder()
        UIView.animate(withDuration: 0.3)
        {
            self.LocationtableView.isHidden = true
            self.Calender_Lbl_View.isHidden = false
            self.LocationList.isHidden = true
            self.LocationList.isHidden = true
            self.Date_View.isHidden = true
        }
        
    }
    
    //MARK:- API Fire to Fetch Locations From Google Maps
    
    func LocationSearch()
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        var strurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchQuery!)&language=en&key=AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM"
        print("Search Query",searchQuery!)
        strurl = strurl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("Google Search URL is : ", strurl)
        
        if let url = NSURL(string: strurl)
            
        {
            if let data = try? Data(contentsOf:url as URL)
            {
                do{
                    let jsonDict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
                    
                    print("Search Result Response : " , jsonDict)
                    resultsArray = jsonDict["predictions"] as! NSArray
                    
                    DispatchQueue.main.async {
                        
                        self.LocationList.delegate = self
                        self.LocationList.dataSource = self
                        self.LocationList.reloadData()
                        SVProgressHUD.dismiss()
                        //self.LocationtableView.isHidden = false
                    }
                }
                catch
                    
                {
                    print("Error")
                }
            }
        }
        
    }

    //MARK:- UITapGesture For BlurView
    func tapGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(Tapped(sender:)))
        self.CalenderBlurView.addGestureRecognizer(tap)
    }
    @objc func Tapped(sender : UITapGestureRecognizer)
    {
        self.ViewCalender.isHidden = true
    }
    

}
//extension RealEstateViewController: GMSAutocompleteViewControllerDelegate {
//
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place address: \(place.formattedAddress)")
//        print("Place attributions: \(place.attributions)")
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//
//}
