
//
//  NewRealEstateAllQNAViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 04/06/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewRealEstateAllQNAViewController: GlobalViewController {


    @IBOutlet weak var allQNATable: UITableView!

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var productID : String!

    var productInfoDictionary : NSDictionary!

    var recordsArray = NSMutableArray()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.FetchData()

    }



    //MARK:- // JSON Post Method to Get Real Estate Details

    func FetchData()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "product_id=\(productID!)&user_id=\(userID!)&lang_id=\(langID!)&start_value=\(startValue)&per_load=\(perLoad)"

        self.CallAPI(urlString: "app_product_details", param: parameters, completion: {

            self.productInfoDictionary = self.globalJson["product_info"] as? NSDictionary

            let questionAnswerArray = (self.productInfoDictionary["question_answer"] as! NSArray)

            for i in 0..<questionAnswerArray.count

            {

                let tempDict = questionAnswerArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start_question"]!)"

            self.startValue = self.startValue + questionAnswerArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()

                self.allQNATable.delegate = self
                self.allQNATable.dataSource = self
                self.allQNATable.reloadData()

                SVProgressHUD.dismiss()
            }

        })
    }


}





// MARK:- // Tableview Delegates

extension NewRealEstateAllQNAViewController: UITableViewDelegate,UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "allQna") as! newRealEstateAllQnaTVC

        cell.question.text = "\((recordsArray[indexPath.row] as! NSDictionary)["question"] ?? "")"
        cell.askedBy.text = "\((recordsArray[indexPath.row] as! NSDictionary)["qusn_user"] ?? "")" + " (\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"] ?? ""))"
        cell.askedOn.text = "\((recordsArray[indexPath.row] as! NSDictionary)["question_date"] ?? "")"

        if "\((recordsArray[indexPath.row] as! NSDictionary)["answer"] ?? "")".elementsEqual("") {
            cell.answer.text = "Not answered yet."
        }
        else {
            cell.answer.text = "\((recordsArray[indexPath.row] as! NSDictionary)["answer"] ?? "")"
        }

        cell.answeredOn.text = "\((recordsArray[indexPath.row] as! NSDictionary)["reply_date"] ?? "")"


        cell.qBackgroundView.backgroundColor = UIColor.yellow2()
        cell.qTitleLBL.textColor = .white
        cell.qBackgroundView.layer.cornerRadius = 5

        // Set Font

        cell.qTitleLBL.font = UIFont(name: cell.qTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        cell.question.font = UIFont(name: cell.question.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        cell.askedBy.font = UIFont(name: cell.askedBy.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.askedOn.font = UIFont(name: cell.askedOn.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        cell.aTitleLBL.font = UIFont(name: cell.aTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        cell.answer.font = UIFont(name: cell.answer.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        cell.answeredOn.font = UIFont(name: cell.answeredOn.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))

        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell

    }




}



// MARK: - // Scrollview Delegates

extension NewRealEstateAllQNAViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.FetchData()
            }
        }
    }
}








// MARK:- // All Question and Answers Tableview Cell

class newRealEstateAllQnaTVC: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var qBackgroundView: UIView!
    @IBOutlet weak var qTitleLBL: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var askedBy: UILabel!
    @IBOutlet weak var askedOn: UILabel!
    @IBOutlet weak var aTitleLBL: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var answeredOn: UILabel!

}
