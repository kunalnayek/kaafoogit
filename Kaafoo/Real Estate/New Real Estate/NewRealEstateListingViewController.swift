//
//  NewRealEstateListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 17/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//
import UIKit
import SVProgressHUD
import SDWebImage
import GoogleMaps
import GooglePlaces

class NewRealEstateListingViewController: GlobalViewController,GMSMapViewDelegate {
    
    @IBAction func tapDetailsCloseBtn(_ sender: UIButton) {
        self.DetailsView.isHidden = true
    }
    var index : Int! = 0
    var isMarkerActive : Bool! = false
    var selectedMarker : GMSMarker!
    
    var SInt : Int!
    
    var markers = [GMSMarker]()
    
    @IBOutlet weak var tapDetailsViewClose: UIButton!
    @IBOutlet weak var DetailsView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var ShowSortView: UIView!
    @IBOutlet weak var SortPickerView: UIPickerView!
    @IBOutlet weak var ListingTableview: UITableView!
    @IBOutlet weak var LocalHeaderVIew: UIView!
    @IBOutlet weak var PropertyType: UILabel!
    @IBOutlet weak var PropertyTypeBtn: UIButton!
    @IBOutlet weak var SortBtn: UIButton!
    @IBOutlet weak var MapViewBtn: UIButton!
    @IBOutlet weak var ListGridBtn: UIButton!
    @IBOutlet weak var ListingCollectionView: UICollectionView!
    var LocalSortArray = [["option_name" : "Type" , "option_value" : ["Asking Price","Buy Now","Auction"]],["option_name" : "Filter", "option_value" : ["On Sale","Cash On Delivery"]]]

    var SortingArray : NSMutableArray! = NSMutableArray()
    var user_type : String! = ""
    var mapStatus : String! = ""
    var ShowMap : Bool! = false
    var ProId : String!
    var type: String! = ""
    var PerPage : String! = ""
    var filter : String! = ""
    var condition : String! = ""
    var sortingType : String! = ""
    var MyCountryId : String! = ""
    var MyCountryName : String! = ""
    var sortSearchRegionId : String! = ""
    var SortSearchCityId : String! = ""
    var MinAmount : String! = ""
    var MaxAmount : String! = ""
    var UserAccountTypeSearch : String! = ""
    var SearchBy : String! = ""
    var ProductBarcode : String! = ""
    var realLatitude : String! = ""
    var realLongitude : String! = ""
    var RealDate : String! = ""
    var MaxDist : String! = ""
    var watchListProductID : String!
    var start_value = 0
    var per_value = 10
    var nextStart : String!
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    let dispatchGroup = DispatchGroup()
    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    let userID = UserDefaults.standard.string(forKey: "userID")

    var info_array: NSDictionary!
    var ProductInfo : NSDictionary!
    var Product_listing : NSMutableArray! = NSMutableArray()
    var recordsArray : NSMutableArray! = NSMutableArray()
    var IconLocalArray : NSMutableArray!
    var IconArray : NSMutableArray! = NSMutableArray()
    var nextOrigin:CGFloat!
    var sortDict = [["key" : "" , "value" : "All"],["key" : "B" , "value" : "Most Bids"] , ["key" : "T" , "value" : "Title"] , ["key" : "LP" , "value" : "Lowest Priced"],["key" : "HP" , "value" : "Highest Priced"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newest Listed"]]


    var featureBackgroundViewArray = [UIView]()

    var tempParams : String! = ""

    var sortArray = NSMutableArray()
    var SelectedInt : Int! = 0

    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateWatchlist), name: NSNotification.Name(rawValue: "UpdateRealEstateWatchlist"), object: nil)
        
        self.setLayer()
        
        self.DetailsView.isHidden = true
        
        self.mapView.delegate = self
        
        ProductOnMapArray = []
        
        self.CreateFavouriteView(inview: self.view)
        self.ShowSortView.isHidden = true
        self.SortFunction()
        self.GetListing()
        self.ListingTableview.isHidden = false
        self.ListingCollectionView.isHidden = true
        self.SortPickerView.delegate = self
        self.SortPickerView.dataSource = self


        self.defineSortButtonActions()


    }
    
    //MARK:- // Notification Method for Updating Watchlist status inside tableview
    
    @objc func updateWatchlist()
    {
        self.start_value = 0
        self.recordsArray.removeAllObjects()
        self.GetListing()
    }
    
    //MARK:- //Layer design Configuration
    
    func setLayer()
    {
        self.DetailsView.layer.borderColor = UIColor.black.cgColor
        self.DetailsView.layer.borderWidth = 1.0
    }
    
    
    @IBOutlet weak var GProductImageView: UIImageView!
    @IBOutlet weak var GProductName: UILabel!
    @IBOutlet weak var GSquareMeters: UILabel!
    @IBOutlet weak var GListedTime: UILabel!
    @IBOutlet weak var GPostedTime: UILabel!
    @IBOutlet weak var GSellerName: UILabel!
    @IBOutlet weak var GproductType: UILabel!
    @IBOutlet weak var GProductAddress: UILabel!
    @IBOutlet weak var GProductPrice: UILabel!
    
    

    
    //MARK:- //GMSMapview Delegate Methods
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
    }
    
    func AppendMapData()
    {
        self.dispatchGroup.enter()
        
        for i in 0..<self.recordsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((recordsArray[i] as! NSDictionary)["product_name"] ?? "")", ProductLatitude: Double(("\((recordsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((recordsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((recordsArray[i] as! NSDictionary)["product_photo"] ?? "")", CurrencySymbol: "\((recordsArray[i] as! NSDictionary)["currency_symbol"] ?? "")", StartPrice: "\((recordsArray[i] as! NSDictionary)["start_price"] ?? "")", ReservePrice: "\((recordsArray[i] as! NSDictionary)["address"] ?? "")", ProductID: "\((recordsArray[i] as! NSDictionary)["product_id"] ?? "")", SellerImage: "\((recordsArray[i] as! NSDictionary)["product_type"] ?? "")", WatchlistStatus: "\((recordsArray[i] as! NSDictionary)["business_name"] ?? "")"))
        }
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
//            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.mapView.animate(to: cameraPosition)
            
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
            
            
            for state in self.ProductOnMapArray {
                let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                state_marker.title = state.ProductName
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                state_marker.map = self.mapView
                state_marker.userData = state
                self.markers.append(state_marker)
                
            }
        })
    }

    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //        customInfoWindow?.removeFromSuperview()
        
        self.index = 0
        self.DetailsView.isHidden = true
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
        //        let update = GMSCameraUpdate.zoom(by: 10)
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
        //        GmapView.moveCamera(update)
        mapView.animate(to: cameraPosition)
    }
    
    func centerInMarker(marker: GMSMarker) {
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        mapView?.moveCamera(updateOne)
    }
    
    
    @IBAction func GNextBtn(_ sender: UIButton) {
        
        if index == self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "No More Product Found")
        }
        else if index > self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "No More Product Found")
        }
        else
        {
            index = index + 1
            
            if index == self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
            else if index > self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
                
            else
            {
                self.DetailsView.isHidden = false
                
                let tempdict = markers[index].userData as! MapListing
                
                self.dispatchGroup.enter()
                
                self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
                self.GProductName.text = "\(tempdict.ProductName ?? "")"
                self.GproductType.text = "\(tempdict.SellerImage ?? "")"
                self.GProductAddress.text = "\(tempdict.ReservePrice ?? "")"
                self.GSellerName.text = "Business : " + "\(tempdict.WatchlistStatus ?? "")"
                self.GProductPrice.text =  "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
                
                self.dispatchGroup.leave()
                
                self.DetailsView.isHidden = false
                self.view.bringSubviewToFront(self.DetailsView)
                
                self.dispatchGroup.notify(queue: .main, execute: {
                    let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                    self.mapView?.moveCamera(updateOne)
                })
                
            }
        }
        
    }
    @IBOutlet weak var GPreviousBtnOutlet: UIButton!
    @IBOutlet weak var GNextBtnOutlet: UIButton!
    @IBAction func GPreviousBtn(_ sender: UIButton) {
        
        index = index - 1
        
        if self.index == 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
            
        else if self.index > self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else if self.index < 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else
        {
            self.DetailsView.isHidden = false
            
            let tempdict = markers[index].userData as! MapListing
            
            self.dispatchGroup.enter()
            
//            if (tempdict.WatchlistStatus?.elementsEqual("0"))!
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = false
//                self.GProductDetailsWatchlistedOutlet.isHidden = true
//            }
//            else
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = true
//                self.GProductDetailsWatchlistedOutlet.isHidden = false
//            }
            
            self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
            self.GProductName.text = "\(tempdict.ProductName ?? "")"
            self.GproductType.text = "\(tempdict.SellerImage ?? "")"
            self.GProductAddress.text = "\(tempdict.ReservePrice ?? "")"
            self.GSellerName.text = "Business : " + "\(tempdict.WatchlistStatus ?? "")"
            self.GProductPrice.text =  "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
            
            self.dispatchGroup.leave()
            
            self.DetailsView.isHidden = false
            self.view.bringSubviewToFront(self.DetailsView)
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                self.mapView?.moveCamera(updateOne)
            })
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = mapView.selectedMarker {
            selectedMarker.icon = GMSMarker.markerImage(with: nil)
        }
        mapView.selectedMarker = marker
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        
        self.DetailsView.isHidden = false
        
        let tempdict = marker.userData as! MapListing
        
        self.dispatchGroup.enter()
        
//        if (tempdict.WatchlistStatus?.elementsEqual("0"))!
//        {
//            self.GProductDetailsWatchlistOutlet.isHidden = false
//            self.GProductDetailsWatchlistedOutlet.isHidden = true
//        }
//        else
//        {
//            self.GProductDetailsWatchlistOutlet.isHidden = true
//            self.GProductDetailsWatchlistedOutlet.isHidden = false
//        }
        
        self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
        self.GProductName.text = "\(tempdict.ProductName ?? "")"
        self.GproductType.text = "\(tempdict.SellerImage ?? "")"
        self.GProductAddress.text = "\(tempdict.ReservePrice ?? "")"
        self.GSellerName.text = "Business : " + "\(tempdict.WatchlistStatus ?? "")"
        self.GProductPrice.text =  "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
        
        self.dispatchGroup.leave()
        
        self.DetailsView.isHidden = false
        self.view.bringSubviewToFront(self.DetailsView)
        
        //        self.GProductDetailsView.isHidden = true
        return true
    }
    
    

    // MARK:- // View Will Appear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.ListingTableview.rowHeight = UITableView.automaticDimension
        self.ListingTableview.estimatedRowHeight = UITableView.automaticDimension

    }

    


    //MARK:- // JSON Post Method to Get Product Listing


    func GetListing()
    {

        self.MyCountryName = UserDefaults.standard.string(forKey: "myCountryName") ?? ""
        self.MyCountryId = UserDefaults.standard.string(forKey: "countryID")

        let Parameters = "user_id=\(userID!)&mycountry_name=\(MyCountryName!)&start_value=\(start_value)&per_value=\(per_value)&lang_id=\(langID!)&sorting_type=\(sortingType!)&type=\(user_type!)&map_list_status=\(mapStatus!)&per_page=\(PerPage!)&filter=\(filter!)&condition=\(condition!)&mycountry_id=\(MyCountryId!)&sort_search_region_id=\(sortSearchRegionId!)&sort_search_city_id=\(SortSearchCityId!)&min_amount=\(MinAmount!)&max_amount=\(MaxAmount!)&user_acc_type_search=\(UserAccountTypeSearch!)&search_by=\(SearchBy!)&product_barcode=\(ProductBarcode!)&real_lattitube=\(realLatitude!)&real_langitube=\(realLongitude!)&reale_date=\(RealDate!)&max_dist=\(self.maxFilterDistance!)&search_option1=\(self.searchOptionOneCommaSeparatedString!)&search_option2=\(self.searchOptionTwoCommaSeparatedString!)&max_amount=\(self.maxFilterPrice!)"

        self.CallAPI(urlString: "app_realestate_listing", param: Parameters, completion: {

            self.ProductInfo = self.globalJson["product_info"] as? NSDictionary
            self.Product_listing = self.ProductInfo["product_listing"] as? NSMutableArray

            self.nextStart = "\(self.globalJson["next_start"]!)"

            for i in 0..<((self.globalJson["product_info"] as! NSDictionary)["option_list"] as! NSArray).count
            {
                self.SortingArray.add("\(((self.globalJson["product_info"] as! NSDictionary)["option_list"] as! NSArray)[i] as! NSMutableDictionary)")
            }


            for j in 0..<self.LocalSortArray.count
            {
                self.SortingArray.add("\(self.LocalSortArray[j] as NSDictionary)")
            }

            //            print("SortingArray",self.SortingArray!)



            self.start_value = self.start_value + self.recordsArray.count
            for i in 0..<((self.globalJson["product_info"] as! NSDictionary)["product_listing"] as! NSArray).count
            {
                let ProDetail = self.Product_listing[i] as! NSDictionary
                print("ProDetail is",ProDetail)
                self.recordsArray.add(((self.globalJson["product_info"] as! NSDictionary)["product_listing"] as! NSArray)[i] as! NSMutableDictionary)
            }
            print("ProDetails",self.Product_listing!)


            self.sortArray.removeAllObjects()

            self.makeSortArray(fromArray: self.ProductInfo["option_list"] as! NSArray, toArray: self.sortArray)

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()

                self.ListingTableview.delegate = self
                self.ListingTableview.dataSource = self
                self.ListingTableview.reloadData()


                self.ListingCollectionView.delegate = self
                self.ListingCollectionView.dataSource = self
                self.ListingCollectionView.reloadData()
                
                self.AppendMapData()

                SVProgressHUD.dismiss()
            }
        })
    }

    // MARK:- // Buttons

    // MARK:- // Sort Button

    @IBAction func SortBtnAction(_ sender: UIButton) {
        
        if self.mapView.isHidden == false
        {
           self.mapView.isHidden = true
        }
        self.DetailsView.isHidden = true

        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

    }


    // MARK:- // Define Filter Button Actions

    func defineSortButtonActions() {

        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)


         self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

    }


    //MARK:- // Sort Action

    @objc func sortAction(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

        self.ShowSortView.isHidden = false
        
        self.view.bringSubviewToFront(self.ShowSortView)
    }

    //MARK:- // Filter Action

    @objc func filterAction(sender: UIButton) {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

        navigate.sortArray = self.sortArray.mutableCopy() as? NSMutableArray

        navigate.tempParams = tempParams

        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newrealestate") as! NewRealEstateListingViewController

        print(self.ProductInfo["filter_country_list"] as! NSArray)

        navigate.locationArray = self.ProductInfo["filter_country_list"] as! NSArray

        navigate.typeArray = [sortClass(key: "P", value: "Asking Price", isClicked: false, totalNo: "nil"),sortClass(key: "B", value: "Buy Now", isClicked: false, totalNo: "nil"),sortClass(key: "A", value: "Auction", isClicked: false, totalNo: "nil"),sortClass(key: "R", value: "Rental", isClicked: false, totalNo: "nil"),sortClass(key: "F", value: "Flatmate", isClicked: false, totalNo: "nil"),sortClass(key: "I", value: "Investment", isClicked: false, totalNo: "nil")]

        navigate.filterArray = [sortClass(key: "S", value: "On Sale", isClicked: false, totalNo: "nil"),sortClass(key: "C", value: "Cash on Delivery", isClicked: false, totalNo: "nil")]

        navigate.showDynamicType = true
        navigate.showLocation = true
        navigate.showCondition = true
        navigate.showType = true
        navigate.showFilter = true
        navigate.showPrice = true
        navigate.showDistance = true

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    //MARK:- // Sort Action

    @objc func tapToClose(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

    }



    // MARK:- // MApview Action

    @IBAction func MapViewAction(_ sender: UIButton) {
        self.mapView.isHidden = false
    }



    // MARK:- // SWitch List and Grid

    @IBOutlet weak var ListGridBtnOutlet: UIButton!
    @IBAction func ListGridBtnAction(_ sender: UIButton) {
        
        if self.mapView.isHidden == false
        {
           self.mapView.isHidden = true
        }
        self.DetailsView.isHidden = true

        //self.ListingTableview.isHidden = !self.ListingTableview.isHidden

        if self.ListingTableview.isHidden == false
        {
            self.ListingTableview.isHidden = true
            self.ListingCollectionView.isHidden = false
            self.ListGridBtnOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        }
        else
        {
            self.ListingTableview.isHidden = false
            self.ListingCollectionView.isHidden = true
            self.ListGridBtnOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        }
    }





    @IBAction func PropertyTypeBtnAction(_ sender: UIButton) {
    }


    //MARK:- // Add to Watchlist Function

    @objc func add(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.ShowAlertMessage(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion:
                {
                    self.start_value = 0
                    self.recordsArray.removeAllObjects()
                    self.GetListing()
            })

        }
    }



    // MARK:- // Remove From Watchlist Function

    @objc func remove(sender: UIButton)
    {

        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.ShowAlertMessage(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion:
                {
                    self.start_value = 0
                    self.recordsArray.removeAllObjects()
                    self.GetListing()
            })

        }
    }



    // MARK:- //


    func SortFunction()
    {
        self.ShowSortView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.ShowSortView.addGestureRecognizer(tap)
    }


    // MARK:- // Sortview Tap Gesture

    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.ShowSortView.isHidden = true
    }
    
    //Marker Highlight Method
    
    func highlight(_ marker: GMSMarker?) {
        if mapView?.selectedMarker != nil {
        }
        do {
            marker?.icon = UIImage(named: "marker-selected-icon")
        }
    }
    
    //MARK:- //Marker Unhighlighting method
    
    func unhighlightMarker(_ marker: GMSMarker?) {
        marker?.icon = UIImage(named: "marker-icon")
    }

    
//    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        if isMarkerActive == true {
//            if mapView.selectedMarker != nil {
//                isMarkerActive = false
//                unhighlightMarker(selectedMarker)
//                selectedMarker = nil
//                mapView.selectedMarker = nil
//            }
//        }
//    }

}




// MARK:- // Tableview Delegates

extension NewRealEstateListingViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count

    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        
        let obj = tableView.dequeueReusableCell(withIdentifier: "listingview", for: indexPath) as! NewRealEstateListingTableViewCell
        
        obj.FeaturesCollectionView.isUserInteractionEnabled = false
        
         self.SelectedInt = indexPath.row

        obj.PropertyWatchlistBtn.layer.masksToBounds = true
        obj.PropertyWatchlistBtn.layer.cornerRadius = 10.0

        obj.PropertyAddress.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"// + " " + "from city center"

        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
        obj.PropertyImage.sd_setImage(with: URL(string: path))

        obj.PropertyClosingTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["endtime"]!)"

        obj.PropertyOwner.text = "\(((recordsArray[indexPath.row] as! NSDictionary)["user_details"] as! NSDictionary)["User_type"] ?? "")" + " : \((recordsArray[indexPath.row] as! NSDictionary)["business_name"] ?? "")"

        obj.PropertyListedTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)" + " ago"

        obj.PropertyName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"

        obj.PropertyPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"

        obj.PropertySize.text = "\((recordsArray[indexPath.row] as! NSDictionary)["total_square_m"]!)" + " " + "sq.m."

//        for view in obj.scrollContentview.subviews {
//            view.removeFromSuperview()
//        }

        ///*

        // Features Section
//        let featuresArray = (((recordsArray[indexPath.row] as! NSDictionary)["icon_show"] as! NSArray))
//
//        self.featureBackgroundViewArray.removeAll()

        //obj.scrollContentviewWidthConstraint.isActive = false

//        if featuresArray.count > 0 {
//
//            obj.featuresScroll.isHidden = false
//
//
//            let tempCount : Int!
//
//            if featuresArray.count > 4 {
//                tempCount = 4
//            }
//            else {
//                tempCount = featuresArray.count
//            }
//
//            for i in 0..<tempCount {
//
//                let backgroundView : UIView = {
//                    let backgroundview = UIView()
//                    backgroundview.translatesAutoresizingMaskIntoConstraints = false
//                    return backgroundview
//                }()
//
//                self.featureBackgroundViewArray.append(backgroundView)
//
//                let featureImageview : UIImageView = {
//                    let featureimageview = UIImageView()
//                    featureimageview.translatesAutoresizingMaskIntoConstraints = false
//                    return featureimageview
//                }()
//
//                let featureCount : UILabel = {
//                    let featurecount = UILabel()
//                    featurecount.translatesAutoresizingMaskIntoConstraints = false
//                    return featurecount
//                }()
//
//                obj.scrollContentview.addSubview(backgroundView)
//                backgroundView.addSubview(featureImageview)
//                backgroundView.addSubview(featureCount)
//
//                if featuresArray.count > 1 {
//
//                    if i == 0 {
//                        backgroundView.anchor(top: obj.scrollContentview.topAnchor, leading: obj.scrollContentview.leadingAnchor, bottom: obj.scrollContentview.bottomAnchor, trailing: nil)
//                    }
//                    else if i == featuresArray.count - 1 {
//                        backgroundView.anchor(top: obj.scrollContentview.topAnchor, leading: self.featureBackgroundViewArray[i-1].trailingAnchor, bottom: obj.scrollContentview.bottomAnchor, trailing: obj.scrollContentview.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 0))
//                    }
//                    else {
//                        backgroundView.anchor(top: obj.scrollContentview.topAnchor, leading: self.featureBackgroundViewArray[i-1].trailingAnchor, bottom: obj.scrollContentview.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 20, bottom: 0, right: 0))
//                    }
//
//                }
//                else {
//                    backgroundView.anchor(top: obj.scrollContentview.topAnchor, leading: obj.scrollContentview.leadingAnchor, bottom: obj.scrollContentview.bottomAnchor, trailing: obj.scrollContentview.trailingAnchor)
//                }
//
//                featureImageview.anchor(top: backgroundView.topAnchor, leading: backgroundView.leadingAnchor, bottom: backgroundView.bottomAnchor, trailing: nil, size: .init(width: 25, height: 0))
//
//                featureCount.anchor(top: backgroundView.topAnchor, leading: featureImageview.trailingAnchor, bottom: backgroundView.bottomAnchor, trailing: backgroundView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
//
//                featureImageview.sd_setImage(with: URL(string: "\((featuresArray[i] as! NSDictionary)["icon"] ?? "")"))
//
//                if"\((featuresArray[i] as! NSDictionary)["count_icon"]!)".isEmpty {
//                    featureCount.text = "0"
//                }
//                else {
//                    featureCount.text = "\((featuresArray[i] as! NSDictionary)["count_icon"] ?? "")"
//                }
//
//
//
//            }
//
//        }
//        else {
//            obj.featuresScroll.isHidden = true
//        }
//
//        //*/




        // Watchlist Button
        obj.PropertyWatchlistBtn.tag = indexPath.row
        obj.PropertyWatchlistedBtn.tag = indexPath.row

        if "\(( recordsArray [indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1") {
            obj.PropertyWatchlistedBtn.isHidden = false
            obj.PropertyWatchlistBtn.isHidden = true
            obj.PropertyWatchlistedBtn.addTarget(self, action: #selector(NewRealEstateListingViewController.remove(sender:)), for: .touchUpInside)
        }
        else if "\(( recordsArray [indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
        {
            obj.PropertyWatchlistedBtn.isHidden = true
            obj.PropertyWatchlistBtn.isHidden = false
            obj.PropertyWatchlistBtn.addTarget(self, action: #selector(NewRealEstateListingViewController.add(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.PropertyWatchlistedBtn.isHidden = true
            obj.PropertyWatchlistBtn.isHidden = false
        }
        
        obj.FeaturesCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        obj.selectionStyle = .none


        // SEt Font
        obj.PropertyName.font = UIFont(name: obj.PropertyName.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        obj.PropertySize.font = UIFont(name: obj.PropertySize.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        obj.PropertyListedTime.font = UIFont(name: obj.PropertyListedTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        obj.PropertyClosingTime.font = UIFont(name: obj.PropertyClosingTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        obj.PropertyOwner.font = UIFont(name: obj.PropertyOwner.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        obj.PropertyAddress.font = UIFont(name: obj.PropertyAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        obj.PropertyType.font = UIFont(name: obj.PropertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        obj.PropertyPrice.font = UIFont(name: obj.PropertyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))

        return obj


    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let Proid = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        //        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "realestatedetail") as! RealListingDetailViewController
        //        nav.ProductId = Proid
        //        self.navigationController?.pushViewController(nav, animated: true)

        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateDetailsVC") as! NewRealEstateDetailsViewController
        nav.productID = Proid
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? NewRealEstateListingTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }

}



// MARK:- // Collectionview Delegates

extension NewRealEstateListingViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//
//        return 1
//    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ListingCollectionView
        {
            return self.recordsArray.count
        }
        else
        {
            let featuresArray = ((recordsArray[SelectedInt] as! NSDictionary)["icon_show"] as! NSArray)
            print("CountNumber",featuresArray.count)
            return featuresArray.count
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ListingCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridview", for: indexPath) as! NewRealEstateListingCollectionViewCell
            let path = "\((recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
            
            cell.PropertyImage.sd_setImage(with: URL(string: path))
            
            cell.address.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
            
            cell.PropertyName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            cell.PropertyListingTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)" + " ago"
            cell.PropertyClosingTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["endtime"]!)"
            cell.PropertySize.text = "\((recordsArray[indexPath.row] as! NSDictionary)["total_square_m"]!)" + "Sq-m"
            cell.PropertyOwner.text = "\((recordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
            cell.PropertyType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
            cell.PropertyPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            
            cell.PropertyWatchlistBtn.tag = indexPath.row
            cell.PropertyWatchlistedBtn.tag = indexPath.row
            
            
            if "\(( recordsArray [indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1") {
                cell.PropertyWatchlistedBtn.isHidden = false
                cell.PropertyWatchlistBtn.isHidden = true
                cell.PropertyWatchlistedBtn.addTarget(self, action: #selector(NewRealEstateListingViewController.remove(sender:)), for: .touchUpInside)
            }
            else if "\(( recordsArray [indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
            {
                cell.PropertyWatchlistedBtn.isHidden = true
                cell.PropertyWatchlistBtn.isHidden = false
                cell.PropertyWatchlistBtn.addTarget(self, action: #selector(NewRealEstateListingViewController.add(sender:)), for: .touchUpInside)
            }
            else
            {
                cell.PropertyWatchlistedBtn.isHidden = true
                cell.PropertyWatchlistBtn.isHidden = false
            }
            
            
            
            // SEt Font
            cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyName.font = UIFont(name: cell.PropertyName.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            cell.PropertySize.font = UIFont(name: cell.PropertySize.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyListingTime.font = UIFont(name: cell.PropertyListingTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyClosingTime.font = UIFont(name: cell.PropertyClosingTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyOwner.font = UIFont(name: cell.PropertyOwner.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            //cell.PropertyAddress.font = UIFont(name: cell.PropertyAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            cell.PropertyType.font = UIFont(name: cell.PropertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
            cell.PropertyPrice.font = UIFont(name: cell.PropertyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            
            
            return cell
        }
        else 
        {
            let tempfeaturesArray = ((recordsArray[SelectedInt] as! NSDictionary)["icon_show"] as! NSArray)
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feature", for: indexPath) as! featuresCVC
//            cell.featureCount.text = "0"
//            print("CountFeature",featuresArray.count)
            cell.featureCount.text = "\((tempfeaturesArray[indexPath.item] as! NSDictionary)["count_icon"] ?? "")"
            let path = "\((tempfeaturesArray[indexPath.item] as! NSDictionary)["icon"] ?? "")"
            print("ImagePath",path)
            cell.featureImage.sd_setImage(with: URL(string: path))
            cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            return cell
        }
        
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ListingCollectionView
        {
            let padding : CGFloat! = 4
            
            let collectionViewSize = ListingCollectionView.frame.size.width - padding
            
            return CGSize(width: collectionViewSize/2, height: 300)
        }
        else
        {
            return CGSize(width: 68, height: 25)
        }
        
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 0

    }

}


// MARK:- // Pickerview Delegates

extension NewRealEstateListingViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sortDict.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((self.sortDict[row])["value"]!)"
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sortingType = "\((self.sortDict[row])["key"]!)"
//        print("SortingType",sortingType)
        self.ShowSortView.isHidden = true
        self.start_value = 0
        self.recordsArray.removeAllObjects()
        self.GetListing()
    }

}
// MARK:- // Scrollview Delegates

extension NewRealEstateListingViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                //GetListing()
            }
        }
    }




    // MARK:- // MAke Sorting Array

    func makeSortArray(fromArray: NSArray , toArray: NSMutableArray)
    {

        for i in 0..<fromArray.count
        {

            var tempArray = [sortClass]()

            for j in 0..<((fromArray[i] as! NSDictionary)["option_value"] as! NSArray).count
            {

                tempArray.append(sortClass(key: "\((((fromArray[i] as! NSDictionary)["option_value"] as! NSArray)[j] as! NSDictionary)["option_value_id"] ?? "")", value: "\((((fromArray[i] as! NSDictionary)["option_value"] as! NSArray)[j] as! NSDictionary)["value_name"] ?? "")", isClicked: false , totalNo: "\((((fromArray[i] as! NSDictionary)["option_value"] as! NSArray)[j] as! NSDictionary)["count"] ?? "")"))

            }

            //            let tempArrayData = NSKeyedArchiver.archivedData(withRootObject: tempArray)

            let tempDict = ["id" : "\((fromArray[i] as! NSDictionary)["option_name_id"] ?? "")" , "key" : "\((fromArray[i] as! NSDictionary)["option_name"] ?? "")" , "value" : tempArray] as [String : Any]

            toArray.add(tempDict)

        }

    }

}






//MARK:- //Real Estate Listing TableView Cell Class
class NewRealEstateListingTableViewCell: UITableViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var PropertyImage: UIImageView!
    @IBOutlet weak var PropertyName: UILabel!
    @IBOutlet weak var PropertyWatchlistBtn: UIButton!
    @IBOutlet weak var PropertySize: UILabel!
    @IBOutlet weak var PropertyListedTime: UILabel!
    @IBOutlet weak var PropertyClosingTime: UILabel!
    @IBOutlet weak var PropertyOwner: UILabel!
    @IBOutlet weak var PropertyType: UILabel!
    @IBOutlet weak var PropertyAddress: UILabel!
    @IBOutlet weak var PropertyPrice: UILabel!
    @IBOutlet weak var PropertyWatchlistedBtn: UIButton!
    @IBOutlet weak var featuresScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var FeaturesCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
//MARK:- //Real Estate Listing CollectionView Cell Class
class NewRealEstateListingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var PropertyImage: UIImageView!
    @IBOutlet weak var PropertyName: UILabel!
    @IBOutlet weak var PropertySize: UILabel!
    @IBOutlet weak var PropertyListingTime: UILabel!
    @IBOutlet weak var PropertyClosingTime: UILabel!
    @IBOutlet weak var PropertyOwner: UILabel!
    @IBOutlet weak var PropertyType: UILabel!
    @IBOutlet weak var PropertyPrice: UILabel!
    @IBOutlet weak var PropertyWatchlistBtn: UIButton!
    @IBOutlet weak var PropertyWatchlistedBtn: UIButton!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var placeholderImageview: UIImageView!
    @IBOutlet weak var address: UILabel!
}

class featuresCVC : UICollectionViewCell
{
    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var featureCount: UILabel!
}

extension NewRealEstateListingTableViewCell
{
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        FeaturesCollectionView.delegate = dataSourceDelegate
        FeaturesCollectionView.dataSource = dataSourceDelegate
        FeaturesCollectionView.tag = row
        FeaturesCollectionView.setContentOffset(FeaturesCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        FeaturesCollectionView.reloadData()
    }
    
//    var collectionViewOffset: CGFloat {
//        set { FeaturesCollectionView.contentOffset.x = newValue }
//        get { return FeaturesCollectionView.contentOffset.x }
//    }
}

struct MapListing
{
    var ProductName : String?
    var ProductLatitude : CLLocationDegrees?
    var ProductLongitude : CLLocationDegrees?
    var ProductImage : String?
    var CurrencySymbol : String?
    var StartPrice : String?
    var ReservePrice : String?
    var ProductID : String?
    var SellerImage : String?
    var WatchlistStatus : String?
    
    init(ProductName : String , ProductLatitude : CLLocationDegrees , ProductLongitude : CLLocationDegrees , ProductImage : String , CurrencySymbol : String , StartPrice : String , ReservePrice : String , ProductID : String , SellerImage : String , WatchlistStatus : String)
    {
        self.ProductName = ProductName
        self.ProductLatitude = ProductLatitude
        self.ProductLongitude = ProductLongitude
        self.ProductImage = ProductImage
        self.CurrencySymbol = CurrencySymbol
        self.StartPrice = StartPrice
        self.ReservePrice = ReservePrice
        self.ProductID = ProductID
        self.SellerImage = SellerImage
        self.WatchlistStatus = WatchlistStatus
        
        
    }
}





