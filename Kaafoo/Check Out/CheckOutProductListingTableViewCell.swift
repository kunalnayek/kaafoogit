//
//  CheckOutProductListingTableViewCell.swift
//  Kaafoo
//
//  Created by Kaustabh on 11/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class CheckOutProductListingTableViewCell: UITableViewCell {
    @IBOutlet var Product_name: UILabel!
    @IBOutlet var reserve_price: UILabel!
    @IBOutlet var start_price: UILabel!
    @IBOutlet var Product_image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

