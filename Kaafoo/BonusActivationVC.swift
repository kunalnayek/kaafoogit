//
//  BonusActivationVC.swift
//  Kaafoo
//
//  Created by priya on 25/07/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BonusActivationVC: GlobalViewController {
    
    
    @IBOutlet weak var bonuappliedBtn: UIButton!
    @IBOutlet weak var termsandconditions: UILabel!
    @IBOutlet weak var checkbox: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bonuappliedBtn.layer.cornerRadius = 15
      
    }
    
    @IBAction func BonusappliedbtnClicked(_ sender: Any)
    {
        self.getdata()
    }
    
    
    func getdata()
    {
        
        let LangID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let parameters = "user_id=\(userID!)&lang_id=\(LangID!)"
       
        self.CallAPI(urlString: "app_bonus_activation", param: parameters, completion: {
              
                   DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        SVProgressHUD.dismiss()
                        //let responseMessage = "\(self.globalJson["message"]!)"
                        //self.ShowAlertMessage(title: "warning", message: responseMessage)
                      
                    }
            })

    }
    
    

}
