//
//  SignUpTwoViewController.swift
//  Kaafoo
//
//  Created by admin on 14/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SignUpTwoViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    @IBOutlet weak var signLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var businessLbl: UILabel!
    @IBOutlet weak var businessBottomView: UIView!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var maleLbl: UILabel!
    @IBOutlet weak var maleImage: UIImageView!
    @IBOutlet weak var femaleLbl: UILabel!
    @IBOutlet weak var femaleImage: UIImageView!
    @IBOutlet weak var dobTXT: UITextField!
    @IBOutlet weak var landLineNoTXT: UITextField!
    @IBOutlet weak var dobPicker: UIDatePicker!
    @IBOutlet weak var dobView: UIView!
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var previousButtonOutlet: UIButton!
    @IBAction func tapPreviousBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    
    var Gender : String! = ""
    
    var click : Int = 0
    
    
    //MARK:- //Add Done Button to Picker Programmatically
    
    func addToolBar()
    {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.backgroundColor = .black
        toolBar.barTintColor = .black
        toolBar.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: 20)
       // toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
//        self.dobTXT.inputView = self.dobPicker
//        self.dobTXT.inputAccessoryView = toolBar
        self.dobPicker.addSubview(toolBar)
        
    }
    
    @objc func donePicker(sender : UIButton)
    {
        print("Nothing")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addToolBar()
        self.dobPicker.maximumDate = Date()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
               view.addGestureRecognizer(tap)
        self.DefaultGender()
        
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (landLineNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")
        
        landLineNoTXT.delegate = self
        
        set_font()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.dobPicker.isHidden = false
//        self.addToolBar()
//    }
    
    //MARK:- //Dismiss Keyboard
       
       @objc func dismissKeyboard() {
           view.endEditing(true)
       }
    
    // MARK:- // Buttons
    
    func DefaultGender()
    {
        maleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
        
        femaleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
        
        maleImage.image = UIImage(named : "002-male")
        femaleImage.image = UIImage(named : "001-female copy")
        
        Gender = "M"
    }
    
    // MARK:- // Click on Male
    
    @IBAction func clickOnMale(_ sender: UIButton) {
       
        maleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
        
        femaleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
        
        maleImage.image = UIImage(named : "002-male")
        femaleImage.image = UIImage(named : "001-female copy")
        
        Gender = "M"
        
//        print("Gender is : ", Gender)
        
    }
    
    
    // MARK:- // Click on Female
    
    @IBAction func clickOnFemale(_ sender: UIButton) {
       
        femaleLbl.textColor = UIColor(red:64/255, green:64/255, blue:64/255, alpha: 1)
        
        maleLbl.textColor = UIColor(red:185/255, green:185/255, blue:185/255, alpha: 1)
        
        maleImage.image = UIImage(named : "002-male copy")
        femaleImage.image = UIImage(named : "001-female")
        
        Gender = "F"
        
//        print("Gender is : ", Gender)
        
    }
    
    
    // MARK:- // Click On Date of Birth
    
    @IBAction func clickOnDob(_ sender: UIButton) {
        
        
        
        if click == 0
        {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {
                  
                    self.dobView.isHidden = false
                    self.click = 1
                    
//                    self.dobLbl.frame.origin.y = ((270/568)*self.FullHeight)
//                    self.dobLbl.font = UIFont(name: (self.dobLbl.font?.fontName)!,size: 11)
//                    self.dobLbl.font = UIFont(name: self.dobLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
//
//                    if (self.landLineNoTXT.text?.isEmpty)!
//                    {
//                        self.landLineGetBackInPosition()
//
//                    }
                    
            }, completion: { (finished: Bool) in})
            
        }
        else
        {
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations:
                {
                    let dateformat = DateFormatter()
                    
                    dateformat.dateFormat = "yyyy-MM-dd"
                    
                    self.dobTXT.text = dateformat.string(from: self.dobPicker.date)
                    
                    self.dobView.isHidden = true
                    
                    self.click = 0
                    
            }, completion: { (finished: Bool) in})
            
            
        }
        
        
        
    }
    
    
    /*
    // MARK:- // Click on Land Line
    
    
    @IBOutlet weak var clickOnLandLineOutlet: UIButton!
    @IBAction func clickOnLandLine(_ sender: Any) {
        
        
        UIView.animate(withDuration: 0.3){
            
            self.landLineLbl.frame.origin.y = (self.landLineLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.landLineLbl.font = UIFont(name: (self.landLineLbl.font?.fontName)!,size: 11)
            self.landLineLbl.font = UIFont(name: self.landLineLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnLandLineOutlet.isUserInteractionEnabled = false
            self.landLineNoTXT.isUserInteractionEnabled = true
            self.landLineNoTXT.becomeFirstResponder()
            if (self.dobTXT.text?.isEmpty)!
            {
                self.dobGetBackInPosition()
                
            }
            
        }
        
    }
    
    */
    
    // MARK:- // Buttons
    
    
    // MARK:- // Next Page

    @IBAction func nextButton(_ sender: UIButton) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpThreeVC") as! SignUpThreeViewController
        
        addDataInArray()
        
        print("Sign UP Info Array : ", signUpInfoArray)
        
        navigate.signUpInfoArray = signUpInfoArray.mutableCopy() as! NSMutableArray
        
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Click on Private SEction
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
    }
    
    
    // MARK:- // Click on Business Section
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        
        navigate.signUpType = "Business"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Cross Button
    
    
    @IBOutlet weak var crossImageView: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    
    
    //MARK:- // Defined Functions
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        businessLbl.font = UIFont(name: businessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        genderLbl.font = UIFont(name: genderLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        dobTXT.font = UIFont(name: (dobTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        landLineNoTXT.font = UIFont(name: (landLineNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        nextButtonOutlet.titleLabel?.font = UIFont(name: (nextButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        
    }
    
    
    // MARK:- // Add Sign Up Info In Array
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(Gender)
        signUpInfoArray.add(dobTXT.text!)
        signUpInfoArray.add(landLineNoTXT.text!)
        
    }
    
    
    /*
    // MARK:- // Date of Birth get back in position
    
    
    func dobGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.dobLbl.frame.origin.y = self.dobTXT.frame.origin.y
            self.dobLbl.font = UIFont(name: (self.dobLbl.font?.fontName)!,size: 16)
            self.dobLbl.font = UIFont(name: self.dobLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            
        }
    }
    
    
    // MARK:- // Land Line get back in position
    
    func landLineGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.landLineLbl.frame.origin.y = self.landLineNoTXT.frame.origin.y
            self.landLineLbl.font = UIFont(name: (self.landLineLbl.font?.fontName)!,size: 16)
            self.landLineLbl.font = UIFont(name: self.landLineLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnLandLineOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    
    // MARK:- // Validate with Alerts
    
    func validateWithAlerts()
    {
        
        if (Gender.elementsEqual(""))
        {
            let alert = UIAlertController(title: "Gender Not Selected", message: "Please select your gender", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (dobTXT.text?.isEmpty == true)
        {
            let alert = UIAlertController(title: "Date of Birth Empty", message: "Please Enter Your Date of Birth", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (landLineNoTXT.text?.isEmpty == false)
        {
            if self.landLineNoTXT.text?.isPhoneNumber == false
            {
                self.ShowAlertMessage(title: "warning", message: "Phone Number is not in correct format")
            }
            else
            {
                //do nothing
            }
        }
            
//        else if (displayNameTXT.text?.isEmpty)!
//        {
//            let alert = UIAlertController(title: "Display Name Empty", message: "Please Enter Display Name", preferredStyle: .alert)
//
//            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
//
//            alert.addAction(ok)
//
//            self.present(alert, animated: true, completion: nil )
//        }
        
    }
    
    
    /*
    // MARK:- // Textfield Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        landLineNoTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.landLineNoTXT.text?.isEmpty)!
        {
            self.landLineGetBackInPosition()
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == landLineNoTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (90/568)*self.FullHeight), animated: true)
            
        }
        
    }
    
  */
}
