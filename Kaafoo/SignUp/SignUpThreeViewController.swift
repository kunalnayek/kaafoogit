//
//  SignUpThreeViewController.swift
//  Kaafoo
//
//  Created by admin on 14/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SignUpThreeViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    @IBAction func tapPreviousButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var PreviousButtonOutlet: UIButton!
    @IBOutlet weak var signLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var BusinessLbl: UILabel!
    @IBOutlet weak var businessBottomView: UIView!
    @IBOutlet weak var mobileNoTXT: UITextField!
    @IBOutlet weak var addressTXT: UITextField!
    @IBOutlet weak var nearbyTownTXT: UITextField!
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")
        self.mobileNoTXT.keyboardType = .numberPad
        mobileNoTXT.delegate = self
        addressTXT.delegate = self
        nearbyTownTXT.delegate = self
        
        set_font()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    //MARK:- //Dismiss Keyboard
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK:- // Buttons
    
    
    
    
    /*
    // MARK:- // Click on Mobile Number
    
    
    @IBOutlet weak var clickOnMobileNoOutlet: UIButton!
    @IBAction func clickOnMobileNo(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.mobileNoLbl.frame.origin.y = (self.mobileNoLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.mobileNoLbl.font = UIFont(name: (self.mobileNoLbl.font?.fontName)!,size: 11)
            self.mobileNoLbl.font = UIFont(name: self.mobileNoLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnMobileNoOutlet.isUserInteractionEnabled = false
            self.mobileNoTXT.isUserInteractionEnabled = true
            self.mobileNoTXT.becomeFirstResponder()
            
            if (self.addressTXT.text?.isEmpty)!
            {
                self.addressGetBackInPosition()
                if (self.nearbyTownTXT.text?.isEmpty)!
                {
                    self.nearbyTownGetBackInPosition()
                }
            }
            else if (self.nearbyTownTXT.text?.isEmpty)!
            {
                self.nearbyTownGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Click on Address
    
    @IBOutlet weak var clickOnAddressOutlet: UIButton!
    @IBAction func clickOnAddress(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.addressLbl.frame.origin.y = (self.addressLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.addressLbl.font = UIFont(name: (self.addressLbl.font?.fontName)!,size: 11)
            self.addressLbl.font = UIFont(name: self.addressLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnAddressOutlet.isUserInteractionEnabled = false
            self.addressTXT.isUserInteractionEnabled = true
            self.addressTXT.becomeFirstResponder()
            
            if (self.mobileNoTXT.text?.isEmpty)!
            {
                self.mobileNumberGetBackInPosition()
                if (self.nearbyTownTXT.text?.isEmpty)!
                {
                    self.nearbyTownGetBackInPosition()
                }
            }
            else if (self.nearbyTownTXT.text?.isEmpty)!
            {
                self.nearbyTownGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Click on Nearby Town
    
    
    
    @IBOutlet weak var clickOnNearbyTownOutlet: UIButton!
    @IBAction func clickOnNearbyTown(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.nearbyTownLbl.frame.origin.y = (self.nearbyTownLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.nearbyTownLbl.font = UIFont(name: (self.nearbyTownLbl.font?.fontName)!,size: 11)
            self.nearbyTownLbl.font = UIFont(name: self.nearbyTownLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnNearbyTownOutlet.isUserInteractionEnabled = false
            self.nearbyTownTXT.isUserInteractionEnabled = true
            self.nearbyTownTXT.becomeFirstResponder()
            
            if (self.mobileNoTXT.text?.isEmpty)!
            {
                self.mobileNumberGetBackInPosition()
                if (self.addressTXT.text?.isEmpty)!
                {
                    self.addressGetBackInPosition()
                }
            }
            else if (self.addressTXT.text?.isEmpty)!
            {
                self.addressGetBackInPosition()
            }
            
        }
        
    }
    */
    
    
    // MARK:- // Next Button
    

    @IBAction func nextButton(_ sender: UIButton) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpFourVC") as! SignUpFourViewController
        
        addDataInArray()
        
        navigate.signUpInfoArray = signUpInfoArray.mutableCopy() as! NSMutableArray
        
        print("Sign UP Info Array : ", signUpInfoArray)
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Click on Private Section
    
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
    }
    
    
    // MARK:- // Click on Business SEction
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        
        navigate.signUpType = "Business"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Cross Button
    
    
    
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController

        self.navigationController?.pushViewController(navigate, animated: true)
        
        
    }
    
    
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        BusinessLbl.font = UIFont(name: BusinessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        
        mobileNoTXT.font = UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        addressTXT.font = UIFont(name: (addressTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        nearbyTownTXT.font = UIFont(name: (nearbyTownTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        nextButtonOutlet.titleLabel?.font = UIFont(name: (nextButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
    }
    
    // MARK:- // Add Data in Array
    
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(mobileNoTXT.text!)
        signUpInfoArray.add(addressTXT.text!)
        signUpInfoArray.add(nearbyTownTXT.text!)
        
    }
  
    /*
    // MARK:- // Mobile Number get back in position
    
    func mobileNumberGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){

            self.mobileNoLbl.frame.origin.y = self.mobileNoTXT.frame.origin.y
            self.mobileNoLbl.font = UIFont(name: (self.mobileNoLbl.font?.fontName)!,size: 16)
            self.mobileNoLbl.font = UIFont(name: self.mobileNoLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnMobileNoOutlet.isUserInteractionEnabled = true

        }
    }
    
    
    // MARK:- // Address get back in position
    
    func addressGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.addressLbl.frame.origin.y = self.addressTXT.frame.origin.y
            self.addressLbl.font = UIFont(name: (self.addressLbl.font?.fontName)!,size: 16)
            self.addressLbl.font = UIFont(name: self.addressLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnAddressOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    
    // MARK:- // Nearby Town Get back in position
    
    func nearbyTownGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.nearbyTownLbl.frame.origin.y = self.nearbyTownTXT.frame.origin.y
            self.nearbyTownLbl.font = UIFont(name: (self.nearbyTownLbl.font?.fontName)!,size: 16)
            self.nearbyTownLbl.font = UIFont(name: self.nearbyTownLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnNearbyTownOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    
    // MARK:- // Validate with alerts
    
    func validateWithAlerts()
    {
        
        if (mobileNoTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Mobile Number Empty", message: "Please Enter Mobile Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (addressTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Address Empty", message: "Please Enter Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (nearbyTownTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Nearby town Empty", message: "Please Enter Nearby Town", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        
    }
    
    
    
    
    
    /*
    // MARK:- // Textfield Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        mobileNoTXT.resignFirstResponder()
        addressTXT.resignFirstResponder()
        nearbyTownTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.mobileNoTXT.text?.isEmpty)!
        {
            self.mobileNumberGetBackInPosition()
        }
        
        if (self.addressTXT.text?.isEmpty)!
        {
            self.addressGetBackInPosition()
        }
        
        if (self.nearbyTownTXT.text?.isEmpty)!
        {
            self.nearbyTownGetBackInPosition()
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == addressTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        else if textField == nearbyTownTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (90/568)*self.FullHeight), animated: true)
            
        }
        
    }
    
    */
}
