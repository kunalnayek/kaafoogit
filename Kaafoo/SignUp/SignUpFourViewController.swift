//
//  SignUpFourViewController.swift
//  Kaafoo
//
//  Created by admin on 23/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpFourViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    
    @IBOutlet weak var signLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var BusinessLbl: UILabel!
    @IBOutlet weak var businessBottomView: UIView!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var passwordTXT: UITextField!
    @IBOutlet weak var passwordWarningLbl: UILabel!
    
    @IBOutlet weak var registerButtonOutlet: UIButton!
    
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    let dispatchGroup = DispatchGroup()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.passwordTXT.isSecureTextEntry = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
               view.addGestureRecognizer(tap)
        
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")
        
        emailTXT.delegate = self
        passwordTXT.delegate = self
        
        set_font()
        registerOutlet.layer.cornerRadius = registerOutlet.frame.size.height / 2

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    //MARK:- //Dismiss Keyboard
          
          @objc func dismissKeyboard() {
              view.endEditing(true)
          }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Register Button
    
    
    @IBOutlet weak var registerOutlet: UIButton!
    @IBAction func register(_ sender: Any) {
        
        self.validateWithAlerts()
        
        
        

            
            
            
//            let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
        
        
    }
    
    
    /*
    // MARK:- // Click on Email
    
    @IBOutlet weak var clickOnEmailOutlet: UIButton!
    @IBAction func clickOnEmail(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = (self.emailLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 11)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = false
            self.emailTXT.isUserInteractionEnabled = true
            self.emailTXT.becomeFirstResponder()
            if (self.passwordTXT.text?.isEmpty)!
            {
                self.passwordGetBackInPosition()
                
            }
            
        }
        
    }
    
    
    
    // MARK:- // Click on Password
    
    
    @IBOutlet weak var clickOnPasswordOutlet: UIButton!
    @IBAction func clickOnPassword(_ sender: Any) {
        
        
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = (self.passwordLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 11)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = false
            self.passwordTXT.isUserInteractionEnabled = true
            self.passwordTXT.becomeFirstResponder()
            if (self.emailTXT.text?.isEmpty)!
            {
                self.emailGetBackInPosition()
                
            }
            
        }
        
        
        
    }
    */
    
    
    // MARK:- // Click on private Section
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
    }
    
    
    // MARK:- // Click on Business SEction
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        
        navigate.signUpType = "Business"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Cross Button
    
    
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        BusinessLbl.font = UIFont(name: BusinessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        passwordWarningLbl.font = UIFont(name: passwordWarningLbl.font.fontName, size: CGFloat(Get_fontSize(size: 10)))
        
        registerButtonOutlet.titleLabel?.font = UIFont(name: (registerButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
    }
    
    
    // MARK:- // Add Data in Array
    
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(emailTXT.text!)
        signUpInfoArray.add(passwordTXT.text!)
        
    }
    
    
    
    
    /*
    // MARK:- //  Email Get back in Position
    
    func emailGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = self.emailTXT.frame.origin.y
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 16)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    
    // MARK:- // Password get back in position
    
    func passwordGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = self.passwordTXT.frame.origin.y
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 16)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    // MARK:- // Validate with Alerts
    
    
    func validateWithAlerts()
    {
        
        if (emailTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Email Empty", message: "Please Enter Email Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
          
        else if isValidEmail(emailStr: emailTXT.text ?? "") == false
        {
            self.ShowAlertMessage(title: "Warning", message: "Your Email is Not in Correct Format")
        }
        else if (passwordTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Password Empty", message: "Please Enter a Valid Password", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if passwordTXT.text?.isValidPassword() == false
        {
            self.ShowAlertMessage(title: "Alert", message: "Your Psssword must be of 8 Characters")
        }
        else
        {
            dispatchGroup.enter()
            
            addDataInArray()
            
            print("Sign UP Info Array : ", signUpInfoArray)
            
            dispatchGroup.leave()
            
            register()
        }
        
       
        
        
        
    }
    
    //1 Alphabet and 1 Number in iOS
    public func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    
    
    // MARK: - // JSON POST Method to submit SIGNUP Data
    
    func register()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_private_signup")!   //change the url
        
        var parameters : String = ""
        
        parameters = "first_name=\(signUpInfoArray[0])&last_name=\(signUpInfoArray[1])&display_name=\(signUpInfoArray[2])&gender=\(signUpInfoArray[3])&dob=\(signUpInfoArray[4])&landline_no=\(signUpInfoArray[5])&mobile=\(signUpInfoArray[6])&address=\(signUpInfoArray[7])&nearby_town=\(signUpInfoArray[8])&llat=\("23.0987")&long=\("23.0987")&email=\(signUpInfoArray[9])&password=\(signUpInfoArray[10])"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Sign Up Response: " , json)
                    
                    if "\(json["response"]! as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                            self.customAlert(title: "Alert", message: "\(json["message"] ?? "")", completion: {
                                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
                                navigate.newUserID = "\(json["new_userid"]!)"
                                self.navigationController?.pushViewController(navigate, animated: true)
                            })
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                        }
                    }
                 
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    /*
    // MARK:- // Textfield Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        emailTXT.resignFirstResponder()
        passwordTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.emailTXT.text?.isEmpty)!
        {
            self.emailGetBackInPosition()
        }
        
        if (self.passwordTXT.text?.isEmpty)!
        {
            self.passwordGetBackInPosition()
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == passwordTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        
        
    }
*/
    
    
}
