//
//  SIgnUpBusinessThreeViewController.swift
//  Kaafoo
//
//  Created by admin on 23/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class SIgnUpBusinessThreeViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    
    @IBOutlet weak var signLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var businessLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var businessBottomView: UIView!
    @IBOutlet weak var nearbyTownTXT: UITextField!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var passwordTXT: UITextField!
    
    @IBOutlet weak var registerButtonOutlet: UIButton!
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    let dispatchGroup = DispatchGroup()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTXT.isSecureTextEntry = true
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (nearbyTownTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")
        
        set_font()
        
        nearbyTownTXT.delegate = self
        emailTXT.delegate = self
        passwordTXT.delegate = self
        
        
        registerOutlet.layer.cornerRadius = registerOutlet.frame.size.height / 2
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Click on Private Section
    
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        
        navigate.signUpType = "Private"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Click on Business SEction
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
    }
    
    /*
    @IBOutlet weak var clickOnNearbyTownOutlet: UIButton!
    @IBAction func clickOnNearbyTown(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.nearbyTownLbl.frame.origin.y = (self.nearbyTownLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.nearbyTownLbl.font = UIFont(name: (self.nearbyTownLbl.font?.fontName)!,size: 11)
            self.nearbyTownLbl.font = UIFont(name: self.nearbyTownLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnNearbyTownOutlet.isUserInteractionEnabled = false
            self.nearbyTownTXT.isUserInteractionEnabled = true
            self.nearbyTownTXT.becomeFirstResponder()
            if (self.emailTXT.text?.isEmpty)!
            {
                self.emailGetBackInPosition()
                if (self.passwordTXT.text?.isEmpty)!
                {
                    self.passwordGetBackInPosition()
                }
            }
            else if (self.passwordTXT.text?.isEmpty)!
            {
                self.passwordGetBackInPosition()
            }
            
        }
        
    }
    
    
    
    
    // MARK:- // Click on Email
    
    
    @IBOutlet weak var clickOnEmailOutlet: UIButton!
    @IBAction func clickOnEmail(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = (self.emailLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 11)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = false
            self.emailTXT.isUserInteractionEnabled = true
            self.emailTXT.becomeFirstResponder()
            if (self.nearbyTownTXT.text?.isEmpty)!
            {
                self.nearbyTownGetBackInPosition()
                if (self.passwordTXT.text?.isEmpty)!
                {
                    self.passwordGetBackInPosition()
                }
            }
            else if (self.passwordTXT.text?.isEmpty)!
            {
                self.passwordGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Click on Password
    
    
    @IBOutlet weak var clickOnPasswordOutlet: UIButton!
    @IBAction func clickOnPassword(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = (self.passwordLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 11)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = false
            self.passwordTXT.isUserInteractionEnabled = true
            self.passwordTXT.becomeFirstResponder()
            if (self.nearbyTownTXT.text?.isEmpty)!
            {
                self.nearbyTownGetBackInPosition()
                if (self.emailTXT.text?.isEmpty)!
                {
                    self.emailGetBackInPosition()
                }
            }
            else if (self.emailTXT.text?.isEmpty)!
            {
                self.emailGetBackInPosition()
            }
            
        }
        
    }
    */
    
    // MARK:- // Register Button
    
    
    @IBOutlet weak var registerOutlet: UIButton!
    @IBAction func registerTapped(_ sender: Any) {
        
        validateWithAlerts()
        
        dispatchGroup.enter()
        
        addDataInArray()
        
        print("Sign UP Info Array : ", self.signUpInfoArray)
        
        dispatchGroup.leave()
        
        if (passwordTXT.text?.isValidPassword())!
        {
             businessRegister()
        }
        else
        {
            self.ShowAlertMessage(title: "Warning", message: "Your password must be in correct format")
        } 
        
    }
    
    
    
    // MARK:- // Cross Button
    
    
    
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    // MARK:- // Defined Functions
    
    
    
    /*
    // MARK:- // Nearby Town get back in position
    
    
    func nearbyTownGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.nearbyTownLbl.frame.origin.y = self.nearbyTownTXT.frame.origin.y
            self.nearbyTownLbl.font = UIFont(name: (self.nearbyTownLbl.font?.fontName)!,size: 16)
            self.nearbyTownLbl.font = UIFont(name: self.nearbyTownLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnNearbyTownOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Email get back in position
    
    
    func emailGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = self.emailTXT.frame.origin.y
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 16)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Password Get back in position
    
    func passwordGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.passwordLbl.frame.origin.y = self.passwordTXT.frame.origin.y
            self.passwordLbl.font = UIFont(name: (self.passwordLbl.font?.fontName)!,size: 16)
            self.passwordLbl.font = UIFont(name: self.passwordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnPasswordOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(nearbyTownTXT.text!)
        signUpInfoArray.add(emailTXT.text!)
        signUpInfoArray.add(passwordTXT.text!)
        
    }
    
    
    // MARK:- // Validate with Alerts
    
    
    func validateWithAlerts()
    {
        
        if (emailTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Email Empty", message: "Please Enter Email Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (passwordTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Password Empty", message: "Please Enter a Valid Password", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to submit SIGNUP Data
    
    func businessRegister()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_business_signup")!   //change the url
        
        var parameters : String = ""
        
        parameters = "business_name=\(signUpInfoArray[0])&bus_web_site=\(signUpInfoArray[1])&display_name=\(signUpInfoArray[2])&bus_landno=\(signUpInfoArray[3])&mobileno=\(signUpInfoArray[4])&business_address=\(signUpInfoArray[5])&mobileno=\(signUpInfoArray[6])&email=\(signUpInfoArray[7])&bus_lat=\("23.0987")&bus_long=\("23.0987")&password=\(signUpInfoArray[8])"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Business Sign Up Response: " , json)
                    
                    if "\(json["response"] as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.dispatchGroup.leave()
                            self.customAlert(title: "Alert", message: "\(json["message"] ?? "")", completion: {
                                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
                                navigate.newUserID = "\(json["new_userid"]!)"
                                self.navigationController?.pushViewController(navigate, animated: true)
                            })
                        }
                    }
                        
                    else
                        
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            let alertMsg = "\(json["message"]!)"
                            self.ShowAlertMessage(title: "Warning", message: alertMsg)
                        }
                       
                        
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        businessLbl.font = UIFont(name: businessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        nearbyTownTXT.font = UIFont(name: (nearbyTownTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        emailTXT.font = UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        passwordTXT.font = UIFont(name: (passwordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        registerButtonOutlet.titleLabel?.font = UIFont(name: (registerButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
    }
    
    
    
    
    /*
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        nearbyTownTXT.resignFirstResponder()
        emailTXT.resignFirstResponder()
        passwordTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.nearbyTownTXT.text?.isEmpty)!
        {
            self.nearbyTownGetBackInPosition()
        }
        
        if (self.emailTXT.text?.isEmpty)!
        {
            self.emailGetBackInPosition()
        }
        
        if (self.passwordTXT.text?.isEmpty)!
        {
            self.passwordGetBackInPosition()
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        else if textField == passwordTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (90/568)*self.FullHeight), animated: true)
            
        }
        
    }
    
    */
    
}

extension String {
    func isValidPassword() -> Bool {
//        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
        let regularExpression = ".{8,}"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: self)
    }
}

