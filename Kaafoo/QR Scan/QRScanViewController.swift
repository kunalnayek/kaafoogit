//
//  QRScanViewController.swift
//  Kaafoo
//
//  Created by admin on 04/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import AVFoundation

class QRScanViewController: GlobalViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var searchByQRCodeView: UIView!
    
    @IBOutlet weak var searchQRCodeTXT: UITextField!
    
    @IBOutlet weak var searchQRCodeButtonOutlet: UIButton!
    
    @IBAction func tapSearchQRCodeButton(_ sender: UIButton) {
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "qrScanInfoMainVC") as! QRScanInfoMainViewController
//
//        navigate.QRString = searchQRCodeTXT.text
//
//        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBOutlet weak var cameraView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    
    var captureSession:AVCaptureSession?
    
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    var qrCodeFrameView:UIView?
    
    let supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
    
    var resultString = ""
        
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.QRScan()
        
        self.setFont()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
    }
    
    
    
    // MARK:- // QR Scan
    
    func QRScan()
    {
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {return}
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            cameraView.layer.addSublayer(videoPreviewLayer!)
            
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                self.cameraView.addSubview(qrCodeFrameView)
                self.cameraView.bringSubviewToFront(qrCodeFrameView)
            }
            
            captureSession?.startRunning()
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            //messageLabel.text = "No QR/barcode is detected"
            print("No QR/barcode is detected")
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                
                resultString = metadataObj.stringValue! 
                
                //Print the String
                print("Qr String ---- ", resultString)
                
                var tempArray = [String]()
                
                tempArray = resultString.components(separatedBy: "-")
                
                if tempArray[0].elementsEqual("kaafoo")
                {
                    
                    captureSession?.stopRunning()
                    
                    
                    //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "qrScanInfoMainVC") as! QRScanInfoMainViewController
                    
                    navigate.QRString = tempArray[1]
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                }
                else
                {
                    print("Not a Valid QR")
                }
                
            }
        }
    }
    
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        searchQRCodeTXT.font = UIFont(name: searchQRCodeTXT.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        //ell.removeOutlet.titleLabel?.font = UIFont(name: (cell.removeOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
    }
    
}
