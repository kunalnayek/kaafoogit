//
//  CodeIDViewController.swift
//  Kaafoo
//
//  Created by admin on 07/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class CodeIDViewController: GlobalViewController {

    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userImageview: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    
    @IBOutlet weak var codeIDView: UIView!
    @IBOutlet weak var codeIDMiddleView: UIView!
    @IBOutlet weak var CodeIDLBL: UILabel!
    @IBOutlet weak var codeIDTXT: UITextField!
    
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {
    }
    
    
    var userInfoDictionary : NSDictionary!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setFont()
        
        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.userImageview.sd_setImage(with: URL(string: "\(self.userInfoDictionary["logo_image"]!)"))
        self.userName.text = "\(self.userInfoDictionary["business_name"]!)"
    }
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        userName.font = UIFont(name: userName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        CodeIDLBL.font = UIFont(name: CodeIDLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        codeIDTXT.font = UIFont(name: (codeIDTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 30)))
        
        
        submitButtonOutlet.titleLabel?.font = UIFont(name: (submitButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
    }
    

    

}
