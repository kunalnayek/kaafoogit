//
//  QRScanInfoMainViewController.swift
//  Kaafoo
//
//  Created by admin on 03/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class QRScanInfoMainViewController: GlobalViewController {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var qrBottomView: UIView!
    @IBOutlet weak var infoImageview: UIImageView!
    @IBOutlet weak var infoLBL: UILabel!
    
    var QRString : String!
    
    let dispatchGroup = DispatchGroup()
    
    var userInfoDictionary : NSDictionary!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setFont()

        self.infoView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        self.giveBonusOutlet.layer.cornerRadius = self.giveBonusOutlet.frame.size.height / 2
        self.viewProfileOutlet.layer.cornerRadius = self.viewProfileOutlet.frame.size.height / 2
        self.payWithBonusOutlet.layer.cornerRadius = self.payWithBonusOutlet.frame.size.height / 2

        self.giveBonusOutlet.layer.borderWidth = 2
        self.giveBonusOutlet.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

        self.viewProfileOutlet.layer.borderWidth = 2
        self.viewProfileOutlet.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

        self.payWithBonusOutlet.layer.borderWidth = 2
        self.payWithBonusOutlet.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        print("QR String",self.QRString!)
        
        self.getprofileDetails()
        
        
        dispatchGroup.notify(queue: .main) {
            
            if "\(self.userInfoDictionary["user_type"]!)".elementsEqual("P")
            {
                self.infoImageview.sd_setImage(with: URL(string: "\(self.userInfoDictionary["logo_image"]!)"))
                self.infoLBL.text = "\(self.userInfoDictionary["display_name"]!)"
            }
            else
            {
                self.infoImageview.sd_setImage(with: URL(string: "\(self.userInfoDictionary["logo_image"]!)"))
                self.infoLBL.text = "\(self.userInfoDictionary["business_name"]!)"
            }
            
            
            
        }
        
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Give Bonus Button
    
    @IBOutlet weak var giveBonusOutlet: UIButton!
    @IBAction func tapOnGiveBonus(_ sender: UIButton) {
        
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "giveBonusVC") as! GiveBonusViewController
        
        navigate.userInfoDictionary = self.userInfoDictionary.copy() as? NSDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // View Profile Button
    
    @IBOutlet weak var viewProfileOutlet: UIButton!
    @IBAction func tapOnViewProfile(_ sender: UIButton) {
        
        let userType = "\(self.userInfoDictionary["user_type"]!)"
        
        if (userType.elementsEqual("B"))
        {
            
            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            object.sellerID = QRString
            self.navigationController?.pushViewController(object, animated: true)
            
        }
        else
        {
            
            let object = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            object.sellerID = QRString
            self.navigationController?.pushViewController(object, animated: true)
            
        }
        
    }
    
    
    // MARK:- // Pay with Bonus Button
    
    @IBOutlet weak var payWithBonusOutlet: UIButton!
    @IBAction func tapOnPayWithBonus(_ sender: UIButton) {
        
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "payWithBonusVC") as! PayWithBonusViewController
        
        navigate.userInfoDictionary = self.userInfoDictionary.copy() as? NSDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to get Profile Details
    
    func getprofileDetails()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_edit_account_details")!   //change the url
        
        var parameters : String = ""
        
        
        parameters = "user_id=\(QRString!)"
        
        print("Get Profile Details URL is : ",url)
        print("Parameters Are : ",parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Daily Rental Listing Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        self.userInfoDictionary = json["info_array"] as? NSDictionary
                        
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        infoLBL.font = UIFont(name: infoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        giveBonusOutlet.titleLabel?.font = UIFont(name: (giveBonusOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
        viewProfileOutlet.titleLabel?.font = UIFont(name: (viewProfileOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
        payWithBonusOutlet.titleLabel?.font = UIFont(name: (payWithBonusOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
    }
   
}
