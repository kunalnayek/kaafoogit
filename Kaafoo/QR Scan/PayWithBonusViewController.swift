//
//  PayWithBonusViewController.swift
//  Kaafoo
//
//  Created by admin on 07/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class PayWithBonusViewController: GlobalViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var formView: UIView!
    
    
    @IBOutlet weak var userImageview: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    
    
    @IBOutlet weak var requestCodeIDButtonOutlet: UIButton!
    @IBAction func requestCodeID(_ sender: UIButton) {
        
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "codeIDVC") as! CodeIDViewController
        
        navigate.userInfoDictionary = self.userInfoDictionary.copy() as? NSDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    @IBOutlet weak var invoiceNoView: UIView!
    @IBOutlet weak var invoiceNoLBL: UILabel!
    @IBOutlet weak var invoiceNoTXT: UITextField!
    @IBOutlet weak var invoiceNoSeparator: UIView!
    
    
    @IBOutlet weak var invoiceAmountView: UIView!
    @IBOutlet weak var invoiceAmountLBL: UILabel!
    @IBOutlet weak var invoiceAmountTXT: UITextField!
    @IBOutlet weak var invoiceAmountSeparator: UIView!
    
    
    @IBOutlet weak var amountToPayView: UIView!
    @IBOutlet weak var amountToPayLBL: UILabel!
    @IBOutlet weak var amountToPayTXT: UITextField!
    @IBOutlet weak var amountTOPaySeparator: UIView!
    
    
    @IBOutlet weak var phoneNoView: UIView!
    @IBOutlet weak var phoneNoLBL: UILabel!
    @IBOutlet weak var phoneNoTXT: UITextField!
    @IBOutlet weak var phoneNoSeparator: UIView!
    
    
    var userInfoDictionary : NSDictionary!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setFont()
        
        self.requestCodeIDButtonOutlet.layer.cornerRadius = self.requestCodeIDButtonOutlet.frame.size.height / 2
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.userImageview.sd_setImage(with: URL(string: "\(self.userInfoDictionary["logo_image"]!)"))
        self.userName.text = "\(self.userInfoDictionary["business_name"]!)"
        
        self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.requestCodeIDButtonOutlet.frame.origin.y + self.requestCodeIDButtonOutlet.frame.size.height + (30/568)*self.FullHeight)

        
    }
    
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        userName.font = UIFont(name: userName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        invoiceNoLBL.font = UIFont(name: invoiceNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        invoiceNoTXT.font = UIFont(name: (invoiceNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        invoiceAmountLBL.font = UIFont(name: invoiceAmountLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        invoiceAmountTXT.font = UIFont(name: invoiceAmountTXT.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        amountToPayLBL.font = UIFont(name: amountToPayLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        amountToPayTXT.font = UIFont(name: (amountToPayTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        phoneNoLBL.font = UIFont(name: phoneNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        phoneNoTXT.font = UIFont(name: phoneNoTXT.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        requestCodeIDButtonOutlet.titleLabel?.font = UIFont(name: (requestCodeIDButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
    }
    

    
}
