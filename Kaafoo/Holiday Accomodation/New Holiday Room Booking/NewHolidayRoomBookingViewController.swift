//
//  NewHolidayRoomBookingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/17/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewHolidayRoomBookingViewController: GlobalViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var ShowPicker: UIView!
    @IBAction func CloseShowPickerView(_ sender: UIButton) {
        self.ShowPicker.isHidden = true
    }
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var TotalPriceArray = [TaxCalculate]()
    
    @IBOutlet weak var DataPicker: UIPickerView!

    var ShowTimePicker : Bool!
    
    var TotalDetailsRoomStruct = [TotalRoomDetails]()
    
    var IndividualArray = [DetailRoom]()
    
    var timeArray = ["12 PM","1 AM","2 AM","3 AM","4 AM","5 AM","6 AM","7 AM","8 AM","9 AM","10 AM","11 AM","12 AM","1 PM","2 PM","3 PM","4 PM","5 PM","6 PM","7 PM","8 PM","9 PM","10 PM","11 PM"]

    var CountryCodeArray = ["+964","+965","+966","+967"]
    
    var SmokingArray = [String]()
    var FloorArray = [String]()
    var RoomConditionArray = [String]()
    var GuestNumberArray = [String]()
    var FirstNameArray = [String]()
    var LastNameArray = [String]()
    var RoomEXTid = [String]()
    var RoomNumbers = [String]()
    var ProductIDs = [String]()


    @IBOutlet weak var MainScrollView: UIScrollView!
    @IBOutlet weak var ScrollContentView: UIView!


    @IBOutlet weak var estimatedTimeArrivalView: UIView!
    @IBOutlet weak var EstimatedTimeLbl: UILabel!
    @IBOutlet weak var EstimatedTime: UILabel!
    @IBOutlet weak var ShowArrivalPicker: UIButton!
    @IBAction func ShowArrivalPickerBtn(_ sender: UIButton) {
        self.ShowTimePicker = true
        self.ShowPicker.isHidden = false
        self.DataPicker.reloadAllComponents()
    }
    @IBOutlet weak var FirstSeparatorView: UIView!


    @IBOutlet weak var MessageView: UIView!
    @IBOutlet weak var MessageLbl: UILabel!
    @IBOutlet weak var MessageTextView: UITextField!
    @IBOutlet weak var SecondSeparatorView: UIView!

    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var EmailAddressLbl: UILabel!
    @IBOutlet weak var EmailAddress: UITextField!
    @IBOutlet weak var ThirdSeparatorView: UIView!


    @IBOutlet weak var MobileNumberView: UIView!
    @IBOutlet weak var MobileNumberLbl: UILabel!
    @IBOutlet weak var MobileStdCode: UILabel!
    @IBOutlet weak var ShowMObileStdPicker: UIButton!
    @IBAction func ShowMobileStdPicker(_ sender: UIButton) {
        self.ShowTimePicker = false
        self.ShowPicker.isHidden = false
        self.DataPicker.reloadAllComponents()
    }
    @IBOutlet weak var MobileNumber: UITextField!
    @IBOutlet weak var ForthSeparatorView: UIView!


    @IBOutlet weak var CheckinAndCheckoutView: UIView!
    @IBOutlet weak var CheckinAndCheckOutLbl: UILabel!
    @IBOutlet weak var totalDays: UILabel!
    @IBOutlet weak var CheckinDatetime: UILabel!
    @IBOutlet weak var CheckOutDateTime: UILabel!
    @IBOutlet weak var CheckInOutDetail: UILabel!


    @IBOutlet weak var FifthSeparatorView: UIView!


    @IBOutlet weak var PriceDetailsView: UIView!
    @IBOutlet weak var PriceLbl: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var TaxesandFeesLbl: UILabel!
    @IBOutlet weak var TotalPayLbl: UILabel!
    @IBOutlet weak var TaxesandFees: UILabel!
    @IBOutlet weak var totalPay: UILabel!


    @IBOutlet weak var SendBookingReqOutlet: UIButton!
    
    //MARK:- // Creating Individual Arrays for Sending Holiday Booking Request
    func IndividualArrayCreation()
    {
        for i in 0..<self.TotalDetailsRoomStruct.count
        {
         self.SmokingArray.append("\(self.TotalDetailsRoomStruct[i].Smoking ?? "")")
         self.FloorArray.append("\(self.TotalDetailsRoomStruct[i].Floor ?? "")")
         self.RoomConditionArray.append("\(self.TotalDetailsRoomStruct[i].AccessibleRoom ?? "")")
         self.RoomConditionArray.append("\(self.TotalDetailsRoomStruct[i].AllergyFriendlyRoom ?? "")")
         self.GuestNumberArray.append("\(self.TotalDetailsRoomStruct[i].GuestNumber ?? "")")
         self.FirstNameArray.append("\(self.TotalDetailsRoomStruct[i].FirstName ?? "")")
         self.LastNameArray.append("\(self.TotalDetailsRoomStruct[i].LastName ?? "")")
         self.RoomNumbers.append("\(self.TotalDetailsRoomStruct[i].RoomNumbers ?? "")")
         self.ProductIDs.append("\(self.TotalDetailsRoomStruct[i].RoomId ?? "")")
        }
    }
    @IBAction func SendBookingReqBtn(_ sender: UIButton) {
        if self.EstimatedTime.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Choose Estimated Arrival Time")
        }
        else if self.MessageTextView.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Your Text Message")
        }
        else if self.EmailAddress.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Your Messages")
        }
        else if self.MobileNumber.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Your Mobile Number")
        }
        else
        {
            self.SendBookingRequest()
        }
        
        
    }
    
    //MARK:- //API Fire For Holiday Booking Request
    
   func SendBookingRequest()
   {
    var parameters : String! = ""
    let langID = UserDefaults.standard.string(forKey: "langID")
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    parameters = "user_id=\(userID!)&lang_id=\(langID!)&mobileno=\(self.MobileNumber.text ?? "")&email=\(self.EmailAddress.text ?? "")&estimated_arrival_option=\(self.MessageTextView.text ?? "")&estimated_arrival_time=\(self.EstimatedTime.text ?? "")&firstname=\(self.FirstNameArray.joined(separator: ","))&lastname=\(LastNameArray.joined(separator: ","))&guest_number=\(self.GuestNumberArray.joined(separator: ","))&roomcondition=\(self.RoomConditionArray.joined(separator: ","))&floor=\(self.FloorArray.joined(separator: ","))&smoking=\(self.SmokingArray.joined(separator: ","))&checkin=\(holidaySearchParameters.shared.checkInDate!)&checkout=\(holidaySearchParameters.shared.checkOutDate!)&adults=\(holidaySearchParameters.shared.adultCount!)&children=\(holidaySearchParameters.shared.childrenCount!)&holi_product=\(holidaySearchParameters.shared.hotelPrductId!)&rooms=\(RoomNumbers.joined(separator: ","))&holi_prod_id=\(ProductIDs.joined(separator: ","))"
    
    self.CallAPI(urlString: "app_holiday_bookingdetails_request", param: parameters, completion: {
        DispatchQueue.main.async {
             SVProgressHUD.dismiss()
            self.globalDispatchgroup.leave()
            //self.ShowAlertMessage(title: "Message", message: "\(self.globalJson["message"] ?? "")")
            self.customAlert(title: "", message: "\(self.globalJson["message"] ?? "")", completion: {
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController

                self.navigationController?.pushViewController(navigate, animated: true)
            })
        }
    })
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        self.MobileNumber.keyboardType = .phonePad
        
        self.setData()
        print("TotalPrice",TotalPriceArray)
        
        self.EstimatedTime.text = "0 AM"
        self.SetLayer()
        
        print("TotalRoomDetails",TotalDetailsRoomStruct)
        self.ShowPicker.isHidden = true


        self.DataPicker.delegate = self
        self.DataPicker.dataSource = self
        self.DataPicker.reloadAllComponents()
        
        self.IndividualArrayCreation()
        
       // self.getPrice()

        // Do any additional setup after loading the view.
    }
    
    func setData()
    {
        self.CheckinDatetime.text = holidaySearchParameters.shared.checkInDate!
        self.CheckOutDateTime.text = holidaySearchParameters.shared.checkOutDate!
        self.totalDays.text = String(holidaySearchParameters.shared.DateDifference!) + " Nights"
        self.CheckInOutDetail.text = holidaySearchParameters.shared.roomCount! + " Room" + "," + holidaySearchParameters.shared.adultCount! + " Adult" + "," + holidaySearchParameters.shared.childrenCount! + " Children"
        self.Price.text = holidaySearchParameters.shared.RoomTotalPrice!
        self.totalPay.text = holidaySearchParameters.shared.RoomTotalPrice!
    }
    
    
   

    func SetLayer()
    {
        self.SendBookingReqOutlet.layer.borderWidth = 1.5
        self.SendBookingReqOutlet.layer.borderColor = UIColor(red: 255.0/255.0, green: 199.0/255.0, blue: 15.0/255.0, alpha: 1.0).cgColor
    }

    //MARK:- //Pickerview Delegate and dataSource Methods

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if ShowTimePicker == false
        {
          return self.CountryCodeArray.count
        }
        else
        {
          return self.timeArray.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if ShowTimePicker == false
        {
           return self.CountryCodeArray[row]
        }
        else
        {
           return self.timeArray[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if ShowTimePicker == false
        {
          self.MobileStdCode.text = self.CountryCodeArray[row]
           self.ShowPicker.isHidden = true
        }
        else
        {
           self.EstimatedTime.text = self.timeArray[row]
            self.ShowPicker.isHidden = true
        }
    }
    
    //MARK:- //Get Tax Price
    func getPrice()
    {
       // let parameters = "checkin=\(holidaySearchParameters.shared.APICheckINDate!)&checkout=\(holidaySearchParameters.shared.APIcheckOutDate!)&user_id=\(userID!)&product_id=\(holidaySearchParameters.shared.hotelPrductId!)&holi_product=\(ProductIDs.joined(separator: ","))&room_quantity=\(RoomNumbers.joined(separator: ","))"
        
        print("room_quantity------",RoomNumbers.joined(separator: ","))
        
        let parameters = "checkin=\(holidaySearchParameters.shared.APICheckINDate!)&checkout=\(holidaySearchParameters.shared.APIcheckOutDate!)&user_id=\(userID!)&product_id=\(holidaySearchParameters.shared.hotelPrductId!)&holi_product=\(ProductIDs.joined(separator: ","))&room_quantity=\(RoomNumbers.joined(separator: ","))"
        
        self.CallAPI(urlString: "app_holiday_price_calculate", param: parameters, completion: {
            
            if "\(self.globalJson["response"] as! Bool)".elementsEqual("true")
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.globalDispatchgroup.leave()
                    self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["error"]!)")
                }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.globalDispatchgroup.leave()
                    self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["error"]!)")
                }
                
                
                
            }
            
            
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct DetailRoom
{
    var smoking : String
    var floor : String
    var roomcondition : String
    var guestnumber : String
    var firstname : String
    var lastname : String
    var rm_ext_id : String
}





//public void settingPriceBottom(){
//    try{
//        String currencySymbol = "";
//        long totalDaysFromTv = DatesHelper.getDaysBetweenTwoDates(checkin,checkOut,1);
//        if(totalDaysFromTv == 0){
//            totalDaysFromTv = 1;
//        }
//        Integer totalDaysFromTvInt = (int)totalDaysFromTv;
//        Double totalPriceWithoutTax = 0.00, totalTax = 0.00, totalFeaturePrice = 0.00;
//        for(int i=0;i<jsonArrayFromAdapter.length();i++){
//            Double singleTotalPrice = 0.00, singleFeaturePrice = 0.00, singleTax = 0.00;
//            JSONObject dataJson = jsonArrayFromAdapter.getJSONObject(i);
//            LogHelper.logReadyMadeString("aeeee"+dataJson.toString());
//            Integer roomNo = dataJson.getInt("roomNo");
//            String roomId = dataJson.getString("room_id");
//            String smoking = dataJson.getString("smoking");
//            String floor = dataJson.getString("floor");
//            String accessibleRoom = dataJson.getString("accessibleRoom");
//            String allergyFriendlyRoom = dataJson.getString("allergyFriendlyRoom");
//            String fName = dataJson.getString("fName");
//            String lName = dataJson.getString("lName");
//            Double specialOfferPrice = dataJson.getDouble("special_offer_price");
//            Double pricePerNight = dataJson.getDouble("price_per_night");
//            singleTax = dataJson.getDouble("tax_and_fee");
//            Integer guestNo = dataJson.getInt("guestNo");
//            currencySymbol = dataJson.getString("currency_symbol");
//            JSONArray room_feature_price = dataJson.getJSONArray("room_feature_price");
//            for(int j=0;j<room_feature_price.length();j++){
//                if(room_feature_price.getJSONObject(j).getBoolean("selected")){
//                    selectedFeatureIdsArr.add(room_feature_price.getJSONObject(j).getString("id"));
//                    singleFeaturePrice+=room_feature_price.getJSONObject(j).getInt("rm_feature_price");
//                }
//            }
//            singleFeaturePrice = singleFeaturePrice*guestNo*roomNo*totalDaysFromTvInt;
//            totalFeaturePrice+=singleFeaturePrice;
//
//            if(specialOfferPrice == 0){
//                singleTotalPrice = pricePerNight;
//            }
//            else{
//                singleTotalPrice = specialOfferPrice;
//            }
//            singleTotalPrice = singleTotalPrice*roomNo*totalDaysFromTvInt;
//
//            totalPriceWithoutTax = totalPriceWithoutTax + singleTotalPrice + singleFeaturePrice
