
//
//  NewHolidayListingViewController.swift
//  Kaafoo
//  Created by IOS-1 on 15/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cosmos
import GoogleMaps
import GooglePlaces


class NewHolidayListingViewController:GlobalViewController,GMSMapViewDelegate {
    
    var isMarkerActive : Bool! = false
    var selectedMarker : GMSMarker!
    var tempdict = NSArray()
    
    @IBOutlet weak var GmapView: GMSMapView!
    @IBOutlet weak var SortView: UIView!
    @IBOutlet weak var SortPickerView: UIPickerView!
    @IBOutlet weak var LocalHeaderView: UIView!
    @IBOutlet weak var HolidayListingTableView: UITableView!
    @IBOutlet weak var HotelListingCollectionView: UICollectionView!
    @IBOutlet weak var SelectedLocationAddress: UILabel!
    @IBOutlet weak var DetailsLbl: UILabel!
    @IBOutlet weak var GProductDetailsView: UIView!
    
    
    
    @IBOutlet weak var GProductImageView: UIImageView!
    @IBOutlet weak var GProductName: UILabel!
    @IBOutlet weak var GProductDistance: UILabel!
    @IBOutlet weak var GProductDiscount: UILabel!
    @IBOutlet weak var GProductStartPrice: UILabel!
    @IBOutlet weak var GProductPerNightPrice: UILabel!
    @IBOutlet weak var GProductReviews: UILabel!
    @IBOutlet weak var GproductNumber: UILabel!
    
    @IBOutlet weak var watchlistbtn: UIButton!
    
    @IBOutlet weak var watchlistedbtn: UIButton!
    
    
    var SortSearch : String! = ""
    var Adults : String! = ""
    var Children : String! = ""
    var Rooms : String! = ""
    var Address : String! = ""
    var CheckIn : String! = ""
    var CheckOut : String! = ""
    var SearchLat : String! = ""
    var SearchLong : String! = ""
    var SearchWith : String! = ""
    var SearchOptionOne : String! = ""
    var SearchOptionTwo : String! = ""
    var UserStatusSearch : String! = ""
    var MapListStatus : String! = ""

    let dispatchGroup = DispatchGroup()
    var holidayAccomodationListingDictionary : NSDictionary!
    var startValue = 0
    var perLoad = 10
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    var nextStart : String!
    var recordsArray = NSMutableArray()
    var index : Int! = 0
    
    var markers = [GMSMarker]()

    var sortDict = [["key" : "" , "value" : "All"], ["key" : "T" , "value" : "Title"] , ["key" : "LP" , "value" : "Lowest Priced"],["key" : "HP" , "value" : "Highest Priced"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newest Listed"]]
    var sortArray = NSMutableArray()
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLayer()
        
        self.GmapView.isHidden = true
        
        self.GmapView.delegate = self
        
        print("Adults",self.Adults!)
        
        print("Children",self.Children!)
        
        print("Rooms-------",self.Rooms!)
        
        print("Checkindate",holidaySearchParameters.shared.checkInDate!)
        
        let location = holidaySearchParameters.shared.HolidaySearchAddress!
        
        self.SelectedLocationAddress.text = String(location)
        
        
        let totalString = holidaySearchParameters.shared.checkInDate! + " - " + holidaySearchParameters.shared.checkOutDate! + ", " +  holidaySearchParameters.shared.adultCount! + "Guests," + holidaySearchParameters.shared.childrenCount! + "Children," + holidaySearchParameters.shared.roomCount! + "rooms"
        
        self.DetailsLbl.text = String(totalString)
        
        self.AddGesture()
        
        self.CreateFavouriteView(inview: self.view)
        
        self.getHolidayAccomodationListingDetails()
        
        self.HolidayListingTableView.separatorStyle = .none
        
        self.HolidayListingTableView.isHidden = false
        
        self.HotelListingCollectionView.isHidden = true
        
        self.defineSortButtonActions()
    }
    
    @IBAction func watchlistbtnclicked(_ sender: Any)
    {
        
     //   watchlistbtn.isHidden = true
     //   watchlistedbtn.isHidden = false
        
//        let userID = UserDefaults.standard.string(forKey: "userID")
//
//        if (userID?.isEmpty)!
//        {
//            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
//        }
//        else
//        {
//            self.GlobalAddtoWatchlist(ProductID:  "\(tempdict.WatchlistStatus ?? "hotel_id")", completion:
//
//                {
//                    self.startValue = 0
//                    self.recordsArray.removeAllObjects()
//                    self.getHolidayAccomodationListingDetails()
//            })
//        }
    }
    
    @IBAction func watchlistedbtnclicked(_ sender: Any)
    {
        
    //    watchlistbtn.isHidden = false
     //   watchlistedbtn.isHidden = true
        
//        let userID = UserDefaults.standard.string(forKey: "userID")
//
//        if (userID?.isEmpty)!
//        {
//            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
//        }
//        else
//        {
//            self.GlobalRemoveFromWatchlist(ProductID:  "\(tempdict.WatchlistStatus ?? "hotel_id")", completion:
//                {
//                    self.startValue = 0
//                    self.recordsArray.removeAllObjects()
//                    self.getHolidayAccomodationListingDetails()
//            })
//        }
    }
    
    
    func setLayer()
    {
        self.GProductDetailsView.layer.borderColor = UIColor.black.cgColor
        
        self.GProductDetailsView.layer.borderWidth = 1.0
    }
    
    func AppendMapData()
    {
        self.dispatchGroup.enter()
        
        for i in 0..<self.recordsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((recordsArray[i] as! NSDictionary)["hotel_name"] ?? "")", ProductLatitude: Double(("\((recordsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((recordsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((recordsArray[i] as! NSDictionary)["image"] ?? "")", CurrencySymbol:  "\((recordsArray[i] as! NSDictionary)["currency_symbol"] ?? "")", StartPrice:  "\((recordsArray[i] as! NSDictionary)["price_per_night"] ?? "")", ReservePrice:  "\((recordsArray[i] as! NSDictionary)["special_offer_price"] ?? "")", ProductID:  "\((recordsArray[i] as! NSDictionary)["hotel_id"] ?? "")", SellerImage: "\((recordsArray[i] as! NSDictionary)["total_review"] ?? "")", WatchlistStatus: "\((recordsArray[i] as! NSDictionary)["avg_rating"] ?? "")"))
        }
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.5)
            //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.GmapView.animate(to: cameraPosition)
            
            self.GmapView.isMyLocationEnabled = true
            
            self.GmapView.settings.myLocationButton = true
            
            
            for state in self.ProductOnMapArray {
                
                let state_marker = GMSMarker()
                
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                
                state_marker.title = state.ProductName
                
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                
                state_marker.map = self.GmapView
                
                state_marker.userData = state
                
                self.markers.append(state_marker)
                
            }
        })
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //        customInfoWindow?.removeFromSuperview()
        
        self.index = 0
        self.GProductDetailsView.isHidden = true
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
        //        let update = GMSCameraUpdate.zoom(by: 10)
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
        //        GmapView.moveCamera(update)
        GmapView.animate(to: cameraPosition)
    }
    
    
    func centerInMarker(marker: GMSMarker) {
        
        var bounds = GMSCoordinateBounds()
        
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        
        GmapView?.moveCamera(updateOne)
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = GmapView.selectedMarker {
            
            selectedMarker.icon = GMSMarker.markerImage(with: nil)
        }
        
        GmapView.selectedMarker = marker
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        
        self.GProductDetailsView.isHidden = false
        
        let tempdict = marker.userData as! MapListing
        
      
        self.dispatchGroup.enter()
        
        self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
        self.GProductName.text = "\(tempdict.ProductName ?? "")"
        self.GProductStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
        self.GProductPerNightPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
        self.GProductReviews.text = "\(tempdict.SellerImage ?? "")" + "Reviews"
        self.GproductNumber.text = "\(tempdict.WatchlistStatus ?? "")"
        
        if "\(tempdict.WatchlistStatus ?? "")".elementsEqual("1")
        {
            watchlistbtn.isHidden = false
            watchlistedbtn.isHidden = true
            
        }
        else if "\(tempdict.WatchlistStatus ?? "")".elementsEqual("0")
        {
            watchlistbtn.isHidden = true
            watchlistedbtn.isHidden = false
            
        }
        else
        {
//            watchlistbtn.isHidden = true
//            watchlistedbtn.isHidden = false
        }
        
        watchlistbtn.addTarget(self, action: #selector(Watchlistbtnclick(sender:)), for: .touchUpInside)
        
        watchlistedbtn.addTarget(self, action: #selector(Watchlistedbtnclick(sender:)), for: .touchUpInside)
        
        self.dispatchGroup.leave()
        
        self.GProductDetailsView.isHidden = false
        self.view.bringSubviewToFront(self.GProductDetailsView)
        
        return true
    }
    
    @objc func Watchlistbtnclick(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["hotel_id"]!)", completion:
                {
                    self.startValue = 0
                    self.recordsArray.removeAllObjects()
                    self.getHolidayAccomodationListingDetails()
            })
        }
    }
    
    
    @objc func Watchlistedbtnclick(sender: UIButton)
       {
           let userID = UserDefaults.standard.string(forKey: "userID")

           if (userID?.isEmpty)!
           {
               self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
           }
           else
           {
               self.GlobalRemoveFromWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["hotel_id"]!)", completion:
                   {
                       self.startValue = 0
                       self.recordsArray.removeAllObjects()
                       self.getHolidayAccomodationListingDetails()
               })
           }

       }

    
    
    @IBOutlet weak var GPreviousBtnOutlet: UIButton!
    
    @IBOutlet weak var GNextBtnOutlet: UIButton!
    
    @IBAction func GPreviousBtn(_ sender: UIButton) {
                index = index - 1
        
                if self.index == 0
                {
                    self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
                }
        
                else if self.index > self.markers.count
                {
                    self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
                }
                else if self.index < 0
                {
                    self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
                }
                else
                {
                    self.GProductDetailsView.isHidden = false
        
                    let tempdict = markers[index].userData as! MapListing
        
                    self.dispatchGroup.enter()
        
                    self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
                    self.GProductName.text = "\(tempdict.ProductName ?? "")"
                    self.GProductStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
                    self.GProductPerNightPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
                    self.GProductReviews.text = "\(tempdict.SellerImage ?? "")" + "Reviews"
                    self.GproductNumber.text = "\(tempdict.WatchlistStatus ?? "")"
        
                    self.dispatchGroup.leave()
        
                    self.GProductDetailsView.isHidden = false
                    self.view.bringSubviewToFront(self.GProductDetailsView)
        
                    self.dispatchGroup.notify(queue: .main, execute: {
        
                        let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                        self.GmapView?.moveCamera(updateOne)
                    })
                }
    }
    @IBAction func GNextBtn(_ sender: UIButton) {
        index = index + 1
        
                if index == self.markers.count
                {
                    self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
                }
                else if index > self.markers.count
                {
                    self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
                }
        
                else
                {
                    self.GProductDetailsView.isHidden = false
        
                    let tempdict = markers[index].userData as! MapListing
        
                    self.dispatchGroup.enter()
        
                    self.GProductImageView.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
                    self.GProductName.text = "\(tempdict.ProductName ?? "")"
                    self.GProductStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
                    self.GProductPerNightPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
                    self.GProductReviews.text = "\(tempdict.SellerImage ?? "")" + "Reviews"
                    self.GproductNumber.text = "\(tempdict.WatchlistStatus ?? "")"
        
                    self.dispatchGroup.leave()
        
                    self.GProductDetailsView.isHidden = false
                    self.view.bringSubviewToFront(self.GProductDetailsView)
        
                    self.dispatchGroup.notify(queue: .main, execute: {
        
                        let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                        self.GmapView?.moveCamera(updateOne)
                    })
        
                }
        
    }
    
    
//    @IBAction func GPreviousBtn(_ sender: UIButton) {
//
//        index = index - 1
//
//        if self.index == 0
//        {
//            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
//        }
//
//        else if self.index > self.markers.count
//        {
//            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
//        }
//        else if self.index < 0
//        {
//            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
//        }
//        else
//        {
//            self.GProductDetailsView.isHidden = false
//
//            let tempdict = markers[index].userData as! MapListing
//
//            self.dispatchGroup.enter()
//
//            if (tempdict.WatchlistStatus?.elementsEqual("0"))!
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = false
//                self.GProductDetailsWatchlistedOutlet.isHidden = true
//            }
//            else
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = true
//                self.GProductDetailsWatchlistedOutlet.isHidden = false
//            }
//
//            self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
//
//            self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
//            self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
//            self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
//            self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
//            self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
//
//            self.dispatchGroup.leave()
//
//            self.GProductDetailsView.isHidden = false
//            self.view.bringSubviewToFront(self.GProductDetailsView)
//
//            self.dispatchGroup.notify(queue: .main, execute: {
//
//                let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
//                self.GmapView?.moveCamera(updateOne)
//            })
//        }
//    }
//
  
    
//    @IBAction func GNextBtn(_ sender: UIButton) {
//
//        index = index + 1
//
//        if index == self.markers.count
//        {
//            self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
//        }
//        else if index > self.markers.count
//        {
//            self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
//        }
//
//        else
//        {
//            self.GProductDetailsView.isHidden = false
//
//            let tempdict = markers[index].userData as! MapListing
//
//            self.dispatchGroup.enter()
//
//            if (tempdict.WatchlistStatus?.elementsEqual("0"))!
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = false
//                self.GProductDetailsWatchlistedOutlet.isHidden = true
//            }
//            else
//            {
//                self.GProductDetailsWatchlistOutlet.isHidden = true
//                self.GProductDetailsWatchlistedOutlet.isHidden = false
//            }
//
//            self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
//
//            self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
//            self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
//            self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
//            self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
//            self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
//
//            self.dispatchGroup.leave()
//
//            self.GProductDetailsView.isHidden = false
//            self.view.bringSubviewToFront(self.GProductDetailsView)
//
//            self.dispatchGroup.notify(queue: .main, execute: {
//
//                let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
//                self.GmapView?.moveCamera(updateOne)
//            })
//
//        }
//
//
//
//
//    }


    // MARK:- // Buttons

    // MARK:- // List Grid Button

    @IBOutlet weak var ListGridBtnOutlet: UIButton!
    @IBAction func ListGridBtn(_ sender: UIButton) {
        if HolidayListingTableView.isHidden == false
        {
            self.HolidayListingTableView.isHidden = true
            self.HotelListingCollectionView.isHidden = false
            self.ListGridBtnOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        }
        else
        {
            self.HolidayListingTableView.isHidden = false
            self.HotelListingCollectionView.isHidden = true
            self.ListGridBtnOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        }
    }


    // MARK:- // Map Button

    @IBOutlet weak var MapBtnOutlet: UIButton!
    @IBAction func MapViewBtn(_ sender: UIButton) {
        if self.GmapView.isHidden == true
        {
            self.GmapView.isHidden = false
        }
        else
        {
            self.GmapView.isHidden = true
        }
    }


    // MARK:- // Sort Button

    @IBOutlet weak var SortBtnOutlet: UIButton!
    @IBAction func SortViewBtn(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

    }

    // MARK:- // Selection Criteria Button Action

    @IBAction func SelectionCriteriaBtn(_ sender: UIButton) {
        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "holidayAccomodationSearchVC") as! HolidayAccomodationSearchViewController
        nav.ExampleSearchListing = self
        self.navigationController?.pushViewController(nav, animated: true)

    }




    //MARK:- // Get Products Listing


    func getHolidayAccomodationListingDetails()
    {
        let myCountry = UserDefaults.standard.string(forKey: "myCountryName")
        let UserID = UserDefaults.standard.string(forKey: "userID")

        let parameters = "user_id=\(UserID!)&mycountry_name=\(myCountry!)&sort_search=\(SortSearch!)&adults=\(Adults!)&children=\(Children!)&rooms=\(Rooms!)&address=\(Address!)&checkin=\(CheckIn!)&checkout=\(CheckOut!)&search_lat=\(holidaySearchParameters.shared.SearchLatitude!)&search_long=\(holidaySearchParameters.shared.SearchLongitude!)&search_with=\(SearchWith!)&search_option2=\(SearchOptionTwo!)&search_option1=\(SearchOptionOne!)&user_status_search=\(UserStatusSearch!)&Map_list_status=\(MapListStatus!)&search_option1=\(self.searchOptionTwoCommaSeparatedString!)&search_option2=\(self.searchOptionOneCommaSeparatedString!)"

        self.CallAPI(urlString: "app_holiday_search_and_listing", param: parameters, completion:
            {
                self.holidayAccomodationListingDictionary = self.globalJson["info_array"] as? NSDictionary

                for i in 0..<(self.holidayAccomodationListingDictionary["holiday_list"] as! NSArray).count

                {

                    let tempDict = (self.holidayAccomodationListingDictionary["holiday_list"] as! NSArray)[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                    
                    print("recordsArray--------",self.recordsArray)
                    

                }

                self.sortArray.removeAllObjects()

                self.makeSortArray(fromArray: self.holidayAccomodationListingDictionary["option_section"] as! NSArray, toArray: self.sortArray)

                self.nextStart = "\(self.globalJson["next_start"]!)"

                self.startValue = self.startValue + (self.holidayAccomodationListingDictionary["holiday_list"] as! NSArray).count

                print("Next Start Value : " , self.startValue)

                DispatchQueue.main.async {

                    self.globalDispatchgroup.leave()

                    self.HolidayListingTableView.delegate = self
                    self.HolidayListingTableView.dataSource = self
                    self.HolidayListingTableView.reloadData()
                    self.HolidayListingTableView.rowHeight = UITableView.automaticDimension
                    self.HolidayListingTableView.estimatedRowHeight = UITableView.automaticDimension

                    self.HotelListingCollectionView.delegate = self
                    self.HotelListingCollectionView.dataSource = self
                    self.HotelListingCollectionView.reloadData()

                    self.SortPickerView.delegate = self
                    self.SortPickerView.dataSource = self
                    self.SortPickerView.reloadAllComponents()
                    
                    self.AppendMapData()

                    SVProgressHUD.dismiss()
                }
        })
    }



    // MARK:- // Add to Favourites Selector Method

    @objc func Watchlist(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["hotel_id"]!)", completion:
                {
                    self.startValue = 0
                    self.recordsArray.removeAllObjects()
                    self.getHolidayAccomodationListingDetails()
            })
        }
    }



    // MARK:- // Remove From Favourites Selector MEthod

    @objc func Watchlisted(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID: "\((recordsArray[sender.tag] as! NSDictionary)["hotel_id"]!)", completion:
                {
                    self.startValue = 0
                    self.recordsArray.removeAllObjects()
                    self.getHolidayAccomodationListingDetails()
            })
        }

    }


    // MARK:- // Define Filter Button Actions

    func defineSortButtonActions() {

        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)
        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

    }


    // MARK;- // Sort Action

    @objc func sortAction(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

        self.SortView.isHidden = false
    }

    // MARK;- // Filter Action

    @objc func filterAction(sender: UIButton) {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

        navigate.sortArray = self.sortArray.mutableCopy() as? NSMutableArray

        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newholidaylisting") as! NewHolidayListingViewController

        navigate.showDynamicType = true
        navigate.showLocation = false
        navigate.showCondition = false
        navigate.showType = false
        navigate.showFilter = false
        navigate.showDistance = true

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK;- // Tap to close Filterview Selector Method

    @objc func tapToClose(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

    }


    // MARK:- // MAke Sorting Array

    func makeSortArray(fromArray: NSArray , toArray: NSMutableArray)
    {

        for i in 0..<fromArray.count
        {

            var tempArray = [sortClass]()

            for j in 0..<((fromArray[i] as! NSDictionary)["opt_value"] as! NSArray).count
            {

                tempArray.append(sortClass(key: "\((((fromArray[i] as! NSDictionary)["opt_value"] as! NSArray)[j] as! NSDictionary)["id"] ?? "")", value: "\((((fromArray[i] as! NSDictionary)["opt_value"] as! NSArray)[j] as! NSDictionary)["opt_value_name"] ?? "")", isClicked: false , totalNo: "\((((fromArray[i] as! NSDictionary)["opt_value"] as! NSArray)[j] as! NSDictionary)["count"] ?? "")"))

            }

            //            let tempArrayData = NSKeyedArchiver.archivedData(withRootObject: tempArray)

            let tempDict = ["id" : "\((fromArray[i] as! NSDictionary)["id"] ?? "")" , "key" : "\((fromArray[i] as! NSDictionary)["opt_name"] ?? "")" , "value" : tempArray] as [String : Any]

            toArray.add(tempDict)

        }

    }







    //Custom Alert Function
    func showAlert(title : String!,message : String!)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }


    //MARK:- //AddGesture to Uiview

    func AddGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(HandleTap(sender:)))
        SortView.isUserInteractionEnabled = true
        SortView.addGestureRecognizer(tap)
    }

    @objc func HandleTap(sender : UITapGestureRecognizer)
    {
        self.SortView.isHidden = true
    }


}




// MARK:- // Tableview Delegate MEthods

extension NewHolidayListingViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "holidaylisting") as! NewHolidayListingTVC
        
        cell.CosmosRatingView.rating = Double("\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"] ?? "")")!
        
        cell.CosmosRatingView.settings.updateOnTouch = false

        cell.HotelImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"))
        cell.HotelName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_name"]!)"

        cell.HotelAddress.text = "\((recordsArray[indexPath.row] as! NSDictionary)["dstnc_km"]!)" + " "
            + "from City Center"
        
       
        var titlearr = NSMutableArray()
        
        titlearr = (recordsArray[indexPath.row] as! NSDictionary)["title"] as! NSMutableArray
        print("titlearr---",titlearr)
        
        for i in 0..<(titlearr.count)
        {
       
            let tempDict = (titlearr[i] as! NSDictionary)
            print("tempDict----",tempDict)
            
            let arr = NSMutableArray()
            
            arr.add(tempDict)
            
            cell.HotelTitle.text = ("\((arr[i] as! NSDictionary)["title"]!)")


        }
        



       

        cell.HotelWatchlistBtn.tag = indexPath.row
        cell.HotelWatchlistedBtn.tag = indexPath.row

//        cell.HotelWatchlistBtn.bringSubviewToFront(cell.contentView)
//        cell.HotelWatchlistedBtn.bringSubviewToFront(cell.contentView)
        cell.HotelWatchlistBtn.addTarget(self, action: #selector(Watchlist(sender:)), for: .touchUpInside)
        cell.HotelWatchlistedBtn.addTarget(self, action: #selector(Watchlisted(sender:)), for: .touchUpInside)

        if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("1")
        {
            cell.HotelWatchlistedBtn.isHidden = false
            cell.HotelWatchlistBtn.isHidden = true
            
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("0")
        {
            cell.HotelWatchlistedBtn.isHidden = true
            cell.HotelWatchlistBtn.isHidden = false
            
        }
        else
        {
            cell.HotelWatchlistedBtn.isHidden = true
            cell.HotelWatchlistBtn.isHidden = false
        }
        
         if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)".elementsEqual("0.00")
         {
        
        cell.ReservePrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)"
            
        cell.StartPrice.isHidden = true
        }
        else
         {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")

             attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

             cell.StartPrice.attributedText = attributeString
            
             // attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            
            cell.StartPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)"
            
            cell.ReservePrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)"
        }

       // cell.ReservePrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)"
        
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)".elementsEqual("0.00")
        {
            cell.DiscountedPercentage.isHidden = true
            
           

                   
            
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)".elementsEqual("0")
        {
            cell.DiscountedPercentage.isHidden = true
        }
        else
        {
            cell.DiscountedPercentage.isHidden = false
            let SpecialOffer = Double("\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")! - Double("\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)")!

            let PercentageOffer = Double(Double(SpecialOffer) / Double("\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")!)

            cell.DiscountedPercentage.text = "-" + "\(Int(PercentageOffer * 100))" + "%"
        }
        
        
        cell.HotelAvgRating.text = "\((recordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)"
        cell.HotelReview.text = "\((recordsArray[indexPath.row] as! NSDictionary)["rating_text"]!)"
        cell.HotelReview.textColor = UIColor.yellow2()
        cell.HotelReviewsNo.text = "\((recordsArray[indexPath.row] as! NSDictionary)["total_review"]!)" + " " + "Reviews"
        cell.HotelReviewsNo.textColor = UIColor.darkGray

      

//        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
        navigate.ProductID = "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_id"]!)"
        navigate.RoomsNo = self.Rooms
        navigate.pernightprice = "\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)"
        
        holidaySearchParameters.shared.setHotelProductID(hotelID: "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_id"]!)")
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }

}



// MARK:- // Collectionview Delegate Methods

extension NewHolidayListingViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recordsArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "holidaylisting", for: indexPath) as! NewHolidayListingCollectionViewCell
        
        var titlearr = NSMutableArray()
         
         titlearr = (recordsArray[indexPath.row] as! NSDictionary)["title"] as! NSMutableArray
         print("titlearr---",titlearr)
         
         for i in 0..<(titlearr.count)
         {
        
             let tempDict = (titlearr[i] as! NSDictionary)
             print("tempDict----",tempDict)
             
            let arr = NSMutableArray()
             
             arr.add(tempDict)
             
             obj.HotelTitle.text = ("\((arr[i] as! NSDictionary)["title"]!)")


         }
         
        
        obj.CosmosRatingView.settings.updateOnTouch = false
        
        obj.CosmosRatingView.rating = Double("\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"] ?? "")")!

        obj.HotelImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"))
        obj.HotelName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_name"]!)"

        obj.HotelAddress.text = "\((recordsArray[indexPath.row] as! NSDictionary)["dstnc_km"]!)" + " "
            + "from City Center"

        obj.DiscountedPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)"

        obj.HotelReviewsNo.text = "\((recordsArray[indexPath.row] as! NSDictionary)["total_review"]!)" + " " + "Reviews"

        obj.HotelWatchlistBtn.tag = indexPath.row
        obj.HotelWatchlistedBtn.tag = indexPath.row

        if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("1")
        {
            obj.HotelWatchlistedBtn.isHidden = false
            obj.HotelWatchlistBtn.isHidden = true
            obj.HotelWatchlistedBtn.addTarget(self, action: #selector(Watchlisted(sender:)), for: .touchUpInside)
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("0")
        {
            obj.HotelWatchlistedBtn.isHidden = true
            obj.HotelWatchlistBtn.isHidden = false
            obj.HotelWatchlistBtn.addTarget(self, action: #selector(Watchlist(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.HotelWatchlistBtn.isHidden = false
            obj.HotelWatchlistedBtn.isHidden = true
        }
        obj.HotelAvgrating.text = "\((recordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)"
        obj.HotelReview.textColor = UIColor.yellow2()
        obj.HotelReview.text = "\((recordsArray[indexPath.row] as! NSDictionary)["rating_text"]!)"

        if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)".elementsEqual("")
        {
            obj.DiscountedPercentage.isHidden = true
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)".elementsEqual("0")
        {
            obj.DiscountedPercentage.isHidden = true
        }
        else
        {
            obj.DiscountedPercentage.isHidden = false
            let SpecialOffer = Double("\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")! - Double("\((recordsArray[indexPath.row] as! NSDictionary)["special_offer_price"]!)")!

            let PercentageOffer = Double(Double(SpecialOffer) / Double("\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")!)

            obj.DiscountedPercentage.text = "-" + "\(Int(PercentageOffer * 100))" + "%"
        }

        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["price_per_night"]!)")

        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

        obj.StartPrice.attributedText = attributeString
        print("Height",obj.HotelImage.frame.size.height)
        print("CellHeight",obj.frame.size.height)
        return obj

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding : CGFloat! = 4

        let collectionViewSize = HotelListingCollectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: 299)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
        
        navigate.ProductID = "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_id"]!)"
        
        holidaySearchParameters.shared.setHotelProductID(hotelID: "\((recordsArray[indexPath.row] as! NSDictionary)["hotel_id"]!)")
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }

}



// MARK:- // Pickerview Delegate Methods

extension NewHolidayListingViewController: UIPickerViewDataSource,UIPickerViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sortDict.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((self.sortDict[row] as NSDictionary)["value"]!)"
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.SortSearch = "\((self.sortDict[row] as NSDictionary)["key"]!)"
        self.SortView.isHidden = true
//        print("SortingType==",self.SortSearch)
        self.recordsArray.removeAllObjects()
        self.startValue = 0
        self.getHolidayAccomodationListingDetails()
    }

}



// AMRK:- // Scrollview Delegates

extension NewHolidayListingViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getHolidayAccomodationListingDetails()
            }
        }
    }


}
// MARK:- // Search Holiday Listing Delegate Method
extension NewHolidayListingViewController : SearchHolidayListing
{
    func ClickSearch(locationName: String!, CheckinDate: String!, CheckoutDate: String!, Adults: String!, Children: String!, Rooms: String) {
        self.SelectedLocationAddress.text = locationName
        self.DetailsLbl.text = CheckinDate + CheckoutDate
    }
}
//MARK:- //Holiday Listing TableviewCEll
class NewHolidayListingTVC: UITableViewCell {
    
    @IBOutlet weak var HotelTitle: UILabel!
    
    @IBOutlet weak var HotelImage: UIImageView!
    @IBOutlet weak var HotelWatchlistBtn: UIButton!
    @IBOutlet weak var HotelName: UILabel!
    @IBOutlet weak var HotelAddress: UILabel!
    @IBOutlet weak var DiscountedPercentage: UILabel!
    @IBOutlet weak var StartPrice: UILabel!
    @IBOutlet weak var ReservePrice: UILabel!
    @IBOutlet weak var HotelReviewsNo: UILabel!
    @IBOutlet weak var HotelReview: UILabel!
    @IBOutlet weak var HotelAvgRating: UILabel!
    @IBOutlet weak var HotelWatchlistedBtn: UIButton!
    @IBOutlet weak var CosmosRatingView: CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//MARK:- //Holiday Listing CollectionViewCell
class NewHolidayListingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var HotelImage: UIImageView!
    @IBOutlet weak var HotelTitle: UILabel!
    @IBOutlet weak var HotelName: UILabel!
    @IBOutlet weak var HotelAddress: UILabel!
    @IBOutlet weak var DiscountedPrice: UILabel!
    @IBOutlet weak var DiscountedPercentage: UILabel!
    @IBOutlet weak var StartPrice: UILabel!
    @IBOutlet weak var HotelReview: UILabel!
    @IBOutlet weak var HotelReviewsNo: UILabel!
    @IBOutlet weak var HotelWatchlistBtn: UIButton!
    @IBOutlet weak var HotelAvgrating: UILabel!
    @IBOutlet weak var HotelWatchlistedBtn: UIButton!
    @IBOutlet weak var CosmosRatingView: CosmosView!
}



