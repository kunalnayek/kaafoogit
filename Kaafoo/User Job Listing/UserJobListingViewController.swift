//

//  UserJobListingViewController.swift

//  Kaafoo

//

//  Created by KAUSTABH K B on 06/02/19.

//  Copyright © 2019 ESOLZ. All rights reserved.

//



import UIKit
import  SVProgressHUD
import SDWebImage



class UserJobListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var JobListing_TableView: UITableView!

    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var info_array:NSMutableArray! = NSMutableArray()
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var start_value = 0
    
    var per_value = 10
    
    var RemoveID : String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.LoadData()
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.JobListing_TableView.rowHeight = UITableView.automaticDimension

        self.JobListing_TableView.estimatedRowHeight =  (210/568)*self.FullHeight

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.info_array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = tableView.dequeueReusableCell(withIdentifier: "joblist") as! JobListingTableViewCell
        
        obj.Job_name.text = "\((self.info_array[indexPath.row] as! NSDictionary)["name"]!)"
        
        let path = "\((self.info_array[indexPath.row] as! NSDictionary)["image"]!)"
        
        obj.Pro_Image.sd_setImage(with: URL(string: path))
        
        obj.closing_time.text = "\((self.info_array[indexPath.row] as! NSDictionary)["expire_time"]!)"
        
        obj.location_lbl.text = "\((self.info_array[indexPath.row] as! NSDictionary)["job_location"]!)"
        
        obj.category_lbl.text = "Top category:" + " " + "\((self.info_array[indexPath.row] as! NSDictionary)["top_categorry"]!)"
        
        obj.delete_btn.addTarget(self, action: #selector(UserJobListingViewController.Delete(sender:)), for: .touchUpInside)
        
        obj.apply_btn.layer.cornerRadius = 10.0
        
        obj.apply_btn.clipsToBounds = true
        
        obj.selectionStyle = .none
        
        obj.Job_name.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.closing_time.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        obj.category_lbl.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
        
        obj.location_lbl.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
        
        obj.apply_btn.titleLabel?.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.delete_btn.titleLabel?.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.delete_btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "delete", comment: ""), for: .normal)
        obj.edit_btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit", comment: ""), for: .normal)
        
        obj.edit_btn.titleLabel?.font = UIFont(name: obj.Job_name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    //MARK:- //Get Listing using Global API
    func LoadData()
    {
        self.start_value = 0
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&start_value=\(start_value)&per_value=\(per_value)"
        self.CallAPI(urlString: "app_user_job_list", param: parameters, completion: {
            self.info_array = self.globalJson["info_array"] as? NSMutableArray
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.start_value = self.start_value + self.info_array.count
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.JobListing_TableView.reloadData()
                self.JobListing_TableView.delegate = self
                self.JobListing_TableView.dataSource = self
                SVProgressHUD.dismiss()
            }
        })
    }
    
    @objc func Data()
        
    {
        
        print("Nothing")
        
    }
    
    // MARK: - // Scrollview Delegate Methods
    
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
            
        {
            
            
            
        }
            
        else
            
        {
            
            
            
            print("next start : ",nextStart )
            
            
            
            if (nextStart).isEmpty
                
            {
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                }
                
                
                
                
                
            }
                
            else
                
            {
                
                LoadData()
                
            }
            
            
            
            
            
            
            
        }
        
    }
    
    //MARK:- Selector MEthod For DELETE Button API
    
    @objc func Delete(sender: UIButton)
        
    {
        
        
        
        
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            
            UIAlertAction in
            
            
            
            // let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            let tempDictionary = self.info_array[sender.tag] as! NSDictionary
            
            self.RemoveID = "\(tempDictionary["job_id"]!)"
            
            
            
            self.SingleCartRemove()
            
            
            
            
            
            
            
        }
        
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        alert.addAction(ok)
        
        alert.addAction(cancel)
        
        
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    //MARK:- Remove Specific Job From User Job Listing

    func SingleCartRemove()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters = "user_id=\(userID!)&remove_id=\(RemoveID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_user_joblist_delete", param: parameters, completion: {
            DispatchQueue.main.async
                {
                    self.globalDispatchgroup.leave()
                    self.globalDispatchgroup.notify(queue: .main, execute: {
                        self.info_array.removeAllObjects()
                        self.LoadData()
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    })
            }
        })
    }
    
    
    
    /*
     
     // MARK: - Navigation
     
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     // Get the new view controller using segue.destination.
     
     // Pass the selected object to the new view controller.
     
     }
     
     */
    
    
    
}
//MARK:- Job Listing TableViewCell
class JobListingTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var Pro_Image: UIImageView!
    @IBOutlet weak var Job_name: UILabel!
    @IBOutlet weak var closing_time: UILabel!
    @IBOutlet weak var category_lbl: UILabel!
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var delete_btn: UIButton!

    @IBOutlet weak var edit_btn: UIButton!
    @IBOutlet weak var apply_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

