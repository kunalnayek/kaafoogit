//
//  NewTextAdvertisementViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/17/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewTextAdvertisementViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    var StartFrom : Int! = 0
     var PerPage : String! = "100"
    @IBOutlet weak var classifiedLBL: UILabel!
    @IBOutlet weak var ShowReportView: UIView!
    @IBOutlet weak var HideReportViewButton: UIButton!
    @IBAction func HideReportBtnAction(_ sender: UIButton) {
        self.ShowReportView.isHidden = true
    }
    @IBOutlet weak var ReportTextView: UITextView!
    @IBAction func ReportSendBtn(_ sender: UIButton) {

        let CountryId = UserDefaults.standard.string(forKey: "countryID")

        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")

        let parameters = "lang_id=\(langID!)&userid=\(userID!)&country_id=\(CountryId!)&text_no=\((self.recordsArray[sender.tag] as! NSDictionary)["unique_no"]!)&category_id=1122&comments=\(ReportTextView.text!)"

        self.CallAPI(urlString: "classified_report", param: parameters, completion:
            {
                self.globalDispatchgroup.leave()
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    let responseMessage = "\(self.globalJson["message"]!)"
                    self.ShowAlertMessage(title: "warning", message: responseMessage)
                }
        })
    }

    func HideReport()
    {
        self.ShowReportView.isHidden = true
    }
    @IBAction func ReportCancelBtn(_ sender: UIButton) {
        self.ShowReportView.isHidden = true
    }

    var index : Int!


    @IBOutlet weak var LocalHeaderView: UIView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var TextAdCollectionView: UICollectionView!
    var StartValue : Int! = 0
    var PerLoad : String! = "10"
    let userID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
  
    var infoArray : NSArray!
    var recordsArray : NSMutableArray! = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        classifiedLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Classified", comment: "")
        self.SetLayer()
        self.GetDetails()
        self.ShowReportView.isHidden = true
        // Do any additional setup after loading the view. 
    }

    func SetLayer()
    {
        self.ReportTextView.layer.borderWidth = 1.0
        self.ReportTextView.layer.borderColor = UIColor.black.cgColor
    }

    func GetDetails()
    {
        
          let CountryId = UserDefaults.standard.string(forKey: "countryID")
     
          print("userid-----",userID)
          print("LangID-----",LangID)
          print("CountryId-----",CountryId)
        
       // let parameters = "lang_id=\(LangID!)&user_id=\(userID!)&mycountry_id=\(CountryId)"
         let parameters = "user_id=\(userID!)&lang_id=\(LangID!)&start_from=\(StartFrom!)&per_page=\(PerPage!)&mycountry_id=\(CountryId!)"
        
        //self.CallAPI(urlString: "app_home_textadv_details", param: parameters, completion: {
      //  self.CallAPI(urlString: "app_text_advertisement_list", param: parameters, completion: {
         self.CallAPI(urlString: "app_classified_list_info", param: parameters, completion: {
        self.infoArray = self.globalJson["info_array"] as? NSArray
        for i in 0..<self.infoArray.count
        {
            let tempdict = self.infoArray[i] as! NSDictionary
            self.recordsArray.add(tempdict )
            print("RecordsArray",self.recordsArray!)
            }
            DispatchQueue.main.async {
                self.TextAdCollectionView.delegate = self
                self.TextAdCollectionView.dataSource = self
                self.TextAdCollectionView.reloadData()
                SVProgressHUD.dismiss()
            }

        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- //CollectionView delegate and DataSource methods

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recordsArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textad", for: indexPath) as! NewTextAdCVC
        cell.ContentView.layer.borderWidth = 1.0
        cell.ContentView.layer.borderColor = UIColor.s_green().cgColor
        
        cell.textAdid.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["unique_no"]!)"
        cell.textAdid.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
       //  cell.textAdid.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["id"]!)"
        cell.textAdname.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["title"]!)"
          cell.textAdname.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        cell.textAdDescription.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["description"]!)"
         cell.textAdDescription.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        cell.textAdPhone.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["phone_no"]!)"
         cell.textAdPhone.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        cell.reportOutlet.tag = indexPath.row
        cell.reportOutlet.addTarget(self, action: #selector(showReport(sender:)), for: .touchUpInside)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding : CGFloat! = 4

        let collectionViewSize = TextAdCollectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: 265)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    @objc func showReport(sender : UIButton)
    {
        self.ShowReportView.isHidden = false
        print("tagNo",sender.tag)
        index = sender.tag
    }

}

class NewTextAdCVC : UICollectionViewCell
{
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var textAdid: UILabel!
    @IBOutlet weak var textAdname: UILabel!
    @IBOutlet weak var textAdDescription: UILabel!
    @IBOutlet weak var textAdPhone: UILabel!
    @IBOutlet weak var reportOutlet: UIButton!
    
}
