//
//  ChatMessageViewController.swift
//  Kaafoo
//
//  Created by esolz on 12/11/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import GoogleMaps
import GooglePlaces
import OpalImagePicker
import AVKit
import AVFoundation
import PusherSwift
import PDFKit


class ChatMessageViewController: GlobalViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate,GMSMapViewDelegate {
    
    
    
    var marker = GMSMarker()

    var tempData = Data()
    
    var dataAppendStatus : Bool! = false
    
    var entermsg : Bool! = false
    
    var audioDataURL = Data()
    
    var pusher: Pusher!
    
    var bottomPosition : Bool! = false
    
    var ScrollBegin : CGFloat!
    
    var ScrollEnd : CGFloat!
    
    var nextStart : String! = ""
    
    @IBOutlet weak var ChatInputView: UIView!
    
    @IBAction func recordBtn(_ sender: UIButton) {
        
        self.record()
    }
    @IBAction func SendBtn(_ sender: UIButton) {
        
       
        
        self.UploadToAudioServer()
    }
    @IBAction func StopBtn(_ sender: UIButton) {
        
        self.StopRecord()
    }
    @IBAction func PlayBtn(_ sender: UIButton) {
        
        self.playRecord()
    }
    @IBOutlet weak var AudioView: UIView!
    
    @IBAction func hideViewAudioBtn(_ sender: UIButton) {
        
        self.ViewAudio.isHidden = true
    }
    @IBOutlet weak var hideViewAudioBtnOutlet: UIButton!
    
    @IBOutlet weak var ViewAudio: UIView!
    
    var audioRecorder : AVAudioRecorder!
    
    var audioPlayer : AVAudioPlayer!
    
    var pdfstring : NSData!
    
    @IBOutlet weak var SendBtnOutlet: UIButton!
    
    @IBAction func MessageSendBtn(_ sender: UIButton) {
        
         print("clicked----")
        
       entermsg = true
        
       // self.getMessageListing()
        
        self.UploadImageToServer()
    }
    @IBOutlet weak var ImageCollectionView: UICollectionView!
    
    @IBAction func hideCollectionViewBtn(_ sender: UIButton) {
        
        self.ViewCollectionView.isHidden = true
    }
    @IBOutlet weak var hideCollectionViewBtnOutlet: UIButton!
    
    @IBOutlet weak var ViewCollectionView: UIView!
    
    @IBAction func mapviewHideBtn(_ sender: UIButton) {
        
        self.ViewMap.isHidden = true
    }
    var SelectedIndex : Int!
    
    var localImageArray : NSMutableArray! = NSMutableArray()
    
    let opalImagepicker = OpalImagePickerController()
    
    @IBOutlet weak var mapViewHideBtnOutlet: UIButton!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var ViewMap: UIView!
    
    @IBOutlet weak var MessagesTableView: UITableView!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var imagePicker = UIImagePickerController()
    
    var MessageArray = ["0","1","0","1","0","1","0","1","0","1"]
    
    let dispatchGroup = DispatchGroup()
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    var ChatLatitude : String! = ""
    
    var ChatLongitude : String! = ""
    
    var SenderId : String! = ""
    
    var receivername : String! = ""
    
    var locationManager = CLLocationManager()
    
    var imageDataArray = [Data]()
    
    var imageData = Data()
    
    var StartFrom : Int! = 0
    
    var PerLoad : Int! = 10
    
    var Messages = [FetchMessages]()
    
    var totalMessages = [FetchMessages]()
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    @IBOutlet weak var MessageTextView: UITextView!
    
    @IBAction func microphoneBtn(_ sender: UIButton) {
        
        self.ViewAudio.isHidden = false
    }
    
    @IBAction func LocationBtn(_ sender: UIButton) {
        
        self.ChatLatitude = "\(currentLocation.coordinate.latitude)"
        
        self.ChatLongitude = "\(currentLocation.coordinate.longitude)"
        
        let latitude = Double(self.ChatLatitude)
        let longitude = Double( self.ChatLongitude)
                   
        let position = CLLocationCoordinate2DMake(latitude!,longitude!)
        print("position------",position)
                   
        let mapMarker = GMSMarker(position: position)
                   
                  
        let markerImage : UIImage = UIImage(named:"KaafooMarker")!
        let markerView = UIImageView(image: markerImage)
        mapMarker.iconView = markerView
                   
        mapMarker.map = self.mapView
        
       
        self.dispatchGroup.enter()
        
        self.ViewMap.isHidden = false
        
        
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            self.moveCamera()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                 self.takeScreenshot()
            }
        })
    }
    
    // MARK:- // Move Camera
    
    func moveCamera() {
        
        let lat  = "\(currentLocation.coordinate.latitude)"
        let long = "\(currentLocation.coordinate.longitude)"
        
        let coordinates = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(1)
        
        CATransaction.commit()
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(1)
        
        // Center Map View
        let camera = GMSCameraUpdate.setTarget(coordinates)
        mapView.animate(with: camera)
        
        CATransaction.commit()
        
    }
    
    @IBAction func AttachmentBtn(_ sender: UIButton) {
        
        self.CreateAlert()
    }
    @IBOutlet weak var AttachmentBtnOutlet: UIButton!
    
    @IBOutlet weak var LocationBtnOutlet: UIButton!
    
    @IBOutlet weak var microphoneBtnOutlet: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print("receivername------",receivername)
        
       
        
        UserDefaults.standard.set(receivername, forKey:"revname")
        
        headerView.headerViewTitle.text = receivername
        
        
        self.MessageTextView.text = ""
        
        self.ViewCollectionView.isHidden = true
        
        self.ViewMap.isHidden = true
        
        self.getMessageListing()
        
        locManager.requestWhenInUseAuthorization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateList), name: NSNotification.Name(rawValue: "update"), object: nil)
        

        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
           CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location
        }
        
        mapView.camera = GMSCameraPosition.camera(withLatitude: Double("25")!, longitude: Double("22")!, zoom: 10)
        //mapVIew.mapType = .satellite
        mapView.mapType = .normal
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        //mapVIew.padding = UIEdgeInsets(top: 0, left: 0, bottom: 90, right: 0)
        mapView.delegate = self
        
        
        
        
        
        
//        let camera = GMSCameraPosition.camera(withLatitude: 23.931735,longitude: 121.082711, zoom: 7)
//
//                    self.ChatLatitude = "22.88"
//
//                    self.ChatLongitude = "88.66"
//
//
//        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//
//             let latitude = Double(self.ChatLatitude)
//            let longitude = Double( self.ChatLongitude)
//
//            let position = CLLocationCoordinate2DMake(latitude!,longitude!)
//            print("position------",position)
//
//            let mapMarker = GMSMarker(position: position)
//
//
//            let markerImage : UIImage = UIImage(named:"KaafooMarker")!
////            let markerView = UIImageView(image: markerImage)
//            mapMarker.icon = markerImage
//
//            mapMarker.map = self.mapView
            
            
            
        
        
//        self.ChatLatitude = "22.88"
//
//        self.ChatLongitude = "88.66"
//
//        let latitude = Double(self.ChatLatitude)
//        let longitude = Double( self.ChatLongitude)
//
//        let position = CLLocationCoordinate2DMake(latitude!,  longitude!)
//        print("position------",position)
//        let mapMarker = GMSMarker(position: position)
//
//
//
//        let markerImage : UIImage = UIImage(named:"KaafooMarker")!
//        let markerView = UIImageView(image: markerImage)
//        mapMarker.iconView = markerView
//
//        mapMarker.map = self.mapView
                
        
        self.locationManager.delegate = self
        
        self.locationManager.startUpdatingLocation()
        
        //MARK:- //Pusher Updation in Chat Section
        
        //push notifications
                     let options = PusherClientOptions(
                        
                            host: .cluster("ap2")
                          )

                          pusher = Pusher(
                            key: "e53a9c4a375968423d66",
                            options: options
                          )

                          pusher.delegate = self

                          // subscribe to channel
                     
                     //MARK:- //Friends who are Online Event CallBack
                     
                          let channel = pusher.subscribe("my-notification-channel")
        
                     let channelOne = pusher.subscribe("my-channel")

                          // bind a callback to handle an event
                     let _ = channel.bind(eventName: "my_noti_event", callback: { (data: Any?) -> Void in
                                if let data = data as? [String : AnyObject] {
                                    
                                    print("Data==",data)
                                 
                                }
                            })
                     
                     //MARK:- //One Person to One Person Chat Section
                     
                     let _ = channelOne.bind(eventName: "my_event", callback: { (data: Any?) -> Void in
                                        if let data = data as? [String : AnyObject] {
                                            
                                            print("DataOne==",data)
                                            
                                            guard let message = data["message"] as? String else {
                                                return
                                            }
                                            
                                            guard let sendname = data["created_at"] as? String else {
                                                        return
                                                
                                            }
                                            
                                            guard let userimage = data["userimage"] as? String else {
                                                                                                   return
                                                                                           
                                                                                       }
                                            print("userimage-----",userimage)
                                            
                                            print("sendname-----",sendname)
                                            
                                            print("message",message)
                                            
                                            guard let receiverid = data["receiverid"] as? String else {
                                                return
                                            }
                                            print("receiverid",receiverid)
                                            guard let senderid = data["senderid"] as? String else {
                                                return
                                            }
                                            print("senderid",senderid)
                                            guard let chatlongitude = data["chatlongitude"] as? String else {
                                                return
                                            }
                                            print("chatlongitude",chatlongitude)
                                            guard let chatlatitude = data["chatlatitude"] as? String else {
                                                return
                                            }
                                            print("chatlatitude",chatlatitude)
                                            guard let created_at = data["created_at"] as? String else {
                                                return
                                            }
                                            guard let pusherImageArray = data["image_array_pusher"] as? NSArray else {
                                                return
                                            }
                                            guard let pusherPdfArray = data["pdf_array_pusher"] as? NSArray else {
                                                return
                                            }
                                            
                                            guard let pusherAudioArray = data["audio_path_pusher"] as? NSArray else {
                                                return
                                            }
                                            
                                            self.totalMessages.append(FetchMessages(textMessage: "\(message)", imageURL: "Blank", audioURL: "Nothing", senderID: "\(message)", receiverID: "\(message)", SenderImage: "\(userimage)", ReceiverImage: "\(userimage)", MessageDate: "\(created_at)", SenderName: "\(sendname)", ReceiverName: "\(sendname)", ImageArray: pusherImageArray, PDFArray: pusherPdfArray, docArray: [], audioArray: pusherAudioArray, messageType: "R", ChatLat: chatlatitude, ChatLong: chatlongitude))
                                            
                                            
                                           // self.generateMarkers()
                                            
                                            
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update"), object: nil)
                                           //print("MessageArray",self.MessagesArray)
                                        }
                                    })

                          pusher.connect()

        
        // Do any additional setup after loading the view.
    }
    
    

     func generateMarkers() {
        
        
//        let latitude = Double(self.ChatLatitude)
//        let longitude = Double( self.ChatLongitude)
//
//        let position = CLLocationCoordinate2DMake(latitude!,  longitude!)
//        print("position------",position)
//        let mapMarker = GMSMarker(position: position)
//
//
//
//        let markerImage : UIImage = UIImage(named:"KaafooMarker")!
//        let markerView = UIImageView(image: markerImage)
//        mapMarker.iconView = markerView
//
//        mapMarker.map = self.mapView
        
        
//        var camera = GMSCameraPosition.camera(withLatitude: -33.86,
//            longitude: 151.20, zoom: 6)
//
//        var mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        //mapView.hidden = true
//
//        mapView.isMyLocationEnabled = true
//        self.view = mapView
//        //self.view = self.tableView
//
//        marker.position = CLLocationCoordinate2DMake(-33.86, 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = mapView

        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.ViewMap.isHidden = true
        
        self.bottomPosition = true
        
        SVProgressHUD.dismiss()
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    //MARK:- //Create AlertView with Multiple Sections
    
    func CreateAlert()
    {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Lato-Bold", size: 18.0)!]
        
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Lato-Regular", size: 16.0)!]

        let titleAttrString = NSMutableAttributedString(string: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), attributes: titleFont)
        
        let messageAttrString = NSMutableAttributedString(string:LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Which one do you want to send?", comment: ""), attributes: messageFont)

        alert.setValue(titleAttrString, forKey: "attributedTitle")
        
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        let firstAction : UIAlertAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Select Image", comment: ""), style: .default) { action -> Void in
            
            print("First Action pressed")
            
            self.SelectImageFunctions()
        }
        
        alert.addAction(firstAction)
        
        let secondAction : UIAlertAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Select Document", comment: ""), style: .default) { action -> Void in

            print("Second Action pressed")
            
            self.clickDocumentPicker()
        }
        
        alert.addAction(secondAction)
        
        let ThirdAction : UIAlertAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Cancel", comment: ""), style: .cancel) { action -> Void in

            print("Second Action pressed")
        }
        
        alert.addAction(ThirdAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:- //Select Image Button In Alert Button Functionality
    func SelectImageFunctions()
    {
        let alert : UIAlertController=UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Choose Image", comment: ""), message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Camera", comment: ""), style: UIAlertAction.Style.default)
            
            {
                UIAlertAction in
                self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Gallary", comment: ""), style: UIAlertAction.Style.default)
            
            {
                UIAlertAction in
                self.multipleImageSelection()
        }
        let cancelAction = UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key:  "Cancel", comment: ""), style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
        }

        // Add the actions
        imagePicker.delegate = self
        
        alert.addAction(cameraAction)
        
        alert.addAction(gallaryAction)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        }
    
    //MARK:- //  Select Video Functionality
    
    func SelectVideos()
    {
        imagePicker.sourceType = .savedPhotosAlbum
        
        imagePicker.delegate = self
        
        imagePicker.mediaTypes = ["public.movie"]
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- //Open Camera Functionality

    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            //imagePicker.mediaTypes = [""]
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            
            alert.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: "")
            
            alert.message = LocalizationSystem.sharedInstance.localizedStringForKey(key: "You_do_not_have_camera", comment: "")
            
            alert.addButton(withTitle: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Ok", comment: ""))
            
            alert.show()
        }
    }

    //MARK:- //Open Gallery Functionality

    func openGallary(){
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //imagePicker.mediaTypes = [""]
        self.present(imagePicker, animated: true, completion: nil)
    }

    //MARK:- //ImagePicker Delegate Methods
       
       func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
              self.dismiss(animated: true, completion: { () -> Void in
              })
              print("Image",image)
              //imageView.image = image
          }
       
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
           let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
           
           let imagefinal = tempImage.jpegData(compressionQuality: 1)
    
           imageData = imagefinal as! Data
    
           self.imageDataArray.append(imageData)
    
          // self.demoImageView.image = UIImage(named: "\(tempImage)")
           self.UploadImageToServer()
        
           print("ImageDetails",tempImage)
        
           self.dismiss(animated: true, completion: nil)

       }
    
    func UploadImageToServer() {
               
        self.globalDispatchgroup.enter()
               
               DispatchQueue.main.async {
                   SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
               }
               
               let langID = UserDefaults.standard.string(forKey: "langID")
        
                print("landID-------",langID)
               
               let parameters = [
                "lang_id" : "\(LangID!)" , "user_id" : "\(self.UserID!)" ,"receiver_id" : "\(SenderId!)" ,"msg" : "\(self.MessageTextView.text ?? "")" , "chatlatitude" : "\(currentLocation.coordinate.latitude)" ,"chatlongitude" : "\(currentLocation.coordinate.longitude)"]
               
               Alamofire.upload(
                   multipartFormData: { MultipartFormData in
                       
                       for (key, value) in parameters {
                           MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                       }
                    
                    print("Tempdata",self.tempData ?? "")
                    
//                   MultipartFormData.append(self.tempData, withName: "message_img[]", fileName: "", mimeType: "application/pdf")
                    
//                    MultipartFormData.append(self.audioDataURL, withName: "blob", fileName: "iOSAudio.mp3", mimeType: "audio/m4a")
                    
                       print("Count",self.imageDataArray.count)
                       
                       for i in 0..<self.imageDataArray.count
                       {
                           MultipartFormData.append(self.imageDataArray[i], withName: "message_img[]", fileName: "messageimg\(i).jpeg", mimeType: "image/jpeg")
                       }
                    
                    
                       
               }, to: (GLOBALAPI + "app_user_send_message"))
               { (result) in
                print("result",result)
                   switch result {
                   case .success(let upload, _, _):
                       
                       upload.uploadProgress(closure: { (Progress) in
                           print("Upload Progress: \(Progress.fractionCompleted)")
                        
                        
                       })
                       
                       upload.responseJSON { response in
                        
                        print("response",response)
       //
                           print(response.request!)  // original URL request
                        
                           print(response.response!) // URL response
                        
                           print(response.data!)     // server data
                        
                           print(response.result)   // result of response serialization

                        if let JSON = response.result.value {
                            
                            print("Image Search Response-----: \(JSON)")
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                self.ViewMap.isHidden = true
                                
                                self.displayToastMessage(LocalizationSystem.sharedInstance.localizedStringForKey(key: "message_sent_successfully", comment: ""))
                                
                                self.MessageTextView.text = ""
                                self.generateMarkers()
                                //self.getMessageListing()
                                
                                
                                
                            }
                        }
                           else {
                               DispatchQueue.main.async {
                                   
                                   self.globalDispatchgroup.leave()
                                   
                                   SVProgressHUD.dismiss()
                                
                               }
                           }
                       }
                       
                   case .failure(let encodingError):
                       
                       print(encodingError)
                       
                       DispatchQueue.main.async {
                           
                           self.globalDispatchgroup.leave()
                           
                           SVProgressHUD.dismiss()
                       }
                   }
               }
           }
    
    
    //MARK:- //Upload to Audio Server
    func UploadToAudioServer()
    {
                   
            self.globalDispatchgroup.enter()
                   
                   DispatchQueue.main.async {
                       SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
                   }
                   
                   let langID = UserDefaults.standard.string(forKey: "langID")
                   
                   let parameters = [
                       "lang_id" : "\(LangID!)" , "user_id" : "\(self.UserID!)" ,"receiver_id" : "\(SenderId!)" ,"msg" : "\(self.MessageTextView.text ?? "")" , "chatlatitude" : "\(self.ChatLatitude!)" ,"chatlongitude" : "\(self.ChatLongitude!)"]
                   
                   Alamofire.upload(
                       multipartFormData: { MultipartFormData in
                           
                           for (key, value) in parameters {
                               MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                           }
                        
                        MultipartFormData.append(self.audioDataURL, withName: "blob", fileName: "iOSAudio.mp3", mimeType: "audio/m4a")
                        
                           
                   }, to: (GLOBALAPI + "app_user_send_message"))
                   { (result) in
                    print("result",result)
                       switch result {
                       case .success(let upload, _, _):
                           
                           upload.uploadProgress(closure: { (Progress) in
                               print("Upload Progress: \(Progress.fractionCompleted)")
                           })
                           
                           upload.responseJSON { response in
                            
                            print("response",response)
           //
                               print(response.request!)  // original URL request
                               print(response.response!) // URL response
                               print(response.data!)     // server data
                               print(response.result)   // result of response serialization

                               if let JSON = response.result.value {
                                   
                                   print("Image Search Response-----: \(JSON)")
                                   
                                   DispatchQueue.main.async {
                                       
                                       SVProgressHUD.dismiss()
                                    
                                    self.displayToastMessage(LocalizationSystem.sharedInstance.localizedStringForKey(key: "message_sent_successfully", comment: ""))
                                    
                                    self.MessageTextView.text = ""
                                    
                                    
                                    
                                   }
                               }
                               else {
                                   DispatchQueue.main.async {
                                       
                                       self.globalDispatchgroup.leave()
                                       
                                       SVProgressHUD.dismiss()
                                    
                                   }
                               }
                           }
                           
                       case .failure(let encodingError):
                           
                           print(encodingError)
                           
                           DispatchQueue.main.async {
                               
                               self.globalDispatchgroup.leave()
                               
                               SVProgressHUD.dismiss()
                           }
                       }
                   }
    }
    
    //MARK:- //Upload to PDF server
    func UploadToPdfServer()
    {
                self.globalDispatchgroup.enter()
                       
                       DispatchQueue.main.async {
                           SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
                       }
                       
                       let langID = UserDefaults.standard.string(forKey: "langID")
                       
                       let parameters = [
                        "lang_id" : "\(self.LangID!)" , "user_id" : "\(self.UserID!)" ,"receiver_id" : "\(self.SenderId!)" ,"msg" : "\(self.MessageTextView.text ?? "")" , "chatlatitude" : "\(self.ChatLatitude!)" ,"chatlongitude" : "\(self.ChatLongitude!)"]
                       
                       Alamofire.upload(
                           multipartFormData: { MultipartFormData in
                               
                               for (key, value) in parameters {
                                   MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                               }
                            
                            print("Tempdata",self.tempData ?? "")
                            
                           MultipartFormData.append(self.tempData, withName: "message_img[]", fileName: "iosPDF.pdf", mimeType: "application/pdf")
                               
                       }, to: (GLOBALAPI + "app_user_send_message"))
                        
                       { (result) in
                           switch result {
                           case .success(let upload, _, _):
                               
                               upload.uploadProgress(closure: { (Progress) in
                                   print("Upload Progress: \(Progress.fractionCompleted)")
                               })
                               
                               upload.responseJSON { response in
                                
                                print("response",response)
               //
                                   print(response.request!)  // original URL request
                                   print(response.response!) // URL response
                                   print(response.data!)     // server data
                                   print(response.result)   // result of response serialization

                                   if let JSON = response.result.value {
                                       
                                       print("PDF Response Response-----: \(JSON)")
                                       
                                       DispatchQueue.main.async {
                                           
                                           SVProgressHUD.dismiss()
                                        
                                        self.displayToastMessage(LocalizationSystem.sharedInstance.localizedStringForKey(key: "message_sent_successfully", comment: ""))
                                        
                                        self.MessageTextView.text = ""
                                       }
                                   }
                                   else {
                                       DispatchQueue.main.async {
                                           
                                           self.globalDispatchgroup.leave()
                                           
                                           SVProgressHUD.dismiss()
                                        
                                       }
                                   }
                               }
                               
                           case .failure(let encodingError):
                               
                               print(encodingError)
                               
                               DispatchQueue.main.async {
                                   
                                   self.globalDispatchgroup.leave()
                                   
                                   SVProgressHUD.dismiss()
                               }
                           }
                       }
                   }
    
    
    //Location Manager delegates
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last

       // let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!,longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        let camera = GMSCameraPosition.camera(withLatitude: 22.34,longitude: 88.76, zoom: 17.0)
        self.mapView?.animate(to: camera)
        
        self.ChatLatitude = "22.88"
               
        self.ChatLongitude = "88.66"

               
               self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)

                    let latitude = Double(self.ChatLatitude)
                   let longitude = Double( self.ChatLongitude)

                   let position = CLLocationCoordinate2DMake(latitude!,longitude!)
                   print("position------",position)

                   let mapMarker = GMSMarker(position: position)


                   let markerImage : UIImage = UIImage(named:"KaafooMarker")!
                   let markerView = UIImageView(image: markerImage)
                   mapMarker.iconView = markerView

                   mapMarker.map = self.mapView
                    
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
    
    @objc func updateList(){
    //write Reload data here......
      print("PICTURE IS UPLOADED")
        
        self.MessageTextView.text = ""
        
        self.bottomPosition = true
        
        self.imageDataArray.removeAll()
        
        self.MessagesTableView.reloadData()
        
        var tuneplayer : AVAudioPlayer!
        
        let path = Bundle.main.path(forResource: "itune", ofType : "mp3")!
        
        let url = URL(fileURLWithPath : path)

        do {
            tuneplayer = try AVAudioPlayer(contentsOf: url)
            
            tuneplayer.play()

        } catch {

            print ("There is an issue with this code!")

        }
        
        self.scrollToBottom()
        
    }
    
    //MARK:- //Get Individual Message Listing
       
       func getMessageListing()
       {
           let UserID = UserDefaults.standard.string(forKey: "userID")
        
           let LangID = UserDefaults.standard.string(forKey: "langID")
        
           print("userid",UserID!)
        
           let parameters = "lang_id=\(LangID!)&user_id=\(UserID!)&sender_id=\(SenderId!)&start_from=\(StartFrom!)&per_page=\(PerLoad!)"
        
           self.CallAPI(urlString: "app_user_message_listing", param: parameters, completion: {
            
               self.globalDispatchgroup.leave()
            
               let infoArray = self.globalJson["info_array"] as! NSArray
            
            self.recordsArray.removeAllObjects()
            
            self.Messages = []
            
            
               for i in 0..<infoArray.count
               {
                   let tempDict = infoArray[i] as! NSDictionary
                   self.recordsArray.add(tempDict)
//                self.Messages.append(FetchMessages(textMessage: "\(tempDict["message_info"] ?? "")", imageURL: "Blank", audioURL: "Nothing", senderID: "\(tempDict["sender_id"] ?? "")", receiverID: "\(tempDict["receiver_id"] ?? "")", SenderImage: "\(tempDict["sender_img"] ?? "")", ReceiverImage: "\(tempDict["receiver_img"] ?? "")", MessageDate: "\(tempDict["msg_date"] ?? "")", SenderName: "\(tempDict["sender_name"] ?? "")", ReceiverName: "\(tempDict["receiver_name"] ?? "")", ImageArray: (tempDict["message_image"] as! NSArray)))
               }
            
            
            var reversedRecordsArray : NSMutableArray! = NSMutableArray()
            
            reversedRecordsArray.removeAllObjects()
            
            reversedRecordsArray = self.recordsArray
            
            
//            for arrayIndex in stride(from: self.recordsArray.count - 1, through: 0, by: -1) {
//                reversedRecordsArray.add(self.recordsArray[arrayIndex])
//            }
            
            if self.dataAppendStatus == true
            {
                for i in 0...reversedRecordsArray.count - 1
                {
                    self.Messages.append(FetchMessages(textMessage: "\((reversedRecordsArray[i] as! NSDictionary)["message_info"] ?? "")", imageURL: "Blank", audioURL: "Nothing", senderID: "\((reversedRecordsArray[i] as! NSDictionary)["sender_id"] ?? "")", receiverID: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_id"] ?? "")", SenderImage: "\((reversedRecordsArray[i] as! NSDictionary)["sender_img"] ?? "")", ReceiverImage: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_img"] ?? "")", MessageDate: "\((reversedRecordsArray[i] as! NSDictionary)["msg_date"] ?? "")", SenderName: "\((reversedRecordsArray[i] as! NSDictionary)["sender_name"] ?? "")", ReceiverName: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_name"] ?? "")", ImageArray: ((reversedRecordsArray[i] as! NSDictionary)["message_image"] as! NSArray), PDFArray: ((reversedRecordsArray[i] as! NSDictionary)["message_pdf"] as! NSArray), docArray: ((reversedRecordsArray[i] as! NSDictionary)["message_doc"] as! NSArray), audioArray: ((reversedRecordsArray[i] as! NSDictionary)["message_audio"] as! NSArray), messageType: "\((reversedRecordsArray[i] as! NSDictionary)["msg_type"] ?? "")", ChatLat: "\((reversedRecordsArray[i] as! NSDictionary)["chat_location_lattitube"] ?? "")", ChatLong: "\((reversedRecordsArray[i] as! NSDictionary)["chat_location_longitude"] ?? "")"))
                }
            }
            else if self.dataAppendStatus == false
            {
                for i in 0...reversedRecordsArray.reversed().count - 1
                {
                    self.Messages.append(FetchMessages(textMessage: "\((reversedRecordsArray[i] as! NSDictionary)["message_info"] ?? "")", imageURL: "Blank", audioURL: "Nothing", senderID: "\((reversedRecordsArray[i] as! NSDictionary)["sender_id"] ?? "")", receiverID: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_id"] ?? "")", SenderImage: "\((reversedRecordsArray[i] as! NSDictionary)["sender_img"] ?? "")", ReceiverImage: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_img"] ?? "")", MessageDate: "\((reversedRecordsArray[i] as! NSDictionary)["msg_date"] ?? "")", SenderName: "\((reversedRecordsArray[i] as! NSDictionary)["sender_name"] ?? "")", ReceiverName: "\((reversedRecordsArray[i] as! NSDictionary)["receiver_name"] ?? "")", ImageArray: ((reversedRecordsArray[i] as! NSDictionary)["message_image"] as! NSArray), PDFArray: ((reversedRecordsArray[i] as! NSDictionary)["message_pdf"] as! NSArray), docArray: ((reversedRecordsArray[i] as! NSDictionary)["message_doc"] as! NSArray), audioArray: ((reversedRecordsArray[i] as! NSDictionary)["message_audio"] as! NSArray), messageType: "\((reversedRecordsArray[i] as! NSDictionary)["msg_type"] ?? "")" , ChatLat: "\((reversedRecordsArray[i] as! NSDictionary)["chat_location_lattitube"] ?? "")", ChatLong: "\((reversedRecordsArray[i] as! NSDictionary)["chat_location_longitude"] ?? "")"))
                }
            }
            
            for struc in 0..<self.Messages.count
            {
                let temp = self.Messages[struc]
                
                self.totalMessages.insert(temp, at: 0)
            }
            
            
            //self.totalMessages.append(<#T##newElement: FetchMessages##FetchMessages#>)
            
            //self.totalMessages = self.totalMessages.reversed()
                        
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.StartFrom = self.StartFrom + self.recordsArray.count
            
               self.globalDispatchgroup.notify(queue: .main, execute: {
                   DispatchQueue.main.async {
                    
                   self.MessagesTableView.delegate = self
                    
                   self.MessagesTableView.dataSource = self
                    
                   self.MessagesTableView.reloadData()
                    

                   self.scrollToBottom()
                    
                   SVProgressHUD.dismiss()
                   }
               })
           })
       }
    
    func multipleImageSelection()
    {
        opalImagepicker.imagePickerDelegate = self
        opalImagepicker.maximumSelectionsAllowed = 10
        present(opalImagepicker, animated: true, completion: nil)
    }
    
    //MARK:- //Document Picker method
    
    func clickDocumentPicker(){

    let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.image", "public.audio", "public.movie", "public.text", "public.item", "public.content", "public.source-code"], in: .import)
    documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
    documentPicker.delegate = self
    present(documentPicker, animated: true, completion: nil)
    }
    
    
    //MARK:- //Take Screenshot of MapView
      func takeScreenshot()
      {
          var image :UIImage?
          let currentLayer = UIApplication.shared.keyWindow!.layer
          let currentScale = UIScreen.main.scale
          UIGraphicsBeginImageContextWithOptions(currentLayer.frame.size, false, currentScale);
          guard let currentContext = UIGraphicsGetCurrentContext() else {return}
          currentLayer.render(in: currentContext)
          image = UIGraphicsGetImageFromCurrentImageContext()
          UIGraphicsEndImageContext()
          guard let img = image else { return }
          UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
          let imageFinal = img.jpegData(compressionQuality: 1)
          let imageDat = imageFinal as! Data
          self.imageDataArray.append(imageDat)
          self.UploadImageToServer()
      }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
       print("This message has more than one content")
        self.localImageArray.removeAllObjects()
        self.localImageArray = self.totalMessages[SelectedIndex].ImageArray.mutableCopy() as? NSMutableArray
        self.ViewCollectionView.isHidden = false
        self.ImageCollectionView.delegate = self
        self.ImageCollectionView.dataSource = self
        self.ImageCollectionView.reloadData()
        print("LocalImageArray",self.localImageArray)
        
    }
    
    //MARK:- //Scroll Tableview to the bottom while updating
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.totalMessages.count-1, section: 0)
            self.MessagesTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop()
    {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.totalMessages.count-1, section: 0)
            self.MessagesTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    
    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
    return filePath
    }
    
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //MARK:- //AV Audio Recorder
    func record(){
        
    let session = AVAudioSession.sharedInstance()
    do
    {
        try session.setCategory(.playAndRecord, mode: .default)
        try session.setActive(true)
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
        ]
        audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
        audioRecorder.delegate = self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
        
        if audioRecorder.isRecording == true
        {
            print("Start recording")
        }
        else
        {
            print("not recording")
        }
    }
    catch let error {
        self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Warning", comment: ""),message: error.localizedDescription)
    }
    }
    
    //MARK:- //Recorded audio playing using AudioPlayer in iOS Swift
    
    func playRecord()
    {
        if audioRecorder?.isRecording == false{
            
           var error : NSError?

            do {
                audioPlayer = try AVAudioPlayer(contentsOf: (audioRecorder?.url)!)
                self.audioDataURL = try Data(contentsOf: audioRecorder.url)
            } catch
            {
                print("Error",error)
            }
            audioPlayer?.delegate = self

            if let err = error{
                print("audioPlayer error: \(err.localizedDescription)")
            }else{
                print("Play Recording")
                audioPlayer?.play()
            }
        }
        else
        {
            //do nothing
        }
    }
    
    //MARK:- // Stop Audio Recorder in iOS Swift
    
    func StopRecord()
    {
        print("Stop Recording")
        if audioRecorder.isRecording == true
        {
            audioRecorder.stop()
        }
        else
        {
            audioPlayer.stop()
        }
    }
    
    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if scrollView == self.MessagesTableView
        {
            ScrollBegin = scrollView.contentOffset.y
        }
        else
        {
            //do nothing
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == self.MessagesTableView
        {
           ScrollEnd = scrollView.contentOffset.y
        }
        else
        {
            //do nothing
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.MessagesTableView
        {
            if ScrollBegin < ScrollEnd
            {

            }
            else
            {
                if (nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        
                    SVProgressHUD.dismiss()
                }
                }
                else
                {
                    self.dataAppendStatus = true
                    
                    self.bottomPosition = false
                    
                    self.getMessageListing()
                    
                    self.displayToastMessage("Loading Previous Messages")
                    
                }
            }
        }
        else
        {
            //do nothing
        }
        
        
    }
    
    //MARK:- //Select Tap Gesture for Downloading PDF file
    
    @objc func DownloadPDF(sender : UITapGestureRecognizer)
    {
        print("Download PDF")
        //Downloader.load(URL: URL(string: DownloadPDFParameters.shared.downloadPDFPath ?? "")! as NSURL)
        self.storeAndShare(withURLString: DownloadPDFParameters.shared.downloadPDFPath ?? "")
        //UIApplication.shared.beginIgnoringInteractionEvents()
//        guard let url = URL(string: DownloadPDFParameters.shared.downloadPDFPath ?? "")
//            else {
//            return
//        }
//        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
//        let downloadTask = urlSession.downloadTask(with: url)
//        downloadTask.resume()
        
    }
    
    //MARK:- //Selector For Sender Audio Download Gesture
    
    @objc func DownloadAudioSender(sender : UITapGestureRecognizer)
    {
        print("Download Audio Sender")
        self.storeAndShare(withURLString: DownloadAudioParameters.shared.downloadAudioPath ?? "")
        //UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    //MARK:- //Selector For Receiver Audio Download Gesture
    
    @objc func DownloadAudioReceiver(sender : UITapGestureRecognizer)
    {
        print("Download Audio Receiver")
        self.storeAndShare(withURLString: DownloadAudioParameters.shared.downloadAudioPath ?? "")
        //UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

class ReceiverMessageCell : UITableViewCell
{
    @IBOutlet weak var SenderName: UILabel!
    
    @IBOutlet weak var SenderProfileImage: UIImageView!
    
    @IBOutlet weak var SenderMessage: UILabel!
    
    @IBOutlet weak var SenderImageMessage: UIImageView!
    
    @IBOutlet weak var SenderVideoMessage: UIView!
    
    @IBOutlet weak var SenderAudioMessage: UIView!
    
    @IBOutlet weak var senderPdfPath: UILabel!
    
    @IBOutlet weak var senderAudioPath: UILabel!
    
    
}

class SenderMessageCell : UITableViewCell
{
    @IBOutlet weak var receiverAudioPath: UILabel!
    
    @IBOutlet weak var ReceiverImage: UIImageView!
    
    @IBOutlet weak var ReceiverName: UILabel!
    
    @IBOutlet weak var ReceiverMessage: UILabel!
    
    @IBOutlet weak var ReceiverImageMessage: UIImageView!
    
    @IBOutlet weak var ReceiverVideoMessage: UIView!
    
    @IBOutlet weak var ReceiverAudioMessage: UIView!
    
    @IBOutlet weak var receiverPdfpath: UILabel!
    
}

extension ChatMessageViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("COUNT",self.totalMessages.count)
        
        return self.totalMessages.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.SelectedIndex = indexPath.row
        
        
        if entermsg == true
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver") as! ReceiverMessageCell
            
            //cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            
            cell.contentView.backgroundColor = .clear
            
            //Multiple Image Array Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 0
            {
                cell.SenderImageMessage.isHidden = false
                
                let imageURL = "\((self.totalMessages[indexPath.row].ImageArray[0] as! NSDictionary)["image_path"] ?? "")"
                
                cell.SenderImageMessage.sd_setImage(with: URL(string: imageURL))
                
                cell.SenderImageMessage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "KaafooPlaceholder"))
            }
            else
            {
                cell.SenderImageMessage.isHidden = true
            }
            
            //Image Message Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 1
            {
                //
                let textLayer = CATextLayer()
                
                textLayer.frame = cell.SenderImageMessage.bounds
                
                textLayer.string = "Tap to see more images"
                
                textLayer.font = CGFont("Verdana" as CFString)!
                
                textLayer.fontSize = 12.0
                
                textLayer.foregroundColor = UIColor.white.cgColor
                
                cell.SenderImageMessage.layer.addSublayer(textLayer)
                //
                
                cell.SenderImageMessage.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                
                cell.SenderImageMessage.addGestureRecognizer(tap)
                
            }
            else
            {
                cell.SenderImageMessage.isUserInteractionEnabled = false
            }
            
            //PDF Message Checking
            
            if self.totalMessages[indexPath.row].PDFArray.count > 0
            {
                cell.SenderVideoMessage.isHidden = false
                ///
                //Create Attachment
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"PDFIcon")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                
                completeText.append(attachmentString)
                
                //Add your text to mutable string
                
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].PDFArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                cell.senderPdfPath.textAlignment = .center;
                
                cell.senderPdfPath.attributedText = completeText;
                //
                DownloadPDFParameters.shared.setDownloadPath(path: "\((self.totalMessages[indexPath.row].PDFArray[0] as! NSDictionary)["image_path"] ?? "")")
                
                let pdfGesture = UITapGestureRecognizer(target: self, action: #selector(self.DownloadPDF(sender:)))
                
                cell.senderPdfPath.isUserInteractionEnabled = true
                
                cell.senderPdfPath.addGestureRecognizer(pdfGesture)
            }
            else
            {
                cell.SenderVideoMessage.isHidden = true
            }
            
            //Audio Message Checking
            
            if self.totalMessages[indexPath.row].audioArray.count > 0
            {
                cell.SenderAudioMessage.isHidden = false
                //
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"support")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                completeText.append(attachmentString)
                
                //Add your text to mutable string
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                cell.senderAudioPath.textAlignment = .center;
                
                cell.senderAudioPath.attributedText = completeText;
                
                //
                DownloadAudioParameters.shared.setDownloadPath(path: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["image_path"] ?? "")")
                
                let audioTapgestureSender = UITapGestureRecognizer(target: self, action: #selector(self.DownloadAudioSender(sender:)))
                
                cell.senderAudioPath.isUserInteractionEnabled = true
                
                cell.senderAudioPath.addGestureRecognizer(audioTapgestureSender)
            }
            else
            {
                cell.SenderAudioMessage.isHidden = true
            }
            
            let imageURL = "\(self.totalMessages[indexPath.row].SenderImage ?? "")"
            
            print("SenderName","\(self.totalMessages[indexPath.row].SenderName ?? "")")
            
            cell.SenderProfileImage.sd_setImage(with: URL(string: imageURL))
            
            cell.SenderName.text = "\(self.totalMessages[indexPath.row].MessageDate ?? "")"
            print("sendername===",cell.SenderName.text)
            
            cell.SenderMessage.text = "\(self.totalMessages[indexPath.row].textMessage ?? "")"
            
            cell.selectionStyle = .none
            
            
            
            
            return cell
        }
            
            
        
        else if  "\(self.totalMessages[indexPath.row].senderID!)".elementsEqual(UserID!)
       // if "\(self.totalMessages[indexPath.row].messageType ?? "")".elementsEqual("S")
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver") as! ReceiverMessageCell
            
            //cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            
            cell.contentView.backgroundColor = .clear
            
            //Multiple Image Array Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 0
            {
                cell.SenderImageMessage.isHidden = false
                
                let imageURL = "\((self.totalMessages[indexPath.row].ImageArray[0] as! NSDictionary)["image_path"] ?? "")"
                
                cell.SenderImageMessage.sd_setImage(with: URL(string: imageURL))
                
                cell.SenderImageMessage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "KaafooPlaceholder"))
            }
            else
            {
                cell.SenderImageMessage.isHidden = true
            }
            
            //Image Message Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 1
            {
                //
                let textLayer = CATextLayer()
                
                textLayer.frame = cell.SenderImageMessage.bounds
                
                textLayer.string = "Tap to see more images"
                
                textLayer.font = CGFont("Verdana" as CFString)!
                
                textLayer.fontSize = 12.0
                
                textLayer.foregroundColor = UIColor.white.cgColor
                
                cell.SenderImageMessage.layer.addSublayer(textLayer)
                //
                
                cell.SenderImageMessage.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                
                cell.SenderImageMessage.addGestureRecognizer(tap)
                
            }
            else
            {
                cell.SenderImageMessage.isUserInteractionEnabled = false
            }
            
            //PDF Message Checking
            
            if self.totalMessages[indexPath.row].PDFArray.count > 0
            {
                cell.SenderVideoMessage.isHidden = false
                ///
                //Create Attachment
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"PDFIcon")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                
                completeText.append(attachmentString)
                
                //Add your text to mutable string
                
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].PDFArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                cell.senderPdfPath.textAlignment = .center;
                
                cell.senderPdfPath.attributedText = completeText;
                //
                DownloadPDFParameters.shared.setDownloadPath(path: "\((self.totalMessages[indexPath.row].PDFArray[0] as! NSDictionary)["image_path"] ?? "")")
                
                let pdfGesture = UITapGestureRecognizer(target: self, action: #selector(self.DownloadPDF(sender:)))
                
                cell.senderPdfPath.isUserInteractionEnabled = true
                
                cell.senderPdfPath.addGestureRecognizer(pdfGesture)
            }
            else
            {
                cell.SenderVideoMessage.isHidden = true
            }
            
            //Audio Message Checking
            
            if self.totalMessages[indexPath.row].audioArray.count > 0
            {
                cell.SenderAudioMessage.isHidden = false
                //
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"support")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                completeText.append(attachmentString)
                
                //Add your text to mutable string
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                cell.senderAudioPath.textAlignment = .center;
                
                cell.senderAudioPath.attributedText = completeText;
                
                //
                DownloadAudioParameters.shared.setDownloadPath(path: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["image_path"] ?? "")")
                
                let audioTapgestureSender = UITapGestureRecognizer(target: self, action: #selector(self.DownloadAudioSender(sender:)))
                
                cell.senderAudioPath.isUserInteractionEnabled = true
                
                cell.senderAudioPath.addGestureRecognizer(audioTapgestureSender)
            }
            else
            {
                cell.SenderAudioMessage.isHidden = true
            }
            
            let imageURL = "\(self.totalMessages[indexPath.row].SenderImage ?? "")"
            
            print("SenderName","\(self.totalMessages[indexPath.row].SenderName ?? "")")
            
            cell.SenderProfileImage.sd_setImage(with: URL(string: imageURL))
            
            cell.SenderName.text = "\(self.totalMessages[indexPath.row].MessageDate ?? "")"
            print("sendername===",cell.SenderName.text)
            
            cell.SenderMessage.text = "\(self.totalMessages[indexPath.row].textMessage ?? "")"
            
            cell.selectionStyle = .none
            
            return cell
        }
            
        
            else
            //if "\(self.Messages[indexPath.row].senderID!)" != "\(SenderId ?? "")"
       // else if "\(self.totalMessages[indexPath.row].messageType ?? "")".elementsEqual("R")
        {
            let celltask = tableView.dequeueReusableCell(withIdentifier: "sender") as! SenderMessageCell
            
            //celltask.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            
            celltask.contentView.backgroundColor = .clear
            
            //celltask.contentView.backgroundColor = .red
            
            //Multiple Image Array Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 0
            {
                celltask.ReceiverImageMessage.isHidden = false
                
             
                let imageURL = "\((self.totalMessages[indexPath.row].ImageArray[0] as! NSDictionary)["image_path"] ?? "")"
                
                celltask.ReceiverImageMessage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "KaafooPlaceholder"))
            }
            else
            {
                celltask.ReceiverImageMessage.isHidden = true
            }
            
            //Image Message Checking
            
            if self.totalMessages[indexPath.row].ImageArray.count > 1
            {
                
                 //
                let textLayer = CATextLayer()
                
                textLayer.frame = celltask.ReceiverImageMessage.bounds
                
                textLayer.string = "Tap to see more images"
                
                textLayer.font = CGFont("Verdana" as CFString)!
                
                textLayer.fontSize = 12.0
                
                textLayer.foregroundColor = UIColor.white.cgColor
                
                celltask.ReceiverImageMessage.layer.addSublayer(textLayer)
                //
                
                celltask.ReceiverImageMessage.isUserInteractionEnabled = true
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                
                celltask.ReceiverImageMessage.addGestureRecognizer(tap)
                
            }
            else
            {
                celltask.ReceiverImageMessage.isUserInteractionEnabled = false
            }
            
            //PDF Message Checking
            
            if self.totalMessages[indexPath.row].PDFArray.count > 0
            {
                celltask.ReceiverVideoMessage.isHidden = false
                
                //
                //Create Attachment
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"PDFIcon")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                completeText.append(attachmentString)
                
                //Adżd your text to mutable string
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].PDFArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                celltask.receiverPdfpath.attributedText = completeText
                
                //
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.DownloadPDF(sender:)))
                
                celltask.receiverPdfpath.isUserInteractionEnabled = true
                
                celltask.receiverPdfpath.addGestureRecognizer(tapGesture)
                
            }
            else
            {
                celltask.ReceiverVideoMessage.isHidden = true
            }
            
            //Audio Message Checking
            
            if self.totalMessages[indexPath.row].audioArray.count > 0
            {
                celltask.ReceiverAudioMessage.isHidden = false
                //
                let imageAttachment =  NSTextAttachment()
                
                imageAttachment.image = UIImage(named:"support")
                
                //Set bound to reposition
                let imageOffsetY:CGFloat = -5;
                
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 20, height: 20)
                
                //Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                
                //Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                
                //Add image to mutable string
                completeText.append(attachmentString)
                
                //Add your text to mutable string
                let  textAfterIcon = NSMutableAttributedString(string: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["display_filename"] ?? "")")
                
                completeText.append(textAfterIcon)
                
                celltask.receiverAudioPath.textAlignment = .center;
                
                celltask.receiverAudioPath.attributedText = completeText;
                
                //
                
                DownloadAudioParameters.shared.setDownloadPath(path: "\((self.totalMessages[indexPath.row].audioArray[0] as! NSDictionary)["image_path"] ?? "")")
                
                
                celltask.receiverAudioPath.isUserInteractionEnabled = true
                
                let audioTapgestureReceiver = UITapGestureRecognizer(target: self, action: #selector(self.DownloadAudioReceiver(sender:)))
                
                celltask.receiverAudioPath.addGestureRecognizer(audioTapgestureReceiver)
                
            }
            else
            {
                celltask.ReceiverAudioMessage.isHidden = true
            }
            
            celltask.ReceiverName.text = "\(self.totalMessages[indexPath.row].ReceiverName ?? "")"
            
            
            
            let imageURLone = "\(self.totalMessages[indexPath.row].ReceiverImage ?? "")"
            
            celltask.ReceiverImage.sd_setImage(with: URL(string: imageURLone))
            
            print("ReceiverName-----=====","\(self.totalMessages[indexPath.row].ReceiverName ?? "")")
            
            celltask.ReceiverMessage.text = "\(self.totalMessages[indexPath.row].textMessage ?? "")"
            
            celltask.selectionStyle = .none
            
            return celltask
        }
            
//        else
//        {
//            let mcell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
//            return mcell
//        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("nothing")
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        UIView.animate(withDuration: 0.4) {
            
            cell.transform = CGAffineTransform.identity
        }
    }
}


struct FetchMessages
{
    var textMessage : String?
    
    var imageURL : String?
    
    var audioURL : String?
    
    var senderID : String?
    
    var receiverID : String?
    
    var SenderImage : String?
    
    var ReceiverImage : String?
    
    var MessageDate : String?
    
    var SenderName : String?
    
    var ReceiverName : String?
    
    var ImageArray : NSArray!
    
    var PDFArray : NSArray!
    
    var docArray : NSArray!
    
    var audioArray : NSArray!
    
    var messageType : String?
    
     var chatlat : String?
    
     var chatlong : String?

    
    init(textMessage : String, imageURL : String , audioURL : String , senderID : String , receiverID : String , SenderImage : String , ReceiverImage : String , MessageDate : String , SenderName : String , ReceiverName : String , ImageArray : NSArray , PDFArray : NSArray , docArray : NSArray , audioArray : NSArray , messageType : String , ChatLat : String , ChatLong : String)
    {
        self.textMessage = textMessage
        
        self.imageURL = imageURL
        
        self.audioURL = audioURL
        
        self.senderID = senderID
        
        self.receiverID = receiverID
        
        self.SenderImage = SenderImage
        
        self.ReceiverImage = ReceiverImage
        
        self.MessageDate = MessageDate
        
        self.SenderName = SenderName
        
        self.ReceiverName = ReceiverName
        
        self.ImageArray = ImageArray
        
        self.PDFArray = PDFArray
        
        self.docArray = docArray
        
        self.audioArray = audioArray
        
        self.messageType = messageType
        
        self.chatlat = ChatLat
        
        self.chatlong = ChatLong
        
    }
}

extension ChatMessageViewController : OpalImagePickerControllerDelegate
{
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        //Save Images, update UI
        //Dismiss Controller
        self.imageDataArray = []
        print("images",images)
        
      //  DispatchQueue.global(qos: .background).async {
            
                       for i in 0..<images.count
                       {
                           let imageFinal = images[i].jpegData(compressionQuality: 1)
                           let imageDat = imageFinal as! Data
                           self.imageDataArray.append(imageDat)
                       }
                //   }
        self.UploadImageToServer()
        
        print("Selected")
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 1
    }
}

extension ChatMessageViewController : UIDocumentMenuDelegate,UIDocumentPickerDelegate
{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        print("do nothing")
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        do {
         //let tempDocURL = try String(contentsOfFile: urls[0].path)
            let tempUrl = NSData(contentsOf: urls[0])
            
            let pdfString : String = tempUrl!.base64EncodedString(options: .endLineWithLineFeed)
            
            self.pdfstring = NSData(contentsOf: urls[0])
            self.tempData = Data(base64Encoded: pdfString, options: .ignoreUnknownCharacters)!
//            self.pdfstring = tempUrl!.base64EncodedString(options: .endLineWithLineFeed)
//            print("PDFUrl",self.pdfstring ?? "")
//            print("tempdocurl",tempDocURL)
            self.UploadToPdfServer()
        } catch _ {
            print("do nothing")
        }
        
    }
    
    
}

class ImagesCVC : UICollectionViewCell
{
    @IBOutlet weak var ShowImage: UIImageView!
}

//MARK:- //CollectionView to Show Multiple Images in Chat Section

extension ChatMessageViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.localImageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvc", for: indexPath) as! ImagesCVC
        
        let imageURL = "\((self.localImageArray[indexPath.row] as! NSDictionary)["image_path"] ?? "")"
//        cell.ShowImage.contentMode = .scaleAspectFill
//        cell.ShowImage.clipsToBounds = true
        //cell.ShowImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "KaafooPlaceholder"), options: .progressiveLoad)
        cell.ShowImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "KaafooPlaceholder"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let collectionViewSize = self.ImageCollectionView.frame.size.width
        //return CGSize(width: collectionViewSize, height: 240)
        return CGSize(width: 320*(self.FullWidth) , height: 240)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}

//MARK:- //Download file using Downloader

class Downloader {
    
    class func load(URL: NSURL) {
        
        let sessionConfig = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        let request = NSMutableURLRequest(url: URL as URL)
        
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest){ data,response,error in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode

                
                if let pdfData = data {
                    
                let pathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("demoPDF.pdf")
                    
                 do {
                    
                     try pdfData.write(to: pathURL, options: .atomic)
                    
                     print("Writing data Locally")
                    
                 }catch{
                    
                     print("Error while writting")
                 }
                }
                
                print("Success: \(statusCode)")
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription);
            }
        }
        task.resume()
    }
}


extension ChatMessageViewController : URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        print("location",location)
    }
    
    
}

