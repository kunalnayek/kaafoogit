//
//  SharingBox.swift
//  Kaafoo
//
//  Created by esolz on 16/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class SharingClass : UIView,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate
{
    @IBOutlet var ContentView: UIView!
    @IBOutlet weak var twitterBtnOutlet: UIButton!
    @IBOutlet weak var WhatsappBtbOutlet: UIButton!
    @IBOutlet weak var InstagramBtnOutlet: UIButton!
    @IBOutlet weak var LineBtnOutlet: UIButton!
    @IBOutlet weak var facebookBtnOutlet: UIButton!
    @IBOutlet weak var wechatBtnOutlet: UIButton!
    @IBOutlet weak var GmailBtnOutlet: UIButton!
    @IBOutlet weak var SmsBtnOutlet: UIButton!
    
    var VC = UIViewController()
    
    
    var LineShareURL : String! = ""
    
    
    var twitterTxt : String! = ""
    var twitterUrl : String! = ""
    
    var whatsAppText : String! = ""
    
    var ProductLink : String! = ""
    
    let fbButton : FBShareButton = FBShareButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("Sharing", owner: self, options: nil)
        addSubview(ContentView)
        
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        
        fbButton.frame = CGRect(x: 20, y: 30, width: 80, height: 20)
//        fbButton.center = self.facebookBtnOutlet.center
        self.facebookBtnOutlet.addSubview(fbButton)
        fbButton.titleLabel?.text = ""
        
      
        
        twitterBtnOutlet.addTarget(self, action: #selector(twitterTarget(sender:)), for: .touchUpInside)
        WhatsappBtbOutlet.addTarget(self, action: #selector(whatsappTarget(sender:)), for: .touchUpInside)
        InstagramBtnOutlet.addTarget(self, action: #selector(instagramAddTarget(sender:)), for: .touchUpInside)
        
//        GmailBtnOutlet.addTarget(self, action: #selector(EmailTarget(sender:)), for: .touchUpInside)
        SmsBtnOutlet.addTarget(self, action: #selector(SMStarget(sender:)), for: .touchUpInside)
        
        facebookBtnOutlet.addTarget(self, action: #selector(faceBookShare(sender:)), for: .touchUpInside)
        
        LineBtnOutlet.addTarget(self, action: #selector(LineTarget(sender:)), for: .touchUpInside)
        
    }
    
    @objc func twitterTarget(sender : UIButton)
    {
        twitterTxt = "your text"
        twitterUrl = "http://stackoverflow.com/"
        
        let shareString = "https://twitter.com/intent/tweet?text=\(twitterTxt ?? "")&url=\(ProductLink ?? "")"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.openURL(url!)
    }
    
    @objc func whatsappTarget(sender : UIButton)
    {
        whatsAppText = "YOUR MSG"
        let urlWhats = "whatsapp://send?text=\(ProductLink ?? "")"
        let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if let whatsappURL = NSURL(string: urlString) {
            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                UIApplication.shared.openURL(whatsappURL as URL)
            } else {
                // Cannot open whatsapp
            }
        }

    }
    
    //MARK:- //Instagram Button Add target method
    
    @objc func instagramAddTarget(sender : UIButton)
    {
        let instagramHooks = "https://lineit.line.me/share/ui?url=\(ProductLink ?? "")"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
            UIApplication.shared.openURL(instagramUrl! as URL)
        } else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: "https://line.me/ti/p/")! as URL)
        }
    }
    
    //MARK:- //SMS Sending Add Target MEthod
    
    @objc func SMStarget(sender : UIButton)
    {
        print("SMS target")
//        if (MFMessageComposeViewController.canSendText()) {
//            let controller = MFMessageComposeViewController()
//            controller.body = "Message Body"
//            controller.recipients = []
//            controller.messageComposeDelegate = self
////            self.VC.navigationController?.pushViewController(controller, animated: true)
//            self.VC.present(controller, animated: true, completion: nil)
//        }
        
        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "VC") as! ViewController
        self.VC.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    //MARK:- // MessageUI Delegate Method
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
//        self.VC.dismiss(animated: true, completion: nil)
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- //Sending Email Add Target Method
    
    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([])
        mailComposeVC.setSubject("Test")
        mailComposeVC.setMessageBody("Test Esolz", isHTML: false)
        return mailComposeVC
    }
    
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- //Send Email button Add target method
    @objc func EmailTarget(sender : UIButton)
    {
        let mailComposeViewController = configureMailComposer()
        if MFMailComposeViewController.canSendMail(){
            self.VC.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            print("Can't send email")
        }
    }
    
    //MARK:- //Line Button Add Target
    @objc func LineTarget(sender : UIButton)
    {
        let linetext = "https://lineit.line.me/share/ui?url=\(ProductLink ?? "")"
        let lineURL = NSURL(string: linetext)
        if UIApplication.shared.canOpenURL(lineURL! as URL) {
            UIApplication.shared.openURL(lineURL! as URL)
        } else
        {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: "https://line.me/ti/p/")! as URL)
        }
    }
    
    //MARK:- //Share Content in Facebook Using FBSDK
    @objc func FacebookShareTarget(sender : UIButton)
    {
       print("Nothing")
    }
    
    @objc func faceBookShare(sender : UIButton)
    {
        let content : ShareLinkContent = ShareLinkContent()
        content.contentURL = NSURL(string: ProductLink) as! URL
        fbButton.shareContent = content
    }
}
