//



//  EventBookingListingViewController.swift



//  Kaafoo



//



//  Created by IOS-1 on 07/03/19.



//  Copyright © 2019 ESOLZ. All rights reserved.



//







import UIKit



import SVProgressHUD







class EventBookingListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    
    
    
    @IBOutlet weak var EventListingTableView: UITableView!
    
    
    var startValue = 0
    
    
    
    var perLoad = 10
    
    
    
    
    
    
    
    var scrollBegin : CGFloat!
    
    
    
    var scrollEnd : CGFloat!
    
    
    
    
    
    
    
    var nextStart : String!
    
    
    
    
    
    
    
    var BookingId : String!
    
    
    
    
    
    
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    
    
    
    
    
    
    var EventBookingArray : NSMutableArray!
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        
        
        self.GetEventBookingListAPI()
        
        
        
        self.EventListingTableView.separatorStyle = .none
        
        self.SetFont()
        
        
        
        
        
        
        
        // Do any additional setup after loading the view.
        
        
        
    }
    
    
    
    //Tableview delegate and datasource methods
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return recordsArray.count
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let obj = tableView.dequeueReusableCell(withIdentifier: "bookinglisting") as! EventBookingListingTableViewCell
        
        
        
        obj.EventNumber.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["event_no"]!)"
        
        
        
        obj.EventDate.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["date"]!)"
        
        
        
        obj.EventName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["event_name"]!)"
        
        
        
        obj.EventBuyerName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["buyer_name"]!)"
        
        
        
        obj.PeopleNo.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["people_count"]!)"
        
        
        
        obj.selectionStyle = .none
        
        
        
        //Set Font For UIlabels Inside TableviewCell
        
        obj.EventNumber.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.EventDate.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.EventName.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.EventBuyerName.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.PeopleNo.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.NoOfPeopleLbl.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.BookingdateLbl.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.BuyerNameLbl.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.EventnameLbl.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.EventnumberLbl.font = UIFont(name: obj.EventNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        
        
        
        
        
        return obj
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return UITableView.automaticDimension
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookinglistingdetails") as! EventBookingListingDetailsViewController
        
        
        
        nav.EventId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["id"]!)"
        
        
        
        self.navigationController?.pushViewController(nav, animated: true)
        
        
        
    }
    

    //MARK:- ScrollView Delegate and Datasource Methods
    
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        
        
        scrollBegin = scrollView.contentOffset.y
        
        
        
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        
        
        scrollEnd = scrollView.contentOffset.y
        
        
        
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if scrollBegin > scrollEnd
        {

        }
        else
            
            
            
        {
            
            
            
            
            
            
            
//            print("next start : ",nextStart )
            
            
            
            
            
            
            
            if (nextStart).isEmpty
                
                
                
            {
                
                
                
                DispatchQueue.main.async {
                    
                    
                    
                    SVProgressHUD.dismiss()
                    
                    
                    
                }
                
                
                
            }
                
                
                
            else
                
                
                
            {
                
                
                
                GetEventBookingListAPI()
                
                
                
            }
            
            
            
            
            
            
            
        }
        
        
        
    }
    
    
    
    //MARK:- Set Font for HeaderView
    
    func SetFont()
        
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
    }
    
    
    
    
    
    
    
    
    
    
    
    /*
     
     
     
     // MARK: - Navigation
     
     
     
     
     
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     
     
     // Get the new view controller using segue.destination.
     
     
     
     // Pass the selected object to the new view controller.
     
     
     
     }

     
     
     */

    func GetEventBookingListAPI()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_event_booking_buyer_list", param: parameters, completion: {
            self.EventBookingArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.EventBookingArray.count-1)
            {
                let tempDict = self.EventBookingArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.recordsArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.EventListingTableView.delegate = self
                self.EventListingTableView.dataSource = self
                self.EventListingTableView.reloadData()
                self.EventListingTableView.estimatedRowHeight = UITableView.automaticDimension
                self.EventListingTableView.rowHeight = UITableView.automaticDimension
                SVProgressHUD.dismiss()
            }
        })
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
    
    
    
    
    
    
    
}
//MARK:- //Event Booking TableviewCEll
class EventBookingListingTableViewCell: UITableViewCell {



    @IBOutlet weak var CellView: UIView!

    @IBOutlet weak var EventNumber: UILabel!

    @IBOutlet weak var EventDate: UILabel!

    @IBOutlet weak var EventName: UILabel!

    @IBOutlet weak var EventBuyerName: UILabel!

    @IBOutlet weak var PeopleNo: UILabel!



    //Static labels Connection

    @IBOutlet weak var EventnumberLbl: UILabel!

    @IBOutlet weak var BookingdateLbl: UILabel!

    @IBOutlet weak var EventnameLbl: UILabel!

    @IBOutlet weak var BuyerNameLbl: UILabel!

    @IBOutlet weak var NoOfPeopleLbl: UILabel!







    override func awakeFromNib() {

        super.awakeFromNib()

        // Initialization code

    }



    override func setSelected(_ selected: Bool, animated: Bool) {

        super.setSelected(selected, animated: animated)



        // Configure the view for the selected state

    }



}
