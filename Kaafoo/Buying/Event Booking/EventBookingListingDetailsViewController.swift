//
//  EventBookingListingDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 07/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class EventBookingListingDetailsViewController: GlobalViewController {
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var EventNo: UILabel!
    @IBOutlet weak var EventDate: UILabel!
    @IBOutlet weak var SellerName: UITextField!
    @IBOutlet weak var SellerEmailId: UITextField!
    @IBOutlet weak var SellerPhoneNo: UITextField!
    @IBOutlet weak var EventMessage: UITextField!
    @IBOutlet weak var EventbookingDate: UITextField!
    @IBOutlet weak var EventBookingTime: UITextField!
    @IBOutlet weak var EventNumberOfpeople: UITextField!
    @IBOutlet weak var EventPrice: UITextField!
    @IBOutlet weak var EventStatus: UITextField!
    @IBOutlet weak var BookingNumberLbl: UILabel!
    
    @IBOutlet weak var SellerPhNoLbl: UILabel!
    @IBOutlet weak var SellerEmailIdLbl: UILabel!
    @IBOutlet weak var DateLbl: UILabel!
    
    @IBOutlet weak var EventStatusLbl: UILabel!
    @IBOutlet weak var EventPriceLbl: UILabel!
    @IBOutlet weak var PeopleNoLbl: UILabel!
    @IBOutlet weak var BookingTimeLbl: UILabel!
    @IBOutlet weak var BookingDateLbl: UILabel!
    @IBOutlet weak var SellerMessageLbl: UILabel!
    @IBOutlet weak var SellerNameLbl: UILabel!
    var infoArray : NSMutableDictionary!
    
    let dispatchGroup = DispatchGroup()
    var EventId : String!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDetails()
        self.SetFont()
        self.StringLocalization()

        // Do any additional setup after loading the view.
    }
    
    
    //API Fire Method
    func getDetails()
    {
    
    dispatchGroup.enter()
    
    DispatchQueue.main.async {
    SVProgressHUD.show()
    }
    
    let url = URL(string: GLOBALAPI + "app_event_booking_buyer_details")!   //change the url
    
    print("Cancel Booking URL : ", url)
    
    var parameters : String = ""
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    let langID = UserDefaults.standard.string(forKey: "langID")
    
    parameters = "user_id=\(userID!)&eventid=\(EventId!)&lang_id=\(langID!)"
    
    print("Parameters are : " , parameters)
    
    let session = URLSession.shared
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST" //set http method as POST
    
    do {
    request.httpBody = parameters.data(using: String.Encoding.utf8)
    
    
    }
//    catch let error {
//    print(error.localizedDescription)
//    }
    
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
    
    guard error == nil else {
    return
    }
    
    guard let data = data else {
    return
    }
    
    do {
    
    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
    print("Event Booking Details: " , json)
        
    if "\(json["response"] as! Bool)".elementsEqual("true")
    {
        self.infoArray = json["info_array"] as? NSMutableDictionary
        DispatchQueue.main.async
            {
            SVProgressHUD.dismiss()
                self.dispatchGroup.leave()
        }
        self.dispatchGroup.notify(queue: .main, execute:
            {
                self.dataFetch()
        })
    }
    else
    {
        DispatchQueue.main.async {
            
            SVProgressHUD.dismiss()
            
            self.dispatchGroup.leave()

            self.NoData()
            
        }
    }
    }
    
    } catch let error {
    print(error.localizedDescription)
    }
    })
    
    task.resume()
    }
    
    //Data Fetch Method
    func dataFetch()
    {
        self.EventName.text = "\(self.infoArray["event_name"]!)"
        self.EventNo.text = "\(self.infoArray["booking_no"]!)"
        self.EventDate.text = "\(self.infoArray["booking_date"]!)"
        self.SellerName.text = "\(self.infoArray["seller_name"]!)"
        self.SellerEmailId.text = "\(self.infoArray["email"]!)"
        self.SellerPhoneNo.text = "\(self.infoArray["phone_no"]!)"
        self.EventMessage.text = "\(self.infoArray["message"]!)"
        self.EventbookingDate.text = "\(self.infoArray["booking_date"]!)"
        self.EventBookingTime.text = "\(self.infoArray["booking_time"]!)"
        self.EventNumberOfpeople.text = "\(self.infoArray["noof_people"]!)"
        self.EventPrice.text = "\(self.infoArray["event_price"]!)"
        self.EventStatus.text = "\(self.infoArray["status"]!)"
    }
    
    func SetFont()
    {
        self.EventName.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventNo.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventDate.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerName.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerEmailId.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerPhoneNo.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventMessage.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventbookingDate.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventBookingTime.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventNumberOfpeople.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventPrice.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventStatus.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        self.BookingNumberLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerPhNoLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerEmailIdLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.DateLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventStatusLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventPriceLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.PeopleNoLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.BookingTimeLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.BookingDateLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerMessageLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerNameLbl.font = UIFont(name: self.EventName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
    }
    //Arabic Localization
    func StringLocalization()
    {
      self.BookingNumberLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        self.DateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        self.SellerNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sellerName", comment: "")
        self.SellerEmailIdLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "emailId", comment: "")
        self.SellerPhNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNo", comment: "")
        self.SellerMessageLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "message", comment: "")
        self.BookingDateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        self.BookingTimeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingTime", comment: "")
        self.EventNumberOfpeople.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "numberofpeople", comment: "")
        self.EventPriceLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventprice", comment: "")
        self.EventStatusLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
    }


    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
