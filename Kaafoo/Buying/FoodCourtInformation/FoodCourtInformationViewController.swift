//
//  FoodCourtInformationViewController.swift
//  Kaafoo
//
//  Created by admin on 15/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cosmos

class FoodCourtInformationViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {

    // Global Font Applied

    @IBOutlet weak var foodCourtInformationTableview: UITableView!
    @IBOutlet weak var ReviewMainView: UIView!
    @IBOutlet weak var ReviewWindowView: UIView!
    @IBOutlet weak var reviewTitleLbl: UILabel!
    @IBOutlet weak var Cross_BtnOutlet: UIButton!
    @IBAction func Cross_Btn(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.ReviewMainView)
            self.ReviewMainView.isHidden = true
        }
    }
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var CommentLbl: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var CommentView: UIView!
    @IBOutlet weak var CommentTxtView: UITextView!
    @IBAction func SaveBtn(_ sender: UIButton) {
        if reviewRating.elementsEqual("0")
        {
            let alert = UIAlertController(title: "Select Rating", message: "Please Select Any Valid Rating", preferredStyle: .alert)

            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

            alert.addAction(ok)

            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            sendReview()
        }
    }
    @IBOutlet weak var SaveBtnOutlet: UIButton!

    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var dateAndTimeView: UIView!
    @IBOutlet weak var dateAndTimeLBL: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    @IBOutlet weak var saveView: UIView!







    var reviewRating : String! = "0"
    //    var orderID : String!

    var productID : String!




    var serviceRequestID : String!

    var reviewType : String!

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var pastOrderArray : NSMutableArray!

    var orderID : String!

    var viewHeight : CGFloat!

    var sellerNameHeight : CGFloat!

    var cell : FoodCourtInformationTVCTableViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        set_font()

        getFoodCourtInformation()
        // rating.rating = 0.0
        rating.didTouchCosmos = didTouchCosmos
        rating.didFinishTouchingCosmos = didFinishTouchingCosmos




    }

    // MARK:- // Cosmos Methods

    private class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }

    private  func didTouchCosmos(_ rating: Double) {

        let ratingValue = FoodCourtInformationViewController.formatValue(rating)

        reviewRating = ratingValue

    }

    private func didFinishTouchingCosmos(_ rating: Double) {

        let ratingValue = FoodCourtInformationViewController.formatValue(rating)

        reviewRating = ratingValue

    }


    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return recordsArray.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! FoodCourtInformationTVCTableViewCell




        // put Data
        cell.orderNo.text = "\((recordsArray[indexPath.row] as! NSDictionary)["order_no"]!)"
        cell.foodCourtDate.text = "\((recordsArray[indexPath.row] as! NSDictionary)["order_date"]!)"
        cell.sellerName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_name"]!)"
        cell.totalItem.text = "\((recordsArray[indexPath.row] as! NSDictionary)["total_item"]!)"
        cell.totalPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["total_price"]!)"


        cell.foodCourtView.layer.borderColor = UIColor.lightGray.cgColor
        cell.foodCourtView.layer.borderWidth = 1.0

        //Review Button checking

        cell.addReview.addTarget(self, action: #selector(addUserReview(sender:)), for: .touchUpInside)
        cell.viewReview.addTarget(self, action: #selector(viewUserReview(sender:)), for: .touchUpInside)

        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["review_status"]!)" == "0"
        {
            cell.addReview.isHidden = false
            cell.viewReview.isHidden = true

        }
        else if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["review_status"]!)" == "1"
        {
            cell.addReview.isHidden = true
            cell.viewReview.isHidden = false
        }

        cell.addReview.tag = indexPath.row
        cell.viewReview.tag = indexPath.row

        cell.addReview.layer.cornerRadius = cell.addReview.frame.size.height / 2
        cell.viewReview.layer.cornerRadius = cell.viewReview.frame.size.height / 2

        // Localization
        cell.orderNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_no", comment: "")
        cell.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        cell.totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
        cell.totalItemLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalItem", comment: "")
        cell.sellerNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "seller_name", comment: "")

        // Set Font

        cell.orderNoLBL.font = UIFont(name: cell.orderNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.orderNo.font = UIFont(name: cell.orderNo.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.foodCourtDate.font = UIFont(name: cell.foodCourtDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.sellerNameLBL.font = UIFont(name: cell.sellerNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.sellerName.font = UIFont(name: cell.sellerName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalItemLBL.font = UIFont(name: cell.totalItemLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalItem.font = UIFont(name: cell.totalItem.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPriceLBL.font = UIFont(name: cell.totalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPrice.font = UIFont(name: cell.totalPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell

    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        orderID = "\((recordsArray[indexPath.row] as! NSDictionary)["order_id"]!)"

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtInformationDetailsVC") as! FoodCourtInformationDetailsViewController

        navigate.orderID = orderID

        self.navigationController?.pushViewController(navigate, animated: true)

    }




    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getFoodCourtInformation()
            }

        }
    }



    // MARK: - // JSON POST Method to get Past Order Data


    func getFoodCourtInformation()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_food_court_info_list", param: parameters, completion: {
            self.pastOrderArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.pastOrderArray.count-1)

            {

                let tempDict = self.pastOrderArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.pastOrderArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.foodCourtInformationTableview.delegate = self
                self.foodCourtInformationTableview.dataSource = self
                self.foodCourtInformationTableview.reloadData()
                self.foodCourtInformationTableview.rowHeight = UITableView.automaticDimension
                self.foodCourtInformationTableview.estimatedRowHeight = UITableView.automaticDimension
                SVProgressHUD.dismiss()
            }
        })
    }

    @objc func addUserReview(sender: UIButton)
    {
        self.rating.rating = 0
        self.CommentTxtView.layer.borderWidth = 1.0
        self.CommentTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.CommentTxtView.text = ""
        self.reviewTitleLbl.text = "Add User Review"


        self.CommentTxtView.isHidden = false
        self.comment.isHidden = true

        self.CommentTxtView.text = ""

        self.dateAndTimeView.isHidden = true
        self.saveView.isHidden = false

        self.view.bringSubviewToFront(self.ReviewMainView)
        self.ReviewMainView.isHidden = false

    }


    // MARK:- // View User Review

    @objc func viewUserReview(sender: UIButton) {

        self.rating.rating = Double("\(((self.recordsArray[sender.tag] as! NSDictionary)["review_details"] as! NSDictionary)["rating"] ?? "")")!
        self.CommentTxtView.layer.borderWidth = 1.0
        self.CommentTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.CommentTxtView.text = ""
        self.reviewTitleLbl.text = "View Review Section"

        self.CommentTxtView.isHidden = true
        self.comment.isHidden = false

        self.comment.text = "\(((self.recordsArray[sender.tag] as! NSDictionary)["review_details"] as! NSDictionary)["comment"] ?? "")"

        self.dateAndTimeView.isHidden = false
        self.saveView.isHidden = true

        self.dateAndTime.text = "\(((self.recordsArray[sender.tag] as! NSDictionary)["review_details"] as! NSDictionary)["datetime"] ?? "")"

        self.view.bringSubviewToFront(self.ReviewMainView)
        self.ReviewMainView.isHidden = false


    }


    //MARK:-  //Send Review

    func sendReview()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)&order_id=\(orderID!)&ratinginput=\(reviewRating!)&description=\(CommentTxtView.text!)"
        self.CallAPI(urlString: "app_addreview_foodlist", param: parameters, completion: {
//            print("Send Review Response: " , self.globalJson)
            DispatchQueue.main.async {

                SVProgressHUD.dismiss()

                let alert = UIAlertController(title: "Rating Response", message: "\(self.globalJson["message"]!)", preferredStyle: .alert)

                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in

                    UIView.animate(withDuration: 0.5)
                    {
                        self.view.sendSubviewToBack(self.ReviewMainView)
                        self.ReviewMainView.isHidden = true
                    }

                }

                alert.addAction(ok)

                self.present(alert, animated: true, completion: nil )

                self.globalDispatchgroup.leave()
            }
        })
    }

    // MARK:- // SETTING FONTS

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }

}
//MARK:- //Food Court Information Tableviewcell

class FoodCourtInformationTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var foodCourtView: UIView!
    @IBOutlet weak var orderNoLBL: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var foodCourtDate: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var sellerNameLBL: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var totalItemLBL: UILabel!
    @IBOutlet weak var totalItem: UILabel!
    @IBOutlet weak var totalPriceLBL: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var addReview: UIButton!
    @IBOutlet weak var viewReview: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}




