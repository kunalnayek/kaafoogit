//
//  ServiceInformationDetailsViewController.swift
//  Kaafoo
//
//  Created by admin on 12/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ServiceInformationDetailsViewController: GlobalViewController {
    
    // Global Font Applied
    
    @IBOutlet weak var serviceBriefInfoView: UIView!
    @IBOutlet weak var invoiceNoLBL: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var serviceDate: UILabel!
    
    
    @IBOutlet weak var sellerInfoView: UIView!
    @IBOutlet weak var sellerInfoTitleLBL: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var sellerEmail: UILabel!
    @IBOutlet weak var sellerContactNo: UILabel!
    @IBOutlet weak var statusLBL: UILabel!
    @IBOutlet weak var status: UILabel!
    
    
    @IBOutlet weak var bookingDateView: UIView!
    @IBOutlet weak var bookingDateLBL: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    
    
    @IBOutlet weak var bookingTimeView: UIView!
    @IBOutlet weak var bookingTimeLBL: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    
    
    
    
    
    var serviceInformationDetailsDictionary : NSDictionary!
    
    let dispatchGroup = DispatchGroup()
    
    var serviceRequestID : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
        set_font()

        getServiceInformationDetails()
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.putData()
            
        }
        
    }

    // MARK: - // JSON POST Method to get Service Detail Information Data

    func getServiceInformationDetails()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&service_req_id=\(serviceRequestID!)"
        self.CallAPI(urlString: "app_service_details", param: parameters, completion: {
            self.serviceInformationDetailsDictionary = self.globalJson["info_array"] as? NSDictionary


            DispatchQueue.main.async {

                SVProgressHUD.dismiss()

                self.globalDispatchgroup.leave()
            }
        })
    }
    
    
    
    // MARK:- // Put All Required Data fetched from Web
    
    func putData()
    {
        
        self.invoiceNo.text = "\(serviceInformationDetailsDictionary["booking_no"]!)"
        self.serviceName.text = "\(serviceInformationDetailsDictionary["service_name"]!)"
        self.serviceDate.text = "\(serviceInformationDetailsDictionary["date"]!)"
        self.sellerName.text = "\(serviceInformationDetailsDictionary["seller_name"]!)"
        self.sellerEmail.text = "\(serviceInformationDetailsDictionary["email"]!)"
        self.sellerContactNo.text = "\(serviceInformationDetailsDictionary["phno"]!)"
        self.status.text = "\(serviceInformationDetailsDictionary["status"]!)"
        self.bookingDate.text = "\(serviceInformationDetailsDictionary["booking_date"]!)"
        self.bookingTime.text = "\(serviceInformationDetailsDictionary["booking_time"]!)"
        
        self.invoiceNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "invoice_no", comment: "")
        self.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        self.sellerInfoTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "seller_info", comment: "")
        self.statusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        self.bookingDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        self.bookingTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingTime", comment: "")
        
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        invoiceNoLBL.font = UIFont(name: invoiceNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        invoiceNo.font = UIFont(name: invoiceNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        serviceName.font = UIFont(name: serviceName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        price.font = UIFont(name: price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dateLBL.font = UIFont(name: dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingDate.font = UIFont(name: bookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        sellerInfoTitleLBL.font = UIFont(name: sellerInfoTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        sellerName.font = UIFont(name: sellerName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        sellerEmail.font = UIFont(name: sellerEmail.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        sellerContactNo.font = UIFont(name: sellerContactNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        statusLBL.font = UIFont(name: statusLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        status.font = UIFont(name: status.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingDateLBL.font = UIFont(name: bookingDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingDate.font = UIFont(name: bookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingTimeLBL.font = UIFont(name: bookingTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingTime.font = UIFont(name: bookingTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }

    

}
