
//
//  WatchListViewController.swift
//  Kaafoo
//
//  Created by admin on 17/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage


class WatchListViewController: GlobalViewController {


    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var watchListTableView: UITableView!

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var watchListArray : NSMutableArray!

    var watchListProductID : String!


    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        set_font()

        self.getWatchlistData()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

    }


    // MARK:- // View Will Appear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.watchListTableView.estimatedRowHeight = 400
        self.watchListTableView.rowHeight = UITableView.automaticDimension

    }






    // MARK:- // Buttons

    // MARK:- // Remove from Watch List Button


    @objc func remove(sender: UIButton)
    {

        self.customAlert(title: "Confirm", message: "Are you sure you want to remove this product from watchlist?") {
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.watchListProductID = "\(tempDictionary["product_id"]!)"

            self.removeFromWatchList()

            self.recordsArray.removeObject(at: sender.tag)

            self.watchListTableView.reloadData()
        }
    }



    // MARK: - // JSON POST Method to get Watchlist Data

    func getWatchlistData() {


        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)"

        self.CallAPI(urlString: "app_my_watchlist", param: parameters) {

            self.watchListArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.watchListArray.count-1)

            {

                let tempDict = self.watchListArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.watchListArray.count

            print("Next Start Value : " , self.startValue)


            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.watchListTableView.delegate = self
                self.watchListTableView.dataSource = self
                self.watchListTableView.reloadData()

                SVProgressHUD.dismiss()
            }

        }

    }




    // MARK: - // JSON POST Method to remove Data from WatchList

    func removeFromWatchList()

    {

        let userID = UserDefaults.standard.string(forKey: "userID")

        var parameters : String = ""

        parameters = "user_id=\(userID!)&remove_product_id=\(watchListProductID!)"

        self.CallAPI(urlString: "app_remove_watchlist", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }

        }
    }


    // MARK:- // Set FOnt

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        //
        //        footerView.homeLabel.font = UIFont(name: footerView.homeLabel.font.fontName, size: CGFloat(Get_fontSize(size: 7)))
        //        footerView.walletLabel.font = UIFont(name: footerView.walletLabel.font.fontName, size: CGFloat(Get_fontSize(size: 7)))
        //        footerView.addLabel.font = UIFont(name: footerView.addLabel.font.fontName, size: CGFloat(Get_fontSize(size: 7)))
        //        footerView.dailyDealsLabel.font = UIFont(name: footerView.dailyDealsLabel.font.fontName, size: CGFloat(Get_fontSize(size: 7)))
        //
    }


    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }




}



// MARK: - // Scrollview Delegates

extension WatchListViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getWatchlistData()
            }
        }
    }
}



// MARK:- // Tableview Delegates

extension WatchListViewController: UITableViewDelegate,UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! WatchListTVCTableViewCell

        cell.cellTitle.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"] ?? "")"

        if ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_type"]!)").elementsEqual("B") {

            if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["special_offer_status"]!)".elementsEqual("0") {

                cell.priceTypeOneLBL.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"]!)"

                cell.priceTypeOneLBL.textColor = UIColor.seaGreen()

                cell.priceTypeOneLBL.isHidden = false
                cell.priceTypeTwoLBL.isHidden = true
                cell.bidView.isHidden = true

            }
            else {

                cell.priceTypeOneLBL.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency_symbol"] ?? "")" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["reserve_price"] ?? "")"

                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency_symbol"] ?? "")") + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"] ?? "")")

                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                cell.priceTypeTwoLBL.attributedText = attributeString

                cell.priceTypeOneLBL.textColor = UIColor.red()
                cell.priceTypeTwoLBL.textColor = UIColor.seaGreen()

                cell.priceTypeOneLBL.isHidden = false
                cell.priceTypeTwoLBL.isHidden = false
                cell.bidView.isHidden = true

            }

        }
        else if ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_type"]!)").elementsEqual("A") {

            cell.priceTypeOneLBL.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency_symbol"] ?? "")" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"] ?? "")"

            cell.bidFlag.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSMutableDictionary)["flag"]!)"))
            cell.bidCount.text = "1 Bid"

            cell.priceTypeOneLBL.textColor = UIColor.seaGreen()

            cell.priceTypeOneLBL.isHidden = false
            cell.priceTypeTwoLBL.isHidden = true
            cell.bidView.isHidden = false

        }
        else {

            cell.priceTypeOneLBL.isHidden = true
            cell.priceTypeTwoLBL.isHidden = true
            cell.bidView.isHidden = true

        }



        cell.closesInLBL.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["closes_in"]!)"

        let path = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_image"]!)"

        cell.productImage.sd_setImage(with: URL(string: path))

        cell.removeOutlet.tag = indexPath.row

        cell.removeOutlet.addTarget(self, action: #selector(WatchListViewController.remove(sender:)), for: .touchUpInside)


        // Set Font //////

        cell.cellTitle.font = UIFont(name: cell.cellTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.bidCount.font = UIFont(name: cell.bidCount.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
        cell.addressLineONe.font = UIFont(name: cell.addressLineONe.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
        //        cell.addressLineTwo.font = UIFont(name: cell.addressLineTwo.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
        cell.priceTypeOneLBL.font = UIFont(name: cell.priceTypeOneLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.priceTypeTwoLBL.font = UIFont(name: cell.priceTypeTwoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.removeOutlet.titleLabel?.font = UIFont(name: (cell.removeOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!

        cell.removeOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Remove", comment: ""), for: .normal)



        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        cell.cellView.layer.cornerRadius = 8

        return cell

    }

}
//MARK:- //Watchlist Tableviewcell
class WatchListTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var bidCount: UILabel!
    @IBOutlet weak var bidFlag: UIImageView!

    @IBOutlet weak var addressLineONe: UILabel!
    @IBOutlet weak var addressLineTwo: UILabel!
    @IBOutlet weak var auctionPrice: UILabel!
    @IBOutlet weak var strikeThroughAuctionPrice: UILabel!
    @IBOutlet weak var priceTypeOneLBL: UILabel!
    @IBOutlet weak var priceTypeTwoLBL: UILabel!

    @IBOutlet weak var removeOutlet: UIButton!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var closesInLBL: UILabel!

    @IBOutlet weak var productImage: UIImageView!

    @IBOutlet weak var priceAndBidStackview: UIStackView!
    @IBOutlet weak var bidView: UIView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



