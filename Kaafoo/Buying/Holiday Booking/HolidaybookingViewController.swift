//
//  HolidaybookingViewController.swift
//  Kaafoo

//  Created by IOS-1 on 06/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
import UIKit
import SVProgressHUD
class HolidaybookingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    var startValue = 0
    var perLoad = 10
    var type = "B"
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    var nextStart : String!
    var BookingId : String!
var recordsArray : NSMutableArray! = NSMutableArray()
    var HolidayListingArray : NSMutableArray!
    @IBOutlet weak var holidayBookingListingTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetholidayBookingListAPI()
        self.holidayBookingListingTableView.separatorStyle = .none
        self.SetFont()
        // Do any additional setup after loading the view
    }
    override func viewWillAppear(_ animated: Bool) {
        self.holidayBookingListingTableView.rowHeight = UITableView.automaticDimension
        self.holidayBookingListingTableView.estimatedRowHeight = UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "bookinglist") as! HolidayBookingTableViewCell
        obj.bookingNo.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["booking_no"]!)"
        obj.bookingStatus.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["status_name"]!)"
        obj.bookingproductname.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        obj.bookingPrice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["total_price"]!)"
        obj.checkindate.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["pickup_date"]!)"
        obj.checkoutDate.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["dropoff_date"]!)"
        obj.DeleteBtn.tag = indexPath.row
        obj.CancelBtn.tag = indexPath.row
        obj.AddReviewBtn.tag = indexPath.row
        obj.AddReviewBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        obj.AddReviewBtn.backgroundColor = UIColor.green
        
        obj.AddReviewBtn.titleLabel?.textColor = UIColor.white
        
        self.BookingId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["id"]!)"
        obj.CancelBtn.addTarget(self, action: #selector(cancelBookingtap(sender:)), for: .touchUpInside)
        obj.DeleteBtn.addTarget(self, action: #selector(deleteBookingTap(sender:)), for: .touchUpInside)

        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["cancel_btn"]!)".elementsEqual("")
        {
            obj.CancelView.isHidden = true
        }
        else
        {
            obj.CancelView.isHidden = false
        }
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["delete_btn"]!)".elementsEqual("")
        {
            obj.Delete_Cancel_View.isHidden = true
        }
        else
        {
            obj.Delete_Cancel_View.isHidden = false
        }
        //Review Button Checking
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["review_btn_status"]!)".elementsEqual("")
        {
            obj.Review_View.isHidden = true
        }
        else
        {
            obj.Review_View.isHidden = false
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["review_btn_status"]!)".elementsEqual("0")
            {
                obj.AddReviewBtn.setTitle("Add Review", for: .normal)
            }
            else
            {
                obj.AddReviewBtn.setTitle("View Review", for: .normal)
            }
        }

        obj.selectionStyle = .none
        
        
        
        //SET Font for Tableviewcell UiLabels
        
        obj.bookingNo.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.BookingNumberLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.bookingStatus.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.BookingStatusLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.ProductNameLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.bookingproductname.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.CheckoutLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.checkoutDate.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.checkindate.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.checkindateLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.TotalPriceLbl.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.bookingPrice.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.DeleteBtn.titleLabel?.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.CancelBtn.titleLabel?.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.AddReviewBtn.titleLabel?.font = UIFont(name: obj.bookingNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CellView.layer.cornerRadius = 8.0
//        obj.ShadowView.layer.cornerRadius = 8.0

        return obj

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func GetholidayBookingListAPI()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_holiday_booking_list", param: parameters, completion: {
            self.HolidayListingArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.HolidayListingArray.count-1)
            {
                let tempDict = self.HolidayListingArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.recordsArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.holidayBookingListingTableView.delegate = self
                self.holidayBookingListingTableView.dataSource = self
                self.holidayBookingListingTableView.reloadData()
                SVProgressHUD.dismiss()
            }
        })
    }
    //MARK:- ScrollView Delegate Methods
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
        }
        else
        {
//            print("next start : ",nextStart )
            if (nextStart).isEmpty
            {
            DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                GetholidayBookingListAPI()
            }
        }
    }
    //Delete Booking From List Methods
    func CancelBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&cancel_id=\(BookingId!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_holiday_bookinglist_cancel", param: parameters, completion: {
//            print("CANCEL BOOKING Response: " , self.globalJson)
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }
    // MARK:- // Delete Booking
    @objc func cancelBookingtap(sender: UIButton)
    {
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            self.BookingId = "\(tempDictionary["id"]!)"
            self.CancelBooking()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.startValue = 0
                self.recordsArray.removeAllObjects()
                self.GetholidayBookingListAPI()
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil )
    }
    
    
    
    //MARK:- Delete Button API Fire
    
    //MARK:- API Fire For Delete Button

    func DeleteBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&delete_id=\(BookingId!)&lang_id=\(langID!)&section_type=\(type)"
        self.CallAPI(urlString: "app_holiday_bookinglist_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()

                self.globalDispatchgroup.leave()
            }

        })
    }
    
    // MARK:- // Delete Booking
    @objc func deleteBookingTap(sender: UIButton)
    {
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            self.BookingId = "\(tempDictionary["id"]!)"
            self.DeleteBooking()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.startValue = 0
                self.recordsArray.removeAllObjects()
                self.GetholidayBookingListAPI()
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil )
    }
    
    func SetFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
}
//MARK:- //Holiday booking tableviewcell
class HolidayBookingTableViewCell: UITableViewCell {

    @IBOutlet weak var CellView: UIView!

    @IBOutlet weak var Delete_Cancel_View: UIView!



    @IBOutlet weak var Review_View: UIView!

    @IBOutlet weak var bookingNo: UILabel!

    @IBOutlet weak var bookingStatus: UILabel!

    @IBOutlet weak var bookingproductname: UILabel!

    @IBOutlet weak var checkindate: UILabel!

    @IBOutlet weak var bookingPrice: UILabel!

    @IBOutlet weak var checkoutDate: UILabel!

    @IBOutlet weak var DeleteBtn: UIButton!

    @IBOutlet weak var AddReviewBtn: UIButton!

    @IBOutlet weak var CancelView: UIView!

    @IBOutlet weak var CancelBtn: UIButton!
    @IBOutlet weak var ShadowView: ShadowView!
    @IBOutlet weak var BookingNumberLbl: UILabel!

    @IBOutlet weak var ProductNameLbl: UILabel!
    @IBOutlet weak var CheckoutLbl: UILabel!
    @IBOutlet weak var TotalPriceLbl: UILabel!
    @IBOutlet weak var checkindateLbl: UILabel!
    @IBOutlet weak var BookingStatusLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
// Initialization code

    }
    override func setSelected(_ selected: Bool, animated: Bool) {

        super.setSelected(selected, animated: animated)



        // Configure the view for the selected state

    }



}

