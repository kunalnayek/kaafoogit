//  ItemsWonViewController.swift
//  Kaafoo
//
//  Created by admin on 19/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import Cosmos

class ItemsWonViewController: GlobalViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate {

    // Global Font Applied

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var itemsWonArray : NSMutableArray! = NSMutableArray()

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var reviewType : String!

    var reviewRating : String! = "0"

    var orderID : String!

    var sellerID : String!

    var productID : String!


    @IBOutlet weak var itemsWonTableView: UITableView!
    @IBOutlet weak var reviewMainView: UIView!
    @IBOutlet weak var reviewWindowView: UIView!
    @IBOutlet weak var reviewTitleLBL: UILabel!
    @IBOutlet weak var crossImageview: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTXTView: UITextView!


    
    @IBOutlet weak var ViewAddedReview: UIView!
    @IBAction func HideViewBtn(_ sender: UIButton) {
        self.ViewAddedReview.isHidden = true
    }
    @IBOutlet weak var reviewView: ViewReview!
    

    // MARK:- // Cross Button

    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCrossButton(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.reviewMainView)
            self.reviewMainView.isHidden = true
        }
    }


    // MARK:- // Save Button

    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func tapOnSaveReview(_ sender: UIButton) {

        if reviewRating.elementsEqual("0")
        {
            self.ShowAlertMessage(title: "Select Rating", message: "Please Select Any Valid Rating")
        }
        else
        {
            sendReview()
        }

    }



    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        set_font()

        commentTXTView.delegate = self

        getItemsWonList()

        setupReviewMainView()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.commentView.layer.borderWidth = 2
        self.commentView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.commentView.layer.cornerRadius = (5/568)*self.FullHeight

        rating.didTouchCosmos = didTouchCosmos
        rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        
        
        self.ViewAddedReview.isHidden = true

    }


    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ItemsWonTVCTableViewCell



        cell.productTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        cell.bookTime.text = "Booked On " + "\((recordsArray[indexPath.row] as! NSDictionary)["booking_time"]!)"
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["order_price"]!)"
        cell.productTypeLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"

        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"

        cell.productImage.sd_setImage(with: URL(string: path))

        ///Set Font //////


        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.bookTime.font = UIFont(name: cell.bookTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productTypeLBL.font = UIFont(name: cell.productTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.addUserReviewOutlet.titleLabel?.font = UIFont(name: (cell.addUserReviewOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 11)))
        cell.addProductReviewOutlet.titleLabel?.font = UIFont(name: (cell.addProductReviewOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 11)))



        /////////////////

        cell.addUserReviewOutlet.layer.cornerRadius = cell.addUserReviewOutlet.frame.size.height / 2
        cell.addProductReviewOutlet.layer.cornerRadius = cell.addProductReviewOutlet.frame.size.height / 2

        cell.addUserReviewOutlet.setBackgroundColor(color: UIColor.yellow2(), forState: .normal)
        cell.addProductReviewOutlet.setBackgroundColor(color: UIColor.yellow2(), forState: .normal)

        ///////


        cell.addUserReview.tag = indexPath.row

//        cell.addUserReview.addTarget(self, action: #selector(ItemsWonViewController.addUserReview(sender:)), for: .touchUpInside)

        cell.addProductReview.tag = indexPath.row

//        cell.addProductReview.addTarget(self, action: #selector(ItemsWonViewController.addProductReview(sender:)), for: .touchUpInside)
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["review_button"] ?? "")".elementsEqual("0")
        {
            cell.reviewview.isHidden = true
        }
        
        else
            
        {
            
            cell.reviewview.isHidden = false
        
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_review_status"] ?? "")".elementsEqual("0")
            {
            cell.addProductReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_product_review", comment: ""), for: .normal)
            cell.addProductReview.addTarget(self, action: #selector(ItemsWonViewController.addProductReview(sender:)), for: .touchUpInside)
            }
            else
            {
            cell.addProductReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_product_review", comment: ""), for: .normal)
            cell.addProductReview.addTarget(self, action: #selector(ViewProductReview(sender:)), for: .touchUpInside)
            }
        
        
        
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["user_review_status"] ?? "")".elementsEqual("0")
            {
            cell.addUserReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: ""), for: .normal)
            cell.addUserReview.addTarget(self, action: #selector(ItemsWonViewController.addUserReview(sender:)), for: .touchUpInside)
            }
            else
            {
            cell.addUserReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_user_review", comment: ""), for: .normal)
            cell.addUserReview.addTarget(self, action: #selector(ViewReview(sender:)), for: .touchUpInside)
            }
            
        }
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none


        return cell

    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return ((170/568)*FullHeight)
        return UITableView.automaticDimension
    }




    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                getItemsWonList()
            }

        }
    }


    // MARK:- // Textview Delegates

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            commentTXTView.resignFirstResponder()
            return false
        }
        return true
    }

    
    //MARK:- //Objective C method
    
    @objc func ViewReview(sender : UIButton)
    {
        print("tag==",sender.tag)
        
        self.reviewMainView.isHidden = true
        self.view.bringSubviewToFront(ViewAddedReview)
        self.ViewAddedReview.isHidden = false
        
        self.reviewView.DescriptionTxt.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["user_review"] as! NSMutableArray)[0] as! NSDictionary)["user_review_comment"] ?? "")"
        self.reviewView.dateTimeTxt.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["user_review"] as! NSMutableArray)[0] as! NSDictionary)["user_review_date"] ?? "")"
        
    }
    
    @objc func ViewProductReview(sender : UIButton)
    {
        print("tag",sender.tag)
        
        self.reviewMainView.isHidden = true
        self.view.bringSubviewToFront(ViewAddedReview)
        self.ViewAddedReview.isHidden = false
        
        self.reviewView.DescriptionTxt.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["product_review"] as! NSMutableArray)[0] as! NSDictionary)["product_review_comment"] ?? "")"
        self.reviewView.dateTimeTxt.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["product_review"] as! NSMutableArray)[0] as! NSDictionary)["product_review_date"] ?? "")"
        
    }


    // MARK:- // Set Font

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

    }
    //MARK:- Get Listing using Global API
    func getItemsWonList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

//        let langID = UserDefaults.standard.string(forKey: "langID")

        let  parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_items_own", param: parameters, completion: {
            self.itemsWonArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.itemsWonArray.count - 1)

            {

                let tempDict = self.itemsWonArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)


            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.itemsWonArray.count

            print("Next Start Value : " , self.startValue)


            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.itemsWonTableView.delegate = self
                self.itemsWonTableView.dataSource = self
                self.itemsWonTableView.reloadData()
                self.itemsWonTableView.rowHeight = UITableView.automaticDimension
                self.itemsWonTableView.estimatedRowHeight = UITableView.automaticDimension

                SVProgressHUD.dismiss()


            }
        })
    }





    // MARK: - // JSON POST Method to Send Review

    func sendReview()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_add_user_review")!   //change the url

        print("Send Review URL------",url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)&order_id=\(orderID!)&seller_id=\(sellerID!)&ratinginput=\(reviewRating!)&description=\(commentTXTView.text!)&add_rev_type=\(reviewType!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Send Review Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        DispatchQueue.main.async {

                            SVProgressHUD.dismiss()

                            let alert = UIAlertController(title: "Rating Response", message: "\(json["message"]!)", preferredStyle: .alert)

                            let ok = UIAlertAction(title: "OK", style: .default) {
                                UIAlertAction in

                                UIView.animate(withDuration: 0.5)
                                {
                                    self.view.sendSubviewToBack(self.reviewMainView)
                                    self.reviewMainView.isHidden = true
                                }

                            }

                            alert.addAction(ok)

                            self.present(alert, animated: true, completion: nil )

                            self.dispatchGroup.leave()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.dispatchGroup.notify(queue: .main, execute: {
                            self.recordsArray.removeAllObjects()
                            self.startValue = 0
                            self.getItemsWonList()
                        })
                    }

                }
                else
                {
                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }
                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
        })

        task.resume()



    }


    // MARK:- // Add User Review

    @objc func addUserReview(sender: UIButton)
    {
        self.ViewAddedReview.isHidden = true
        
        UIView.animate(withDuration: 0.5)
        {
            self.rating.rating = 0
            self.commentTXTView.text = ""
            self.reviewTitleLBL.text = "Add user Review"

            self.reviewType = "U"

            self.orderID = "\((self.recordsArray[sender.tag] as! NSDictionary)["order_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_seller_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"

            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }

    // MARK:- // Add Product Review

    @objc func addProductReview(sender: UIButton)
    {
        self.ViewAddedReview.isHidden = true
        
        UIView.animate(withDuration: 0.5)
        {
            self.rating.rating = 0
            self.commentTXTView.text = ""
            self.reviewTitleLBL.text = "Add product Review"

            self.reviewType = "P"

            self.orderID = "\((self.recordsArray[sender.tag] as! NSDictionary)["order_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_seller_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"

            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }


    //

    public class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }

    private func didTouchCosmos(_ rating: Double) {

        let ratingValue = ItemsWonViewController.formatValue(rating)

        reviewRating = ratingValue

    }

    private func didFinishTouchingCosmos(_ rating: Double) {

        let ratingValue = ItemsWonViewController.formatValue(rating)

        reviewRating = ratingValue

    }


    // MARK:- // ReviewMainView Multilingual setup

    func setupReviewMainView()
    {
        reviewTitleLBL.font = UIFont(name: reviewTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        ratingLBL.font = UIFont(name: ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentLBL.font = UIFont(name: commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentTXTView.font = UIFont(name: (commentTXTView.font?.fontName)!, size: CGFloat(Get_fontSize(size: 15)))

        saveButtonOutlet.titleLabel?.font = UIFont(name: (saveButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!


        reviewTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: "")
        ratingLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rating", comment: "")
        commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        saveButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)

    }


    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }


}
//MARK:- //Items Won TableViewCEll
class ItemsWonTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var bookTime: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var addUserReviewOutlet: UIButton!
    @IBOutlet weak var addProductReviewOutlet: UIButton!
    @IBOutlet weak var productTypeLBL: UILabel!

    @IBOutlet weak var addUserReview: UIButton!
    @IBOutlet weak var addProductReview: UIButton!

    @IBOutlet weak var reviewview: UIView!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



