//
//  FoodCourtInformationDetailsViewController.swift
//  Kaafoo
//
//  Created by admin on 15/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class FoodCourtInformationDetailsViewController: GlobalViewController {
    
    // Global Font Applied
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    
    @IBOutlet weak var invoiceNoView: UIView!
    @IBOutlet weak var invoiceNoLBL: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    
    

    
    
    @IBOutlet weak var foodDetailsView: UIView!
    @IBOutlet weak var foodDetailsViewHeightConstraint: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var foodDate: UILabel!
    
    
    @IBOutlet weak var shippingDetailView: UIView!
    @IBOutlet weak var shippingDetailLBL: UILabel!
    @IBOutlet weak var shippingName: UILabel!
    @IBOutlet weak var shippingEmail: UILabel!
    @IBOutlet weak var shippingAddress: UILabel!
    @IBOutlet weak var shippingContactNo: UILabel!
    
    @IBOutlet weak var nearbyShippingtypePaymenttypeView: UIView!
    
    
    @IBOutlet weak var nearbyLBL: UILabel!
    @IBOutlet weak var nearby: UILabel!
    
    
    @IBOutlet weak var shippingTypeLBL: UILabel!
    @IBOutlet weak var shippingType: UILabel!
    
    
    @IBOutlet weak var paymentTypeLBL: UILabel!
    @IBOutlet weak var paymentType: UILabel!
    
    
    var foodCourtInformationDetailsDictionary : NSDictionary!
    
    var orderID : String!
    
    let dispatchGroup = DispatchGroup()
    
    var foodInformationView : UIView!
    
    var tempOrigin : CGFloat!
    
    var arrayIndex : Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.set_font()
        
        self.setLanguageStrings()
        
        self.getFoodCourtInformationDetails()
        
        self.dispatchGroup.notify(queue: .main) {
            
            self.fillData()
            
            self.createInformationList()
            
        }
        
        
        
    }
    
    
    // MARK: - // JSON POST Method to get Food Court Detail Information Data

    func getFoodCourtInformationDetails()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&order_id=\(orderID!)"
        self.CallAPI(urlString: "app_food_court_info_details", param: parameters, completion: {
            self.foodCourtInformationDetailsDictionary = self.globalJson["info_array"] as? NSDictionary
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    
    
    
    
    
    // MARK:- // Function to create the total information list
    
    func createInformationList()
    {
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        //y origin to initiate the first view from
        tempOrigin = 0//(35/568)*self.FullHeight
        
        //initiating loop to create views
        for i in 0..<(foodCourtInformationDetailsDictionary["product_details"] as! NSArray).count
        {
            arrayIndex = i
            
            
            // Food Name and Price View
            let foodNameAndPriceView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (60/568)*self.FullHeight))
            
            let foodNameAndQuantity = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight))
            
            let foodPriceAll = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight))
            
            let foodPriceSingle = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            foodNameAndQuantity.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["product_name"]!)" + " (Qty " + "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["quantity"]!)" + ")"
            
            foodPriceAll.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["sub_total"]!)"
            foodPriceAll.textAlignment = .right
            
            foodPriceSingle.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["original_price"]!)"
            
            
            // Setting Font
            
            foodNameAndQuantity.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            foodPriceAll.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            foodPriceSingle.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            
            
            foodNameAndPriceView.addSubview(foodNameAndQuantity)
            foodNameAndPriceView.addSubview(foodPriceAll)
            foodNameAndPriceView.addSubview(foodPriceSingle)
            
            
            
            // Extra Features View
            var extraFeaturesViewHeight = (0/568)*self.FullHeight
            
            let extraFeaturesView = UIView(frame: CGRect(x: 0, y: (60/568)*self.FullHeight, width: self.FullWidth, height: 0))
            
            if (((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray).count > 0
            {
                extraFeaturesViewHeight = (CGFloat((((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["feature_info"] as! NSArray).count) * (40/568)*self.FullHeight) + (40/568)*self.FullHeight
                
                extraFeaturesView.frame = CGRect(x: 0, y: (60/568)*self.FullHeight, width: self.FullWidth, height: extraFeaturesViewHeight)
                
                let extraFeaturesLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: self.FullWidth - (20/320)*self.FullWidth, height: (20/568)*self.FullHeight))
                
                extraFeaturesLBL.text = "Extra Features"
                
                
                // Arabic and English UI
                if (langID?.elementsEqual("EN"))!
                {
                    extraFeaturesLBL.textAlignment = .left
                    
                }
                else
                {
                    extraFeaturesLBL.textAlignment = .right
                }
                extraFeaturesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraFeatures", comment: "")
                ////
                
                let extraFeaturesListView = UIView(frame: CGRect(x: 0, y: (40/568)*self.FullHeight, width: self.FullWidth, height: extraFeaturesViewHeight - (40/568)*self.FullHeight))
                
                var tempTwoOrigin = (0/568)*self.FullHeight
                
                for i in 0..<(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray).count
                {
                    
                    let NameAndPriceView = UIView(frame: CGRect(x: 0, y: tempTwoOrigin, width: self.FullWidth, height: (40/568)*self.FullHeight))
                    
                    let NameAndQuantity = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    let PriceAll = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    let PriceSingle = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    // Setting Font
                    
                    NameAndQuantity.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
                    PriceAll.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    PriceSingle.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    
                    NameAndPriceView.addSubview(NameAndQuantity)
                    NameAndPriceView.addSubview(PriceAll)
                    NameAndPriceView.addSubview(PriceSingle)
                    
                    extraFeaturesLBL.textColor = UIColor.green
                    extraFeaturesView.addSubview(extraFeaturesLBL)
                    extraFeaturesListView.addSubview(NameAndPriceView)
                    extraFeaturesView.addSubview(extraFeaturesListView)
                    
                    
                    let tempDictionary = (((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray)[i] as! NSDictionary
                    
                    NameAndQuantity.text = "\(tempDictionary["feature_name"]!)" + " (Qty " + "\(tempDictionary["feature_quantity"]!)" + ")"
                    
                    PriceAll.text = "\(tempDictionary["price"]!)"
                    PriceAll.textAlignment = .right
                    
                    PriceSingle.text = "\(tempDictionary["feature_price"]!)"
                    
                    tempTwoOrigin = NameAndPriceView.frame.origin.y + NameAndPriceView.frame.size.height
                    
                    
                    // Arabic and English UI
                    if (langID?.elementsEqual("EN"))!
                    {
                        NameAndQuantity.frame = CGRect(x: (20/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        NameAndQuantity.textAlignment = .left
                        
                        PriceAll.frame = CGRect(x: (210/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceAll.textAlignment = .right
                        
                        PriceSingle.frame = CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (280/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceSingle.textAlignment = .left
                    }
                    else
                    {
                        NameAndQuantity.frame = CGRect(x: (100/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        NameAndQuantity.textAlignment = .right
                        
                        PriceAll.frame = CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceAll.textAlignment = .left
                        
                        PriceSingle.frame = CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (280/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceSingle.textAlignment = .right
                    }
                    ////////
                    
                    
                }
                
                
            }
            
            //Delivery and Total Price View
            let deliveryAndTotalPriceView = UIView(frame: CGRect(x: 0, y: extraFeaturesView.frame.origin.y + extraFeaturesView.frame.size.height + (10/568)*self.FullHeight, width: self.FullWidth, height: (60/568)*self.FullHeight))
            
            let deliveryChargeLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let deliveryCharge = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let totalPriceLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let totalPrice = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            deliveryChargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "delivery_charge", comment: "")
            deliveryCharge.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["delivery_charge"]!)"
            totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
            totalPrice.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["total_price"]!)"
            
            deliveryCharge.textAlignment = .right
            totalPrice.textAlignment = .right
            
            let separatorView = UIView(frame: CGRect(x: (10/320)*self.FullWidth, y: deliveryAndTotalPriceView.frame.size.height - (1/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (1/568)*self.FullHeight))
            
            separatorView.backgroundColor = UIColor.lightGray
            
            // Setting Font
            
            deliveryChargeLBL.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            deliveryCharge.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            totalPriceLBL.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            totalPrice.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            deliveryAndTotalPriceView.addSubview(deliveryChargeLBL)
            deliveryAndTotalPriceView.addSubview(deliveryCharge)
            deliveryAndTotalPriceView.addSubview(totalPriceLBL)
            deliveryAndTotalPriceView.addSubview(totalPrice)
            deliveryAndTotalPriceView.addSubview(separatorView)
            
            //Food Information View
            foodInformationView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: self.FullWidth, height: deliveryAndTotalPriceView.frame.origin.y + deliveryAndTotalPriceView.frame.size.height))
            
            foodInformationView.addSubview(foodNameAndPriceView)
            foodInformationView.addSubview(extraFeaturesView)
            foodInformationView.addSubview(deliveryAndTotalPriceView)
            
            //mainScroll.bringSubview(toFront: foodInformationView)
            
            tempOrigin = foodInformationView.frame.origin.y + foodInformationView.frame.size.height
            
            //self.mainScroll.addSubview(foodInformationView)
            self.foodDetailsView.autoresizesSubviews = false
            
            self.foodDetailsView.addSubview(foodInformationView)
            
            self.foodDetailsViewHeightConstraint.constant = tempOrigin
            self.foodDetailsView.autoresizesSubviews = true
            
            //            self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.tempOrigin)
            
            if (langID?.elementsEqual("EN"))!
            {
                foodNameAndQuantity.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodNameAndQuantity.textAlignment = .left
                
                foodPriceAll.frame = CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodPriceAll.textAlignment = .right
                
                foodPriceSingle.frame = CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                foodPriceSingle.textAlignment = .left
                
                
                
                deliveryChargeLBL.frame = CGRect(x: (20/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryChargeLBL.textAlignment = .left
                deliveryChargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "delivery_charge", comment: "")
                
                deliveryCharge.frame = CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryCharge.textAlignment = .right
                
                totalPriceLBL.frame = CGRect(x: (20/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPriceLBL.textAlignment = .left
                totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
                
                totalPrice.frame = CGRect(x: (210/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPrice.textAlignment = .right
            }
            else
            {
                foodNameAndQuantity.frame = CGRect(x: (110/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodNameAndQuantity.textAlignment = .right
                
                foodPriceAll.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodPriceAll.textAlignment = .left
                
                foodPriceSingle.frame = CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                foodPriceSingle.textAlignment = .right
                
                deliveryChargeLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryChargeLBL.textAlignment = .right
                
                deliveryCharge.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryCharge.textAlignment = .left
                
                totalPriceLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPriceLBL.textAlignment = .right
                
                totalPrice.frame = CGRect(x: (10/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPrice.textAlignment = .left
            }
            
            
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Fill Data from WEB
    
    func fillData()
    {
        invoiceNo.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["invoice_no"]!)"
        foodDate.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["date"]!)"
        shippingName.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["name"]!)"
        shippingEmail.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["email"]!)"
        shippingAddress.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["shipping_addrs"]!)"
        shippingContactNo.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["contact_no"]!)"
        nearby.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["near_by"]!)"
        shippingType.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["shipping_type"]!)"
        paymentType.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["payment_type"]!)"
    }
    
    
    
    
    // MARK: - // Set Font
    
    func set_font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        invoiceNoLBL.font = UIFont(name: invoiceNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        invoiceNo.font = UIFont(name: invoiceNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dateLBL.font = UIFont(name: dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        foodDate.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingDetailLBL.font = UIFont(name: shippingDetailLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingName.font = UIFont(name: shippingName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingEmail.font = UIFont(name: shippingEmail.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingAddress.font = UIFont(name: shippingAddress.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingContactNo.font = UIFont(name: shippingContactNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearbyLBL.font = UIFont(name: nearbyLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearby.font = UIFont(name: nearby.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingTypeLBL.font = UIFont(name: shippingTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingType.font = UIFont(name: shippingType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentTypeLBL.font = UIFont(name: paymentTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentType.font = UIFont(name: paymentType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
    }
    
    
    
    
    
    
    // MARK:- // Set Language Strings
    
    func setLanguageStrings()
    {
        self.invoiceNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "invoice_no", comment: "")
//        self.extraFeaturesTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraFeatures", comment: "")
//        self.deliveryChargeTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "deliveryCharge", comment: "")
//        self.totalPriceTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
        self.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        self.shippingDetailLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingDetail", comment: "")
        self.nearbyLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
        self.shippingTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shipping_type", comment: "")
        self.paymentTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "payment_type", comment: "")
    }
    
    
    
    /*
    
    var insideTableviewDataArray : NSArray!
    
    var foodInformationView : UIView!
    
    var tempOrigin : CGFloat!
    
    var arrayIndex : Int!
    
    var tableviewArray = [UITableView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFoodCourtInformationDetails()
        
        dispatchGroup.notify(queue: .main) {
            
            self.set_font()
            
            self.fillData()
            
            self.createInformationList()
            
            //            self.dateView.frame.origin.y = self.foodInformationView.frame.origin.y + self.foodInformationView.frame.size.height + (10/568)*self.FullHeight
            //
            //            self.shippingDetailView.frame.origin.y = self.dateView.frame.origin.y + self.dateView.frame.size.height
            //
            //            self.nearbyShippingtypePaymenttypeView.frame.origin.y = self.shippingDetailView.frame.origin.y + self.shippingDetailView.frame.size.height
            //
            //            self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.nearbyShippingtypePaymenttypeView.frame.origin.y + self.nearbyShippingtypePaymenttypeView.frame.size.height)
            
            
        }
        
    }
    
    
    
    
    
    
    // MARK: - // JSON POST Method to get Food Court Detail Information Data
    
    func getFoodCourtInformationDetails()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_food_court_info_details")!   //change the url
        
        print("Food Court Information Details URL : --------",url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&order_id=\(orderID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Get Booking Information Detail Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {
                        self.foodCourtInformationDetailsDictionary = json["info_array"] as! NSDictionary
                        
                        
                        DispatchQueue.main.async {
                            
                            SVProgressHUD.dismiss()
                            
                            self.dispatchGroup.leave()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                        }
                    }
  
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK: - // Defined Functions
    
    // MARK:- // Function to create the total information list
    
    func createInformationList()
    {
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        //y origin to initiate the first view from
        tempOrigin = (35/568)*self.FullHeight
        
        //initiating loop to create views
        for i in 0..<(foodCourtInformationDetailsDictionary["product_details"] as! NSArray).count
        {
            arrayIndex = i
            
            
            // Food Name and Price View
            let foodNameAndPriceView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (60/568)*self.FullHeight))
            
            let foodNameAndQuantity = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight))
            
            let foodPriceAll = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight))
            
            let foodPriceSingle = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            foodNameAndQuantity.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["product_name"]!)" + " (Qty " + "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["quantity"]!)" + ")"
            
            foodPriceAll.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["sub_total"]!)"
            foodPriceAll.textAlignment = .right
            
            foodPriceSingle.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["original_price"]!)"
            
            
            // Setting Font
            
            foodNameAndQuantity.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            foodPriceAll.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            foodPriceSingle.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            
            
            foodNameAndPriceView.addSubview(foodNameAndQuantity)
            foodNameAndPriceView.addSubview(foodPriceAll)
            foodNameAndPriceView.addSubview(foodPriceSingle)
            
            
            
            // Extra Features View
            var extraFeaturesViewHeight = (0/568)*self.FullHeight
            
            let extraFeaturesView = UIView(frame: CGRect(x: 0, y: (60/568)*self.FullHeight, width: self.FullWidth, height: 0))
            
            if (((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray).count > 0
            {
                extraFeaturesViewHeight = (CGFloat((((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["feature_info"] as! NSArray).count) * (40/568)*self.FullHeight) + (40/568)*self.FullHeight
                
                extraFeaturesView.frame = CGRect(x: 0, y: (60/568)*self.FullHeight, width: self.FullWidth, height: extraFeaturesViewHeight)
                
                let extraFeaturesLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: self.FullWidth - (20/320)*self.FullWidth, height: (20/568)*self.FullHeight))
                
                extraFeaturesLBL.text = "Extra Features"
                
                
                // Arabic and English UI
                if (langID?.elementsEqual("EN"))!
                {
                    extraFeaturesLBL.textAlignment = .left
                    
                }
                else
                {
                    extraFeaturesLBL.textAlignment = .right
                }
                extraFeaturesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraFeatures", comment: "")
                ////
                
                let extraFeaturesListView = UIView(frame: CGRect(x: 0, y: (40/568)*self.FullHeight, width: self.FullWidth, height: extraFeaturesViewHeight - (40/568)*self.FullHeight))
                
                var tempTwoOrigin = (0/568)*self.FullHeight
                
                for i in 0..<(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray).count
                {
                    
                    let NameAndPriceView = UIView(frame: CGRect(x: 0, y: tempTwoOrigin, width: self.FullWidth, height: (40/568)*self.FullHeight))
                    
                    let NameAndQuantity = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    let PriceAll = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    let PriceSingle = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight))
                    
                    // Setting Font
                    
                    NameAndQuantity.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
                    PriceAll.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    PriceSingle.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                    
                    NameAndPriceView.addSubview(NameAndQuantity)
                    NameAndPriceView.addSubview(PriceAll)
                    NameAndPriceView.addSubview(PriceSingle)
                    
                    extraFeaturesLBL.textColor = UIColor.green
                    extraFeaturesView.addSubview(extraFeaturesLBL)
                    extraFeaturesListView.addSubview(NameAndPriceView)
                    extraFeaturesView.addSubview(extraFeaturesListView)
                    
                    
                    let tempDictionary = (((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[arrayIndex] as! NSDictionary)["feature_info"] as! NSArray)[i] as! NSDictionary
                    
                    NameAndQuantity.text = "\(tempDictionary["feature_name"]!)" + " (Qty " + "\(tempDictionary["feature_quantity"]!)" + ")"
                    
                    PriceAll.text = "\(tempDictionary["price"]!)"
                    PriceAll.textAlignment = .right
                    
                    PriceSingle.text = "\(tempDictionary["feature_price"]!)"
                    
                    tempTwoOrigin = NameAndPriceView.frame.origin.y + NameAndPriceView.frame.size.height
                    
                    
                    // Arabic and English UI
                    if (langID?.elementsEqual("EN"))!
                    {
                        NameAndQuantity.frame = CGRect(x: (20/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        NameAndQuantity.textAlignment = .left
                        
                        PriceAll.frame = CGRect(x: (210/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceAll.textAlignment = .right
                        
                        PriceSingle.frame = CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (280/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceSingle.textAlignment = .left
                    }
                    else
                    {
                        NameAndQuantity.frame = CGRect(x: (100/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        NameAndQuantity.textAlignment = .right
                        
                        PriceAll.frame = CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceAll.textAlignment = .left
                        
                        PriceSingle.frame = CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (280/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                        PriceSingle.textAlignment = .right
                    }
                    ////////
                    
                    
                }
                
                
            }
            
            //Delivery and Total Price View
            let deliveryAndTotalPriceView = UIView(frame: CGRect(x: 0, y: extraFeaturesView.frame.origin.y + extraFeaturesView.frame.size.height + (10/568)*self.FullHeight, width: self.FullWidth, height: (60/568)*self.FullHeight))
            
            let deliveryChargeLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let deliveryCharge = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let totalPriceLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            let totalPrice = UILabel(frame: CGRect(x: (210/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight))
            
            deliveryChargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "delivery_charge", comment: "")
            deliveryCharge.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["delivery_charge"]!)"
            totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
            totalPrice.text = "\(((foodCourtInformationDetailsDictionary["product_details"] as! NSArray)[i] as! NSDictionary)["total_price"]!)"
            
            deliveryCharge.textAlignment = .right
            totalPrice.textAlignment = .right
            
            let separatorView = UIView(frame: CGRect(x: (10/320)*self.FullWidth, y: deliveryAndTotalPriceView.frame.size.height - (1/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (1/568)*self.FullHeight))
            
            separatorView.backgroundColor = UIColor.lightGray
            
            // Setting Font
            
            deliveryChargeLBL.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            deliveryCharge.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            totalPriceLBL.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            totalPrice.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            deliveryAndTotalPriceView.addSubview(deliveryChargeLBL)
            deliveryAndTotalPriceView.addSubview(deliveryCharge)
            deliveryAndTotalPriceView.addSubview(totalPriceLBL)
            deliveryAndTotalPriceView.addSubview(totalPrice)
            deliveryAndTotalPriceView.addSubview(separatorView)
            
            //Food Information View
            foodInformationView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: self.FullWidth, height: deliveryAndTotalPriceView.frame.origin.y + deliveryAndTotalPriceView.frame.size.height))
            
            foodInformationView.addSubview(foodNameAndPriceView)
            foodInformationView.addSubview(extraFeaturesView)
            foodInformationView.addSubview(deliveryAndTotalPriceView)
            
            mainScroll.bringSubview(toFront: foodInformationView)
            
            tempOrigin = foodInformationView.frame.origin.y + foodInformationView.frame.size.height
            
            self.mainScroll.addSubview(foodInformationView)
            
            //            self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.tempOrigin)
            
            if (langID?.elementsEqual("EN"))!
            {
                foodNameAndQuantity.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodNameAndQuantity.textAlignment = .left
                
                foodPriceAll.frame = CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodPriceAll.textAlignment = .right
                
                foodPriceSingle.frame = CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                foodPriceSingle.textAlignment = .left
                
                
                
                deliveryChargeLBL.frame = CGRect(x: (20/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryChargeLBL.textAlignment = .left
                deliveryChargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "delivery_charge", comment: "")
                
                deliveryCharge.frame = CGRect(x: (210/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryCharge.textAlignment = .right
                
                totalPriceLBL.frame = CGRect(x: (20/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPriceLBL.textAlignment = .left
                totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "total_price", comment: "")
                
                totalPrice.frame = CGRect(x: (210/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPrice.textAlignment = .right
            }
            else
            {
                foodNameAndQuantity.frame = CGRect(x: (110/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodNameAndQuantity.textAlignment = .right
                
                foodPriceAll.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                foodPriceAll.textAlignment = .left
                
                foodPriceSingle.frame = CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                foodPriceSingle.textAlignment = .right
                
                deliveryChargeLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryChargeLBL.textAlignment = .right
                
                deliveryCharge.frame = CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                deliveryCharge.textAlignment = .left
                
                totalPriceLBL.frame = CGRect(x: (100/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPriceLBL.textAlignment = .right
                
                totalPrice.frame = CGRect(x: (10/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (15/568)*self.FullHeight)
                totalPrice.textAlignment = .left
            }
            
            
            
        }
        
    }
    
    // MARK:- // Fill Data from WEB
    
    func fillData()
    {
        invoiceNo.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["invoice_no"]!)"
        foodDate.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["date"]!)"
        shippingName.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["name"]!)"
        shippingEmail.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["email"]!)"
        shippingAddress.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["shipping_addrs"]!)"
        shippingContactNo.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["contact_no"]!)"
        nearby.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["near_by"]!)"
        shippingType.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["shipping_type"]!)"
        paymentType.text = "\((foodCourtInformationDetailsDictionary["user_details"] as! NSDictionary)["payment_type"]!)"
    }
    
    
    
    
    
    // MARK: - // Set Font
    
    func set_font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        invoiceNoLBL.font = UIFont(name: invoiceNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        invoiceNo.font = UIFont(name: invoiceNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dateLBL.font = UIFont(name: dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        foodDate.font = UIFont(name: foodDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingDetailLBL.font = UIFont(name: shippingDetailLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingName.font = UIFont(name: shippingName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingEmail.font = UIFont(name: shippingEmail.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingAddress.font = UIFont(name: shippingAddress.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingContactNo.font = UIFont(name: shippingContactNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearbyLBL.font = UIFont(name: nearbyLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearby.font = UIFont(name: nearby.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingTypeLBL.font = UIFont(name: shippingTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingType.font = UIFont(name: shippingType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentTypeLBL.font = UIFont(name: paymentTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentType.font = UIFont(name: paymentType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        invoiceNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "invoice_no", comment: "")
        dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        shippingDetailLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingDetail", comment: "")
        nearbyLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
        shippingTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shipping_type", comment: "")
        paymentTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "payment_type", comment: "")
        
        
    }
 
 */
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
    
}
//MARK:- //Food Details Tableviewcell
class FoodDetailsTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var nameAndQuantity: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var individualPrice: UILabel!





    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
//MARK:- //Extra Features Tableviewcell
class ExtraFeatureTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!

    @IBOutlet weak var nameAndQuantity: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var individualPrice: UILabel!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
