//
//  OrderDetailViewController.swift
//  Kaafoo
//
//  Created by admin on 11/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderDetailViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var ConTentView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var invoiceNoLBL: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    
    @IBOutlet weak var briefOrderDetailTableView: UITableView!
    
    @IBOutlet weak var orderPriceDetailsView: UIView!
    @IBOutlet weak var noOfItemsLBL: UILabel!
    @IBOutlet weak var noOfItems: UILabel!
    @IBOutlet weak var subTotalLBL: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var deliveryChargeLBL: UILabel!
    @IBOutlet weak var deliveryCharge: UILabel!
    @IBOutlet weak var totalAmountLBL: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    
    @IBOutlet weak var orderDateView: UIView!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    
    
    @IBOutlet weak var shippingDetailView: UIView!
    @IBOutlet weak var shippingDetailTitleLBL: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerEmail: UILabel!
    @IBOutlet weak var customerAddress: UILabel!
    @IBOutlet weak var customerPhoneNo: UILabel!
    
   
    
    @IBOutlet weak var nearbyShippingTypePaymentTypeView: UIView!
    
    
    @IBOutlet weak var nearbyView: UIView!
    @IBOutlet weak var nearbyLBL: UILabel!
    @IBOutlet weak var nearby: UILabel!
    
    
    @IBOutlet weak var shippingTypeTitleView: UIView!
    @IBOutlet weak var shippingTypeLBL: UILabel!
    
    @IBOutlet weak var shippingTypeView: UIView!
    @IBOutlet weak var shippingType: UILabel!
    
    @IBOutlet weak var paymentTypeTitleView: UIView!
    @IBOutlet weak var paymentTypeLBL: UILabel!
    
    @IBOutlet weak var paymentTypeView: UIView!
    @IBOutlet weak var paymentType: UILabel!
    
    
    var orderDetailDictionary : NSDictionary!
    var orderID : String!
    
    let dispatchGroup = DispatchGroup()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        set_font()
        
        getOrderDetail()
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.globalDispatchgroup.notify(queue: .main) {
            
            self.putData()
            
            self.setLocalizedString()
            
//            self.mainScroll.layoutIfNeeded()
//            self.mainScroll.contentSize = CGSize(width: self.ConTentView.frame.size.width, height: self.ConTentView.frame.size.height)
            
//            self.setFrames()
            
        }
        
        

    }
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (orderDetailDictionary["order_product"] as! NSArray).count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BriefOrderDetailTVCTableViewCell
        
        cell.orderNameAndQuantity.text = "\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_name"]!)" + " (Qty: " + "\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_quantity"]!)" + ")"
        
        cell.priceLBL.text = "\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + String(Float("\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["price"]!)")! * Float("\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_quantity"]!)")!)
        
        
        
        cell.itemPrice.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "item_price", comment: "") + " " + "\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\(((orderDetailDictionary["order_product"] as! NSArray)[indexPath.row] as! NSDictionary)["price"]!)"
        
        
        ///// Set Font
        
        cell.orderNameAndQuantity.font = UIFont(name: cell.orderNameAndQuantity.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.priceLBL.font = UIFont(name: cell.priceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.itemPrice.font = UIFont(name: cell.itemPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        
        return cell
        
    }
    //MARK:- //Get listing using Global API
    func getOrderDetail()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&order_id=\(orderID!)"
        self.CallAPI(urlString: "app_order_details", param: parameters, completion: {
            self.orderDetailDictionary = self.globalJson["info_array"] as? NSDictionary


            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.briefOrderDetailTableView.delegate = self
                self.briefOrderDetailTableView.dataSource = self
                self.briefOrderDetailTableView.reloadData()
                self.briefOrderDetailTableView.rowHeight = UITableView.automaticDimension
                self.briefOrderDetailTableView.estimatedRowHeight = UITableView.automaticDimension

                SVProgressHUD.dismiss()

            }
        })
    }
    
    
    
    
    // MARK:- // Putting All Data in Desired Places after fetching from WEB
    
    func putData()
    {
        
        invoiceNo.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["order_no"]!)"
        noOfItems.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["no_if_items"]!)"
        subTotal.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["currency_symbol"]!)" + "\((orderDetailDictionary["order_info"] as! NSDictionary)["sub_total"]!)"
        deliveryCharge.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["currency_symbol"]!)" + "\((orderDetailDictionary["order_info"] as! NSDictionary)["delivery_charge"]!)"
        totalAmount.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["currency_symbol"]!)" + "\((orderDetailDictionary["order_info"] as! NSDictionary)["total_amount"]!)"
        orderDate.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["order_date"]!)"
        customerName.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["name"]!)"
        customerEmail.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["email_id"]!)"
        customerAddress.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["shipping_address"]!)"
        customerPhoneNo.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["contact_no"]!)"
        nearby.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["near_by"]!)"
        shippingType.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["shipping_type"]!)"
        paymentType.text = "\((orderDetailDictionary["order_info"] as! NSDictionary)["pay_type"]!)"
        
    }
    
    
    // MARK:- // Setting frame of Views According to Data
    
    func setFrames()
    {
        
        self.briefOrderDetailTableView.frame.size.height = (85/568)*self.FullHeight * CGFloat((orderDetailDictionary["order_product"] as! NSArray).count)
        
        self.orderPriceDetailsView.frame.origin.y = self.briefOrderDetailTableView.frame.origin.y + self.briefOrderDetailTableView.frame.size.height
        
        self.orderDateView.frame.origin.y = self.orderPriceDetailsView.frame.origin.y + self.orderPriceDetailsView.frame.size.height

        self.shippingDetailView.frame.origin.y = self.orderDateView.frame.origin.y + self.orderDateView.frame.size.height
        
        //dynamic address and its height
        let addressString = "\((orderDetailDictionary["order_info"] as! NSDictionary)["shipping_address"]!)"
        let addressHeight = addressString.heightWithConstrainedWidth(width: (300/320)*self.FullWidth, font: UIFont(name: customerAddress.font.fontName, size: CGFloat(Get_fontSize(size: 14)))!)
        customerAddress.frame.size.height = addressHeight
        customerPhoneNo.frame.origin.y = customerAddress.frame.origin.y + addressHeight + (5/568)*self.FullHeight
        
        self.shippingDetailView.autoresizesSubviews = false
        self.shippingDetailView.frame.size.height = self.customerPhoneNo.frame.origin.y + customerPhoneNo.frame.size.height + (15/568)*self.FullHeight
        self.shippingDetailView.autoresizesSubviews = true
        
        
        
        self.nearbyShippingTypePaymentTypeView.frame.origin.y = self.shippingDetailView.frame.origin.y + self.shippingDetailView.frame.size.height
        
        self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.nearbyShippingTypePaymentTypeView.frame.origin.y + self.nearbyShippingTypePaymentTypeView.frame.size.height)
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        invoiceNoLBL.font = UIFont(name: invoiceNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        invoiceNo.font = UIFont(name: invoiceNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        noOfItemsLBL.font = UIFont(name: noOfItemsLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        noOfItems.font = UIFont(name: noOfItems.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        subTotalLBL.font = UIFont(name: subTotalLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        subTotal.font = UIFont(name: subTotal.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        deliveryChargeLBL.font = UIFont(name: deliveryChargeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        deliveryCharge.font = UIFont(name: deliveryCharge.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        totalAmountLBL.font = UIFont(name: totalAmountLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        totalAmount.font = UIFont(name: totalAmount.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dateLBL.font = UIFont(name: dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        orderDate.font = UIFont(name: orderDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingDetailTitleLBL.font = UIFont(name: shippingDetailTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        customerName.font = UIFont(name: customerName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        customerEmail.font = UIFont(name: customerEmail.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        customerAddress.font = UIFont(name: customerAddress.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        customerPhoneNo.font = UIFont(name: customerPhoneNo.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearbyLBL.font = UIFont(name: nearbyLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        nearby.font = UIFont(name: nearby.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingTypeLBL.font = UIFont(name: shippingTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        shippingType.font = UIFont(name: shippingType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentTypeLBL.font = UIFont(name: paymentTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        paymentType.font = UIFont(name: paymentType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
    }
    
    
    
    
    // MARK:- // Set Localized String
    
    func setLocalizedString()
    {
        invoiceNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "invoice_no", comment: "")
        noOfItemsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_of_items", comment: "")
        subTotalLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "subTotal", comment: "")
        deliveryChargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "deliveryCharge", comment: "")
        totalAmountLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalAmount", comment: "")
        dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        shippingDetailTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingDetail", comment: "")
        nearbyLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
        shippingTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shipping_type", comment: "")
        paymentTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "payment_type", comment: "")
       
        
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }

}
//MARK:- Order Details TableviewCell
class BriefOrderDetailTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var briefOrderDetailView: UIView!
    @IBOutlet weak var orderNameAndQuantity: UILabel!
    @IBOutlet weak var priceLBL: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var separatorView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

