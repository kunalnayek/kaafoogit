//
//  ReportViewController.swift
//  Kaafoo
//
//  Created by priya on 05/06/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ReportViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
   
    

    
     let dispatchGroup = DispatchGroup()
     var allDataDictionary : NSDictionary!
     var otherInfoArray : NSArray!
    var  categoryID : NSString = ""
    
    @IBOutlet weak var viewtableheightconstraints: NSLayoutConstraint!
    @IBOutlet weak var viewtableofbuttomconstrint: NSLayoutConstraint!
    
    @IBOutlet weak var viewtable: UIView!
    @IBOutlet weak var tableviewheightcontraints: NSLayoutConstraint!
    @IBOutlet weak var selectcategorytitleLBL: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var selectcategoryLBL: UILabel!
    
    @IBOutlet weak var textLBL: UILabel!
    @IBOutlet weak var textTXT: UITextField!
    
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var commentTXT: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
       
    }
    

    @IBAction func selectcategoryBTNClicked(_ sender: Any) {
        
        
       // viewtable.isHidden = false
        
        self.getdata()
        
        if self.viewtable.isHidden == true
        {
            self.viewtable.isHidden = false
            self.viewtableheightconstraints.constant = 300
            self.viewtableofbuttomconstrint.constant = 20
            }
            else
            {
                self.viewtable.isHidden = true
                self.viewtableheightconstraints.constant = 0
                self.viewtableofbuttomconstrint.constant = 50
            }
    }
    
    @IBAction func submitBTNClicked(_ sender: Any)
    {
        self.reportsubmitAPI()
    }
    
    
    //Tableview delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return otherInfoArray.count
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
       
       {
          
               let cell = tableView.dequeueReusableCell(withIdentifier: "reporttablecell") as! ReportTableViewCell
                
                      
                cell.textLabel?.text =  "\((self.otherInfoArray[indexPath.row] as! NSDictionary)["cat_name"] ?? "")"
                
                print("celltext-----------",cell.textLabel?.text)
        
        categoryID =  "\((self.otherInfoArray[indexPath.row] as! NSDictionary)["cat_id"] ?? "")" as NSString
                       
                       print("categoryID-----------",categoryID)
        
                
     
                      
                return cell
               
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
         self.selectcategoryLBL.text = "\((self.otherInfoArray[indexPath.row] as! NSDictionary)["cat_name"] ?? "")"
        
        self.viewtable.isHidden = true
        self.viewtableheightconstraints.constant = 0
        self.viewtableofbuttomconstrint.constant = 50
        
        
    }
    
    
    func getdata()
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_report_category_dropdown")!       //change the url
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        
        parameters = "lang_id=\(langID!)"
        
        
        print("Get Data URL : ", url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
                
                return
            }
            
            guard let data = data else {
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Category List Response: " , json)
                    
                    self.allDataDictionary = json.copy() as? NSDictionary
                    
                    self.otherInfoArray = self.allDataDictionary["info_array"] as? NSArray
                    
                    print("otherinfoarray------",self.otherInfoArray)
                    
                   
                    
                   
                    
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                       
                            self.tableview.delegate = self
                            self.tableview.dataSource = self
                            self.tableview.reloadData()
                       
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
            }
        })
        
        task.resume()
        
        
        
    }
    
    func reportsubmitAPI()
    {
        var parameters : String = ""
               
               let langID = UserDefaults.standard.string(forKey: "langID")
               let CountryId = UserDefaults.standard.string(forKey: "countryID")
                let userID = UserDefaults.standard.string(forKey: "userID")
        
             
      
               parameters = "lang_id=\(langID ?? "")&user_id=\(userID ?? "")&text_no=\(textTXT.text!)&comments=\(commentTXT.text!)&mycountry_id=\(CountryId!)&category_id=\(categoryID)"
               
               self.CallAPI(urlString: "app_report_info", param: parameters) {
                   
                   
                   
                   DispatchQueue.main.async {
                       
                       self.globalDispatchgroup.leave()
                       SVProgressHUD.dismiss()
                       let responseMessage = "\(self.globalJson["message"]!)"
                       self.ShowAlertMessage(title: "warning", message: responseMessage)
    }
                
    }
    
}
}
