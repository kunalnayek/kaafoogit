//
//  RoomListingViewController.swift
//  Kaafoo
//
//  Created by priya on 24/04/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//



import UIKit
import SVProgressHUD
import SDWebImage

class RoomListingViewController: GlobalViewController,UITableViewDataSource,UITableViewDelegate {

    
    
    @IBOutlet weak var roomListTableView: UITableView!
    
    var RoomListingArray : NSMutableArray! = NSMutableArray()
    var startValue = 0
    var perLoad = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       roomlistingAPI()
        
//        self.roomListTableView.delegate = self
//
//        self.roomListTableView.dataSource = self

       
    }
    
      // MARK: - // JSON POST Method to get RoomList
    
    func roomlistingAPI()
    
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        //https://kaafoo.com/appcontrol/app_room_info_listing?lang_id=en&user_id=19&hotel_id=2023&sort_search=&per_load=10&start_value=0
        
         //https://kaafoo.com/appcontrol/app_add_edit_room_info?user_id=19&lang_id=en&hotel_id=2023
        
        
        //https://kaafoo.com/appcontrol/app_room_info_listing?lang_id=en&user_id=19&hotel_id=2023&sort_search=&per_load=10&start_value=0
        
        let parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&sort_search=&hotel_id=2023"
        
        self.CallAPI(urlString: "app_room_info_listing", param: parameters, completion: {
            
            print("gobaljson-------",self.globalJson)
            
            self.RoomListingArray = self.globalJson["info_array"] as? NSMutableArray

           print("RoomListingArray------",   self.RoomListingArray)


            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.roomListTableView.delegate = self
                
                self.roomListTableView.dataSource = self
                
                self.roomListTableView.reloadData()
                
                SVProgressHUD.dismiss()
            }

        })
    }
    
     // MARK:- // Tableview Delegates

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return 6
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
           // self.SelectedExtend = indexPath.row

            let cell = tableView.dequeueReusableCell(withIdentifier: "RoomListingTableViewCell") as! RoomListingTableViewCell

            //cell.productTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            
           
            //let path = "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"

            //cell.productImage.sd_setImage(with: URL(string: path))

           

            cell.selectionStyle = .none
            
          
            

            // Set Font //////

           // cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
           
           

           // cell.cellView.layer.cornerRadius = 8

            return cell

        }


      


        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {



//            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
//
//            let tempDictionary = self.recordsArray[indexPath.row] as! NSDictionary
//
//            nav.ProductID = "\(tempDictionary["product_id"]!)"
//
//            self.navigationController?.pushViewController(nav, animated: true)
        }

        
    

   
}

//MARK:- //RoomListing tableviewcell
class RoomListingTableViewCell: UITableViewCell
{

}
