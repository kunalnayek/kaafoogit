//
//  ItemsIAmSellingViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 31/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ItemsIAmSellingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {

    // Global Font Applied
    var SelectedPickerInt : Int! = 0
    
    var PickerdataArray : NSMutableArray! = NSMutableArray()
    
    var PID : String! = ""
    
    var SelectedExtend : Int! = 0

    var startValue = 0
    
    var perLoad = 10

    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!

    var nextStart : String!

    var itemsIAmSellingArray : NSMutableArray! = NSMutableArray()

    var recordsArray : NSMutableArray! = NSMutableArray()

    var productID : String!

    var stepFinal : String!
    
    var CategoryId : String! = ""

    @IBOutlet weak var itemsIAmSellingTableView: UITableView!
    
    
    var ShowPicker : UIView = {
        
        let showPicker = UIView()
        
        showPicker.translatesAutoresizingMaskIntoConstraints = false
        
        showPicker.backgroundColor = UIColor(red: 65/255, green: 65/255, blue: 65/255, alpha: 0.85)
        
        return showPicker
    }()
    
    var hidePickerBtn : UIButton = {
        
        let hidepickerbtn = UIButton()
        
        hidepickerbtn.translatesAutoresizingMaskIntoConstraints = false
        
        hidepickerbtn.backgroundColor = .clear
        
        return hidepickerbtn
        
    }()
    
    var PickerView : UIView = {
        
        let pickerview = UIView()
        
        pickerview.translatesAutoresizingMaskIntoConstraints = false
        
        pickerview.backgroundColor = .white
        
        pickerview.isUserInteractionEnabled = true
        
        return pickerview
    }()
    
    var pickerBtn : UIButton = {
        
        let pickerbtn = UIButton()
        
        pickerbtn.translatesAutoresizingMaskIntoConstraints = false
        
        pickerbtn.isUserInteractionEnabled = true
        
        pickerbtn.layer.borderColor = UIColor.lightgraymore().cgColor
        
        pickerbtn.backgroundColor = .yellow2()
        
        pickerbtn.layer.cornerRadius = 5.0
        
        pickerbtn.clipsToBounds = true
        
        pickerbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_select_expiry_time", comment: ""), for: .normal)
        
        return pickerbtn
        
    }()
    
    var SaveBtn : UIButton = {
        
        let savebtn = UIButton()
        
        savebtn.translatesAutoresizingMaskIntoConstraints = false
        
        savebtn.isUserInteractionEnabled = true
        
        savebtn.layer.cornerRadius = 5.0
        
        savebtn.clipsToBounds = true
        
        savebtn.backgroundColor = .yellow2()
        
        savebtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
        
        return savebtn
        
    }()
    
    var ExpireTime : UILabel = {
        
        let expiretime = UILabel()
        
        expiretime.translatesAutoresizingMaskIntoConstraints = false
        
        expiretime.clipsToBounds = true
        
        expiretime.textColor = .black
        
        expiretime.layer.borderColor = UIColor.black.cgColor
        
        expiretime.text = "  " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_expiry_time", comment: "")
        
        expiretime.layer.borderWidth = 1.0
        
        return expiretime
        
    }()
    
    
    var DataPicker : UIPickerView = {
        
        let dataPicker = UIPickerView()
        
        dataPicker.translatesAutoresizingMaskIntoConstraints = false
        
        dataPicker.backgroundColor = .white
        
        return dataPicker
        
    }()
    
    var toolBar : UIToolbar = {
        
        let toolbar = UIToolbar()
        
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        
        return toolbar
        
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        set_font()
        
        self.setLayer()

        itemsIAmSelling()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
    }
    
    @IBAction func roominfoClicked(_ sender: Any)
    {
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "RoomListingViewController") as! RoomListingViewController
        
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    
    func setLayer()
    {
        self.view.addSubview(self.ShowPicker)
        
        self.ShowPicker.anchor(top: self.view.topAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.ShowPicker.addSubview(self.hidePickerBtn)
        
        self.hidePickerBtn.anchor(top: self.ShowPicker.topAnchor, leading: self.ShowPicker.leadingAnchor, bottom: self.ShowPicker.bottomAnchor, trailing: self.ShowPicker.trailingAnchor)
        
        self.hidePickerBtn.addTarget(self, action: #selector(self.showPickerHide(sender:)), for: .touchUpInside)
        
        self.ShowPicker.addSubview(self.PickerView)
        
        self.PickerView.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        self.PickerView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        self.PickerView.centerXAnchor.constraint(equalTo: self.ShowPicker.centerXAnchor).isActive = true
        
        self.PickerView.centerYAnchor.constraint(equalTo: self.ShowPicker.centerYAnchor).isActive = true
        
        self.PickerView.addSubview(self.pickerBtn)
        
        self.pickerBtn.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
        self.pickerBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.pickerBtn.centerXAnchor.constraint(equalTo: self.PickerView.centerXAnchor).isActive = true
        
        self.pickerBtn.topAnchor.constraint(equalTo: self.PickerView.topAnchor, constant: 40).isActive = true
        
        self.PickerView.addSubview(self.ExpireTime)
        
        self.ExpireTime.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
        self.ExpireTime.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.ExpireTime.centerXAnchor.constraint(equalTo: self.PickerView.centerXAnchor).isActive = true
        
        self.ExpireTime.topAnchor.constraint(equalTo: self.PickerView.topAnchor, constant: 110).isActive = true
        
        self.PickerView.addSubview(self.SaveBtn)
        
        self.SaveBtn.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
        self.SaveBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.SaveBtn.centerXAnchor.constraint(equalTo: self.PickerView.centerXAnchor).isActive = true
        
        self.SaveBtn.topAnchor.constraint(equalTo: self.PickerView.topAnchor, constant: 180).isActive = true
        
        self.SaveBtn.addTarget(self, action: #selector(self.SaveBtnTarget(sender:)), for: .touchUpInside)
        
        self.pickerBtn.addTarget(self, action: #selector(self.PickerBtnTarget(sender:)), for: .touchUpInside)
        
        self.view.addSubview(self.DataPicker)
        
        self.DataPicker.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.DataPicker.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        self.DataPicker.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.DataPicker.heightAnchor.constraint(equalToConstant: 240).isActive = true
        
        self.DataPicker.isHidden = true
        
        self.toolBar.isHidden = true
        
        self.CreateToolbar()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.itemsIAmSellingTableView.rowHeight = UITableView.automaticDimension
        
        self.itemsIAmSellingTableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.ShowPicker.isHidden = true
    }
    
    //MARK:- //Create Toolbar for Pickerview in Swift
    
       func CreateToolbar()
       {
           // ToolBar
           
           toolBar.barStyle = .default
           
           toolBar.isTranslucent = true
           
           toolBar.tintColor = .white
           
           toolBar.backgroundColor = .black
           
           toolBar.barTintColor = .black
           
           toolBar.sizeToFit()
           
           let doneButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneClick))
           
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           
           let cancelButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: ""), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelClick))
           
           toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
           
           toolBar.isUserInteractionEnabled = true
           
           self.view.addSubview(toolBar)
           
           toolBar.translatesAutoresizingMaskIntoConstraints = false
           
           toolBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
           
           toolBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
           
           toolBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -240).isActive = true
           
           toolBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
       }
    
    @objc func showPickerHide(sender : UIButton)
    {
        self.ShowPicker.isHidden = true
    }
    
    @objc func SaveBtnTarget(sender : UIButton)
    {

        print("SAVE")
    }
    
    @objc func PickerBtnTarget(sender : UIButton)
    {
        self.ExtendAPI()
    }
    
    func ExtendAPI()
    {
        let parameters = "user_id=\(self.USERID ?? "")&lang_id=\(self.langID ?? "")&product_id=\(self.PID ?? "")"
        
        self.CallAPI(urlString: "app_items_extending", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.PickerdataArray = (((self.globalJson["info_array"] as! NSArray)[0] as! NSDictionary)["option_content_list"] as! NSMutableArray)
            
            DispatchQueue.main.async {
                
                SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: "\(self.globalJson["message"] ?? "")")
                
                self.DataPicker.delegate = self
                
                self.DataPicker.dataSource = self
                
                self.DataPicker.reloadAllComponents()
            }
        })
        
        self.DataPicker.selectRow(0, inComponent: 0, animated: false)
        
        self.DataPicker.isHidden = false
        
        self.toolBar.isHidden = false
        
        self.view.bringSubviewToFront(self.DataPicker)
        
        self.view.bringSubviewToFront(self.toolBar)
    }
    
    func SaveAPI()
    {
        let parameters = "user_id=\(self.USERID ?? "")&lang_id=\(self.langID ?? "")&expiry_time=\(self.PickerdataArray[SelectedPickerInt])&product_id=\(self.PID ?? "")"
        
        self.CallAPI(urlString: "app_items_extending_date_save", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            DispatchQueue.main.async {
                
                SVProgressHUD.dismiss()
                
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: "\(self.globalJson["message"] ?? "")")
                
                self.ShowPicker.isHidden = true
                
                self.DataPicker.isHidden = true
                
                self.toolBar.isHidden = true
            }
        })
    }
    
    @objc func doneClick(sender : UIButton)
    {
        self.ExpireTime.text = "\(self.PickerdataArray[SelectedPickerInt])"
        
        self.DataPicker.isHidden = true
        
        self.toolBar.isHidden = true
        
        self.view.sendSubviewToBack(self.DataPicker)
        
        self.view.sendSubviewToBack(self.toolBar)
        
        self.SaveAPI()
    }
    
    @objc func cancelClick(sender : UIButton)
    {
        self.DataPicker.isHidden = true
        
        self.toolBar.isHidden = true
        
        self.view.sendSubviewToBack(self.DataPicker)
        
        self.view.sendSubviewToBack(self.toolBar)
    }


    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.SelectedExtend = indexPath.row

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ItemsIAmSellingTVCTableViewCell

        cell.productTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        cell.productType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)"
        
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"

        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"

        cell.productImage.sd_setImage(with: URL(string: path))

        cell.productExpiry.text = "\((recordsArray[indexPath.row] as! NSDictionary)["expire_time"]!)"
        
        cell.topCategory.text = "Top category: " + "\((recordsArray[indexPath.row] as! NSDictionary)["top_category_name"]!)"
        
        cell.productAddress.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"

        cell.bidCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bid_count"]!)" + " bid"
        
        let pathTwo = "\((recordsArray[indexPath.row] as! NSDictionary)["flag"]!)"

        cell.bidFlag.sd_setImage(with: URL(string: pathTwo))

        cell.selectionStyle = .none
        
        //cell.ExtendBtnOutlet.tag = indexPath.row

        cell.deleteOutlet.tag = indexPath.row
        
        cell.editOutlet.tag = indexPath.row
        
        //
        //        cell.deleteOutlet.addTarget(self, action: #selector(ItemsIAmSellingViewController.remove(sender:)), for: .touchUpInside)
        //Asking button
        //        if "\((recordsArray[indexPath.row] as! NSDictionary)["asking_btn"]!)".elementsEqual("")
        //        {
        //
        //        }
        //        else
        //        {
        //
        //        }
        //Extend Btn
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["extend_button"]!)".elementsEqual("")
        {
            cell.ExtendBtnView.isHidden = true
        }
        else
        {
            cell.ExtendBtnView.isHidden = false
        }
        //Delete Btn Checking
        if "\((recordsArray[indexPath.row] as! NSDictionary)["admin_delete_status"]!)".elementsEqual("0")
        {
            cell.editOutlet.isHidden = true
        }
        else
        {
            cell.editOutlet.isHidden = false
        }
        //Room info Btn
        if "\((recordsArray[indexPath.row] as! NSDictionary)["room_info_button"]!)".elementsEqual("0")
        {
            cell.RoomInfoBtnView.isHidden = true
        }
        else
        {
            cell.RoomInfoBtnView.isHidden = false
        }
        //Asking price btn
        if "\((recordsArray[indexPath.row] as! NSDictionary)["room_info_button"]!)".elementsEqual("room")
        {
            cell.RoomInfoBtnView.isHidden = false
        }
        else
        {
            cell.RoomInfoBtnView.isHidden = true
        }


        cell.deleteOutlet.addTarget(self, action: #selector(remove(sender:)), for: .touchUpInside)
        cell.editOutlet.addTarget(self, action: #selector(editProduct(sender:)), for: .touchUpInside)
        

        // Set Font //////

        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.productExpiry.font = UIFont(name: cell.productExpiry.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.productAddress.font = UIFont(name: cell.productAddress.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.topCategory.font = UIFont(name: cell.topCategory.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.productType.font = UIFont(name: cell.productType.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.specialPrice.font = UIFont(name: cell.specialPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.ExtendBtnOutlet.tag = indexPath.row
        
        cell.ExtendBtnOutlet.addTarget(self, action: #selector(self.ToggleShowPicker(sender:)), for: .touchUpInside)

        // cell.editLBL.font = UIFont(name: cell.editLBL.font.fontName, size: CGFloat(Get_fontSize(size: 10)))
        // cell.deleteLBL.font = UIFont(name: cell.deleteLBL.font.fontName, size: CGFloat(Get_fontSize(size: 10)))

        ///////////

        //        cell.cellView.layer.borderWidth = 2
        //        cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        //        cell.layer.borderWidth = 1.0
        //        cell.layer.borderColor = UIColor.lightGray.cgColor

        //castShadow(selectedView: cell.cellView)

        cell.cellView.layer.cornerRadius = 8

        return cell

    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let cell = cell as! ItemsIAmSellingTVCTableViewCell

        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"] ?? "")".elementsEqual("A") {
            
            cell.price.isHidden = false
            
            cell.specialPrice.isHidden = true
            
            cell.productType.isHidden = true
            
            cell.bidView.isHidden = false

            cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")"
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"] ?? "")".elementsEqual("B") {

            cell.price.isHidden = false
            
            cell.productType.isHidden = true
            
            cell.bidView.isHidden = true

            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer"] ?? "")".elementsEqual("0") {
                
                cell.specialPrice.isHidden = true

                cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")"

            }
            else {
                
                cell.specialPrice.isHidden = false

                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")")
                
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                cell.price.attributedText = attributeString

                cell.specialPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSDictionary)["reserve_price"] ?? "")"
            }

        }
        else {
            
            cell.price.isHidden = true
            
            cell.specialPrice.isHidden = true
            
            cell.bidView.isHidden = true
            
            cell.productType.isHidden = false
        }


        let langID = UserDefaults.standard.string(forKey: "langID")

        if (langID?.elementsEqual("AR"))! {
            
            cell.editOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        }
        else {
            
            cell.editOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        }
        
        

    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "productDetailsVC") as! ProductDetailsViewController
//
//        let tempDictionary = self.recordsArray[indexPath.row] as! NSDictionary
//
//        navigate.productID = "\(tempDictionary["product_id"]!)"
//
//        self.navigationController?.pushViewController(navigate, animated: true)


        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        
        let tempDictionary = self.recordsArray[indexPath.row] as! NSDictionary
        
        nav.ProductID = "\(tempDictionary["product_id"]!)"
        
        self.navigationController?.pushViewController(nav, animated: true)
    }

    @objc func ToggleShowPicker(sender : UIButton)
    {
        self.PID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")"
        
        if self.ShowPicker.isHidden
        {
            self.ShowPicker.isHidden = false
        }
        else
        {
            self.ShowPicker.isHidden = true
        }
    }

    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                itemsIAmSelling()
            }

        }
    }



    // MARK: - // JSON POST Method to get Items I am Selling Data
    func itemsIAmSelling()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)&categoryid=\(CategoryId!)"
        
        self.CallAPI(urlString: "app_items_selling", param: parameters, completion: {
            
            self.itemsIAmSellingArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.itemsIAmSellingArray.count - 1)

            {
                let tempDict = self.itemsIAmSellingArray[i] as! NSDictionary
                
                print("tempdict is : " , tempDict)

                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.itemsIAmSellingArray.count

            print("Next Start Value : " , self.startValue)


            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.itemsIAmSellingTableView.delegate = self
                
                self.itemsIAmSellingTableView.dataSource = self
                
                self.itemsIAmSellingTableView.reloadData()
                
                SVProgressHUD.dismiss()
            }

        })
    }
    // MARK: - // JSON POST Method to remove Data from WatchList
    func removeFromSellingList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)"
        
        self.CallAPI(urlString: "app_item_selllist_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.displayToastMessage("\(self.globalJson["message"]!)")
                
                SVProgressHUD.dismiss()
            }
        })
    }


    // MARK:- // Buttons

    // MARK:- // Remove from Watch List Button


    @objc func remove(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in

            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.productID = "\(tempDictionary["product_id"]!)"

            self.removeFromSellingList()

            self.recordsArray.removeObject(at: sender.tag)

            self.itemsIAmSellingTableView.reloadData()

        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )

    }



    // MARK:- // Edit product


    @objc func editProduct(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in

            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "NewPostAdViewController") as! NewPostAdViewController


            UserDefaults.standard.setValue("\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", forKey: "productID")

            UserDefaults.standard.setValue(true, forKey: "autoLoad")

            UserDefaults.standard.setValue("\((self.recordsArray[sender.tag] as! NSDictionary)["step_final"]!)", forKey: "stepFinal")

            navigate.topCatID = "\((self.recordsArray[sender.tag] as! NSDictionary)["top_cat"]!)"
            navigate.isNewAdd = false
            UserDefaults.standard.set(true, forKey: "isFillbasicInfo")

            self.navigationController?.pushViewController(navigate, animated: true)

        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )

    }



    // MARK:- // Defining a Function to check expiry time Empty or not

    func checkExpiryTime(dataArray: NSMutableArray,expiryLabel : UILabel,addressView: UIView,indexpath: Int)
    {
        let timeString = "\((dataArray[indexpath] as! NSDictionary)["expire_time"]!)"

        if timeString.isEmpty
        {
            expiryLabel.isHidden = true
            addressView.frame.origin.y = (50/568)*self.FullHeight
        }
        else
        {
            expiryLabel.isHidden = false
            
            expiryLabel.frame.origin.y = (50/568)*self.FullHeight
            
            addressView.frame.origin.y = (65/568)*self.FullHeight
        }
    }

    // MARK:- // SEt Font

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

    }

    // MARK:- // Cast Shadow on UIview

    func castShadow(selectedView: UIView)
    {

        selectedView.layer.shadowColor = UIColor.black.cgColor
        
        selectedView.layer.shadowOpacity = 1
        
        selectedView.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        selectedView.layer.shadowRadius = 20

        selectedView.layer.shadowPath = UIBezierPath(rect: selectedView.bounds).cgPath

        selectedView.layer.shouldRasterize = true

    }

}
//MARK:- //Items I Am Selling tableviewcell
class ItemsIAmSellingTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productTitle: UILabel!
    
    @IBOutlet weak var productExpiry: UILabel!
    
    @IBOutlet weak var productAddressView: UIView!
    
    @IBOutlet weak var productAddress: UILabel!
    
    @IBOutlet weak var topCategory: UILabel!
    
    @IBOutlet weak var price: UILabel!

    @IBOutlet weak var productType: UILabel!

    @IBOutlet weak var bidView: UIView!
    
    @IBOutlet weak var bidCount: UILabel!
    
    @IBOutlet weak var bidFlag: UIImageView!

    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var editLBL: UILabel!
    
    @IBOutlet weak var editOutlet: UIButton!
    
    @IBOutlet weak var deleteLBL: UILabel!

    @IBOutlet weak var ExtendBtnView: UIView!
    
    @IBOutlet weak var RoomInfoBtnView: UIView!
    
    @IBOutlet weak var EditDeleteBtnView: UIView!

    @IBOutlet weak var deleteOutlet: UIButton!

    @IBOutlet weak var ExtendBtnOutlet: UIButton!
    
    @IBOutlet weak var priceProductTypeStackview: UIStackView!
    
    @IBOutlet weak var specialPriceProductTypeBidStackview: UIStackView!
    
    @IBOutlet weak var specialPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ItemsIAmSellingViewController : UIPickerViewDataSource,UIPickerViewDelegate

{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.PickerdataArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(self.PickerdataArray[row])"
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.SelectedPickerInt = row
        
        print("DidSelect")
    }
}

