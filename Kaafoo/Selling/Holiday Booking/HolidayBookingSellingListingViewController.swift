//
//  HolidayBookingSellingListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 08/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class HolidayBookingSellingListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var HolidayBookingSellerListingTableView: UITableView!

    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var BookingId : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var HolidayListingArray : NSMutableArray!
    
    var type = "S"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetHolidaySellerListing()
        self.HolidayBookingSellerListingTableView.separatorStyle = .none
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
               self.HolidayBookingSellerListingTableView.isHidden = true
               self.noDataFound(mainview: self.view)
            }
            else
            {
              print("Do nothing")
            }
        })
        self.SetFont()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.HolidayBookingSellerListingTableView.rowHeight = UITableView.automaticDimension
        self.HolidayBookingSellerListingTableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "bookinglisting") as! HolidayBookingSellerListingDetailsTableViewCell
        obj.BookingNo.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["booking_no"]!)"
        obj.BookingStatus.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)"
        obj.ProductName.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"]!)"
        obj.CheckInDate.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["pickup_date"]!)"
        obj.CheckoutDate.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["dropoff_date"]!)"
        obj.TotalPrice.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["currency"]!)" + "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["total_price"]!)"

        obj.BookingNoLbl.textColor = .yellow2()
        obj.BookingNoLbl.text = "Booking no"
        
        //Button Tags
        obj.DeleteBtn.tag = indexPath.row
        obj.CancelBtn.tag = indexPath.row
        obj.ConfirmBtn.tag = indexPath.row
        
        //Add Target Methods
        obj.DeleteBtn.addTarget(self, action: #selector(deleteBookingTap(sender:)), for: .touchUpInside)
        obj.CancelBtn.addTarget(self, action: #selector(CancelBookingTap(sender:)), for: .touchUpInside)
        obj.ConfirmBtn.addTarget(self, action: #selector(ConfirmBookingTap(sender:)), for: .touchUpInside)
        
        
        //Buttons Checking based On API Fire
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["cancel_btn"]!)".elementsEqual("")
            
        {
            
            obj.CancelView.isHidden = true
            
        }
            
        else
            
        {
            
            obj.CancelView.isHidden = false
            
        }
        
        
        
        //Delete Button Checking
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["delete_button"]!)".elementsEqual("")
            
        {
            
            obj.DeleteView.isHidden = true
            
        }
            
        else
            
        {
            
            obj.DeleteView.isHidden = false
            
        }
        
        //Review Button Checking
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["confirm_button"]!)".elementsEqual("")
            
        {
            
            obj.ConfirmView.isHidden = true
            
        }
            
        else
            
        {
            
            obj.ConfirmView.isHidden = false

            
        }
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["delete_button"]!)".elementsEqual("") &&  "\((self.recordsArray[indexPath.row] as! NSDictionary)["cancel_btn"]!)".elementsEqual("") &&  "\((self.recordsArray[indexPath.row] as! NSDictionary)["confirm_button"]!)".elementsEqual("")
        {
           obj.DeleteView.isHidden = true
            obj.CancelView.isHidden = true
            obj.ConfirmView.isHidden = true
        }
        else
        {
           //Do Nothing
        }
        
        
        //SET Font For Tableview Cell labels
        
        obj.BookingNoLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BookingNo.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BookingStatus.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BookingstatusLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.ProductName.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.ProductnameLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CheckInDate.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CheckoutDate.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.TotalPrice.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CheckindateLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CheckoutLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.TotalPriceLbl.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.DeleteBtn.titleLabel?.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.CancelBtn.titleLabel?.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.ConfirmBtn.titleLabel?.font = UIFont(name: obj.BookingNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


        //Mark:- String Localization

        obj.BookingNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        obj.BookingstatusLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        obj.ProductnameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "productName", comment: "")
        obj.CheckindateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "checkinDate", comment: "")
        obj.TotalPriceLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalPrice", comment: "")
        obj.CheckoutLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "checkoutDate", comment: "")

        
        
        obj.selectionStyle = .none
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- API Date Fetch
    func GetHolidaySellerListing()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_holiday_booking_info_list", param: parameters, completion: {
            self.HolidayListingArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.HolidayListingArray.count-1)
            {
                let tempDict = self.HolidayListingArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.recordsArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.HolidayBookingSellerListingTableView.delegate = self
                self.HolidayBookingSellerListingTableView.dataSource = self
                self.HolidayBookingSellerListingTableView.reloadData()
                SVProgressHUD.dismiss()
            }

        })
    }

    //MARK:_ Scrollview Delegate Methods
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
            
        {
            
            
            
        }
            
        else
            
        {
            
            
            
//            print("next start : ",nextStart )
            
            
            
            if (nextStart).isEmpty
                
            {
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                }
                
            }
                
            else
                
            {
                
                GetHolidaySellerListing()
                
            }
            
            
            
        }
        
    }
    
    //MARK:- API Fire For Delete Button
    func DeleteBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&delete_id=\(BookingId!)&lang_id=\(langID!)&section_type=\(type)"
        self.CallAPI(urlString: "app_holiday_bookinglist_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }
    
    
    
    
    
    
    
    // MARK:- // Delete Booking
    
    
    
    
    
    @objc func deleteBookingTap(sender: UIButton)
        
    {
        
        
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            
            UIAlertAction in
            
            
            
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            
            
            self.BookingId = "\(tempDictionary["id"]!)"
            
            
            
            self.DeleteBooking()
            
            
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                
                
                self.startValue = 0
                
                
                
                self.recordsArray.removeAllObjects()
                
                
                
                self.GetHolidaySellerListing()
                
                
                
            })
            
            
            
        }
        
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        alert.addAction(ok)
        
        alert.addAction(cancel)
        
        
        
        self.present(alert, animated: true, completion: nil )
    
    }

    //MARK:- //Cancel API Fire
    func CancelBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&order_id=\(BookingId!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_holiday_booking_seller_cancel", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }

    //MARK:- //Confirm Bookinng
    func ConfirmBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&order_id=\(BookingId!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_holiday_booking_seller_confirm", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }





    @objc func ConfirmBookingTap(sender: UIButton)

    {



        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)



        let ok = UIAlertAction(title: "OK", style: .default) {

            UIAlertAction in





            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary



            self.BookingId = "\(tempDictionary["id"]!)"



            self.ConfirmBooking()



            self.globalDispatchgroup.notify(queue: .main, execute: {



                self.startValue = 0



                self.recordsArray.removeAllObjects()



                self.GetHolidaySellerListing()



            })



        }



        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)



        alert.addAction(ok)

        alert.addAction(cancel)



        self.present(alert, animated: true, completion: nil )

    }

    @objc func CancelBookingTap(sender: UIButton)

    {



        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)



        let ok = UIAlertAction(title: "OK", style: .default) {

            UIAlertAction in





            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary



            self.BookingId = "\(tempDictionary["id"]!)"



            self.CancelBooking()



            self.globalDispatchgroup.notify(queue: .main, execute: {



                self.startValue = 0



                self.recordsArray.removeAllObjects()



                self.GetHolidaySellerListing()



            })



        }



        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)



        alert.addAction(ok)

        alert.addAction(cancel)



        self.present(alert, animated: true, completion: nil )

    }


    func SetFont()
    {
         headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
//    func NoData()
//    {
//        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
//        nolbl.textAlignment = .right
//        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
//        view.addSubview(nolbl)
//        nolbl.bringSubview(toFront: view)
//    }
//

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- //Holiday Booking Seller Listing Tableviewcell
class HolidayBookingSellerListingDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var DeleteView: UIView!
    @IBOutlet weak var CancelView: UIView!
    @IBOutlet weak var ConfirmBtn: UIButton!

    @IBOutlet weak var ConfirmView: UIView!
    @IBOutlet weak var BookingNo: UILabel!
    @IBOutlet weak var BookingStatus: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var CheckInDate: UILabel!
    @IBOutlet weak var TotalPrice: UILabel!
    @IBOutlet weak var CheckoutDate: UILabel!
    @IBOutlet weak var DeleteBtn: UIButton!
    @IBOutlet weak var CancelBtn: UIButton!



    //Static Labels
    @IBOutlet weak var BookingNoLbl: UILabel!
    @IBOutlet weak var BookingstatusLbl: UILabel!
    @IBOutlet weak var ProductnameLbl: UILabel!
    @IBOutlet weak var CheckindateLbl: UILabel!
    @IBOutlet weak var TotalPriceLbl: UILabel!
    @IBOutlet weak var CheckoutLbl: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

