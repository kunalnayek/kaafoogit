//
//  PostAdPhotosViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 04/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import SVProgressHUD

class PostAdPhotosViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate {



    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var addPhotosTItleview: UIView!
    @IBOutlet weak var addPhotosTitleLBL: UILabel!
    @IBOutlet weak var addPhotosSubtitleLBL: UILabel!
    @IBOutlet weak var photosCollectionview: UICollectionView!
    @IBOutlet weak var addVideoTItleview: UIView!
    @IBOutlet weak var addVideoTitleLBL: UILabel!
    @IBOutlet weak var addVideoSubtitleLBL: UILabel!
    @IBOutlet weak var addVideoStackview: UIStackView!
    @IBOutlet weak var addVideoView: UIView!
    @IBOutlet weak var addVideoImageview: UIImageView!
    @IBOutlet weak var addVideoLBL: UILabel!
    @IBOutlet weak var playVideoView: UIView!
    @IBOutlet weak var playVideoImageview: UIImageView!

    @IBOutlet weak var photosCollectionviewHeightConstraint: NSLayoutConstraint!


    var staticCellNumber = 7

    var imageArray = [NSDictionary]()

    var addedImageArray = [UIImage]()

    var addVideoRequest : Bool! = false

    var videoUrl : URL!

    var tapGesture = UITapGestureRecognizer()

    var imagesToDeleteArray = [Int]()

    var localImagesToDeleteArray = [Int]()
    var webImagesToDeleteArray = [Int]()
    var deletedImagesStringArray = [String]()

    let dispatchGroup = DispatchGroup()

    var productID : String! = ""

    var photoVideoDetailsDictionary : NSDictionary!

    var newVidAdded : Bool! = false


    // MARK:- // Viewdidload

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setFont()
        if UserDefaults.standard.bool(forKey: "isFillbasicInfo"){
            self.loadPhotoDetails()
            
            self.dispatchGroup.notify(queue: .main) {
                
                
                self.photosCollectionview.delegate = self
                self.photosCollectionview.dataSource = self
                
                
                if (self.photoVideoDetailsDictionary["video_list"] as! NSArray).count > 0
                {
                    self.addVideoView.isHidden = true
                    self.playVideoView.isHidden = false
                    
                    self.videoUrl = URL(string: "\(((self.photoVideoDetailsDictionary["video_list"] as! NSArray)[0] as! NSDictionary)["videopath"] ?? "")")
                    
                    do {
                        let asset = AVURLAsset(url: self.videoUrl, options: nil)
                        let imgGenerator = AVAssetImageGenerator(asset: asset)
                        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                        let thumbnail = UIImage(cgImage: cgImage)
                        self.playVideoImageview.image = thumbnail
                    } catch let error {
                        print("*** Error generating thumbnail: \(error.localizedDescription)")
                    }
                    
                    self.addVideoView.isHidden = true
                    self.playVideoView.isHidden = false
                    
                    self.playVideoView.bringSubviewToFront(self.playVideoImageview)
                    self.playVideoView.bringSubviewToFront(self.playVideoButtonOutlet)
                    
                }
                else
                {
                    self.addVideoView.isHidden = false
                    self.playVideoView.isHidden = true
                }
                
                
                
                
                
            }
        }
        else{
            self.photosCollectionview.delegate = self
            self.photosCollectionview.dataSource = self
        }
        
        
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.setCollectionviewHeight(heightConstraint: self.photosCollectionviewHeightConstraint)
    }



    // MARK:- // Buttons

    // MARK:- // Delete Image Button


    @IBOutlet weak var deleteImageButtonOutlet: UIButton!
    @IBAction func deleteImageButton(_ sender: UIButton) {

        self.localImagesToDeleteArray.removeAll()
        self.webImagesToDeleteArray.removeAll()

        for i in 0..<self.imagesToDeleteArray.count
        {
            if self.imagesToDeleteArray[i] > self.imageArray.count - 1
            {
                self.localImagesToDeleteArray.append(self.imagesToDeleteArray[i] - self.imageArray.count)
            }
            else
            {
                self.webImagesToDeleteArray.append(self.imagesToDeleteArray[i])
                self.deletedImagesStringArray.append("\((self.imageArray[self.imagesToDeleteArray[i]])["id"] ?? "")")
            }
        }

        print("webImagesToDeleteArray----",self.webImagesToDeleteArray)
        print("localImagesToDeleteArray----",self.localImagesToDeleteArray)

        self.imageArray.remove(at: self.webImagesToDeleteArray)
        self.addedImageArray.remove(at: self.localImagesToDeleteArray)

        self.imagesToDeleteArray.removeAll()


        if (self.addedImageArray.count + self.imageArray.count) < 7
        {
            self.staticCellNumber = 7
        }
        else
        {
            self.staticCellNumber = (self.addedImageArray.count + self.imageArray.count) + 1
        }

        self.photosCollectionview.reloadData()

        self.setCollectionviewHeight(heightConstraint: self.photosCollectionviewHeightConstraint)



        print("Deleted Images String Array  -----",self.deletedImagesStringArray)

    }



    // MARK:- // Add Video Button


    @IBOutlet weak var addVideoButtonOutlet: UIButton!
    @IBAction func addVideoButton(_ sender: UIButton) {

        self.createActionsheetToPickVideo()

    }


    // MARK:- // Play Video Button

    @IBOutlet weak var playVideoButtonOutlet: UIButton!
    @IBAction func playVideoButton(_ sender: UIButton) {

        let player = AVPlayer(url: videoUrl)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }

    }




    // MARK:- // Next Button


    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButton(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "isFillbasicInfo"){
            self.SaveAllData()
            
            self.dispatchGroup.notify(queue: .main) {
                
                if self.newVidAdded == true
                {
                    self.uploadVideo()
                    
                    self.dispatchGroup.notify(queue: .main, execute: {
                        
                        NotificationCenter.default.post(name: .passDataAddPost, object: "2")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                            NotificationCenter.default.post(name: Notification.Name("tapNextButton"), object: nil, userInfo: ["indx":3])
                        })
                        
                    })
                }
                else
                {
                    NotificationCenter.default.post(name: .passDataAddPost, object: "2")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        NotificationCenter.default.post(name: Notification.Name("tapNextButton"), object: nil, userInfo: ["indx":3])
                    })
                }
                
                
                
            }
        }
        else{
                customAlert(title: "Please provided Basic Info", message: "", completion: {
                    
                })
            }
        
    }





    // MARK:- // Delegate Methods

    // MARK:- // Collectionview Delegates

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return staticCellNumber

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photos", for: indexPath) as! PostAdPhotosCVCCollectionViewCell

        cell.crossImage.isHidden = true

        if indexPath.item < imageArray.count
        {

            cell.cellImage.sd_setImage(with: URL(string: "\((self.imageArray[indexPath.item])["imagepath"] ?? "")"))
        }
        else
        {
            let startPoint = indexPath.item - self.imageArray.count

            if startPoint < self.addedImageArray.count
            {
                cell.cellImage.image = self.addedImageArray[indexPath.item - self.imageArray.count]
            }
            else
            {
                if indexPath.row == (self.staticCellNumber - 1)
                {
                    cell.cellImage.image = UIImage(named: "add photo")
                }
                else
                {
                    cell.cellImage.image = UIImage(named: "upload photo")
                }
            }

        }

        if imagesToDeleteArray.contains(indexPath.row)
        {
            cell.crossImage.isHidden = false
        }
        else
        {
            cell.crossImage.isHidden = true
        }
        /*
         if indexPath.row < self.addedImageArray.count
         {
         cell.cellImage.image = self.addedImageArray[indexPath.row]

         if imagesToDeleteArray.contains(indexPath.row)
         {
         cell.crossImage.isHidden = false
         }
         else
         {
         cell.crossImage.isHidden = true
         }

         }
         else
         {
         if indexPath.row == (self.staticCellNumber - 1)
         {
         cell.cellImage.image = UIImage(named: "add photo")
         }
         else
         {
         cell.cellImage.image = UIImage(named: "upload photo")
         }
         }

         */

        return cell

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        let heightVal = self.photosCollectionview.frame.size.width / 3

        let tempValue = (heightVal - 10)*3

        let lineSpace = (self.photosCollectionview.frame.size.width - tempValue) / 2

        return lineSpace
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let heightVal = self.photosCollectionview.frame.size.width / 3

        return CGSize(width: heightVal - 10, height: heightVal - 10)

    }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.item < (self.addedImageArray.count + self.imageArray.count) // for cells that have images Selected
        {
            var tempInt : Int!

            if imagesToDeleteArray.contains(indexPath.item)
            {
                for i in 0..<imagesToDeleteArray.count
                {
                    if indexPath.row == imagesToDeleteArray[i]
                    {
                        tempInt = i
                    }
                }

                self.imagesToDeleteArray.remove(at: tempInt)
            }
            else
            {
                self.imagesToDeleteArray.append(indexPath.row)
            }

            //print("Images to Delete Array---",self.imagesToDeleteArray)


            self.photosCollectionview.reloadData()


        }
        else // for empty cells
        {
            if indexPath.item == (self.staticCellNumber - 1)
            {

                if (self.addedImageArray.count + self.imageArray.count) < (self.staticCellNumber - 1)
                {
                    self.createActionsheetToPickImage()
                }
                else
                {
                    self.customAlert(title: "Are you Sure?", message: "Adding more than six images will require charges.") {

                        self.createActionsheetToPickImage()

                    }
                }


            }
            else
            {
                self.createActionsheetToPickImage()
            }
        }



    }




    // MARK:- // Imagepicker Delegate


    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)


        if self.addVideoRequest == true
        {

            videoUrl = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL

            playVideoView.isHidden = false

            dismiss(animated: true) {

                do {
                    let asset = AVURLAsset(url: self.videoUrl!, options: nil)
                    let imgGenerator = AVAssetImageGenerator(asset: asset)
                    imgGenerator.appliesPreferredTrackTransform = true
                    let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                    let thumbnail = UIImage(cgImage: cgImage)
                    self.playVideoImageview.image = thumbnail
                } catch let error {
                    print("*** Error generating thumbnail: \(error.localizedDescription)")
                }

            }

            self.addVideoView.isHidden = true
            self.playVideoView.isHidden = false

            self.playVideoView.bringSubviewToFront(self.playVideoImageview)
            self.playVideoView.bringSubviewToFront(self.playVideoButtonOutlet)

            self.addVideoRequest = false

            self.newVidAdded = true

        }
        else
        {
            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage

            self.addedImageArray.append(image)

            if (self.addedImageArray.count + self.imageArray.count) == self.staticCellNumber
            {
                self.staticCellNumber = self.staticCellNumber + 1
            }

            photosCollectionview.reloadData()

            picker.dismiss(animated: true, completion: nil)

            self.setCollectionviewHeight(heightConstraint: self.photosCollectionviewHeightConstraint)
        }

    }



    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {

        dismiss(animated: true, completion: nil)

    }



    // MARK:- // Tapgesture to Delete Cell

    @objc func tapGestureHandler() {

    }






    // MARK:- // Set Collectionview Height

    func setCollectionviewHeight(heightConstraint: NSLayoutConstraint)
    {
        let heightVal = self.photosCollectionview.frame.size.width / 3

        let cellSize = (heightVal - 10)

        let tempValue = cellSize * 3

        let lineSpace = (self.photosCollectionview.frame.size.width - tempValue) / 2

        var numberOfRows : Int!

        if (self.staticCellNumber % 3) == 0
        {
            numberOfRows = (self.staticCellNumber / 3)
        }
        else
        {
            numberOfRows = (self.staticCellNumber / 3) + 1
        }

        heightConstraint.constant = (CGFloat(numberOfRows) * cellSize) + (lineSpace * CGFloat(numberOfRows - 1))

    }


    //    // MARK:- // Validation Custom Alert Function
    //
    //    func customAlert(title: String,message: String, completion : @escaping () -> ())
    //    {
    //        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //
    //        let ok = UIAlertAction(title: "OK", style: .default) {
    //            UIAlertAction in
    //
    //            UIView.animate(withDuration: 0.5)
    //            {
    //                completion()
    //            }
    //
    //        }
    //
    //        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    //
    //        alert.addAction(ok)
    //        alert.addAction(cancel)
    //
    //        self.present(alert, animated: true, completion: nil )
    //    }


    // MARK:- // Create Actionsheet to Pick Image

    func createActionsheetToPickImage()
    {
        let imagePickerController = UIImagePickerController()

        imagePickerController.delegate = self

        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)

        }))

        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)

        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

        self.present(actionSheet, animated: true, completion: nil)


    }



    // MARK:- // Create Actionsheet to Pick Video

    func createActionsheetToPickVideo()
    {
        let imagePickerController = UIImagePickerController()

        imagePickerController.delegate = self

        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            imagePickerController.mediaTypes = ["public.movie"]

            self.addVideoRequest = true

        }))

        actionSheet.addAction(UIAlertAction(title: "Phone Gallery", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            imagePickerController.mediaTypes = [kUTTypeMovie as String]

            self.addVideoRequest = true

        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

        self.present(actionSheet, animated: true, completion: nil)
    }



    // MARK:- // Set Font

    func setFont()
    {
        self.addPhotosTitleLBL.font = UIFont(name: self.addPhotosTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.addPhotosSubtitleLBL.font = UIFont(name: self.addPhotosSubtitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))

        self.addVideoTitleLBL.font = UIFont(name: self.addVideoTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.addVideoSubtitleLBL.font = UIFont(name: self.addVideoSubtitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
    }






    // MARK: - // JSON POST Method to Load PHOTO Details

    func loadPhotoDetails()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_item_photo_list")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        self.productID = UserDefaults.standard.object(forKey: "productID") as? String

        let stepFinal = UserDefaults.standard.object(forKey: "stepFinal")

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(self.productID!)&step_final=\(stepFinal ?? "")"


        print("Load Details URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Load Details Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.photoVideoDetailsDictionary = json["details_info"] as? NSDictionary

                        self.imageArray = self.photoVideoDetailsDictionary["photo_list"] as! [NSDictionary]

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }




                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()




    }






















    // MARK:- // Json Post Method to Upload an Array of Images



    func SaveAllData()
    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }

        let myUrl = NSURL(string: GLOBALAPI + "app_item_photo_save")!   //change the url

        print("Image Upload Url : ", myUrl)

        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"


        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let delCommaSeparatedString = self.deletedImagesStringArray.joined(separator: ",")

        // User "authentication":
        let parameters = ["user_id" : "\(userID!)" , "lang_id" : "\(langID!)" , "product_id" : "\(self.productID!)" , "del_image_id" : "\(delCommaSeparatedString)"]

        print("Parameters : ", parameters)

        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")

        var imageDataArray = [Data]()
        //
        //        print("Image Array : ", imageArray)
        //
        for i in 0..<addedImageArray.count
        {
            let imageData = addedImageArray[i].jpegData(compressionQuality: 1)
            imageDataArray.append(imageData!)
        }



        //        if(imageDataArray.count==0)  {
        //
        //
        //
        //
        //            DispatchQueue.main.async {
        //
        //                self.dispatchGroup.leave()
        //
        //                SVProgressHUD.dismiss()
        ////
        ////                completion()
        //            }
        //
        //            return; }

        request.httpBody = createBodyWithParametersAndImages(parameters: parameters, filePathKey: "prod_img[]", imageDataKey: imageDataArray, boundary: boundary) as Data



        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in

            if error != nil {
                print("error=\(error!)")
                return
            }

            // You can print out response object
            print("******* response = \(response!)")

            //self.responseDict = response?.copy() as! NSDictionary

            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as? NSDictionary

                print(json!)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()


                    SVProgressHUD.dismiss()
                    //
                    //                    completion()
                }

            }catch
            {
                print(error)
            }

        }

        task.resume()



    }



    // MARK:- // Creating HTTP Body with Parameters while Sending an Array of Images


    func createBodyWithParametersAndImages(parameters: [String: Any]?, filePathKey: String, imageDataKey: [Data], boundary: String) -> NSData {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }

        for index in 0..<imageDataKey.count {
            let data = imageDataKey[index]

            let filename = "image\(index).jpeg"
            let mimetype = "image/jpeg"

            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.appendString(string: "\r\n")

        }


        body.appendString(string: "--\(boundary)--\r\n")

        return body
    }





    // MARK:- // Generating Boundary String for Multipart Form Data

    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }




    // MARK:- // Create Body with Parameters for Video Upload


    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }

        let filename = "upload.mov"
        let mimetype = "video/mov"

        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")



        body.appendString(string: "--\(boundary)--\r\n")

        return body
    }




    // MARK: - // JSON POST Method to Upload Video


    func uploadVideo()
    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }

        let myUrl = NSURL(string: GLOBALAPI + "app_item_photo_save")!   //change the url

        print("Image Upload Url : ", myUrl)

        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"

        let userID = UserDefaults.standard.string(forKey: "userID")

        // User "authentication":
        let parameters = ["user_id" : "\(userID!)" , "lang_id" : "\(langID!)" , "product_id" : "\(self.productID!)"]

        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")

        var videoData: Data?
        do {
            videoData = try Data(contentsOf: videoUrl, options: Data.ReadingOptions.alwaysMapped)
        } catch _ {
            videoData = nil

            DispatchQueue.main.async {

                self.dispatchGroup.leave()

                SVProgressHUD.dismiss()
            }

            return
        }

        request.httpBody = createBodyWithParameters(parameters: parameters, filePathKey: "product_video", imageDataKey: videoData! as NSData, boundary: boundary) as Data

        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in

            if error != nil {
                print("error=\(error!)")

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            // You can print out response object
            print("******* response = \(response!)")

            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                print("Video Upload Request Response----",json!)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }catch
            {
                print(error)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }

        }

        task.resume()



    }




}



// MARK:- // Sorting array of indexes from high to low
extension Array {
    mutating func remove(at indexes: [Int]) {
        for index in indexes.sorted(by: >) {
            remove(at: index)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
