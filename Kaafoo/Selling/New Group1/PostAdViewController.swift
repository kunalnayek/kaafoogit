//
//  PostAdViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 31/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class PostAdViewController: GlobalViewController {
    
    var buttonSelected : String! = "BI"
    
    @IBOutlet weak var containerView: UIView!
    
    @IBAction func topBarButtonsTap(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            self.buttonSelected = "BI"
            
            let postAdBasicInfo = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdBasicInfoVC") as! PostAdBasicInfoViewController
            
            self.addChild(postAdBasicInfo)
            
            self.containerView.addSubview(postAdBasicInfo.view)
            
            postAdBasicInfo.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
            postAdBasicInfo.mainScroll.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
        }
        else if sender.tag == 1
        {
            self.buttonSelected = "D"
            
            let postAdDetails = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdDetailsVC") as! postAdDetailsViewController
            
            self.addChild(postAdDetails)
            
            self.containerView.addSubview(postAdDetails.view)
            
            postAdDetails.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
            postAdDetails.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0).isActive = true
            postAdDetails.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
            postAdDetails.view.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
            postAdDetails.mainScroll.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
            //postAdDetails.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: 0).isActive = true
            
            postAdDetails.didMove(toParent: self)
        }
        else if sender.tag == 2
        {
            self.buttonSelected = "P"
            
            let postAdPhotos = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdPhotosVC") as! PostAdPhotosViewController
            
            self.addChild(postAdPhotos)
            
            self.containerView.addSubview(postAdPhotos.view)
            
            postAdPhotos.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
            postAdPhotos.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0).isActive = true
            postAdPhotos.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
            postAdPhotos.view.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
            postAdPhotos.mainScroll.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
        }
        else if sender.tag == 3
        {
            
        }
        else
        {
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.buttonSelected.elementsEqual("BI")
        {
            let postAdBasicInfo = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdBasicInfoVC") as! PostAdBasicInfoViewController
            
            self.addChild(postAdBasicInfo)
            
            self.containerView.addSubview(postAdBasicInfo.view)
            
            postAdBasicInfo.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
            postAdBasicInfo.view.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
            postAdBasicInfo.mainScroll.heightAnchor.constraint(equalTo: self.containerView.heightAnchor, constant: 0).isActive = true
        }
        
        
        
    }
    
    
}
