//
//  NewPostAdCVC.swift
//  Kaafoo
//
//  Created by esolz on 16/04/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

class NewPostAdCVC: UICollectionViewCell {
    @IBOutlet weak var tabTxtLbl: UILabel!
    
    @IBOutlet weak var underLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UserDefaults.standard.string(forKey: "langID") == "AR"{
            contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
}
