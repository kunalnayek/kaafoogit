//
//  NewPostAdViewController.swift
//  Kaafoo
//
//  Created by esolz on 16/04/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

class NewPostAdViewController: GlobalViewController{

    var isNewAdd: Bool!
    var topCatID : String! = ""
    var dataArray = [String]()
    var selectedIndx = 0
    
    @IBOutlet weak var demoRegularFont: UILabel!
    @IBOutlet weak var demoBoldFont: UILabel!
    @IBOutlet weak var mainContainView: UIView!
    @IBOutlet weak var topBarCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isNewAdd == true{
            UserDefaults.standard.set(false, forKey: "isFillbasicInfo")
        }
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        print()
        if UserDefaults.standard.string(forKey: "langID") == "AR"{
            //topBarCollectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            topBarCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.tapNextButton(notification:)), name: Notification.Name("tapNextButton"), object: nil)
        
        if self.topCatID.isEmpty
        {
            
            dataArray =  ["Basic Info", "Details", "Photos", "Extras", "Confirm"]
        }
        else
        {
            
            if self.topCatID.elementsEqual("13")
            {
                dataArray = ["Basic Info", "Details", "Photos", "Confirm"]
                
            }
            else if self.topCatID.elementsEqual("16")
            {
                dataArray = ["Basic Info", "Details", "Photos", "Confirm"]
                
            }
            else if self.topCatID.elementsEqual("11")
            {
                dataArray = ["Basic Info", "Details", "Photos", "Confirm"]
            }
            else
            {
                dataArray = ["Basic Info", "Details", "Photos", "Extras", "Confirm"]
            }
        }
        let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "basicInfoVC") as! BasicInfoViewController
        self.addChild(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
        self.mainContainView.addSubview(vc.view)
        vc.didMove(toParent: self)
        
        self.topBarCollectionView.delegate = self
        self.topBarCollectionView.dataSource = self
        // Do any additional setup after loading the view.
        
    }
    
    @objc func tapNextButton(notification: Notification) {
        //print((notification.userInfo as! NSDictionary)["indx"] as! Int)
        self.selectedIndx = (notification.userInfo! as NSDictionary)["indx"] as! Int
        
        self.topBarCollectionView.reloadData()
        loadViewController()
        
    }
    func loadViewController(){
        switch selectedIndx {
        case 0:
            
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "basicInfoVC") as! BasicInfoViewController
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
        case 1:
            
            //Details
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdDetailsVC") as! postAdDetailsViewController
            
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
            
            
            
        case 2:
            
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "postAdPhotosVC") as! PostAdPhotosViewController
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
            
        case 3:
            
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "extrasVC") as! ExtrasViewController
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
            
        case 4:
            
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "confirmVC") as! ConfirmViewController
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
            
        default:
            
            let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "basicInfoVC") as! BasicInfoViewController
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.mainContainView.frame.size.width, height: self.mainContainView.frame.size.height);
            self.mainContainView.addSubview(vc.view)
            vc.didMove(toParent: self)
        }
    }

}

extension NewPostAdViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewPostAdCVC", for: indexPath) as! NewPostAdCVC
        cell.tabTxtLbl.text = dataArray[indexPath.row]
        if indexPath.row == selectedIndx{
            cell.underLineView.isHidden = false
            cell.tabTxtLbl.font = UIFont(name: self.demoRegularFont.font.fontName, size: CGFloat(self.Get_fontSize(size: 24)))
        }
        else{
            cell.underLineView.isHidden = true
            cell.tabTxtLbl.font = UIFont(name: self.demoBoldFont.font.fontName, size: CGFloat(self.Get_fontSize(size: 24)))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserDefaults.standard.bool(forKey: "isFillbasicInfo"){
            self.selectedIndx = indexPath.row
            self.topBarCollectionView.reloadData()
            self.loadViewController()
        }
        else{
            customAlert(title: "Please provided Basic Info", message: "", completion: {
                
            })
        }
        
    }
}
//self.barcodeNumberTXT.font = UIFont(name: (self.barcodeNumberTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
