//
//  PostAdBasicInfoViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 05/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class PostAdBasicInfoViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    
    
    // Category View Section
    
    @IBOutlet weak var categoriesStackview: UIStackView!
    
    // MARK:- // First Category View
    @IBOutlet weak var firstCategoryView: UIView!
    @IBOutlet weak var fcTitleLBL: UILabel!
    @IBOutlet weak var fcLBL: UILabel!
    
    
       // MARK:- // First Category Drop Down Button
    @IBOutlet weak var fcDropdownButtonOutlet: UIButton!
    @IBAction func fcDropdownButton(_ sender: UIButton) {
        
        if self.fcViewofTable.isHidden == true
        {
            self.fcViewofTable.isHidden = false
            self.fcViewOfTableHeightConstraint.constant = 300
            self.fcViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.fcViewofTable.isHidden = true
            self.fcViewOfTableHeightConstraint.constant = 0
            self.fcViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    @IBOutlet weak var fcTableview: UITableView!
    @IBOutlet weak var fcViewofTable: UIView!
    @IBOutlet weak var fcViewOfTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var fcViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
    
    // MARK:- // Second Category View
    
    @IBOutlet weak var secondCategoryView: UIView!
    @IBOutlet weak var scTitleLBL: UILabel!
    @IBOutlet weak var scLBL: UILabel!
    
       // MARK:- // Second Category Drop Down Button
    
    @IBOutlet weak var scDropdownButtonOutlet: UIButton!
    @IBAction func scDropdownButton(_ sender: UIButton) {
        
        if self.scViewOfTableview.isHidden == true
        {
            self.scViewOfTableview.isHidden = false
            self.scViewOfTableHeightConstraint.constant = 300
            self.scViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.scViewOfTableview.isHidden = true
            self.scViewOfTableHeightConstraint.constant = 0
            self.scViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    @IBOutlet weak var scViewOfTableview: UIView!
    @IBOutlet weak var scTableview: UITableView!
    @IBOutlet weak var scViewOfTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
    // MARK:- // Third Category View
    
    
    @IBOutlet weak var thirdCategoryView: UIView!
    @IBOutlet weak var tcTitleLBL: UILabel!
    @IBOutlet weak var tcLBL: UILabel!
    
    
    // MARK:- // Third Category Drop down Button
    
    @IBOutlet weak var thirdCategoryDropdownButtonOutlet: UIButton!
    @IBAction func thirdCategoryDropdownButton(_ sender: UIButton) {
        
        if self.tcViewOfTable.isHidden == true
        {
            self.tcViewOfTable.isHidden = false
            self.tcViewOfTableHeightConstraint.constant = 300
            self.tcViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.tcViewOfTable.isHidden = true
            self.tcViewOfTableHeightConstraint.constant = 0
            self.tcViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    
    
    @IBOutlet weak var tcViewOfTable: UIView!
    @IBOutlet weak var tcTableview: UITableView!
    @IBOutlet weak var tcViewOfTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tcViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
    
    
    // MARK:- // Fourth Category View
    
    
    @IBOutlet weak var lastCategoryView: UIView!
    @IBOutlet weak var lcTitleLBL: UILabel!
    @IBOutlet weak var lcLBL: UILabel!
    
    
        // MARK:- // Last Category Drop Down Button
    
    @IBOutlet weak var lcDropdownButtonOutlet: UIButton!
    @IBAction func lcDropdownButton(_ sender: UIButton) {
        
        if self.lcViewOfTable.isHidden == true
        {
            self.lcViewOfTable.isHidden = false
            self.lcViewOfTableHeightConstraint.constant = 300
            self.lcViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.lcViewOfTable.isHidden = true
            self.lcViewOfTableHeightConstraint.constant = 0
            self.lcViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    
    @IBOutlet weak var lcViewOfTable: UIView!
    @IBOutlet weak var lcTableview: UITableView!
    @IBOutlet weak var lcViewOfTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lcViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
    
    // MARK:- // Other Info View
    
    @IBOutlet weak var otherInfoStackview: UIStackView!
    
    @IBOutlet weak var otherInfoTopView: UIView!
    
    
    // MARK:- // Location View
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTitleLBL: UILabel!
    @IBOutlet weak var locationLBL: UILabel!
    
    
    @IBOutlet weak var locationDropdownButtonOutlet: UIButton!
    @IBAction func locationDropdownButton(_ sender: UIButton) {
        
        if self.locationViewOfTable.isHidden == true
        {
            self.locationViewOfTable.isHidden = false
            self.locationViewOfTableHeightConstraint.constant = 300
            self.locationViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.locationViewOfTable.isHidden = true
            self.locationViewOfTableHeightConstraint.constant = 0
            self.locationViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    
    @IBOutlet weak var locationViewOfTable: UIView!
    @IBOutlet weak var locationTableview: UITableView!
    
    @IBOutlet weak var locationViewOfTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
    
    
    // MARK:- // Name View
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTitleLBL: UILabel!
    @IBOutlet weak var nameTXT: UITextField!
    
    
    
    // MARK:- // Listing Duration View
    
    @IBOutlet weak var listingDurationStackview: UIStackView!
    @IBOutlet weak var listingDurationDropdownView: UIView!
    @IBOutlet weak var listingDurationTitleLBL: UILabel!
    @IBOutlet weak var listingDurationLBL: UILabel!
    
    
        // MARK:- // Listing Duration Drop down Button
    
    @IBOutlet weak var listingDurationDropdownButtonOutlet: UIButton!
    @IBAction func listingDurationDropdownButton(_ sender: UIButton) {
        
        if self.listingDurationViewOfTable.isHidden == true
        {
            self.listingDurationViewOfTable.isHidden = false
            self.listingDurationViewOfTableHeightCOnstraint.constant = 300
            self.listingDurationViewOfTableBottomConstraint.constant = 20
        }
        else
        {
            self.listingDurationViewOfTable.isHidden = true
            self.listingDurationViewOfTableHeightCOnstraint.constant = 0
            self.listingDurationViewOfTableBottomConstraint.constant = 50
        }
        
    }
    
    
    @IBOutlet weak var listingDurationViewOfTable: UIView!
    @IBOutlet weak var listingDurationTableview: UITableView!
    
    @IBOutlet weak var listingDurationViewOfTableHeightCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var listingDurationViewOfTableBottomConstraint: NSLayoutConstraint!
    
    
         // MARK:- // Listing Duration To and From View
    
    
    @IBOutlet weak var listingDurationToFromView: UIView!
    
    @IBOutlet weak var orStaticLBL: UILabel!
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var fromTitleLBL: UILabel!
    @IBOutlet weak var fromTXT: UITextField!
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var toTitleLBL: UILabel!
    @IBOutlet weak var toTXT: UITextField!
    
    
    
    // MARK:- // Driver AGe View
    
    
    @IBOutlet weak var driverAgeView: UIView!
    @IBOutlet weak var driverAgeTitleLBL: UILabel!
    @IBOutlet weak var driverAgeTXT: UITextField!
    
    
    
    // MARK:- // Name of Food View
    
    
    @IBOutlet weak var nameOfFoodView: UIView!
    @IBOutlet weak var nameOfFoodTitleLBL: UILabel!
    @IBOutlet weak var nameOfFoodTXT: UITextField!
    
    
    
    
    // MARK:- // Quantity View
    
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityTItleLBL: UILabel!
    @IBOutlet weak var quantityTXT: UITextField!
    
    
    // MARK:- // Popular Dishes View
    
    
    @IBOutlet weak var popularDishesView: UIView!
    @IBOutlet weak var popularDishesTitleLBL: UILabel!
    @IBOutlet weak var popularDishesStackview: UIStackView!
    
    
    @IBOutlet weak var popularDishesStackviewHeightConstraint: NSLayoutConstraint!
    
    
    
    // MARK:- // Name of Hotel View
    
    @IBOutlet weak var nameOfHotelView: UIView!
    @IBOutlet weak var nameOfHotelTitleLBL: UILabel!
    @IBOutlet weak var nameOfHotelTXT: UITextField!
    
    
    
    // MARK:- // No. of Rooms View
    
    @IBOutlet weak var noOfRoomsView: UIView!
    @IBOutlet weak var noOfRoomsTitleLBL: UILabel!
    @IBOutlet weak var noOfRoomsTXT: UITextField!
    
    
    
    // MARK:- // Number of person (each room) View
    
    @IBOutlet weak var noOfPersonView: UIView!
    @IBOutlet weak var noOfPersonTitleLBL: UILabel!
    @IBOutlet weak var noOfPersonTXT: UITextField!
    
    
    // MARK:- // Event Name View
    
    @IBOutlet weak var eventNameView: UIView!
    @IBOutlet weak var eventNameTitleLBL: UILabel!
    @IBOutlet weak var eventNameTXT: UITextField!
    
    
    
    
    // MARK:- // No of People View
    
    @IBOutlet weak var noOfPeopleView: UIView!
    @IBOutlet weak var noOFPeopleTitleLBL: UILabel!
    @IBOutlet weak var noOfPeopleTXT: UITextField!
    
    
    
    // MARK:- // Name of Service View
    
    @IBOutlet weak var nameOfServiceView: UIView!
    @IBOutlet weak var nameOfServiceTitleLBL: UILabel!
    @IBOutlet weak var nameOfServiceTXT: UITextField!
    
    
    
    // MARK:- // No of Vacancy View
    
    @IBOutlet weak var noOfVacancyView: UIView!
    @IBOutlet weak var noOfVacancyTitleLBL: UILabel!
    @IBOutlet weak var noOfVacancyTXT: UITextField!
    
    
    
    // MARK:- // Product Type View
    
    
    @IBOutlet weak var productTypeView: UIView!
    @IBOutlet weak var productTypeTitleLBL: UILabel!
    @IBOutlet weak var producTypeStackview: UIStackView!
    
    
    @IBOutlet weak var productTypeStackviewHeightConstraint: NSLayoutConstraint!
    
    
    
    // MARK:- // Barcode Number View
    
    @IBOutlet weak var barcodeNumberView: UIView!
    @IBOutlet weak var barcodeNumberTitleLBL: UILabel!
    @IBOutlet weak var barcodeNumberTXT: UITextField!
    
    
    // MARK:- // Product Condition View
    
    @IBOutlet weak var productConditionView: UIView!
    @IBOutlet weak var productConditionTitleLBL: UILabel!
    @IBOutlet weak var productConditionStackview: UIStackView!
    
    @IBOutlet weak var productConditionStackviewHeightConstraint: NSLayoutConstraint!
    
    
    
    
    
    
    
    
    
    let dispatchGroup = DispatchGroup()
    
    var allDataDictionary : NSDictionary!
    
    var topCatID : String!
    
    var parentID : String!
    
    var nextLabel : String!
    
    var catID : String!
    
    var topTablesCategory : Int!
    
    var titleLabelsArray = [UILabel]()
    var titleLabelStringsArray = [String]()
    
    var firstCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
    var secondCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
    var thirdCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
    var lastCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
    
    
    var otherInfoArray : NSArray!
    
    var locationArray = [["id" : "","show" : "","value" : "Select Location"]]
    
    var listingDurationArray = [["id" : "","value" : "Select Listing Duration","show" : 1]]
    
    var popularDishesDataArray = [NSDictionary]()
    var popularDishesRadiobuttonImageArray = [UIImageView]()
    
    var productType : String! = ""
    var productTypeDataArray = [NSDictionary]()
    var productTypeRadiobuttonImageArray = [UIImageView]()
    
    var productConditionDataArray = [NSDictionary]()
    var productConditionRadioButtonImageArray = [UIImageView]()
    
    private var datepickerWithDate : UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setFont()
        
        self.createMandatoryLabels(label: [self.fcTitleLBL,self.scTitleLBL,self.tcTitleLBL,self.lcTitleLBL], withText: ["Select Category","Select Category","Select Category","Select Category"])

        self.getData(category: 0)
        
        self.fromTXT.delegate = self
        self.toTXT.delegate = self
        
    }
    
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == fcTableview
        {
            return self.firstCategoryArray.count
        }
        else if tableView == scTableview
        {
            return self.secondCategoryArray.count
        }
        else if tableView == tcTableview
        {
            return thirdCategoryArray.count
        }
        else if tableView == lcTableview
        {
            return lastCategoryArray.count
        }
        else if tableView == locationTableview
        {
            return locationArray.count
        }
        else //if tableView == listingDurationTableview
        {
            return listingDurationArray.count
        }
        
        //return (self.allDataDictionary["category_info"] as! NSArray).count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.fcTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryOne")
            
            cell?.textLabel?.text = "\((firstCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
          
            
            return cell!
        }
        else if tableView == scTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTwo")
            
            cell?.textLabel?.text = "\((secondCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            return cell!
        }
        else if tableView == tcTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryThree")
            
            cell?.textLabel?.text = "\((thirdCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            return cell!
        }
        else if tableView == lcTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryFour")
            
            cell?.textLabel?.text = "\((lastCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            return cell!
        }
        else if tableView == locationTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "location")
            
            cell?.textLabel?.text = "\((locationArray[indexPath.row])["value"] ?? "")"
            
            return cell!
        }
        else //if tableView == listingDurationTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listingDuration")
            
            cell?.textLabel?.text = "\((listingDurationArray[indexPath.row])["value"] ?? "")"
            
            return cell!
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == fcTableview
        {
            
            self.fcLBL.text = "\((firstCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            self.topCatID = "\((firstCategoryArray[indexPath.row])["id"] ?? "")"
            self.parentID = self.topCatID
            self.catID = self.topCatID
            
            
            self.fcViewofTable.isHidden = true
            self.fcViewOfTableHeightConstraint.constant = 0
            self.fcViewOfTableBottomConstraint.constant = 50
            
            self.secondCategoryView.isHidden = true
            self.thirdCategoryView.isHidden = true
            self.lastCategoryView.isHidden = true
            
            secondCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
            thirdCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
            lastCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]

            
            if self.topCatID.isEmpty
            {
                self.secondCategoryArray.removeAll()
            }
            else
            {
                self.topTablesCategory = 1
                self.getData(category: 1)
            }
            
            
        }
        else if tableView == scTableview
        {
            self.scLBL.text = "\((secondCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            
            self.parentID = "\((secondCategoryArray[indexPath.row])["id"] ?? "")"
            self.nextLabel = self.parentID
            self.catID = "\(self.topCatID!),\(self.parentID!)"
            
            
            self.scViewOfTableview.isHidden = true
            self.scViewOfTableHeightConstraint.constant = 0
            self.scViewOfTableBottomConstraint.constant = 50
            
            self.thirdCategoryView.isHidden = true
            self.lastCategoryView.isHidden = true
            
            thirdCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
            lastCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]

            
            if self.parentID.isEmpty
            {
                self.thirdCategoryArray.removeAll()
            }
            else
            {
                self.topTablesCategory = 2
                self.getData(category: 2)
            }
            
        }
        else if tableView == tcTableview
        {
            self.tcLBL.text = "\((thirdCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            self.parentID = "\((thirdCategoryArray[indexPath.row])["id"] ?? "")"
            self.catID = "\(self.topCatID!),\(self.nextLabel!),\(self.parentID!)"
            
            self.tcViewOfTable.isHidden = true
            self.tcViewOfTableHeightConstraint.constant = 0
            self.tcViewOfTableBottomConstraint.constant = 50
            
            self.lastCategoryView.isHidden = true
            
            lastCategoryArray = [["id" : "","parentid" : "","categoryname" : "Select Category"]]
            
            
            if self.parentID.isEmpty
            {
                self.lastCategoryArray.removeAll()
            }
            else
            {
                self.topTablesCategory = 3
                self.getData(category: 3)
            }
            
        }
        else if tableView == lcTableview
        {
            self.lcLBL.text = "\((lastCategoryArray[indexPath.row])["categoryname"] ?? "")"
            
            self.lcViewOfTable.isHidden = true
            self.lcViewOfTableHeightConstraint.constant = 0
            self.lcViewOfTableBottomConstraint.constant = 50
            
        }
        else if tableView == locationTableview
        {
            self.locationLBL.text = "\((locationArray[indexPath.row])["value"] ?? "")"
            
            self.locationViewOfTable.isHidden = true
            self.locationViewOfTableHeightConstraint.constant = 0
            self.locationViewOfTableBottomConstraint.constant = 50
            
        }
        else if tableView == listingDurationTableview
        {
            self.listingDurationLBL.text = "\((listingDurationArray[indexPath.row])["value"] ?? "")"
            
            self.listingDurationViewOfTable.isHidden = true
            self.listingDurationViewOfTableHeightCOnstraint.constant = 0
            self.listingDurationViewOfTableBottomConstraint.constant = 50
            
        }
        
    }
    
    
    
    
    // MARK:- // Textfield Delegates
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let dateFormat = DateFormatter()
        
        if textField == fromTXT
        {
            dateFormat.dateFormat = "dd/MM/yyyy"
            self.fromTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
        }
        else if textField == toTXT
        {
            dateFormat.dateFormat = "dd/MM/yyyy"
            self.toTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
        }
        
        
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to get Listing Data
    
    func getData(category : Int) // 0 = First , 1 = Second , 2 = Third , 3 = Fourth
        
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_post_category")!       //change the url
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "lang_id=\(langID!)&user_id=\(userID!)&parent_category=\(parentID ?? "")&top_catid=\(topCatID ?? "")&next_label=\(nextLabel ?? "")&product_type=\(productType ?? "")&catid=\(catID ?? "")"
        
        
        print("Get Data URL : ", url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
                
                return
            }
            
            guard let data = data else {
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("First Category List Response: " , json)
                    
                    self.allDataDictionary = json.copy() as? NSDictionary
                    
                    self.otherInfoArray = self.allDataDictionary["other_info"] as? NSArray
                    
                    if category == 0
                    {
                        // Creating First Category Array
                        for i in 0..<(self.allDataDictionary["category_info"] as! NSArray).count
                        {
                            self.firstCategoryArray.append((self.allDataDictionary["category_info"] as! NSArray)[i] as! [String : String])
                        }
                    }
                    else if category == 1
                    {
                        // Creating Second Category Array
                        for i in 0..<(self.allDataDictionary["category_info"] as! NSArray).count
                        {
                            self.secondCategoryArray.append((self.allDataDictionary["category_info"] as! NSArray)[i] as! [String : String])
                        }
                    }
                    else if category == 2
                    {
                        // Creating Third Category Array
                        for i in 0..<(self.allDataDictionary["category_info"] as! NSArray).count
                        {
                            self.thirdCategoryArray.append((self.allDataDictionary["category_info"] as! NSArray)[i] as! [String : String])
                        }
                    }
                    else if category == 3
                    {
                        // Creating Last Category Array
                        for i in 0..<(self.allDataDictionary["category_info"] as! NSArray).count
                        {
                            self.lastCategoryArray.append((self.allDataDictionary["category_info"] as! NSArray)[i] as! [String : String])
                        }
                    }
                    
                    
                    
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        if category == 0
                        {
                            self.fcTableview.delegate = self
                            self.fcTableview.dataSource = self
                            self.fcTableview.reloadData()
                        }
                        else if category == 1
                        {
                            self.catID = "\(self.topCatID!)"
                            
                            if self.secondCategoryArray.count > 1
                            {
                                self.secondCategoryView.isHidden = false
                                
                                self.scLBL.text = "Select Category"
                                
                                self.scTableview.delegate = self
                                self.scTableview.dataSource = self
                                self.scTableview.reloadData()
                            }
                            else
                            {
                                self.secondCategoryView.isHidden = true
                            }
                            
                            
                        }
                        else if category == 2
                        {
                            
                            self.catID = self.catID + self.parentID
                            
                            if self.thirdCategoryArray.count > 1
                            {
                                self.thirdCategoryView.isHidden = false
                                
                                self.tcLBL.text = "Select Category"
                                
                                self.tcTableview.delegate = self
                                self.tcTableview.dataSource = self
                                self.tcTableview.reloadData()
                            }
                            else
                            {
                                self.thirdCategoryView.isHidden = true
                            }
                            
                            
                        }
                        else //if category == 3
                        {
                            
                            self.catID = self.catID + self.parentID
                            
                            if self.lastCategoryArray.count > 1
                            {
                                self.lastCategoryView.isHidden = false
                                
                                self.lcLBL.text = "Select Category"
                                
                                self.lcTableview.delegate = self
                                self.lcTableview.dataSource = self
                                self.lcTableview.reloadData()
                            }
                            else
                            {
                                self.lastCategoryView.isHidden = true
                            }
                            
                            
                        }
                        
                        
                        //Create Other Info View
                        self.createOtherInfoView()
                        
                        
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Create Other Info View
    
    func createOtherInfoView()
    {
        
        if self.otherInfoArray.count > 0
        {
            self.otherInfoStackview.isHidden = false
        }
        else
        {
            self.otherInfoStackview.isHidden = true
        }
        
        for view in self.otherInfoStackview.subviews
        {
            view.isHidden = true
        }
        
        self.otherInfoTopView.isHidden = false
        
        for i in 0..<self.otherInfoArray.count
        {
            
            // Location View
            if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Location")
            {
                self.locationArray.removeAll()
                for j in 0..<((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                {
                    self.locationArray.append(((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[j] as! [String : String])
                    //self.locationArray.append(((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[j])
                }
                
                self.locationView.isHidden = false
                
                self.locationTableview.delegate = self
                self.locationTableview.dataSource = self
                self.locationTableview.reloadData()
                
                
                self.titleLabelsArray.append(self.locationTitleLBL)
                self.titleLabelStringsArray.append("Location")
            }
            
                
            // Name View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Name")
            {
                self.nameView.isHidden = false
                
                self.titleLabelsArray.append(self.nameTitleLBL)
                self.titleLabelStringsArray.append("Name")
            }
            
            
            // Listing Duration View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Listing duration")
            {
                
                self.listingDurationArray.removeAll()
                for j in 0..<((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                {
                    self.listingDurationArray.append(((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[j] as! [String : Any])
                }
                
                self.listingDurationStackview.isHidden = false
                
                if "\((self.otherInfoArray[i] as! NSDictionary)["calender_show"] ?? "")".elementsEqual("0")
                {
                    self.listingDurationToFromView.isHidden = true
                }
                else
                {
                    self.listingDurationToFromView.isHidden = false
                    self.setDatepickerWithDate()
                }
                
                self.listingDurationTableview.delegate = self
                self.listingDurationTableview.dataSource = self
                self.listingDurationTableview.reloadData()
                
                self.titleLabelsArray.append(self.listingDurationTitleLBL)
                self.titleLabelStringsArray.append("Listing Duration")
            }
            
            
            // Driver Age View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Driver age")
            {
                self.driverAgeView.isHidden = false
                
                self.titleLabelsArray.append(self.driverAgeTitleLBL)
                self.titleLabelStringsArray.append("Driver Age")
            }
            
            // Name of Food View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Name of food")
            {
                self.nameOfFoodView.isHidden = false
                
                self.titleLabelsArray.append(self.nameOfFoodTitleLBL)
                self.titleLabelStringsArray.append("Name of Food")
            }
            
            // Quantity View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Quantity")
            {
                self.quantityView.isHidden = false
                
                self.titleLabelsArray.append(self.quantityTItleLBL)
                self.titleLabelStringsArray.append("Quantity")
            }
            
            // Popular Dishes View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Popular Dishes")
            {
                self.popularDishesView.isHidden = false
                
                self.popularDishesDataArray.removeAll()
                for x in 0..<((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                {
                    
                    self.popularDishesDataArray.append((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary))
                    //self.popularDishesDataArray.append("\((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary)["value"] ?? "")")
                }
                
                self.createRadioButtons(inside: self.popularDishesStackview, viewHeightConstraint: self.popularDishesStackviewHeightConstraint, dataArray: self.popularDishesDataArray, buttonImageArray: &self.popularDishesRadiobuttonImageArray , section: "PopularDishes")
                
                self.titleLabelsArray.append(self.popularDishesTitleLBL)
                self.titleLabelStringsArray.append("Popular Dishes")
                
                
            }
            
            
            // Name of Hotel View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Name of hotel")
            {
                self.nameOfHotelView.isHidden = false
                
                self.titleLabelsArray.append(self.nameOfHotelTitleLBL)
                self.titleLabelStringsArray.append("Name of Hotel")
            }
            
            
            // No of Rooms View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("No of Room")
            {
                self.noOfRoomsView.isHidden = false
                
                self.titleLabelsArray.append(self.noOfRoomsTitleLBL)
                self.titleLabelStringsArray.append("No. of Rooms")
            }
            
            
            // No of Person (each room) View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Number of person (each room)")
            {
                self.noOfPersonView.isHidden = false
                
                self.titleLabelsArray.append(self.noOfPersonTitleLBL)
                self.titleLabelStringsArray.append("No. of Person (each room)")
            }
            
            
            // Event Name View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Event Name")
            {
                self.eventNameView.isHidden = false
                
                self.titleLabelsArray.append(self.eventNameTitleLBL)
                self.titleLabelStringsArray.append("Event Name")
            }
            
            
            // No. Of People View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("No of people")
            {
                self.noOfPeopleView.isHidden = false
                
                self.titleLabelsArray.append(self.noOFPeopleTitleLBL)
                self.titleLabelStringsArray.append("No. of People")
            }
            
            
            // Name of Service View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Name of Service")
            {
                self.nameOfServiceView.isHidden = false
                
                self.titleLabelsArray.append(self.nameOfServiceTitleLBL)
                self.titleLabelStringsArray.append("Name of Service")
            }
            
            
            // No. of Vacancy View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("No of Vacancy")
            {
                self.noOfVacancyView.isHidden = false
                
                self.titleLabelsArray.append(self.noOfVacancyTitleLBL)
                self.titleLabelStringsArray.append("No. of Vacancy")
            }
            
            
            
            // Product Type View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Product Type")
            {
                self.productTypeView.isHidden = false
                
                self.productTypeDataArray.removeAll()
                for x in 0..<((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                {
                    if "\((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary)["show"] ?? "")".elementsEqual("1")
                    {
                        self.productTypeDataArray.append((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary))
                        //self.productTypeDataArray.append("\((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary)["value"] ?? "")")
                    }
                    
                }
                
                self.createRadioButtons(inside: self.producTypeStackview, viewHeightConstraint: self.productTypeStackviewHeightConstraint, dataArray: self.productTypeDataArray, buttonImageArray: &self.productTypeRadiobuttonImageArray , section: "ProductType")
                
                self.titleLabelsArray.append(self.productTypeTitleLBL)
                self.titleLabelStringsArray.append("Product Type")
                
                
            }
            
            
            
            // Barcode Number View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Barcode number")
            {
                self.barcodeNumberView.isHidden = false
                
                self.titleLabelsArray.append(self.barcodeNumberTitleLBL)
                self.titleLabelStringsArray.append("Barcode Number")
            }
            
            
            
            // Product Condition View
            else if "\((self.otherInfoArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Product Condition")
            {
                self.productConditionView.isHidden = false
                
                self.productConditionDataArray.removeAll()
                for x in 0..<((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                {
                    
                    self.productConditionDataArray.append((((self.otherInfoArray[i] as! NSDictionary)["field_value"] as! NSArray)[x] as! NSDictionary))
                    
                }
                
                self.createRadioButtons(inside: self.productConditionStackview, viewHeightConstraint: self.productConditionStackviewHeightConstraint, dataArray: self.productConditionDataArray, buttonImageArray: &self.productConditionRadioButtonImageArray , section: "ProductCondition")
                
                self.titleLabelsArray.append(self.productConditionTitleLBL)
                self.titleLabelStringsArray.append("Product Condition")
                
            }
        }
        
        
        
        
        // MARK:- // Give Labels Mandatory Asterisk
        self.createMandatoryLabels(label: self.titleLabelsArray, withText: self.titleLabelStringsArray)
        

    }
    
    
    
    
    
    
    
    
    
    // MARK:- // Create Other Second Info View
    
    func createOtherSecondInfoView()
    {
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Create Radio Buttons
    
    func createRadioButtons(inside: UIStackView , viewHeightConstraint: NSLayoutConstraint , dataArray: [NSDictionary] , buttonImageArray : inout [UIImageView] ,section : String)
    {
        
        for view in inside.subviews
        {
            view.removeFromSuperview()
        }
        
        
        viewHeightConstraint.constant = CGFloat(dataArray.count) * 25
        
        buttonImageArray.removeAll()
        
        for i in 0..<dataArray.count
        {
            let backgroundView = UIView()
            backgroundView.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
            backgroundView.widthAnchor.constraint(equalToConstant: self.otherInfoStackview.frame.size.width).isActive = true
            
            
        
            
            let buttonImageview = UIImageView(frame: CGRect(x: 0, y: 5, width: 15, height: 15))
            let buttonName = UILabel(frame: CGRect(x: 25, y: 0, width: 200, height: 25))
            let radioButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.otherInfoStackview.frame.size.width, height: 25))
        
            backgroundView.addSubview(buttonImageview)
            backgroundView.addSubview(buttonName)
            backgroundView.addSubview(radioButton)
            
            buttonImageArray.append(buttonImageview)
            buttonImageview.image = UIImage(named: "radioUnselected")
            
            buttonName.text = "\((dataArray[i])["value"] ?? "")"
            buttonName.font = UIFont(name: self.popularDishesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            radioButton.tag = i
            
            if section.elementsEqual("PopularDishes")
            {
                radioButton.addTarget(self, action: #selector(PostAdBasicInfoViewController.popularDishes(sender:)), for: .touchUpInside)
            }
            
            if section.elementsEqual("ProductType")
            {
                radioButton.addTarget(self, action: #selector(PostAdBasicInfoViewController.productType(sender:)), for: .touchUpInside)
            }
            
            if section.elementsEqual("ProductCondition")
            {
                radioButton.addTarget(self, action: #selector(PostAdBasicInfoViewController.productCondition(sender:)), for: .touchUpInside)
            }

            inside.addArrangedSubview(backgroundView)
        
        }
        
        
        
        /*
        let stackView = UIStackView()
        
        stackView.distribution  = UIStackViewDistribution.equalSpacing
        stackView.alignment = UIStackViewAlignment.center
        
        self.popularDishesView.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.topAnchor.constraint(equalTo: self.popularDishesTitleLBL.bottomAnchor, constant: 10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.popularDishesView.leadingAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.popularDishesView.trailingAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.popularDishesView.bottomAnchor, constant: 0).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 15 * CGFloat(dataArray.count))
        
        for i in 0..<dataArray.count
        {
            let backgroundView = UIView()
            
            stackView.addArrangedSubview(backgroundView)
            
            backgroundView.heightAnchor.constraint(equalToConstant: 30).isActive = true
            backgroundView.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: 0).isActive = true
            
            if i == 0
            {
                backgroundView.backgroundColor = UIColor.red
            }
            else if i == 1
            {
                backgroundView.backgroundColor = UIColor.blue
            }
            
            
            //inside.translatesAutoresizingMaskIntoConstraints = false
            
            //inside.addArrangedSubview(backgroundView)
            /*
            let buttonImageview = UIImageView()
            let buttonName = UILabel()
            let radioButton = UIButton()
            
            buttonImageview.backgroundColor = UIColor.green
            buttonName.backgroundColor = UIColor.orange
            radioButton.backgroundColor = UIColor.lightGray
            
            backgroundView.addSubview(buttonImageview)
            backgroundView.addSubview(buttonName)
            backgroundView.addSubview(radioButton)
            
            backgroundView.translatesAutoresizingMaskIntoConstraints = false
            
            buttonImageview.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor, constant: 0).isActive = true
            buttonImageview.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 0).isActive = true
            buttonImageview.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: 0).isActive = true
            buttonImageview.widthAnchor.constraint(equalToConstant: 15).isActive = true
            buttonImageview.heightAnchor.constraint(equalToConstant: 15).isActive = true
            
            buttonImageview.image = UIImage(named: "radioUnselected")
            self.popularDishesRadiobuttonImageArray.append(buttonImageview)
            
            buttonName.leadingAnchor.constraint(equalTo: buttonImageview.leadingAnchor, constant: 10).isActive = true
            buttonName.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 0).isActive = true
            buttonName.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: 0).isActive = true
            buttonName.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor, constant: 0)
            
            buttonName.text = dataArray[i]
            
            radioButton.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor, constant: 0).isActive = true
            radioButton.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 0).isActive = true
            radioButton.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: 0).isActive = true
            radioButton.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor, constant: 0)
            
            radioButton.tag = i
            
            if section.elementsEqual("PopularDishes")
            {
                radioButton.addTarget(self, action: #selector(PostAdBasicInfoViewController.popularDishes(sender:)), for: .touchUpInside)
            }
          */
        }
        //viewHeightConstraint.constant = 15 * CGFloat(dataArray.count)
        */
    }
    
    
    
    // MARK:- // Popular Dishes Radio Button Action
    
    @objc func popularDishes(sender: UIButton)
    {
        
        for i in 0..<self.popularDishesRadiobuttonImageArray.count
        {
            self.popularDishesRadiobuttonImageArray[i].image = UIImage(named: "radioUnselected")
        }
        
        self.popularDishesRadiobuttonImageArray[sender.tag].image = UIImage(named: "radioSelected")
        
        
    }
    
    
    // MARK:- // Product Type Radio Button Action
    
    @objc func productType(sender: UIButton)
    {
        for i in 0..<self.productTypeRadiobuttonImageArray.count
        {
            self.productTypeRadiobuttonImageArray[i].image = UIImage(named: "radioUnselected")
        }
        self.productTypeRadiobuttonImageArray[sender.tag].image = UIImage(named: "radioSelected")
        
        self.productType = "\((self.productTypeDataArray[sender.tag])["id"] ?? "")"
        
        self.getData(category: self.topTablesCategory)
        
        self.dispatchGroup.notify(queue: .main) {
            
            for i in 0..<self.productTypeRadiobuttonImageArray.count
            {
                self.productTypeRadiobuttonImageArray[i].image = UIImage(named: "radioUnselected")
            }
            
            self.productTypeRadiobuttonImageArray[sender.tag].image = UIImage(named: "radioSelected")
            
        }
        
    }
    
    
    // MARK:- // Product Condition Radio Button Action
    
    @objc func productCondition(sender: UIButton)
    {
        
        for i in 0..<self.productConditionRadioButtonImageArray.count
        {
            self.productConditionRadioButtonImageArray[i].image = UIImage(named: "radioUnselected")
        }
        
        self.productConditionRadioButtonImageArray[sender.tag].image = UIImage(named: "radioSelected")
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Make Asterisk (*) beside mandatory labels
    
    func createMandatoryLabels(label: [UILabel], withText: [String])
    {
        for i in 0..<label.count
        {
            let passwordAttriburedString = NSMutableAttributedString(string: withText[i])
            let asterix = NSAttributedString(string: "*", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            passwordAttriburedString.append(asterix)
            
            label[i].attributedText = passwordAttriburedString
        }
    }
    
    
    // MARK:- // Set up Datepicker with Date
    
    func setDatepickerWithDate()
    {
        datepickerWithDate = UIDatePicker()
        datepickerWithDate?.datePickerMode = .date
        fromTXT.inputView = datepickerWithDate
        toTXT.inputView = datepickerWithDate
    }
    
    // MARK:- // Set Font
    
    func setFont()
    {
        self.fcTitleLBL.font = UIFont(name: self.fcTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.fcLBL.font = UIFont(name: self.fcLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.scTitleLBL.font = UIFont(name: self.scTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.scLBL.font = UIFont(name: self.scLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.tcTitleLBL.font = UIFont(name: self.tcTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.tcLBL.font = UIFont(name: self.tcLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.lcTitleLBL.font = UIFont(name: self.lcTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.lcLBL.font = UIFont(name: self.lcLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        self.locationLBL.font = UIFont(name: self.locationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.listingDurationLBL.font = UIFont(name: self.listingDurationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        self.locationLBL.font = UIFont(name: self.locationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        self.orStaticLBL.font = UIFont(name: self.orStaticLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.toTitleLBL.font = UIFont(name: self.toTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.fromTitleLBL.font = UIFont(name: self.fromTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        for i in 0..<self.titleLabelsArray.count
        {
            self.titleLabelsArray[i].font = UIFont(name: self.titleLabelsArray[i].font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        }
        
        
        self.nameTXT.font = UIFont(name: (self.nameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.nameOfFoodTXT.font = UIFont(name: (self.nameOfFoodTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.quantityTXT.font = UIFont(name: (self.quantityTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.nameOfHotelTXT.font = UIFont(name: (self.nameOfHotelTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.noOfRoomsTXT.font = UIFont(name: (self.noOfRoomsTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.eventNameTXT.font = UIFont(name: (self.eventNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.noOfPeopleTXT.font = UIFont(name: (self.noOfPeopleTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.nameOfServiceTXT.font = UIFont(name: (self.nameOfServiceTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.noOfVacancyTXT.font = UIFont(name: (self.noOfVacancyTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.barcodeNumberTXT.font = UIFont(name: (self.barcodeNumberTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.fromTXT.font = UIFont(name: (self.fromTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.toTXT.font = UIFont(name: (self.toTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.noOfPersonTXT.font = UIFont(name: (self.noOfPersonTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        self.driverAgeTXT.font = UIFont(name: (self.driverAgeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        
        
    }

    

}
