//
//  ConfirmViewController.swift
//  Kaafoo
//
//  Created by admin on 06/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ConfirmViewController: GlobalViewController {

    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var yourAccountBalanceLBL: UILabel!
    @IBOutlet weak var yourAccountBalance: UILabel!


    @IBOutlet weak var totalExtraFeesLBL: UILabel!
    @IBOutlet weak var totalExtraFees: UILabel!


    @IBOutlet weak var remainingBalanceLBL: UILabel!
    @IBOutlet weak var remainingBalance: UILabel!



    // MARK:- // Confirm Button

    @IBOutlet weak var confirmButtonOutlet: UIButton!
    @IBAction func confirmTap(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "isFillbasicInfo")  {      self.confirmRequest()
            
            self.dispatchGroup.notify(queue: .main) {
                
                self.navigate()
                
            }
        }
        else{
            customAlert(title: "Please provided Basic Info", message: "", completion: {
                
            })
        }
        
        
    }


    let dispatchGroup = DispatchGroup()

    var allDataDictionary : NSDictionary!



    override func viewDidLoad() {
        super.viewDidLoad()

        self.setStaticStrings()
        set_font()


        self.getConfirmDetails()

        self.dispatchGroup.notify(queue: .main) {

            self.fillData()

        }

    }






    // MARK: - // JSON POST Method to get Confirm Details

    func getConfirmDetails()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_item_confirm_info")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let productID = UserDefaults.standard.object(forKey: "productID") as! String

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(productID)"


        print("Get Data with Product ID URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Get Data with Product ID Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json as? NSDictionary

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }




    // MARK: - // JSON POST Method to get Confirm Request

    func confirmRequest()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_item_confirm_save")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let productID = UserDefaults.standard.object(forKey: "productID") as! String

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(productID)&total_extra_fees=\(self.totalExtraFees.text ?? "")&remaining_balance=\(self.remainingBalance.text ?? "")&confirm_status=\((self.allDataDictionary["info_array"] as! NSDictionary)["confirm_status"] ?? "")"


        print("Get Data with Product ID URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } catch let error {
            print(error.localizedDescription)

            DispatchQueue.main.async {
                self.dispatchGroup.leave()
                SVProgressHUD.dismiss()
            }


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Confirm Request Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()


                        }

                    }
                    else {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }


    // MARK:- // Navigate

    func navigate() {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "ThankYouViewController") as! ThankYouViewController

        self.navigationController?.pushViewController(navigate, animated: true)

    }


    // MARK:- // Fill Data

    func fillData() {

        self.yourAccountBalance.text = "\((self.allDataDictionary["info_array"] as! NSDictionary)["user_wallet_amount"] ?? "")"
        self.totalExtraFees.text = "\((self.allDataDictionary["info_array"] as! NSDictionary)["total_extra_fees"] ?? "")"
        self.remainingBalance.text = "\((self.allDataDictionary["info_array"] as! NSDictionary)["remaining_balance"] ?? "")"

    }




    // MARK:- // Set Font

    func set_font()
    {

        yourAccountBalanceLBL.font = UIFont(name: yourAccountBalanceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 20)))
        yourAccountBalance.font = UIFont(name: yourAccountBalance.font.fontName, size: CGFloat(Get_fontSize(size: 30)))
        totalExtraFeesLBL.font = UIFont(name: totalExtraFeesLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        totalExtraFees.font = UIFont(name: totalExtraFees.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        remainingBalanceLBL.font = UIFont(name: remainingBalanceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        remainingBalance.font = UIFont(name: remainingBalance.font.fontName, size: CGFloat(Get_fontSize(size: 15)))


    }


    // MARK:- // Set Static Strings

    func setStaticStrings()
    {
        self.yourAccountBalanceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "your_account_balance", comment: "")
        self.totalExtraFeesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalExtraFees", comment: "")
        self.remainingBalanceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "remaining_balance", comment: "")
    }




}
