//
//  PostAdDetailsFeaturesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 01/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class PostAdDetailsFeaturesTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var featureNameLBL: UILabel!
    @IBOutlet weak var featureNameTXT: UITextField!
    @IBOutlet weak var featurePriceLBL: UILabel!
    @IBOutlet weak var featurePriceTXT: UITextField!
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var deleteButton: UIButton!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
