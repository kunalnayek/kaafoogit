//
//  BasicInfoViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 18/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD



class BasicInfoViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{



    // MARK:- // Next Button
    
     let userID = UserDefaults.standard.string(forKey: "userID")
    
    
    var titleLBL = UILabel()
    var textField = UITextField()
    var fromTXT = UITextField()
    var toTXT = UITextField()
     var refundableDaysTXT = UITextField()
     var containerView = UIView()
    var isNewAdd: Bool!
    
     var productnametextField = UITextField()

    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButtonTap(_ sender: UIButton) {

        print("heldValuesArray--------",self.heldValuesArray)
        
        self.heldValuesArray.removeAllObjects()





        self.holdValuesWhenAPIFires(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
        self.holdValuesWhenAPIFires(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")


        UserDefaults.standard.setValue(self.heldValuesArray, forKey: "heldValuesArray")


        self.params = ""
        self.submitTextfieldArray.removeAllObjects()
        self.submitListViewArray.removeAllObjects()
        self.submitCheckboxArray.removeAllObjects()

        self.checkEmptySections {

            if self.redTitleLBLArrray.count > 0
            {
               
                
                
                for i in 0..<self.redTitleLBLArrray.count
                {
                    self.redTitleLBLArrray[i].textColor = UIColor.red
                }
                
                self.redTitleLBLArrray = [UILabel]()
                
                //print("titlelbl------",self.titleLBL.text)
                
                self.textField.text = " "
               
                //self.getData()
                
                

                self.customAlert(title: "Empty Fields!", message: "There are one or more mandatory fields,that needs to be filled.", completion: {
                    print("Empty Fields")
                    
                UserDefaults.standard.set(false, forKey: "isFillbasicInfo")
                    
                    
                })

            }
            else
            {
                 print("redTitleLBLArrray--------",self.redTitleLBLArrray)
                
                self.saveData(check: false)

                self.sendAllInformation()

                self.dispatchGroup.notify(queue: .main) {

                    if self.topCatID.elementsEqual("13")
                    {
                        UserDefaults.standard.setValue(self.catID, forKey: "catID")
                        UserDefaults.standard.setValue(true, forKey: "autoLoad")

                        NotificationCenter.default.post(name: .passDataAddPost, object: "noExtra")
                    }
                    else if self.topCatID.elementsEqual("16")
                    {
                        UserDefaults.standard.setValue(self.catID, forKey: "catID")
                        UserDefaults.standard.setValue(true, forKey: "autoLoad")

                        NotificationCenter.default.post(name: .passDataAddPost, object: "noExtra")
                    }
                    else if self.topCatID.elementsEqual("11")
                    {
                        UserDefaults.standard.setValue(self.catID, forKey: "catID")
                        UserDefaults.standard.setValue(true, forKey: "autoLoad")

                        NotificationCenter.default.post(name: .passDataAddPost, object: "noExtra")
                    }
                    else
                    {
                        UserDefaults.standard.setValue(false, forKey: "autoLoad")

                        NotificationCenter.default.post(name: .passDataAddPost, object: "0")
                    }
                }
                
                UserDefaults.standard.set(true, forKey: "isFillbasicInfo")//isFillbasicInfo
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    NotificationCenter.default.post(name: Notification.Name("tapNextButton"), object: nil, userInfo: ["indx":1])
                })
                 
                
            }
            
           

        }


        /*
         self.dispatchGroup.notify(queue: .main) {

         print(self.params!)

         self.sendAllInformation()

         self.submitArraysDictionary = ["params":self.params! , "txtArray":self.submitTextfieldArray , "listArray":self.submitListViewArray , "checkArray":self.submitCheckboxArray , "radioArray":self.submitRadioButtonArray] as [String : Any]


         //            UserDefaults.standard.set(self.params, forKey: "postAddParams")
         UserDefaults.standard.setValue(self.submitArraysDictionary, forKey: "postAddDict")
         UserDefaults.standard.synchronize()

         }
         */

    }



    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var scrollContentviewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var allCategoriesView: UIView!
    @IBOutlet weak var allCategoriesviewHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var otherInfoView: UIView!
    @IBOutlet weak var otherInfoViewHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var otherSecondInfoView: UIView!
    @IBOutlet weak var otherSecondInfoViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var sampleLBL: UILabel!

    @IBOutlet weak var viewOfPickerview: UIView!
    @IBOutlet weak var pagePickerview: UIPickerView!


    @IBOutlet weak var viewOfPickerTopConstraint: NSLayoutConstraint!



    var submitArraysDictionary = [String : Any]()

    var submitTextfieldArray = NSMutableArray()//[[String : String]]()
    var submitListViewArray = NSMutableArray()//[[String : String]]()
    var submitRadioButtonArray = NSMutableArray()//[[String : String]]()
    var submitCheckboxArray = NSMutableArray() //[[String : AnyObject]]()



    var params : String! = ""


    var categoryOpen : Int!


    var tapGesture = UITapGestureRecognizer()

    let dispatchGroup = DispatchGroup()

    var allDataDictionary : NSDictionary!

    var topCatID : String!

    var parentID : String!

    var nextLabel : String!

    var catID : String!

    var productType : String! = ""

    var tabletag : Int! = 0

    private var datepickerWithDate = UIDatePicker()

    var productRefundable : String! = "0"


    // Basic Info ViewController

    var stateArray = [NSDictionary]()
    var cityArray = [NSDictionary]()
    var countryID : String!
    var stateID : String!
    var cityID : String!



    var categoryViewArray = [UIView]()
    var viewOfTableviewArray = [UIView]()
    var tableviewArray = [UITableView]()
    var allCategoriesDataArray = [NSArray]()
    var categoryLBLArray = [UILabel]()

    var otherInfoSubviews = [UIView]()
    var otherSecondInfoSubviews = [UIView]()

    public var otherInfoViewDataArray : NSArray!
    var otherInfoTextfieldArray = [UITextField]()

    public var otherSecondInfoViewDataArray : NSArray!
    var otherSecondInfoTextfieldArray = [UITextField]()


    var allCategoriesViewHC : CGFloat = 0
    var otherInfoViewHC : CGFloat = 0
    var otherSecondInfoViewHC : CGFloat! = 0

    var langSuffix : String!


    var otherInfoListViewDataArray = [NSDictionary]()
    var otherSecondInfoListViewDataArray = [NSDictionary]()
    var pickerViewDataArray = [NSDictionary]()
    var otherInfolistViewLblArray = [UILabel]()
    var otherSecondInfolistViewLblArray = [UILabel]()
    var listingDurationTextfieldArray = [UITextField]()

    var inSection : String! = ""
    var listClicked : Int! = 0

    var otherInfoRadioButtonArray = [[UIButton]]()
    var otherSecondInfoRadioButtonArray = [[UIButton]]()

    var otherInfoRadioButtonDataArray = [NSArray]()
    var otherSecondInfoRadioButtonDataArray = [NSArray]()


    var otherInfoCheckboxArray = [[UIButton]]()
    var otherSecondInfoCheckboxArray = [[UIButton]]()

    var otherInfoCheckboxDataArray = [NSArray]()
    var otherSecondInfoCheckboxDataArray = [NSArray]()


    var stepFinal : String!

    /////////////////////////


    var autoLoad : Bool! = false


    override func viewDidLoad() {
        super.viewDidLoad()
        let temp = (UserDefaults.standard.string(forKey: "langID"))?.lowercased()
        self.langSuffix = "_\(temp!)"
        print("catid------",catID)
        if UserDefaults.standard.bool(forKey: "isFillbasicInfo")  != true{
            UserDefaults.standard.set(false, forKey: "autoLoad")
            //UserDefaults.standard.set(false,forKey: "isFillbasicInfo")
            self.isNewAdd = false
        }
        else{
            UserDefaults.standard.set(true, forKey: "autoLoad")
        }
        
        self.viewOfPickerTopConstraint.constant = self.FullHeight

        self.autoLoad = UserDefaults.standard.bool(forKey: "autoLoad")

        if self.autoLoad == true
        {
            let userdefaultCatID = UserDefaults.standard.string(forKey: "catID")
            self.autoLoadPage(finalCatID: userdefaultCatID ?? "")
        }
        else
        {
            
            if userID?.isEmpty == true
                   {
                    
                    print("Not Login")
                    
                    
//                       let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""), preferredStyle: .alert)
//
//                       let ok = UIAlertAction(title: "Ok", style: .default, handler: {
//                           UIAlertAction in
//
//                                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
//
//                               self.navigationController?.pushViewController(navigate, animated: true)
//                       })
//                       alert.addAction(ok)
//
//                       self.present(alert, animated: true, completion: nil)
                    

                   }
                   else
                   {
                       

            self.getData()

            self.dispatchGroup.notify(queue: .main) {

                self.otherInfoView.isHidden = true
                self.otherSecondInfoView.isHidden = true

                if (self.allDataDictionary["category_info"] as! NSArray).count > 0
                {
                    self.allCategoriesDataArray.append(self.allDataDictionary["category_info"] as! NSArray)

                    self.tableviewArray.removeAll()

                    self.createViewWithTableview(yOrigin: CGFloat(self.allCategoriesDataArray.count - 1) * (60/568)*self.FullHeight, title: "Select Category", tableTag: self.tabletag)

                    self.allCategoriesviewHeightConstraint.constant = self.allCategoriesViewHC

                    for i in 0..<self.allCategoriesDataArray.count
                    {
                        self.tableviewArray[i].delegate = self
                        self.tableviewArray[i].dataSource = self
                        self.tableviewArray[i].reloadData()
                    }

                    //UserDefaults.standard.setValue(false, forKey: "autoLoad")



                }

            }
        }

        }

    }




    // MARK: - // JSON POST Method to get Listing Data

    func getData()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_post_category")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
       

        if (langID?.elementsEqual("EN"))!
        {
            self.langSuffix = "_en"
        }
        
        //top_catid = 15 dailyRental
        
        

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&parent_category=\(parentID ?? "")&top_catid=\(topCatID ?? "")&next_label=\(nextLabel ?? "")&product_type=\(productType ?? "")&catid=\(catID ?? "")"


        print("Get Data URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("First Category List Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json.copy() as? NSDictionary
                        
                        

                        self.otherInfoViewDataArray = json["other_info"] as? NSArray

                        self.otherSecondInfoViewDataArray = json["other_second_info"] as? NSArray
                    }

                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }


                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }



    // MARK: - // JSON POST Method to get Country State City List

    func countryStateCity()

    {

        self.dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_country_region_city")!       //change the url

        print("Country State City URL : ", url)

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")

        parameters = "lang_id=\(langID!)&mycountry_id=\(countryID ?? "")&region_id=\(stateID ?? "")&city_id="

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Country State City Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        if self.countryID.isEmpty
                        {

                            for i in 0..<(json["info_array"] as! NSArray).count
                            {
                                self.cityArray.append((json["info_array"] as! NSArray)[i] as! NSDictionary)
                            }


                        }
                        else
                        {

                            for i in 0..<(json["info_array"] as! NSArray).count
                            {
                                self.stateArray.append((json["info_array"] as! NSArray)[i] as! NSDictionary)
                            }
                        }

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }










    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return (self.allCategoriesDataArray[tableView.tag]).count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell")

        if self.autoLoad == true
        {
            
            print("categoryname-----",(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname\(langSuffix!)"] ?? ""))
            
            
            cell?.textLabel?.text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname\(langSuffix!)"] ?? "")"
            
             print("celltextlabel------",cell?.textLabel?.text)
            
            
        }
        else
        {
            //print("categoryname-----",(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname\(langSuffix!)"] ?? ""))
            
            cell?.textLabel?.text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname"] ?? "")"
            
            print("celltextlabel------",cell?.textLabel?.text)
            
            
        }


        //
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none

        //
        cell?.textLabel!.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))

        return cell!

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return (20/568)*self.FullHeight

    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        //let vc = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "listItemVC") as! ListItemViewController

        self.heldValuesArray.removeAllObjects()

        self.holdValuesWhenAPIFires(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
        self.holdValuesWhenAPIFires(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")


        var tempArray = [Int]()
        for view in self.allCategoriesView.subviews
        {
            if view.tag > tableView.tag
            {
                view.removeFromSuperview()

                tempArray.append(view.tag)
            }
        }

        self.allCategoriesDataArray.remove(at: tempArray)
        self.categoryViewArray.remove(at: tempArray)
        self.viewOfTableviewArray.remove(at: tempArray)
        self.tableviewArray.remove(at: tempArray)
        self.categoryLBLArray.remove(at: tempArray)

        self.otherInfoSubviews.removeAll()
        self.otherSecondInfoSubviews.removeAll()

        self.tabletag = tableView.tag + 1


        if tableView.tag == 0
        {
            self.topCatID = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["id"] ?? "")"
            self.parentID = self.topCatID
            self.catID = self.topCatID

            self.categoryLBLArray[0].text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname"] ?? "")"
        }
        else if tableView.tag == 1
        {
            self.parentID = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["id"] ?? "")"
            self.nextLabel = self.parentID
            self.catID = "\(self.topCatID!),\(self.parentID!)"

            self.categoryLBLArray[1].text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname"] ?? "")"
        }
        else if tableView.tag == 2
        {
            self.parentID = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["id"] ?? "")"
            self.catID = "\(self.topCatID!),\(self.nextLabel!),\(self.parentID!)"

            self.categoryLBLArray[2].text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname"] ?? "")"
        }
        else
        {
            self.parentID = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["id"] ?? "")"
            self.catID = "\(self.catID!),\(self.parentID!)"

            self.categoryLBLArray[tableView.tag].text = "\(((self.allCategoriesDataArray[tableView.tag])[indexPath.row] as! NSDictionary)["categoryname"] ?? "")"
        }

        self.getData()

        self.dispatchGroup.notify(queue: .main) {


            self.categoryViewArray[tableView.tag].frame.size.height = self.categoryViewArray[tableView.tag].frame.size.height - (150/568)*self.FullHeight

            self.viewOfTableviewArray[tableView.tag].frame.size.height = 0

            self.tableviewArray[tableView.tag].frame.size.height = 0

            self.viewOfTableviewArray[tableView.tag].isHidden = true

            if (self.allDataDictionary["category_info"] as! NSArray).count > 0
            {

                //
                self.allCategoriesDataArray.append(self.allDataDictionary["category_info"] as! NSArray)

                //self.tabletag = self.tabletag + 1

                self.createViewWithTableview(yOrigin: CGFloat(self.allCategoriesDataArray.count - 1) * (60/568)*self.FullHeight, title: "Select Category", tableTag: self.tabletag)


                for i in 0..<self.allCategoriesDataArray.count
                {
                    self.tableviewArray[i].delegate = self
                    self.tableviewArray[i].dataSource = self
                    self.tableviewArray[i].reloadData()
                }
            }

            self.allCategoriesviewHeightConstraint.constant = CGFloat(self.allCategoriesDataArray.count) * (60/568)*self.FullHeight




            // Creating Other Info View
            if self.otherInfoViewDataArray.count > 0
            {


                self.otherInfoView.isHidden = false

                for view in self.otherInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherInfoView(dataArray: self.otherInfoViewDataArray)
            }
            else
            {
                self.otherInfoView.isHidden = true
            }




            //Creating Other Second Info View
            if self.otherSecondInfoViewDataArray.count > 0
            {
                self.otherSecondInfoView.isHidden = false

                for view in self.otherSecondInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherSecondInfoView(dataArray: self.otherSecondInfoViewDataArray)
            }
            else
            {
                self.otherSecondInfoView.isHidden = true
            }







            self.putValuesAfterAPIFire(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            self.putValuesAfterAPIFire(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")




        }

    }






    // MARK:- // Pickerview Delegates

    func numberOfComponents(in pickerView: UIPickerView) -> Int {

        return 1

    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if self.inSection.elementsEqual("otherInfo")
        {
            if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("State")
            {
                return stateArray.count
            }
            else if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("City")
            {
                return cityArray.count
            }
            else
            {
                if ((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray).count == 0{
//                    self.pagePickerview.isHidden = true
//                    self.viewOfPickerview.isHidden = true
                    self.viewOfPickerTopConstraint.constant = self.FullHeight
                    return 0
                }
                return ((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray).count
            }

        }
        else
        {
            return ((self.pickerViewDataArray[listClicked])["opt_value"] as! NSArray).count
        }

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if self.inSection.elementsEqual("otherInfo")
        {
            if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("State")
            {
                return "\((stateArray[row])["region_name"] ?? "")"
            }
            else if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("City")
            {
                return "\((cityArray[row])["city_name"] ?? "")"
            }
            else
            {
                return "\((((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray)[row] as! NSDictionary)["value"] ?? "")"
            }



        }
        else
        {
            return "\((((self.pickerViewDataArray[listClicked])["opt_value"] as! NSArray)[row] as! NSDictionary)["option_value\(langSuffix!)"] ?? "")"
        }

    }


    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if self.inSection.elementsEqual("otherInfo")
        {

            let x : String!

            if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("Country")
            {
                x = "\((((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray)[row] as! NSDictionary)["value"] ?? "")"

                self.countryID = "\((((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray)[row] as! NSDictionary)["id"] ?? "")"

                self.stateID = ""
                self.cityID = ""

                self.stateArray.removeAll()
                self.cityArray.removeAll()

                self.countryStateCity()

                self.otherInfolistViewLblArray[listClicked + 1].text = "Select State"


                self.otherInfolistViewLblArray[listClicked + 2].text = "Select City"


            }
            else if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("State")
            {
                x = "\((stateArray[row])["region_name"] ?? "")"

                self.stateID = "\((stateArray[row])["region_id"] ?? "")"

                self.countryID = ""
                self.cityID = ""

                self.cityArray.removeAll()

                self.countryStateCity()

                self.otherInfolistViewLblArray[listClicked + 1].text = "Select City"
            }
            else if "\((self.pickerViewDataArray[listClicked])["field_name"] ?? "")".elementsEqual("City")
            {
                x = "\((cityArray[row])["city_name"] ?? "")"

                self.cityID = "\((cityArray[row])["city_id"] ?? "")"


            }
            else
            {
                x = "\((((self.pickerViewDataArray[listClicked])["field_value"] as! NSArray)[row] as! NSDictionary)["value"] ?? "")"
            }

            self.otherInfolistViewLblArray[listClicked].text = x
        }
        else
        {
            self.otherSecondInfolistViewLblArray[listClicked].text = "\((((self.pickerViewDataArray[listClicked])["opt_value"] as! NSArray)[row] as! NSDictionary)["option_value\(langSuffix!)"] ?? "")"
        }

        UIView.animate(withDuration: 0.5)
        {
            self.viewOfPickerTopConstraint.constant = self.FullHeight
        }

    }




    // MARK:- // Textfield Delegates

    func textFieldDidEndEditing(_ textField: UITextField) {

        let dateFormat = DateFormatter()

        if (textField.restorationIdentifier?.elementsEqual("calendar"))!
        {
            dateFormat.dateFormat = "dd-MM-yyyy"
            textField.text = dateFormat.string(from: (self.datepickerWithDate.date))

        }


    }












    // MARK:- // Create View with Tableview

    func createViewWithTableview(yOrigin : CGFloat , title : String , tableTag : Int)
    {
        let categoryView = UIView(frame: CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: (60/568)*self.FullHeight))

        categoryView.tag = tabletag
        categoryViewArray.append(categoryView)


        let titleLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (self.FullWidth - (90/320)*self.FullWidth), height: (20/568)*self.FullHeight))
        
        //titleLBL.backgroundColor = .red

        titleLBL.accessibilityIdentifier = "titleLBL"

        let categoryLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (self.FullWidth - (90/320)*self.FullWidth), height: (30/568)*self.FullHeight))
        self.categoryLBLArray.append(categoryLBL)

        let dropdownImageview = UIImageView(frame: CGRect(x: (265/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (15/320)*self.FullWidth, height: (15/568)*self.FullHeight))

        dropdownImageview.contentMode = .scaleAspectFit


        let dropDownButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.FullWidth - (40/320)*self.FullWidth, height: (50/568)*self.FullHeight))

        dropDownButton.tag = tableTag
        dropDownButton.addTarget(self, action: #selector(clickOnDropDown), for: .touchUpInside)


        let viewOfTableview = UIView(frame: CGRect(x: (20/320)*self.FullWidth, y: yOrigin, width: self.FullWidth - (90/320)*self.FullWidth, height: 0))

        viewOfTableview.isHidden = true


        viewOfTableviewArray.append(viewOfTableview)
        //viewOfTableview.tag = tableTag


        // initiating drop down tableview
        let dropDownTableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.FullWidth - (90/320)*self.FullWidth, height: 0))

        dropDownTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableCell")
        //dropDownTableView.isHidden = true
        dropDownTableView.tag = tableTag
        tableviewArray.append(dropDownTableView)

        dropDownTableView.isUserInteractionEnabled = true
        dropDownTableView.isScrollEnabled = true
        dropDownTableView.backgroundColor = UIColor.clear
        dropDownTableView.separatorStyle = .none


        titleLBL.attributedText = asterixText(withText: title)

        categoryLBL.text = ""

        dropdownImageview.image = UIImage(named: "dropDownImage")


        self.allCategoriesView.addSubview(categoryView)
        categoryView.backgroundColor = .red
        categoryView.addSubview(titleLBL)
        categoryView.addSubview(categoryLBL)
        categoryView.addSubview(dropdownImageview)
        categoryView.addSubview(dropDownButton)
        self.allCategoriesView.addSubview(viewOfTableview)
        viewOfTableview.addSubview(dropDownTableView)

        allCategoriesViewHC = categoryView.frame.origin.y + categoryView.frame.size.height


        categoryView.backgroundColor = UIColor.white
        viewOfTableview.backgroundColor = UIColor.white

        titleLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        categoryLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))

    }



    // MARK:- // Function defining Drop down click Action

    @objc func clickOnDropDown(sender: UIButton!)
    {

        print("tag---",sender.tag)

        self.categoryViewArray[sender.tag].autoresizesSubviews = false

        if viewOfTableviewArray[sender.tag].isHidden == true
        {
            self.categoryViewArray[sender.tag].frame.size.height = self.categoryViewArray[sender.tag].frame.size.height + (150/568)*self.FullHeight


            for i in 0..<viewOfTableviewArray.count
            {
                self.viewOfTableviewArray[i].frame.size.height = 0

                self.tableviewArray[i].frame.size.height = 0

                viewOfTableviewArray[i].isHidden = true
            }

            self.viewOfTableviewArray[sender.tag].frame.size.height = (150/568)*self.FullHeight

            self.tableviewArray[sender.tag].frame.size.height = (150/568)*self.FullHeight

            viewOfTableviewArray[sender.tag].isHidden = false

            viewOfTableviewArray[sender.tag].dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

            self.allCategoriesView.bringSubviewToFront(viewOfTableviewArray[sender.tag])

            allCategoriesViewHC = ((60/568)*self.FullHeight) * CGFloat(categoryViewArray.count) + (150/568)*self.FullHeight

            self.allCategoriesviewHeightConstraint.constant = self.allCategoriesViewHC

        }
        else
        {
            self.categoryViewArray[sender.tag].frame.size.height = self.categoryViewArray[sender.tag].frame.size.height - (150/568)*self.FullHeight

            self.viewOfTableviewArray[sender.tag].frame.size.height = 0

            self.tableviewArray[sender.tag].frame.size.height = 0

            viewOfTableviewArray[sender.tag].isHidden = true

            allCategoriesViewHC = ((60/568)*self.FullHeight) * CGFloat(categoryViewArray.count)

            self.allCategoriesviewHeightConstraint.constant = self.allCategoriesViewHC
        }

        self.categoryViewArray[sender.tag].autoresizesSubviews = true




        self.allCategoriesviewHeightConstraint.constant = allCategoriesViewHC
        //


        /*
         let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideView))

         //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
         tap.cancelsTouchesInView = false

         //self.allCategoriesView.backgroundColor = UIColor.red
         //self.allCategoriesView.addGestureRecognizer(tap)
         //        self.otherInfoView.addGestureRecognizer(tap)
         //        self.otherSecondInfoView.addGestureRecognizer(tap)

         self.categoryOpen = sender.tag

         self.allCategoriesView.bringSubview(toFront: self.viewOfTableviewArray[sender.tag])
         */

    }


    @objc func hideView() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.categoryViewArray[categoryOpen].autoresizesSubviews = false

        self.categoryViewArray[categoryOpen].frame.size.height = self.categoryViewArray[categoryOpen].frame.size.height - (150/568)*self.FullHeight

        self.viewOfTableviewArray[categoryOpen].frame.size.height = 0

        self.tableviewArray[categoryOpen].frame.size.height = 0

        viewOfTableviewArray[categoryOpen].isHidden = true

        allCategoriesViewHC = ((60/568)*self.FullHeight) * CGFloat(categoryViewArray.count)

        self.allCategoriesviewHeightConstraint.constant = self.allCategoriesViewHC

        self.categoryViewArray[categoryOpen].autoresizesSubviews = true

        self.allCategoriesviewHeightConstraint.constant = allCategoriesViewHC



    }


    // MARK:- // Create Other Info View

    func createOtherInfoView(dataArray : NSArray)
    {
        self.otherInfoTextfieldArray.removeAll()

        self.otherInfolistViewLblArray.removeAll()

        var listViewTag : Int = 0
        self.otherInfoListViewDataArray.removeAll()

        var radioButtonTag : Int = 0
        self.otherInfoRadioButtonArray.removeAll()

        var checkBoxTag : Int = 0
        self.otherInfoCheckboxArray.removeAll()

        var yOrigin : CGFloat! = (20/568)*self.FullHeight

        for i in 0..<dataArray.count
        {
            //View with Textfield
            if "\((dataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("T")
            {
                self.createViewWithTextfield(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherInfoView, section: "otherInfo")

                yOrigin = yOrigin + (60/568)*self.FullHeight
            }


            //View with List
            if "\((dataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("S")
            {
                self.createListTypeViews(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherInfoView, section: "otherInfo", tag: listViewTag)

                if "\((dataArray[i] as! NSDictionary)["calender_show"] ?? "")".elementsEqual("1")
                {
                    yOrigin = yOrigin + (205/568)*self.FullHeight
                }
                else
                {
                    yOrigin = yOrigin + (60/568)*self.FullHeight
                }


                self.otherInfoListViewDataArray.append((dataArray[i] as! NSDictionary))

                listViewTag = listViewTag + 1
            }


            //View with Radio Button
            if "\((dataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("R")
            {
                self.createViewWithRadioButton(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherInfoView, section: "otherInfo", tag: radioButtonTag)

                let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(((dataArray[i] as! NSDictionary)["field_value"] as! NSArray).count)) + (10/568)*self.FullHeight

                yOrigin = yOrigin + tempHeight

                self.otherInfoRadioButtonDataArray.append((dataArray[i] as! NSDictionary)["field_value"] as! NSArray)

                radioButtonTag = radioButtonTag + 1

            }




            //View with Check Box
            if "\((dataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("C")
            {
                self.createViewWithCheckboxes(data: dataArray[i] as! NSDictionary, yOrigin: yOrigin, addToView: self.otherInfoView, section: "otherInfo", tag: checkBoxTag)

                let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray).count)) + (10/568)*self.FullHeight

                yOrigin = yOrigin + tempHeight

                self.otherInfoCheckboxDataArray.append((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray)

                checkBoxTag = checkBoxTag + 1

            }

        }

        self.otherInfoViewHC = yOrigin

        self.otherInfoViewHeightConstraint.constant = self.otherInfoViewHC
    }




    // MARK:- // Create Other Second Info View

    func createOtherSecondInfoView(dataArray : NSArray)
    {

        self.otherSecondInfoTextfieldArray.removeAll()

        self.otherSecondInfolistViewLblArray.removeAll()

        var listViewTag : Int! = 0
        self.otherSecondInfoListViewDataArray.removeAll()

        var radioButtonTag : Int = 0
        self.otherSecondInfoRadioButtonArray.removeAll()

        var checkBoxTag : Int = 0
        self.otherSecondInfoCheckboxArray.removeAll()


        var yOrigin : CGFloat! = (20/568)*self.FullHeight

        for i in 0..<dataArray.count
        {
            //View with Textfield
            if "\((dataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("T")
            {
                self.createViewWithTextfield(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherSecondInfoView, section: "otherSecondInfo")

                yOrigin = yOrigin + (60/568)*self.FullHeight
            }


            //View with List
            if "\((dataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("S")
            {
                self.createListTypeViews(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherSecondInfoView, section: "otherSecondInfo", tag: listViewTag)

                yOrigin = yOrigin + (60/568)*self.FullHeight

                self.otherSecondInfoListViewDataArray.append((dataArray[i] as! NSDictionary))

                listViewTag = listViewTag + 1
            }



            //View with Radio Button
            if "\((dataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("R")
            {
                self.createViewWithRadioButton(data: (dataArray[i] as! NSDictionary), yOrigin: yOrigin, addToView: self.otherSecondInfoView, section: "otherSecondInfo", tag: radioButtonTag)

                let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray).count)) + (10/568)*self.FullHeight

                yOrigin = yOrigin + tempHeight

                self.otherSecondInfoRadioButtonDataArray.append((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray)

                radioButtonTag = radioButtonTag + 1

            }



            //View with Check Box
            if "\((dataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("C")
            {
                self.createViewWithCheckboxes(data: dataArray[i] as! NSDictionary, yOrigin: yOrigin, addToView: self.otherSecondInfoView, section: "otherSecondInfo", tag: checkBoxTag)

                let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray).count)) + (10/568)*self.FullHeight

                yOrigin = yOrigin + tempHeight

                self.otherSecondInfoCheckboxDataArray.append((dataArray[i] as! NSDictionary)["opt_value"] as! NSArray)

                checkBoxTag = checkBoxTag + 1

            }

        }

        self.otherSecondInfoViewHC = yOrigin

        self.otherSecondInfoViewHeightConstraint.constant = self.otherSecondInfoViewHC
    }





    // MARK:- // Create View with TextField

    func createViewWithTextfield(data: NSDictionary,yOrigin : CGFloat,addToView : UIView, section: String)
    {
        containerView = UIView(frame: CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: (60/568)*self.FullHeight))

        titleLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (self.FullWidth - (40/320)*self.FullWidth), height: (20/568)*self.FullHeight))
       

        textField = UITextField(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (self.FullWidth - (40/320)*self.FullWidth), height: (30/568)*self.FullHeight))
        
        if UserDefaults.standard.string(forKey: "langID") == "AR"{
            textField.textAlignment = .right
        }
        
        else
        {
            textField.textAlignment = .left
        }
        

        if section.elementsEqual("otherInfo")
        {
            print("otherInfoTextfieldArray----",  self.otherInfoTextfieldArray)
            
            self.otherInfoTextfieldArray.append(textField)
        }
        else
        {
             print("otherSecondInfoTextfieldArray----",  self.otherSecondInfoTextfieldArray)
            
            self.otherSecondInfoTextfieldArray.append(textField)
        }


        if section.elementsEqual("otherInfo")
        {
            if "\(data["mandatory_field"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["field_name"] ?? "")")
                
            }
            else
            {
                titleLBL.text = "\(data["field_name"] ?? "")"
            }
            
            print("textfield-------",textField)

            textField.placeholder = "Enter \(data["field_name"] ?? "")"
            
            //textField.backgroundColor = .green
           
        }
        else
        {
            if "\(data["mandatory"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["option_name\(langSuffix ?? "")"] ?? "")")
                
                
            }
            else
            {
                titleLBL.text = "\(data["option_name\(langSuffix ?? "")"] ?? "")"
            }
            
            print("textfield-------",textField)


            textField.placeholder = "Enter \(data["option_name\(langSuffix ?? "")"] ?? "")"
            
             // textField.backgroundColor = .blue
           
        }


        containerView.addSubview(titleLBL)
        containerView.addSubview(textField)
        addToView.addSubview(containerView)

        containerView.backgroundColor = UIColor.white

        titleLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        textField.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))

        //self.otherInfoViewHC = self.otherInfoViewHC + (60/568)*self.FullHeight

        if section.elementsEqual("otherInfo")
        {
            textField.restorationIdentifier = "\(data["field_name"] ?? "")"

            self.otherInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["field_name"] ?? "")"
        }
        else
        {
            textField.restorationIdentifier = "\(data["option_name\(langSuffix ?? "")"] ?? "")"

            self.otherSecondInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["option_name\(langSuffix ?? "")"] ?? "")"
        }

    }





    // MARK:- // Create List Type Views

    func createListTypeViews(data: NSDictionary,yOrigin : CGFloat,addToView : UIView, section: String,tag : Int)
    {

        let containerView = UIView()

        let titleLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (self.FullWidth - (90/320)*self.FullWidth), height: (20/568)*self.FullHeight))

        let optionLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: (self.FullWidth - (90/320)*self.FullWidth), height: (20/568)*self.FullHeight))

        if section.elementsEqual("otherInfo")
        {
            self.otherInfolistViewLblArray.append(optionLBL)
        }
        else
        {
            self.otherSecondInfolistViewLblArray.append(optionLBL)
        }


        let dropdownImageview = UIImageView(frame: CGRect(x: (265/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (15/320)*self.FullWidth, height: (15/568)*self.FullHeight))

        dropdownImageview.contentMode = .scaleAspectFit

        dropdownImageview.image = UIImage(named: "dropDownImage")


        let dropDownButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.FullWidth - (40/320)*self.FullWidth, height: (50/568)*self.FullHeight))

        dropDownButton.tag = tag
        dropDownButton.addTarget(self, action: #selector(openPicker), for: .touchUpInside)


        if section.elementsEqual("otherInfo")
        {
            if "\(data["mandatory_field"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["field_name"] ?? "")")
                 
            }
            else
            {
                titleLBL.text = "\(data["field_name"] ?? "")"
                  
            }

            optionLBL.text = "Select \(data["field_name"] ?? "")"
           

            dropDownButton.restorationIdentifier = "otherInfo"

        }
        else
        {
            if "\(data["mandatory"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["option_name\(langSuffix ?? "")"] ?? "")")
            }
            else
            {
                titleLBL.text = "\(data["option_name\(langSuffix ?? "")"] ?? "")"
                 
            }

            optionLBL.text = "Select \(data["option_name\(langSuffix ?? "")"] ?? "")"
             
            dropDownButton.restorationIdentifier = "otherSecondInfo"

        }


        containerView.addSubview(titleLBL)
        containerView.addSubview(optionLBL)
        containerView.addSubview(dropdownImageview)
        containerView.addSubview(dropDownButton)
        addToView.addSubview(containerView)

        containerView.backgroundColor = UIColor.white

        titleLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        optionLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))


        if section.elementsEqual("otherInfo")
        {
            optionLBL.restorationIdentifier = "\(data["field_name"] ?? "")"


            self.otherInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["field_name"] ?? "")"
        }
        else
        {
            optionLBL.restorationIdentifier = "\(data["option_name\(langSuffix ?? "")"] ?? "")"


            self.otherSecondInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["option_name\(langSuffix ?? "")"] ?? "")"
        }

        titleLBL.restorationIdentifier = "titleLBL"





        //Calendar Show View

        let calendarShowView = UIView(frame: CGRect(x: 0, y: (60/568)*self.FullHeight, width: self.FullWidth, height: (145/568)*self.FullHeight))

        let orLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        orLBL.text = "OR"

        let fromLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (30/568)*self.FullHeight, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        fromLBL.text = "FROM"

        fromTXT = UITextField(frame: CGRect(x: (20/320)*self.FullWidth, y: (50/568)*self.FullHeight, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        
        if UserDefaults.standard.string(forKey: "langID") == "AR"{
            fromTXT.textAlignment = .right
        }
        
        else
        {
            fromTXT.textAlignment = .left
        }
        
        fromTXT.placeholder = "From Date"
        fromTXT.restorationIdentifier = "calendar"
        fromTXT.delegate = self
        fromTXT.inputView = datepickerWithDate
       
        self.listingDurationTextfieldArray.append(fromTXT)
        
        

        let toLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: (85/568)*self.FullHeight, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        toLBL.text = "TO"

        toTXT = UITextField(frame: CGRect(x: (20/320)*self.FullWidth, y: (105/568)*self.FullHeight, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
        
        if UserDefaults.standard.string(forKey: "langID") == "AR"
        {
            toTXT.textAlignment = .right
        }
               
        else
        {
            toTXT.textAlignment = .left
               
        }
        
        toTXT.placeholder = "To Date"
        toTXT.restorationIdentifier = "calendar"
        toTXT.delegate = self
        toTXT.inputView = datepickerWithDate
        self.listingDurationTextfieldArray.append(toTXT)

        datepickerWithDate.datePickerMode = .date



        orLBL.font = UIFont(name: self.sampleLBL!.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        fromLBL.font = UIFont(name: self.sampleLBL!.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        fromTXT.font = UIFont(name: self.sampleLBL!.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        toLBL.font = UIFont(name: self.sampleLBL!.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        toTXT.font = UIFont(name: self.sampleLBL!.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        calendarShowView.addSubview(orLBL)
        calendarShowView.addSubview(fromLBL)
        calendarShowView.addSubview(fromTXT)
        calendarShowView.addSubview(toLBL)
        calendarShowView.addSubview(toTXT)
        containerView.addSubview(calendarShowView)


        if "\(data["calender_show"] ?? "")".elementsEqual("1")
        {
            containerView.frame = CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: (205/568)*self.FullHeight)

            calendarShowView.isHidden = false
        }
        else
        {
            containerView.frame = CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: (60/568)*self.FullHeight)

            calendarShowView.isHidden = true
        }


    }




    // MARK:- // Create View with Radio Button

    func createViewWithRadioButton(data: NSDictionary,yOrigin : CGFloat,addToView : UIView, section: String,tag : Int)
    {
        var tempButtonArray = [NSDictionary]()

        if section.elementsEqual("otherInfo")
        {
            for i in 0..<(data["field_value"] as! NSArray).count
            {
                if "\(((data["field_value"] as! NSArray)[i] as! NSDictionary)["show"] ?? "")".elementsEqual("1")
                {
                    tempButtonArray.append(((data["field_value"] as! NSArray)[i] as! NSDictionary))
                }
            }
        }
        else
        {
            for i in 0..<(data["opt_value"] as! NSArray).count
            {
                tempButtonArray.append(((data["opt_value"] as! NSArray)[i] as! NSDictionary))
            }
        }




        let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(tempButtonArray.count)) + (10/568)*self.FullHeight

        let containerView = UIView(frame: CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: tempHeight))
//        if UserDefaults.standard.string(forKey: "langID") == "AR"{
//            tempButton.titleEdgeInsets.left = (-10/320)*self.FullWidth
//        }
//        else{
//            tempButton.titleEdgeInsets.left = (0/320)*self.FullWidth
//        }

        let titleLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (self.FullWidth - (40/320)*self.FullWidth), height: (20/568)*self.FullHeight))

        titleLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
       

        if section.elementsEqual("otherInfo")
        {
            if "\(data["mandatory_field"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["field_name"] ?? "")")
                  
            }
            else
            {
                titleLBL.text = "\(data["field_name"] ?? "")"
               
            }
        }
        else
        {
            if "\(data["mandatory"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["option_name\(langSuffix!)"] ?? "")")
                 
            }
            else
            {
                titleLBL.text = "\(data["option_name\(langSuffix!)"] ?? "")"
                 
            }
        }





        containerView.addSubview(titleLBL)

        var tempOrigin : CGFloat! = (25/568)*self.FullHeight

        var tempArray = [UIButton]()

        for i in 0..<tempButtonArray.count
        {
            var tempButton: UIButton!
            if UserDefaults.standard.string(forKey: "langID") == "AR"{
                tempButton = UIButton(frame: CGRect(x: (280/320)*self.FullWidth, y: tempOrigin, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/320)*self.FullWidth))
            }
            else{
                tempButton = UIButton(frame: CGRect(x: (20/320)*self.FullWidth, y: tempOrigin, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/320)*self.FullWidth))
                
            }
            

            if section.elementsEqual("otherSecondInfo")
            {
                tempButton.accessibilityIdentifier = "otherSecondInfo"
            }

            tempButton.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            tempButton.tag = i
            tempButton.setImage(UIImage(named: "radioUnselected"), for: .normal)
            tempButton.setImage(UIImage(named: "radioSelected"), for: .selected)
//            if UserDefaults.standard.string(forKey: "langID") == "AR"{
//              tempButton.titleEdgeInsets.left = (-10/320)*self.FullWidth
//            }
//            else{
                tempButton.titleEdgeInsets.left = (10/320)*self.FullWidth
         //   }
            
            var buttonLbl: UILabel!
            if UserDefaults.standard.string(forKey: "langID") == "AR"{
                buttonLbl = UILabel(frame: CGRect(x: (self.FullWidth - tempButton.frame.size.width), y: tempOrigin, width: (self.FullWidth - (80/320)*self.FullWidth), height: (20/320)*self.FullWidth))
                buttonLbl.textAlignment = .right
                buttonLbl.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                if section.elementsEqual("otherInfo"){
                    buttonLbl.text = "\((tempButtonArray[i])["value"] ?? "")"
                }
                else{
                    buttonLbl.text = "\((tempButtonArray[i])["option_value\(langSuffix!)"] ?? "")"
                }
                
                 containerView.addSubview(buttonLbl)
                
            }
            else{
                if section.elementsEqual("otherInfo")
                {
                    tempButton.setTitle("\((tempButtonArray[i])["value"] ?? "")", for: .normal)
                }
                else
                {
                    tempButton.setTitle("\((tempButtonArray[i])["option_value\(langSuffix!)"] ?? "")", for: .normal)
                }
            }
            
            


            tempButton.setTitleColor(UIColor.black, for: .normal)
            tempButton.sizeToFit()
            tempButton.frame.size.width = tempButton.frame.size.width + (10/320)*self.FullWidth


            tempButton.addTarget(self, action: #selector(radioButtonAction(sender:)), for: .touchUpInside)


            if "\(data["field_name"] ?? "")".elementsEqual("Product type")
            {
                tempButton.accessibilityIdentifier = "productType"
            }
            else if "\(data["field_name"] ?? "")".elementsEqual("Product Refundable")
            {
                tempButton.accessibilityIdentifier = "productRefundable"
            }
            else
            {
                if section.elementsEqual("otherInfo")
                {
                    tempButton.accessibilityIdentifier = "other"
                }
                else
                {
                    tempButton.accessibilityIdentifier = "otherSecondInfo"
                }

            }
            //            if section.elementsEqual("otherInfo")
            //            {
            //                tempButton.accessibilityIdentifier = "otherInfo"
            //            }
            //            else
            //            {
            //                tempButton.accessibilityIdentifier = "otherSecondInfo"
            //            }
            tempButton.restorationIdentifier = "\(tag)"


            tempArray.append(tempButton)


            containerView.addSubview(tempButton)
           

            tempOrigin = tempOrigin + (25/568)*self.FullHeight

        }

        addToView.addSubview(containerView)

        if section.elementsEqual("otherInfo")
        {
            self.otherInfoRadioButtonArray.append(tempArray)

            self.otherInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["field_name"] ?? "")"
        }
        else
        {
            self.otherSecondInfoRadioButtonArray.append(tempArray)

            self.otherSecondInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["option_name\(langSuffix!)"] ?? "")"
        }

        containerView.tag = tag


    }


    // MARK:- // Radio Button Action

    @objc func radioButtonAction(sender: UIButton!)
    {






        if (sender.accessibilityIdentifier?.elementsEqual("otherSecondInfo"))!
        {

            ///
            self.heldValuesArray.removeAllObjects()

            self.holdValuesWhenAPIFires(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            self.holdValuesWhenAPIFires(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")

            let tempInt = Int(sender.restorationIdentifier!)

            for i in 0..<(self.otherSecondInfoRadioButtonArray[tempInt!]).count
            {
                (self.otherSecondInfoRadioButtonArray[tempInt!])[i].isSelected = false
            }

            (self.otherSecondInfoRadioButtonArray[tempInt!])[sender.tag].isSelected = true

            //self.productType = "\(((self.otherSecondInfoRadioButtonDataArray[tempInt!])[sender.tag] as! NSDictionary)["id"] ?? "")"

        }
        else //(sender.accessibilityIdentifier?.elementsEqual("otherInfo"))!
        {
            let tempInt = Int(sender.restorationIdentifier!)

            print("Other Info Radio Button Array COunt----",self.otherInfoRadioButtonArray.count)

            for i in 0..<(self.otherInfoRadioButtonArray[tempInt!]).count
            {
                (self.otherInfoRadioButtonArray[tempInt!])[i].isSelected = false
            }

            (self.otherInfoRadioButtonArray[tempInt!])[sender.tag].isSelected = true


            ///
            self.heldValuesArray.removeAllObjects()

            self.holdValuesWhenAPIFires(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            self.holdValuesWhenAPIFires(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")

            if (sender.accessibilityIdentifier?.elementsEqual("productType"))!
            {
                self.productType = "\(((self.otherInfoRadioButtonDataArray[tempInt!])[sender.tag] as! NSDictionary)["id"] ?? "")"


                self.otherInfoSubviews.removeAll()
                self.otherSecondInfoSubviews.removeAll()

                self.otherInfoRadioButtonDataArray.removeAll()
                self.otherSecondInfoRadioButtonDataArray.removeAll()

                self.getData()

                self.dispatchGroup.notify(queue: .main) {


                    // Creating Other Info View
                    if self.otherInfoViewDataArray.count > 0
                    {


                        self.otherInfoView.isHidden = false

                        for view in self.otherInfoView.subviews
                        {
                            view.removeFromSuperview()
                        }
                        self.createOtherInfoView(dataArray: self.otherInfoViewDataArray)



                        //for selecting preselected Radio Button in product Type
                        for i in 0..<self.otherInfoRadioButtonArray.count
                        {
                            for j in 0..<self.otherInfoRadioButtonArray[i].count
                            {
                                if ((self.otherInfoRadioButtonArray[i])[j].accessibilityIdentifier?.elementsEqual("productType"))!
                                {
                                    if "\((((self.otherInfoRadioButtonDataArray[i])[j] as! NSDictionary)["id"] ?? ""))".elementsEqual("\(self.productType ?? "")")
                                    {
                                        (self.otherInfoRadioButtonArray[i])[j].isSelected = true
                                    }
                                }
                                if ((self.otherInfoRadioButtonArray[i])[j].accessibilityIdentifier?.elementsEqual("productRefundable"))!
                                {

                                    if "\((((self.otherInfoRadioButtonDataArray[i])[j] as! NSDictionary)["id"] ?? ""))".elementsEqual("\(self.productRefundable ?? "")")
                                    {
                                        (self.otherInfoRadioButtonArray[i])[j].isSelected = true
                                    }
                                }

                            }
                        }

                    }
                    else
                    {
                        self.otherInfoView.isHidden = true
                    }




                    //Creating Other Second Info View
                    if self.otherSecondInfoViewDataArray.count > 0
                    {
                        self.otherSecondInfoView.isHidden = false

                        for view in self.otherSecondInfoView.subviews
                        {
                            view.removeFromSuperview()
                        }
                        self.createOtherSecondInfoView(dataArray: self.otherSecondInfoViewDataArray)
                    }
                    else
                    {
                        self.otherSecondInfoView.isHidden = true
                    }



                    self.putValuesAfterAPIFire(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
                    self.putValuesAfterAPIFire(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")


                }
            }
            else if (sender.accessibilityIdentifier?.elementsEqual("productRefundable"))!
            {

                var viewNumber : Int!

                for i in 0..<self.otherInfoSubviews.count
                {

                    if (self.otherInfoSubviews[i].restorationIdentifier?.elementsEqual("Product Refundable"))!
                    {
                        viewNumber = i

                        let tempOrigin = (self.otherInfoSubviews[i]).frame.size.height

                        if "\((((self.otherInfoViewDataArray[viewNumber] as! NSDictionary)["field_value"] as! NSArray)[sender.tag] as! NSDictionary)["id"] ?? "")".elementsEqual("1")
                        {

                            self.productRefundable = "1"


                            let containerView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: self.FullWidth, height: (60/568)*self.FullHeight))
                            containerView.restorationIdentifier = "Product Refundable"

                            let refundableDaysLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
                            refundableDaysLBL.text = "Refundable Days"
                            refundableDaysLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

                             refundableDaysTXT = UITextField(frame: CGRect(x: (20/320)*self.FullWidth, y: (20/568)*self.FullHeight, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/568)*self.FullHeight))
                            refundableDaysTXT.placeholder = "Refundable Days"
                            refundableDaysTXT.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

                            containerView.addSubview(refundableDaysLBL)
                            containerView.addSubview(refundableDaysTXT)
                            self.otherInfoSubviews[i].addSubview(containerView)

                            self.otherInfoSubviews[i].autoresizesSubviews = false
                            self.otherInfoSubviews[i].frame.size.height = tempOrigin + (60/568)*self.FullHeight
                            self.otherInfoSubviews[i].autoresizesSubviews = true

                            for i in 0..<self.otherInfoSubviews.count
                            {
                                if i > viewNumber
                                {
                                    self.otherInfoSubviews[i].frame.origin.y = self.otherInfoSubviews[i].frame.origin.y + (60/568)*self.FullHeight
                                }
                            }

                            self.otherInfoViewHC = self.otherInfoViewHC + (60/568)*self.FullHeight
                        }
                        else
                        {
                            if self.productRefundable.elementsEqual("0")
                            {

                            }
                            else
                            {
                                self.productRefundable = "0"

                                for view in self.otherInfoSubviews[i].subviews
                                {
                                    if view.restorationIdentifier?.isEmpty == false
                                    {
                                        if (view.restorationIdentifier?.elementsEqual("Product Refundable"))!
                                        {
                                            view.removeFromSuperview()
                                        }
                                    }

                                }


                                self.otherInfoSubviews[i].autoresizesSubviews = false
                                self.otherInfoSubviews[i].frame.size.height = self.otherInfoSubviews[i].frame.size.height - (60/568)*self.FullHeight
                                self.otherInfoSubviews[i].autoresizesSubviews = true

                                for i in 0..<self.otherInfoSubviews.count
                                {
                                    if i > viewNumber
                                    {
                                        self.otherInfoSubviews[i].frame.origin.y = self.otherInfoSubviews[i].frame.origin.y - (60/568)*self.FullHeight
                                    }
                                }

                                self.otherInfoViewHC = self.otherInfoViewHC - (60/568)*self.FullHeight

                            }


                        }





                        self.otherInfoView.autoresizesSubviews = false
                        self.otherInfoViewHeightConstraint.constant = self.otherInfoViewHC
                        self.otherInfoView.autoresizesSubviews = true


                    }
                }
            }




        }






    }





    // MARK:- // Function to Open PickerView

    @objc func openPicker(sender: UIButton!)
    {
        if (sender.restorationIdentifier?.elementsEqual("otherInfo"))!
        {
            self.pickerViewDataArray = self.otherInfoListViewDataArray

            self.inSection = "otherInfo"
        }
        else
        {
            self.pickerViewDataArray = self.otherSecondInfoListViewDataArray

            self.inSection = "otherSecondInfo"
        }

        self.listClicked = sender.tag


        // MARK:- // COuntry CIty State check
        if (sender.restorationIdentifier?.elementsEqual("otherInfo"))!
        {


            for i in 0..<self.otherInfoListViewDataArray.count
            {

                if "\((self.otherInfoListViewDataArray[i])["field_name"] ?? "")".elementsEqual("\((self.otherInfoListViewDataArray[sender.tag])["field_name"] ?? "")")
                {


                    if "\((self.otherInfoListViewDataArray[i])["field_name"] ?? "")".elementsEqual("State")
                    {
                        if self.stateArray.count == 0
                        {
                            self.customAlert(title: "Select Country", message: "Please Select a Country First ") {

                                print("Country Not Selected")

                            }
                        }
                        else
                        {
                            self.pagePickerview.delegate = self
                            self.pagePickerview.dataSource = self
                            self.pagePickerview.reloadAllComponents()

                            UIView.animate(withDuration: 0.3) {

                                self.viewOfPickerTopConstraint.constant = 0

                            }

                            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                            self.viewOfPickerview.addGestureRecognizer(self.tapGesture)
                        }
                    }
                    else if "\((self.otherInfoListViewDataArray[sender.tag])["field_name"] ?? "")".elementsEqual("City")
                    {
                        if self.cityArray.count == 0
                        {
                            self.customAlert(title: "Select City", message: "Please Select a State First ") {

                                print("State Not Selected")

                            }
                        }
                        else
                        {
                            self.pagePickerview.delegate = self
                            self.pagePickerview.dataSource = self
                            self.pagePickerview.reloadAllComponents()

                            UIView.animate(withDuration: 0.3) {

                                self.viewOfPickerTopConstraint.constant = 0

                            }

                            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                            self.viewOfPickerview.addGestureRecognizer(self.tapGesture)
                        }
                    }
                    else
                    {
                        self.pagePickerview.delegate = self
                        self.pagePickerview.dataSource = self
                        self.pagePickerview.reloadAllComponents()

                        UIView.animate(withDuration: 0.3) {

                            self.viewOfPickerTopConstraint.constant = 0

                        }

                        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                        self.viewOfPickerview.addGestureRecognizer(self.tapGesture)
                    }
                }

            }
        }
        else
        {
            self.pagePickerview.delegate = self
            self.pagePickerview.dataSource = self
            self.pagePickerview.reloadAllComponents()

            UIView.animate(withDuration: 0.3) {

                self.viewOfPickerTopConstraint.constant = 0

            }

            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            self.viewOfPickerview.addGestureRecognizer(self.tapGesture)
        }
        ///




    }

    // MARK: - Hide pickerView when tapping outside

    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.viewOfPickerTopConstraint.constant = self.FullHeight
        }
    }



    // MARK:- // Create View with CheckBoxes

    func createViewWithCheckboxes(data: NSDictionary,yOrigin : CGFloat,addToView : UIView, section: String,tag : Int)
    {

        let dataArray = data["opt_value"] as! NSArray

        let tempHeight = (25/568)*self.FullHeight + (((25/568)*self.FullHeight) * CGFloat(dataArray.count)) + (10/568)*self.FullHeight

        let containerView = UIView(frame: CGRect(x: 0, y: yOrigin, width: self.FullWidth, height: tempHeight))

        let titleLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (self.FullWidth - (40/320)*self.FullWidth), height: (20/568)*self.FullHeight))

        titleLBL.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

        if section.elementsEqual("otherInfo")
        {
            if "\(data["mandatory_field"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["field_name"] ?? "")")
                
            }
            else
            {
                titleLBL.text = "\(data["field_name"] ?? "")"
                 
            }

        }
        else
        {
            if "\(data["mandatory"] ?? "")".elementsEqual("1")
            {
                titleLBL.attributedText = asterixText(withText: "\(data["option_name\(langSuffix ?? "")"] ?? "")")
                
            }
            else
            {
                titleLBL.text = "\(data["option_name\(langSuffix ?? "")"] ?? "")"
                 
            }

        }


        containerView.addSubview(titleLBL)

        var tempOrigin : CGFloat! = (25/568)*self.FullHeight

        var tempArray = [UIButton]()

        for i in 0..<dataArray.count
        {

            //let tempButton = UIButton(frame: CGRect(x: (20/320)*self.FullWidth, y: tempOrigin, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/320)*self.FullWidth))
            var tempButton: UIButton!
            if UserDefaults.standard.string(forKey: "langID") == "AR"{
                tempButton = UIButton(frame: CGRect(x: (280/320)*self.FullWidth, y: tempOrigin, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/320)*self.FullWidth))
            }
            else{
                tempButton = UIButton(frame: CGRect(x: (20/320)*self.FullWidth, y: tempOrigin, width: self.FullWidth - (40/320)*self.FullWidth, height: (20/320)*self.FullWidth))
            }

            tempButton.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            tempButton.tag = i
            tempButton.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            tempButton.setImage(UIImage(named: "checkBoxFilled"), for: .selected)
            var buttonLbl: UILabel!
            if UserDefaults.standard.string(forKey: "langID") == "AR"{
                buttonLbl = UILabel(frame: CGRect(x: (self.FullWidth - tempButton.frame.size.width), y: tempOrigin, width: (self.FullWidth - (80/320)*self.FullWidth), height: (20/320)*self.FullWidth))
                buttonLbl.textAlignment = .right
                buttonLbl.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                buttonLbl.text = "\((dataArray[i] as! NSDictionary)["option_value\(self.langSuffix ?? "")"] ?? "")"
                containerView.addSubview(buttonLbl)
            }
            else{
                tempButton.setTitle("\((dataArray[i] as! NSDictionary)["option_value\(self.langSuffix ?? "")"] ?? "")", for: .normal)
                tempButton.titleEdgeInsets.left = (10/320)*self.FullWidth
            }
            

            tempButton.setTitleColor(UIColor.black, for: .normal)
            tempButton.sizeToFit()
            tempButton.frame.size.width = tempButton.frame.size.width + (10/320)*self.FullWidth


            tempButton.addTarget(self, action: #selector(checkBoxAction(sender:)), for: .touchUpInside)



            tempButton.restorationIdentifier = "\(tag)"
            tempButton.accessibilityIdentifier = section


            tempArray.append(tempButton)


            containerView.addSubview(tempButton)
            

            tempOrigin = tempOrigin + (25/568)*self.FullHeight

        }

        addToView.addSubview(containerView)

        if section.elementsEqual("otherInfo")
        {
            self.otherInfoCheckboxArray.append(tempArray)

            self.otherInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\(data["field_name"] ?? "")"
        }
        else
        {
            self.otherSecondInfoCheckboxArray.append(tempArray)

            self.otherSecondInfoSubviews.append(containerView)
            containerView.restorationIdentifier = "\((data)["option_name\(self.langSuffix ?? "")"] ?? "")"
        }

        containerView.tag = tag


    }



    // MARK:- // CheckBox Action Method

    @objc func checkBoxAction(sender: UIButton!)
    {
        let tempint = Int(sender.restorationIdentifier!)

        if (sender.accessibilityIdentifier?.elementsEqual("otherInfo"))!
        {
            (self.otherInfoCheckboxArray[tempint!])[sender.tag].isSelected = !(self.otherInfoCheckboxArray[tempint!])[sender.tag].isSelected
        }
        else
        {
            (self.otherSecondInfoCheckboxArray[tempint!])[sender.tag].isSelected = !(self.otherSecondInfoCheckboxArray[tempint!])[sender.tag].isSelected
        }
    }













    //MARK:- // Save Data

    func saveData(check : Bool)
    {

        self.dispatchGroup.enter()

        var tempArray = [String]()


        if check == true
        {
            print("other second info View Data Array count---",self.otherSecondInfoViewDataArray.count)
        }
        else
        {
            print("other info View Data Array count---",self.otherInfoViewDataArray.count)


            var otherInfoTextFieldCount = 0
            var otherSecondInfoTextFieldCount = 0

            var otherInfoListViewCount = 0
            var otherSecondInfoListViewCount = 0

            var otherInfoCheckboxCount = 0
            var otherSecondInfoCheckboxCount = 0

            var otherInfoRadioButtonCount = 0
            var otherSecondInfoRadioButtonCount = 0



            // MARK:- // Other Info View

            if self.otherInfoViewDataArray.count > 0
            {
                for i in 0..<self.otherInfoView.subviews.count
                {

                    if self.otherInfoView.subviews.count == 0
                    {

                    }
                    else
                    {
                        //print((self.otherInfoView.subviews)[i].restorationIdentifier ?? "")



                        // MARK:- // Text Fields Other Info View

                        if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("T")
                        {
                            if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)".elementsEqual("")
                            {
                                submitTextfieldArray.add(["id" : "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\(self.otherInfoTextfieldArray[otherInfoTextFieldCount].text ?? "")"])
                            }
                            else
                            {

                                if ((self.otherInfoTextfieldArray[otherInfoTextFieldCount].text)?.isEmpty)!
                                {
                                    //                                    self.customAlert(title: "Enter Details", message: "Please enter (\((self.otherInfoView.subviews)[i].restorationIdentifier ?? ""))") {
                                    //
                                    //                                    }
                                }


                                //                                print("testValue","\(self.otherInfoTextfieldArray[otherInfoTextFieldCount].text ?? "")&")
                                //                                print("TestCount",otherInfoTextFieldCount)


                                params = params + "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)" + "=" + "\(self.otherInfoTextfieldArray[otherInfoTextFieldCount].text ?? "")&"
                            }

                            otherInfoTextFieldCount = otherInfoTextFieldCount + 1

                        }



                        // MARK:- // List Views Other Info View



                        if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("S")
                        {





                            if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)".elementsEqual("")
                            {





                                if ("Select \(self.otherInfoView.subviews[i].restorationIdentifier!)").elementsEqual("\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")")
                                {
                                    submitListViewArray.add(["id" : "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : ""])
                                    //submitListViewArray.append(["id" : "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : ""])
                                }
                                else
                                {


                                    for k in 0..<((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                                    {
                                        if "\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")".elementsEqual("\((((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray)[k] as! NSDictionary)["value"] ?? "")")
                                        {
                                            submitListViewArray.add(["id" : "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\((((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray)[k] as! NSDictionary)["id"] ?? "")"])
                                        }
                                    }


                                    //submitListViewArray.add(["id" : "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")"])

                                }


                            }
                            else
                            {

                                //                                if ((self.otherInfoTextfieldArray[otherInfoTextFieldCount].text)?.isEmpty)!
                                //                                {
                                //                                    //                                    self.customAlert(title: "Enter Details", message: "Please enter (\((self.otherInfoView.subviews)[i].restorationIdentifier ?? ""))") {
                                //                                    //
                                //                                    //                                    }
                                //                                }





                                if ("Select \(self.otherInfoView.subviews[i].restorationIdentifier!)").elementsEqual("\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")")
                                {
                                    params = params + "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)" + "=" + "&"
                                }
                                else
                                {

                                    for k in 0..<((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray).count
                                    {
                                        if "\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")".elementsEqual("\((((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray)[k] as! NSDictionary)["value"] ?? "")")
                                        {
                                            params = params + "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)" + "=" + "\((((self.otherInfoViewDataArray[i] as! NSDictionary)["field_value"] as! NSArray)[k] as! NSDictionary)["id"] ?? "")&"
                                        }
                                    }

                                    //"\(self.otherInfolistViewLblArray[otherInfoListViewCount].text ?? "")"
                                }



                            }

                            otherInfoListViewCount = otherInfoListViewCount + 1

                        }


                        // MARK:- // Radio Buttons Other Info View

                        if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["field_type"] ?? "")".elementsEqual("R")
                        {
                            let tempID = "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")"


                            for j in 0..<(self.otherInfoRadioButtonArray[otherInfoRadioButtonCount]).count
                            {
                                if (self.otherInfoRadioButtonArray[otherInfoRadioButtonCount])[j].isSelected
                                {

                                    if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)".elementsEqual("")
                                    {
                                        //tempArray.append("\(((self.otherInfoRadioButtonDataArray[otherInfoRadioButtonCount])[j] as! NSDictionary)["id"]!)")
                                        self.submitRadioButtonArray.add(["id" : tempID , "value" : "\(((self.otherInfoRadioButtonDataArray[otherInfoRadioButtonCount])[j] as! NSDictionary)["id"]!)"])

                                    }
                                    else
                                    {
                                        params = params + "\((self.otherInfoViewDataArray[i] as! NSDictionary)["given_field"]!)" + "=" + "\(((self.otherInfoRadioButtonDataArray[otherInfoRadioButtonCount])[j] as! NSDictionary)["id"]!)&"
                                    }



                                }
                            }


                            otherInfoRadioButtonCount = otherInfoRadioButtonCount + 1

                            if tempArray.count > 0
                            {

                                //                                self.submitRadioButtonArray.add(["id" : tempID , "value" : tempArray])


                                //self.submitCheckboxArray.append(["id" : tempID as AnyObject , "value" : tempArray as AnyObject])
                            }
                        }




                        // MARK:- // Check Boxes Other Info View



                        if "\((self.otherInfoViewDataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("C")
                        {



                            let tempID = "\((self.otherInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")"

                            for j in 0..<(self.otherInfoCheckboxArray[otherInfoCheckboxCount]).count
                            {
                                if (self.otherInfoCheckboxArray[otherInfoCheckboxCount])[j].isSelected
                                {

                                    tempArray.append("\(((self.otherInfoCheckboxDataArray[otherInfoCheckboxCount])[j] as! NSDictionary)["id"]!)")

                                }
                            }

                            otherInfoCheckboxCount = otherSecondInfoCheckboxCount + 1

                            if tempArray.count > 0
                            {
                                let tempString = tempArray.joined(separator: ",")

                                self.submitCheckboxArray.add(["id" : tempID , "value" : tempString])
                                //self.submitCheckboxArray.append(["id" : tempID as AnyObject , "value" : tempArray as AnyObject])

                            }

                        }


                    }

                }

            }



            // MARK:- // Other Second Info View


            if self.otherSecondInfoViewDataArray.count > 0
            {
                for i in 0..<self.otherSecondInfoView.subviews.count
                {

                    if self.otherSecondInfoView.subviews.count == 0
                    {

                    }
                    else
                    {



                        print((self.otherSecondInfoView.subviews)[i].restorationIdentifier ?? "")


                        // MARK:- // Text Fields Other Second Info View

                        if "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("T")
                        {



                            submitTextfieldArray.add(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\(self.otherSecondInfoTextfieldArray[otherSecondInfoTextFieldCount].text ?? "")"])
                            //submitTextfieldArray.append(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\(self.otherSecondInfoTextfieldArray[otherSecondInfoTextFieldCount].text ?? "")"])

                            otherSecondInfoTextFieldCount = otherSecondInfoTextFieldCount + 1

                        }


                        // MARK:- // List Views Other Second Info View


                        if "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("S")
                        {

                            if ("Select \(self.otherSecondInfoView.subviews[i].restorationIdentifier!)").elementsEqual("\(self.otherSecondInfolistViewLblArray[otherSecondInfoListViewCount].text ?? "")")
                            {
                                submitListViewArray.add(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : ""])
                                //submitListViewArray.append(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : ""])
                            }
                            else
                            {
                                //submitListViewArray.add(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\(self.otherSecondInfolistViewLblArray[otherSecondInfoListViewCount].text ?? "")"])

                                print(otherSecondInfoListViewCount)

                                for k in 0..<((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["opt_value"] as! NSArray).count
                                {
                                    if "\(self.otherSecondInfolistViewLblArray[otherSecondInfoListViewCount].text ?? "")".elementsEqual("\((((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["opt_value"] as! NSArray)[k] as! NSDictionary)["option_value\(langSuffix!)"] ?? "")")
                                    {
                                        submitListViewArray.add(["id" : "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")" , "value" : "\((((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["opt_value"] as! NSArray)[k] as! NSDictionary)["id"] ?? "")"])
                                    }
                                }



                            }



                            otherSecondInfoListViewCount = otherSecondInfoListViewCount + 1

                        }


                        // MARK:- // Check Boxes Other Second Info View



                        if "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("C")
                        {



                            let tempID = "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")"

                            for j in 0..<(self.otherSecondInfoCheckboxArray[otherSecondInfoCheckboxCount]).count
                            {
                                if (self.otherSecondInfoCheckboxArray[otherSecondInfoCheckboxCount])[j].isSelected
                                {

                                    tempArray.append("\(((self.otherSecondInfoCheckboxDataArray[otherSecondInfoCheckboxCount])[j] as! NSDictionary)["id"]!)")

                                }
                            }

                            otherSecondInfoCheckboxCount = otherSecondInfoCheckboxCount + 1

                            if tempArray.count > 0
                            {

                                let tempString = tempArray.joined(separator: ",")

                                self.submitCheckboxArray.add(["id" : tempID , "value" : tempString])
                                //self.submitCheckboxArray.append(["id" : tempID as AnyObject , "value" : tempArray as AnyObject])

                            }

                        }


                        // MARK:- // Radio Buttons Other Second Info View

                        if "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["type"] ?? "")".elementsEqual("R")
                        {
                            let tempID = "\((self.otherSecondInfoViewDataArray[i] as! NSDictionary)["id"] ?? "")"

                            for j in 0..<(self.otherSecondInfoRadioButtonArray[otherSecondInfoRadioButtonCount]).count
                            {
                                if (self.otherSecondInfoRadioButtonArray[otherSecondInfoRadioButtonCount])[j].isSelected
                                {

                                    //                                    tempArray.append("\(((self.otherSecondInfoRadioButtonDataArray[otherSecondInfoRadioButtonCount])[j] as! NSDictionary)["id"]!)")

                                    self.submitRadioButtonArray.add(["id" : tempID , "value" : "\(((self.otherSecondInfoRadioButtonDataArray[otherSecondInfoRadioButtonCount])[j] as! NSDictionary)["id"]!)"])

                                }
                            }

                            otherSecondInfoRadioButtonCount = otherSecondInfoRadioButtonCount + 1

                            if tempArray.count > 0
                            {

                                //                                self.submitRadioButtonArray.add(["id" : tempID , "value" : tempArray])

                            }
                        }



                    }


                }
            }







            //            print("Params-----",params!)
            //            print("Submit Text Field Array----",self.submitTextfieldArray)
            //            print("Submit List View Array----",self.submitListViewArray)
            //            print("Submit Check Box Array----",self.submitCheckboxArray)
            //            print("Submit Radio Button Array----",self.submitRadioButtonArray)



        }

        self.dispatchGroup.leave()
    }

    @IBOutlet weak var readyToSubmitButtonOutlet: UIButton!
    @IBAction func readyToSubmit(_ sender: UIButton) {

        self.submitTextfieldArray.removeAllObjects()
        self.submitListViewArray.removeAllObjects()
        self.submitCheckboxArray.removeAllObjects()

        if sender.isSelected
        {
            self.saveData(check: true)

            sender.isSelected = false
        }
        else
        {

            sender.isSelected = true

            self.saveData(check: false)


            //            self.a = "This is Working..."
            //
            //            passDataDelegate?.didPassData(data: a)


            self.submitArraysDictionary = ["params":params! , "txtArray":submitTextfieldArray , "listArray":submitListViewArray , "checkArray":submitCheckboxArray , "radioArray":submitRadioButtonArray] as [String : Any]


            UserDefaults.standard.set(self.params, forKey: "postAddParams")
            UserDefaults.standard.setValue(self.submitArraysDictionary, forKey: "postAddDict")
            UserDefaults.standard.synchronize()

            NotificationCenter.default.post(name: .passDataAddPost, object: "1")

            //            dismiss(animated: true, completion: nil)

            //self.sendAllInformation()
        }

    }


    var redTitleLBLArrray = [UILabel]()

    // MARK:- // Check Empty Sections

    func checkEmptySections(completion : @escaping ()->())
    {

        self.redTitleLBLArrray.removeAll()


        // Category Views

        self.checkForEmptyCategoryviews(parentView: self.allCategoriesView)

        // Other Info View
        if self.otherInfoViewDataArray.count > 0
        {
            // TextField
            self.checkForEmptyTextfields(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type", mandatroyString: "mandatory_field", fieldName: "field_name")

            // ListViews
            self.checkForEmptyListviews(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type", mandatroyString: "mandatory_field", fieldName: "field_name")

            // Radio Buttons
            self.checkForEmotyButtonsSections(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type", mandatroyString: "mandatory_field" , type: "R", fieldName: "field_name")

            // Checkboxes
            self.checkForEmotyButtonsSections(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type", mandatroyString: "mandatory_field" , type: "C", fieldName: "field_name")
        }


        // Other Second Info View
        if self.otherSecondInfoViewDataArray.count > 0
        {
            // TextField
            self.checkForEmptyTextfields(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type", mandatroyString: "mandatory", fieldName: "option_name\(langSuffix!)")


            // ListViews
            self.checkForEmptyListviews(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type", mandatroyString: "mandatory", fieldName: "option_name\(langSuffix!)")


            // Radio Buttons
            self.checkForEmotyButtonsSections(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type", mandatroyString: "mandatory", type: "R", fieldName: "option_name\(langSuffix!)")

            // Checkboxes
            self.checkForEmotyButtonsSections(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type", mandatroyString: "mandatory", type: "C", fieldName: "option_name\(langSuffix!)")
        }







        completion()
    }





    // MARK:- // Check For Empty Category Views

    func checkForEmptyCategoryviews(parentView:UIView)
    {
        for i in 0..<parentView.subviews.count
        {
            for view in (parentView.subviews[i]).subviews
            {
                if view.isKind(of: UILabel.self)
                {
                    if ((view as! UILabel).text?.isEmpty)!
                    {
                        for label in (parentView.subviews[i]).subviews
                        {
                            if label.isKind(of: UILabel.self)
                            {
                                if (label as! UILabel).text?.isEmpty == false
                                {
                                    self.redTitleLBLArrray.append(label as! UILabel)
                                }
                            }
                        }
                    }
                    else
                    {
                        for label in (parentView.subviews[i]).subviews
                        {
                            if label.isKind(of: UILabel.self)
                            {
                                if label.isKind(of: UILabel.self)
                                {
                                    (label as! UILabel).textColor = UIColor.black

                                    if (((label as! UILabel).accessibilityIdentifier ?? "").elementsEqual("titleLBL"))
                                    {
                                        (label as! UILabel).attributedText = asterixText(withText: "Select Category")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }





    // MARK:- // CHeck for Empty TextFields

    func checkForEmptyTextfields(parentView:UIView , dataArray:NSArray , typeKey:String , mandatroyString:String , fieldName:String)
    {
        for i in 0..<parentView.subviews.count
        {
            if "\((dataArray[i] as! NSDictionary)[mandatroyString] ?? "")".elementsEqual("1")
            {
                if "\((dataArray[i] as! NSDictionary)[typeKey]!)".elementsEqual("T")
                {
                    for view in (parentView.subviews[i]).subviews
                    {
                        if view.isKind(of: UITextField.self)
                        {
                            if ((view as! UITextField).text?.isEmpty)!
                            {
                                for label in (parentView.subviews[i]).subviews
                                {
                                    if label.isKind(of: UILabel.self)
                                    {
                                        self.redTitleLBLArrray.append(label as! UILabel)
                                    }
                                }
                            }
                            else
                            {
                                for label in (parentView.subviews[i]).subviews
                                {
                                    if label.isKind(of: UILabel.self)
                                    {
                                        (label as! UILabel).textColor = UIColor.black
                                        (label as! UILabel).attributedText = asterixText(withText: "\((dataArray[i] as! NSDictionary)[fieldName]!)")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }





    // MARK:- // Check For Empty List Views

    func checkForEmptyListviews(parentView:UIView , dataArray:NSArray , typeKey:String , mandatroyString:String , fieldName:String)
    {
        for i in 0..<parentView.subviews.count
        {
            if "\((dataArray[i] as! NSDictionary)[mandatroyString] ?? "")".elementsEqual("1")
            {
                if "\((dataArray[i] as! NSDictionary)[typeKey]!)".elementsEqual("S")
                {
                    for view in (parentView.subviews[i]).subviews
                    {
                        if view.isKind(of: UILabel.self)
                        {
                            if ((view as! UILabel).text?.elementsEqual("Select \(parentView.subviews[i].restorationIdentifier!)"))!
                            {
                                //print(("Select \(parentView.subviews[i].restorationIdentifier!)"))
                                for label in (parentView.subviews[i]).subviews
                                {
                                    if label.isKind(of: UILabel.self)
                                    {
                                        if (label.restorationIdentifier?.elementsEqual("titleLBL"))!
                                        {
                                            self.redTitleLBLArrray.append(label as! UILabel)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for label in (parentView.subviews[i]).subviews
                                {
                                    if label.isKind(of: UILabel.self)
                                    {
                                        if (label.restorationIdentifier?.elementsEqual("titleLBL"))!
                                        {
                                            (label as! UILabel).textColor = UIColor.black
                                            if (((label as! UILabel).restorationIdentifier ?? "").elementsEqual("titleLBL"))
                                            {
                                                (label as! UILabel).attributedText = asterixText(withText: "\((dataArray[i] as! NSDictionary)[fieldName]!)")
                                                //(label as! UILabel).text = "Priya"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    // MARK:- // Check For Empty Button Sections

    func checkForEmotyButtonsSections(parentView:UIView , dataArray:NSArray , typeKey:String , mandatroyString:String , type:String , fieldName:String)
    {
        for i in 0..<parentView.subviews.count
        {
            if "\((dataArray[i] as! NSDictionary)[mandatroyString] ?? "")".elementsEqual("1")
            {
                if "\((dataArray[i] as! NSDictionary)[typeKey]!)".elementsEqual(type)
                {
                    var tempCount = 0
                    for view in (parentView.subviews[i]).subviews
                    {
                        if view.isKind(of: UIButton.self)
                        {
                            if (view as! UIButton).isSelected
                            {
                                tempCount = tempCount + 1
                            }
                        }
                    }

                    if tempCount == 0
                    {
                        for view in (parentView.subviews[i]).subviews
                        {
                            if view.isKind(of: UILabel.self)
                            {
                                self.redTitleLBLArrray.append((view as! UILabel))
                            }
                        }
                    }
                    else
                    {
                        for view in (parentView.subviews[i]).subviews
                        {
                            if view.isKind(of: UILabel.self)
                            {
                                (view as! UILabel).textColor = UIColor.black
                                (view as! UILabel).attributedText = asterixText(withText: "\((dataArray[i] as! NSDictionary)[fieldName]!)")
                            }
                        }
                    }

                }
            }
        }
    }









    // MARK: - // JSON POST Method to get Watchlist Data

    func sendAllInformation()

    {
        
        
        


        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_add_item_post")!   //change the url
        
      
           

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        print("submitTextfieldArray-----",self.submitTextfieldArray)
         print("submitCheckboxArray-----",self.submitCheckboxArray)
         print("submitRadioButtonArray-----",self.submitRadioButtonArray)
         print("submitListViewArray-----",self.submitListViewArray)
          print("userID-----",self.userID!)
          print("langID-----",self.langID!)
          print("catID-----",self.catID!)
          print("topCatID-----",self.topCatID!)
       
              
        

       // parameters = params + "textfield=\(JSONStringify(value: self.submitTextfieldArray as AnyObject))&checkboxfield=\(JSONStringify(value: self.submitCheckboxArray as AnyObject))&radiofield=\(JSONStringify(value: self.submitRadioButtonArray as AnyObject))&selectfield=\(JSONStringify(value: self.submitListViewArray as AnyObject))&user_id=\(userID!)&lang_id=\(langID!)" + "&cat_id=\(catID!)&top_cat=\(topCatID!)&step_final=0"
        
        
        
        //https://kaafoo.com/appcontrol/app_add_item_post?user_id=158&product_name=lorem&product_type=B&pro_refund_type=0&start_date_time=&end_date_time=&pro_refund_days=&duration_time=2weeks&no_of_product=12&product_condition=N&product_barcode=&cat_id=9%2C27&top_cat=9&pro_location=349&no_of_person=&driver_age=&country=&state=&city=&suburb=&street=&built_square_value=&tot_built_square_value=&step_final=0&product_id=&textfield=%5B%5D&checkboxfield=%5B%5D&radiofield=%5B%7B%22id%22%3A%22131%22%2C%22value%22%3A%22906%22%7D%5D&selectfield=%5B%7B%22id%22%3A%2257%22%2C%22value%22%3A%22%22%7D%5D&descriptionfield=%5B%5D&attr_selectfield=%5B%5D&increase_quantity=0&lang_id=en
        
        parameters = "user_id=158&product_name=priya&product_type=A&pro_refund_type=0&start_date_time=&end_date_time=&pro_refund_days=&duration_time=4+months&no_of_product=12&product_condition=N&product_barcode=&cat_id=\(catID!)&top_cat=\(topCatID!)&pro_location=0&no_of_person=3&driver_age=&country=&state=&city=&suburb=&street=&built_square_value=&tot_built_square_value=&step_final=0&product_id=&textfield=\(JSONStringify(value: self.submitTextfieldArray as AnyObject))&checkboxfield=\(JSONStringify(value: self.submitCheckboxArray as AnyObject))&radiofield=\(JSONStringify(value: self.submitRadioButtonArray as AnyObject))&selectfield=\(JSONStringify(value: self.submitListViewArray as AnyObject))&descriptionfield=&attr_selectfield=&increase_quantity=0&lang_id=\(langID!)&license=123&min_exp=3"

        print("App Add Item Post URL is : ",url)
        print("Parameters are : " , parameters)

        UserDefaults.standard.setValue(self.productType ?? "", forKey: "productType")

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {

            request.httpBody = parameters.data(using: String.Encoding.utf8)

        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Send All Information Response: " , json)


                    if (json["response"] as! Bool) == false
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        UserDefaults.standard.setValue("\(json["product_id"]!)", forKey: "productID")

                        UserDefaults.standard.setValue("0", forKey: "stepFinal")
                        UserDefaults.standard.set(true, forKey: "isFillbasicInfo")

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }


                }
                else
                {
                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }
                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
        })

        task.resume()



    }




    var heldValuesArray = NSMutableArray()

    // MARK:- // Hold Values when API fires

    func holdValuesWhenAPIFires(parentView:UIView , dataArray:NSArray , typeKey:String)
    {
//        var tempButtonSection = 0

        for i in 0..<dataArray.count
        {
            
            print("dataArray----",dataArray)

            // Text Field
            if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("T")
            {
                for view in parentView.subviews[i].subviews
                {
                    if view.isKind(of: UITextField.self)
                    {
                        self.heldValuesArray.add(["key" : parentView.subviews[i].restorationIdentifier , "value" : (view as! UITextField).text ?? ""])
                        print("heldValuesArray----",self.heldValuesArray)
                    }
                }
            }


            // List Views
            if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("S")
            {
                for view in parentView.subviews[i].subviews
                {
                    if view.isKind(of: UILabel.self)
                    {
                        if (view as! UILabel).restorationIdentifier != "titleLBL"
                        {
                            self.heldValuesArray.add(["key" : parentView.subviews[i].restorationIdentifier , "value" : (view as! UILabel).text ?? ""])
                             print("heldValuesArray----",self.heldValuesArray)
                        }
                    }
                }
            }



            // Radio Buttons
            if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("R")
            {
                for view in parentView.subviews[i].subviews
                {
                    if view.isKind(of: UIButton.self)
                    {
                        if (view as! UIButton).isSelected == true
                        {
                            self.heldValuesArray.add(["key" : parentView.subviews[i].restorationIdentifier , "value" : (view as! UIButton).titleLabel?.text ?? ""])
                             print("heldValuesArray----",self.heldValuesArray)
                        }
                    }
                }
            }


            // Check Boxes
            if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("C")
            {
                var tempArray = [String]()
                for view in parentView.subviews[i].subviews
                {
                    if view.isKind(of: UIButton.self)
                    {
                        if (view as! UIButton).isSelected == true
                        {

                            tempArray.append((view as! UIButton).titleLabel?.text ?? "")
                             print("heldValuesArray----",self.heldValuesArray)
                        }
                    }
                }
                self.heldValuesArray.add(["key" : parentView.subviews[i].restorationIdentifier! , "value" : tempArray])
                
                 print("heldValuesArray----",self.heldValuesArray)
            }
        }
    }





    // MARK:- // Put Values after API FIre

    func putValuesAfterAPIFire(parentView:UIView , dataArray:NSArray , typeKey:String)
    {
        print(self.heldValuesArray)

        if dataArray.count > 0
        {
            for i in 0..<parentView.subviews.count
            {
                for j in 0..<self.heldValuesArray.count
                {

                    print(parentView.subviews[i].restorationIdentifier ?? "")
                    print("\((self.heldValuesArray[j] as! NSDictionary)["key"] ?? "")")

                    if (parentView.subviews[i].restorationIdentifier?.elementsEqual("\((self.heldValuesArray[j] as! NSDictionary)["key"] ?? "")"))!
                    {


                        // Text Field
                        if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("T")
                        {
                            for view in parentView.subviews[i].subviews
                            {
                                if view.isKind(of: UITextField.self)
                                {
                                    (view as! UITextField).text = "\((self.heldValuesArray[j] as! NSDictionary)["value"] ?? "")"
                                }
                            }
                        }



                        // List Views
                        if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("S")
                        {
                            for view in parentView.subviews[i].subviews
                            {
                                if view.isKind(of: UILabel.self)
                                {
                                    if (view as! UILabel).restorationIdentifier != "titleLBL"
                                    {
                                        (view as! UILabel).text = "\((self.heldValuesArray[j] as! NSDictionary)["value"] ?? "")"
                                    }
                                }
                            }
                        }



                        // Radio Buttons
                        if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("R")
                        {
                            for view in parentView.subviews[i].subviews
                            {
                                if view.isKind(of: UIButton.self)
                                {
                                    if ((view as! UIButton).titleLabel?.text?.elementsEqual("\("\((self.heldValuesArray[j] as! NSDictionary)["value"] ?? "")")"))!
                                    {
                                        (view as! UIButton).isSelected = true
                                    }
                                }
                            }
                        }



                        // Check Boxes
                        if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("C")
                        {
                            for view in parentView.subviews[i].subviews
                            {
                                for k in 0..<((self.heldValuesArray[j] as! NSDictionary)["value"] as! [String]).count
                                {
                                    if view.isKind(of: UIButton.self)
                                    {
                                        if ((view as! UIButton).titleLabel?.text?.elementsEqual("\(((self.heldValuesArray[j] as! NSDictionary)["value"] as! [String])[k])"))!
                                        {
                                            (view as! UIButton).isSelected = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    }





    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{

        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)


        if JSONSerialization.isValidJSONObject(value) {

            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {

                    return string as String
                }
            }catch {

                print("error")
                //Access error here
            }

        }
        return ""

    }










    var tempCount = 0

    // MARK:- // Auto Load Page

    func autoLoadPage(finalCatID : String)
    {

        self.getListingDataWithProductID()

        self.dispatchGroup.notify(queue: .main) {


            self.tableviewArray.removeAll()


            for i in 0..<self.allCategoriesDataArray.count
            {
                self.createViewWithTableview(yOrigin: CGFloat(i) * (60/568)*self.FullHeight, title: "Select Category", tableTag: i)
            }

            for i in 0..<self.allCategoriesDataArray.count
            {
                self.tableviewArray[i].delegate = self
                self.tableviewArray[i].dataSource = self
                self.tableviewArray[i].reloadData()
            }

            self.allCategoriesviewHeightConstraint.constant = CGFloat(self.allCategoriesDataArray.count) * (60/568)*self.FullHeight

            for j in 0..<self.allCategoriesDataArray.count
            {
                for i in 0..<(self.allCategoriesDataArray[j]).count
                {
                    if "\(((self.allCategoriesDataArray[j])[i] as! NSDictionary)["given_status"] ?? "")".elementsEqual("1")
                    {

                        print(self.langSuffix)

                        self.categoryLBLArray[j].text = "\(((self.allCategoriesDataArray[j])[i] as! NSDictionary)["categoryname\(self.langSuffix!)"] ?? "")"
                    }
                }
            }





            // Creating Other Info View
            if self.otherInfoViewDataArray.count > 0
            {


                self.otherInfoView.isHidden = false

                for view in self.otherInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherInfoView(dataArray: self.otherInfoViewDataArray)
            }
            else
            {
                self.otherInfoView.isHidden = true
            }




            //Creating Other Second Info View
            if self.otherSecondInfoViewDataArray.count > 0
            {
                self.otherSecondInfoView.isHidden = false

                for view in self.otherSecondInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherSecondInfoView(dataArray: self.otherSecondInfoViewDataArray)
            }
            else
            {
                self.otherSecondInfoView.isHidden = true
            }



            self.loadValuesWhenAutoload(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type", arrayKey: "field_value", optionKey: "given_value", valueKey: "value", radioButtonArray: self.otherInfoRadioButtonArray, checkboxArray: self.otherInfoCheckboxArray)

            self.loadValuesWhenAutoload(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type", arrayKey: "opt_value", optionKey: "product_option", valueKey: "option_value\(self.langSuffix!)", radioButtonArray: self.otherSecondInfoRadioButtonArray, checkboxArray: self.otherSecondInfoCheckboxArray)


            //            self.heldValuesArray.removeAllObjects()
            //
            //            self.heldValuesArray = (UserDefaults.standard.object(forKey: "heldValuesArray") as! NSArray).mutableCopy() as! NSMutableArray
            //
            //            print(self.heldValuesArray)
            //
            //            self.putValuesAfterAPIFire(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            //            self.putValuesAfterAPIFire(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")
            //
            //            UserDefaults.standard.removeObject(forKey: "heldValuesArray")
        }



        /*
         let catArray = finalCatID.components(separatedBy: ",")

         self.selectCategory(selectedCategory: tempCount, dataArray: catArray) {

         self.categoryLoop(dataArray: catArray)

         }

         UserDefaults.standard.setValue(false, forKey: "autoLoad")
         */

    }


    // MARK:- // Load Values when Automatically loading

    func loadValuesWhenAutoload(parentView:UIView , dataArray:NSArray , typeKey:String , arrayKey:String , optionKey:String , valueKey:String , radioButtonArray:[[UIButton]] , checkboxArray:[[UIButton]])
    {

        var radioButtonSectionCount = 0
        var checkboxSectionCount = 0

        print(self.heldValuesArray)

        if dataArray.count > 0
        {
            for i in 0..<dataArray.count
            {
                // Text Field
                if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("T")
                {
                    for view in parentView.subviews[i].subviews
                    {
                        if view.isKind(of: UITextField.self)
                        {


                            (view as! UITextField).text = "\((dataArray[i] as! NSDictionary)[optionKey] ?? "")"


                        }
                    }
                }



                // List Views
                if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("S")
                {
                    for view in parentView.subviews[i].subviews
                    {
                        if view.isKind(of: UILabel.self)
                        {
                            if (view as! UILabel).restorationIdentifier != "titleLBL"
                            {
                                for j in 0..<((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray).count
                                {
                                    if "\((dataArray[i] as! NSDictionary)[optionKey] ?? "")".elementsEqual("\((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[j] as! NSDictionary)["id"] ?? "")")
                                    {
                                        (view as! UILabel).text = "\((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[j] as! NSDictionary)[valueKey] ?? "")"
                                    }
                                }
                            }
                        }
                    }
                }



                // Radio Buttons
                if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("R")
                {
                    var tempArray = [NSDictionary]()

                    for l in 0..<((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray).count
                    {
                        if "\((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[l] as! NSDictionary)["show"] ?? "")".elementsEqual("1")
                        {
                            tempArray.append((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[l] as! NSDictionary))
                        }
                    }

                    //                    print("asdadasdasd---",((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray))
                    //
                    //                    print("llklk---",(radioButtonArray[i]))
                    //                    print(tempArray)

                    if parentView == self.otherInfoView
                    {
                        for j in 0..<tempArray.count
                        {

                            if "\((dataArray[i] as! NSDictionary)[optionKey] ?? "")".elementsEqual("\((tempArray[j])["id"] ?? "")")
                            {
                                (radioButtonArray[radioButtonSectionCount])[j].isSelected = true

                                if "\((dataArray[i] as! NSDictionary)["field_name"] ?? "")".elementsEqual("Product type")
                                {
                                    UserDefaults.standard.setValue("\((tempArray[j])["id"] ?? "")", forKey: "productType")
                                }
                            }
                        }
                    }
                    else
                    {
                        for j in 0..<((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray).count
                        {

                            if "\((dataArray[i] as! NSDictionary)[optionKey] ?? "")".elementsEqual("\((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[j] as! NSDictionary)["id"] ?? "")")
                            {
                                (radioButtonArray[radioButtonSectionCount])[j].isSelected = true
                            }
                        }
                    }



                    radioButtonSectionCount = radioButtonSectionCount + 1

                }



                // Check Boxes
                if "\((dataArray[i] as! NSDictionary)[typeKey] ?? "")".elementsEqual("C")
                {
                    let tempArray = "\((dataArray[i] as! NSDictionary)[optionKey] ?? "")".components(separatedBy: ",")


                    for j in 0..<((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray).count
                    {

                        for l in 0..<tempArray.count
                        {
                            if "\(tempArray[l])".elementsEqual("\((((dataArray[i] as! NSDictionary)[arrayKey] as! NSArray)[j] as! NSDictionary)["id"] ?? "")")
                            {
                                (checkboxArray[checkboxSectionCount])[j].isSelected = true
                            }
                        }


                    }

                    checkboxSectionCount = checkboxSectionCount + 1

                }
            }
        }


    }


    func categoryLoop(dataArray: [String])
    {
        tempCount = tempCount + 1

        if tempCount < dataArray.count
        {
            self.selectCategory(selectedCategory: tempCount, dataArray: dataArray) {

                self.categoryLoop(dataArray: dataArray)

            }
        }
        else
        {
            self.heldValuesArray.removeAllObjects()

            self.heldValuesArray = (UserDefaults.standard.object(forKey: "heldValuesArray") as! NSArray).mutableCopy() as! NSMutableArray

            print("heldValuesArray===",heldValuesArray)

            self.putValuesAfterAPIFire(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            self.putValuesAfterAPIFire(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")

            UserDefaults.standard.removeObject(forKey: "heldValuesArray")
        }
    }


    // MARK:- // Select Category

    func selectCategory(selectedCategory : Int,dataArray : [String] , completion : @escaping () -> ())
    {
        self.otherInfoSubviews.removeAll()
        self.otherSecondInfoSubviews.removeAll()


        if selectedCategory == 0
        {
            self.topCatID = "\(dataArray[selectedCategory])"
            self.parentID = self.topCatID
            self.catID = self.topCatID
        }
        else if selectedCategory == 1
        {
            self.parentID = "\(dataArray[selectedCategory])"
            self.nextLabel = self.parentID
            self.catID = "\(self.topCatID!),\(self.parentID!)"
        }
        else if selectedCategory == 2
        {
            self.parentID = "\(dataArray[selectedCategory])"
            self.catID = "\(self.topCatID!),\(self.nextLabel!),\(self.parentID!)"
        }
        else
        {
            self.parentID = "\(dataArray[selectedCategory])"
            self.catID = "\(self.catID!),\(self.parentID!)"
        }


        for i in 0..<(self.allCategoriesDataArray[selectedCategory]).count
        {
            if "\(((self.allCategoriesDataArray[selectedCategory])[i] as! NSDictionary)["id"] ?? "")".elementsEqual(self.parentID)
            {
                self.categoryLBLArray[selectedCategory].text = "\(((self.allCategoriesDataArray[selectedCategory])[i] as! NSDictionary)["categoryname\(langSuffix!)"] ?? "")"
            }
        }




        self.tabletag = selectedCategory + 1


        self.getData()

        self.dispatchGroup.notify(queue: .main) {


            //            self.categoryViewArray[tableView.tag].frame.size.height = self.categoryViewArray[tableView.tag].frame.size.height - (150/568)*self.FullHeight
            //
            //            self.viewOfTableviewArray[tableView.tag].frame.size.height = 0
            //
            //            self.tableviewArray[tableView.tag].frame.size.height = 0
            //
            //            self.viewOfTableviewArray[tableView.tag].isHidden = true

            if (self.allDataDictionary["category_info"] as! NSArray).count > 0
            {

                //
                self.allCategoriesDataArray.append(self.allDataDictionary["category_info"] as! NSArray)

                //self.tabletag = self.tabletag + 1

                self.createViewWithTableview(yOrigin: CGFloat(self.allCategoriesDataArray.count - 1) * (60/568)*self.FullHeight, title: "Select Category", tableTag: self.tabletag)


                for i in 0..<self.allCategoriesDataArray.count
                {
                    self.tableviewArray[i].delegate = self
                    self.tableviewArray[i].dataSource = self
                    self.tableviewArray[i].reloadData()
                }
            }

            self.allCategoriesviewHeightConstraint.constant = CGFloat(self.allCategoriesDataArray.count) * (60/568)*self.FullHeight




            // Creating Other Info View
            if self.otherInfoViewDataArray.count > 0
            {


                self.otherInfoView.isHidden = false

                for view in self.otherInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherInfoView(dataArray: self.otherInfoViewDataArray)
            }
            else
            {
                self.otherInfoView.isHidden = true
            }




            //Creating Other Second Info View
            if self.otherSecondInfoViewDataArray.count > 0
            {
                self.otherSecondInfoView.isHidden = false

                for view in self.otherSecondInfoView.subviews
                {
                    view.removeFromSuperview()
                }
                self.createOtherSecondInfoView(dataArray: self.otherSecondInfoViewDataArray)
            }
            else
            {
                self.otherSecondInfoView.isHidden = true
            }




            completion()


            //            self.putValuesAfterAPIFire(parentView: self.otherInfoView, dataArray: self.otherInfoViewDataArray, typeKey: "field_type")
            //            self.putValuesAfterAPIFire(parentView: self.otherSecondInfoView, dataArray: self.otherSecondInfoViewDataArray, typeKey: "type")




        }

    }





    // MARK: - // JSON POST Method to get Listing Data with PRoduct ID

    func getListingDataWithProductID()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_post_category")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let productID = UserDefaults.standard.object(forKey: "productID") as! String
           
       
        self.stepFinal = UserDefaults.standard.object(forKey: "stepFinal") as? String

        if (langID?.elementsEqual("EN"))!
        {
            self.langSuffix = "_en"
        }
        else if (langID?.elementsEqual("AR"))!
        {
            self.langSuffix = "_ar"
        }

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&step_final=\(stepFinal ?? "")&product_id=\(productID)"


        print("Get Data with Product ID URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Get Data with Product ID Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json.copy() as? NSDictionary

                        self.otherInfoViewDataArray = json["other_info"] as? NSArray

                        self.otherSecondInfoViewDataArray = json["other_second_info"] as? NSArray

                        self.allCategoriesDataArray.removeAll()

                        for i in 0..<(self.allDataDictionary["given_catgeory_info"] as! NSArray).count
                        {
                            self.allCategoriesDataArray.append((self.allDataDictionary["given_catgeory_info"] as! NSArray)[i] as! NSArray)
                        }
                    }

                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }


                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }



}









