//
//  ThankYouViewController.swift
//  Kaafoo
//
//  Created by esolz on 16/04/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

class ThankYouViewController: GlobalViewController {

    @IBOutlet weak var messageTxt: UILabel!
    @IBOutlet weak var thankYou: UILabel!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backBtnOut.layer.cornerRadius = 10
        self.count = 0
        messageTxt.font = UIFont(name: self.messageTxt.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        thankYou.font = UIFont(name: self.thankYou.font.fontName, size: CGFloat(Get_fontSize(size: 30)))
        // Do any additional setup after loading the view.
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            //let randomNumber = Int.random(in: 1...20)
            //print("Number: \(randomNumber)")
            self.count = self.count + 1
            self.messageTxt.text = "You will be redirected to item list page in \(10 - self.count) second"
            if self.count == 10 {
                timer.invalidate()
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController

                self.navigationController?.pushViewController(navigate, animated: true)
            }
        }
    }
    

    @IBOutlet weak var backBtnOut: UIButton!
    @IBAction func btnBack(_ sender: UIButton) {
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController

        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
}
