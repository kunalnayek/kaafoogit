//
//  ListItemViewController.swift
//  Kaafoo
//
//  Created by admin on 09/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import CarbonKit




class ListItemViewController: GlobalViewController, CarbonTabSwipeNavigationDelegate {




    var instanceArray = [UIViewController]()
    
    var itemTapped : Int!
    
    var topCatID : String! = ""
    
    var isNewAdd: Bool!
    
    

    @IBOutlet weak var swipeableViewWithTabs: UIView!


    var items = NSArray()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()


    var myAccountBalance : Float! = 10000.00


    override func viewDidLoad() {
        super.viewDidLoad()


        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)


        if self.topCatID.isEmpty
        {

            self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Extras", "Confirm"])
        }
        else
        {

            if self.topCatID.elementsEqual("13")
            {
                self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Confirm"])
            }
            else if self.topCatID.elementsEqual("16")
            {
                self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Confirm"])
            }
            else if self.topCatID.elementsEqual("11")
            {
                self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Confirm"])
            }
            else
            {
                self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Extras", "Confirm"])
            }
        }







        UserDefaults.standard.set(myAccountBalance!, forKey: "myAccountBalance")


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(passDataAddPost), name: .passDataAddPost, object: nil)

    }







    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {

        switch index {
        case 0:

            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let basicInfo  = ss.instantiateViewController(withIdentifier:"basicInfoVC") as! BasicInfoViewController
            basicInfo.isNewAdd = self.isNewAdd

            return basicInfo

        case 1:
            
            //Details
          
            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let  details  = ss.instantiateViewController(withIdentifier:"postAdDetailsVC") as! postAdDetailsViewController

            return details
           

        case 2:

            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let  photos  = ss.instantiateViewController(withIdentifier:"postAdPhotosVC") as! PostAdPhotosViewController

            return photos

        case 3:

            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let  extras  = ss.instantiateViewController(withIdentifier:"extrasVC") as! ExtrasViewController

            return extras

        case 4:

            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let  confirm  = ss.instantiateViewController(withIdentifier:"confirmVC")

            return confirm

        default:

            let ss = UIStoryboard.init(name: "AutoLayout", bundle: nil)
            let basicInfo  = ss.instantiateViewController(withIdentifier:"basicInfoVC") as! BasicInfoViewController

            return basicInfo
        }

    }

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {

        nextPageOutlet.tag = Int(index)

        print("next index :========",  nextPageOutlet.tag)

    }


    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didFinishTransitionTo index: UInt) {
    }





    @IBOutlet weak var nextPageOutlet: UIButton!
    @IBAction func nextPageTap(_ sender: UIButton) {

        switch nextPageOutlet.tag {
        case 0:



            //            NotificationCenter.default.addObserver(forName: .passDataAddPost, object: nil, queue: OperationQueue.main) { (notification) in
            //
            //                let dataPassed = notification.object as! BasicInfoViewController
            //                print("Parameters----",dataPassed.params!)
            //                print("Submit Dictionary----",dataPassed.submitArraysDictionary)
            //
            //            }

            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(sender.tag + 1), withAnimation: true)


        case 1:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(sender.tag + 1), withAnimation: true)

        case 2:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(sender.tag + 1), withAnimation: true)

        case 3:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(sender.tag + 1), withAnimation: true)

        case 4:
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            self.navigationController?.pushViewController(navigate, animated: true)

        default:



            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(sender.tag + 1), withAnimation: true)


        }



    }


    // MARK:- // Notification Handler Function

    @objc func passDataAddPost(notification:Notification)
    {
        //        let dataPassed = "\(notification.object!)"
        //
        //        self.goNextOrNot(itemTapped: Int(dataPassed)!)
        let dataPassed = "\(notification.object!)"

        if dataPassed.elementsEqual("noExtra")
        {
            self.setupCarbonkit(dataArray: ["Basic Info", "Details", "Photos", "Confirm"])

            self.goNextOrNot(itemTapped: 0)
        }
        else
        {
            self.goNextOrNot(itemTapped: Int(dataPassed)!)
        }



    }




    // MARK:- // Go Next or Not

    func goNextOrNot(itemTapped : Int)
    {

        self.itemTapped = itemTapped

        switch self.itemTapped {

        case 0:

            //print("\(UserDefaults.standard.object(forKey: "postAddParams") ?? "")")
            //            print("\(UserDefaults.standard.object(forKey: "postAddDict") ?? "")")

            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(itemTapped + 1), withAnimation: true)

        case 1:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(itemTapped + 1), withAnimation: true)

        case 2:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(itemTapped + 1), withAnimation: true)

        case 3:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(itemTapped + 1), withAnimation: true)

        case 4:
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
            self.navigationController?.pushViewController(navigate, animated: true)

        default:
            carbonTabSwipeNavigation.setCurrentTabIndex(UInt(itemTapped + 1), withAnimation: true)


        }
    }
    // MARK:- // setUp Carbonkit

    func setupCarbonkit(dataArray: [String])
    {
        //items = ["Basic Info", "Details", "Photos", "Extras", "Confirm"]
        //carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)

        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: dataArray as [AnyObject], delegate: self)

        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: swipeableViewWithTabs)
        carbonTabSwipeNavigation.toolbar.backgroundColor = UIColor.white
        carbonTabSwipeNavigation.toolbar.isTranslucent = true
        carbonTabSwipeNavigation.toolbar.barTintColor = UIColor.white
        carbonTabSwipeNavigation.toolbar.alpha = 1.0
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.orange)
        carbonTabSwipeNavigation.setNormalColor(.white)
        carbonTabSwipeNavigation.setSelectedColor(.white)

        carbonTabSwipeNavigation.setNormalColor(UIColor.gray, font: headerView.headerViewTitle.font)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.black, font: UIFont(name: "Oswald-Bold", size: 25)!)

        //carbonTabSwipeNavigation.setTabBarHeight((50/568)*FullHeight)
        carbonTabSwipeNavigation.setTabBarHeight(60)
    }


}






extension Notification.Name {

    static let passDataAddPost = Notification.Name(rawValue: "passDataAddPost")

}

