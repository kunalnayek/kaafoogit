//
//  UnsoldItemsViewController.swift
//  Kaafoo
//
//  Created by admin on 31/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class UnsoldItemsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    var startValue = 0

    var perLoad = 10
    
    var scrollBegin : CGFloat!

    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var unsoldItemsArray : NSMutableArray! = NSMutableArray()
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var unsoldItemsTableVIew: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        unsoldItems()

        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
              self.unsoldItemsTableVIew.isHidden = true
              self.noDataFound(mainview: self.view)
            }
            else
            {
              print("do nothing")
            }
        })
        
        set_font()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
    }
    
    // MARK:- // Delegate Methods
    // MARK:- // TableView Delegates Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! UnsoldItemsTVCTableViewCell
        
        cell.productName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        cell.unsoldStatusLBL.text = "\((recordsArray[indexPath.row] as! NSDictionary)["unsold_status"]!)"
        
        cell.listingtype.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_type"]!)"
        cell.listingtype.textColor = .red
        
//        cell.topCategoryLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "topCategory", comment: "")

        cell.Product_Address.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"]!)"

        cell.topCategoryLBL.text = "Top Category:" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["top_category_name"]!)"
        
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        if (langID?.elementsEqual("EN"))!
        {
            
            cell.bidCount.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bidPlural", comment: "") + "\((recordsArray[indexPath.row] as! NSDictionary)["bid_count"]!)"
            
        }
        else
        {
            
            cell.bidCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["bid_count"]!)" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "bidPlural", comment: "")
            
        }
        
        
        cell.productPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
        
        cell.productImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"))
        
        cell.bidFlag.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["flag"]!)"))
        
        // Set Font
        
        cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        cell.unsoldStatusLBL.font = UIFont(name: cell.unsoldStatusLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.topCategoryLBL.font = UIFont(name: cell.topCategoryLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
         cell.listingtype.font = UIFont(name: cell.listingtype.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
//        cell.topCategory.font = UIFont(name: cell.topCategory.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
//        cell.bidCount.font = UIFont(name: cell.bidCount.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productPrice.font = UIFont(name: cell.productPrice.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        cell.cellView.layer.borderWidth = 2
        cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
//        navigate.ProductID = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
//        self.navigationController?.pushViewController(navigate, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (145/568)*self.FullHeight
        
    }

    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
            print("next start : ",nextStart )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                
            }
            else
            {
                unsoldItems()
            }
            
        }
    }
    
    
    
    // MARK: - // JSON POST Method to get Items I am Selling Data
    func unsoldItems()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_unsold_items", param: parameters, completion: {
            self.unsoldItemsArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.unsoldItemsArray.count - 1)

            {

                let tempDict = self.unsoldItemsArray[i] as! NSDictionary
                print("tempdict is : " , tempDict)

                self.recordsArray.add(tempDict as! NSMutableDictionary)


            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.unsoldItemsArray.count

            print("Next Start Value : " , self.startValue)


            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.unsoldItemsTableVIew.delegate = self
                self.unsoldItemsTableVIew.dataSource = self
                self.unsoldItemsTableVIew.reloadData()
                self.unsoldItemsTableVIew.rowHeight = UITableView.automaticDimension
                self.unsoldItemsTableVIew.estimatedRowHeight = UITableView.automaticDimension
                SVProgressHUD.dismiss()
            }
        })
    }
    
    // MARK:- // SETTING FONTS
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
//    func NoData()
//    {
//        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
//        nolbl.textAlignment = .right
//        nolbl.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
//        view.addSubview(nolbl)
//        nolbl.bringSubview(toFront: view)
//    }

    
    
}
//MARK:- // Unsold Items Tableviewcell
class UnsoldItemsTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var Product_Address: UILabel!
    @IBOutlet weak var DeleteBtnOutlet: UIButton!
    @IBOutlet weak var ProductDiscountPrice: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var unsoldStatusLBL: UILabel!
    @IBOutlet weak var topCategoryLBL: UILabel!
    @IBOutlet weak var topCategory: UILabel!
    @IBOutlet weak var bidView: UIView!
    @IBOutlet weak var bidCount: UILabel!
    @IBOutlet weak var bidFlag: UIImageView!
    @IBOutlet weak var productPrice: UILabel!

    @IBOutlet weak var listingtype: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

