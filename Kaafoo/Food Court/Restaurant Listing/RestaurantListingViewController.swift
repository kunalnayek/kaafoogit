//
//  RestaurantListingViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 17/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos
import GoogleMaps
import GooglePlaces

import SVProgressHUD

class RestaurantListingViewController: GlobalViewController,GMSMapViewDelegate {
    
    var isMarkerActive : Bool! = false
    
    var selectedMarker : GMSMarker!
    
    @IBOutlet weak var GmapView: GMSMapView!
    
    var ProductsOnMapArray = [MapListing]()

    @IBOutlet weak var secondaryHeaderview: UIView!
    @IBOutlet weak var listTableview: UITableView!
    @IBOutlet weak var listCollectionview: UICollectionView!
    @IBOutlet weak var restaurantMapView: mapView!
    @IBOutlet weak var viewOfPicker: UIView!
    @IBOutlet weak var PickerView: UIPickerView!
    @IBOutlet weak var viewOfPickerTopConstraint: NSLayoutConstraint!

    var restaurantListArray = NSArray()

    var recordsArray = NSMutableArray()

    var currentRecordsArray = NSMutableArray()

    var startValue = 0
    
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var searchLat : String!

    var searchLong : String!

    var searchCategoryID : String!

    var searchCategoryName : String!

    var sortType : String!

    var ratingValue : String!

    let dispatchGroup = DispatchGroup()

    var allDataDictionary : NSDictionary!

    var restaurantListMapArray = [NSDictionary]()

    var sortTypeArray = [["key" : "" , "value" : "All"] , ["key" : "T" , "value" : "Name"] , ["key" : "R" , "value" : "Review"]]

    var cellBottomViewArray = [[String]]()

    var backgroundViewArray = [UIView]()
    
    var SelectedSortPickerRow : Int! = 0


    // MARK:- // ViewDidload

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.GmapView.isHidden = true
        
        self.restaurantMapView.isHidden = true
        
        self.GmapView.delegate = self
        
        self.ProductsOnMapArray = []

        if self.searchCategoryID != nil {
            UserDefaults.standard.set(self.searchCategoryID, forKey: "foodCourtSearchCategoryID")

        }

        self.CreateFavouriteView(inview: self.view)

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.viewOfPickerTopConstraint.constant = self.FullHeight

        self.PickerView.delegate = self

        self.getRestaurantList()

        self.dispatchGroup.notify(queue: .main) {

            self.restaurantMapView.getData(dataArray: self.restaurantListMapArray, listingDataArray: self.currentRecordsArray as! [NSDictionary])

        }

        self.defineSortButtonActions()
        
        //self.CreateToolBar()

    }
    
    func CreateToolBar()
    {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.barTintColor = .black
        
        toolBar.tintColor = .white
        
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePickerView))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPickerView))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        self.viewOfPicker.addSubview(toolBar)
        
        toolBar.anchor(top: nil, leading: self.viewOfPicker.leadingAnchor, bottom: self.viewOfPicker.topAnchor, trailing: self.viewOfPicker.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
    }
    
    @objc func donePickerView()
    {
        print("Done")
    }
    
    @objc func cancelPickerView()
    {
        print("Cancel")
    }
    
    func AppendMapData()
    {
        self.dispatchGroup.enter()
        
        for i in 0..<self.recordsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((recordsArray[i] as! NSDictionary)["businessname"] ?? "")", ProductLatitude: Double(("\((recordsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((recordsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((recordsArray[i] as! NSDictionary)["logo"] ?? "")", CurrencySymbol: "0", StartPrice: "0" , ReservePrice: "0", ProductID: "\((recordsArray[i] as! NSDictionary)["id"] ?? "")", SellerImage: "", WatchlistStatus: ""))
        }
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
            //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.GmapView.animate(to: cameraPosition)
            
            self.GmapView.isMyLocationEnabled = true
            self.GmapView.settings.myLocationButton = true
            
            
            for state in self.ProductOnMapArray {
                let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                state_marker.title = state.ProductName
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                state_marker.map = self.GmapView
                
            }
        })
    }



    // MARK:- // ViewWillAppear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.listTableview.estimatedRowHeight = 300
        self.listTableview.rowHeight = UITableView.automaticDimension

    }




    // MARK:- // Buttons

    // MARK;- // Toggole Between List and Grid Button

    @IBOutlet weak var toggoleListGridButtonOutlet: UIButton!
    @IBAction func toggleListGrid(_ sender: UIButton) {

        if listTableview.isHidden == true {
            self.listTableview.isHidden = false
            self.listCollectionview.isHidden = true
            self.restaurantMapView.isHidden = true
            self.GmapView.isHidden = true
        }
        else
        {
            self.listTableview.isHidden = true
            self.listCollectionview.isHidden = false
            self.restaurantMapView.isHidden = true
            self.GmapView.isHidden = true
        }

    }



    // MARK:- // Open MapView


    @IBOutlet weak var openMapviewButton: UIButton!
    @IBAction func openMapview(_ sender: UIButton) {

        if self.GmapView.isHidden == true {
            self.listTableview.isHidden = true
            self.listCollectionview.isHidden = true
            self.GmapView.isHidden = false
        }
        else {
            print("Map is Already Visible.")
        }

    }



    // MARK:- // Open Pickerview Button

    @IBOutlet weak var openPickerviewButtonOutlet: UIButton!
    @IBAction func openPickerviewButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

    }



    // MARK:- // Close Pickerview Button

    @IBOutlet weak var closePickerviewButtonOutlet: UIButton!
    @IBAction func closePickerviewButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = self.FullHeight

        }

    }


    // MARK:- // Define Filter Button Actions

    func defineSortButtonActions() {

        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)


        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

    }


    // MARK:- // Sort Action

    @objc func sortAction(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = 0

        }
    }

    // MARK:- // Filter Action

    @objc func filterAction(sender: UIButton) {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController

        navigate.showRating = true
        navigate.showDeliveryCost = true
        navigate.showDistance = false


        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK:- // Sort Action

    @objc func tapToClose(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

    }







    // MARK: - // JSON POST Method to get Food Court List

    func getRestaurantList()

    {

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        let searchCategory = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryID")

        print(self.filterRating!)

        parameters = "lang_id=\(langID!)&mycountry_name=\(myCountryName!)&search_cat=\(searchCategory ?? "")&search_sort=\(sortType ?? "")&ratingvalue=\(self.filterRating ?? "")&per_load=\(perLoad)&start_value=\(startValue)&search_lat=\(searchLat ?? "")&search_long=\(searchLong ?? "")&delivery_type=\(self.filterDeliveryType ?? "")"

        self.CallAPI(urlString: "app_restaurent_listing", param: parameters) {

            self.allDataDictionary = self.globalJson

            self.createData()

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.listTableview.delegate = self
                self.listTableview.dataSource = self
                self.listTableview.reloadData()


                self.listCollectionview.delegate = self
                self.listCollectionview.dataSource = self
                self.listCollectionview.reloadData()
                
                self.AppendMapData()

                SVProgressHUD.dismiss()
            }

        }


    }

    // MARK:- // Create Data Arrays

    func createData() {

        self.restaurantListArray = (self.allDataDictionary["info_array"] as! NSDictionary)["product"] as! NSArray


        //Creating Cell Bottom View Array
        for i in 0..<self.restaurantListArray.count {
            self.cellBottomViewArray.append(["\((self.restaurantListArray[i] as! NSDictionary)["delivery_type"] ?? "")","Min: $10:00","30-45 Mins","\((self.restaurantListArray[i] as! NSDictionary)["dstnc_km"] ?? "")"])

        }


        for i in 0..<(self.restaurantListArray.count-1) {

            self.restaurantListMapArray.append(["address" : "\((self.restaurantListArray[i] as! NSDictionary)["address"] ?? "")" , "lat" : "\((self.restaurantListArray[i] as! NSDictionary)["lat"] ?? "")" , "long" : "\((self.restaurantListArray[i] as! NSDictionary)["long"] ?? "")"])

        }


        for i in 0...(self.restaurantListArray.count-1) {

            let tempDict = self.restaurantListArray[i] as! NSDictionary

            self.recordsArray.add(tempDict as! NSMutableDictionary)

        }

        self.currentRecordsArray = self.recordsArray.mutableCopy() as! NSMutableArray

        self.nextStart = "\(self.allDataDictionary["next_start"]!)"

        self.startValue = self.startValue + self.restaurantListArray.count

        print("Next Start Value : " , self.startValue)


        //Getting Food Type Name to send to next Screen

        let searchCategory = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryID")

        for i in 0..<((self.allDataDictionary["info_array"] as! NSDictionary)["category"] as! NSArray).count {

            if searchCategory!.elementsEqual("\((((self.allDataDictionary["info_array"] as! NSDictionary)["category"] as! NSArray)[i] as! NSDictionary)["id"]!)") {

                self.searchCategoryName = "\((((self.allDataDictionary["info_array"] as! NSDictionary)["category"] as! NSArray)[i] as! NSDictionary)["cat_name"] ?? "")"

            }

        }

    }


}



// MARK:- // Tableview Delegates

extension RestaurantListingViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.currentRecordsArray.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantListingTable") as! RestaurantListingTVCTableViewCell

        cell.restaurantImageview.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"] ?? "")"))

        cell.restaurantName.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"] ?? "")"

        if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"] ?? "")".isEmpty
        {
            cell.ratingView.rating = 0
        }
        else
        {
            cell.ratingView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"] ?? "")")!
        }



        
        cell.restaurantAddress.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"] ?? "")"

        cell.deliveryType.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["delivery_type"] ?? "")"
        cell.minPrice.text = "Min:" + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["minimum_price"] ?? "")" + "$"
        
//        if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["minimum_time"] ?? "")".elementsEqual("0 min")
//        {
//            cell.eta.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["minimum_time"] ?? "")" + "-" +  "\((currentRecordsArray[indexPath.row] as! NSDictionary)["maximum_time"] ?? "")"
//        }
//        else
//        {
            cell.eta.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["minimum_time"] ?? "")" + "-" +  "\((currentRecordsArray[indexPath.row] as! NSDictionary)["maximum_time"] ?? "")" + " Mins"
      //  }
        cell.distance.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["dstnc_km"] ?? "")"

        cell.totalRating.text =  "\((currentRecordsArray[indexPath.row] as! NSDictionary)["total_review"] ?? "")" + " Reviews"

        //cell.deliveryType.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["delivery_type"] ?? "")"


        /*
         let cellBottomViewWidth = cell.cellBottomView.frame.size.width

         cell.cellBottomView.removeConstraint(cell.cellBottomViewHeightConstraint)

         var widthTillNow = 0
         var currentStringWidth = 0
         var nextStringWidth = 0

         for i in 0..<self.cellBottomViewArray.count {

         var backgroundView : UIView = {
         let backgroundview = UIView()
         backgroundview.translatesAutoresizingMaskIntoConstraints = false
         return backgroundview
         }()

         self.backgroundViewArray.append(backgroundView)

         var insideLBL : UILabel = {
         let insidelbl = UILabel()
         insidelbl.font = UIFont(name: cell.restaurantAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
         insidelbl.textAlignment = .natural
         insidelbl.translatesAutoresizingMaskIntoConstraints = false
         return insidelbl
         }()

         var separatorView : UIView = {
         let separatorview = UIView()
         separatorview.backgroundColor = .black
         separatorview.translatesAutoresizingMaskIntoConstraints = false
         return separatorview
         }()

         cell.cellBottomView.addSubview(backgroundView)
         cell.backgroundView.addSubview(insideLBL)
         cell.backgroundView.addSubview(separatorView)

         currentStringWidth = (self.cellBottomViewArray[indexPath.row])[i].width(withConstrainedHeight: 20, font: CGFloat(self.Get_fontSize(size: 11)))

         if (i + 1) < self.cellBottomViewArray.count {
         nextStringWidth = (self.cellBottomViewArray[indexPath.row])[i + 1].width(withConstrainedHeight: 20, font: CGFloat(self.Get_fontSize(size: 11)))
         }
         else {

         }


         widthTillNow = widthTillNow + currentStringWidth

         if (widthTillNow + 8) > cellBottomViewWidth {

         }
         else {




         if i > 0 {
         backgroundView.anchor(top: cell.cellBottomView.topAnchor, leading: self.backgroundViewArray[i-1].trailingAnchor, bottom: cell.cellBottomView.bottomAnchor, trailing: nil)
         separatorView.anchor(top: self.backgroundViewArray[i].topAnchor, leading: self.backgroundViewArray[i].leadingAnchor, bottom: self.backgroundViewArray[i].bottomAnchor, trailing: nil, size: .init(width: 2, height: 0))
         insideLBL.anchor(top: backgroundViewArray[i].topAnchor, leading: separatorView.trailingAnchor, bottom: backgroundViewArray[i].bottomAnchor, trailing: backgroundViewArray[i].trailingAnchor, padding: .init(top: 0, left: 3, bottom: 0, right: 3), size: .init(width: 0, height: 20))

         }
         else {
         backgroundView.anchor(top: cell.cellBottomView.topAnchor, leading: cell.cellBottomView.leadingAnchor, bottom: cell.cellBottomView.bottomAnchor, trailing: nil)
         insideLBL.anchor(top: backgroundViewArray[i].topAnchor, leading: backgroundViewArray[i].leadingAnchor, bottom: backgroundViewArray[i].bottomAnchor, trailing: backgroundViewArray[i].trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 3), size: .init(width: 0, height: 20))

         separatorView.isHidden == true

         }
         }




         }
         */


        //Set Font
        cell.restaurantName.font = UIFont(name: cell.restaurantName.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        cell.restaurantAddress.font = UIFont(name: cell.restaurantAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        cell.totalRating.font = UIFont(name: cell.totalRating.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.deliveryType.font = UIFont(name: cell.deliveryType.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.minPrice.font = UIFont(name: cell.minPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.eta.font = UIFont(name: cell.eta.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.distance.font = UIFont(name: cell.distance.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))

        cell.cellView.layer.cornerRadius = 8

        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell

    }



    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "foodListingVC") as! FoodListingViewController

        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController

        UserDefaults.standard.set(self.searchCategoryName, forKey: "foodCourtSearchCategoryName")
        
       // UserDefaults.standard.set("\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"] ?? "")", forKey: "foodCourtSellerID")
        
         UserDefaults.standard.set("\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"] ?? "")", forKey: "foodCourtSellerID")
        
       
        
        let tempval = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"] ?? "")"

         print("tempval-----",tempval)
        
        navigate.sellerID = tempval
        
        
        UserDefaults.standard.set("\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"] ?? "")", forKey: "foodCourtSellerName")
        
        UserDefaults.standard.set("\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"] ?? "")", forKey: "foodCourtSellerImageURL")
        
        self.navigationController?.pushViewController(navigate, animated: true)

    }


}



// MARK:- // Scrollview Delegates

extension RestaurantListingViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getRestaurantList()
            }
        }
    }
}




// MARK:- // Collectionview Delegates

extension RestaurantListingViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {



    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.currentRecordsArray.count

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restaurantListingCollectionview", for: indexPath) as! RestaurantListingCVCCollectionViewCell

        cell.restaurantImageview.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"] ?? "")"))

        cell.restaurantName.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"] ?? "")"

        if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"] ?? "")".isEmpty
        {
            cell.ratingView.rating = 0
        }
        else
        {
            cell.ratingView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"] ?? "")")!
        }



        //cell.ratingView.settings.filledImage = UIImage(named: "Shape")

        cell.restaurantAddress.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"] ?? "")"

        cell.deliveryType.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["delivery_type"] ?? "")"





        //Set Font
        cell.restaurantName.font = UIFont(name: cell.restaurantName.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        cell.restaurantAddress.font = UIFont(name: cell.restaurantAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        //cell.totalRating.font = UIFont(name: cell.totalRating.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
        cell.deliveryType.font = UIFont(name: cell.deliveryType.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        cell.minPrice.font = UIFont(name: cell.minPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        cell.ETA.font = UIFont(name: cell.ETA.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        cell.distance.font = UIFont(name: cell.distance.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))

        cell.cellView.layer.cornerRadius = 8

        return cell

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: ((self.FullWidth - 10) / 2), height: 315)

    }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "foodListingVC") as! FoodListingViewController

        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController


        navigate.sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"] ?? "")"
        navigate.seller = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"] ?? "")"
        navigate.sellerImageURL = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"] ?? "")"

        navigate.searchCategoryID = searchCategoryID

        navigate.searchCategoryName = self.searchCategoryName



        self.navigationController?.pushViewController(navigate, animated: true)



    }


}




// MARK:- // Pickerview Delegates


extension RestaurantListingViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }


    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return sortTypeArray.count

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return (self.sortTypeArray[row])["value"]

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.startValue = 0
//        self.allDataDictionary.removeAllObjects()
        self.recordsArray.removeAllObjects()
        self.currentRecordsArray.removeAllObjects()

        self.sortType = (self.sortTypeArray[row])["key"]

        self.getRestaurantList()

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = self.FullHeight

        }
    }

}

class RestaurantListingTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var restaurantImageview: UIImageView!
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var totalRating: UILabel!
    @IBOutlet weak var addressPlaceholderImageview: UIImageView!
    @IBOutlet weak var restaurantAddress: UILabel!
    @IBOutlet weak var cellBottomView: UIView!
    @IBOutlet weak var deliveryType: UILabel!
    @IBOutlet weak var minPrice: UILabel!
    @IBOutlet weak var eta: UILabel!
    @IBOutlet weak var distance: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class RestaurantListingCVCCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var restaurantImageview: UIImageView!
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var addressPlaceholderImageview: UIImageView!
    @IBOutlet weak var restaurantAddress: UILabel!
    @IBOutlet weak var cellBottomView: UIView!
    @IBOutlet weak var deliveryType: UILabel!
    @IBOutlet weak var minPrice: UILabel!
    @IBOutlet weak var ETA: UILabel!
    @IBOutlet weak var distance: UILabel!
    
    
}





