//
//  FoodCourtMainFilterProductsViewController.swift
//  Kaafoo
//
//  Created by admin on 03/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class FoodCourtMainFilterProductsViewController: GlobalViewController {

    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var backImageview: UIImageView!
    @IBOutlet weak var searchRatingView: UIView!
    @IBOutlet weak var ratingTitleLBL: UILabel!
   // @IBOutlet weak var nextImageview: UIImageView!
    @IBOutlet weak var searchRatingViewSeparator: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var ratingViewSeparator: UIView!
    
    var ratingValue : String! = ""
    
    var RatingSelected : Bool! = false
    
    var searchCategoryID : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        starView.didTouchCosmos = didTouchCosmos
        starView.didFinishTouchingCosmos = didFinishTouchingCosmos
        
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func tapOnBackButton(_ sender: UIButton) {
        
        if self.ratingView.isHidden == true
        {

            self.navigationController?.popViewController(animated: true)
            
        }
        else
        {
            UIView.animate(withDuration: 0.3) {
                
                self.view.sendSubviewToBack(self.ratingView)
                self.ratingView.isHidden = true
                //self.nextImageview.isHidden = false
                self.nextButtonOutlet.isHidden = false
                
            }
            
        }
        
    }
    
    
    // MARK:- // Next Button
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func tapOnNextButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3) {
            
            self.view.bringSubviewToFront(self.ratingView)
            self.ratingView.isHidden = false
            //self.nextImageview.isHidden = true
            self.nextButtonOutlet.isHidden = true
            
        }
        
    }
    
    
    
    // MARK:- // Reset Button
    
    @IBOutlet weak var resetButtonOutlet: UIButton!
    @IBAction func tapOnReset(_ sender: UIButton) {
        
        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
        
        self.ratingValue = ""
        
        navigate.ratingValue = "\(ratingValue!)"
        
        self.RatingSelected = false
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK:- // Apply Button
    
    @IBOutlet weak var applyButtonOutlet: UIButton!
    @IBAction func tapOnApplyButton(_ sender: UIButton) {
        
        if RatingSelected == false
        {
            if (ratingValue?.isEmpty)!
            {
                let alert = UIAlertController(title: "Select Rating", message: "Select a Rating to Filter", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else
            {
                
                UIView.animate(withDuration: 0.3) {
                    
                    self.applyButtonOutlet.setTitle("DONE", for: .normal)
                    self.ratingTitleLBL.text = "Rating ( " + "\(self.ratingValue!)" + " )"
                    
                    self.view.sendSubviewToBack(self.ratingView)
                    self.ratingView.isHidden = true
                    //self.nextImageview.isHidden = false
                    self.nextButtonOutlet.isHidden = false
                    
                    self.RatingSelected = true
                    
                }
            }
            
        }
        else
        {
            
//            let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
            
            navigate.ratingValue = "\(ratingValue!)"
            
            navigate.searchCategoryID = searchCategoryID
            
            self.RatingSelected = false
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        
    }
    
    

    // Cosmos Delegates
    
    private func didTouchCosmos(_ rating: Double) {
        
        ratingValue = ItemsWonViewController.formatValue(rating)
        
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        
        ratingValue = ItemsWonViewController.formatValue(rating)
        
    }

}
