//
//  FoodListingViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 13/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD


class FoodListingViewController: GlobalViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var categorySortAndMapView: UIView!
    @IBOutlet weak var categoryLBL: UILabel!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var accountView: UIView!
    @IBOutlet weak var sellerImage: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var mainStackview: UIStackView!
    @IBOutlet weak var popularDishesView: UIView!
    @IBOutlet weak var popularDishesTitleLBL: UILabel!
    @IBOutlet weak var popularDishesTableview: UITableView!
    @IBOutlet weak var allListingView: UIView!
    @IBOutlet weak var allListingStackView: UIStackView!
    @IBOutlet weak var allListingTitleLBL: UILabel!
    @IBOutlet weak var listingCategoryTitleView: UIView!
    @IBOutlet weak var listingCategoryImage: UIImageView!
    @IBOutlet weak var listingCategoryTitleLBl: UILabel!
    @IBOutlet weak var listingCategoryTableview: UITableView!
    @IBOutlet weak var popularDishesTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var listingCategoryTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewOfPicker: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewOfPickerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var foodListingView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var reviewsTitleLBL: UILabel!
    @IBOutlet weak var reviewsTableview: UITableView!
    @IBOutlet weak var reviewsTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewOfImagescroll: UIView!
    @IBOutlet weak var viewOfimagescrollHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewOfImagescrollWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageScroll: UIScrollView!

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var deliveryTimesTitleLBL: UILabel!
    @IBOutlet weak var deliveryTimesTableview: UITableView!
    @IBOutlet weak var deliveryTimesTableHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var paymentMethodView: UIView!
    @IBOutlet weak var paymentMethodTitleLBL: UILabel!
    @IBOutlet weak var paymentMethodTableview: UITableView!
    @IBOutlet weak var paymentMethodTableviewHeightConstraint: NSLayoutConstraint!



    @IBOutlet weak var foodlistMapview: mapView!






    var imageScrollContentview : UIView = {
        let imagescrollcontentview = UIView()
        imagescrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        //imagescrollcontentview.backgroundColor = .green
        return imagescrollcontentview
    }()




    let dispatchGroup = DispatchGroup()

    var allDataDictionary : NSDictionary!


    var seller : String!

    var sellerID : String!

    var sellerImageURL : String!

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    var currentRecordsArray : NSMutableArray! = NSMutableArray()

    var restaurantListArray = NSMutableArray()

    var listingType : String! = "P"

    var searchCategoryID : String! //= "pd"

    var searchCategoryName : String!

    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)

    var tapGesture = UITapGestureRecognizer()

    let imagePageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 0, height: 0))

    var categoryArray : NSArray!

    var reviewArray : NSArray!

    var reviewRecordsArray = NSMutableArray()

    var reviewCurrentRecordsArray = NSMutableArray()

    var infoDictionary : NSDictionary!

    var paymentTypeArray : NSArray!

    var popularDishesArray = NSMutableArray()

    var categoryListArray = NSMutableArray()

    var response : Bool!

    var closeImagescrollTapGesture = UITapGestureRecognizer()

    var showMap : Bool! = true

    var mapArray = [NSDictionary]()
    var dataArray = [NSDictionary]()


    // MARK:- // ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //GettingUserDefaults
        self.searchCategoryID = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryID")
        self.searchCategoryName = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryName")
        self.seller = UserDefaults.standard.string(forKey: "foodCourtSellerName")
        self.sellerID = UserDefaults.standard.string(forKey: "foodCourtSellerID")
        self.sellerImageURL = UserDefaults.standard.string(forKey: "foodCourtSellerImageURL")

        self.defineSortButtonActions()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.setLanguageStrings()

        self.SetFont()

        self.categoryLBL.text = self.searchCategoryName ?? ""

        self.viewOfPickerTopConstraint.constant = self.FullHeight

        self.sellerName.text = self.seller

        self.sellerImage.sd_setImage(with: URL(string: "\(self.sellerImageURL ?? "")"))

        self.getFoodListings()

    }


    // MARK:- // ViewWillAppear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.popularDishesTableview.estimatedRowHeight = 500
        self.popularDishesTableview.rowHeight = UITableView.automaticDimension

        self.listingCategoryTableview.estimatedRowHeight = 500
        self.listingCategoryTableview.rowHeight = UITableView.automaticDimension

        self.reviewsTableview.estimatedRowHeight = 500
        self.reviewsTableview.rowHeight = UITableView.automaticDimension

        self.deliveryTimesTableview.estimatedRowHeight = 500
        self.deliveryTimesTableview.rowHeight = UITableView.automaticDimension

        self.paymentMethodTableview.estimatedRowHeight = 500
        self.paymentMethodTableview.rowHeight = UITableView.automaticDimension

    }



    // MARK:- // ViewDidAppear

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()


            self.setTableviewHeight(tableview: self.popularDishesTableview, heightConstraint: self.popularDishesTableHeightConstraint)
            self.setTableviewHeight(tableview: self.listingCategoryTableview, heightConstraint: self.listingCategoryTableHeightConstraint)

        }

    }



    // MARK:- //  Buttons

    // MARK:- // Select Category Button

    @IBOutlet weak var selectCategoryButtonOutlet: UIButton!
    @IBAction func selectCategoryButtonTap(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = 0

        }

    }

    @IBOutlet weak var sortAndMapView: UIView!

    // MARK:- // Sort Button

    @IBOutlet weak var sortButtonOutlet: UIButton!
    @IBAction func sortButtonTap(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

        
    }


    // MARK:- // Map Button

    @IBOutlet weak var mapButtonOutlet: UIButton!
    @IBAction func mapButtonTap(_ sender: UIButton) {



        if self.showMap == true {

            self.startValue = 0

            self.listingType = "C"

            self.mapButtonOutlet.setImage(UIImage(named: "list"), for: .normal)

            self.getFoodListings()

            self.globalDispatchgroup.notify(queue: .main) {

                self.foodlistMapview.getData(dataArray: self.mapArray, listingDataArray: self.currentRecordsArray as! [NSDictionary])

                self.accordingToListingType()

            }
        }
        else {
            self.listingType = "P"

            self.foodListingView.isHidden = false
            self.reviewsView.isHidden = true
            self.infoView.isHidden = true
            self.foodlistMapview.isHidden = true

            self.mapButtonOutlet.setImage(UIImage(named: "Map-1"), for: .normal)

            self.showMap = true
        }

    }


    // MARK:- // Review Button

    @IBOutlet weak var reviewsButtonOutlet: UIButton!
    @IBAction func reviewsButtonTap(_ sender: UIButton) {

        self.startValue = 0

        self.listingType = "R"

        self.mapButtonOutlet.setImage(UIImage(named: "list"), for: .normal)

        self.showMap = false

        self.getFoodListings()

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

            self.setTableviewHeight(tableview: self.reviewsTableview, heightConstraint: self.reviewsTableHeightConstraint)

        }




    }




    // MARK:- // Info Button

    @IBOutlet weak var infoButtonOutlet: UIButton!
    @IBAction func infoButtonTap(_ sender: UIButton) {

        self.startValue = 0

        self.listingType = "C"

        self.mapButtonOutlet.setImage(UIImage(named: "list"), for: .normal)

        self.showMap = false

        self.getFoodListings()

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

            self.setTableviewHeight(tableview: self.deliveryTimesTableview, heightConstraint: self.deliveryTimesTableHeightConstraint)

            self.setTableviewHeight(tableview: self.paymentMethodTableview, heightConstraint: self.paymentMethodTableviewHeightConstraint)

        }

    }


    // MARK:- // Close Pickerview Button

    @IBOutlet weak var closePickerButtonOutlet: UIButton!
    @IBAction func closePickerButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = self.FullHeight

        }

    }

    
    
    
    // MARK:- // Define Filter Button Actions
    
    func defineSortButtonActions() {
        
        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)
        
        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)
        
    }
    
    
    // MARK;- // Filter Action
    
    @objc func filterAction(sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController
        
        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "foodListingVC") as! FoodListingViewController
        
        navigate.showDeliveryCost = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK;- // Sort Action
    
    @objc func tapToClose(sender: UIButton) {
        
        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)
        
    }
    
    



    // MARK: - // JSON POST Method to get Restaurant List

    func getFoodListings()

    {
        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        parameters = "login_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&seller_id=\(sellerID!)&mycountry_name=\(myCountryName!)&listing_type=\(listingType!)&search_cat=\(searchCategoryID!)&delivery_type=\(self.filterDeliveryType ?? "")"

        self.CallAPI(urlString: "app_restaurent_details", param: parameters) {



            if self.listingType.elementsEqual("P") {

                self.allDataDictionary = self.globalJson["info_array"] as? NSDictionary

                self.nextStart = "\(self.globalJson["next_start"]!)"

            }
            else if self.listingType.elementsEqual("R") {

                self.reviewArray = self.globalJson["info_array"] as? NSArray

            }
            else if self.listingType.elementsEqual("C") {

                self.infoDictionary = (self.globalJson["info_array"] as! NSArray)[0] as? NSDictionary
                self.paymentTypeArray = self.infoDictionary["payment_type"] as? NSArray

                for i in 0..<(self.infoDictionary["map_details"] as! NSArray).count {
                    self.mapArray.append((self.infoDictionary["map_details"] as! NSArray)[i] as! NSDictionary)
                }

            }

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }

        }

    }




    // MARK:- // Set Product Details Tableview Height

    func setTableviewHeight(tableview: UITableView,heightConstraint: NSLayoutConstraint)
    {

        var height : CGFloat! = 0

        tableview.frame.size.height = 3000

        for cell in tableview.visibleCells {
            height += cell.bounds.height
        }

        heightConstraint.constant = height


    }



    // MARK:- // Show Category

    func setLayout()
    {

        if self.currentRecordsArray.count == 0
        {
            self.allListingView.isHidden = true
            self.popularDishesView.isHidden = true
        }
        else
        {
            self.allListingView.isHidden = false
            self.popularDishesView.isHidden = false
        }

        for i in 0..<(self.allDataDictionary["category_list"] as! NSArray).count {

            if "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")".elementsEqual(self.searchCategoryID!) {

                self.listingCategoryImage.sd_setImage(with: URL(string: "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["cat_image"] ?? "")"))

                self.listingCategoryTitleLBl.text = "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["cat_name"] ?? "")"

            }

        }
    }




    // MARK:- // According to Listing Type

    func accordingToListingType() {

        if self.listingType.elementsEqual("P") {

            self.foodListingView.isHidden = false
            self.reviewsView.isHidden = true
            self.infoView.isHidden = true
            self.foodlistMapview.isHidden = true


            self.restaurantListArray = self.allDataDictionary["cat_product"] as! NSMutableArray

            self.popularDishesArray = self.allDataDictionary["popular_dish"] as! NSMutableArray

            self.categoryListArray = self.allDataDictionary["category_list"] as! NSMutableArray

            if self.restaurantListArray.count > 0
            {
                for i in 0...(self.restaurantListArray.count-1)

                {
                    let tempDict = self.restaurantListArray[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                }

                self.currentRecordsArray = self.recordsArray.mutableCopy() as? NSMutableArray
            }





            self.startValue = self.startValue + self.restaurantListArray.count

            self.setLayout()

            self.popularDishesTableview.delegate = self
            self.popularDishesTableview.dataSource = self
            self.popularDishesTableview.reloadData()

            self.listingCategoryTableview.delegate = self
            self.listingCategoryTableview.dataSource = self
            self.listingCategoryTableview.reloadData()


            self.pickerView.delegate = self
            self.pickerView.dataSource = self
            self.pickerView.reloadAllComponents()


        }
        else if self.listingType.elementsEqual("R") {

            self.foodListingView.isHidden = true
            self.reviewsView.isHidden = false
            self.infoView.isHidden = true
            self.foodlistMapview.isHidden = true

            self.reviewsTableview.delegate = self
            self.reviewsTableview.dataSource = self
            self.reviewsTableview.reloadData()

        }
        else if self.listingType.elementsEqual("C") {

            if self.showMap == true {
                self.foodListingView.isHidden = true
                self.reviewsView.isHidden = true
                self.infoView.isHidden = true
                self.foodlistMapview.isHidden = false

                self.showMap = !self.showMap

                self.foodlistMapview.getData(dataArray: self.mapArray, listingDataArray: self.currentRecordsArray as! [NSDictionary])

            }
            else {
                self.foodListingView.isHidden = true
                self.reviewsView.isHidden = true
                self.infoView.isHidden = false
                self.foodlistMapview.isHidden = true

                self.deliveryTimesTableview.delegate = self
                self.deliveryTimesTableview.dataSource = self
                self.deliveryTimesTableview.reloadData()

                self.paymentMethodTableview.delegate = self
                self.paymentMethodTableview.dataSource = self
                self.paymentMethodTableview.reloadData()
            }



        }

    }



    // MARK: - Open Imagescroll for Popular Dishes

    @objc func popularDishesOpenImageScroll(sender: UIButton) {

        self.createImageScroll(popularDishes: true, index: sender.tag)

        UIView.animate(withDuration: 0.5)
        {
            self.viewOfimagescrollHeightConstraint.constant = self.FullHeight
            self.viewOfImagescrollWidthConstraint.constant = self.FullWidth
            self.viewOfImagescroll.isHidden = false
            self.view.layoutIfNeeded()

            self.closeImagescrollTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeImageScroll))
            self.imageScroll.addGestureRecognizer(self.closeImagescrollTapGesture)
        }
    }


    // MARK: - Open Imagescroll

    @objc func openImageScroll(sender: UIButton) {

        self.createImageScroll(popularDishes: false, index: sender.tag)

        UIView.animate(withDuration: 0.5)
        {
            self.viewOfimagescrollHeightConstraint.constant = self.FullHeight
            self.viewOfImagescrollWidthConstraint.constant = self.FullWidth
            self.viewOfImagescroll.isHidden = false
            self.view.layoutIfNeeded()

            self.closeImagescrollTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.closeImageScroll))
            self.imageScrollContentview.addGestureRecognizer(self.closeImagescrollTapGesture)
        }
    }


    // MARK: - Close Imagescroll when tapping outside

    @objc func closeImageScroll() {
        UIView.animate(withDuration: 0.5)
        {
            self.viewOfimagescrollHeightConstraint.constant = 0
            self.viewOfImagescrollWidthConstraint.constant = 0
            self.viewOfImagescroll.isHidden = true
            self.view.layoutIfNeeded()
        }
    }


    // MARK:- // Create Image Scroll

    func createImageScroll(popularDishes : Bool , index : Int) {

        for view in imageScrollContentview.subviews {
            view.removeFromSuperview()
        }

        var imageStringArray = [String]()
        var imageviewArray = [UIImageView]()

        if popularDishes == true {
            for i in 0..<((self.popularDishesArray[index] as! NSDictionary)["photos"] as! NSArray).count {
                imageStringArray.append("\((((self.popularDishesArray[index] as! NSDictionary)["photos"] as! NSArray)[i] as! NSDictionary)["img"] ?? "")")
            }
        }
        else {
            for i in 0..<((self.currentRecordsArray[index] as! NSDictionary)["photos"] as! NSArray).count {
                imageStringArray.append("\((((self.currentRecordsArray[index] as! NSDictionary)["photos"] as! NSArray)[i] as! NSDictionary)["img"] ?? "")")
            }
        }



        self.imageScroll.addSubview(imageScrollContentview)


        imageScrollContentview.anchor(top: imageScroll.topAnchor, leading: imageScroll.leadingAnchor, bottom: imageScroll.bottomAnchor, trailing: imageScroll.trailingAnchor, size: .init(width: 0, height: self.FullHeight))


        for i in 0..<imageStringArray.count {

            let itemImageview : UIImageView = {
                let itemimageview = UIImageView()
                itemimageview.contentMode = .scaleAspectFit
                itemimageview.clipsToBounds = true
                itemimageview.translatesAutoresizingMaskIntoConstraints = false
                return itemimageview
            }()

            imageviewArray.append(itemImageview)

            imageScrollContentview.addSubview(itemImageview)

            itemImageview.sd_setImage(with: URL(string: "\(imageStringArray[i])"))


            //            itemImageview.topAnchor.constraint(equalTo: scrollContentview.topAnchor, constant: 125).isActive = true
            //            itemImageview.bottomAnchor.constraint(equalTo: imageScrollContentview.bottomAnchor, constant: -125).isActive = true
            itemImageview.widthAnchor.constraint(equalTo: self.imageScroll.widthAnchor, multiplier: 1, constant: -20).isActive = true
            itemImageview.heightAnchor.constraint(equalToConstant: 300).isActive = true

            itemImageview.centerYAnchor.constraint(equalTo: self.imageScrollContentview.centerYAnchor).isActive = true


            if imageStringArray.count == 1
            {

                itemImageview.leadingAnchor.constraint(equalTo: imageScrollContentview.leadingAnchor, constant: 10).isActive = true
                itemImageview.trailingAnchor.constraint(equalTo: imageScrollContentview.trailingAnchor, constant: -10).isActive = true
            }
            else
            {

                if i == 0 {
                    itemImageview.leadingAnchor.constraint(equalTo: imageScrollContentview.leadingAnchor, constant: 10).isActive = true
                }
                else if i == (imageStringArray.count - 1)
                {
                    itemImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                    itemImageview.trailingAnchor.constraint(equalTo: imageScrollContentview.trailingAnchor, constant: -10).isActive = true
                }
                else {
                    itemImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                }
            }



        }





    }



    // MARK:- // Set Language Strings

    func setLanguageStrings() {

        self.reviewsButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "review", comment: ""), for: .normal)
        self.infoButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "info", comment: ""), for: .normal)

        self.popularDishesTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "popularDishes", comment: "")
        self.deliveryTimesTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "deliveryTimes", comment: "")
        self.paymentMethodTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "paymentMethods", comment: "")

    }



    // MARK:- // Set Font

    func SetFont() {

        self.categoryLBL.font = UIFont(name: self.categoryLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.sellerName.font = UIFont(name: self.sellerName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        self.reviewsButtonOutlet.titleLabel?.font = UIFont(name: (self.reviewsButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))
        self.infoButtonOutlet.titleLabel?.font = UIFont(name: (self.infoButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))
        self.popularDishesTitleLBL.font = UIFont(name: self.popularDishesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        self.deliveryTimesTitleLBL.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }




}



//  MARK:- // Tableview Delegate Functions


extension FoodListingViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == popularDishesTableview {

            print(popularDishesArray)

            return self.popularDishesArray.count
        }
        else if tableView == self.listingCategoryTableview{  // listing Table
            return self.currentRecordsArray.count
        }
        else if tableView == self.reviewsTableview{  // Reviews Table
            return self.reviewArray.count
        }
        else if tableView == self.deliveryTimesTableview{  // Delivery Time Table
            return (self.infoDictionary["service_available"] as! NSArray).count
        }
        else {  // Payment Type Table
            return (self.infoDictionary["payment_type"] as! NSArray).count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == popularDishesTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "popularDishes") as! PopularDishesTVCTableViewCell

            cell.productName.text = "\((self.popularDishesArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
            cell.productDescription.text = "\((self.popularDishesArray[indexPath.row] as! NSDictionary)["description"] ?? "")"
            cell.startPrice.text = "\((self.popularDishesArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")"
            cell.reservePrice.isHidden = true


            cell.cellView.layer.cornerRadius = 8

            cell.addToCartButton.layer.cornerRadius = 10

            cell.viewImagesButton.addTarget(self, action: #selector(self.popularDishesOpenImageScroll(sender:)), for: .touchUpInside)


            //Set Font
            cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.productDescription.font = UIFont(name: cell.productDescription.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.startPrice.font = UIFont(name: cell.startPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.reservePrice.font = UIFont(name: cell.reservePrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.addToCartButton.titleLabel?.font = UIFont(name: (cell.addToCartButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 12)))!
            cell.viewImagesButton.titleLabel?.font = UIFont(name: (cell.viewImagesButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))!

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            cell.cellView.layer.cornerRadius = 8

            cell.addToCartButton.layer.cornerRadius = 10



            return cell
        }
        else if tableView == self.listingCategoryTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! FoodListingTVCTableViewCell

            cell.productName.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
            cell.productDescription.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["description"] ?? "")"
            cell.startPrice.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")"
            cell.reservePrice.isHidden = true

            cell.viewImagesButton.addTarget(self, action: #selector(self.openImageScroll(sender:)), for: .touchUpInside)


            //Set Font
            cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.productDescription.font = UIFont(name: cell.productDescription.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.startPrice.font = UIFont(name: cell.startPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.reservePrice.font = UIFont(name: cell.reservePrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.addToCartButton.titleLabel?.font = UIFont(name: (cell.addToCartButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 12)))!
            cell.viewImagesButton.titleLabel?.font = UIFont(name: (cell.viewImagesButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))!

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none


            cell.cellView.layer.cornerRadius = 8

            cell.addToCartButton.layer.cornerRadius = 10


            return cell
        }
        else if tableView == reviewsTableview{

            let cell = tableView.dequeueReusableCell(withIdentifier: "reviews") as! FoodListReviewsTVCTableViewCell

            cell.name.text = "\((reviewArray[indexPath.row] as! NSDictionary)["name"]!)"
            cell.reviewDate.text = "\((reviewArray[indexPath.row] as! NSDictionary)["review_date"]!)"
            cell.comment.text = "\((reviewArray[indexPath.row] as! NSDictionary)["comment"]!)"
            cell.totalRating.text = "\((reviewArray[indexPath.row] as! NSDictionary)["rvwtotal_rating"]!)"
            cell.reviewRating.rating = Double("\((reviewArray[indexPath.row] as! NSDictionary)["rvwavg_rating"]!)")!

            cell.totalRatingImage.sd_setImage(with: URL(string: "\((reviewArray[indexPath.row] as! NSDictionary)["rating_img"]!)"))


            //Set Font
            cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.reviewDate.font = UIFont(name: cell.reviewDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.commentLBL.font = UIFont(name: cell.commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.comment.font = UIFont(name: cell.comment.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.ratingLBL.font = UIFont(name: cell.ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalRatingLBL.font = UIFont(name: cell.totalRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalRating.font = UIFont(name: cell.totalRating.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            cell.cellView.layer.cornerRadius = 8

            return cell

        }
        else if tableView == deliveryTimesTableview { // Info Delivery Time Table
            let cell = tableView.dequeueReusableCell(withIdentifier: "info") as! FoodListInfoTVCTableViewCell

            cell.deliveryTimesLBL.text = "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["day"] ?? "")"
            cell.deliveryTime.text = "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["open_time"] ?? "")" + " - " + "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["close_time"] ?? "")"

            //Set Font
            cell.deliveryTimesLBL.font = UIFont(name: cell.deliveryTimesLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.deliveryTime.font = UIFont(name: cell.deliveryTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else { // Info Payment Type Table
            if "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["type"] ?? "")".elementsEqual("COD") {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCOD")

                cell?.textLabel!.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["type"] ?? "")"

                //Set Font
                cell?.textLabel!.font = UIFont(name: (cell?.textLabel!.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))

                //
                cell?.selectionStyle = UITableViewCell.SelectionStyle.none

                return cell!
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentBank") as! PaymentMethodBankTableViewCell

                cell.bankNameTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bankName", comment: "")
                cell.bankName.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["bank_name"] ?? "")"
                cell.accountNumberTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountNumber", comment: "")
                cell.accountNumber.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["account_name"] ?? "")"

                //Set Font
                cell.bankNameTitleLBL.font = UIFont(name: cell.bankNameTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.bankName.font = UIFont(name: cell.bankName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.accountNumberTitleLBL.font = UIFont(name: cell.accountNumberTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.accountNumber.font = UIFont(name: cell.accountNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                return cell
            }
        }

    }



}





// MARK:- // Pickerview Delegates


extension FoodListingViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }


    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return categoryListArray.count

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return "\((self.categoryListArray[row] as! NSDictionary)["cat_name"] ?? "")"

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.categoryLBL.text = "\((self.categoryListArray[row] as! NSDictionary)["cat_name"] ?? "")"

        self.searchCategoryID = "\((self.categoryListArray[row] as! NSDictionary)["id"] ?? "")"


        self.startValue = 0

        self.recordsArray.removeAllObjects()
        self.currentRecordsArray.removeAllObjects()

        self.getFoodListings()

        UIView.animate(withDuration: 0.5) {

            self.viewOfPickerTopConstraint.constant = self.FullHeight

        }



        self.globalDispatchgroup.notify(queue: .main) {



            self.accordingToListingType()

            self.setTableviewHeight(tableview: self.popularDishesTableview, heightConstraint: self.popularDishesTableHeightConstraint)
            self.setTableviewHeight(tableview: self.listingCategoryTableview, heightConstraint: self.listingCategoryTableHeightConstraint)

        }

    }

}





