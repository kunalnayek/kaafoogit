//
//  FoodCourtAddCartViewController.swift
//  Kaafoo
//
//  Created by Subir Saha on 29/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class FoodCourtAddCartViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var QuantityArray : NSMutableArray!
    
    @IBOutlet weak var Total_Amount: UILabel!
    
    @IBOutlet weak var Message_TxtView: UITextView!
    
    var priceArray : NSMutableArray! = NSMutableArray()
    
    var FeatureidArray : NSMutableArray! = NSMutableArray()
    
    var IndexInt : Int!
    
    let NumberArray = ["0","1","2","3","4"]
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    //let langID = UserDefaults.standard.string(forKey: "langID")
    var labelArray = [UILabel]()
    
    var DataArray : NSMutableArray! = NSMutableArray()
    
    @IBAction func Close_Btn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var Button_View: UIView!
    
    @IBOutlet weak var Price_View: UIView!
    
    @IBOutlet weak var Message_View: UIView!
    
    var productId : String!
    
    var FeatureArray : NSMutableArray!
    
    var buttonArray = ["Hello","Hi","Hello"]
    
    let dispatchGroup = DispatchGroup()
    
    @IBOutlet weak var Quantity: UIPickerView!
    
    @IBAction func Back_btn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var Quantity_TableView: UITableView!
    
    @IBAction func add_to_cartBtn(_ sender: UIButton) {
//        print("QUANTITYARRAY",self.DataArray)
        self.loadData()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print("featurearray",self.FeatureArray!)
        
        self.FetchData()
        
        self.messageView()
        
        self.Quantity_TableView.delegate = self
        
        self.Quantity_TableView.dataSource = self
        
        self.Quantity_TableView.separatorStyle = .none
        
        self.Message_TxtView.layer.borderWidth = 1.5
        
        self.Message_TxtView.layer.borderColor = UIColor.black.cgColor
        
        self.Total_Amount.text = "Total Amount: $0 "
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Tableview Datasource & Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FeatureArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "foodcourtcart") as! FoodCourtCartTableViewCell
        
        obj.Price.text = "\((self.FeatureArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" +  "\((self.FeatureArray[indexPath.row] as! NSDictionary)["feature_price"]!)"
        if self.DataArray.count == 0
        {
            obj.Quantity_Lbl.text = "0"
        }
        else
        {
            obj.Quantity_Lbl.text = "\(self.DataArray[indexPath.row])"
        }
        obj.feature_name.text = "\((self.FeatureArray[indexPath.row] as! NSDictionary)["feature_name"]!)"
        obj.QBtn.tag = indexPath.row

        self.labelArray.append(obj.Quantity_Lbl)
        
        obj.QBtn.addTarget(self, action: #selector(FoodCourtAddCartViewController.FindQuantity(sender:)), for: .touchUpInside)
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        IndexInt = indexPath.row
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let obj = tableView.dequeueReusableCell(withIdentifier: "foodcourtcart") as! FoodCourtCartTableViewCell
        
        let testView : UIView! = UIView()
        obj.addSubview(testView)
        testView.backgroundColor = .green
        testView.translatesAutoresizingMaskIntoConstraints = false
        testView.leadingAnchor.constraint(equalTo: obj.leadingAnchor).isActive = true
        testView.trailingAnchor.constraint(equalTo: obj.trailingAnchor).isActive = true
        testView.topAnchor.constraint(equalTo: obj.topAnchor).isActive = true
        testView.bottomAnchor.constraint(equalTo: obj.bottomAnchor).isActive = true
        testView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        testView.widthAnchor.constraint(equalToConstant: self.FullWidth).isActive = true
        return testView

//        let ProductName = UILabel(frame: CGRect(x: 0, y: 0, width: <#T##CGFloat#>, height: <#T##CGFloat#>))
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }


    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return NumberArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("==nn",NumberArray[row])
        
        self.DataArray[IndexInt] = NumberArray[row]
        self.Quantity_TableView.reloadData()
        
        UIView.animate(withDuration: 0.1)
        {
            self.Button_View.isHidden = false
            self.Message_View.isHidden = false
            self.Price_View.isHidden = false
            self.Quantity.isHidden = true

//            self.to
        }
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string = NumberArray[row]
        return NSAttributedString(string: string, attributes: [NSAttributedString.Key.foregroundColor : UIColor.blue])
    }
    
    @objc func FindQuantity(sender: UIButton)
    {
        self.IndexInt = sender.tag
        UIView.animate(withDuration: 0.1)
        {
            //self.view.addSubview(self.Quantity)
            self.Button_View.isHidden = true
            self.Message_View.isHidden = true
            self.Price_View.isHidden = true
            self.view.bringSubviewToFront(self.Quantity)
        }
        self.Quantity.isHidden = false
        self.Quantity.delegate = self
        self.Quantity.dataSource = self
        
    }
    // MARK:- JSON API Fire to Fetch Data
    func loadData()
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_add_to_cart")!   //change the url
        
        print("Post a Question URL : ", url)
        
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let Featureidstring = self.FeatureidArray.componentsJoined(by: ",")
        
        let Quantitystring = self.DataArray.componentsJoined(by: ",")
        
        let pricestring = self.priceArray.componentsJoined(by: ",")
        
        parameters = "user_id=\(userID!)&product_id=\(productId!)&Lang_id=\(langID!)&qnty=\("1")&feature_id=\(Featureidstring)&feature_price=\(pricestring)&feature_quantity=\(Quantitystring)&leave_message=\(Message_TxtView.text!)"
        //&feature_quantity=\(Quantitystring)
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Post a Question Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.dispatchGroup.notify(queue: .main, execute: {
                                
                                let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
                                nav.LanguageID = self.langID as NSString?
                                nav.ProductID = self.productId as NSString?
                                self.navigationController?.pushViewController(nav, animated: true)
                                
                            })
                            
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                        
                        
                    else
                    {
                        
                        if "\(json["confirmstatus"]!)".elementsEqual("1")
                        {
                            
                            self.dispatchGroup.leave()
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                            }
                            
                            self.dispatchGroup.notify(queue: .main, execute: {
                                
                                let alert = UIAlertController(title: "Confirm", message: "You have tried to add products from different seller. If you still want to proceed please delete existent cart products", preferredStyle: .alert)
                                
                                let ok = UIAlertAction(title: "OK", style: .default) {
                                    UIAlertAction in
                                    self.dispatchGroup.notify(queue: .main, execute: {
                                        //MARK:- Remove All Cart API Fire
                                        
                                        self.removeCarts {
                                            
                                            self.loadData()
                                            
                                        }
                                        
                                    })
                                }
                                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                
                                alert.addAction(ok)
                                alert.addAction(cancel)
                                
                                self.present(alert, animated: true, completion: nil )
                                
                            })
                        }
                        else
                        {
                            self.dispatchGroup.leave()
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    //                    self.dispatchGroup.leave()
                    //
                    //
                    //                    DispatchQueue.main.async {
                    //
                    ////                        self.loadData()
                    //                        SVProgressHUD.dismiss()
                    //
                    //                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    /*
     MARK: - Navigation
     
     In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     Get the new view controller using segue.destination.
     Pass the selected object to the new view controller.
     }
     */
    func removeCarts(completion : @escaping () -> ())
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_remove_cart_by_userid")!   //change the url
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove All Item From Cart API " , json)
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            
                            completion()
                            
                        })
                        
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    func FetchData()
    {
        for i in 0..<self.FeatureArray.count
        {
            self.priceArray.add("\((self.FeatureArray[i] as! NSDictionary)["feature_price"]!)")
            self.FeatureidArray.add("\((self.FeatureArray[i] as! NSDictionary)["feature_id"]!)")
            self.DataArray.add("0")
        }
        print("pricearray",self.priceArray!)
        print("Featureidarray",self.FeatureidArray!)
        //        print("Array",self.FeatureidArray.componentsJoined(by: ","))
    }
    func messageView()
    {
        self.Message_View.layer.borderWidth = 1.0
        self.Message_View.layer.borderColor = UIColor.black.cgColor
    }
    
}
