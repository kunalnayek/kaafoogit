//
//  RestaurantDetailsTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 03/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class RestaurantDetailsTVCTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var bottomViewFIrstSeparator: UIView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    
    @IBOutlet weak var viewImagesButton: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
