//
//  InfoTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 05/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class InfoTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var clockImageview: UIImageView!
    @IBOutlet weak var deliveryTimes: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
