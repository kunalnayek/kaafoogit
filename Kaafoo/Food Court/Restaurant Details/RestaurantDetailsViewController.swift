//
//  RestaurantDetailsViewController.swift
//  Kaafoo
//
//  Created by admin on 03/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleMaps
import GooglePlaces

class RestaurantDetailsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var viewOfImageScroll: UIView!
    @IBOutlet weak var imageScroll: UIScrollView!
    
    @IBOutlet weak var selectCategoryAndSortMainView: UIView!
    @IBOutlet weak var selectCategoryAndSortView: UIView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryDropDownButton: UIButton!
    @IBAction func tapOnCategoryDropDown(_ sender: UIButton) {
        
        if selectCategoryView.isHidden{
            animateCategory(toggle: true)
            
        }
        else {
            animateCategory(toggle: false)
            
        }
        
    }
    
    
    
    
    @IBOutlet weak var sortViewFirstSeparator: UIView!
    @IBOutlet weak var sortViewSecondSeparator: UIView!
    @IBOutlet weak var selectCategoryView: UIView!
    @IBOutlet weak var selectCategoryTableview: UITableView!
    @IBOutlet weak var reviewsAndInfoView: UIView!
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var reviewsAndInfoViewSeparator: UIView!
    @IBOutlet weak var restaurantDetailsTableview: UITableView!
    @IBOutlet weak var sortBackgroundView: UIView!
    @IBOutlet weak var sortTransparentVIew: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var sortContainerView: UIView!
    @IBOutlet weak var sortLBL: UILabel!
    @IBOutlet weak var categoryDetailsView: UIView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var noDataFoundLBL: UILabel!
    
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var reviewsTitleLBL: UILabel!
    @IBOutlet weak var reviewsTableview: UITableView!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var deliveryTimesTitleLBL: UILabel!
    @IBOutlet weak var deliveryTimesTableview: UITableView!
    @IBOutlet weak var paymentMethodsView: UIView!
    @IBOutlet weak var locationMapView: GMSMapView!
    
    
    
    var sellerName : String!
    
    var sellerID : String!
    
    var sellerImageURL : String!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var currentRecordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var restaurantListArray : NSArray!
    
    var listingType : String! = "P"
    
    var searchCategoryID : String! = "pd"
    
    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var tapGesture = UITapGestureRecognizer()
    
    let imagePageControl = UIPageControl(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    var categoryArray : NSArray!
    
    var reviewArray : NSArray!
    
    var reviewRecordsArray : NSMutableArray! = NSMutableArray()
    
    var reviewCurrentRecordsArray : NSMutableArray! = NSMutableArray()
    
    var infoArray : NSArray!
    
    var mapIcon : Bool! = true
    
    var locationArray : NSArray!
    
    var addressLat : Double!
    
    var addressLong : Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.getRestaurantList()
        
        self.setFont()
        
        self.dispatchGroup.notify(queue: .main) {
            
            self.putData()
            
        }
        
        self.categoryImage.layer.cornerRadius = (5/320)*self.FullWidth
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Sort Button
    
    @IBOutlet weak var sortButton: UIButton!
    @IBAction func tapOnSortButton(_ sender: UIButton) {
    }
    
    
    // MARK:- // Reviews Button
    
    @IBOutlet weak var reviewsButton: UIButton!
    @IBAction func tapOnReviews(_ sender: UIButton) {
        
        self.mapIcon = false
        
        UIView.animate(withDuration: 0.5) {
            
            self.categoryDetailsView.isHidden = true
            self.locationMapView.isHidden = true
            self.reviewsView.isHidden = false
            self.infoView.isHidden = true
            self.mapButton.setImage(UIImage(named: "list"), for: .normal)
            
            self.view.bringSubviewToFront(self.reviewsView)
            
            self.listingType = "R"
            
            self.startValue = 0
            
            self.getRestaurantList()
            
        }
        
    }
    
    
    // MARK:- // Map Button
    
    @IBOutlet weak var mapButton: UIButton!
    @IBAction func tapOnMapButton(_ sender: UIButton) {
        
        if mapIcon == true
        {
            listingType = "C"
            
            self.getRestaurantList()
            
            dispatchGroup.notify(queue: .main, execute: {
                
                UIView.animate(withDuration: 0.5) {
                    
                    self.showLocation()
                    
                    self.reviewsView.isHidden = true
                    self.infoView.isHidden = true
                    self.categoryDetailsView.isHidden = true
                    
                    self.locationMapView.isHidden = false
                    self.view.bringSubviewToFront(self.locationMapView)
                    
                    self.mapButton.setImage(UIImage(named: "list"), for: .normal)
                    
                }
                
            })
            
            
            
        }
        else
        {
            UIView.animate(withDuration: 0.5) {
                
                self.reviewsView.isHidden = true
                self.infoView.isHidden = true
                self.categoryDetailsView.isHidden = false
                
                self.locationMapView.isHidden = true
                self.view.bringSubviewToFront(self.categoryDetailsView)
                
                self.mapButton.setImage(UIImage(named: "Map-1"), for: .normal)
                
            }
        }
        
        self.mapIcon = !self.mapIcon
        
    }
    
    
    // MARK:- // Info Button
    
    @IBOutlet weak var infoButton: UIButton!
    @IBAction func tapOnInfoButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            self.categoryDetailsView.isHidden = true
            self.reviewsView.isHidden = true
            self.locationMapView.isHidden = true
            self.infoView.isHidden = false
            self.mapButton.setImage(UIImage(named: "list"), for: .normal)
            
            self.view.bringSubviewToFront(self.infoView)
            
            self.listingType = "C"
            
            self.startValue = 0
            
            self.getRestaurantList()
            
        }
        
    }
    
    
    // MARK:- // Open Sort Category
    
    @IBOutlet weak var openSortCategoryButtonOutlet: UIButton!
    @IBAction func tapOnOpenSortCategory(_ sender: UIButton) {
        
        
        
    }
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == selectCategoryTableview
        {
            return categoryArray.count
        }
        else if tableView == reviewsTableview
        {
            return reviewCurrentRecordsArray.count
        }
        else if tableView == deliveryTimesTableview
        {
            return ((infoArray[0] as! NSDictionary)["service_available"] as! NSArray).count
        }
        else
        {
            return currentRecordsArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == selectCategoryTableview
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")
            
            cell?.textLabel?.text = "\((categoryArray[indexPath.row] as! NSDictionary)["cat_name"]!)"
            
            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
            
            return cell!
            
        }
        else if tableView == reviewsTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! RestaurantReviewTVCTableViewCell
            
            
            cell.name.text = "\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["name"]!)"
            cell.reviewDate.text = "\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["review_date"]!)"
            cell.comment.text = "\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["comment"]!)"
            cell.totalReviewCount.text = "\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["rvwtotal_rating"]!)"
            cell.reviewRating.rating = Double("\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["rvwavg_rating"]!)")!
            
            cell.totalReviewsImage.sd_setImage(with: URL(string: "\((reviewCurrentRecordsArray[indexPath.row] as! NSDictionary)["rating_img"]!)"))
            
            
            //Set Font
            cell.nameView.font = UIFont(name: cell.nameView.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.reviewDate.font = UIFont(name: cell.reviewDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.commentLBL.font = UIFont(name: cell.commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.comment.font = UIFont(name: cell.comment.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.ratingLBL.font = UIFont(name: cell.ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalRatingLBL.font = UIFont(name: cell.totalRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalReviewCount.font = UIFont(name: cell.totalReviewCount.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        else if tableView == deliveryTimesTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! InfoTVCTableViewCell
            
            let tempdictionary = (((infoArray[0] as! NSDictionary)["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)
            
            cell.dayName.text = "\(tempdictionary["day"]!)"
            cell.deliveryTimes.text = "\(tempdictionary["open_time"]!)" + " - " + "\(tempdictionary["close_time"]!)"
            
            
            //Set Font
            cell.dayName.font = UIFont(name: cell.dayName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.deliveryTimes.font = UIFont(name: cell.deliveryTimes.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! RestaurantDetailsTVCTableViewCell
            
            //put Date
            cell.productName.text = "\((restaurantListArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            cell.productDescription.text = "\((restaurantListArray[indexPath.row] as! NSDictionary)["description"]!)"
            cell.productPrice.text = "\((restaurantListArray[indexPath.row] as! NSDictionary)["currency_symbol"]!) " + "\((restaurantListArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            
            cell.viewImagesButton.tag = indexPath.row
            
            cell.viewImagesButton.addTarget(self, action: #selector(RestaurantDetailsViewController.viewImages(sender:)), for: .touchUpInside)
            
            
            
            
            //Set Font
            cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.productDescription.font = UIFont(name: cell.productDescription.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.productPrice.font = UIFont(name: cell.productPrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.addToCartButton.titleLabel?.font = UIFont(name: (cell.addToCartButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
            cell.viewImagesButton.titleLabel?.font = UIFont(name: (cell.viewImagesButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 13)))!
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == restaurantDetailsTableview
        {
            let cell = cell as! RestaurantDetailsTVCTableViewCell
            
            cell.addToCartButton.layer.cornerRadius = cell.addToCartButton.frame.size.height / 2
            
            cell.cellView.clipsToBounds = false
            
            cell.cellView.layer.cornerRadius = (5/320)*self.FullWidth
            
            cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
            
        }
        else if tableView == reviewsTableview
        {
            let cell = cell as! RestaurantReviewTVCTableViewCell
            
            cell.cellView.clipsToBounds = false
            
            cell.cellView.layer.cornerRadius = (5/320)*self.FullWidth
            
            cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == selectCategoryTableview
        {
            return (20/568)*self.FullHeight
        }
        else if tableView == reviewsTableview
        {
            return (203/568)*self.FullHeight
        }
        else if tableView == deliveryTimesTableview
        {
            return (50/568)*self.FullHeight
        }
        else
        {
            return (135/568)*self.FullHeight
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == selectCategoryTableview
        {
            //typeID = "\(((servicesMainDictionary["category_list"] as! NSArray)[indexPath.row] as! NSDictionary)["id"]!)"
            
            searchCategoryID = "\((categoryArray[indexPath.row] as! NSDictionary)["id"]!)"
            
            recordsArray.removeAllObjects()
            currentRecordsArray.removeAllObjects()
            self.startValue = 0
            
            animateCategory(toggle: false)
            
            getRestaurantList()
            
            dispatchGroup.notify(queue: .main, execute: {
                
                self.categoryName.text = "\((self.categoryArray[indexPath.row] as! NSDictionary)["cat_name"]!)"
                
                self.putData()
                //
                //                self.selectedCategoryIndex = indexPath.row
                //
            })
            
        }
        else if tableView == restaurantDetailsTableview
        {
            let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "foodcourtaddcart") as! FoodCourtAddCartViewController
            print("===restaurantListArray===",self.restaurantListArray!)
            nav.productId = "\((self.restaurantListArray[indexPath.row] as! NSDictionary)["product_id"]!)"
            nav.FeatureArray = ((self.restaurantListArray[indexPath.row] as! NSDictionary)["extra_feature_val"]!) as? NSMutableArray
            
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
        
    }
    
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == imageScroll
        {
            let pageNumber = imageScroll.contentOffset.x / imageScroll.frame.size.width
            
            imagePageControl.currentPage = Int(pageNumber)
        }
        else
        {
            if scrollBegin > scrollEnd
            {
                
            }
            else
            {
//                print("next start : ",nextStart )
                
                if (nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                else
                {
                    self.getRestaurantList()
                }
            }
        }
    }
    
    
    
    // MARK:- // Json APi Fires
    
    // MARK: - // JSON POST Method to get Restaurant List
    
    func getRestaurantList()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_restaurent_details")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        
        parameters = "login_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&seller_id=\(sellerID!)&mycountry_name=\(myCountryName!)&listing_type=\(listingType!)&search_cat=\(searchCategoryID!)"
        
        print("Restaurant List URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Restaurant List Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.categoryDetailsView.isHidden = true
                            self.noDataFoundLBL.isHidden = false
                            
                            self.noDataFoundLBL.text = "\((json["message"]!))"
                            
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        
                        if self.listingType.elementsEqual("P")
                        {
                            self.categoryArray = (json["info_array"] as! NSDictionary)["category_list"] as? NSArray
                            
                            //                            if self.searchCategoryID.elementsEqual("pd")
                            //                            {
                            //                                self.restaurantListArray = (json["info_array"] as! NSDictionary)["popular_dish"] as! NSArray
                            //                            }
                            //                            else
                            //                            {
                            self.restaurantListArray = (json["info_array"] as! NSDictionary)["cat_product"] as? NSArray
                            //}
                            
                            for i in 0...(self.restaurantListArray.count-1)
                                
                            {
                                
                                let tempDict = self.restaurantListArray[i] as! NSDictionary
                                
                                self.recordsArray.add(tempDict as! NSMutableDictionary)
                                
                                
                            }
                            
                            self.currentRecordsArray = self.recordsArray.mutableCopy() as? NSMutableArray
                            
                            self.nextStart = "\(json["next_start"]!)"
                            
                            self.startValue = self.startValue + self.restaurantListArray.count
                            
                            print("Next Start Value : " , self.startValue)
                            
                            
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                                self.categoryDetailsView.isHidden = false
                                self.noDataFoundLBL.isHidden = true
                                
                                self.restaurantDetailsTableview.delegate = self
                                self.restaurantDetailsTableview.dataSource = self
                                self.restaurantDetailsTableview.reloadData()
                                
                                self.selectCategoryTableview.delegate = self
                                
                                self.selectCategoryTableview.dataSource = self
                                
                                self.selectCategoryTableview.reloadData()
                                
                                
                                SVProgressHUD.dismiss()
                            }
                        }
                        else if self.listingType.elementsEqual("R")
                        {
                            self.reviewArray = json["info_array"] as? NSArray
                            
                            for i in 0...(self.reviewArray.count-1)
                                
                            {
                                
                                let tempDict = self.reviewArray[i] as! NSDictionary
                                
                                self.reviewRecordsArray.add(tempDict as! NSMutableDictionary)
                                
                                
                            }
                            
                            self.reviewCurrentRecordsArray = self.reviewRecordsArray.mutableCopy() as? NSMutableArray
                            
                            self.nextStart = "\(json["next_start_product"]!)"
                            
                            self.startValue = self.startValue + self.reviewArray.count
                            
                            print("Next Start Value : " , self.startValue)
                            
                            
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                                self.reviewsView.isHidden = false
                                self.noDataFoundLBL.isHidden = true
                                
                                self.reviewsTableview.delegate = self
                                self.reviewsTableview.dataSource = self
                                self.reviewsTableview.reloadData()
                                
                                SVProgressHUD.dismiss()
                            }
                        }
                        else if self.listingType.elementsEqual("C")
                        {
                            self.infoArray = json["info_array"] as? NSArray
                            
                            self.locationArray = (self.infoArray[0] as! NSDictionary)["map_details"] as? NSArray
                            
                            self.nextStart = ""
                            
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                                self.infoView.isHidden = false
                                self.noDataFoundLBL.isHidden = true
                                
                                self.deliveryTimesTableview.delegate = self
                                self.deliveryTimesTableview.dataSource = self
                                self.deliveryTimesTableview.reloadData()
                                
                                self.setInfoViewFrame()
                                
                                SVProgressHUD.dismiss()
                            }
                        }
                        
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Set Image Scroll
    
    func setImageScroll(index : NSInteger)
    {
        print("Index----",index)
        
        
        
        UIView.animate(withDuration: 0.5) {
            
            
            self.viewOfImageScroll.autoresizesSubviews = false
            self.imageScroll.autoresizesSubviews = false
            
            self.viewOfImageScroll.frame = CGRect(x: 0, y: 0, width: (self.FullWidth), height: (self.FullHeight))
            self.imageScroll.frame = CGRect(x: 0, y: 0, width: (self.FullWidth), height: (200/568)*self.FullHeight)
            
            self.viewOfImageScroll.center = self.view.center
            self.imageScroll.center = self.viewOfImageScroll.center
            
            self.viewOfImageScroll.autoresizesSubviews = true
            self.imageScroll.autoresizesSubviews = true
            
            // *** Hide sortBackgroundView when tapping outside ***
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            self.viewOfImageScroll.addGestureRecognizer(self.tapGesture)
            
            
            let tempArray = (self.restaurantListArray[index] as! NSDictionary)["photos"] as! NSArray
            
            
            self.imagePageControl.frame = CGRect(x: 0, y: (400/468)*self.FullHeight, width: self.FullWidth, height: (50/568)*self.FullHeight)
            
            self.imagePageControl.numberOfPages = tempArray.count
            
            //self.imagePageControl.backgroundColor = UIColor.red
            
            self.viewOfImageScroll.addSubview(self.imagePageControl)
            
            for i in 0..<tempArray.count
            {
                self.tempFrame.origin.x = self.imageScroll.frame.size.width * CGFloat(i)
                self.tempFrame.size = self.imageScroll.frame.size
                
                let imageview = UIImageView(frame: self.tempFrame)
                imageview.sd_setImage(with: URL(string: "\((tempArray[i] as! NSDictionary)["img"]!)"))
                
                imageview.contentMode = .scaleAspectFit
                imageview.clipsToBounds = true
                
                self.imageScroll.addSubview(imageview)
            }
            self.imageScroll.contentSize = CGSize(width: (self.imageScroll.frame.size.width * CGFloat(tempArray.count)), height: self.imageScroll.frame.size.height)
            
            
            self.imageScroll.delegate = self
            
            self.view.bringSubviewToFront(self.viewOfImageScroll)
            self.viewOfImageScroll.isHidden = false
            
        }
        
    }
    
    
    // MARK:- // View Images
    
    @objc func viewImages(sender: UIButton)
    {
        self.setImageScroll(index: sender.tag)
    }
    
    
    // MARK: - Hide View if Image Scroll when tapping outside
    
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            
            
            self.viewOfImageScroll.autoresizesSubviews = false
            self.imageScroll.autoresizesSubviews = false
            
            self.viewOfImageScroll.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
            self.imageScroll.frame = CGRect(x: 0, y: 0, width: 1, height: 1)
            
            for view in self.imageScroll.subviews {
                view.removeFromSuperview()
            }
            
            self.viewOfImageScroll.center = self.view.center
            self.imageScroll.center = self.viewOfImageScroll.center
            
            self.viewOfImageScroll.autoresizesSubviews = true
            self.imageScroll.autoresizesSubviews = true
            
            self.view.sendSubviewToBack(self.viewOfImageScroll)
            self.viewOfImageScroll.isHidden = true
            
        }
    }
    
    
    
    // MARK:- // PutData
    
    func putData()
    {
        
        self.businessName.text = sellerName!
        self.businessImage.sd_setImage(with: URL(string: sellerImageURL!))
        
        for i in 0..<categoryArray.count
        {
            if "\((categoryArray[i] as! NSDictionary)["id"]!)".elementsEqual(searchCategoryID!)
            {
                self.categoryImage.sd_setImage(with: URL(string: "\((categoryArray[i] as! NSDictionary)["cat_image"]!)"))
                self.categoryName.text = "\((categoryArray[i] as! NSDictionary)["cat_name"]!)"
            }
        }
        
    }
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        businessName.font = UIFont(name: businessName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        reviewsButton.titleLabel?.font = UIFont(name: (reviewsButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 12)))!
        infoButton.titleLabel?.font = UIFont(name: (mapButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 12)))!
        categoryName.font = UIFont(name: categoryName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        noDataFoundLBL.font = UIFont(name: noDataFoundLBL.font.fontName, size: CGFloat(Get_fontSize(size: 24)))
        
    }
    
    
    // MARK: - // Animate Category DropDown Tableview
    
    
    func animateCategory(toggle: Bool) {
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                
                self.view.bringSubviewToFront(self.selectCategoryView)
                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = false
                self.selectCategoryView.frame.size.height = ((100/568)*self.FullHeight)
                self.selectCategoryTableview.frame.size.height = ((100/568)*self.FullHeight)
                self.selectCategoryView.autoresizesSubviews = true
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = true
                self.selectCategoryView.frame.size.height = ((1/568)*self.FullHeight)
                self.selectCategoryTableview.frame.size.height = ((1/568)*self.FullHeight)
                self.selectCategoryView.autoresizesSubviews = true
                self.view.sendSubviewToBack(self.selectCategoryView)
            }
        }
        
    }
    
    
    // MARK:- // Give Shadow to a UIView
    
    func castShadow(view: UIView)
    {
        let shadowSize : CGFloat = 10.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,y: -shadowSize / 2,width: view.frame.size.width + shadowSize,height: view.frame.size.height + shadowSize))
        self.view.layer.masksToBounds = false
        self.view.layer.shadowColor = UIColor.black.cgColor
        self.view.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        self.view.layer.shadowOpacity = 0.5
        self.view.layer.shadowPath = shadowPath.cgPath
    }
    
    
    // MARK:- // Set frame of Info View
    
    func setInfoViewFrame()
    {
        self.deliveryTimesTableview.frame.size.height = ((50/568)*self.FullHeight) * CGFloat(((infoArray[0] as! NSDictionary)["service_available"] as! NSArray).count)
        
        self.paymentMethodsView.frame.origin.y = self.deliveryTimesTableview.frame.origin.y + self.deliveryTimesTableview.frame.size.height
        
        //creating Payment Method View
        
        let paymentTypeArray = ((infoArray[0] as! NSDictionary)["payment_type"] as!NSArray)
        
        let paymentMethodTitleLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        
        paymentMethodTitleLBL.text = "PAYMENT METHODS"
        
        self.paymentMethodsView.addSubview(paymentMethodTitleLBL)
        
        paymentMethodTitleLBL.textColor = UIColor.black
        
        paymentMethodTitleLBL.textAlignment = .left
        
        paymentMethodTitleLBL.font = UIFont(name: businessName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        var tempYOrigin : CGFloat! = paymentMethodTitleLBL.frame.origin.y + paymentMethodTitleLBL.frame.size.height
        
        
        for i in 0..<paymentTypeArray.count
        {
            
            if "\((paymentTypeArray[i] as! NSDictionary)["type"]!)".elementsEqual("COD")
            {
                let cod = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (25/568)*self.FullHeight))
                
                cod.text = "Cash on Delivery"
                
                let separatorView = UIView(frame: CGRect(x: (10/320)*self.FullWidth, y: (44/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (1/568)*self.FullHeight))
                
                separatorView.backgroundColor = UIColor.lightGray
                
                let tempView = UIView(frame: CGRect(x: (0/320)*self.FullWidth, y: tempYOrigin, width: self.FullWidth, height: (45/568)*self.FullHeight))
                
                tempView.addSubview(cod)
                tempView.addSubview(separatorView)
                
                self.paymentMethodsView.addSubview(tempView)
                
                tempYOrigin = tempYOrigin + tempView.frame.size.height
                
                
                //
                cod.textAlignment = .left
                
                cod.textColor = UIColor.black
                
                cod.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
            }
            
            if "\((paymentTypeArray[i] as! NSDictionary)["type"]!)".elementsEqual("Bank Transfar")
            {
                let bankNameLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (25/568)*self.FullHeight))
                
                bankNameLBL.text = "Bank Name"
                
                let accountNumberLBL = UILabel(frame: CGRect(x: (140/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: (25/568)*self.FullHeight))
                
                accountNumberLBL.text = "Account Number"
                
                let bankName = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (25/568)*self.FullHeight))
                
                bankName.text = "\((paymentTypeArray[i] as! NSDictionary)["bank_name"]!)"
                
                let accountNumber = UILabel(frame: CGRect(x: (140/320)*self.FullWidth, y: (35/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: (25/568)*self.FullHeight))
                
                accountNumber.text = "\((paymentTypeArray[i] as! NSDictionary)["account_name"]!)"
                
                let separatorView = UIView(frame: CGRect(x: (10/320)*self.FullWidth, y: (69/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (1/568)*self.FullHeight))
                
                separatorView.backgroundColor = UIColor.lightGray
                
                let tempView = UIView(frame: CGRect(x: (0/320)*self.FullWidth, y: tempYOrigin, width: self.FullWidth, height: (70/568)*self.FullHeight))
                
                tempView.addSubview(bankNameLBL)
                tempView.addSubview(bankName)
                tempView.addSubview(accountNumberLBL)
                tempView.addSubview(accountNumber)
                tempView.addSubview(separatorView)
                
                self.paymentMethodsView.addSubview(tempView)
                
                tempYOrigin = tempYOrigin + tempView.frame.size.height
                
                
                
                //
                bankNameLBL.textAlignment = .left
                accountNumberLBL.textAlignment = .left
                bankName.textAlignment = .left
                accountNumber.textAlignment = .left
                
                bankNameLBL.textColor = UIColor.gray
                accountNumberLBL.textColor = UIColor.gray
                bankName.textColor = UIColor.black
                accountNumber.textColor = UIColor.black
                
                bankNameLBL.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                accountNumberLBL.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                bankName.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                accountNumber.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                
            }
            
            self.paymentMethodsView.autoresizesSubviews = false
            self.paymentMethodsView.frame.size.height = tempYOrigin
            self.paymentMethodsView.autoresizesSubviews = true
            
        }
        
        //
        //        let bankNameLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (45/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        //
        //        bankNameLBL.text = "Bank Name"
        //
        //        let accountNumberLBL = UILabel(frame: CGRect(x: (140/320)*self.FullWidth, y: (45/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        //
        //        accountNumberLBL.text = "Account Number"
        //
        //        let bankName = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (70/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        //
        //        bankName.text = "AXIS Bank"
        //
        //        let accountNumber = UILabel(frame: CGRect(x: (140/320)*self.FullWidth, y: (70/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        //
        //        accountNumber.text = "123456789"
        //
        //        let cod = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (95/568)*self.FullHeight, width: (130/320)*self.FullWidth, height: (25/568)*self.FullHeight))
        //
        //
        //        bankNameLBL.textAlignment = .left
        //        accountNumberLBL.textAlignment = .left
        //        bankName.textAlignment = .left
        //        accountNumber.textAlignment = .left
        //
        //        bankNameLBL.textColor = UIColor.gray
        //        accountNumberLBL.textColor = UIColor.gray
        //        bankName.textColor = UIColor.black
        //        accountNumber.textColor = UIColor.black
        //
        //        bankNameLBL.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        //        accountNumberLBL.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        //        bankName.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        //        accountNumber.font = UIFont(name: self.deliveryTimesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        //
        //        let separatorView = UIView(frame: CGRect(x: (10/320)*self.FullWidth, y: (99/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: (1/568)*self.FullHeight))
        //
        //        self.paymentMethodsView.addSubview(paymentMethodTitleLBL)
        //        self.paymentMethodsView.addSubview(bankNameLBL)
        //        self.paymentMethodsView.addSubview(bankName)
        //        self.paymentMethodsView.addSubview(accountNumberLBL)
        //        self.paymentMethodsView.addSubview(accountNumber)
        //
        
    }
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation()
    {
        
        for i in 0..<locationArray.count
        {
            addressLat = Double("\((locationArray[i] as! NSDictionary)["lat"]!)")
            addressLong = Double("\((locationArray[i] as! NSDictionary)["long"]!)")
            
            let marker = GMSMarker()
            
            let camera = GMSCameraPosition.camera(withLatitude: addressLat, longitude: addressLong, zoom: locationMapView.minZoom)
            
            marker.position = CLLocationCoordinate2D(latitude: addressLat, longitude: addressLong)
            marker.title = ""
            marker.snippet = "\((locationArray[i] as! NSDictionary)["address"]!)"
            marker.map = self.locationMapView
            
            self.locationMapView.animate(to: camera)
        }
        
        
    }
    
    
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
