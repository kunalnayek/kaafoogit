//
//  NewFoodCartViewController.swift
//  Kaafoo
//
//  Created by esolz on 12/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewFoodCartViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    
    //MARK:- //Variable Declaration
    
    var ProductID : String! = ""
    
    @IBOutlet weak var foodDescriptionView: UIView!
    @IBOutlet weak var foodname: UILabel!
    @IBOutlet weak var foodDescription: UILabel!
    @IBOutlet weak var foodPrice: UILabel!
    
    @IBOutlet weak var extraFeatureHeaderView: UIView!
    @IBOutlet weak var extraLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var FeatureTableView: UITableView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageTxtView: UITextView!
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var totalPrice: UILabel!
    
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var CloseBtnOutlet: UIButton!
    @IBOutlet weak var addtoCartBtnOutlet: UIButton!
    @IBAction func closeBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addtoCartBtn(_ sender: UIButton) {
        self.Checkcart()
    }
    
    
    
    
    //MARK:- // Variable Decalaration
    
    var tempPrice : Double = 0.00
    
    var foodDetailsStruct = [foodDetails]()
    
    var mainFoodStruct = [mainFoodDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.setlayer()
        
        self.messageTxtView.text = "Placeholder"
        self.messageTxtView.textColor = UIColor.lightGray
        
        self.messageTxtView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    // MARK:- // Check Cart
    
    func Checkcart()
    {
        self.globalDispatchgroup.enter()
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show()
            
        }
        
        let url = URL(string: GLOBALAPI + "app_add_to_cart")!   //change the url
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&product_id=\(ProductID!)&Lang_id=\(langID!)&qnty=\("1")"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Add To Cart API Fire " , json)
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                //let nav = self.storyboard?.instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
                                let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
                                
                                nav.LanguageID = self.langID as NSString?
                                
                                nav.ProductID = self.ProductID as? NSString
                                
                                self.navigationController?.pushViewController(nav, animated: true)
                                
                            })
                            
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                        
                        
                    else
                    {
                        
                        if "\(json["confirmstatus"]!)".elementsEqual("1")
                        {
                            
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                            }
                            
                            self.globalDispatchgroup.notify(queue: .main, execute: {
                                
                                let alert = UIAlertController(title: "Confirm", message: "You have tried to add products from different seller. If you still want to proceed please delete existent cart products", preferredStyle: .alert)
                                
                                let ok = UIAlertAction(title: "OK", style: .default) {
                                    UIAlertAction in
                                    self.globalDispatchgroup.notify(queue: .main, execute: {
                                        
                                        self.removeCarts {
                                            
                                            self.Checkcart()
                                            
                                        }
                                        
                                    })
                                }
                                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                
                                alert.addAction(ok)
                                
                                alert.addAction(cancel)
                                
                                self.present(alert, animated: true, completion: nil )
                                
                            })
                        }
                        else
                        {
                            self.globalDispatchgroup.leave()
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    // MARK:- // Remove Carts
       
       func removeCarts(completion : @escaping () -> ())
       {
           self.globalDispatchgroup.enter()
           
           DispatchQueue.main.async {
               SVProgressHUD.show()
           }
           
           
           
           let url = URL(string: GLOBALAPI + "app_remove_cart_by_userid")!   //change the url
           
           print("add from watchlist URL : ", url)
           
           var parameters : String = ""
           
           let langID = UserDefaults.standard.string(forKey: "langID")
           
           let userID = UserDefaults.standard.string(forKey: "userID")
           
           parameters = "user_id=\(userID!)&lang_id=\(langID!)"
           
           print("Parameters are : " , parameters)
           
           let session = URLSession.shared
           
           var request = URLRequest(url: url)
           request.httpMethod = "POST" //set http method as POST
           
           do {
               request.httpBody = parameters.data(using: String.Encoding.utf8)
               
               
           }
           
           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
               
               guard error == nil else {
                   return
               }
               
               guard let data = data else {
                   return
               }
               
               do {
                   
                   if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                       print("Remove All Item From Cart API " , json)
                       
                       DispatchQueue.main.async {
                           
                           self.globalDispatchgroup.leave()
                           
                           SVProgressHUD.dismiss()
                           
                           self.globalDispatchgroup.notify(queue: .main, execute: {
                               
                               completion()
                               
                           })
                           
                           
                           
                       }
                       
                   }
                   
               } catch let error {
                   print(error.localizedDescription)
               }
           })
           
           task.resume()
       }
    
    
    //MARK:- //set data
    
    func setData()
    {
        self.foodname.text = "\(self.mainFoodStruct[0].foodName ?? "")"
        self.foodDescription.text = "\(self.mainFoodStruct[0].foodDescription ?? "")".htmlToString
        self.foodPrice.text = "\(self.mainFoodStruct[0].foodPrice ?? "")"
        
        self.FeatureTableView.delegate = self
        self.FeatureTableView.dataSource = self
        self.FeatureTableView.reloadData()
        
    }
    
    //MARK:- //Set layer to Views
    func setlayer()
    {
        self.foodDescriptionView.layer.borderWidth = 0.5
        self.foodDescriptionView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.messageTxtView.layer.borderWidth = 0.5
        self.messageTxtView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.FeatureTableView.tableFooterView = UIView()
        self.FeatureTableView.isScrollEnabled = false
    }
    
    //MARK:- //tableview datasource and delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.foodDetailsStruct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodcart") as! NewFoodCartTVC
        
        cell.featureName.text = "\(self.foodDetailsStruct[indexPath.row].featureName ?? "")"
        cell.featurePrice.text = "\(self.foodDetailsStruct[indexPath.row].featurePrice ?? "")"
        cell.featureQuantity.text = String(self.foodDetailsStruct[indexPath.row].featureQuantity)
        cell.plusBtnOutlet.tag = indexPath.row
        cell.minusBtnOutlet.tag = indexPath.row
        cell.plusBtnOutlet.addTarget(self, action: #selector(increaseQuantity(sender:)), for: .touchUpInside)
        cell.minusBtnOutlet.addTarget(self, action: #selector(decreaseQuantity(sender:)), for: .touchUpInside)
        
        //MARK:- //Price Calculation
        
//       tempPrice = 0.00 + (Double("\(self.foodDetailsStruct[indexPath.row].featureOnlyPrice! as NSString)")! * Double(self.foodDetailsStruct[indexPath.row].featureQuantity))
//
//
//        let totaltempPrice = tempPrice + Double("\(self.mainFoodStruct[0].foodOnlyPrice! as NSString)")!
//
//        print("quantity",Double(self.foodDetailsStruct[indexPath.row].featureQuantity))
//
//
//        self.totalPrice.text = "Total Price : " + "\(self.mainFoodStruct[0].foodPriceCurrency ?? "")" + String(totaltempPrice)
        
        var sum = 0.0
        
        for item in self.foodDetailsStruct {
            sum += Double("\(item.featureOnlyPrice! as NSString)")! * Double(item.featureQuantity)
        }
        
        let totaltempPrice = sum + Double("\(self.mainFoodStruct[0].foodOnlyPrice! as NSString)")!
        
        self.totalPrice.text = "Total Price :" + String(totaltempPrice)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- //Plus Button Objective C Method
    @objc func increaseQuantity(sender : UIButton)
    {
        var tempInt = Int(self.foodDetailsStruct[sender.tag].featureQuantity)
        tempInt = tempInt + 1
        self.foodDetailsStruct[sender.tag].featureQuantity = tempInt
        self.FeatureTableView.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath], with: UITableView.RowAnimation.fade)
    }
    
    //MARK:- //Minus Button Objective C Method
    @objc func decreaseQuantity(sender : UIButton)
    {
       var tempInt = Int(self.foodDetailsStruct[sender.tag].featureQuantity)
       tempInt = tempInt - 1
       if tempInt < 0
       {
         self.ShowAlertMessage(title: "Warning", message: "Quantity can not be less than 0")
       }
       else
       {
        self.foodDetailsStruct[sender.tag].featureQuantity = tempInt
        self.FeatureTableView.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath], with: UITableView.RowAnimation.fade)
       }
    }
    
    //MARK:- TextView Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if messageTxtView.textColor == UIColor.lightGray {
            messageTxtView.text = nil
            messageTxtView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTxtView.text.isEmpty {
            messageTxtView.text = "Placeholder"
            messageTxtView.textColor = UIColor.lightGray
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class NewFoodCartTVC : UITableViewCell
{
    @IBOutlet weak var featureName : UILabel!
    
    @IBOutlet weak var featureQuantity : UILabel!
    
    @IBOutlet weak var featurePrice : UILabel!
    
    @IBOutlet weak var minusBtnOutlet: UIButton!
    
    @IBOutlet weak var plusBtnOutlet: UIButton!
    
}
