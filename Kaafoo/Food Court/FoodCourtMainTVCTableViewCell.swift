//
//  FoodCourtMainTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 30/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class FoodCourtMainTVCTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var addressPlaceholderImage: UIImageView!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var cellBottomView: UIView!
    @IBOutlet weak var deliveryTypeLBL: UILabel!
    @IBOutlet weak var firstSeparatorView: UIView!
    @IBOutlet weak var estimatedDeliveryTime: UILabel!
    @IBOutlet weak var secondSeparatorView: UIView!
    @IBOutlet weak var priceLBL: UILabel!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
