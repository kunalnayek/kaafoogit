//
//  CommercialAdsVC.swift
//  Kaafoo
//
//  Created by priya on 20/07/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage

class CommercialAdsVC: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var rangeCollectionView: UICollectionView!
    
    @IBOutlet weak var typeofaccountCollectionView: UICollectionView!
    
    @IBOutlet weak var kaafoobannercollectionView: UICollectionView!
    
    @IBOutlet weak var socialmediaAccountCollectionview: UICollectionView!
    @IBOutlet weak var socialmediaCollectionView: UICollectionView!
    
    var bannerarr : NSMutableArray! = NSMutableArray()
    var socialmediaarr : NSMutableArray! = NSMutableArray()
    var socialmediaaccountarr : NSMutableArray! = NSMutableArray()
    var typeaccountarr : NSMutableArray! = NSMutableArray()
    var rangearr : NSMutableArray! = NSMutableArray()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        bannerarr = ["Main page","Second page","Third page","Fourth page","Fifth page","Top page","Bottom page","Middle page","Left page","Right page"]
        
      
        socialmediaarr = ["facebook","twitter","icon_youtube","snapchat","icon_instagram","icon_googleplus","icon_linkedin","icon_pinterest"]
        
        typeaccountarr = ["Daily life vlogs","Building/Construction","Games","Health","Real Estate","Vehicles","Travel","Animals","Beauty","Farmimg","Movie","Sports","Fun/Entertainment","Handcraft","Electronics","Garden","Educational","Celebrity","Cooking/Baking","Camping/Hiking","Job seeking","Interior decoration/Design","Children","Others"]
        
        rangearr = ["100,000 - 200,000","300,000 - 500,000","600,000 - 1,000,000","2,000,000 - 3,000,000","4,000,000 - 6,000,000","7,000,000 - 10,000,000","11,000,000 - 20,000,000"]
        
       
        
        kaafoobannercollectionView.dataSource = self
        kaafoobannercollectionView.delegate = self
        kaafoobannercollectionView.reloadData()
        
        socialmediaCollectionView.dataSource = self
        socialmediaCollectionView.delegate = self
        socialmediaCollectionView.reloadData()
        
        socialmediaAccountCollectionview.dataSource = self
        socialmediaAccountCollectionview.delegate = self
        socialmediaAccountCollectionview.reloadData()
        
        typeofaccountCollectionView.dataSource = self
        typeofaccountCollectionView.delegate = self
        typeofaccountCollectionView.reloadData()
        
        rangeCollectionView.dataSource = self
        rangeCollectionView.delegate = self
        rangeCollectionView.reloadData()
        
        
    }
    
    @IBAction func submitBtnClicked(_ sender: Any)
    {
        
    }
    
   // MARK:- // Collectionview Delegates
      
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
          if collectionView == self.kaafoobannercollectionView
          {
              return self.bannerarr.count
          }
            
          else  if collectionView == self.typeofaccountCollectionView
            {
                return self.typeaccountarr.count
            }
            
          else if collectionView == self.rangeCollectionView
          {
            return self.rangearr.count
          }
        else
          {
            return self.socialmediaarr.count
        }
          
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          if collectionView == self.kaafoobannercollectionView
          {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommercialAdsCollectionViewCell", for: indexPath) as! CommercialAdsCollectionViewCell
              
             
              
        cell.bannertitle.text = self.bannerarr[indexPath.row] as? String
              
              return cell
          }
          else  if collectionView == self.socialmediaCollectionView

          {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "socialmediaCollectionViewCell", for: indexPath) as! socialmediaCollectionViewCell
                         
            cell.socialmediaimg?.image = UIImage(named: socialmediaarr![indexPath.row] as! String)
            
            return cell
            
        }
            
            else  if collectionView == self.rangeCollectionView

        {
                       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RangeCollectionViewCell", for: indexPath) as! RangeCollectionViewCell
                                    
            cell.rangeLBL.text = self.rangearr[indexPath.row] as? String
                       
            return cell
                       
        }
            
            else  if collectionView == self.typeofaccountCollectionView

              {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeofAccountCollectionViewCell", for: indexPath) as! TypeofAccountCollectionViewCell
                             
               cell.typetitle.text = self.typeaccountarr[indexPath.row] as? String
                
                return cell
                
            }
        
        else
          {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "socialmediaAccountCollectionViewCell", for: indexPath) as! socialmediaAccountCollectionViewCell
                         
            cell.socialmediaAccountimg?.image = UIImage(named: socialmediaarr![indexPath.row] as! String)
            
            return cell
            
        }
          
          
      }
      
      func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          sizeForItemAt indexPath: IndexPath) -> CGSize {
          
//          if collectionView == self.rangeCollectionView
//          {
//              let cellsize = CGSize(width: 160, height: 80)
//              return cellsize
//          }
//          else
//
//          {
            let cellsize = CGSize(width: 160, height: 50)
             return cellsize
        //  }

          
          
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          
          return 0
          
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          
          return 0
          
      }
      
      
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
         
          
      }
      
      

}

class CommercialAdsCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var bannertitle: UILabel!
}

class socialmediaCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var socialmediaimg: UIImageView!
}

class socialmediaAccountCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var socialmediaAccountimg: UIImageView!
}

class TypeofAccountCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var typetitle: UILabel!
    
}
class RangeCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var rangeLBL: UILabel!
}
