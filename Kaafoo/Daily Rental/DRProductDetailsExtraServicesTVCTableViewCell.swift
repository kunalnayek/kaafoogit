//
//  DRProductDetailsExtraServicesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 26/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class DRProductDetailsExtraServicesTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceCharge: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
