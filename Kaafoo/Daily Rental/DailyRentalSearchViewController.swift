//
//  DailyRentalSearchViewController.swift
//  Kaafoo
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation
import GoogleMaps
import GooglePlaces

class DailyRentalSearchViewController: GlobalViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- //GeoCoding For Pickup Location and DropOff Location
   
    
    var pickupClicked : Bool! = false
    
    @IBOutlet weak var localHeaderview: UIView!
    
    @IBOutlet weak var localHeaderTitleLogo: UIImageView!
    
    @IBOutlet weak var localHeadertitleLBL: UILabel!
    
    @IBOutlet weak var ArrowView: UIView!
    
    @IBOutlet weak var ScrollableCollectionView: UICollectionView!
    
    var ClickedIndex : Int! = 0
    
    @IBOutlet weak var viewOfTypeScroll: UIView!
    
    @IBOutlet weak var typeScrollview: UIScrollView!
    
    @IBOutlet weak var pickupView: UIView!
    
    @IBOutlet weak var pickupPlaceholder: UIImageView!
    
    @IBOutlet weak var pickupLocationLBL: UILabel!
    
    @IBOutlet weak var pickupLocationTXT: UITextField!
    
    @IBOutlet weak var pickupDateLBL: UILabel!
    
    @IBOutlet weak var pickupDateTXT: UITextField!
    
    @IBOutlet weak var pickupTimeLBL: UILabel!
    
    @IBOutlet weak var pickupTimeTXT: UITextField!
    
    @IBOutlet weak var dropoffView: UIView!
    
    @IBOutlet weak var dropoffPlaceholder: UIImageView!
    
    @IBOutlet weak var dropoffLocationLBL: UILabel!
    
    @IBOutlet weak var dropoffLocationTXT: UITextField!
    
    @IBOutlet weak var dropoffDateLBL: UILabel!
    
    @IBOutlet weak var dropoffDateTXT: UITextField!
    
    @IBOutlet weak var dropoffTimeLBL: UILabel!
    
    @IBOutlet weak var dropoffTimeTXT: UITextField!
    
    @IBOutlet weak var driverAgeView: UIView!
    
    @IBOutlet weak var driverAgeLBL: UILabel!
    
    @IBOutlet weak var driverAge: UILabel!
    
    @IBOutlet weak var dropDownTriangleImage: UIImageView!
    
    @IBOutlet weak var viewOfDropdownTableview: UIView!
    
    @IBOutlet weak var driverAgedropdownTableview: UITableView!
    
    @IBOutlet weak var ShowPickerView: UIView!
    
    @IBOutlet weak var HidePickerViewBtnOutlet: UIButton!
    
    @IBOutlet weak var CountryPicker: UIPickerView!
    
    @IBAction func HidePickerBtn(_ sender: UIButton) {
        
        self.ShowPickerView.isHidden = true
    }
    
    
    var MycountryId : String! = UserDefaults.standard.string(forKey: "countryID")
    
    var MyCountryName : String! = UserDefaults.standard.string(forKey: "countryName")
    
    var CityListArray : NSMutableArray! = NSMutableArray()
    
    var SelectedCategoryID : String! = "1003"
    
    @IBOutlet weak var DriverAgetableViewHeightConstraint: NSLayoutConstraint!
    
    var dailyRentalTypesArray = ["Cars","Caravans","Motorbikes","Trucks","Heavy Vehicles","Trailers","Buses","Aircraft","Equipment"]

//    var dailyRentalTypesArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "cars", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "caravans", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "motorbikes", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "trucks", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "heavyvehicles", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "trailers", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "buses", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "aircraft", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "equipment", comment: "")]

    var driverAgeArray = ["16","17","18","19","20","21","22","23","24","25","25+"]
    var ID = ["1003","1185","1189","1190","1188","1191","1186","1187","1006"]

    
    
    
    let dispatchGroup = DispatchGroup()
    
    var dailyRentalTypesButtonArray = [UIButton]()
    
    var isChange : Bool = false
    
    private var datepickerWithDate : UIDatePicker?
    
    private var datepickerWithTime : UIDatePicker?
    
    var productID : String!
    
    var categoryArray : NSMutableArray! = NSMutableArray()
    
    var categoryID : String!
    
    var age : String! = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ShowPickerView.isHidden = true
        self.getCityList()
//        self.SetUpHeaderView()
        
        self.localHeadertitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dailyRentals", comment: "")

        self.pickupDateTXT.delegate = self
        self.pickupTimeTXT.delegate = self
        self.dropoffDateTXT.delegate = self
        self.dropoffTimeTXT.delegate = self
        
        
        self.pickupLocationTXT.delegate = self
        self.dropoffLocationTXT.delegate = self
        
        self.driverAgedropdownTableview.delegate = self
        self.driverAgedropdownTableview.dataSource = self

        // Do any additional setup after loading the view.
        
        self.getCategoryList()
        
        dispatchGroup.notify(queue: .main) {
            
            self.setFont()
            
//            self.setScroll()

            self.changeContent(change: self.isChange)
            
//            self.dailyRentalTypesButtonArray[0].setTitleColor(UIColor.white, for: .normal)
//            self.dailyRentalTypesButtonArray[0].backgroundColor = UIColor(red: (255/255), green: (171/255), blue: (1/255), alpha: 1.0)
//
//            self.categoryID = "\((self.categoryArray[0] as! NSDictionary)["id"]!)"

            self.setDatepickerWithDate()
            self.setDatepickerWithTime()
            
        }
        self.ScrollableCollectionView.delegate = self
        self.ScrollableCollectionView.dataSource = self
        self.ScrollableCollectionView.reloadData()
        
//        pickupView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        dropoffView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        driverAgeView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        //self.searchNowButtonOutlet.layer.cornerRadius = self.searchNowButtonOutlet.frame.size.height / 2

        self.searchNowButtonOutlet.layer.cornerRadius = 5.0
        
        headerView.contentView.backgroundColor = .clear//UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
        pickupLocationTXT.inputView = UIView.init(frame: CGRect.zero)
        pickupLocationTXT.inputAccessoryView = UIView.init(frame: CGRect.zero)
        
        dropoffLocationTXT.inputView = UIView.init(frame: CGRect.zero)
        dropoffLocationTXT.inputAccessoryView = UIView.init(frame: CGRect.zero)
        
    }
    
    //MARK:- //View Will Appear Method
    
    override func viewWillAppear(_ animated: Bool) {
        self.ShowPickerView.isHidden = true
    }

    //MARK:- GetCityList
    
    func getCityList()
    {
        let parameters = "lang_id=\(langID!)&mycountry_id=\(MycountryId ?? "")"
        self.CallAPI(urlString: "app_daily_rental_search_view", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.CityListArray = self.globalJson["pick_up_city"] as? NSMutableArray
                DispatchQueue.main.async {
                    self.CountryPicker.delegate = self
                    self.CountryPicker.dataSource = self
                    self.CountryPicker.reloadAllComponents()
                }
            })
        })
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Driver Age Dropdown Button
    
    @IBOutlet weak var driverAgeDropdownButtonOutlet: UIButton!
    @IBAction func tapOnDriverAgeDropdown(_ sender: UIButton) {
        
        if viewOfDropdownTableview.isHidden{
            animateDriverAge(toggle: true)
            
        }
        else {
            animateDriverAge(toggle: false)
            
        }
        
    }

    // MARK:- // Search Now Button
    
    
    @IBOutlet weak var searchNowButtonOutlet: UIButton!
    @IBAction func searchNowButton(_ sender: UIButton) {
        
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalListingVC") as! DailyRentalListingViewController
        
        UserDefaults.standard.set(self.categoryID, forKey: "dailyRentalCategoryID")
        
        navigate.categoryID = SelectedCategoryID
        navigate.pickupDate = self.pickupDateTXT.text
        navigate.pickupTime = self.pickupTimeTXT.text
        navigate.dropoffDate = self.dropoffDateTXT.text
        navigate.dropoffTime = self.dropoffTimeTXT.text
        navigate.driverAge = self.age


//        var finalPickuptext = "\(self.pickupDateTXT.text!),\(self.pickupTimeTXT.text!)"

//        navigate.PickupDetails.text = "\(finalPickuptext ?? "")"

//        if let pickupdatename = self.pickupDateTXT.text
//        {
//            navigate.PickupDetails.text?.append(pickupdatename)
//        }
//
//        if let pickuptimename = self.pickupTimeTXT.text
//        {
//            navigate.PickupDetails.text?.append(pickuptimename)
//        }

        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    // MARK:- // Defines Functions
    
    // MARK:- // Setup Daily Rental Type Scrollview
    
//    func setScroll()
//    {
//        var tempOrigin : CGFloat! = (20/320)*self.FullWidth
//
//        for i in 0..<self.categoryArray.count
//        {
//            let tempView = UIView(frame: CGRect(x: tempOrigin, y: 0, width: (40/320)*self.FullWidth, height: (40/568)*self.FullHeight))
//
//            let tempButton = UIButton(frame: CGRect(x: (10/320)*self.FullWidth, y: 0, width: (40/320)*self.FullWidth, height: (40/568)*self.FullHeight))
//
//            tempButton.setTitle("\((categoryArray[i] as! NSDictionary)["name"]!)", for: .normal)
//
//            tempButton.sizeToFit()
//
//            tempButton.tag = i
//            tempButton.addTarget(self, action: #selector(DailyRentalSearchViewController.typeClick), for: .touchUpInside)
//            tempButton.setTitleColor(UIColor.black, for: .normal)
//            tempButton.titleLabel?.font = UIFont(name: (tempButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
//
//            tempView.frame.size.width = tempButton.frame.size.width + (20/320)*self.FullWidth
//
//            tempView.backgroundColor = .white
//
//            tempView.layer.cornerRadius = 2.0
//
//            tempButton.layer.cornerRadius = 2.0
//
//            dailyRentalTypesButtonArray.append(tempButton)
//
//            tempView.addSubview(tempButton)
//            self.typeScrollview.addSubview(tempView)
//
//            tempOrigin = tempOrigin + tempView.frame.size.width
//
//            self.typeScrollview.contentSize = CGSize(width: tempOrigin, height: (40/568)*self.FullHeight)
//        }
//        self.typeScrollview.bounces = false
//    }

    
    // MARK: - // Function for type Click
    
    
    @objc func typeClick(sender: UIButton)
    {
        
        categoryID = "\((categoryArray[sender.tag] as! NSDictionary)["id"]!)"
       
//        for i in 0..<dailyRentalTypesButtonArray.count
//        {
//            dailyRentalTypesButtonArray[i].setTitleColor(UIColor.black, for: .normal)
//            dailyRentalTypesButtonArray[i].backgroundColor = .white
//
//
//        }
//        dailyRentalTypesButtonArray[sender.tag].setTitleColor(UIColor.white, for: .normal)
//         dailyRentalTypesButtonArray[sender.tag].backgroundColor = UIColor(red: (255/255), green: (171/255), blue: (1/255), alpha: 1.0)

        if dailyRentalTypesArray[sender.tag].elementsEqual("Aircraft")
        {
            self.isChange = true
            
            self.driverAgeView.isHidden = true
            
            self.searchNowButtonOutlet.frame.origin.y = self.dropoffView.frame.origin.y + self.dropoffView.frame.size.height + (10/568)*self.FullHeight
        }
        else if dailyRentalTypesArray[sender.tag].elementsEqual("Equipment")
        {
            self.isChange = true
            
            self.driverAgeView.isHidden = true
            
            self.searchNowButtonOutlet.frame.origin.y = self.dropoffView.frame.origin.y + self.dropoffView.frame.size.height + (10/568)*self.FullHeight
        }
        else
        {
            self.isChange = false
            
            self.driverAgeView.isHidden = false
            
            self.searchNowButtonOutlet.frame.origin.y = self.driverAgeView.frame.origin.y + self.driverAgeView.frame.size.height + (10/568)*self.FullHeight
        }
        
        self.changeContent(change: isChange)
        
    }
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        pickupLocationLBL.font = UIFont(name: pickupLocationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupLocationTXT.font = UIFont(name: (pickupLocationTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        pickupDateLBL.font = UIFont(name: pickupDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupDateTXT.font = UIFont(name: (pickupDateTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        pickupTimeLBL.font = UIFont(name: pickupTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupTimeTXT.font = UIFont(name: (pickupTimeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        dropoffLocationLBL.font = UIFont(name: dropoffLocationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffLocationTXT.font = UIFont(name: (dropoffLocationTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        dropoffDateLBL.font = UIFont(name: dropoffDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffDateTXT.font = UIFont(name: (dropoffDateTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        dropoffTimeLBL.font = UIFont(name: dropoffTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffTimeTXT.font = UIFont(name: (dropoffTimeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        driverAgeLBL.font = UIFont(name: driverAgeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        driverAge.font = UIFont(name: driverAge.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
    }
    
    
    // MARK:- // Change Content
    
    func changeContent(change: Bool)
    {
        if change == false
        {
            pickupLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupLocation", comment: "")
            pickupLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupLocation", comment: "")
            pickupDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupDate", comment: "")
            pickupDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupDate", comment: "")
            pickupTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupTime", comment: "")
            pickupTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupTime", comment: "")
            
            dropoffLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffLocation", comment: "")
            dropoffLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffLocation", comment: "")
            dropoffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffDate", comment: "")
            dropoffDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffDate", comment: "")
            dropoffTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffTime", comment: "")
            dropoffTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffTime", comment: "")
            
        }
        else
        {
            pickupLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureLocation", comment: "")
            pickupLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureLocaiton", comment: "")
            pickupDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureDate", comment: "")
            pickupDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureDate", comment: "")
            pickupTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureTime", comment: "")
            pickupTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "departureTime", comment: "")
            
            dropoffLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationLocation", comment: "")
            dropoffLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationLocaiton", comment: "")
            dropoffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationDate", comment: "")
            dropoffDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationDate", comment: "")
            dropoffTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationTime", comment: "")
            dropoffTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "destinationTime", comment: "")
        }
        
        driverAgeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "driverAge", comment: "")
        driverAge.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "driverAge", comment: "")
        
    }
    
    // MARK:- // Set up Datepicker with Date
    
    func setDatepickerWithDate()
    {
        datepickerWithDate = UIDatePicker()
        datepickerWithDate?.datePickerMode = .date
        pickupDateTXT.inputView = datepickerWithDate
        dropoffDateTXT.inputView = datepickerWithDate
    }
    
    // MARK:- // Set up Datepicker with Time
    
    func setDatepickerWithTime()
    {
        datepickerWithTime = UIDatePicker()
        datepickerWithTime?.datePickerMode = .time
        pickupTimeTXT.inputView = datepickerWithTime
        dropoffTimeTXT.inputView = datepickerWithTime
        
        
        // 24 hour format
        datepickerWithTime?.locale = NSLocale(localeIdentifier: "en_GB") as Locale
    }
    
    
    // MARK: - // Animate Driver Age DropDown Tableview
    
    func animateDriverAge(toggle: Bool) {
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                
                self.view.bringSubviewToFront(self.viewOfDropdownTableview)
                self.viewOfDropdownTableview.autoresizesSubviews = false
                self.viewOfDropdownTableview.isHidden = false

//                let tblheight = self.driverAgedropdownTableview.visibleCells[0].bounds.height

//                self.DriverAgetableViewHeightConstraint.constant = CGFloat(self.driverAgeArray.count) * tblheight
                self.DriverAgetableViewHeightConstraint.constant = (150/568)*self.FullHeight

//                self.viewOfDropdownTableview.frame.size.height = ((100/568)*self.FullHeight)
//                self.driverAgedropdownTableview.frame.size.height = ((100/568)*self.FullHeight)
                self.viewOfDropdownTableview.autoresizesSubviews = true
                
                self.viewOfDropdownTableview.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
                
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.viewOfDropdownTableview.autoresizesSubviews = false
                self.viewOfDropdownTableview.isHidden = true
                self.DriverAgetableViewHeightConstraint.constant = 0
//                self.viewOfDropdownTableview.frame.size.height = ((1/568)*self.FullHeight)
//                self.driverAgedropdownTableview.frame.size.height = ((1/568)*self.FullHeight)
                self.viewOfDropdownTableview.autoresizesSubviews = true
                self.view.sendSubviewToBack(self.viewOfDropdownTableview)
            }
        }
        
    }
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Textfield Delegates
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let dateFormat = DateFormatter()
        let APIdateFormat = DateFormatter()
        
        if textField == pickupDateTXT
        {
            dateFormat.dateFormat = "dd-MM-yyyy"
            APIdateFormat.dateFormat = "dd/MM/yyyy"
            self.pickupDateTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
            
            dailyRentalSearchParameters.shared.setPickupDate(PIckupdate: APIdateFormat.string(from: (self.datepickerWithDate?.date)!))
        }
        else if textField == dropoffDateTXT
        {
            dateFormat.dateFormat = "dd-MM-yyyy"
            APIdateFormat.dateFormat = "dd/MM/yyyy"
            self.dropoffDateTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
            dailyRentalSearchParameters.shared.setDropOffdate(DropOffDate: APIdateFormat.string(from: (self.datepickerWithDate?.date)!))
        }
        else if textField == pickupTimeTXT
        {
            dateFormat.dateFormat = "HH:mm"
            APIdateFormat.dateFormat = "h:mm a"
            self.pickupTimeTXT.text = dateFormat.string(from: (self.datepickerWithTime?.date)!)
            dailyRentalSearchParameters.shared.setPickupTime(pickupTIME: APIdateFormat.string(from: (self.datepickerWithTime?.date)!))
            
        }
        else if textField == dropoffTimeTXT
        {
            dateFormat.dateFormat = "HH:mm"
            APIdateFormat.dateFormat = "h:mm a"
            self.dropoffTimeTXT.text = dateFormat.string(from: (self.datepickerWithTime?.date)!)
            dailyRentalSearchParameters.shared.setdropOffTime(DropOffTime: APIdateFormat.string(from: (self.datepickerWithTime?.date)!))
        }
        else
        {
            //do nothing
        }
        
    }
    
    
    // MARK:- // Tableview Delegates
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return driverAgeArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")
        
        cell?.textLabel?.text = driverAgeArray[indexPath.row] + " years"
        
        cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (20/568)*self.FullHeight
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        driverAge.text = driverAgeArray[indexPath.row]
        
        age = driverAgeArray[indexPath.row]
        
        dailyRentalSearchParameters.shared.setDriverAge(driverage: driverAgeArray[indexPath.row])
        
        driverAge.textColor = UIColor.black
        
        animateDriverAge(toggle: false)
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to get Category Data
    
    func getCategoryList()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_category_list")!   //change the url
        
        var parameters : String = ""
        
        parameters = "mycountry_id=99"
        
        print("Category List URL is : ",url)
        
        print("Parameters Are : ",parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Category List Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        for i in 0..<(json["info_categories"] as! NSArray).count
                        {
                            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                            let tempname = "\(((json["info_categories"] as! NSArray)[i] as! NSDictionary)[categoryName ?? ""]!)"
                            let tempID = "\(((json["info_categories"] as! NSArray)[i] as! NSDictionary)["id"]!)"
                            
                            let tempDictionary = ["id" : "\(tempID)" , "name" : "\(tempname)"]
                            
                            self.categoryArray.add(tempDictionary)
                        }
                        
                        self.categoryID = "\((self.categoryArray[0] as! NSDictionary)["id"]!)"
                        
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    func SetUpHeaderView()
    {
        headerView.HeaderImageView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 12).isActive = true
        headerView.HeaderImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        headerView.headerViewTitle.topAnchor.constraint(equalTo: headerView.HeaderImageView.bottomAnchor, constant: 3).isActive = true
        headerView.headerViewTitle.heightAnchor.constraint(equalToConstant: 15).isActive = true

    }

    //MARK:- Collectionview Delegate and datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dailyRentalTypesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "dailyScroll", for: indexPath) as! DailyRentalScrollableCollectionViewCell
        obj.namebl.isHidden = false
        if indexPath.row == ClickedIndex
        {
//            obj.ClickedBtn.backgroundColor = UIColor.yellow2()
            obj.ClickedBtn.setBackgroundColor(color: UIColor.yellow2(), forState: .normal)
//            obj.backgroundColor = UIColor.yellow2()
            print("Yellow")
        }
        else
        {
//            obj.ClickedBtn.backgroundColor = UIColor.ShadowDark()
            obj.ClickedBtn.setBackgroundColor(color: UIColor.ShadowDark(), forState: .normal)
//            obj.backgroundColor = UIColor.ShadowDark()
            print("ShadowDark")
        }
//        obj.namebl.text = dailyRentalTypesArray[indexPath.row]
        obj.ClickedBtn.setTitle(dailyRentalTypesArray[indexPath.row], for: .normal)
        obj.ClickedBtn.tag = indexPath.row
        obj.ClickedBtn.addTarget(self, action: #selector(typeClick(sender:)), for: .touchUpInside)
        return obj
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "dailyScroll", for: indexPath) as! DailyRentalScrollableCollectionViewCell
        obj.ClickedBtn.setTitle(dailyRentalTypesArray[indexPath.row], for: .normal)
//        obj.namebl.text = dailyRentalTypesArray[indexPath.row]
        obj.ClickedBtn.sizeToFit()
        print("height",obj.ClickedBtn.frame.width)
        return CGSize(width: obj.ClickedBtn.frame.width + (20/320)*self.FullHeight, height: 41)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//         let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "dailyScroll", for: indexPath) as! DailyRentalScrollableCollectionViewCell
         ClickedIndex = indexPath.row
        
         SelectedCategoryID = ID[indexPath.row]
        
//        print("SelectedCategoryID",SelectedCategoryID)
        
         if ClickedIndex == (dailyRentalTypesArray.count - 1)
         {
            ArrowView.isHidden = true
         }
         else
         {
            ArrowView.isHidden = false
         }
         ScrollableCollectionView.reloadData()
    }
    
    //MARK:- Google AutoComplete Objective C Method
    
    
    // Present the Autocomplete view controller when the button is pressed.
    @objc func autocompleteClicked(_ sender: UITextField) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK:- //Textfield DidbeginEditing Method
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == self.pickupLocationTXT
//        {
//            self.pickupLocationTXT.addTarget(self, action: #selector(autocompleteClicked(_:)), for: UIControl.Event.editingDidBegin)
//        }
//        else
//        {
//            self.dropoffLocationTXT.addTarget(self, action: #selector(autocompleteClicked(_:)), for: UIControl.Event.editingDidBegin)
//        }
        
         if textField == pickupLocationTXT
        {
            
            self.ShowPickerView.isHidden = false
            pickupClicked = true
            self.CountryPicker.selectRow(0, inComponent: 0, animated: true)
//
//            let autocompleteController = GMSAutocompleteViewController()
//            autocompleteController.delegate = self
//            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//                UInt(GMSPlaceField.placeID.rawValue))!
//            autocompleteController.placeFields = fields
//
//            // Specify a filter.
//            let filter = GMSAutocompleteFilter()
//            filter.type = .address
//            autocompleteController.autocompleteFilter = filter
//
//            // Display the autocomplete view controller.
//            present(autocompleteController, animated: true, completion: nil)
        }
        else if textField == dropoffLocationTXT
        {
            self.ShowPickerView.isHidden = false
            pickupClicked = false
            self.CountryPicker.selectRow(0, inComponent: 0, animated: true)
//            let autocompleteController = GMSAutocompleteViewController()
//            autocompleteController.delegate = self
//            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//                UInt(GMSPlaceField.placeID.rawValue))!
//            autocompleteController.placeFields = fields
//
//            // Specify a filter.
//            let filter = GMSAutocompleteFilter()
//            filter.type = .address
//            autocompleteController.autocompleteFilter = filter
//
//            // Display the autocomplete view controller.
//            present(autocompleteController, animated: true, completion: nil)
        }
        else
         {
            //do nothing
        }
    }

}

//extension UIToolbar {
//
//    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
//
//        let toolBar = UIToolbar()
//
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor.black
//        toolBar.sizeToFit()
//
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//
//        toolBar.setItems([ spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//
//        return toolBar
//    }
//
//}
extension DailyRentalSearchViewController : GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
       
        if pickupClicked == true
        {
           self.pickupLocationTXT.text = "\(place.name!)"
        }
        else
        {
            self.dropoffLocationTXT.text = "\(place.name!)"
        }
        
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension DailyRentalSearchViewController : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.CityListArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((self.CityListArray[row] as! NSDictionary)["pick_up_data"] as! String)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.pickupClicked == true
        {
            self.pickupLocationTXT.text = "\((self.CityListArray[row] as! NSDictionary)["pick_up_data"] as! String)"
            self.ShowPickerView.isHidden = true
            dailyRentalSearchParameters.shared.setPickupLocation(pickuplocation: "\((self.CityListArray[row] as! NSDictionary)["pick_up_data"] as! String)")
        }
        else if self.pickupClicked == false
        {
            self.dropoffLocationTXT.text = "\((self.CityListArray[row] as! NSDictionary)["pick_up_data"] as! String)"
            self.ShowPickerView.isHidden = true
            dailyRentalSearchParameters.shared.setdropofflocation(DropOffLocation: "\((self.CityListArray[row] as! NSDictionary)["pick_up_data"] as! String)")
        }
        else
        {
            //do nothing
        }
    }
}
