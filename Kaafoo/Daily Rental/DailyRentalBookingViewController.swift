//
//  DailyRentalBookingViewController.swift
//  Kaafoo
//
//  Created by admin on 27/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class DailyRentalBookingViewController: GlobalViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet weak var SelectTitletableviewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var pickupPlaceholder: UIImageView!
    @IBOutlet weak var pickupLocationLBL: UILabel!
    @IBOutlet weak var pickupLocationTXT: UITextField!
    @IBOutlet weak var pickupDateLBL: UILabel!
    @IBOutlet weak var pickupDateTXT: UITextField!
    @IBOutlet weak var pickupTimeLBL: UILabel!
    @IBOutlet weak var pickupTimeTXT: UITextField!
    @IBOutlet weak var dropoffView: UIView!
    @IBOutlet weak var dropoffPlaceholder: UIImageView!
    @IBOutlet weak var dropoffLocationLBL: UILabel!
    @IBOutlet weak var dropoffLocationTXT: UITextField!
    @IBOutlet weak var dropoffDateLBL: UILabel!
    @IBOutlet weak var dropoffDateTXT: UITextField!
    @IBOutlet weak var dropoffTimeLBL: UILabel!
    @IBOutlet weak var dropoffTimeTXT: UITextField!
    
    @IBOutlet weak var driverDetailsView: UIView!
    @IBOutlet weak var driverDetailsTItleLBL: UILabel!
    
    
    @IBOutlet weak var driverTitleView: UIView!
    @IBOutlet weak var driverTitleLBL: UILabel!
    @IBOutlet weak var driverTitle: UILabel!
    @IBOutlet weak var dropDownImageview: UIImageView!
    
    var ExtraserviceArray = [ExtraServiceStruct]()
    
    
    @IBOutlet weak var driverTitleDropdownButton: UIButton!
    @IBAction func tapOnDriverTitle(_ sender: UIButton) {
        
        if selectTitleView.isHidden{
            animateTitle(toggle: true)
            
        }
        else {
            animateTitle(toggle: false)
            
        }
        
    }
    
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var firstNameLBL: UILabel!
    @IBOutlet weak var firstName: UITextField!
    
    
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var lastNameLBL: UILabel!
    @IBOutlet weak var lastName: UITextField!
    
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var email: UITextField!
    
    
    @IBOutlet weak var phoneNoView: UIView!
    @IBOutlet weak var phoneNoLBL: UILabel!
    @IBOutlet weak var phoneNo: UITextField!
    
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var comment: UITextField!
    
    @IBOutlet weak var extraServicesView: UIView!
    @IBOutlet weak var extraServicesTitleLBL: UILabel!
    @IBOutlet weak var extraServicesTableview: UITableView!
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var finalPriceTitleLBL: UILabel!
    @IBOutlet weak var finalPrice: UILabel!
    @IBOutlet weak var deposit: UILabel!
    
    
    @IBOutlet weak var confirmRequestButton: UIButton!
    @IBAction func tapOnConfirmRequest(_ sender: UIButton) {
        if self.pickupLocationTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Pick up location can not be blank")
        }
        else if self.pickupTimeTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Pick up time can not be blank")
        }
        else if self.pickupDateTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Pick up date can not be blank")
        }
        else if self.dropoffLocationTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Drop off location can not be blank")
        }
        else if self.dropoffDateTXT.text?.isEmpty == true
        {
           self.ShowAlertMessage(title: "Warning", message: "Drop off date can not be blank")
        }
        else if self.dropoffTimeTXT.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Drop off time can not be blank")
        }
        else
        {
           self.confirmRequest()
        }
    }
    
    @IBOutlet weak var selectTitleView: UIView!
    @IBOutlet weak var selectTitleTableview: UITableView!
    
    @IBOutlet weak var viewOfPickerView: UIView!
    @IBOutlet weak var selectServiceNumberPickerview: UIPickerView!
    
    @IBOutlet weak var ExtraServiceStackView: UIStackView!
    @IBOutlet weak var ExtraServiceTblHeight: NSLayoutConstraint!
    
    
    private var datepickerWithDate : UIDatePicker?
    private var datepickerWithTime : UIDatePicker?
    
    var localTitleArray = ["Mr.","Mrs."]
    
    var extraServicesArray : NSArray!
    
    var checkBoxArray = [UIImageView]()
    
    var serviceNumberLBLArray = [UILabel]()
    
    var dropdownImageviewArray = [UIImageView]()
    
    var openPickerArray = [UIButton]()
    
    var cellClickedArray = [Bool]()
    
    var tapGesture = UITapGestureRecognizer()
    
    var pickerviewArray = ["1","2","3","4","5"]
    
    var buttonTag : Int!
    
    var finalPriceString : String!
    
    var depositString : String!
    
    var finalPriceAmount : Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("extraservicearray",self.extraServicesArray)
        self.setBasicDetails()
        self.setTextDelegate()
        
        
        self.pickupDateTXT.delegate = self
        self.pickupTimeTXT.delegate = self
        self.dropoffDateTXT.delegate = self
        self.dropoffTimeTXT.delegate = self
        
        self.selectTitleTableview.delegate = self
        self.selectTitleTableview.dataSource = self
        
//        for _ in 0..<extraServicesArray.count
//        {
//            cellClickedArray.append(false)
//        }
//
//
        self.extraServicesTableview.delegate = self
        self.extraServicesTableview.dataSource = self
        self.extraServicesTableview.reloadData()
        
        
        
        
        self.selectServiceNumberPickerview.delegate = self
        self.selectServiceNumberPickerview.dataSource = self
        
        
        self.setFont()
        
        self.setDatepickerWithDate()
        
        self.setDatepickerWithTime()
        
//        self.set_scroll()
        
        self.autoFillDriverDeatils()

        self.StringLocalization()
        
//        pickupView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        dropoffView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        driverDetailsView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        extraServicesView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//
//        priceView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        //confirmRequestButton.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        confirmRequestButton.layer.cornerRadius = confirmRequestButton.frame.size.height / 2
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
//        self.finalPrice.text = self.finalPriceString
        
//        self.deposit.text = "Deposit : " + depositString + " to pay in destination"
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.extraServicesTableview.estimatedRowHeight = 100
        self.extraServicesTableview.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
     self.SetTableheight(table: self.extraServicesTableview, heightConstraint: self.ExtraServiceTblHeight)
    }
    
    
    //MARK:- //Set Basic Details From Previous Search Page
    func setBasicDetails()
    {
        self.pickupLocationTXT.text = dailyRentalSearchParameters.shared.PickupLocation
        
        self.dropoffLocationTXT.text = dailyRentalSearchParameters.shared.dropOffLocation
        
        self.pickupTimeTXT.text = dailyRentalSearchParameters.shared.pickuptime
        
        self.dropoffTimeTXT.text = dailyRentalSearchParameters.shared.dropofftime
        
        self.pickupDateTXT.text = dailyRentalSearchParameters.shared.pickupdate
        
        self.dropoffDateTXT.text = dailyRentalSearchParameters.shared.dropoffdate
    }
    
    //MARK:- //Set Textfields Delegate methods
    func setTextDelegate()
    {
        self.pickupLocationTXT.delegate = self
        self.dropoffLocationTXT.delegate = self
        self.pickupDateTXT.delegate = self
        self.dropoffDateTXT.delegate = self
        self.pickupTimeTXT.delegate = self
        self.dropoffTimeTXT.delegate = self
    }
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set up Datepicker with Date
    
    func setDatepickerWithDate()
    {
        datepickerWithDate = UIDatePicker()
        datepickerWithDate?.datePickerMode = .date
        pickupDateTXT.inputView = datepickerWithDate
        dropoffDateTXT.inputView = datepickerWithDate
    }
    
    // MARK:- // Set up Datepicker with Time
    
    func setDatepickerWithTime()
    {
        datepickerWithTime = UIDatePicker()
        datepickerWithTime?.datePickerMode = .time
        pickupTimeTXT.inputView = datepickerWithTime
        dropoffTimeTXT.inputView = datepickerWithTime
        
        
        // 24 hour format
        datepickerWithTime?.locale = NSLocale(localeIdentifier: "en_GB") as Locale
    }
    
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Textfield Delegates
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let dateFormat = DateFormatter()
        
        if textField == pickupDateTXT
        {
            dateFormat.dateFormat = "dd-MM-yyyy"
            self.pickupDateTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
        }
        else if textField == dropoffDateTXT
        {
            dateFormat.dateFormat = "dd-MM-yyyy"
            self.dropoffDateTXT.text = dateFormat.string(from: (self.datepickerWithDate?.date)!)
        }
        else if textField == pickupTimeTXT
        {
            dateFormat.dateFormat = "HH:mm"
            self.pickupTimeTXT.text = dateFormat.string(from: (self.datepickerWithTime?.date)!)
        }
        else
        {
            dateFormat.dateFormat = "HH:mm"
            self.dropoffTimeTXT.text = dateFormat.string(from: (self.datepickerWithTime?.date)!)
        }
        
    }
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == selectTitleTableview
        {
            return localTitleArray.count
        }
        else
        {
            return self.ExtraserviceArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == selectTitleTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")
            
            cell?.textLabel?.text = "\((localTitleArray[indexPath.row]))"

            cell?.selectionStyle = .none
            
            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! DRBookingExtraServicesTVCTableViewCell
            
            cell.serviceName.text = "\(self.ExtraserviceArray[indexPath.row].ServiceName ?? "")"
            cell.servicePrice.text = "\(self.ExtraserviceArray[indexPath.row].ServicePrice ?? "")"
            cell.serviceNumber.text = "\(self.ExtraserviceArray[indexPath.row].Quantity ?? "")"
            
            cell.checkboxImageview.image = (self.ExtraserviceArray[indexPath.row].ImageCheck)
            
//            cell.checkboxImageview.image = UIImage(named: "\(self.ExtraserviceArray[indexPath.row].ImageCheck ?? "")")
            
            
//            self.checkBoxArray.append(cell.checkboxImageview)
//            self.serviceNumberLBLArray.append(cell.serviceNumber)
//            self.dropdownImageviewArray.append(cell.dropdownImageview)
//            self.openPickerArray.append(cell.selectServiceNumber)
//
//            cell.serviceName.text = "\((self.extraServicesArray[indexPath.row] as! NSDictionary)["service_name"]!)"
//
//            cell.servicePrice.text = "\((self.extraServicesArray[indexPath.row] as! NSDictionary)["currency"]!)" + " " + "\((self.extraServicesArray[indexPath.row] as! NSDictionary)["price_value"]!)" + " /Day"
//
//            cell.selectServiceNumber.tag = indexPath.row
//
//            cell.selectServiceNumber.addTarget(self, action: #selector(DailyRentalBookingViewController.selectServiceNumber(sender:)), for: .touchUpInside)
//
//            if cellClickedArray[indexPath.row] == false
//            {
//                cell.checkboxImageview.image = UIImage(named: "checkBoxEmpty")
//                cell.serviceNumber.textColor = UIColor.darkGray
//                cell.dropdownImageview.image = UIImage(named: "Path Copy")
//                cell.selectServiceNumber.isUserInteractionEnabled = false
//            }
//            else
//            {
//                cell.checkboxImageview.image = UIImage(named: "checkBoxFilled")
//                cell.serviceNumber.textColor = UIColor.black
//                cell.dropdownImageview.image = UIImage(named: "Triangle")
//                cell.selectServiceNumber.isUserInteractionEnabled = true
//            }
//
            cell.selectionStyle = .none
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if tableView == selectTitleTableview
        {
            return (30/568)*self.FullHeight
        }
        else
        {
           return UITableView.automaticDimension
        }


    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == selectTitleTableview
        {
            self.driverTitle.text = "\(localTitleArray[indexPath.row])"
            
            self.driverTitle.textColor = UIColor.black
            
            self.animateTitle(toggle: false)
        }
        else
        {
//            cellClickedArray[indexPath.row] = !cellClickedArray[indexPath.row]
//            self.extraServicesTableview.reloadData()
//
//            self.calculateFinalPrice()
        }
        
        
    }
    
    
    // MARK:- // Pickerview Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerviewArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerviewArray[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        serviceNumberLBLArray[buttonTag].text = pickerviewArray[row]
        self.extraServicesTableview.reloadData()
        
        self.calculateFinalPrice()
        
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.viewOfPickerView)
            
            self.viewOfPickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.viewOfPickerView.frame.size.width, height: self.viewOfPickerView.frame.size.height)
            
        }
        
    }
    
    
    
    // MARK: - // Animate Title DropDown Tableview
    
    
    func animateTitle(toggle: Bool) {
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                
                self.view.bringSubviewToFront(self.selectTitleView)
                self.selectTitleView.autoresizesSubviews = false
                self.selectTitleView.isHidden = false
                self.SelectTitletableviewHeightConstraint.constant = ((60/568)*self.FullHeight)
//                self.selectTitleView.frame.size.height = ((60/568)*self.FullHeight)
//                self.selectTitleTableview.frame.size.height = ((60/568)*self.FullHeight)
                self.selectTitleView.autoresizesSubviews = true
                
                self.selectTitleView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.selectTitleView.autoresizesSubviews = false
                self.selectTitleView.isHidden = true

                 self.SelectTitletableviewHeightConstraint.constant = 0
//                self.selectTitleView.frame.size.height = ((1/568)*self.FullHeight)
//                self.selectTitleTableview.frame.size.height = ((1/568)*self.FullHeight)
                self.selectTitleView.autoresizesSubviews = true
                self.view.sendSubviewToBack(self.selectTitleView)
            }
        }
        
    }
    
    
    
    // MARK:- // Set Scroll
    
    func set_scroll()
    {
        
        if extraServicesArray.count > 0
        {
            self.extraServicesView.isHidden = false

            
//            self.extraServicesView.autoresizesSubviews = false
//            self.extraServicesTableview.autoresizesSubviews = false
//
//            self.extraServicesTableview.frame.size.height = CGFloat(self.extraServicesArray.count) * (75/568)*self.FullHeight
//            self.extraServicesView.frame.size.height = self.extraServicesTableview.frame.origin.y + self.extraServicesTableview.frame.size.height
//
//            self.priceView.frame.origin.y = self.extraServicesView.frame.origin.y + self.extraServicesView.frame.size.height + (10/568)*self.FullHeight
//
//            self.confirmRequestButton.frame.origin.y = self.priceView.frame.origin.y + self.priceView.frame.size.height + (15/568)*self.FullHeight
//
//            self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.confirmRequestButton.frame.origin.y + self.confirmRequestButton.frame.size.height + (5/568)*self.FullHeight)
        }
        else
        {
            self.extraServicesView.isHidden = true
            
//            self.priceView.frame.origin.y = self.driverDetailsView.frame.origin.y + self.driverDetailsView.frame.size.height + (10/568)*self.FullHeight
//
//            self.confirmRequestButton.frame.origin.y = self.priceView.frame.origin.y + self.priceView.frame.size.height + (15/568)*self.FullHeight
//
//            self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.confirmRequestButton.frame.origin.y + self.confirmRequestButton.frame.size.height + (5/568)*self.FullHeight)
        }
        
        
    }
    
    
    // MARK:- // Select Service Number Button
    
    
    @objc func selectServiceNumber(sender: UIButton)
    {
        UIView.animate(withDuration: 0.2) {
            
            self.buttonTag = sender.tag
            
            self.view.bringSubviewToFront(self.viewOfPickerView)
            
            self.viewOfPickerView.frame = CGRect(x: 0, y: 0, width: self.viewOfPickerView.frame.size.width, height: self.viewOfPickerView.frame.size.height)
            
            // *** Hide viewOfPickerview when tapping outside ***
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            self.viewOfPickerView.addGestureRecognizer(self.tapGesture)
            
        }
    }
    
    // MARK: - Hide View of Pickerview when tapping outside
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.viewOfPickerView)
            
            self.viewOfPickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.viewOfPickerView.frame.size.width, height: self.viewOfPickerView.frame.size.height)
            
        }
    }
    
    
    // MARK:- //  Auto Fill Driver Details
    
    func autoFillDriverDeatils()
    {
        
        self.firstName.text = UserDefaults.standard.string(forKey: "FirstName")
        self.lastName.text = UserDefaults.standard.string(forKey: "LastName")
        self.email.text = UserDefaults.standard.string(forKey: "EmailID")
        self.phoneNo.text = UserDefaults.standard.string(forKey: "PhoneNo")
        
    }
    
    // MARK: - // Calculate Final Price
    
    func calculateFinalPrice()
    {
        
        self.finalPriceAmount = Double(finalPriceString)
        
        var amountToAdd : Double! = 0
        
        for i in 0..<extraServicesArray.count
        {
            if cellClickedArray[i] == true
            {
                let selectedServicePrice = Double("\((self.extraServicesArray[i] as! NSDictionary)["price_value"]!)")
                
                if let selectedServiceQuantity = Double(serviceNumberLBLArray[i].text!)
                {
                    amountToAdd = amountToAdd + (selectedServicePrice! * selectedServiceQuantity)
                }
            }
        }
        
        finalPriceAmount = finalPriceAmount + amountToAdd
        
        self.finalPrice.text = "\(finalPriceAmount!)"
        
//        if cellClickedArray[index] == true
//        {
//            let selectedServicePrice = Double("\((self.extraServicesArray[index] as! NSDictionary)["price_value"]!)")
//
//            if let selectedServiceQuantity = Double(serviceNumberLBLArray[index].text!)
//            {
//                if pickerDidSelectArray[index] == true
//                {
//                    finalPriceAmount = Double(finalPriceString)
//
//                    finalPriceAmount = finalPriceAmount + (selectedServicePrice! * selectedServiceQuantity)
//                }
//                else
//                {
//                    finalPriceAmount = finalPriceAmount + (selectedServicePrice! * selectedServiceQuantity)
//                }
//
//            }
//
//            self.finalPrice.text = "\(finalPriceAmount!)"
//        }
//        else
//        {
//
//        }
        
        
        /*
        if isClicked == true
        {
            let selectedServicePrice = Double("\((self.extraServicesArray[index] as! NSDictionary)["price_value"]!)")
            
            if let selectedServiceQuantity = Double(serviceNumberLBLArray[index].text!)
            {
                if pickerDidSelectArray[index] == true
                {
                    finalPriceAmount = Double(finalPriceString)
                    
                    finalPriceAmount = finalPriceAmount + (selectedServicePrice! * selectedServiceQuantity)
                }
                else
                {
                    finalPriceAmount = finalPriceAmount + (selectedServicePrice! * selectedServiceQuantity)
                }
                
            }
            
             self.finalPrice.text = "\(finalPriceAmount!)"
        }
        else
        {
            let deselectedServicePrice = Double("\((self.extraServicesArray[index] as! NSDictionary)["price_value"]!)")
            
            if let deselectedServiceQuantity = Double(serviceNumberLBLArray[index].text!)
            {
                finalPriceAmount = finalPriceAmount - (deselectedServicePrice! * deselectedServiceQuantity)
            }
            
            self.finalPrice.text = "\(finalPriceAmount!)"
        }
 */
        
    }
    
    
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        pickupLocationLBL.font = UIFont(name: pickupLocationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupLocationTXT.font = UIFont(name: (pickupLocationTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        pickupDateLBL.font = UIFont(name: pickupDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupDateTXT.font = UIFont(name: (pickupDateTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        pickupTimeLBL.font = UIFont(name: pickupTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        pickupTimeTXT.font = UIFont(name: (pickupTimeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        dropoffLocationLBL.font = UIFont(name: dropoffLocationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffLocationTXT.font = UIFont(name: (dropoffLocationTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        dropoffDateLBL.font = UIFont(name: dropoffDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffDateTXT.font = UIFont(name: (dropoffDateTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        dropoffTimeLBL.font = UIFont(name: dropoffTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        dropoffTimeTXT.font = UIFont(name: (dropoffTimeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        driverDetailsTItleLBL.font = UIFont(name: (driverDetailsTItleLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        driverTitleLBL.font = UIFont(name: (driverTitleLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        driverTitle.font = UIFont(name: (driverTitle.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        firstNameLBL.font = UIFont(name: (firstNameLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        firstName.font = UIFont(name: (firstName.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        lastNameLBL.font = UIFont(name: (lastNameLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        lastName.font = UIFont(name: (lastName.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        emailLBL.font = UIFont(name: (emailLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        email.font = UIFont(name: (email.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        phoneNoLBL.font = UIFont(name: (phoneNoLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        phoneNo.font = UIFont(name: (phoneNo.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        commentLBL.font = UIFont(name: (commentLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        comment.font = UIFont(name: (comment.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        extraServicesTitleLBL.font = UIFont(name: (extraServicesTitleLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        finalPriceTitleLBL.font = UIFont(name: (finalPriceTitleLBL.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        finalPrice.font = UIFont(name: (finalPrice.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        deposit.font = UIFont(name: (deposit.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        confirmRequestButton.titleLabel?.font = UIFont(name: (confirmRequestButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 16)))!
        
        
    }
    
    //MARK:- //String Localization
    
    func StringLocalization()
    {
      self.pickupLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupLocation", comment: "")
       self.pickupLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupLocation", comment: "")
      self.pickupDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupDate", comment: "")
        self.pickupDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupDate", comment: "")
        self.pickupTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupTime", comment: "")
        self.pickupTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupTime", comment: "")
        self.dropoffLocationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffLocation", comment: "")
        self.dropoffLocationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffLocation", comment: "")
        self.dropoffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffDate", comment: "")
        self.dropoffDateTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffDate", comment: "")
        self.dropoffTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffTime", comment: "")
        self.dropoffTimeTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffTime", comment: "")
        self.driverTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")
        self.firstNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "first_name", comment: "")
        self.firstName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "first_name", comment: "")
        self.lastNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_name", comment: "")
        self.lastName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "last_name", comment: "")
        self.emailLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "emailId", comment: "")
        self.email.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "emailId", comment: "")
        self.phoneNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNo", comment: "")
        self.phoneNo.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNo", comment: "")
        self.commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment1", comment: "")
        self.comment.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment1", comment: "")
        self.extraServicesTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraServices", comment: "")
        self.finalPriceTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "finalprice", comment: "")
        self.driverDetailsTItleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "driverdetails", comment: "")
        self.confirmRequestButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirmrequest", comment: ""), for: .normal)
        }
    
    //MARK:- //Confirm Button Request API fire
    
    func confirmRequest()
    {
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&driver_age=\(dailyRentalSearchParameters.shared.driverAge ?? "")&pick_up_date=\(dailyRentalSearchParameters.shared.pickupdate ?? "")&pick_up_time=\(dailyRentalSearchParameters.shared.pickuptime ?? "")&drop_off_date=\(dailyRentalSearchParameters.shared.dropoffdate ?? "")&drop_off_time=\(dailyRentalSearchParameters.shared.dropofftime ?? "")&pick_up_loc=\(dailyRentalSearchParameters.shared.PickupLocation ?? "")&drop_off_loc=\(dailyRentalSearchParameters.shared.dropOffLocation ?? "")&product_id=\(dailyRentalSearchParameters.shared.rentalProductID ?? "")&title=\(self.driverTitle.text ?? "")&first_name=\(self.firstName.text ?? "")&surname=\(self.lastName.text ?? "")&email=\(self.email.text ?? "")&phno=\(self.phoneNo.text ?? "")&comment=\(self.comment.text ?? "")"
        self.CallAPI(urlString: "app_daily_rental_booking_request", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            DispatchQueue.main.async {
              SVProgressHUD.dismiss()
            }
            
            if "\(self.globalJson["response"] as! Bool)".elementsEqual("true")
            {
                self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["message"] ?? "")")
            }
            
        })
    }
    
    //MARK:- //TextField DidendEditing delegate method
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == self.pickupLocationTXT
        {
            dailyRentalSearchParameters.shared.setPickupLocation(pickuplocation: self.pickupLocationTXT.text ?? "")
        }
        else if textField == self.pickupDateTXT
        {
            
        }
        else if textField == self.pickupTimeTXT
        {
            
        }
        else if textField == self.dropoffLocationTXT
        {
            dailyRentalSearchParameters.shared.setdropofflocation(DropOffLocation: self.dropoffLocationTXT.text ?? "")
        }
        else if textField == self.dropoffDateTXT
        {
            
        }
        else if textField == self.dropoffTimeTXT
        {
            
        }
        else
        {
            //do nothing
        }
    }
    
    // MARK:- // Set Table Height
    
    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        var setheight: CGFloat  = 0
        table.frame.size.height = 3000
        
        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
    }
    
}
