//
//  DRBookingExtraServicesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 28/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class DRBookingExtraServicesTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var checkboxImageview: UIImageView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceNumber: UILabel!
    @IBOutlet weak var dropdownImageview: UIImageView!
    @IBOutlet weak var selectServiceNumber: UIButton!
    @IBOutlet weak var separatorView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
