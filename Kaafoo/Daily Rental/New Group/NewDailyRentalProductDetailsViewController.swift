//
//  NewDailyRentalProductDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/15/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import GoogleMaps
import GooglePlaces


class NewDailyRentalProductDetailsViewController:GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    //MARK:- // Variable Declaration
    
    
    var sellerid : String!
    var tempFINALprice : String! = ""
    
    var QuantityLabelArray = [UILabel]()
    
    var SellerDetails : NSDictionary!
    
    @IBOutlet weak var followbtn: UIButton!
    
    @IBOutlet weak var unfollowbtn: UIButton!
     
    @IBOutlet weak var accounttypeLBL: UILabel!
    
    @IBOutlet weak var ShowPickerView: UIView!
    @IBOutlet weak var HidePickerOutlet: UIButton!
    @IBAction func HidePickerBtn(_ sender: UIButton) {
        self.ShowPickerView.isHidden = true
    }
    @IBOutlet weak var DataPicker: UIPickerView!
    
    //MARK:- Set BreadCrump Variables
    @IBOutlet weak var BreadCrump: UILabel!
    @IBAction func BreadCrumpBtn(_ sender: UIButton) {
    }
    @IBOutlet weak var BreadCrumpOutlet: UIButton!
    @IBOutlet weak var BreadCrumpImageView: UIImageView!
    
    //MARK:- //Deposit View Outlets
    @IBOutlet weak var DepositLbl: UILabel!
    @IBOutlet weak var DepositAmount: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    
    @IBOutlet weak var SellerImage: UIImageView!
    @IBOutlet weak var SellerName: UILabel!
    @IBOutlet weak var FeedbackPercentage: UILabel!
    @IBOutlet weak var RatingNumber: UILabel!
    @IBOutlet weak var LocationAddress: UILabel!
    @IBOutlet weak var ContactNumber: UILabel!
    
    
    @IBOutlet weak var ReportView: UIView!
    @IBAction func HideReportViewBtn(_ sender: UIButton) {
        self.ReportView.isHidden = true
    }
    @IBOutlet weak var ReportTextView: UITextView!
    @IBAction func ReportSubmitBtn(_ sender: UIButton) {
        if self.tapReport == true
        {
            if self.ReportTextView.text.isEmpty == true
            {
                self.ShowAlertMessage(title: "Warning", message: "Report can not be blank")
            }
            else
            {
                self.ReportAPI()
                self.ReportView.isHidden = true
            }
        }
        else
        {
            if self.ReportTextView.text.isEmpty == true
            {
                self.ShowAlertMessage(title: "Warning", message: "Send Message can not be blank")
            }
            else
            {
                self.ReportAPI()
                self.ReportView.isHidden = true
            }
        }
        
    }
   
    @IBOutlet weak var ReportOutlet: UIButton!
    @IBAction func hideBtn(_ sender: UIButton) {
        self.ReportView.isHidden = true
    }
    
    @IBAction func SendMessageBtn(_ sender: UIButton) {
        let userID = UserDefaults.standard.string(forKey: "userID")
        if userID!.elementsEqual("\(self.tempDict["seller_id"]!)")
        {
            self.ShowAlertMessage(title: "Warning", message: "You can not send Message to Your own.")
        }
        else
        {
           self.ReportView.isHidden = false
           self.tapReport = false
        }
        
    }
    @IBAction func ViewAccountBtn(_ sender: UIButton) {
        if "\(self.tempDict["product_usertype"] ?? "")".elementsEqual("B") {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            
            navigate.sellerID = "\(self.tempDict["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            
            navigate.sellerID = "\(self.tempDict["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    
    @IBOutlet weak var SendMessageOutlet: UIButton!
    @IBOutlet weak var ViewAccountOutlet: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    var finalPrice : Double = 0.00
    
    
    @IBAction func ProductBookingButton(_ sender: UIButton) {
        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalBookingVC") as! DailyRentalBookingViewController
        
        navigate.ExtraserviceArray = self.ExtraServiceStructArray
//        navigate.finalPrice.text = "\(self.tempFINALprice ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    @IBOutlet weak var typeAccountPicture: UIImageView!
    
    
    
    var dataArray = ["1","2","3","4","5","6","7","8","9","10"]

    @IBOutlet weak var FeaturestableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var FreeServicesTableViewHeightConstraint: NSLayoutConstraint!
    
    var ExtraServiceStructArray = [ExtraServiceStruct]()
    
    var ImageViewArray = [UIImageView]()
    
    var ButtonIndex : Int! = 0
    
    var tapReport : Bool! = true
    
    var marker = GMSMarker()
    
    var langName : String!
    
    
    @IBOutlet weak var InformationTableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PaymentTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var FeaturesStackView: UIStackView!
    @IBOutlet weak var InformationStackView: UIStackView!
    @IBOutlet weak var InformationTableView: UITableView!
    
    
    @IBOutlet weak var FreeServicesStackView: UIStackView!
    @IBOutlet weak var FreeServicesTableView: UITableView!
    
    
    @IBOutlet weak var PaymentStackView: UIStackView!
    @IBOutlet weak var PaymentTableView: UITableView!
    
    
    @IBOutlet weak var ExtraServicesStackView: UIStackView!
    
    
    @IBOutlet weak var WatchlistView: UIView!
    @IBOutlet weak var PrdocutImagesCollectionView: UICollectionView!
    @IBOutlet weak var ControlReportView: UIView!
    @IBOutlet weak var ProductNameView: UIView!
    @IBOutlet weak var PickUpView: UIView!
    @IBOutlet weak var DropOffView: UIView!
    @IBOutlet weak var DescriptionHeaderView: UIView!
    @IBOutlet weak var DescriptionView: UIView!
    @IBOutlet weak var FeaturesHeaderView: UIView!
    @IBOutlet weak var FeaturestableView: UITableView!
    @IBOutlet weak var DriverRequirementHeaderView: UIView!
    @IBOutlet weak var DriverRequirementView: UIView!
    @IBOutlet weak var FollowingFeaturesHeaderView: UIView!
    @IBOutlet weak var ExtraServicesHeaderView: UIView!
    @IBOutlet weak var ExtraServicestableView: UITableView!
    @IBOutlet weak var ExtrasSevicestableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var LocationTimetableHeaderView: UIView!
    @IBOutlet weak var timeTableview: UITableView!
    @IBOutlet weak var timeTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var InformationHeaderView: UIView!
    @IBOutlet weak var InformationView: UIView!
    @IBOutlet weak var PaymentOptionsHeaderView: UIView!
    @IBOutlet weak var PaymentOptionsView: UIView!
    @IBOutlet weak var BrowseCategoryHeaderView: UIView!
    @IBOutlet weak var BreadCrumbView: UIView!
    @IBOutlet weak var PriceDetailsView: UIView!
    @IBOutlet weak var BookBtnOutlet: UIButton!

    
    @IBOutlet weak var minimumageName: UILabel!
    @IBOutlet weak var licenseName: UILabel!
    @IBOutlet weak var minimumexperienceName: UILabel!
    

    var DailyRentalProductID : String! = ""
    var InfoArray : NSMutableArray! = NSMutableArray()
    var tempDict : NSDictionary! = NSDictionary()
    var ProductImagesArray : NSMutableArray! = NSMutableArray()
    var ExtraServiceArray : NSMutableArray! = NSMutableArray()
    var LocationTimetableArray : NSMutableArray! = NSMutableArray()
    var infOaRRAy : NSMutableArray! = NSMutableArray()
    var ServiceDataArray : NSMutableArray! = NSMutableArray()
    var EngineEmissionValue : NSMutableArray! = NSMutableArray()
    var PaymentArray : NSMutableArray! = NSMutableArray()
    var FreeFeatureArray : NSMutableArray! = NSMutableArray()
    var SellerInfo : NSMutableArray! = NSMutableArray()

    @IBOutlet weak var DescriptionString: UILabel!
    @IBOutlet weak var ImagePAgeControl: UIPageControl!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var productAddress: UILabel!
    
    @IBOutlet weak var reportBtnOutlet: UIButton!
    
    @IBAction func reportBtn(_ sender: UIButton) {
        self.ReportView.isHidden = false
    }
    @IBOutlet weak var pickUpLbl: UILabel!
    @IBOutlet weak var pikcupDetail: UILabel!
    @IBOutlet weak var pickupLocation: UILabel!

    @IBOutlet weak var dropoffLbl: UILabel!
    @IBOutlet weak var dropoffDetail: UILabel!
    @IBOutlet weak var dropoffLocation: UILabel!

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var featureLbl: UILabel!
    @IBOutlet weak var driverRequirementLbl: UILabel!


    @IBOutlet weak var minimumAge: UILabel!
    @IBOutlet weak var license: UILabel!
    @IBOutlet weak var minimumExperience: UILabel!

    @IBOutlet weak var followingFeatureLbl: UILabel!
    @IBOutlet weak var extraservicesLbl: UILabel!
    @IBOutlet weak var locationTimetableLbl: UILabel!
    @IBOutlet weak var informationLbl: UILabel!
    @IBOutlet weak var paymentOptionLbl: UILabel!
    @IBOutlet weak var browseCategoryLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var postedbyLbl: UILabel!

    @IBOutlet weak var postedByHeaderView: UIView!
    @IBOutlet weak var PostedByView: UIView!
    @IBOutlet weak var LocationHeaderView: UIView!
    @IBOutlet weak var PostedStackView: UIStackView!


    var customView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight))
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        
         self.followbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Follow", comment: ""), for: .normal)
        
         self.unfollowbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Unfollow", comment: ""), for: .normal)
        

        
        
        
        if UserDefaults.standard.string(forKey: "langID") != nil
        {
            langName = UserDefaults.standard.string(forKey: "langID")
        }
        else
        {
            UserDefaults.standard.set("EN", forKey: "langID")
            langName = UserDefaults.standard.string(forKey: "langID")
        }

        self.ShowPickerView.isHidden = true
        self.DataPicker.delegate = self
        self.DataPicker.dataSource = self
        self.DataPicker.reloadAllComponents()
        
        self.SetLayer()
        self.SetLanguage()
        self.getDetails()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func followbtnclicked(_ sender: Any)
    {
        
//        followbtn.isHidden = true
//        unfollowbtn.isHidden = false
//
       
                
                
                self.globalDispatchgroup.enter()
                
                DispatchQueue.main.async {
                    SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
                }
                
                let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url
                
                print("Place Bid URL : ", url)
                
                var parameters : String = ""
                let userID = UserDefaults.standard.string(forKey: "userID")
                var langID = UserDefaults.standard.string(forKey: "langID")
                let SellerID = sellerid
                
                parameters = "user_id=\(userID!)&buss_seller_id=\(SellerID!)&lang_id=\(langID!)"
                
                print("Parameters are : " , parameters)
                
                let session = URLSession.shared
                
                var request = URLRequest(url: url)
                request.httpMethod = "POST" //set http method as POST
                
                do {
                    request.httpBody = parameters.data(using: String.Encoding.utf8)
                    
                    
                }
                
                let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                    
                    guard error == nil else {
                        return
                    }
                    
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                            print("Place Bid Api Fire" , json)
                            
                            DispatchQueue.main.async {
                                
                                self.globalDispatchgroup.leave()
                                
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                                self.customView.removeFromSuperview()
                                
                            }
                            
                            self.globalDispatchgroup.notify(queue: .main, execute:
                                {
                                    if "\(json["response"]! as! Bool)".elementsEqual("true")
                                    {
                                        self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                        
                                        self.followbtn.isHidden = true
                                        self.unfollowbtn.isHidden = false
                                        
                                    }
                                    else
                                    {
                                        self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                        
        //                                if(self.followunfollowButton == true)
        //                                {
        //                                    self.FollowBtnOutlet.isHidden = true
        //                                    self.unfollowbtnoutlet.isHidden = false
        //
        //
        //                                }
        //                                else
        //                                {
        //                                    self.FollowBtnOutlet.isHidden = false
        //                                    self.unfollowbtnoutlet.isHidden = true
        //                                }
        //
        //                                 self.followunfollowButton = false
                                    }
                            })
                            
                        }
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                })
                
                task.resume()
            
    }
    
    @IBAction func unfollowbtnclicked(_ sender: Any)
    {
        
//        followbtn.isHidden = false
//        unfollowbtn.isHidden = true
        
            self.globalDispatchgroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
            
            let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url
            
            print("Place Bid URL : ", url)
            
            var parameters : String = ""
           
            
            let userID = UserDefaults.standard.string(forKey: "userID")
            var langID = UserDefaults.standard.string(forKey: "langID")
            let SellerID = sellerid
                           
            parameters = "user_id=\(userID!)&buss_seller_id=\(SellerID!)&lang_id=\(langID!)"
            
            print("Parameters are : " , parameters)
            
            let session = URLSession.shared
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)
                
                
            }
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        print("Place Bid Api Fire" , json)
                        
                        DispatchQueue.main.async {
                            
                            self.globalDispatchgroup.leave()
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                            
                        }
                        
                        self.globalDispatchgroup.notify(queue: .main, execute:
                            {
                                if "\(json["response"]! as! Bool)".elementsEqual("true")
                                {
                                    self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                    self.followbtn.isHidden = false
                                    self.unfollowbtn.isHidden = true
                                }
                                else
                                {
                                    self.ShowAlertMessage(title: "Warning", message: "\(json["message"]!)")
                                    
    //                                if(self.followunfollowButton == false)
    //                                {
    //                                    self.FollowBtnOutlet.isHidden = false
    //                                    self.unfollowbtnoutlet.isHidden = true
    //
    //
    //                                }
    //                                else
    //                                {
    //                                    self.FollowBtnOutlet.isHidden = true
    //                                    self.unfollowbtnoutlet.isHidden = false
    //                                }
    //
    //                                 self.followunfollowButton = true
                                }
                        })
                        
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            
            task.resume()
        }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
//        self.view.isHidden = true
    }
    
    
    //MARK:- //Create ExtraService Struct Array
    func CreateExtraServiceStructArray()
    {
        for i in 0..<self.ExtraServiceArray.count
        {
            self.ExtraServiceStructArray.append(ExtraServiceStruct(ServiceName: "\((self.ExtraServiceArray[i] as! NSDictionary)["service_name"] ?? "")", ServicePrice: ("\((self.ExtraServiceArray[i] as! NSDictionary)["currency"] ?? "")" + "\((self.ExtraServiceArray[i] as! NSDictionary)["price_value"] ?? "")"), ServiceOnlyPrice: "\((self.ExtraServiceArray[i] as! NSDictionary)["price_value"] ?? "")" , Quantity: "1", SelectService: false, ImageCheck: UIImage(named: "checkBoxEmpty")!))
        }
    }


    func SetData()
    {
        self.ImagePAgeControl.numberOfPages = self.ProductImagesArray.count
        self.ProductName.text = "\(self.tempDict["product_name"]!)"
        self.productAddress.text = "\(self.tempDict["product_location"]!)"

        self.minimumageName.text = "\(self.tempDict["driver_age"]!)"
        self.licenseName.text = "\(self.tempDict["license"]!)"
        self.minimumexperienceName.text = "\(self.tempDict["min_experience"]!)"

        self.DescriptionString.text = "\(self.tempDict["min_experience"]!)"
        
        self.DepositAmount.text = "\(self.tempDict["currency"]!)" + "\(self.tempDict["security_amount_value"]!)"
        
        self.totalAmount.text = "\(self.tempDict["currency"]!)" + "\(self.tempDict["final_value"]!)"
        
        self.SellerName.text = "\(self.tempDict["seller_name"]!)"
        
        self.SellerImage.sd_setImage(with: URL(string: "\(self.tempDict["seller_image"]!)"))
        
        self.FeedbackPercentage.text = "\(self.tempDict["positive_feedback"]!)" + "% " + "Positive Feedback"
        
        self.RatingNumber.text = "\(self.tempDict["ratting"]!)"
        
        self.ContactNumber.text = "Contact : " + "\(self.tempDict["landline"]!)"
        
        self.LocationAddress.text = "Location : " + "\(self.tempDict["address_seller"]!)"
        
        self.showLocation(addresslat: "\(self.tempDict["user_lat"] ?? "")", addresslong: "\(self.tempDict["user_long"] ?? "")")
        
       
        
        
        if langName.elementsEqual("AR")
        {
            self.typeAccountPicture.image = UIImage(named: "button-2")
        }
        else
        {
            self.typeAccountPicture.image = UIImage(named: "button-1")
        }
        
        dailyRentalSearchParameters.shared.setRentalProductID(productID: "\(self.tempDict["product_id"] ?? "")")

        self.PrdocutImagesCollectionView.delegate = self
        self.PrdocutImagesCollectionView.dataSource = self
        self.PrdocutImagesCollectionView.reloadData()

        self.FeaturestableView.delegate = self
        self.FeaturestableView.dataSource = self
        self.FeaturestableView.reloadData()

        self.timeTableview.delegate = self
        self.timeTableview.dataSource = self
        self.timeTableview.reloadData()
        
        self.InformationTableView.delegate = self
        self.InformationTableView.dataSource = self
        self.InformationTableView.reloadData()
        
        self.PaymentTableView.delegate = self
        self.PaymentTableView.dataSource = self
        self.PaymentTableView.reloadData()
        
        self.ExtraServicestableView.delegate = self
        self.ExtraServicestableView.dataSource = self
        self.ExtraServicestableView.reloadData()
        
        self.FreeServicesTableView.delegate = self
        self.FreeServicesTableView.dataSource = self
        self.FreeServicesTableView.reloadData()

        self.SetTableheight(table: self.FeaturestableView, heightConstraint: self.FeaturestableViewHeightConstraint)
        self.SetTableheight(table: self.timeTableview, heightConstraint: self.timeTableViewHeightConstraint)
        self.SetTableheight(table: self.ExtraServicestableView, heightConstraint: self.ExtrasSevicestableViewHeightConstraint)
        self.SetTableheight(table: self.PaymentTableView, heightConstraint: self.PaymentTableViewHeightConstraint)
        self.SetTableheight(table: self.InformationTableView, heightConstraint: self.InformationTableviewHeightConstraint)
        self.SetTableheight(table: self.FreeServicesTableView, heightConstraint: self.FreeServicesTableViewHeightConstraint)
        
    }

    //MARK:- //Get Product Details From API
    func getDetails()
    {
        let parameters = "lang_id=\(langID!)&product_id=\(DailyRentalProductID!)"
        self.CallAPI(urlString: "app_daily_rental_details", param: parameters, completion: {

            self.InfoArray = self.globalJson["info_array"] as? NSMutableArray
            print("infoarray------",self.InfoArray)
            self.tempDict = self.InfoArray[0] as? NSDictionary
            self.ProductImagesArray = self.tempDict["product_image"] as? NSMutableArray
            print("ProductImageArray",self.ProductImagesArray!)
            self.ExtraServiceArray = self.tempDict["extra_services"] as? NSMutableArray
            self.LocationTimetableArray = self.tempDict["location_timetables"] as? NSMutableArray
            self.infOaRRAy = self.tempDict["info"] as? NSMutableArray
            self.ServiceDataArray = self.tempDict["service_data"] as? NSMutableArray
            self.EngineEmissionValue = self.tempDict["enginee_emission_val"] as? NSMutableArray
            self.PaymentArray = self.tempDict["payment_type"] as? NSMutableArray
            self.FreeFeatureArray = self.tempDict["free_feature"] as? NSMutableArray
            //self.SellerInfo = self.tempDict["seller_id"] as? NSMutableArray
            
             self.sellerid = "\(self.tempDict["seller_id"]!)"
            print("sellerid------",self.sellerid)
           
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                if   "\(self.tempDict["favourite_status"]!)".elementsEqual("0")
                {
                    self.unfollowbtn.isHidden = false
                    self.followbtn.isHidden = true
                }
                else
                {
                    self.unfollowbtn.isHidden = true
                    self.followbtn.isHidden = false
                }
                
                if "\(self.tempDict["product_usertype"]!)".elementsEqual("B")
                {
                  self.accounttypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_account", comment: "")
                }
                else
                {
                  self.accounttypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "private_account", comment: "")
                }
                
                self.CreateExtraServiceStructArray()
                self.SetData()
                self.setBreadcrump()
                SVProgressHUD.dismiss()
                self.customView.removeFromSuperview()
                
                self.view.isHidden = false

            }
        })
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ProductImagesArray.count
    }

    //MARK:- //CollectionView delegate and DataSource Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! dailyRentalProductImgCVC
//        let path = "\((self.ProductImagesArray[indexPath.row] as! NSDictionary)["image"]!)"
//        let url = URL(string: path)
//        let data = try? Data(contentsOf: url!)
//
//        //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//
//        cell.ShowImg.image = UIImage(data: data!)
        
        cell.ShowImg.sd_setImage(with: URL(string: "\((self.ProductImagesArray[indexPath.row] as! NSDictionary)["image"]!)"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let CollectionViewWidthSize = PrdocutImagesCollectionView.frame.size.width
        return CGSize(width: CollectionViewWidthSize, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }


    func SetLayer()
    {
        self.PickUpView.layer.borderWidth = 0.5
        self.PickUpView.layer.borderColor = UIColor.darkText.cgColor

        self.DropOffView.layer.borderWidth = 0.5
        self.DropOffView.layer.borderColor = UIColor.darkText.cgColor
    }

    //MARK:- UItableview datasource and delegate methods

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ExtraServicestableView
        {
            return self.ExtraServiceStructArray.count
        }
        else if tableView == timeTableview
        {
            return self.LocationTimetableArray.count
        }
        else if tableView == FeaturestableView
        {
            return self.EngineEmissionValue.count
        }
        else if tableView == InformationTableView
        {
            return self.infOaRRAy.count
        }
        else if tableView == PaymentTableView
        {
            return self.PaymentArray.count
        }
        else if tableView == FreeServicesTableView
        {
            return self.FreeFeatureArray.count
        }
        else
        {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == FeaturestableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "product") as! dailyRentalProductFeatureTVC
            cell.FeatureOption.text = "\((self.EngineEmissionValue[indexPath.row] as! NSDictionary)["title_value"]!)"
            cell.FeatureDetail.text = "\((self.EngineEmissionValue[indexPath.row] as! NSDictionary)["title_name"]!)"
            cell.selectionStyle = .none
            return cell
        }
        else if tableView == timeTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "location") as! dailyRentalLocationDetailTVC
            cell.dayDetails.text = "\((self.LocationTimetableArray[indexPath.row] as! NSDictionary)["day"]!)"
            cell.timeDetails.text = "\((self.LocationTimetableArray[indexPath.row] as! NSDictionary)["timing"]!)"
            cell.selectionStyle = .none
            return cell
        }
        else if tableView == ExtraServicestableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "extra") as! ExtraDailyRentalTVC
            
           self.ImageViewArray.append(cell.CheckImageView)
            
           self.QuantityLabelArray.append(cell.QuantityLbl)
            
           cell.extraServicesName.text = "\(self.ExtraServiceStructArray[indexPath.row].ServiceName ?? "")"
           cell.extraServicePrice.text = "\(self.ExtraServiceStructArray[indexPath.row].ServicePrice ?? "")"
           cell.CheckImageView.image = (self.ExtraServiceStructArray[indexPath.row].ImageCheck)
           cell.QuantityLbl.text = "\(self.ExtraServiceStructArray[indexPath.row].Quantity ?? "")"
           cell.SelectQuantityButton.layer.borderWidth = 0.5
           cell.SelectQuantityButton.layer.borderColor = UIColor.lightgray().cgColor
            
           cell.SelectQuantityButton.tag = indexPath.row
            
            cell.SelectQuantityButton.addTarget(self, action: #selector(SelectQuantity(sender:)), for: .touchUpInside)
            
           cell.selectionStyle = .none
            
            return cell
        }
        else if tableView == InformationTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "information") as! informationDetailsTVC
            cell.titleValue.text = "\((self.infOaRRAy[indexPath.row] as! NSDictionary)["title"]!)"
            cell.descriptionValue.text = "\((self.infOaRRAy[indexPath.row] as! NSDictionary)["description"]!)"
            cell.selectionStyle = .none
            return cell
        }
        else if tableView == PaymentTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "payment") as! PaymentTVC
            cell.PaymentName.text = "\((self.PaymentArray[indexPath.row] as! NSDictionary)["name"]!)"
            cell.selectionStyle = .none
            return cell
        }
        else if tableView == FreeServicesTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "free") as! FreeServicesTVC
            cell.ServiceName.text = "\((self.FreeFeatureArray[indexPath.row] as! NSDictionary)["features"] ?? "")"
            cell.ServiceName.font = UIFont(name: "Lato-Regular", size: 14)
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- //Total Price Calculation
    
    func totalCalculation()
    {
        
        var sum = 0.0
        
        for item in self.ExtraServiceStructArray {
            
            if item.ImageCheck == UIImage(named: "checkBoxEmpty")
            {
                
            }
            else
            {
               sum += Double("\(item.ServiceOnlyPrice! as NSString)")! * Double("\(item.Quantity! as NSString)")!
            }
            
        }
        
        let pricetotal = (sum + Double("\(self.tempDict["final_value"]! as! NSString)")!)
        
        self.totalAmount.text = "\(self.tempDict["currency"] ?? "")" + "\(pricetotal)"
        
        self.tempFINALprice = self.totalAmount.text ?? ""
    }


    //MARK:- //Tableview Did Select Method
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == ExtraServicestableView
        {
            let cell = self.ExtraServicestableView.cellForRow(at: indexPath) as! ExtraDailyRentalTVC
            
          if cell.CheckImageView.image == UIImage(named: "checkBoxEmpty")
          {
            cell.CheckImageView.image  = UIImage(named: "checkBoxFilled")
            self.ExtraServiceStructArray[indexPath.row].ImageCheck = UIImage(named: "checkBoxFilled")
          }
          else
          {
            cell.CheckImageView.image = UIImage(named: "checkBoxEmpty")
            self.ExtraServiceStructArray[indexPath.row].ImageCheck = UIImage(named: "checkBoxEmpty")
          }
          
            self.totalCalculation()
        }
        else
        {
            //do nothing
        }
    }
    // MARK:- // Set Table Height
    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        var setheight: CGFloat  = 0
        table.frame.size.height = 3000

        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
    }
    
    //MARK:- //Set BreadCrump Bar For Daily Rental product Details page
    
    func setBreadcrump()
    {
        let BreadCrumpString = "\(self.tempDict["breadcrumb_bar"]!)"
        
        let BRStr = BreadCrumpString.replacingOccurrences(of: ",", with: "/", options: .literal, range: nil) + "/"
        let BreadCrumbArray = BRStr.components(separatedBy: "/")
        //print("Array",BreadCrumbArray)
        
        let reversedNames : [String] = Array(BreadCrumbArray.reversed())
        
//        print("ReversedArray",reversedNames)
        
        if langID.elementsEqual("AR")
        {
            let str = reversedNames.joined(separator: "/")
//            print("String",str)
            self.BreadCrump.halfTextColorChange(fullText: ( "\(self.tempDict["product_name"]!)" + str), changeText: "\(self.tempDict["product_name"]!)", color: .black())
//            self.BreadCrumpBtnOutlet.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
        }
        else
        {
            self.BreadCrump.halfTextColorChange(fullText: (BRStr + "\(self.tempDict["product_name"]!)" ), changeText: "\(self.tempDict["product_name"]!)", color: .black())
        }
    }
    
    //MARK:- //Creating Objective C Method for Select Quantity Button
    @objc func SelectQuantity(sender : UIButton)
    {
        ButtonIndex = sender.tag
        
        if self.ExtraServiceStructArray[sender.tag].ImageCheck == UIImage(named: "checkBoxEmpty")
        {
//            print("Empty")
            self.ShowPickerView.isHidden = true
        }
        else
        {
//            print("Filled")
            self.ShowPickerView.isHidden = false
        }
    }

    //MARK:- //Set Language Localization
    func SetLanguage()
    {
      self.locationLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")
      self.postedbyLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")
      self.browseCategoryLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "browseCategory", comment: "")
      self.paymentOptionLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "payment_option", comment: "")
        self.informationLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "information", comment: "")
      self.locationTimetableLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "locationandtimetables", comment: "")
      self.extraservicesLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraServices", comment: "")
      self.followingFeatureLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "wegiveyouthefollowingforfree", comment: "")
      self.driverRequirementLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "driver_requirement", comment: "")
      self.featureLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "features", comment: "")
      self.descriptionLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
      self.dropoffLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "drop-off", comment: "")
      self.pickUpLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pick-up", comment: "")
      self.ViewAccountOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)
      self.SendMessageOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
    }
    
//    MARK:- ScrollView Delegate and Datasource Methods
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == PrdocutImagesCollectionView
        {
            let pageNumber = PrdocutImagesCollectionView.contentOffset.x / PrdocutImagesCollectionView.frame.size.width

            ImagePAgeControl.currentPage = Int(pageNumber)
        }
        else
        {
            //do nothing
        }
    }
    
    //MARK:- //PickerView Delegate and datasource Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.dataArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.dataArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let tempString = self.dataArray[row]
        //print("IndexpathNumber",ButtonIndex)
        self.ExtraServiceStructArray[ButtonIndex].Quantity = tempString
//        self.QuantityLabelArray[ButtonIndex].text = tempString
        self.ExtraServicestableView.reloadRows(at: [NSIndexPath(row: ButtonIndex, section: 0) as IndexPath] , with: UITableView.RowAnimation.fade)
        self.ShowPickerView.isHidden = true
        
        self.totalCalculation()
    }
    
    //MARK:- Total Price Calculation
    
//    func totalCalculation()
//    {
//        var finalPrice : Double = 0
//
//        for i in 0..<self.ExtraServiceStructArray.count
//        {
//            let tempPrice = Double("\(self.ExtraServiceStructArray[i].ServiceOnlyPrice! as NSString)")!
//
//            let Quantity = Double("\(self.ExtraServiceStructArray[i].Quantity! as NSString)")!
//
//            finalPrice += tempPrice * Quantity
//
//        }
//        print("totalFinalPrice",finalPrice)
//        self.totalAmount.text = String(finalPrice)
//    }
    
    //MARK:- //Report API Fire
    func ReportAPI()
    {
        if tapReport == true
        {
            let langID = UserDefaults.standard.string(forKey: "langID")
            let userID = UserDefaults.standard.string(forKey: "userID")
            let parameters = "user_id=\(userID!)&product_id=\(self.tempDict["product_id"] ?? "")&report_msg=\(self.ReportTextView.text ?? "")&lang_id=\(langID!)"
            self.CallAPI(urlString: "app_report_product", param: parameters, completion: {
                self.globalDispatchgroup.leave()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                })
            })
        }
        else
        {
            let userID = UserDefaults.standard.string(forKey: "userID")
            let parameters = "user_id=\(userID!)&seller_id=\(self.tempDict["seller_id"] ?? "")&message=\(self.ReportTextView.text ?? "")"
            self.CallAPI(urlString: "app_friend_send_message", param: parameters, completion: {
                self.globalDispatchgroup.leave()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                    self.ShowAlertMessage(title: "Alert", message: "Message sent Successfully")
                })
            })
        }
    }
    
    // MARK:- // Functions to show location
    
    func showLocation( addresslat : String , addresslong : String)
    {
        let camera = GMSCameraPosition.camera(withLatitude: Double(addresslat)!, longitude: Double(addresslong)!, zoom: 12.0)
        
        marker.position = CLLocationCoordinate2D(latitude: Double(addresslat)!, longitude: Double(addresslong)!)
        //        marker.title = "Selected Address"
        //        marker.snippet = "Description"
        marker.map = self.mapView
        
        self.mapView.animate(to: camera)
        
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
class dailyRentalProductImgCVC : UICollectionViewCell
{
    @IBOutlet weak var ShowImg: UIImageView!
}

class dailyRentalProductFeatureTVC : UITableViewCell
{
    @IBOutlet weak var FeatureOption: UILabel!
    @IBOutlet weak var FeatureDetail: UILabel!
    @IBOutlet weak var SeparatorView: UIView!
}
class ExtraDailyRentalTVC : UITableViewCell
{
    @IBOutlet weak var CheckImageView: UIImageView!
    @IBOutlet weak var extraServicesName: UILabel!
    @IBOutlet weak var extraServicePrice: UILabel!
    @IBOutlet weak var SelectQuantityButton: UIButton!
    @IBOutlet weak var QuantityLbl: UILabel!
}

class dailyRentalLocationDetailTVC : UITableViewCell
{
    @IBOutlet weak var dayDetails: UILabel!
    @IBOutlet weak var timeDetails: UILabel!
}

class informationDetailsTVC : UITableViewCell
{
    @IBOutlet weak var titleValue: UILabel!
    @IBOutlet weak var descriptionValue: UILabel!
}
class PaymentTVC : UITableViewCell
{
    @IBOutlet weak var PaymentName: UILabel!
    
}
class FreeServicesTVC : UITableViewCell
{
    @IBOutlet weak var ServiceName: UILabel!
}


struct ExtraServiceStruct {
    
    
    var ServiceName : String?
    var ServicePrice : String?
    var ServiceOnlyPrice : String?
    var Quantity  : String?
    var SelectService : Bool!
    var ImageCheck : UIImage!
    
    init(ServiceName : String , ServicePrice : String , ServiceOnlyPrice : String ,Quantity : String, SelectService : Bool , ImageCheck : UIImage ) {
        
        self.ServiceName = ServiceName
        self.ServicePrice = ServicePrice
        self.ServiceOnlyPrice = ServiceOnlyPrice
        self.Quantity = Quantity
        self.SelectService = SelectService
        self.ImageCheck = ImageCheck
    }
}
