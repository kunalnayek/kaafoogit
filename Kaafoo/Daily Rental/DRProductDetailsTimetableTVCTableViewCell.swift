//
//  DRProductDetailsTimetableTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 27/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class DRProductDetailsTimetableTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var day: UILabel!
    
    
    @IBOutlet weak var time: UILabel!
    
    
    @IBOutlet weak var separatorView: UIView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
