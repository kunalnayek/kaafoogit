//
//  ReviewsViewController.swift
//  Kaafoo
//
//  Created by admin on 26/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ReviewsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var reviewTypesView: UIView!
    @IBOutlet weak var reviewTableview: UITableView!
    
//    var reviewTypesArray = ["All","Selling","Buying"]

    var reviewTypesArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Selling", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Buying", comment: "")]
    
    var typeButtonArray = [UIButton]()
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var reviewsArray : NSArray!
    
    var reviewType : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createReviewTypes()
        
        setColor(tag: 0)
        
        reviewType = "ALL"
        
        getReviewsList()
        
    }
    
    
    
    
    // MARK:- // Review Types Button
    
    @IBOutlet weak var allReviewTypeButtonOutlet: UIButton!
    @IBOutlet weak var buyingReviewTypeButtonOutlet: UIButton!
    @IBOutlet weak var sellingReviewTypeButtonOutlet: UIButton!
    
    @IBAction func reviewTypeButtonsAction(_ sender: UIButton) {
        
        setColor(tag: sender.tag)
        
        if sender.tag == 0
        {
            reviewType = "ALL"
        }
        else if sender.tag == 1
        {
            reviewType = "SELL"
        }
        else
        {
            reviewType = "BUY"
        }
        
        self.startValue = 0
        self.recordsArray.removeAllObjects()
        
        getReviewsList()
        
    }
    
    
    
    
    // MARK:- // Create Revie Types
    
    func createReviewTypes()
    {
        
        self.typeButtonArray.append(allReviewTypeButtonOutlet)
        self.typeButtonArray.append(buyingReviewTypeButtonOutlet)
        self.typeButtonArray.append(sellingReviewTypeButtonOutlet)
        
        for i in  0..<self.typeButtonArray.count
        {
          
            typeButtonArray[i].setTitle("\(reviewTypesArray[i])", for: .normal)
            typeButtonArray[i].titleLabel?.font = UIFont(name: (typeButtonArray[i].titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
            
            typeButtonArray[i].tag = i
        
        }
        reviewTypesView.layer.borderWidth = 1
        reviewTypesView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
    }
    
    
    
    // MARK:- // Set Color of Selected Button
    
    func setColor(tag: NSInteger)
    {
        for i in 0..<typeButtonArray.count
        {
            typeButtonArray[i].setTitleColor(UIColor.black, for: .normal)
        }
        
        typeButtonArray[tag].setTitleColor(UIColor.red, for: .normal)
    }
    
    
    
    
    // MARK:- // Delegate Functions
    
    // Mark:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ReviewsTVCTableViewCell
        
        cell.name.text = "\((recordsArray[indexPath.row] as! NSDictionary)["name"]!)"
        cell.reviewDate.text = "\((recordsArray[indexPath.row] as! NSDictionary)["date"]!)"
        cell.comment.text = "\((recordsArray[indexPath.row] as! NSDictionary)["comment"]!)"
        cell.totalRating.text = "(" + "\((recordsArray[indexPath.row] as! NSDictionary)["total_rating"]!)" + "." + "00"
        cell.ratingNo.text = "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)"
      
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)".elementsEqual("1")
        {
            cell.ratingView.backgroundColor = UIColor(red: 246, green: 15, blue: 49)
        }
                       
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)".elementsEqual("2")
        {
                      
            cell.ratingView.backgroundColor = UIColor(red: 242, green: 144, blue: 0)
        }
                       
        else
        {
            cell.ratingView.backgroundColor = UIColor(red: 0, green: 119, blue: 0)
        }
                       
        
        
        // Set Font //////
        cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        cell.reviewDate.font = UIFont(name: cell.reviewDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.comment.font = UIFont(name: cell.comment.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.totalRating.font = UIFont(name: cell.totalRating.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.ratingNo.font = UIFont(name: cell.ratingNo.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        //
//        cell.ratingView.layer.cornerRadius = cell.ratingView.frame.size.height / 2
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return (93/568)*self.FullHeight
//
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    
    
    // MARK: - // JSON POST Method to list Reviews
    
    func getReviewsList()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_service_user_details_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let sellerID = UserDefaults.standard.string(forKey: "sellerID")
        
        parameters = "login_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&seller_id=\(sellerID!)&listing_type=R&type=\(reviewType!)"
        
        print("Reviews URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
       
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Reviews Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        self.reviewsArray = ((json["info_array"] as! NSDictionary)["review_section"] as! NSArray)
                        
                        for i in 0..<(self.reviewsArray.count)
                            
                        {
                            
                            let tempDict = self.reviewsArray[i] as! NSDictionary
                            
                            self.recordsArray.add(tempDict as! NSMutableDictionary)
                            
                            
                        }
                        
                        self.nextStart = "\(json["next_start"]!)"
                        
                        self.startValue = self.startValue + self.reviewsArray.count
                        
                        print("Next Start Value : " , self.startValue)
                        
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.reviewTableview.delegate = self
                            self.reviewTableview.dataSource = self
                            self.reviewTableview.reloadData()
                            self.reviewTableview.rowHeight = UITableView.automaticDimension
                            self.reviewTableview.estimatedRowHeight = UITableView.automaticDimension
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }

}
