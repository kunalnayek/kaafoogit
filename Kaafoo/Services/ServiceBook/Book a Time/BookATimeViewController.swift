//
//  BookATimeViewController.swift
//  Kaafoo
//
//  Created by admin on 23/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookATimeViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var bookATimeTableview: UITableView!
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var serviceArray : NSArray!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.bookATime()

        // Do any additional setup after loading the view.
    }
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BookATimeTVCTableViewCell
        
        cell.serviceImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["service_image"]!)"))
        
        cell.serviceName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        cell.serviceDescription.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_desc"]!)"
        
        cell.serviceReviews.text = "Reviews:  " + "\((recordsArray[indexPath.row] as! NSDictionary)["review_count"]!)"
        
        cell.servicePrice.text = "Per Hour:  " + "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["service_price"]!)"
        
        //SetFont
        
        cell.serviceName.font = UIFont(name: cell.serviceName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        cell.serviceDescription.font = UIFont(name: cell.serviceDescription.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        cell.serviceReviews.font = UIFont(name: cell.serviceReviews.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.servicePrice.font = UIFont(name: cell.servicePrice.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.readMore.titleLabel?.font = UIFont(name: (cell.readMore.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        
        
        ///////////
        
        cell.cellView.layer.borderWidth = 2
        
        cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        cell.readMore.layer.cornerRadius = cell.readMore.frame.size.height / 2

        cell.readMore.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "readMore", comment: ""), for: .normal)

        cell.readMore.tag = indexPath.row
        
        cell.readMore.addTarget(self, action: #selector(SetExpandable(sender:)), for: .touchUpInside)
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        //return (172/568)*self.FullHeight
        return UITableView.automaticDimension

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
//
//        navigate.productID = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "servicelisting") as! ServiceListingViewController
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                bookATime()
            }



        }
    }


    
    // MARK: - // JSON POST Method to Book a Time
    
    func bookATime()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_service_user_details_main")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let sellerID = UserDefaults.standard.string(forKey: "sellerID")
        
        let catID = UserDefaults.standard.string(forKey: "catID")
        
        print("sellerid",catID)
        

        parameters = "login_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)&seller_id=\(sellerID!)&listing_type=P&cat_id=\(catID ?? "")"

        print("Book a Time URL is : ",url)
        
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Book A Time Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {
                        self.serviceArray = ((json["info_array"] as! NSDictionary)["service_list"] as! NSArray)
                        
                        for i in 0..<(self.serviceArray.count)
                            
                        {
                            
                            let tempDict = self.serviceArray[i] as! NSDictionary
                            print("tempdict is : " , tempDict)
                            
                            self.recordsArray.add(tempDict as! NSMutableDictionary)
                            
                            
                        }
                        
//                        print("RecordsArray-----",self.recordsArray)
                        
                        self.nextStart = "\(json["next_start"]!)"
                        
                        self.startValue = self.startValue + self.serviceArray.count
                        
                        print("Next Start Value : " , self.startValue)


                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.bookATimeTableview.delegate = self
                            
                            self.bookATimeTableview.dataSource = self
                            
                            self.bookATimeTableview.reloadData()
                            
                            self.bookATimeTableview.rowHeight = UITableView.automaticDimension
                            
                            self.bookATimeTableview.estimatedRowHeight = UITableView.automaticDimension
                            
                            SVProgressHUD.dismiss()
                        }
                    }


                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }
    //MARK:- Objective C method For UITableviewCell
    @objc func SetExpandable(sender: UIButton)
    {
//        print("records array " , self.recordsArray)

//        let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
        
        obj.productID =   "\((recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
        
        self.navigationController?.pushViewController(obj, animated: true)
    }

    

}
