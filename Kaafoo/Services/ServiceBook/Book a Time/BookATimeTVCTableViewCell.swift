//
//  BookATimeTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 23/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class BookATimeTVCTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var serviceReviews: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    
    @IBOutlet weak var readMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
