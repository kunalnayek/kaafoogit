//
//  serviceBookingInformationViewController.swift
//  Kaafoo
//
//  Created by admin on 27/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos
import SVProgressHUD
import FSCalendar

class serviceBookingInformationViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,FSCalendarDelegate,FSCalendarDataSource {

    var monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    var SID : String! = ""
    
    var pickdate : String = ""
    
    var pickmonth : String = ""
    
    var pickweek : String = ""
    
    var fulldate : String = ""
    
    var testintcell : Int! = 0
    
    var testindex : NSNumber!
    
    var pickdaycell : String = ""
    
    var tapGesture = UITapGestureRecognizer()
    
    var setdateboolval : Bool! = false
    
    var dateMonthYearArray = NSArray()
    var arrDate = NSMutableArray()
    
    var dictdata = NSDictionary()
       
    @IBOutlet weak var setdateview: UIView!
    
    @IBOutlet weak var setdateday: UILabel!
    
    @IBOutlet weak var setdatemonth: UILabel!
    
    @IBOutlet weak var setdateweek: UILabel!
    
    
    @IBOutlet weak var ShowCalendarView: UIView!
    
    @IBOutlet weak var LocalToolbarView: UIView!
    
    @IBOutlet weak var OkBtnOutlet: UIButton!
    
    @IBOutlet weak var CancelBtnOutlet: UIButton!
    
    @IBOutlet weak var HideCalendarView: UIButton!
    
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var DatepickerView: UIView!
    
    @IBOutlet weak var datepicker: UIDatePicker!
    
    
    @IBOutlet weak var setdatebutton: UIButton!
    
    
    fileprivate let formatter: DateFormatter = {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MMM"
        
        return formatter
    }()
    
    fileprivate let dateFormatter : DateFormatter = {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter
    }()
    
    fileprivate let APIdateFormatter : DateFormatter = {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter
    }()
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    
    @IBOutlet weak var sellerInformationView: UIView!
    @IBOutlet weak var sellerImage: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviews: UILabel!
    
    @IBOutlet weak var addressAndPhoneNoView: UIView!
    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var midSeparatorView: UIView!
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressPlaceholderImage: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phoneNoView: UIView!
    @IBOutlet weak var phoneNoPLaceholderImage: UIImageView!
    @IBOutlet weak var phoneNo: UILabel!
    
    @IBOutlet weak var serviceInformationView: UIView!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var perHourLBL: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var serviceDescriptionSeparatorView: UIView!
    
    @IBOutlet weak var selectedTimeAndMessageView: UIView!
    
    @IBOutlet weak var dynamicHeightView: UIView!
    
    
    @IBOutlet weak var messageToView: UIView!
    
    @IBOutlet weak var selelctedTimeView: UIView!
    @IBOutlet weak var timeAndDateCollectionview: UICollectionView!
    @IBOutlet weak var bookingTimesTableview: UITableView!
    
    
    
    
    
    
    @IBOutlet weak var enterEmailView: UIView!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    
    
    @IBOutlet weak var enterPhoneNoView: UIView!
    @IBOutlet weak var phoneNoLBL: UILabel!
    @IBOutlet weak var phoneNoTXT: UITextField!
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLBL: UILabel!
    @IBOutlet weak var messageTXT: UITextField!
    
    
    @IBOutlet weak var cancelOrSaveBookingView: UIView!
    
    
    
    @IBOutlet weak var bookingTimeTableHeightConstraint: NSLayoutConstraint!
    
    
    
    
    let dispatchGroup = DispatchGroup()
    
    var bookingInformationDictionary : NSDictionary!
    
    var bookingInformationArray : NSArray!
    
    var bookingArray : NSMutableArray! = NSMutableArray()
    
    var productID : String!
    
    var testint : NSInteger! = 0
    
    var checkImageArray = [UIImageView]()
    
    var testImageArray = [UIImage]()
    
    let tableCell = BookingTimesTVCTableViewCell()
    
    var buttonClicked = "ST"
    
    var dateIndexpath : Int!
    
    var scrollSrartPoint : CGFloat!
    
    //var productID : String!
    var selectedSlot :String!
    var selectedDate : String!
    var selectedTime : String!
    
    
    var customView = UIView()
    // MARK:- // VIewdidload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight))
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.setFont()
        self.Set_Font()

         self.ShowCalendarView.isHidden = true
        
        setdateday.isHidden = true
        
        self.getServiceBookingInformation()
        
        self.dispatchGroup.notify(queue: .main) {
            
            self.fillData()
            
            self.setViewAccordingly()
            
        }
        
    }
    
    
    @IBAction func HideCalendarBtn(_ sender: Any) {
        
         self.ShowCalendarView.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bookingTimesTableview.estimatedRowHeight = 50
        self.bookingTimesTableview.rowHeight = UITableView.automaticDimension
        
    }
    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Selected Time Button
    
    @IBOutlet weak var selectedTimeButtonOutlet: UIButton!

    @IBAction func tapSelectedTime(_ sender: UIButton) {
        
        self.buttonClicked = "ST"
        
        self.setViewAccordingly()
        
    }
    
    
    // MARK:- // Message To Button
    
    @IBOutlet weak var messageButtonOutlet: UIButton!
    @IBAction func tapMessageButton(_ sender: UIButton) {
        
        self.buttonClicked = "MT"
        
        self.setViewAccordingly()
        
    }
    
    
    // MARK:- // Tap on Previous
    
    @IBOutlet weak var leftArrowOutlet: UIButton!
    @IBAction func tapOnLeftArrow(_ sender: UIButton) {
        
        setdateday.isHidden = true
        
        for i in 0..<checkImageArray.count
        {
            checkImageArray[i].image = UIImage(named: " ")
        }
        
        self.checkImageArray.removeAll()
        
        self.TapOnPrevious()
        
        self.bookingTimesTableview.reloadData()
        
        self.setViewAccordingly()
        
        //self.scrollToBottom()
        
    }
    
    
    //MARK:- // Tap on Next
    
    @IBOutlet weak var rightArrowOutlet: UIButton!
    @IBAction func tapOnRightArrow(_ sender: UIButton) {
        
        setdateday.isHidden = true
        
        for i in 0..<checkImageArray.count
        {
            checkImageArray[i].image = UIImage(named: " ")
        }
        
        self.checkImageArray.removeAll()
        
        TapOnNext()
        
        self.bookingTimesTableview.reloadData()
        
        self.setViewAccordingly()
        
    }
    
    @IBAction func setdatebtnclick(_ sender: Any) {
        
        
        setdateboolval = true
        
        //self.ShowCalendarView.isHidden = false
        self.DatepickerView.isHidden = false
        
        self.DatepickerView?.backgroundColor = UIColor(white: 1, alpha: 0.5)
        
        self.datepicker.backgroundColor = .white
        
        self.datepicker.addTarget(self, action: #selector(serviceBookingInformationViewController.datePickerValueChanged(_:)), for: .valueChanged)

        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
        self.DatepickerView.addGestureRecognizer(self.tapGesture)

        self.view.bringSubviewToFront(self.DatepickerView)

        
    }
    
    // MARK: - Hide datepickerView when tapping outside
       
       @objc func tapGestureHandler() {
           UIView.animate(withDuration: 0.5)
           {
               self.DatepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
               self.view.sendSubviewToBack(self.DatepickerView)
               self.DatepickerView.isHidden = true
           }
       }
    
    @IBAction func okbtnClicked(_ sender: Any) {
        
         setdateview.isHidden = true
      // setdateboolval = true
        self.DatepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
                      self.view.sendSubviewToBack(self.DatepickerView)
                      self.DatepickerView.isHidden = true
        
        setdateday.isHidden = false
        setdateday.text = pickdate
        setdatemonth.text = pickmonth + " " + pickweek
        setdatebutton.isHidden = false

        
        
        
        for i in 0..<checkImageArray.count
               {
                   checkImageArray[i].image = UIImage(named: " ")
               }
               
               self.checkImageArray.removeAll()
               
               self.setdatefromcalender()
               
               self.bookingTimesTableview.reloadData()
               
               self.setViewAccordingly()
        
        
      
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker)
        {
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "dd"
            pickdate = dateFormatter.string(from: sender.date)
            print("pickdate----",pickdate)
            
           let dateFormatter1 = DateFormatter()
           dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
           dateFormatter1.dateFormat = "MMMM"
           pickmonth = dateFormatter1.string(from: sender.date)
           print("pickmonth----",pickmonth)
            
            
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter2.dateFormat = "yyyy"
            pickweek = dateFormatter2.string(from: sender.date)
            print("pickweek----",pickweek)
            
            
            let dateFormatter3 = DateFormatter()
            dateFormatter3.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter3.dateFormat = "yyyy-MM-dd"
            fulldate = dateFormatter3.string(from: sender.date)
            print("fulldate----",fulldate)
            
        }
    
    
    @IBAction func cancelbtnClicked(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5) {
            
            
            self.setdateboolval = false
            
            self.DatepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            self.view.sendSubviewToBack(self.DatepickerView)
            self.DatepickerView.isHidden = true
            self.setdatebutton.isHidden = false
            
            self.setdateday.isHidden = true
            
        }
    }
    
    // MARK:- // Cancel Booking Button
    
    @IBOutlet weak var cancelBookingButtonOutlet: UIButton!
    @IBAction func tapCancelBooking(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
    // MARK:- // Next Step and Save Booking Button
    
    
    @IBOutlet weak var nextStepAndSaveBookingButtonOutlet: UIButton!
    @IBAction func tapNextStepOrSaveBooking(_ sender: UIButton) {
        
        if self.buttonClicked.elementsEqual("ST")  // Selected Time
        {
            self.buttonClicked = "MT"
            
            self.setViewAccordingly()
            
        }
        else  // Message To
        {
            self.validateData {
                
                self.saveServiceBooking()
                
            }
            
        }
        
    }
    
    
    
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // CollectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("date--------",((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count)
        
        return ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCollectionCell", for: indexPath) as! timeAndDateCVCCollectionViewCell
        

        

        dateMonthYearArray = "\((((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray)[indexPath.item] as! NSDictionary)["date"] ?? "")".components(separatedBy: "-") as NSArray
        
        print("datemonthyeararray------",dateMonthYearArray)
        
        
        var monthArray = NSArray()
        
        monthArray = "\((((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray)[indexPath.item] as! NSDictionary)["month"] ?? "")".components(separatedBy: " ") as NSArray
        
        print("monthArray------",monthArray)


        cell.dateLBL.text = "\(dateMonthYearArray[2])"
      
        setdateview.isHidden = true

            cell.dayLBL.text = "\((((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray)[indexPath.item] as! NSDictionary)["day"] ?? "")"
        
         cell.monthAndYearLBL.text = "\(monthArray[1])" +  " " + "\(dateMonthYearArray[0])"
 
        pickdaycell = cell.dateLBL.text!
        print("pickdaycell---",pickdaycell)
        
        testintcell = Int(pickdaycell)
        print("testintcell---",testintcell as Any)
        testintcell = indexPath.item
        print("testintcell---",testintcell as Any)
        
        //SetFont
        cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        cell.dayLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        cell.monthAndYearLBL.font = UIFont(name:  cell.monthAndYearLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
       
        cell.setcalendardatebtn.tag = indexPath.row
        //cell.setcalendardatebtn.addTarget(self, action: #selector(self.setdateBTnClicked(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.timeAndDateCollectionview.frame.size.width, height: 100)
        
        
    }
    /*
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let intTest = timeAndDateCollectionview.contentOffset.x / timeAndDateCollectionview.frame.size.width
        
        intvalue = Int(intTest)
        
        print("--------intvalue-------", intTest)
        print("--------scrollViewDidScroll-------", intTest)
    }
    */
//    @objc func setdateBTnClicked(sender : UIButton)
//    {
//       // self.ShowCalendarView.isHidden = false
//
//        let rowindex:Int = sender.tag
//        print(rowindex)
//
//        self.DatepickerView.isHidden = false
//
//
//        self.datepicker.addTarget(self, action: #selector(serviceBookingInformationViewController.datePickerValueChanged(_:)), for: .valueChanged)
//
//        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
//        self.DatepickerView.addGestureRecognizer(self.tapGesture)
//
//        self.timeAndDateCollectionview.bringSubviewToFront(self.DatepickerView)
//
//    }
    
    
    

    //MARK:- FScalendar Delegate and DataSource Methods
       func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {


              let daTE = self.formatter.string(from: date)
               let dateStr = self.dateFormatter.string(from: date)
               let dateSTROne = self.APIdateFormatter.string(from: date)
             
           self.ShowCalendarView.isHidden = true
          // print("did select date \(self.formatter.string(from: date))")


       }
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print("booingarraycount---",(bookingArray[testint] as! NSArray).count)
        
        if self.bookingArray.count == 0
        {
            return 0
        }
        else
        {
            return (bookingArray[testint] as! NSArray).count
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BookingTimesTVCTableViewCell
        
        cell.timeLBL.text = "\(((bookingArray[testint] as! NSArray)[indexPath.row] as! NSDictionary)["time"]!)"
        
        self.checkImageArray.append(cell.checkImage)
        
        
        //SetFont
        cell.timeLBL.font = UIFont(name: cell.timeLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return (35/568)*self.FullHeight
    //
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        for i in 0..<checkImageArray.count
        {
            checkImageArray[i].image = UIImage(named: " ")
        }
        
        checkImageArray[indexPath.row].image = UIImage(named: "checked")
        
        self.dateIndexpath = indexPath.row
        
    }
    
    
    
    
    
    
    
    
    // MARK: - // JSON POST Method to get Service Booking Information
    
    func getServiceBookingInformation()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_service_book_details_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(SID!)&lang_id=\(langID!)&product_id=\(productID!)"
        
        print("Service Booking Information URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Service Booking Information Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {
                        self.bookingInformationDictionary = json["info_array"] as? NSDictionary
                        
                        self.bookingInformationArray = (((json["info_array"] as! NSDictionary)["service_info"] as! NSDictionary)["booking_available"] as! NSArray)
                        
                        for i in 0..<self.bookingInformationArray.count
                        {
                            let tempArray = ((self.bookingInformationArray[i] as! NSDictionary)["time_slot_list"] as! NSArray)
                            
                            self.bookingArray.add(tempArray)
                            
                            
                        }
                        
                        
                        
                        for j in 0..<self.bookingInformationArray.count {
                            self.arrDate.add((self.bookingInformationArray[j] as! NSDictionary)["date"] as! String )
                        }
                        print("DATE ARRAY:::...", self.arrDate)
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.timeAndDateCollectionview.delegate = self
                            self.timeAndDateCollectionview.dataSource = self
                            self.timeAndDateCollectionview.reloadData()
                            
                            self.bookingTimesTableview.delegate = self
                            self.bookingTimesTableview.dataSource = self
                            self.bookingTimesTableview.reloadData()
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                    self.customView.removeFromSuperview()
                }
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to Save Service Booking
    
    func saveServiceBooking()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_service_book_save_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)&selected_slot=\(selectedSlot!)&selected_date=\(selectedDate!)&selected_time=\(selectedTime!)&email=\(self.emailTXT.text!)&phone_no=\(self.phoneNoTXT.text!)&message=\(self.messageTXT.text!)"
        
        print("Save Service Booking URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Save Service Booking Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.showAlert(title: "Booking Response", message: "\(json["message"]!)")
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                            
                        }
                        
                    }
                    else
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.customAlert(title: "Booking Response", message: "\(json["message"]!)", completion: {
                                
                                self.navigationController?.popViewController(animated: true)
                                
                            })
                            
                            SVProgressHUD.dismiss()
                            self.customView.removeFromSuperview()
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    
    
    
    
    
    // MARK:- // Fill Data After Firing API
    
    func fillData()
    {
        sellerImage.sd_setImage(with: URL(string: "\(bookingInformationDictionary["business_image"]!)"))
        
        ratingView.rating = Double("\(bookingInformationDictionary["avg_rating"]!)")!
        ratingView.settings.filledImage = UIImage(named: "Shape")
        
        address.text = "\(bookingInformationDictionary["address"]!)"
        phoneNo.text = "\(bookingInformationDictionary["mobile"]!)"
        
        
        serviceImage.sd_setImage(with: URL(string: "\((bookingInformationDictionary["service_info"] as! NSDictionary)["product_image"]!)"))
        
        serviceName.text = "\((bookingInformationDictionary["service_info"] as! NSDictionary)["service_product_name"]!)"
        
        perHourLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "perHour", comment: "")
        
        servicePrice.text = "\((bookingInformationDictionary["service_info"] as! NSDictionary)["currency_symbol"]!)" + "\((bookingInformationDictionary["service_info"] as! NSDictionary)["reserve_price"]!)"
        
        serviceDescription.text = "\((bookingInformationDictionary["service_info"] as! NSDictionary)["description"]!)"
        
        
    }
    
    
    
    
    // MARK:- // SetFont
    
    func setFont()
    {
        self.sellerName.font = UIFont(name: self.sellerName.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        self.reviews.font = UIFont(name: self.reviews.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.address.font = UIFont(name: self.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.phoneNo.font = UIFont(name: self.phoneNo.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.serviceName.font = UIFont(name: self.serviceName.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.perHourLBL.font = UIFont(name: self.perHourLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.servicePrice.font = UIFont(name: self.servicePrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.serviceDescription.font = UIFont(name: self.serviceDescription.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.selectedTimeButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "selectedTime", comment: ""), for: .normal)
        self.messageButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "messageTo", comment: ""), for: .normal)
        self.emailLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "emailId", comment: "")
        self.phoneNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "phoneNo", comment: "")
        self.messageLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "message", comment: "")
        self.emailTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_email", comment: "")
        self.phoneNoTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enterPhoneNo", comment: "")
        self.messageTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enterMessage", comment: "")
    }
    
    
    
    
    
    
    // MARK:- // Set View Accordingly Button CLicked
    
    func setViewAccordingly()
    {
        
        if self.buttonClicked.elementsEqual("ST")
        {
            
            self.selelctedTimeView.isHidden = false
            self.messageToView.isHidden = true
            
            selectedTimeButtonOutlet.setTitleColor(UIColor.black, for: .normal)
            messageButtonOutlet.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.nextStepAndSaveBookingButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "nextStep", comment: ""), for: .normal)
            self.cancelBookingButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancelBooking", comment: ""), for: .normal)
            
            
            var bookingTableviewHeight : CGFloat!
            
            if self.bookingArray.count == 0
            {
                bookingTableviewHeight = 0
            }
            else
            {
                bookingTableviewHeight = CGFloat((self.bookingArray[self.testint] as! NSArray).count) * 35//((35/568)*self.FullHeight)
            }
            
            
            self.bookingTimeTableHeightConstraint.constant = bookingTableviewHeight
            
        }
        else
        {
            self.selelctedTimeView.isHidden = true
            self.messageToView.isHidden = false
            
            selectedTimeButtonOutlet.setTitleColor(UIColor.darkGray, for: .normal)
            messageButtonOutlet.setTitleColor(UIColor.black, for: .normal)
            
            self.nextStepAndSaveBookingButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendBookingRequest", comment: ""), for: .normal)
            self.cancelBookingButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancelBooking", comment: ""), for: .normal)
        }
        
        self.hideNextOrPrevious()
        
    }
    
    
    
    
    
    
    
    // MARK:- // Tap Next
    
    func TapOnNext()
    {
        testint = testint + 1
        
        if testint > (((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count - 1) // tapping next on last item
        {
            testint = 0
            
            // Scrolling CollectionView
            
            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count {
                self.timeAndDateCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)
                
            }
            
        }
        else  // tapping next normally
        {
            
            // Scrolling CollectionView According to Selected Property
            
            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count {
                self.timeAndDateCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)
                
            }
            
            
        }
        
    }
    
    func setdatefromcalender()
    {
      
        for i in 0..<self.arrDate.count
        {
            let str = arrDate.object(at: i)as! String
                print("str------",str)
          
            if arrDate.contains(fulldate)
            {
                if str == fulldate
                {


                let nextDate: IndexPath = IndexPath(item: i, section: 0)
                          // Scroll happens here
                          if nextDate.row < ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count {
                              self.timeAndDateCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)
                           

                }
                
                 break

                }
            }
            else
            {

                self.ShowAlertMessage(title: "Warning", message: "Date doesn't exist")
                
                break
            }


 
        }
    }
    
    
    // MARK:- // Tap on Previous
    
    func TapOnPrevious()
    {
        
        testint = testint - 1
        
        if testint < 0   // tapping previous on first item
        {
            testint = ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count - 1
            
            // Scrolling CollectionView According to Selected Property
            
            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count {
                self.timeAndDateCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)
                
            }
            
        }
        else  // tapping previous normally
        {
            
            // Scrolling CollectionView According to Selected Property
            
            print("testint-----",testint)
            
            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < ((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count {
                self.timeAndDateCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)
                
            }
            
        }
        
        
    }
    
    
    // MARK:- // Hide Previous Arrow on first and Next Scroll on Last
    
    func hideNextOrPrevious()
    {
        
        if testint == 0
        {
            self.leftArrowOutlet.isHidden = true
            self.rightArrowOutlet.isHidden = false
        }
        else if testint == (((bookingInformationDictionary["service_info"] as! NSDictionary)["booking_available"] as! NSArray).count - 1)
        {
            self.leftArrowOutlet.isHidden = false
            self.rightArrowOutlet.isHidden = true
        }
        else
        {
            self.leftArrowOutlet.isHidden = false
            self.rightArrowOutlet.isHidden = false
        }
        
    }
    
    
    // MARK:- // Validate Data before Booking
    
    func validateData(completion : @escaping () -> ())
    {
        // get Data
        //productID = "\((bookingInformationDictionary["service_info"] as! NSDictionary)["procuct_id"]!)"
        selectedSlot = "\((bookingInformationDictionary["service_info"] as! NSDictionary)["slot_time"]!)"
        selectedDate = "\((bookingInformationArray[self.testint] as! NSDictionary)["date"]!)"
        
        if dateIndexpath == nil
        {
        
            selectedTime = ""
        }
        else
        {
            selectedTime = "\((((bookingInformationArray[self.testint] as! NSDictionary)["time_slot_list"] as! NSArray)[dateIndexpath] as! NSDictionary)["time_format"]!)"
        }
        
        let emailStatus = isValidEmail(testStr: emailTXT.text!)
        let phoneStatus = isValidPhone(value: phoneNoTXT.text!)
        
        if emailStatus == false
        {
            showAlert(title: "Invalid Email", message: "Please enter a valid Email.")
        }
        else if phoneStatus == false
        {
            showAlert(title: "Invalid Phone No", message: "Please enter a valid Phone No.")
        }
        else if (messageTXT.text?.isEmpty)!
        {
            showAlert(title: "Message Empty", message: "Please input any message to continue...")
        }
        else if selectedDate.isEmpty
        {
            showAlert(title: "No Date Selected", message: "Please select a Date.")
        }
        else if selectedTime.isEmpty
        {
            showAlert(title: "No Time SElected.", message: "Please select a time.")
        }
        else
        {
            completion()
        }
    }
    
    
    
    // MARK:- // Validate Email
    
    func isValidEmail(testStr:String) -> Bool {
        
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    
    // MARK:- // Validate Phone No
    
    func isValidPhone(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    // MARK:- // Validation Alert Function
    
    func showAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil )
        
    }
    func Set_Font()
    {

        
    }
    
}

