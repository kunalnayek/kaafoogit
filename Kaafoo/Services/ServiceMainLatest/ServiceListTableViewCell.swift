//
//  ServiceListTableViewCell.swift
//  Kaafoo
//
//  Created by IOS-1 on 03/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class ServiceListTableViewCell: UITableViewCell {


    @IBOutlet weak var VerifiedImage: UIImageView!
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ReviewView: CosmosView!
    @IBOutlet weak var ServiceProviderName: UILabel!
    @IBOutlet weak var VerifiedAccount: UILabel!
    @IBOutlet weak var ServiceProviderAddress: UILabel!
    @IBOutlet weak var ReviewsNo: UILabel!
    @IBOutlet weak var WatchlistBtn: UIButton!
    @IBOutlet weak var ServiceProviderImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
