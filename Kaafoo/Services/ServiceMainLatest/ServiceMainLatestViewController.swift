//
//  ServiceMainLatestViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 03/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ServiceMainLatestViewController: GlobalViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDelegate,UIPickerViewDataSource {


    
    // MARK:- // Tap on Category Button
    
    
    
   
    @IBOutlet weak var SortPickerView: UIPickerView!
    @IBOutlet weak var SortView: UIView!
    @IBOutlet weak var filteroutlet: UIButton!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var tapOnCategoryBtnOutlet: UIButton!
    @IBAction func TapOnCategoryBtn(_ sender: UIButton) {
        self.ShowPicker.isHidden = !self.ShowPicker.isHidden
    }
    
    @IBOutlet weak var ShowPicker: UIView!
    @IBOutlet weak var dataPicker: UIPickerView!
    @IBOutlet weak var SearchSortView: UIView!
    @IBOutlet weak var OutsideScrollView: UIView!
    @IBOutlet weak var ImagePageControl: UIPageControl!
    
    
    // MARK:- // List Grid Button
    
    @IBAction func ListGridBtn(_ sender: UIButton) {
        if ServiceListTableview.isHidden == true
        {
//            SVProgressHUD.show(withStatus: "Loading! Please Wait")
            ServiceListCollectionView.isHidden = true
            ServiceListTableview.isHidden = false
            self.ListGridBtnOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        }
        else
        {
//            SVProgressHUD.show(withStatus: "Loading")
            ServiceListCollectionView.isHidden = false
            ServiceListTableview.isHidden = true
            self.ListGridBtnOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        }
    }
    
    @IBOutlet weak var SortCategoryTableview: UITableView!
    @IBOutlet weak var ListGridBtnOutlet: UIButton!
    @IBOutlet weak var ServiceListTableview: UITableView!
    @IBOutlet weak var ImageScrollView: UIScrollView!
    @IBOutlet weak var ServiceListCollectionView: UICollectionView!
    
    
    let dispatchGroup = DispatchGroup()
    var typeID : String! = ""
    var typeName : String! = ""
    var SearchWith : String! = ""
    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    var servicesMainDictionary : NSDictionary!

    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)

    var categoryNameToShow : String!

    var businessNameHeight : CGFloat!

    var businessAddressHeight : CGFloat!

    var viewHeight : CGFloat!

    var tableviewHeight: CGFloat = 0

    var localCategoryArray : NSMutableArray! = [["id" : " " , "name" : "All"]]

    var tapGesture = UITapGestureRecognizer()

    var localSortCategoryArray = [["key" : "" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")] , ["key" : "T" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")] , ["key" : "N" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "newest_listed", comment: "")] , ["key" : "C" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "closing_soon", comment: "")]]
    
     var sortDict = [["key" : "" , "value" : "All"], ["key" : "T" , "value" : "Title"],["key" : "N" , "value" : "Newest Listed"] ,["key" : "C" , "value" : "Closing Soon"]]

    var sortType : String! = ""

    var searchString : String! = ""

    var currentRecordsArray : NSMutableArray = NSMutableArray()

    var sellerID : String! = ""

    let userID = UserDefaults.standard.string(forKey: "userID")
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        
        self.CreateFavouriteView(inview: self.view)
        
        self.ListGridBtnOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        super.viewDidLoad()
        self.SetTapGesture()
        self.ServiceListTableview.isHidden = false
        self.ServiceListCollectionView.isHidden = true
        self.ServiceListTableview.separatorStyle = .none
        self.SortPickerView.delegate = self
        self.SortPickerView.dataSource = self
        self.SortPickerView.reloadAllComponents()
        self.defineSortButtonActions()
        self.getServicesData()
        
       
        
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.currentRecordsArray.count == 0
            {
               self.ShowAlertMessage(title: "Warning", message: "No data found")
            }
            else
            {
               print("do nothing")
            }
        })
        self.headerView.tapSearch.addTarget(self, action: #selector(self.HeaderSearch(sender:)), for: .touchUpInside)
        self.SearchBar.delegate = self
        self.SearchBar.isHidden = true
    }
    
    
    // MARK:- // Define Filter Button Actions

       func defineSortButtonActions() {

           self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        
         self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)
        
           self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

       }


       // MARK;- // Sort Action

       @objc func sortAction(sender: UIButton) {

           self.filterView.isHidden = true
           self.view.sendSubviewToBack(self.filterView)

           self.SortView.isHidden = false
       }

     // MARK;- // Filter Action

        @objc func filterAction(sender: UIButton) {
            
            self.filterView.isHidden = true
                      self.view.sendSubviewToBack(self.filterView)

                      self.SortView.isHidden = true
            self.SearchSortView.isHidden = false
    }


       // MARK;- // Tap To Close

       @objc func tapToClose(sender: UIButton) {

           self.filterView.isHidden = true
           self.view.sendSubviewToBack(self.filterView)
       }
  
    
    // MARK:- // filter Clicked
    
    @IBAction func filterbuttonClicked(_ sender: Any)
    {

        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else
            {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

    }
    
    
    // MARK:- // View Will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        self.ShowPicker.isHidden = true
    }
    
    
    @objc func HeaderSearch(sender : UIButton)
    {
        self.SearchBar.isHidden = false
    }
    
    
    //MARK:- // JSON Post Method to get Listing Data
    
    func getServicesData()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        print("test",localCategoryArray)

        var parameters : String! = ""


        if (userID?.isEmpty)!
        {
            parameters = "lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&mycountry_name=\(myCountryName!)&child_cat_id=\(typeID!)&sort_search=\(sortType!)&search_with=\(SearchWith ?? "")"
        }
        else
        {
            parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&mycountry_name=\(myCountryName!)&child_cat_id=\(typeID!)&sort_search=\(sortType!)&search_with=\(SearchWith ?? "")"
        }
        self.CallAPI(urlString: "app_service_list_main", param: parameters, completion:
            {
                self.servicesMainDictionary = self.globalJson["info_array"] as? NSDictionary

                for i in 0..<((self.servicesMainDictionary["service"] as! NSArray).count)

                {

                    let tempDict = (self.servicesMainDictionary["service"] as! NSArray)[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)


                }

                self.currentRecordsArray = self.recordsArray.mutableCopy() as! NSMutableArray

                self.nextStart = "\(self.globalJson["next_start"]!)"

                self.startValue = self.startValue + (self.servicesMainDictionary["service"] as! NSArray).count

                print("Next Start Value : " , self.startValue)

                self.localCategoryArray.removeAllObjects()
                self.localCategoryArray.add(["id" : "","name" : "All"])
                for i in 0..<((self.servicesMainDictionary["category_list"] as! NSArray).count)
                {
                    self.localCategoryArray.add((self.servicesMainDictionary["category_list"] as! NSArray)[i] as! NSDictionary)
                }
                print("LocalCategoryArray",self.localCategoryArray!)

                DispatchQueue.main.async {

                    self.globalDispatchgroup.leave()
                    
                    self.setImageScroll()
                    
                    self.ServiceListTableview.delegate = self
                    
                    self.ServiceListTableview.dataSource = self
                    
                    self.ServiceListTableview.reloadData()
                    
                    self.ServiceListTableview.estimatedRowHeight = UITableView.automaticDimension
                    
                    self.ServiceListTableview.rowHeight = UITableView.automaticDimension

                    self.ServiceListCollectionView.delegate = self
                    
                    self.ServiceListCollectionView.dataSource = self
                    
                    self.ServiceListCollectionView.reloadData()

                    self.dataPicker.delegate = self
                    
                    self.dataPicker.dataSource = self
                    
                    self.dataPicker.reloadAllComponents()
                    
                    SVProgressHUD.dismiss()
                }
        })
    }
    func setImageScroll()
    {

        let tempArray = (self.servicesMainDictionary["slider_image"] as! NSArray)
        self.ImagePageControl.numberOfPages = tempArray.count
        for i in 0..<tempArray.count
        {
            self.tempFrame.origin.x = self.ImageScrollView.frame.size.width * CGFloat(i)
            print("OriginX",self.tempFrame.origin.x)
            self.tempFrame.size = self.ImageScrollView.frame.size

            let imageview = UIImageView(frame: self.tempFrame)
            imageview.sd_setImage(with: URL(string: "\((tempArray[i] as! NSDictionary)["slider_img"]!)"))

            imageview.contentMode = .scaleAspectFill
            imageview.clipsToBounds = true

            self.ImageScrollView.addSubview(imageview)
        }
        self.ImageScrollView.contentSize = CGSize(width: (self.ImageScrollView.frame.size.width * CGFloat(tempArray.count)), height: self.ImageScrollView.frame.size.height)
        self.ImageScrollView.delegate = self
    }

    // MARK: - // JSON POST Method to add Favourite Service

    func serviceAddToFavourites(sellerid : String)

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }


        let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")


        parameters = "user_id=\(userID!)&lang_id=\(langID!)&buss_seller_id=\(sellerid)&cat_id=11"

        print("Save Service Booking URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Service Add to Favourites Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Add to Favourites Response", message: "\(json["message"]!)")

                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Add to Favourites Response", message: "\(json["message"]!)")


                            self.startValue = 0
                            self.recordsArray.removeAllObjects()
                            self.currentRecordsArray.removeAllObjects()
                            self.getServicesData()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }
    // MARK: - // JSON POST Method to Remove Favourite Service

    func serviceRemoveFromFavourites(sellerid : String)

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }


        let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")


        parameters = "user_id=\(userID!)&lang_id=\(langID!)&buss_seller_id=\(sellerid)&cat_id=11"

        print("Remove Service from Favourite URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Service Remove from Favourites Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Remove from Favourites Response", message: "\(json["message"]!)")

                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Remove from Favourites Response", message: "\(json["message"]!)")

                            self.startValue = 0
                            self.recordsArray.removeAllObjects()
                            self.currentRecordsArray.removeAllObjects()
                            self.getServicesData()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }

    // MARK:- // Add to Favourites

    @objc func addToFavourites(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.serviceAddToFavourites(sellerid: "\((currentRecordsArray[sender.tag] as! NSDictionary)["seller_id"]!)")
        }

    }

    // MARK:- // Remove From Favourites

    @objc func removeFromFavourites(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.serviceRemoveFromFavourites(sellerid: "\((currentRecordsArray[sender.tag] as! NSDictionary)["seller_id"]!)")
        }

    }

    //Custom Alert Function
    func showAlert(title : String!,message : String!)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }

    //MARK:- //UITABLEVIEW Delegate and Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.currentRecordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "servicelist") as! ServiceListTableViewCell
        let imgStr = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["business_image"]!)"
        obj.ServiceProviderImage.sd_setImage(with: URL(string: imgStr))
        obj.ServiceProviderName.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        obj.ReviewsNo.text = "Reviews(" + "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["reviews_count"]!)" + ")"
        obj.ServiceProviderAddress.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        obj.ReviewView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)")!
        obj.ReviewView.settings.updateOnTouch = false
        let VerifiedImgStr = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["verified_img"]!)"
        obj.VerifiedImage.sd_setImage(with: URL(string: VerifiedImgStr))
        if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["verified_status"]!)".elementsEqual("V")
        {
           obj.VerifiedAccount.isHidden = false
           obj.VerifiedAccount.text = "Verified Account"
        }
        else
        {
           obj.VerifiedAccount.isHidden = true
           obj.VerifiedAccount.text = "Nothing"
        }
        if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["favourite_status"]!)".elementsEqual("1")
        {
            obj.WatchlistBtn.setImage(UIImage(named: "heart_red"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(removeFromFavourites(sender:)), for: .touchUpInside)
        }
        else if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["favourite_status"]!)".elementsEqual("0")
        {
            obj.WatchlistBtn.setImage(UIImage(named: "heart"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(addToFavourites(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.WatchlistBtn.setImage(UIImage(named: "heart"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(addToFavourites(sender:)), for: .touchUpInside)
        }
        obj.selectionStyle = .none
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookVC") as! ServiceBookViewController
//        sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
//        UserDefaults.standard.set(sellerID, forKey: "sellerID")
//        navigate.sellerID = sellerID
//        self.navigationController?.pushViewController(navigate, animated: true)
        
        
        
        //let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ServiceVC") as! NewServiceProfileVC

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController

        sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
        print(sellerID)
        navigate.sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
        
        //UserDefaults.standard.set(sellerID, forKey: "sellerID")
        //navigate.SellerId = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }


    //MARK:- //UICOLLECTIONVIEW Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.currentRecordsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "servicelist", for: indexPath) as! ServiceListCollectionViewCell
        let imgStr = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["business_image"]!)"
        obj.ServiceProviderImage.sd_setImage(with: URL(string: imgStr))
        obj.ServiceProviderName.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        obj.ServiceProviderAddress.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        obj.ReviewsNo.text = "Reviews(" + "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["reviews_count"]!)" + ")"
        obj.ReviewView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)")!
        obj.ReviewView.settings.updateOnTouch = false
        let VerifiedImgStr = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["verified_img"]!)"
        obj.VerifiedImage.sd_setImage(with: URL(string: VerifiedImgStr))
        if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["verified_status"]!)".elementsEqual("V")
        {
            obj.VerifiedAccount.isHidden = false
            obj.VerifiedAccount.text = "Verified Account"
        }
        else
        {
            obj.VerifiedAccount.isHidden = true
            obj.VerifiedAccount.text = "Nothing"
        }
        if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["favourite_status"]!)".elementsEqual("1")
        {
           obj.WatchlistBtn.setImage(UIImage(named: "heart_red"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(removeFromFavourites(sender:)), for: .touchUpInside)
        }
        else if "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["favourite_status"]!)".elementsEqual("0")
        {
           obj.WatchlistBtn.setImage(UIImage(named: "heart"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(addToFavourites(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.WatchlistBtn.setImage(UIImage(named: "heart"), for: .normal)
            obj.WatchlistBtn.addTarget(self, action: #selector(addToFavourites(sender:)), for: .touchUpInside)
        }

        return obj
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding : CGFloat! = 4

        let collectionViewSize = ServiceListCollectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: (224/568)*self.FullHeight )
    }


    //MARK:- //PickerView Delegate and DataSource Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return self.localCategoryArray.count
        
         if pickerView == SortPickerView
               {
                   return sortDict.count
               }
               else
               {
                   return self.localCategoryArray.count
               }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return "\((self.localCategoryArray[row] as! NSDictionary)["name"]!)"
        
                if pickerView == SortPickerView
               {
                   return "\((sortDict[row] as NSDictionary)["value"]!)"
               }
               else
               {
                     return "\((self.localCategoryArray[row] as! NSDictionary)["name"]!)"
               }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        
            if pickerView == SortPickerView
               {
                   self.sortType = "\((self.sortDict[row] as NSDictionary)["key"]!)"
                   self.SortView.isHidden = true
                   self.startValue = 0
                   self.recordsArray.removeAllObjects()
                   self.currentRecordsArray.removeAllObjects()
                   self.getServicesData()
               }
               else
               {
                   let BtnName = "\((self.localCategoryArray[row] as! NSDictionary)["name"]!)"
                          self.tapOnCategoryBtnOutlet.setTitle(BtnName, for: .normal)
                          self.ShowPicker.isHidden = true
                          let SetId = "\((self.localCategoryArray[row] as! NSDictionary)["id"]!)"
                          self.typeID = SetId
                          self.startValue = 0
                          self.recordsArray.removeAllObjects()
                          self.currentRecordsArray.removeAllObjects()
                          self.getServicesData()
               }
    }

    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if scrollView == ImageScrollView
        {

            let pageNumber = ImageScrollView.contentOffset.x / ImageScrollView.frame.size.width

            ImagePageControl.currentPage = Int(pageNumber)
        }

        else
        {

            if scrollBegin > scrollEnd
            {

            }
            else
            {

//                print("next start : ",nextStart )

                if (nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }


                }
                else
                {
                    getServicesData()
                }



            }
        }

    }

    func SetTapGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.ShowPicker.isUserInteractionEnabled = true
        self.ShowPicker.addGestureRecognizer(tap)
    }
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.ShowPicker.isHidden = true
    }

}
 

extension ServiceMainLatestViewController : UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Searching is in progress")
        view.endEditing(true)
        self.SearchWith = self.SearchBar.searchTextField.text
        self.recordsArray.removeAllObjects()
        self.currentRecordsArray.removeAllObjects()
        self.startValue = 0
        self.getServicesData()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.SearchBar.searchTextField.text = ""
        self.SearchBar.isHidden = true
        view.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("nothing")
    }
}
