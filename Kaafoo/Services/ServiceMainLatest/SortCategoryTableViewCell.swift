//
//  SortCategoryTableViewCell.swift
//  Kaafoo
//
//  Created by IOS-1 on 07/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class SortCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var SeparatorView: UIView!
    @IBOutlet weak var CellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
