//
//  SocialMediaVC.swift
//  Kaafoo
//
//  Created by priya on 24/07/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class SocialMediaVC: GlobalViewController,UITextFieldDelegate {
    
    @IBOutlet weak var fbtext: UITextField!
    
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var googletext: UITextField!
    @IBOutlet weak var twittertext: UITextField!
    
    @IBOutlet weak var instagramtext: UITextField!
    @IBOutlet weak var whatsuptext: UITextField!
    
    @IBOutlet weak var wechattext: UITextField!
    @IBOutlet weak var linetext: UITextField!
    
    @IBOutlet weak var smslinktext: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fbtext.delegate = self
        googletext.delegate = self
        twittertext.delegate = self
        instagramtext.delegate = self
        whatsuptext.delegate = self
        smslinktext.delegate = self
        linetext.delegate = self
        
       
    }
    
    
    func isValidUrl(url: String) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
    
    @IBAction func submitbtnClicked(_ sender: Any)
    {
        //self.fbtext.text?.isEmpty == true
        
        if  (isValidUrl(url:fbtext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid facebook link")
        }
        else if (isValidUrl(url:googletext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid google link")
        }
        else if  (isValidUrl(url:twittertext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid twitter link")
        }
        else if  (isValidUrl(url:instagramtext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid instagram link")
        }
        else if  (isValidUrl(url:whatsuptext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid whatsup link")
        }
        else if  (isValidUrl(url:smslinktext.text!)==false)
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter Valid smslink")
        }
     
        else
        {
            self.getdata()
        }
    }
    
    
    func getdata()
    {
        
        let LangID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let parameters = "user_id=\(userID!)&lang_id=\(LangID!)&fb_scl_lnk=\(fbtext.text ?? "")&ggl_pls_link=\(googletext.text ?? "")&twitr_lnk=\(twittertext.text ?? "")&instgrm_lnk=\(instagramtext.text ?? "")&wtsaap_lnk=\(whatsuptext.text ?? "")&sms_lnk=\(smslinktext.text ?? "")&line_lnk=\(linetext.text ?? "")&wecht_lnk=\(wechattext.text ?? "")"
       
        self.CallAPI(urlString: "app_social_media_save", param: parameters, completion: {
              
                   DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        SVProgressHUD.dismiss()
                        let responseMessage = "\(self.globalJson["message"]!)"
                        self.ShowAlertMessage(title: "warning", message: responseMessage)
                      
                    }
            })

    }
    
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        fbtext.resignFirstResponder()
        googletext.resignFirstResponder()
        wechattext.resignFirstResponder()
        linetext.resignFirstResponder()
        instagramtext.resignFirstResponder()
        twittertext.resignFirstResponder()
        smslinktext.resignFirstResponder()
        
        self.view.endEditing(true)
        return true
    }

}

