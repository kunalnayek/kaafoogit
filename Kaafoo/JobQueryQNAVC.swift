//
//  JobQueryQNAVC.swift
//  Kaafoo
//
//  Created by esolz on 17/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class JobQueryQNAVC: GlobalViewController {
    
    @IBOutlet weak var JobQNATV: UITableView!
    
    var QNAArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var ViewReport: UIView!
    
    @IBOutlet weak var hideViewReport: UIButton!
    
    @IBOutlet weak var reportView: Report!
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var JobID : String! = ""
    
    var MessageID : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLayer()
        
        self.ConfigureReportView()
        
        self.JobQNATV.delegate = self
        
        self.JobQNATV.dataSource = self
        
        self.JobQNATV.reloadData()
        // Do any additional setup after loading the view.
    }
    @IBAction func hideReportViewBtn(_ sender: UIButton) {
        
        self.ViewReport.isHidden = true
    }
    
    @objc func report(sender : UIButton)
    {
        self.JobID = "\((self.QNAArray[sender.tag] as! NSDictionary)["job_id"] ?? "")"
        
        self.MessageID = "\((self.QNAArray[sender.tag] as! NSDictionary)["message_id"] ?? "")"
        
        self.ViewReport.isHidden = false
        
        self.reportView.ReportTxtView.text = ""
    }
    
    //MARK:- //Set Layer Configuration for design
    
    func setLayer()
    {
        self.JobQNATV.bounces = false
        
        self.JobQNATV.separatorStyle = .none
    }
    
    func ConfigureReportView()
    {
        self.reportView.ReportLbl.text = "Reply"
        
        self.reportView.CrossBtnOutlet.addTarget(self, action: #selector(hideReport(sender:)), for: .touchUpInside)
        
        self.reportView.SendBtnOutlet.addTarget(self, action: #selector(SendReply(sender:)), for: .touchUpInside)
    }
    
    @objc func hideReport(sender : UIButton)
    {
        self.ViewReport.isHidden = true
    }
    
    @objc func SendReply(sender : UIButton)
    {
        if self.reportView.ReportTxtView.text.elementsEqual("")
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reply text can not be blank", comment: ""))
        }
        else
        {
            self.sendReply()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func sendReply()
    {
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")&msg_id=\(self.MessageID ?? "")&job_id=\(self.JobID ?? "")&answer=\(self.reportView.ReportTxtView.text ?? "")"
        
        self.CallAPI(urlString: "app_job_question_query_reply", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                DispatchQueue.main.async {
                    
                    self.customAlert(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: "\(self.globalJson["message"] ?? "")", completion: {
                        
                        self.navigationController?.popViewController(animated: false)
                    })
                    
                    self.ViewReport.isHidden = true
                    
                    SVProgressHUD.dismiss()
                }
            })
        })
    }

}
class JobQueryQNAListigTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var QuestionView: UIView!
    
    @IBOutlet weak var AnswerView: UIView!
    
    @IBOutlet weak var ReplyView: UIView!
    
    @IBOutlet weak var QurstionLabel: UILabel!
    
    @IBOutlet weak var QuestionAnswerName: UILabel!
    
    @IBOutlet weak var QuestionDateTime: UILabel!
    
    @IBOutlet weak var AnswerLabel: UILabel!
    
    @IBOutlet weak var replyBtnOutlet: UIButton!
    
}
 

extension JobQueryQNAVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.QNAArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobqna") as! JobQueryQNAListigTVC
        
        cell.QurstionLabel.text = "Q : " + "\((self.QNAArray[indexPath.row] as! NSDictionary)["question"] ?? "")"
        
        cell.QuestionDateTime.text = "\((self.QNAArray[indexPath.row] as! NSDictionary)["sending_time"] ?? "")"
        
        cell.QuestionAnswerName.text = "\((self.QNAArray[indexPath.row] as! NSDictionary)["sender_name"] ?? "")"
        
        if "\((self.QNAArray[indexPath.row] as! NSDictionary)["answer"] ?? "")".elementsEqual("")
        {
            cell.ReplyView.isHidden = false
            
            cell.AnswerView.isHidden = true
        }
        else
        {
            cell.ReplyView.isHidden = true
            
            cell.AnswerView.isHidden = false
        }
        
        cell.AnswerLabel.text = "\((self.QNAArray[indexPath.row] as! NSDictionary)["answer"] ?? "")"
        
        cell.AnswerLabel.numberOfLines = 0
        
        cell.replyBtnOutlet.addTarget(self, action: #selector(report(sender:)), for: .touchUpInside)
        
        cell.replyBtnOutlet.setTitleColor(.white, for: .normal)
        
        cell.replyBtnOutlet.layer.cornerRadius = cell.replyBtnOutlet.frame.size.height / 2
        
        cell.replyBtnOutlet.clipsToBounds = true
        
        cell.replyBtnOutlet.tag = indexPath.row
        
        cell.ViewContent.layer.cornerRadius = 8.0
        
        cell.ViewContent.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        //Font
        
        cell.AnswerLabel.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionDateTime.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QurstionLabel.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionAnswerName.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        //
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
}
