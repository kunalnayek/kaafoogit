//
//  AddToCartViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 05/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.


import UIKit
import SDWebImage
import SVProgressHUD


class AddToCartViewController:GlobalViewController,UITableViewDelegate,UITableViewDataSource{
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    //let langID = UserDefaults.standard.string(forKey: "langID")
    var LanguageID: NSString!
    
    var ProductID: NSString!
    
    let dispatchGroup = DispatchGroup()
    
    var info_array:NSDictionary!
    
    var ProductDetails : NSMutableArray! = NSMutableArray()
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var PriceDetails: NSMutableDictionary! = NSMutableDictionary()
    
    var CartID : String!
    
    var quantity = 1
    
    var tempQuantity: Int!
    
    var Productid1 : Int!
    
    var Cartid1 : Int!
    
    var Productid2: Int!
    
    var Cartid2: Int!
    
    var newQuantity: String!
    
    var requestDict: NSMutableDictionary! = NSMutableDictionary()
    
    var productQuantity : Int! = 1
    
    var prodcutQuantity1 : Int!
    
    var tempQuantity1 : Int!
    
    var productQuantityArray : NSMutableArray! = NSMutableArray()
    
    var individualitempriceArray : NSMutableArray! = NSMutableArray()
    
    var ProductNoArray : NSMutableArray! = NSMutableArray()
    
    var ItemDel: NSString!
    
    @IBOutlet var PriceView: UIView!
    @IBOutlet var Empty_View: UIView!
    @IBOutlet var Price: UILabel!
    @IBOutlet var total_price: UILabel!
    @IBOutlet var delivery_charge: UILabel!
    @IBOutlet var item_price: UILabel!
    @IBOutlet var AddToCart_tableView: UITableView!
    
    @IBOutlet var DetailTableView: UIView!
    @IBOutlet var Cart_View: UIView!
    @IBOutlet var Lbl1: UILabel!
    @IBOutlet var Lbl2: UILabel!
    @IBOutlet var Lbl3: UILabel!
    @IBOutlet var Lbl4: UILabel!
    
    
    
    
    //MARK:- Navigation To Check Out Page
    @IBAction func Continue_btn(_ sender: UIButton) {
//        let nav =   self.storyboard?.instantiateViewController(withIdentifier: "checkout") as! CheckOutViewController
//        let nav =   self.storyboard?.instantiateViewController(withIdentifier: "checkout") as! CheckOutViewController
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "checkout") as! CheckOutViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
        self.view.addSubview(Empty_View)
        self.view.addSubview(Cart_View)
        self.Cart_View.addSubview(PriceView)
        
        AddToCart_tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.SetFont()
        
        // Do any additional setup after loading the view.
    }
    //MARK:- UITableView Delegate And Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("testCount-----",recordsArray.count)
        
        return recordsArray.count
    }
    
    //MARK:- TableView Cell For Item
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "addtocart") as! AddToCartTableViewCell
        if "\((recordsArray[indexPath.row] as! NSDictionary)["seller_name"]!)" == ""
        {
            obj.seller_name_lbl.isHidden = true
        }
        else
        {
            obj.seller_name_lbl.isHidden = false
            obj.seller_name_lbl.text = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_name"]!)"
        }
        obj.name_lbl.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer"]!)" == "0"
        {
            obj.offer_price.isHidden = true
            obj.start_price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.start_price.textColor = UIColor.green
        }
        else
        {
            obj.offer_price.isHidden = false
            
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.item] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.item] as! NSDictionary)["start_price"]!)")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            obj.start_price.attributedText = attributeString
            
            obj.start_price.textColor = UIColor.green
            obj.offer_price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
            obj.offer_price.textColor = UIColor.red
        }
        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["product_image"]!)"
        obj.Product_image.sd_setImage(with: URL(string: path))
        
        obj.selectionStyle = UITableViewCell.SelectionStyle.none
        obj.item_count.text = "\((recordsArray[indexPath.row] as! NSDictionary)["quantity"]!)"
        obj.addToCart_btn.layer.borderWidth = 2.0
        obj.addToCart_btn.layer.borderColor = UIColor.gray.cgColor
        obj.addToCart_btn.layer.cornerRadius = 5.0
        obj.removeCart_btn.layer.borderWidth = 2.0
        obj.removeCart_btn.layer.borderColor = UIColor.gray.cgColor
        obj.removeCart_btn.layer.cornerRadius = 5.0
        obj.DeleteCart_btn.setTitleColor(.red, for: .normal)
        
        //MARK:- Feature Price Checking
        
        if "\((recordsArray[indexPath.row] as! NSDictionary)["extra_feature_price"]!)" == ""
        {
            obj.Feature_Price.isHidden = true
            obj.Feature_Price.text = ""
        }
        else
        {
            obj.Feature_Price.isHidden = false
            obj.Feature_Price.text = "Feature Price" + " " + ":" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["extra_feature_price"]!)"
        }
        
        //MARK:- Delete Item Button Selector Method
        obj.removeCart_btn.addTarget(self, action: #selector(AddToCartViewController.DeleteItem(sender:)), for: .touchUpInside)
        obj.removeCart_btn.tag = indexPath.row
        
        obj.addToCart_btn.addTarget(self, action: #selector(AddToCartViewController.AddItem(sender:)), for: .touchUpInside)
        obj.addToCart_btn.tag = indexPath.row
        
        obj.DeleteCart_btn.addTarget(self, action: #selector(AddToCartViewController.Delete(sender:)), for: .touchUpInside)
        obj.DeleteCart_btn.tag = indexPath.row
        
        return obj
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return (191/568)*self.FullHeight
    //    }
    //
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK:- API Fire Method
    func loadData()
    {
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_cart_page_details")!   //change the url
        
        var parameters : String = ""
        
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    if "\(json["response"]!)".elementsEqual("1")
                        
                    {
                        self.dispatchGroup.leave()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            
                            self.Cart_View.isHidden = false
                            self.Empty_View.isHidden = true
                            
                            self.info_array = json["info_array"] as? NSDictionary
//                            print("INFO_ARRAY",self.info_array)
                            self.ProductDetails = self.info_array["product_details"] as? NSMutableArray
                            for i in (0..<self.ProductDetails.count)
                            {
                                let ProDetail = self.ProductDetails[i] as! NSDictionary
                                print("ProDetail is : " , ProDetail)
                                
                                self.recordsArray.add(ProDetail )
                                
                                
                            }
                            
                            for i in 0..<self.recordsArray.count
                            {
                                //Individual Item Price Storing In NSMutableArray
                                
                                if "\((self.recordsArray[i] as! NSDictionary)["special_offer"]!)" == "0"
                                {
                                    self.individualitempriceArray.add((self.recordsArray[i] as! NSDictionary)["start_price"]!)
//                                    print("Total Price",self.individualitempriceArray)
                                }
                                else
                                {
                                    self.individualitempriceArray.add((self.recordsArray[i] as! NSDictionary)["reserve_price"]!)
//                                    print("Total Price 1",self.individualitempriceArray)
                                }
                                
                                
                                self.productQuantityArray.add((self.recordsArray[i] as! NSDictionary)["quantity"]!)
                            }
                            
                            self.PriceDetails = self.info_array["price_details"] as? NSMutableDictionary
                            
                            
                            SVProgressHUD.dismiss()
                            self.AddToCart_tableView.reloadData()
                            self.AddToCart_tableView.delegate = self
                            self.AddToCart_tableView.dataSource = self
                            self.AddToCart_tableView.rowHeight = UITableView.automaticDimension
                            self.AddToCart_tableView.estimatedRowHeight = UITableView.automaticDimension
                            self.priceDetailsAPI()
                            self.setFrames()
                            
                        })
                    }
                        
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                            self.Empty_View.isHidden = false
                            self.Cart_View.isHidden = true
                            //self.view.bringSubview(toFront: self.Empty_View)
                        })
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    //MARK:- Price Details API Fire
    func priceDetailsAPI()
    {
        self.delivery_charge.text = "\(self.PriceDetails["shipping_charge"]!)"
        
        self.delivery_charge.textColor = UIColor.green
        
        self.total_price.text = "\(self.PriceDetails["currency_symbol"]!)" + "\(self.PriceDetails["total_amount"]!)"
        
        self.Price.text = "Price(" + "\(self.PriceDetails["count_items"]!)" + "Item)"
        
        self.item_price.text = "\(self.PriceDetails["currency_symbol"]!)" + "\(self.PriceDetails["total_price"]!)"
    }
    //MARK:- Set Frames
    func setFrames()
    {
        
        self.Cart_View.frame = CGRect(x: 0, y: (71/568)*self.FullHeight, width: FullWidth, height: (497/568)*self.FullHeight)
        self.DetailTableView.frame.size.height = (310/568)*self.FullHeight
        
    }
    //MARK:- Remove A Single Cart Selector Method
    @objc func Delete(sender: UIButton)
    {
        
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            self.CartID = "\(tempDictionary["cart_id"]!)"
            
            self.SingleCartRemove()
            
            
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- REMOVE SINGLE ITEM(DELETION) From CART Page
    func SingleCartRemove()
        
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_remove_cart")!   //change the url
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&cart_id=\(CartID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove Item From Cart API " , json)
                    //DispatchQueue Main Asynchronous
                    DispatchQueue.main.async
                        {
                            self.dispatchGroup.leave()
                            
                            
                            self.dispatchGroup.notify(queue: .main, execute: {
                                
                                
                                self.recordsArray.removeAllObjects()
                                
                                self.loadData()
                                
                                DispatchQueue.main.async {
                                    
                                    SVProgressHUD.dismiss()
                                    
                                }
                                
                            })
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    //MARK:- Add Item To Cart API
    @objc func AddItem(sender: UIButton)
    {
        
        let tempDictionary = recordsArray[sender.tag] as! NSDictionary
        print("sendertag===",sender.tag)
        
        Productid1 = Int("\(tempDictionary["product_id"]!)")!
//        print("Product_Id",Productid1)
        
        Cartid1 = Int("\(tempDictionary["cart_id"]!)")!
//        print("Cart_Id",Cartid1)
        
        
        tempQuantity = Int("\(tempDictionary["quantity"]!)")!
//        print("Print tempQuantity=====",tempQuantity)
        productQuantity = tempQuantity + 1
        print("ProductQuantity=======",productQuantity!)
        self.AddItemButtonAPI()
        print("sender.tag====||===========" , sender.tag)
        
    }
    //MARK:- Add Item To My Cart Page API
    func AddItemButtonAPI()
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_change_cart_qnty")!   //change the url
        
        print("add Item in Add Cart For Same Product(Quantity Change) : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&product_id=\(Productid1!)&lang_id=\(langID!)&quantity=\(productQuantity!)&cartid=\(Cartid1!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Add More Item To Cart Details: " , json)
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            self.recordsArray.removeAllObjects()
                            self.loadData()
                            SVProgressHUD.dismiss()
                            
                        }
                    }
                    else
                    {
                        //DOn't move
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    //MARK:- Delete Cart Selector
    @objc func DeleteItem(sender: UIButton)
    {
        let tempDictionary = recordsArray[sender.tag] as! NSDictionary
        
        Productid2 = Int("\(tempDictionary["product_id"]!)")!
        Cartid2 = Int("\(tempDictionary["cart_id"]!)")!
        
        
        tempQuantity1 = Int("\(tempDictionary["quantity"]!)")!
        print("Print tempQuantity=====",tempQuantity1)
        prodcutQuantity1 = tempQuantity1 - 1
        
        if    tempQuantity1 == 1
        {
            let alert = UIAlertController(title: "Warning", message: "Count can not be less than one", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.DeleteItemButtonAPI()
        }
        
    }
    //MARK:- Delete Item From My Cart Page
    
    func DeleteItemButtonAPI()
    {
        self.dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        let url = URL(string: GLOBALAPI + "app_change_cart_qnty")!   //change the url
        
        print("Remove from My Cart URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&product_id=\(Productid2!)&lang_id=\(langID!)&quantity=\(prodcutQuantity1!)&cartid=\(Cartid2!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
       
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Delete Items From Cart Details: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            self.recordsArray.removeAllObjects()
                            self.loadData()
                            SVProgressHUD.dismiss()
                            
                        }
                    }
                    else
                    {
                        //DOn't move
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    //MARK: - Set Font For Labels & TextFields
    func SetFont()
    {
        self.Lbl1.font = UIFont(name: self.Lbl1.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl2.font = UIFont(name: self.Lbl2.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl3.font = UIFont(name: self.Lbl3.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl4.font = UIFont(name: self.Lbl4.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Price.font = UIFont(name: self.Price.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.item_price.font = UIFont(name: self.item_price.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.delivery_charge.font = UIFont(name: self.delivery_charge.font!.fontName, size: 14)
        self.total_price.font = UIFont(name: self.total_price.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
    }
}

class AddToCartTableViewCell: UITableViewCell {

    @IBOutlet var name_lbl: UILabel!
    
    @IBOutlet var Product_image: UIImageView!
    
    @IBOutlet var DeleteCart_btn: UIButton!
    
    @IBOutlet var seller_name_lbl: UILabel!
    
    @IBOutlet weak var Feature_Price: UILabel!
    
    @IBOutlet var item_count: UILabel!
    
    @IBOutlet var removeCart_btn: UIButton!
    
    @IBOutlet var addToCart_btn: UIButton!
    
    @IBOutlet var offer_price: UILabel!
    
    @IBOutlet var start_price: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
}
}
