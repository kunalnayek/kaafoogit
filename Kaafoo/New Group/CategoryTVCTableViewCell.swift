//
//  AnimalsTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 16/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class CategoryTVCTableViewCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var bidCount: UILabel!
    @IBOutlet weak var bidFlag: UIImageView!
    @IBOutlet weak var closesIn: UILabel!
    @IBOutlet weak var businessView: UIView!
    @IBOutlet weak var businessLabel: UILabel!
    @IBOutlet weak var verifiedImage: UIImageView!
    @IBOutlet weak var kaafooLogo: UIImageView!
    @IBOutlet weak var dailyDealsImageview: UIImageView!
    @IBOutlet weak var dailyDealsLBL: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var placeholderImage: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var buyNowLabel: UILabel!
    @IBOutlet weak var buyNowPrice: UILabel!
    @IBOutlet weak var reservePrice: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var watchListOutlet: UIButton!
    @IBOutlet weak var removeFromWatchlistOutlet: UIButton!
    @IBOutlet weak var listingTime: UILabel!
    
    @IBOutlet weak var refundTypeLBL: UILabel!
    
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
