//
//  NewDailyDealsInfoTableViewCell.swift
//  Kaafoo
//
//  Created by Kaustabh on 23/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class NewDailyDealsInfoTableViewCell: UITableViewCell {
    @IBOutlet var parameter_lbl: UILabel!
    @IBOutlet var detail_lbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
