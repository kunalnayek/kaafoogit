//
//  ListingDetailsCollectionViewCell.swift
//  Kaafoo
//
//  Created by Kaustabh on 26/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class ListingDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var Listing_img: UIImageView!
    @IBOutlet var product_name: UILabel!
    
}

