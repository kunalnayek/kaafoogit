//
//  ReviewTableViewCell.swift
//  Kaafoo
//
//  Created by Kaustabh on 28/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet var total_rating: UILabel!
    
    @IBOutlet var name_lbl: UILabel!
    @IBOutlet var rating_no_lbl: UILabel!
    @IBOutlet var comment_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    
    @IBOutlet var review_View: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

