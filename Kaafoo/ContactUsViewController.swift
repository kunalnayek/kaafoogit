//
//  ContactUsViewController.swift
//  Kaafoo
//
//  Created by priya on 01/06/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContactUsViewController: GlobalViewController {
    
    
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var nameTXT: UITextField!
    @IBOutlet weak var contactLBL: UILabel!
    @IBOutlet weak var contactTXT: UITextField!
    @IBOutlet weak var emailLBL: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var commentTXT: UITextField!
    @IBOutlet weak var submitBTN: UIButton!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
      
        
        
    }
    
    @IBAction func submitBTNclicked(_ sender: Any)
    {
        
        
        if (nameTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Required Field", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (contactTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Required Field", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (emailTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Required Field", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if isValidEmail(emailStr: self.emailTXT.text ?? "") == false
        {
            let alert = UIAlertController(title: "Warning", message: "Your Email is not in Correct Format", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (commentTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Required Field", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        
        else
        {
        self.contactUsAPI()
        }
        
        
    }
    
    
    func contactUsAPI()
    {
        
//   lang_id
//   name
//         contact_no
//         email
//         comments
//         mycountry_id
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        let CountryId = UserDefaults.standard.string(forKey: "countryID")
        
        parameters = "lang_id=\(langID ?? "")&name=\(nameTXT.text!)&contact_no=\(contactTXT.text!)&email=\(emailTXT.text!)&comments=\(commentTXT.text!)&mycountry_id=\(CountryId!)"
        
        self.CallAPI(urlString: "app_contactus_info", param: parameters) {
            
            
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
                let responseMessage = "\(self.globalJson["message"]!)"
                self.ShowAlertMessage(title: "warning", message: responseMessage)
              
            }
            
        }
    }
    

   

}
