//
//  SectionView.swift
//  CollapsibleTBLVIEW
//
//  Created by admin on 18/07/18.
//  Copyright © 2018 esolz. All rights reserved.
//

import UIKit

protocol sectionDelegate {
    func callSection(idx: Int)
}


class SectionView: UIView {

    var secIndex : Int?
    var delegate : sectionDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(btn)
        self.addSubview(titleLBL)
        self.addSubview(plusImage)
        self.addSubview(barImage)
         self.addSubview(countLBL)
        // self.addSubview(countView)

        self.plusImage.anchor(top: nil, leading: nil, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 15), size: .init(width: 10, height: 10))
        self.plusImage.centerYAnchor.constraint(equalTo: self.btn.centerYAnchor).isActive = true

        self.titleLBL.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0))

        self.btn.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, size: .init(width: 0, height: (30/568)*UIScreen.main.bounds.size.height))

        self.barImage.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 4, height: 0))
        
        self.countLBL.anchor(top: nil, leading: self.titleLBL.trailingAnchor, bottom: nil, trailing:nil, padding: .init(top: 0, left: 10, bottom: 0, right: 15), size: .init(width: 25, height: 25))
               self.countLBL.centerYAnchor.constraint(equalTo: self.btn.centerYAnchor).isActive = true
        
       // self.countView.anchor(top: nil, leading: self.titleLBL.trailingAnchor, bottom: nil, trailing:nil, padding: .init(top: 0, left: 0, bottom: 0, right: 15), size: .init(width: 15, height: 15))
        //self.countView.centerYAnchor.constraint(equalTo: self.btn.centerYAnchor).isActive = true

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK:- // Defining The TitleLabel

    lazy var titleLBL: UILabel = {
        let titleLBL = UILabel()
        titleLBL.backgroundColor = UIColor.white
        titleLBL.textAlignment = .natural
        //titleLBL.contentHorizontalAlignment = .left
        titleLBL.clipsToBounds = true
        //titleLBL.addTarget(self, action: #selector(onClickSectionView), for: .touchUpInside)
        return titleLBL
    }()

    // MARK:- // Defining The Button

    lazy var btn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        //titleLBL.contentHorizontalAlignment = .left
        btn.clipsToBounds = true
        btn.addTarget(self, action: #selector(onClickSectionView), for: .touchUpInside)
        return btn
    }()


    // MARK:- // Defining The ImageView



    lazy var plusImage: UIImageView = {

        let imageName = "plus"
        let plusImage = UIImage(named: imageName)
        let plusImageView = UIImageView(image: plusImage)
        plusImageView.contentMode = .scaleAspectFit

        return plusImageView

    }()
    
    // MARK:- // Defining The countLBL

       lazy var countLBL: UILabel = {
           let countLBL = UILabel()
           countLBL.backgroundColor = UIColor.red
           //countLBL.textAlignment = .natural
          
           
        countLBL.textColor = .white
           //countLBL.clipsToBounds = true
         
           return countLBL
       }()
    
    // MARK:- // Defining The countView

    lazy var countView: UIView = {
        let countView = UIView()
        countView.backgroundColor = UIColor.blue
        //countLBL.textAlignment = .natural
         countView.layer.cornerRadius = countView.frame.size.height/2
        // countView.layer.masksToBounds = true
        //countLBL.clipsToBounds = true
      
        return countView
    }()

    // MARK:- // Defining The ImageView



    lazy var barImage: UIImageView = {

        let imageName = "bar"
        let barImage = UIImage(named: imageName)
        let barImageView = UIImageView(image: barImage)
        barImageView.contentMode = .scaleToFill
        //barImageView.frame = CGRect(x: 0, y: self.frame.origin.y, width: 4, height: 15)
        return barImageView

    }()



    //    // MARK:- // Defining The sectionSideBar
    //
    //    var FullHeight = (UIScreen.main.bounds.size.height)
    //    var FullWidth = (UIScreen.main.bounds.size.width)
    //
    //    lazy var sectionSideBar : UIView = {
    //
    //        let sectionSideBar = UIView(frame: CGRect(x: 0, y: 0, width: ((4/320)*FullWidth), height: ((18/320)*FullHeight)))
    //        sectionSideBar.backgroundColor = UIColor.white
    //
    //        return sectionSideBar
    //
    //    }()


    // MARK:- // On Click Sectiionview

    @objc func onClickSectionView() {

        if let idx = secIndex {
            delegate?.callSection(idx: idx)
        }
    }

}

