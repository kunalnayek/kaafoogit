//
//  CategoryCell.swift
//  Kaafoo
//
//  Created by admin on 17/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var cellimagebottomcons: NSLayoutConstraint!
    @IBOutlet weak var cellimageleadingcons: NSLayoutConstraint!
    @IBOutlet weak var cellimageheightcons: NSLayoutConstraint!
    
    @IBOutlet weak var cellimagewidthcons: NSLayoutConstraint!
    
    @IBOutlet weak var celltitletopcons: NSLayoutConstraint!
    
    @IBOutlet weak var celltitlebottomcons: NSLayoutConstraint!
    
    @IBOutlet weak var cellimagetopcons: NSLayoutConstraint!
    
    @IBOutlet weak var MAROOFlbl: UILabel!
    
    @IBOutlet weak var marooflblbottomcons: NSLayoutConstraint!
    
    
    @IBOutlet weak var newimage: UIImageView!
    @IBOutlet weak var newtitle: UILabel!
    @IBOutlet weak var newtitletopconstraints: NSLayoutConstraint!
    

    @IBOutlet weak var celltitleleadingconstraints: NSLayoutConstraint!
    
    @IBOutlet weak var cellimage: UIImageView!
    
    @IBOutlet weak var cellTitleLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
