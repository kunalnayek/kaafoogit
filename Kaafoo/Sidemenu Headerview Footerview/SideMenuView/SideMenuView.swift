//
//  SideMenuView.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

protocol Side_menu_delegate: class {
    
    func action_method(sender: NSInteger?)
    
    func myAccount_section(sender: NSInteger?)
    
    func buying_section(sender: NSInteger?)
    
    func selling_section(sender: NSInteger?)
    
    func job_section(sender: NSInteger?)
    
    func isGuest()
    
    func sideMenuButtons(sender: NSInteger?)
    
    func categoryDidSelect(typeID : String , categoryID : String , categoryName : String )
    
    func servicesDidselect(typeID : String, categoryName: String)
    
    func dailyDealsDidSelect()
    
    func HappeningDidSelect()
    
    func foodCourtDidSelect(typeID: String)
    
    func foodCourtdidSelect()
    
    func holidayAccomodationDidSelect()
    
    func dailyRentalDidSelect(productID : String)
    
    func RealEstateDidSelect()
    
    func jobDidSelect()
    
    func ClassifiedDidSelect()
    
    func NearBy()
    
    func myAccount_marketplace_Section(sender : NSInteger?)
    
    func myAccount_dailyRental_Section(sender : NSInteger?)
    
    func myAccount_Happening_Section(sender : NSInteger?)
    
    func myAccount_HolidayAccommodation_Section(sender : NSInteger?)
    
    func myAccount_FoodCourt_Section(sender : NSInteger?)
    
    func myAccount_Commercial_Section(sender : NSInteger?)
    
    func myAccount_Service_Section(sender : NSInteger?)
    
    func myAccount_Real_Estate_Section(sender : NSInteger?)
    
    func myAccount_Advertsiement_Section(sender :  NSInteger?)
    
    func myAccount_Latest_news()
    
    func myAccount_Query_Section(sender : NSInteger?)
    
    func chat_Section(sender : NSInteger?)
    
    func myAccount_account_wallet_Section(sender : NSInteger?)
    
    func myAccount_notification()
    
    func SelectAllCategory()
    
    func Selectservicecategory()
    
    func Selectfoodcourtcategory()
    
    func Selectclassifiedcategory()
    
    func bonusActivation_section(sender: NSInteger?)
    
    func forthTable_action(sender : NSInteger?)
}


class SideMenuView: UIView {
    
    var notificationcount : NSNumber!
     let globalDispatchgroup = DispatchGroup()
      var globalJson : NSDictionary!
     var allDataDictionary : NSDictionary!
    var globaljson : NSDictionary!
    
    let sectionview : SectionView = {
        let sectionView = SectionView()
        sectionView.translatesAutoresizingMaskIntoConstraints = false
        return sectionView
    }()
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var menuView: UIView!
    
    @IBOutlet weak var userDisplayName: UILabel!
    
    @IBOutlet weak var homeView: UIView!
    
    @IBOutlet weak var homeLBL: UILabel!


    // MARK:- // Home Button
    @IBOutlet weak var homeBTN: UIButton!
    
    @IBAction func homeBTNTap(_ sender: UIButton) {
        
        Side_delegate?.sideMenuButtons(sender: sender.tag)
    }

    @IBOutlet weak var ForthTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ForthTableView: UITableView!
    
    @IBOutlet weak var sideMenuCategoryTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var collapsibleTableHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var collapsibleTableview: UITableView!
    
    @IBOutlet var closeSideMenu: UIButton!

    @IBOutlet weak var sideMenuCategoryTable: UITableView!

    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var insideCategoryView: UIView!
    
    @IBOutlet weak var backToMainMenuIMage: UIImageView!
    
    @IBOutlet weak var backToMainMenuLBL: UILabel!
    
    @IBOutlet weak var categoryBarImage: UIImageView!
    
    @IBOutlet weak var categoryNameLBL: UILabel!
    
    @IBOutlet weak var CategoryNameBtn: UIButton!
    
    @IBOutlet weak var insideCategoryTableview: UITableView!

    @IBOutlet weak var upperActivityView: UIView!
    
    @IBOutlet weak var loginImageView: UIImageView!
    
    @IBOutlet weak var loginLBL: UILabel!
    
    @IBOutlet weak var registerImageVIew: UIImageView!
    
    @IBOutlet weak var registerLBL: UILabel!
    
    @IBOutlet weak var snapchatImageView: UIImageView!
    
    @IBOutlet weak var snapchatLBL: UILabel!

    @IBOutlet weak var nearbyAndPostAdView: UIView!
    
    @IBOutlet weak var nearbyLBL: UILabel!

    @IBOutlet weak var userView: UIView!
    
    @IBOutlet weak var userImageview: UIImageView!
    
    @IBOutlet weak var guestView: UIView!
    
    @IBOutlet weak var guestImageview: UIImageView!
    
    @IBOutlet weak var tapOnProfile: UIButton!

    @IBOutlet weak var loginAndRegisterView: UIView!

    @IBOutlet weak var snapchatAndQrView: UIView!

    @IBOutlet weak var upperActivityViewSeparator: UIView!

    @IBOutlet weak var scanQRCodeLBL: UILabel!


    
    weak var SideBtnDelegate : SideButtonDelegate?

    weak var Side_delegate: Side_menu_delegate?

    let dispatchGroup = DispatchGroup()

    var allCategoriesArray = NSArray()

    var allSubcategoriesArray = [AnyObject]()

    var FullWidth = UIScreen.main.bounds.size.width
    
    var FullHeight = UIScreen.main.bounds.size.height

    var FoodCourtArray : NSMutableArray! = NSMutableArray()

    var categoryID : String! = ""

    var categoryType : String! = ""
    
    var  str : String! = ""


    let langID = UserDefaults.standard.string(forKey: "langID")

    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

    let languageName = UserDefaults.standard.string(forKey: "myLanguage")

    var ForthTableArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "About Kaafoo", comment: ""),
    LocalizationSystem.sharedInstance.localizedStringForKey(key: "Kaafoo Bonus", comment: ""),
    LocalizationSystem.sharedInstance.localizedStringForKey(key: "Contact Us", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Help", comment: ""),
  LocalizationSystem.sharedInstance.localizedStringForKey(key: "Report", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Terms and Conditions", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Privacy Policy", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "About us", comment: ""),
    LocalizationSystem.sharedInstance.localizedStringForKey(key: "SOCIAL MEDIA", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Instagram", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Youtube", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "SnapChat", comment: ""),
    LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share with friends", comment: ""),
   LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rate us on App store", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "MAROOF", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "MAROOFimg", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "kaafoo logo", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "kaafoo logoimg", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Copyright", comment: "")
        ,LocalizationSystem.sharedInstance.localizedStringForKey(key: "Logout", comment: "")
    ]

    var data = [
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_account", comment: ""), subType:
            //[LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: ""),
                                                                                                                      [LocalizationSystem.sharedInstance.localizedStringForKey(key: "View own profile", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "Edit Account", comment: ""),
                                                                                                                               
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "Add Category", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "verification_file", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "User_information", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "Social media", comment: "")],isExpandable: false),
                                                                                                                               
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "Latest news", comment: ""),
                                                                                                                               
                                                                                                                               
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "Text_advertisement", comment: ""),
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_advertisement", comment: ""),
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "Restaurant_feature", comment: ""),
          //  LocalizationSystem.sharedInstance.localizedStringForKey(key: "Users service_feature", comment: ""),
        //],isExpandable: false),
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "category_section", comment: "")],isExpandable: false),
                                                                                                                               //LocalizationSystem.sharedInstance.localizedStringForKey(key: "holiday_features", comment: "")], isExpandable: false),
        
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_interests", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "watchlist", comment: ""),
                                                                                                                                 
                                                                                                                                 LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_favourites", comment: ""),
                                                                                                                                 LocalizationSystem.sharedInstance.localizedStringForKey(key: "recently_viewed", comment: "")
        ], isExpandable: false),
        
        //MARK:- //My Account Latest News Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: "")], isExpandable: false),
        
        //End
        
        //MARK:- //Bonus Activation
               
               //Start
               
               dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Bonus Activation", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Bonus Activation", comment: "")], isExpandable: false),
               
               //End
        
        //MY Account Notification Section
               
               dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")], isExpandable: false),
               
        
        //MARK:- My Account Marketplace Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_am_selling", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_I_won", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_lost", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "sold_items", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "unsold_items", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "buy_now_invoices", comment: "")
        ], isExpandable: false),
        
        //End
        
        
        //MARK:- //My Account Real Estate Section
               
               //Start
               
               dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Real Estate", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_real_estate", comment: "")], isExpandable: false),
               
               //End
        
        
        //MARK:- //My Account Daily Rental section
              
              //Start
              
              dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Rental", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_daily_rental", comment: ""),
                                                                                                                                       LocalizationSystem.sharedInstance.localizedStringForKey(key: "Booking Information", comment: ""),
                                                                                                                                       LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingItem", comment: "")
              ], isExpandable: false),
              
              
              //End
               
        
        //MARK:- My Account Happening Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "happening", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_happening", comment: ""),
                                                                                                                              LocalizationSystem.sharedInstance.localizedStringForKey(key: "event_booking", comment: ""),
                                                                                                                              LocalizationSystem.sharedInstance.localizedStringForKey(key: "event_item_booking", comment: "")
        ], isExpandable: false),
        
        //End
        
        
      
        
        //MARK:- //My Account Holiday Accommodation section
        
        //start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Holiday Accomodation", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_accommodation", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidayBooking", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidaybookinginformation", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "Users Holiday Features", comment: "")
        ], isExpandable: false),
        
        //End
        
        //MARK:- //My Account Service Section
        
        //Start
        
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Services", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_services", comment: ""),
                                                                                                                             LocalizationSystem.sharedInstance.localizedStringForKey(key: "booked_services", comment: ""),
                                                                                                                             LocalizationSystem.sharedInstance.localizedStringForKey(key: "service_invoices", comment: ""),
                                                                                                                             LocalizationSystem.sharedInstance.localizedStringForKey(key: "users_holiday_services", comment: "")
        ], isExpandable: false),
        
        //End
        
        //MARK :- //My Account Food Court Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Food Court", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "food-list", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "food_court_orders", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "food_order_items", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "users_restaurent_features", comment: "")], isExpandable: false),
        
        //End
        
        //MARK :- //My Account Commercial Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial Advertisement", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial ads", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial ads list", comment: "")], isExpandable: false),
        
        //End
        
        
       
        
        /*
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "selling", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "List an Item", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_am_selling", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "sold_items", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "unsold_items", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingItem", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceItem", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "bonusRequest", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodList", comment: ""),
                                                                                                                            
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventBooking", comment: ""),
                                                                                                                            
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidayBooking", comment: "")], isExpandable: false),
         
         
         
        
        */
        
        //Text Advertisement Section
               
               dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_advertisement", comment: "")], isExpandable: false),
               
               
               dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobSection", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobPost", comment: ""),
                                                                                                                                             LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobPostList", comment: "")], isExpandable: false),
               
               
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Account Wallet", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "credit_charging", comment: ""),
                                                                                                                                   LocalizationSystem.sharedInstance.localizedStringForKey(key: "kaafooBonusSection", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "wallet_refund", comment: "")], isExpandable: false),
       
        
        //Commercial Advertisement List
        
        //        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement_list", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement_list", comment: "")], isExpandable: false),
        
        
        
       
        //MARK:- //Query Section My Account Section
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "query", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_query", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "job_query", comment: "")], isExpandable: false),
        
        //MY Chat Section
                      
       dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "chat", comment: "")], isExpandable: false),
             
       
        
         //dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "logout", comment: ""), subType: [], isExpandable: false)
        
       // dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: ""), subType: [], isExpandable: false),
       // dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: ""), subType: [], isExpandable: false),
        
       
        
        
    ]

    // MARK:- // Override Init

    override init(frame: CGRect) {
        super.init(frame: frame)

    }


    // MARK:- // Required Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        Bundle.main.loadNibNamed("SideMenuView", owner: self, options: nil)
        
        addSubview(contentView)
        
        contentView.frame = self.bounds
        
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        let cellIdentifier = "SideViewCell"
        
        let nibName = UINib(nibName: "SideViewCell", bundle:nil)
        
        self.collapsibleTableview.register(nibName, forCellReuseIdentifier: cellIdentifier)

        let categoryCellIdentifier = "SideViewCategoryCell"
        
        let categoryNibName = UINib(nibName: "SideViewCategoryCell", bundle:nil)
        
        self.sideMenuCategoryTable.register(categoryNibName, forCellReuseIdentifier: categoryCellIdentifier)

        let identifier = "CategoryCell"
        
        let detailsNibName = UINib(nibName: "CategoryCell", bundle: nil)
        
        self.insideCategoryTableview.register(detailsNibName, forCellReuseIdentifier: identifier)
        
        self.ForthTableView.register(detailsNibName, forCellReuseIdentifier: identifier)
        
        

        self.fetchData()
        //
        self.changeLanguageStrings()

//        self.SetCountryChecking()

        self.postAdButtonOutlet.layer.cornerRadius = self.postAdButtonOutlet.frame.size.height / 2

        self.sideMenuCategoryTable.estimatedRowHeight = 70
        
        self.sideMenuCategoryTable.rowHeight = UITableView.automaticDimension

        self.insideCategoryTableview.estimatedRowHeight = 70
        
        self.insideCategoryTableview.rowHeight = UITableView.automaticDimension

        self.collapsibleTableview.estimatedRowHeight = 70
        
        self.collapsibleTableview.rowHeight = UITableView.automaticDimension
        
        self.ForthTableView.estimatedRowHeight = UITableView.automaticDimension
        
        //self.ForthTableView.backgroundColor = .lightGray
        
        
        
       
        
    }
    
    // MARK:- // Change Language Strings
    
    func changeLanguageStrings() {
        self.homeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: "")
        
        self.loginLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")
        
        self.registerLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Register", comment: "")
        
        self.snapchatLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "snapChat", comment: "")
        
        self.scanQRCodeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "scanqrcode", comment: "")
        
        self.nearbyLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
        
        self.postAdButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_ad", comment: ""), for: .normal)
        
    }


    func SetCountryChecking()
    {
        if langID == nil
        {
          data[8].headerName = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")
        }
        else
        {
          data[8].headerName = langID
        }
    }



    // MARK:- // Buttons


    // MARK:- // Back to Main Menu Button

    @IBAction func tapOnBackToMainMenu(_ sender: UIButton) {

        self.insideCategoryView.isHidden = true
        self.menuView.sendSubviewToBack(self.insideCategoryView)


    }



    // MARK:- // Login Button

    @IBAction func tapOnLogin(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Register Button

    @IBAction func tapOnRegister(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }



    // MARK:- // Snapchat Button

    @IBAction func tapOnSnapchat(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Scan QR Button

    @IBAction func tapOnScanQR(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Nearby Button

    @IBAction func tapOnNearby(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Post Ad Button

    @IBOutlet weak var postAdButtonOutlet: UIButton!
    @IBAction func tapOnPostAd(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }

    func fetchData(){
        let myCountryName = "\(UserDefaults.standard.string(forKey: "myCountryName") ?? "")"
        let preMyCountryName = "\(UserDefaults.standard.string(forKey: "PremyCountryName") ?? "SS")"
        let userID = UserDefaults.standard.string(forKey: "userID")
         print("userID========::::",userID)
        print("preMyCountryName========",preMyCountryName)
        
        
        if myCountryName != preMyCountryName{
            
//            if userID?.isEmpty == true
//            {
//                print("userid is blank")
//            }
//            else
 //           {
                 fetchDataApi()
  //          }
           
        }
        else
        {
            
            
            if GlobalDataClass.shared.allCategoryData == nil{
                fetchData()
            }
            else{
               self.allCategoriesArray = GlobalDataClass.shared.allCategoryData
            }
            DispatchQueue.main.async {
                self.sideMenuCategoryTable.delegate = self

                self.sideMenuCategoryTable.dataSource = self

                self.sideMenuCategoryTable.reloadData()
            }
        }
        
    }




    // MARK: - // JSON Get Method to get Category Data from Web
    
    
    func fetchDataApi(){
        self.dispatchGroup.enter()
        
        let CountryId = UserDefaults.standard.string(forKey: "countryID")

        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        
        UserDefaults.standard.set(myCountryName, forKey: "PremyCountryName")
        
        
        let LangID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        print("userodtestapi====",userID)
        
        
        if Connectivity.isConnectedToInternet() {
            let encodedString = myCountryName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            print("encodedString====",encodedString!)
            
            let urlstr = "https://kaafoo.com/appcontrol/app_category_list"

            print("urlStr:\(urlstr)")
//            var parameters: [String: Any]?
//
//                parameters = [
//                    "mycountry_id" : "\(encodedString!)",
//                    "lang_id" : "\(LangID!)",
//                    "user_id" : "\(userID ?? "")"
//                ]
            
            var parameters : [String : String] = [:]

            parameters["user_id"] = "\(userID ?? "")"
            parameters["mycountry_id"] = "\(encodedString!)"
            parameters["lang_id"] =  "\(LangID!)"
            
            // let parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(187)"
            
            print("url---------",urlstr)
            print("parameters---------",parameters as Any)
            
            print("URL ====: ", "\(urlstr)" + "?" + "\(parameters)")


            let headersStr: [String:Any] = [
                :
            ]
            SVProgressHUD.show()

            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120

            Alamofire.request(urlstr, method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: headersStr as? HTTPHeaders)
                .responseJSON { response in

                    switch (response.result)
                    {
                    case .success :

                        let responseDict = (response.result.value as! NSDictionary)
                        print("Data..: \(responseDict)")

                        //let status = "\(responseDict["response"] ?? "")"

                        if (responseDict["response"] as! Bool) == true{

                            self.allCategoriesArray = responseDict["info_categories"] as! NSArray
                            GlobalDataClass.shared.setUserData(category_data: self.allCategoriesArray)
                            print("AllCategories Count--------",self.allCategoriesArray.count)
                            
                            //self.notificationstr = responseDict["notification_count"] as? NSString
                            
                           // print("notificationstring------",self.notificationstr)

                            DispatchQueue.main.async {

                                self.dispatchGroup.leave()
                                SVProgressHUD.dismiss()

                                self.sideMenuCategoryTable.delegate = self

                                self.sideMenuCategoryTable.dataSource = self

                                self.sideMenuCategoryTable.reloadData()

                                
                            }

                        }
                        else
                        {
                            print("Do nothing============")

                            DispatchQueue.main.async {

                                self.dispatchGroup.leave()

                            }
                        }

                    case .failure(let error):

                        SVProgressHUD.dismiss()

                        if error._code == NSURLErrorTimedOut
                        {
                            //SVProgressHUD.dismiss()
                        }
                        print("\n\nAuth request failed with error:\n \(error)")
                        break

                    }

            }
        }
        else
        {
            print("Network Not available")
        }

    }
    
   
    
    
   
        

    
   /* func fetchDataApi()
        
     {
        
            self.dispatchGroup.enter()
        
           let GLOBALAPI = "https://kaafoo.com/appcontrol/"
        
        
               let CountryId = UserDefaults.standard.string(forKey: "countryID")

               let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
               UserDefaults.standard.set(myCountryName, forKey: "PremyCountryName")
               
               
               let LangID = UserDefaults.standard.string(forKey: "langID")
               
               let userID = UserDefaults.standard.string(forKey: "userID")
               
       
        
        let encodedString = myCountryName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        print("encodedString====",encodedString!)
        
       
      

//             self.dispatchGroup.enter()
//             SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        
             let url = URL(string: GLOBALAPI + "app_category_list")!   //change the url

             var parameters : String = ""

              parameters = "user_id=\(userID!)&lang_id=\(LangID!)&mycountry_id=\(encodedString!)"

             print("url======= : ",url)
             
             print("Parameters are--------======: " , parameters)

             let session = URLSession.shared

             var request = URLRequest(url: url)

             request.httpMethod = "POST" //set http method as POST

             do {
                 request.httpBody = parameters.data(using: String.Encoding.utf8)
             }

             let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                 guard error == nil else {
                     return
                 }

                 guard let data = data else {
                     return
                 }

                 do {

                     if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        
                         print("Response=============: " , json)
                        

                         if (json["response"] as! Bool) == true
                         {
                            
                           
                            self.allCategoriesArray = json["info_categories"] as! NSArray
                             GlobalDataClass.shared.setUserData(category_data: self.allCategoriesArray)
                             print("AllCategories Count--------",self.allCategoriesArray.count)
                             
                             self.notificationcount = json["notification_count"] as? NSNumber
                             
                             print("notificationcount------",self.notificationcount)
                            
                            if  userID?.isEmpty == true
                            {
                                
                                print("no userid==")
                                
                            }
                            
                            else
                            {
                             
                           let number : NSNumber = self.notificationcount
                            self.str = number.stringValue
                            
                            UserDefaults.standard.set(self.str, forKey: "notificationcount")
                            
                            }
                             DispatchQueue.main.async {

                                 self.dispatchGroup.leave()
                                
                                SVProgressHUD.dismiss()
                                
                                

                                self.sideMenuCategoryTable.delegate = self

                                self.sideMenuCategoryTable.dataSource = self

                                self.sideMenuCategoryTable.reloadData()

                             }

                         }

                         else
                         {
                             print("Do nothing")

                             DispatchQueue.main.async {

                                 self.dispatchGroup.leave()

                             }
                         }

                     }

                 } catch let error {
                     print(error.localizedDescription)
                 }
             })

             task.resume()
         }
     
    
*/


    // MARK: - // JSON Get Method to get inside Category Data from Web
    func InsideCategoryFetchData(){
        
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        let preMyCountryName = UserDefaults.standard.string(forKey: "PremyCountryName")
        if myCountryName != preMyCountryName{
            InsideCategoryFetchDataAPI()
        }
        else{
            self.allCategoriesArray = GlobalDataClass.shared.allCategoryData
            let infoArray = self.allCategoriesArray
            //print("InfoArray=\\==",infoArray)

            for i in 0..<infoArray.count
            {
                if "\((infoArray[i] as! NSDictionary)["id"]!)".elementsEqual(self.categoryID)
              {
                let DemoArray = ((infoArray[i] as! NSDictionary)["child_cat"]! as! NSArray)
                //print("demoArray==",DemoArray)
                self.allSubcategoriesArray = DemoArray as [AnyObject]
                
                print("AllsubcategoriesArray------",self.allSubcategoriesArray)
                
                
              }
                else
              {
                print("Do Nothing")
              }
            }

            DispatchQueue.main.async {

                //self.dispatchGroup.leave()

                self.insideCategoryTableview.delegate = self
                self.insideCategoryTableview.dataSource = self
                self.insideCategoryTableview.reloadData()
                
                
                SVProgressHUD.dismiss()
            }
        }
        
    }
    
    // MARK: - // JSON Get Method to get inside Category Data from Web
    func InsideCategoryFetchDataAPI(){
        
        self.dispatchGroup.enter()
        let CountryId = UserDefaults.standard.string(forKey: "countryID")
        
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        UserDefaults.standard.set(myCountryName, forKey: "PremyCountryName")
        let LangID = UserDefaults.standard.string(forKey: "langID")
               
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        if Connectivity.isConnectedToInternet() {
            let encodedString = myCountryName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let urlstr = "https://kaafoo.com/appcontrol/app_category_list"
            
            print("urlStr:\(urlstr)")
            var parameters: [String: Any]?
            
            parameters = [
                "mycountry_id" : "\(encodedString!)",
                "lang_id" : "\(LangID!)",
                "user_id" : "\(userID ?? "")"
            ]
            
            print("url---------",urlstr)
            print("parameters---------",parameters)
            
            
            let headersStr: [String:Any] = [
                :
            ]
            SVProgressHUD.show()
            
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            Alamofire.request(urlstr, method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: headersStr as? HTTPHeaders)
                .responseJSON { response in
                    
                    switch (response.result)
                    {
                    case .success :
                        
                        let responseDict = (response.result.value as! NSDictionary)
                        print("Data..: \(responseDict)")
                        
                        //let status = "\(responseDict["response"] ?? "")"
                        
                        if (responseDict["response"] as! Bool) == true{
                            
                            let infoArray = responseDict["info_categories"] as! NSArray
                            //print("InfoArray=\\==",infoArray)
                            
                            for i in 0..<infoArray.count
                            {
                                if "\((infoArray[i] as! NSDictionary)["id"]!)".elementsEqual(self.categoryID)
                                {
                                    let DemoArray = ((infoArray[i] as! NSDictionary)["child_cat"]! as! NSArray)
                                    //print("demoArray==",DemoArray)
                                    self.allSubcategoriesArray = DemoArray as [AnyObject]
                                    
                                    print("AllsubcategoriesArray------",self.allSubcategoriesArray)
                                    
                                    
                                }
                                else
                                {
                                    print("Do Nothing")
                                }
                            }
                            
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                                self.insideCategoryTableview.delegate = self
                                self.insideCategoryTableview.dataSource = self
                                self.insideCategoryTableview.reloadData()
                                
                                
                                SVProgressHUD.dismiss()
                            }
                            
                        }
                        else
                        {
                            print("Do nothing")
                            
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                            }
                        }
                        
                    case .failure(let error):
                        
                        SVProgressHUD.dismiss()
                        
                        if error._code == NSURLErrorTimedOut
                        {
                            //SVProgressHUD.dismiss()
                        }
                        print("\n\nAuth request failed with error:\n \(error)")
                        break
                        
                    }
                    
            }
        }
        else
        {
            print("Network Not available")
        }
    }

//    func InsideCategoryFetchData()
//    {
//
//        dispatchGroup.enter()
//
//        DispatchQueue.main.async {
//            SVProgressHUD.show()
//        }
//       let CountryId = UserDefaults.standard.string(forKey: "countryID")
//
//
//        let myCountryNameOne = UserDefaults.standard.string(forKey: "myCountryName")
//
//        let encodedCountry = myCountryNameOne?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//
//        var str_url : String! = ""
//
//
//        str_url = "https://kaafoo.com/appcontrol/app_category_list?mycountry_id=\(encodedCountry ?? "")"
//
//        print("Url is : ", str_url)
//
//        if let url = NSURL(string: str_url)
//
//        {
//
//            if let data = try? Data(contentsOf:url as URL)
//
//            {
//
//                do{
//
//                    let dict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
//
//                    //print("Inside Category fetch Data Response : " , dict)
//
//                    //making a deffirent dictionary for JOb in requirement
//
//                    let infoArray = dict["info_categories"] as! NSArray
//                    //print("InfoArray=\\==",infoArray)
//
//                    for i in 0..<infoArray.count
//                    {
//                      if "\((infoArray[i] as! NSDictionary)["id"]!)".elementsEqual(categoryID)
//                      {
//                        let DemoArray = ((infoArray[i] as! NSDictionary)["child_cat"]! as! NSArray)
//                        //print("demoArray==",DemoArray)
//                        self.allSubcategoriesArray = DemoArray as [AnyObject]
//
//                        print("AllsubcategoriesArray------",self.allSubcategoriesArray)
//
//
//                      }
//                        else
//                      {
//                        print("Do Nothing")
//                      }
//                    }
//
//
//
//
//
//
//
//                    DispatchQueue.main.async {
//
//                        self.dispatchGroup.leave()
//
//                        self.insideCategoryTableview.delegate = self
//                        self.insideCategoryTableview.dataSource = self
//                        self.insideCategoryTableview.reloadData()
//
//
//                        SVProgressHUD.dismiss()
//                    }
//
//                }
//
//                catch
//
//                {
//                    print("Error")
//
//                }
//
//            }
//
//        }
//
//    }



    // MARK:- // function for Set Font

    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size

        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }

        return size1
    }
    
    @objc func viewTapped(sender : UITapGestureRecognizer)
    {
        Side_delegate?.SelectAllCategory()
        print("Marketplace is tapped")
    }
    
    @objc func serviceviewTapped(sender : UITapGestureRecognizer)
    {
        Side_delegate?.Selectservicecategory()
        print("service is tapped")
        
        

    }
    
    @objc func foodviewTapped(sender : UITapGestureRecognizer)
    {
         Side_delegate?.Selectfoodcourtcategory()
         print("food is tapped")
    }
    
    
    @objc func classifiedTapped(sender : UITapGestureRecognizer)
    {
         Side_delegate?.Selectclassifiedcategory()
         print("classified is tapped")
    }
    
    @objc func tapOnCategoryCell(_sender: UIButton){
        
        categoryID = "\((allCategoriesArray[_sender.tag] as! NSDictionary)["id"] ?? "")"
        
        //print("Category ID --- ", categoryID)
        
        if categoryID.elementsEqual("11")
        {
            categoryType = "Services"
            
            let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(serviceviewTapped))
            gesture.numberOfTapsRequired = 1
            categoryNameLBL.isUserInteractionEnabled = true
            categoryNameLBL.addGestureRecognizer(gesture)
        }
        else if categoryID.elementsEqual("9")
        {
            categoryNameLBL.textColor = .black
            //                CategoryNameBtn.isHidden = false
            //                CategoryNameBtn.addTarget(self, action: #selector(SideBtnDelegate?.MarketPlace(sender:)), for: .touchUpInside)
            let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
            gesture.numberOfTapsRequired = 1
            categoryNameLBL.isUserInteractionEnabled = true
            categoryNameLBL.addGestureRecognizer(gesture)
            
        }
        else if categoryID.elementsEqual("10")
        {
            categoryType = "None"
            Side_delegate?.dailyDealsDidSelect()
        }
        else if categoryID.elementsEqual("13")
        {
            categoryType = "Food Court"
            //                Side_delegate?.foodCourtDidSelect(typeID: "\(self.FoodCourtArray[indexPath.row])")
            //                Side_delegate?.foodCourtDidSelect()
            
            let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(foodviewTapped))
            gesture.numberOfTapsRequired = 1
            categoryNameLBL.isUserInteractionEnabled = true
            categoryNameLBL.addGestureRecognizer(gesture)
            
        }
        else if categoryID.elementsEqual("16")
        {
            categoryType = "None"
            Side_delegate?.holidayAccomodationDidSelect()
        }
        else if categoryID.elementsEqual("999")
        {
            categoryType = "None"
            Side_delegate?.RealEstateDidSelect()
        }
        else if categoryID.elementsEqual("15")
        {
            categoryType = "None"
            Side_delegate?.dailyRentalDidSelect(productID: categoryID)
        }
        else if categoryID.elementsEqual("12")
        {
            categoryType = "None"
            Side_delegate?.jobDidSelect()
        }
        else if categoryID.elementsEqual("14")
        {
            categoryType = "None"
            Side_delegate?.HappeningDidSelect()
        }
        else if categoryID.elementsEqual("1122")
        {
            categoryType = "None"
            Side_delegate?.ClassifiedDidSelect()
            
            let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(classifiedTapped))
            gesture.numberOfTapsRequired = 1
            categoryNameLBL.isUserInteractionEnabled = true
            categoryNameLBL.addGestureRecognizer(gesture)
            
            
        }
        else
        {
            categoryType = "None"
        }
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if (langID?.elementsEqual("EN"))!
        {
            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
            categoryNameLBL.text = "\((allCategoriesArray[_sender.tag] as! NSDictionary)[categoryName ?? ""] ?? "")"
        }
        else
        {
            categoryNameLBL.text = "\((allCategoriesArray[_sender.tag] as! NSDictionary)["categoryname_ar"]!)"
        }
        
        //back to main menu lbl
        backToMainMenuLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Back to Main Menu", comment: "")
        
        
        
        self.InsideCategoryFetchData()
        
        dispatchGroup.notify(queue: .main) {
            
            self.insideCategoryView.isHidden = false
            self.menuView.bringSubviewToFront(self.insideCategoryView)
            
        }
        
    }
   
    
}



// MARK:- // SEction Delegate

extension SideMenuView : sectionDelegate {


    func callSection(idx: Int) {

        for i in 0..<self.data.count {

            if i == idx {
                self.data[i].isExpandable = !self.data[i].isExpandable
            }
            else {
                self.data[i].isExpandable = false
            }

            self.collapsibleTableview.reloadSections([i], with: .automatic)
        }
        var tempCount: Int!
        if UserDefaults.standard.string(forKey: "isGuest") == "1"{
            tempCount = self.data.count - 2
        }
        else{
            tempCount = self.data.count
        }
        if self.data[idx].isExpandable == true {
            if self.data[idx].subType.count > 0 {
                self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(tempCount)) + ((30/568)*self.FullHeight * CGFloat(self.data[idx].subType.count))//self.collapsibleTableview.visibleCells[0].bounds.height
            }
            else {
                self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(tempCount))
            }

        }
        else {
            self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(tempCount))
        }

        Side_delegate?.action_method(sender: idx)

    }


}



// MARK:- // Tableview Delegates

extension SideMenuView : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == collapsibleTableview
        {
            print("Data set count.....",data.count)
            return data.count
        }
        else
        {
            return 1
        }

    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == collapsibleTableview  // Home / My Account / Buying etc.
        {
            if data[section].isExpandable {
                
                print("sec=====",data[section].subType.count)
                
                return data[section].subType.count
                
            } else {
                return 0
            }
        }
        else if tableView == sideMenuCategoryTable   // Sidemenu Category Table
        {
            return self.allCategoriesArray.count
        }
        else if tableView == ForthTableView
        {
            
            print("fourthtablearray--------",self.ForthTableArray)
            
            return self.ForthTableArray.count
        }
        else   // InsideCategory Table
        {
            return allSubcategoriesArray.count
        }

    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if tableView == collapsibleTableview
        {

            let viewOfHeader = UIView()

            let sectionview : SectionView = {
                let sectionView = SectionView()
                sectionView.translatesAutoresizingMaskIntoConstraints = false
                return sectionView
            }()

            viewOfHeader.addSubview(sectionview)
//            viewOfHeader.backgroundColor = .red

            sectionview.anchor(top: viewOfHeader.topAnchor, leading: viewOfHeader.leadingAnchor, bottom: viewOfHeader.bottomAnchor, trailing: viewOfHeader.trailingAnchor)

            sectionview.delegate = self
            sectionview.secIndex = section
            sectionview.titleLBL.text = data[section].headerName
            print("HeaderName",data[section].headerName!)
//            sectionview.titleLBL.textColor = .red

            sectionview.btn.setTitleColor(UIColor.black, for: .normal)
            sectionview.btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            
            
            
            
            if (data[section].headerName!.elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Notification", comment: "")))
            {
                sectionview.countLBL.isHidden = false
                sectionview.countLBL.layer.cornerRadius = 12
                sectionview.countLBL.layer.masksToBounds = true
                sectionview.countLBL.clipsToBounds = true
                sectionview.countLBL.text = "114"
               // sectionview.countLBL.text =  UserDefaults.standard.string(forKey: "notificationcount")
               // print("string======:", sectionview.countLBL.text)
                
                sectionview.countLBL.font = UIFont(name: (self.homeLBL.font.fontName), size: CGFloat(Get_fontSize(size: 12)))
                
                sectionview.countLBL.textColor = .white
              
            }
            
            else
            {
                
                 sectionview.countLBL.isHidden = true
                
               
            }
            
            
//            //MarketPlace Header Add Target Method
//
//            if sectionview.titleLBL.text == LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: "")
//            {
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//                gesture.numberOfTapsRequired = 1
//                sectionview.isUserInteractionEnabled = true
//                sectionview.addGestureRecognizer(gesture)
//            }
//            else
//            {
//               //do nothing
//            }
//
            

            // Check if Guest
            
            

            let isGuest = UserDefaults.standard.string(forKey: "isGuest")

            if (isGuest?.elementsEqual("1"))!  // check if the user entered as guest
            {
                if section == (data.count - 1) {
                    sectionview.titleLBL.text = ""
                }
                if section == 17
                {
                    sectionview.isHidden = true
                }
                
                if section == 18
                {
                    sectionview.isHidden = true
                }
            }
            else
            {
                sectionview.titleLBL.text = data[section].headerName
            }


            if (data[section].headerName!.elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")))
            {
                if myCountryName == nil
                {
                    sectionview.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")
                }
                else
                {
                    sectionview.titleLBL.text = myCountryName
                }
            }
            else
            {
                //do Nothing
            }


            if (data[section].headerName!.elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")))
            {
                if languageName == nil
                {
                    sectionview.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")
                }
                else
                {
                    sectionview.titleLBL.text = languageName
                }
            }
            else
            {
                //do Nothing
            }



            if self.data[section].isExpandable == true {
                if self.data[section].subType.count > 0 {
                    sectionview.barImage.isHidden = false
                    sectionview.plusImage.isHidden = false
                    sectionview.plusImage.image = UIImage(named: "minus")
                }
                else {
                    sectionview.barImage.isHidden = true
                    sectionview.plusImage.isHidden = true
                }

            }
            else {
                if self.data[section].subType.count > 0 {
                    sectionview.plusImage.isHidden = false
                    sectionview.plusImage.image = UIImage(named: "plus")
                }
                else {
                    sectionview.plusImage.isHidden = true
                }
                sectionview.barImage.isHidden = true
            }
            
            //sectionview.backgroundColor = .red
            
            sectionview.titleLBL.font = UIFont(name: (self.homeLBL.font.fontName), size: CGFloat(Get_fontSize(size: 16)))    // Setting Font of Sections

            //sectionview.backgroundColor = UIColor.red

            return viewOfHeader
        }
        else
        {
            return nil
        }

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == collapsibleTableview
        {
//            if section == 17
//            {
//                return 0
//            }
//            if section == 18
//            {
//                return 0
//            }
            
            return (30/568)*FullHeight
        }
            
        else
        {
            return 0
        }
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == collapsibleTableview
        {

            let cellIdentifier = "SideViewCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideViewCell

            //cell.backgroundColor=UIColor.clear

            cell.cellLabel.text = data[indexPath.section].subType[indexPath.row]

            cell.cellLabel.textAlignment = .natural
            
            cell.cellLabel.textColor = .black


            // Setting Font size of cells
            cell.cellLabel.font = UIFont(name: (cell.cellLabel.font.fontName), size: CGFloat(Get_fontSize(size: 14)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if UserDefaults.standard.string(forKey: "langID") == "AR"{
                cell.cellLabel.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            
            return cell

        }
        else if tableView == sideMenuCategoryTable
        {
            let categoryCellIdentifier = "SideViewCategoryCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifier, for: indexPath) as! SideViewCategoryCell
            
        
            

            let langID = UserDefaults.standard.string(forKey: "langID")

            //AR or
            if (langID?.elementsEqual("EN"))!
            {
               // let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                cell.cellTitleLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"] ?? "")" + "(" +  "\((allCategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
            }
            else if (langID?.elementsEqual("AR"))!
            {
                cell.cellTitleLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)" + "(" +  "\((allCategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
                cell.cellImageview.transform = CGAffineTransform(scaleX: -1, y: 1)
            }

            cell.backgroundColor = UIColor.clear

            // Setting Font size of cells
            cell.cellTitleLBL.font = UIFont(name: (cell.cellTitleLBL.font.fontName), size: CGFloat(Get_fontSize(size: 15)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.isUserInteractionEnabled = true
            cell.buttonOut.tag = indexPath.row
            cell.buttonOut.addTarget(self, action: #selector(tapOnCategoryCell(_sender:)), for: .touchUpInside)

            return cell
        }
        else if tableView == self.ForthTableView
        {
           
            //ForthTableView.isScrollEnabled = false
            
            let identifier = "CategoryCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CategoryCell
            

            
            cell.cellTitleLBL.font = UIFont(name: (self.homeLBL.font.fontName), size: CGFloat(Get_fontSize(size: 17)))
            
           
            
            if indexPath.row == 0
            {
                print("language")
                
                 cell.MAROOFlbl.isHidden = true
                cell.celltitleleadingconstraints.constant = -20
                cell.celltitletopcons.constant = 0
               //  cell.cellTitleLBL.backgroundColor = .red
                cell.cellimage.isHidden = true
                if (languageName?.elementsEqual(""))!
                {
                   
                   cell.cellTitleLBL.text = "\(self.ForthTableArray[indexPath.row])"
                }
                else
                {
                    cell.cellTitleLBL.text = "\(languageName ?? "")"
                }
            }
               
            else if indexPath.row == 1
            {
                
                 print("country")
                
                 cell.MAROOFlbl.isHidden = true
                cell.celltitleleadingconstraints.constant = -20
                 // cell.cellTitleLBL.backgroundColor = .green
                cell.cellimage.isHidden = true
                if (myCountryName?.elementsEqual(""))!
                {
                    
                    
                   cell.cellTitleLBL.text = "\(self.ForthTableArray[indexPath.row])"
                }
                else
                {
                    
                    cell.cellTitleLBL.text = "\(myCountryName ?? "")"
                }
            }
            
            else if indexPath.row == 2
            {
                
               
                 cell.MAROOFlbl.isHidden = true
                cell.celltitleleadingconstraints.constant = -20
                cell.cellimage.isHidden = true
                //cell.cellTitleLBL.text = "About Kaafoo"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"About Kaafoo", comment: "")
                
               
            }
            else if indexPath.row == 3
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Kaafoo Bonus"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Kaafoo Bonus", comment: "")
                cell.celltitleleadingconstraints.constant = -20
                               cell.cellimage.isHidden = true
                cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
               
            }
            else if indexPath.row == 4
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Contact Us"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Contact Us", comment: "")
                cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
                
            }
            else if indexPath.row == 5
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Help"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Help", comment: "")
                cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
            }
            else if indexPath.row == 6
            {
                 cell.MAROOFlbl.isHidden = true
               // cell.cellTitleLBL.text = "Report"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Report", comment: "")
               cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
            }
            else if indexPath.row == 7
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Terms and Conditions"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Terms and Conditions", comment: "")
               cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
            }
            else if indexPath.row == 8
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Privacy Policy"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"Privacy Policy", comment: "")
               cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
            }
            else if indexPath.row == 9
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "About Us"
                  cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"About Us", comment: "")
               cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
            }
            else if indexPath.row == 10
            {
                 cell.MAROOFlbl.isHidden = true
              
                //cell.cellTitleLBL.text = "SOCIAL MEDIA"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"SOCIAL MEDIA", comment: "")
                cell.celltitleleadingconstraints.constant = -20
                                             cell.cellimage.isHidden = true
              
            }
            else if indexPath.row == 11
            {
                 cell.MAROOFlbl.isHidden = true
                cell.cellimage.isHidden = false
                cell.cellimage.image = UIImage(named: "facebook")
                //cell.cellTitleLBL.text = "Facebook"
                cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Facebook", comment: "")
                cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 12
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Twitter"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Twitter", comment: "")
                cell.cellimage.image = UIImage(named: "twitter")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 13
            {
                 cell.MAROOFlbl.isHidden = true
               // cell.cellTitleLBL.text = "Instagram"
                  cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Instagram", comment: "")
               cell.cellimage.image = UIImage(named: "instagram")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 14
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Youtube"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Youtube", comment: "")
              cell.cellimage.image = UIImage(named: "icon_youtube")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 15
            {
                 cell.MAROOFlbl.isHidden = true
               // cell.cellTitleLBL.text = "SnapChat"
                cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "SnapChat", comment: "")
              cell.cellimage.image = UIImage(named: "snapchat")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 16
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Share with friends"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Share with friends", comment: "")
              cell.cellimage.image = UIImage(named: "share")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 17
            {
                 cell.MAROOFlbl.isHidden = true
                //cell.cellTitleLBL.text = "Rate us on App store"
                 cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rate us on App store", comment: "")
              cell.cellimage.image = UIImage(named: "star")
                 cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
            }
            else if indexPath.row == 18
            {
                

                 cell.cellTitleLBL.isHidden = true
                 cell.cellimage.isHidden = true
                cell.MAROOFlbl.isHidden = false

              
            cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 15)

                cell.cellTitleLBL.textAlignment = .center
                
                
               
             
                
            }
            else if indexPath.row == 19
            {
               cell.MAROOFlbl.isHidden = true

                 cell.cellimage.isHidden = false
               cell.cellTitleLBL.isHidden = true

                  cell.cellimage.image = UIImage(named: "unnamed")

                cell.cellimageheightcons.constant = 50
                cell.cellimagewidthcons.constant = 80
                cell.cellimage.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive=true
                 // cell.cellimage.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive=true
                cell.cellimagetopcons.constant = 0
                // cell.cellimage.backgroundColor = .green
                
                
                   

            }
                
                else if indexPath.row == 20
                {
                    
                      cell.MAROOFlbl.isHidden = true
                cell.cellimage.isHidden = false
                              cell.cellTitleLBL.isHidden = true

                                 cell.cellimage.image = UIImage(named: "logo")
                          //cell.cellimage.backgroundColor = .blue
                               cell.cellimageheightcons.constant = 40
                               cell.cellimagewidthcons.constant = 50
                     cell.cellimagetopcons.constant = 0
                               cell.cellimage.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive=true
                }
                
                else if indexPath.row == 21
                    {
                        
                          cell.MAROOFlbl.isHidden = false
                    cell.cellTitleLBL.isHidden = true
                                    cell.cellimage.isHidden = true
                              
                                   // cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Version 1.0.0", comment: "")
                        
                        cell.MAROOFlbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Version 1.0.0", comment: "")
                               
                         cell.marooflblbottomcons.constant = 20

                                   //cell.cellTitleLBL.backgroundColor = .green
                               cell.MAROOFlbl.font = UIFont(name: "Lato-Regular", size: 15)
                                 
//                                     cell.cellTitleLBL.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive=true
//                                     cell.cellTitleLBL.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive=true
//                                   cell.cellTitleLBL.textAlignment = .center
                    }
                
            else if indexPath.row == 22
            {
                
                  cell.MAROOFlbl.isHidden = false
               cell.cellimage.isHidden = true
               cell.cellTitleLBL.isHidden = true
                 cell.MAROOFlbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Copyright@2020KaafooAll rights reserved", comment: "")
            
            cell.MAROOFlbl.font = UIFont(name: "Lato-Regular", size: 10)
                cell.marooflblbottomcons.constant = 30
                
            }
            
            else if indexPath.row == 23
                       {
                        
                        
                        var isGuest = "1"
                        
                        if (UserDefaults.standard.string(forKey: "isGuest")?.isEmpty)!
                        {
                            isGuest = "1"
                        }
                        else
                        {
                            isGuest = UserDefaults.standard.string(forKey: "isGuest")!
                        }
                        
                        if (isGuest.elementsEqual("1"))
                        {
                            cell.MAROOFlbl.isHidden = true
                            cell.cellimage.isHidden = true
                            cell.cellTitleLBL.isHidden = true
                        }
                        
                         
                        
                        else
                        {
                           
                             cell.MAROOFlbl.isHidden = true
                          cell.cellimage.isHidden = true
                          cell.cellTitleLBL.isHidden = false
                            cell.cellTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Logout", comment: "")
                       
                       cell.cellTitleLBL.font = UIFont(name: "Lato-Regular", size: 18)
                           cell.marooflblbottomcons.constant = 30
                        
                           
                       }
            }
            
            
          
            
            return cell

        }
        else
        {


            let identifier = "CategoryCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CategoryCell

            // AR or EN
            
            cell.celltitleleadingconstraints.constant = 0

            let langID = UserDefaults.standard.string(forKey: "langID")

            //  print("All Categories Array----",self.allCategoriesArray)
            
            //print("totalCount","\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")")

            if (langID?.elementsEqual("EN"))!
            {
                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                
                
                print("Subcategory Count--------","\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")")
                
                cell.cellTitleLBL.text = "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"] ?? "")" + "(" +  "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
                
            }
            else
            {
               
                cell.cellTitleLBL.text = "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)" + "(" +  "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
            }

            cell.backgroundColor = UIColor.clear

            // Setting Font size of cells
            cell.cellTitleLBL.font = UIFont(name: (cell.cellTitleLBL.font.fontName), size: CGFloat(Get_fontSize(size: 14)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }



    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == collapsibleTableview
        {

            if data[indexPath.section].isExpandable
            {
                print("headername : ",((data[indexPath.section].headerName)!) )
                if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_account", comment: "")))
                {
                    Side_delegate?.myAccount_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_interests", comment: "")))
                {
                    Side_delegate?.buying_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "selling", comment: "")))
                {
                    Side_delegate?.selling_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual("Job Section"))
                {
                    Side_delegate?.job_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: "")))
                {
                    Side_delegate?.myAccount_marketplace_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Rental", comment: "")))
                {
                    Side_delegate?.myAccount_dailyRental_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "happening", comment: "")))
                {
                    Side_delegate?.myAccount_Happening_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Holiday Accomodation", comment: "")))
                {
                    Side_delegate?.myAccount_HolidayAccommodation_Section(sender: indexPath.row)
                }
                else if       (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Food Court", comment: "")))
                {
                    Side_delegate?.myAccount_FoodCourt_Section(sender: indexPath.row)
                }
                    
                else if       (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Commercial Advertisement", comment: "")))
                {
                    Side_delegate?.myAccount_Commercial_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Services", comment: "")))
                {
                    Side_delegate?.myAccount_Service_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Real Estate", comment: "")))
                {
                    Side_delegate?.myAccount_Real_Estate_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: "")))
                {
                    Side_delegate?.myAccount_Advertsiement_Section(sender: indexPath.row)
                }
                
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: "")))
                {
                    Side_delegate?.myAccount_Latest_news()
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Query", comment: "")))
                {
                    Side_delegate?.myAccount_Query_Section(sender : indexPath.row)
                }
                    
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Chat", comment: "")))
                {
                    Side_delegate?.chat_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")))
                {
                    Side_delegate?.myAccount_notification()
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "account_wallet", comment: "")))
                {
                    Side_delegate?.myAccount_account_wallet_Section(sender: indexPath.row)
                }
                
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Bonus Activation", comment: "")))
                {
                    Side_delegate?.bonusActivation_section(sender: indexPath.row)
                }
                
                
                
                

            }
        }
        else if tableView == sideMenuCategoryTable
        {
//
//            categoryID = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["id"] ?? "")"
//
//            //print("Category ID --- ", categoryID)
//
//            if categoryID.elementsEqual("11")
//            {
//                categoryType = "Services"
//
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(serviceviewTapped))
//                gesture.numberOfTapsRequired = 1
//                categoryNameLBL.isUserInteractionEnabled = true
//                categoryNameLBL.addGestureRecognizer(gesture)
//            }
//            else if categoryID.elementsEqual("9")
//            {
//                categoryNameLBL.textColor = .black
////                CategoryNameBtn.isHidden = false
////                CategoryNameBtn.addTarget(self, action: #selector(SideBtnDelegate?.MarketPlace(sender:)), for: .touchUpInside)
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//                gesture.numberOfTapsRequired = 1
//                categoryNameLBL.isUserInteractionEnabled = true
//                categoryNameLBL.addGestureRecognizer(gesture)
//
//            }
//            else if categoryID.elementsEqual("10")
//            {
//                categoryType = "None"
//                Side_delegate?.dailyDealsDidSelect()
//            }
//            else if categoryID.elementsEqual("13")
//            {
//                categoryType = "Food Court"
//                //                Side_delegate?.foodCourtDidSelect(typeID: "\(self.FoodCourtArray[indexPath.row])")
//                //                Side_delegate?.foodCourtDidSelect()
//
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(foodviewTapped))
//                gesture.numberOfTapsRequired = 1
//                categoryNameLBL.isUserInteractionEnabled = true
//                categoryNameLBL.addGestureRecognizer(gesture)
//
//            }
//            else if categoryID.elementsEqual("16")
//            {
//                categoryType = "None"
//                Side_delegate?.holidayAccomodationDidSelect()
//            }
//            else if categoryID.elementsEqual("999")
//            {
//                categoryType = "None"
//                Side_delegate?.RealEstateDidSelect()
//            }
//            else if categoryID.elementsEqual("15")
//            {
//                categoryType = "None"
//                Side_delegate?.dailyRentalDidSelect(productID: categoryID)
//            }
//            else if categoryID.elementsEqual("12")
//            {
//                categoryType = "None"
//                Side_delegate?.jobDidSelect()
//            }
//            else if categoryID.elementsEqual("14")
//            {
//                categoryType = "None"
//                Side_delegate?.HappeningDidSelect()
//            }
//            else if categoryID.elementsEqual("1122")
//            {
//                categoryType = "None"
//                Side_delegate?.ClassifiedDidSelect()
//
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(classifiedTapped))
//                               gesture.numberOfTapsRequired = 1
//                               categoryNameLBL.isUserInteractionEnabled = true
//                               categoryNameLBL.addGestureRecognizer(gesture)
//
//
//            }
//            else
//            {
//                categoryType = "None"
//            }
//
//            let langID = UserDefaults.standard.string(forKey: "langID")
//
//            if (langID?.elementsEqual("EN"))!
//            {
//                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
//                categoryNameLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)[categoryName ?? ""] ?? "")"
//            }
//            else
//            {
//                categoryNameLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)"
//            }
//
//            //back to main menu lbl
//            backToMainMenuLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Back to Main Menu", comment: "")
//
//
//
//            self.InsideCategoryFetchData()
//
//            dispatchGroup.notify(queue: .main) {
//
//                self.insideCategoryView.isHidden = false
//                self.menuView.bringSubviewToFront(self.insideCategoryView)
//
//            }

        }
        else if tableView == insideCategoryTableview
        {

            if categoryType.elementsEqual("Services")
            {
                Side_delegate?.servicesDidselect(typeID: "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"]!)" , categoryName : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"]!)")
            }
            else if categoryType.elementsEqual("Food Court")
            {

                Side_delegate?.foodCourtDidSelect(typeID: "\((self.allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"] ?? "")")

            }
                //            else if categoryType.elementsEqual("Jobs")
                //            {
                //                Side_delegate?.jobDidSelect(typeID: "\((allCategoriesArray[indexPath.row] as! NSDictionary)["id"]!)")
                //            }
            else
            {
                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                
                MarketPlaceParameters.shared.setMarketPlaceCategory(selectcategory: "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)[categoryName!]!)")
                
                Side_delegate?.categoryDidSelect(typeID : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"]!)" , categoryID : categoryID! , categoryName : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)[categoryName!]!)")

            }
        }
        
        else if tableView == self.ForthTableView
        {
            Side_delegate?.forthTable_action(sender: indexPath.row)
        }
    }
}

//MARK:- //Add Target To MarketPlace Heading

@objc public protocol SideButtonDelegate : class
{
    @objc func MarketPlace(sender : UIButton)
    
}




