//
//  FooterView.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class FooterView: UIView {
    
    
    @IBOutlet weak var homeView: UIView!
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var homeLabel: UILabel!
    
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var notificationLBL: UILabel!
    
    
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var addLBL: UILabel!
    
    
    
    @IBOutlet weak var dailyDealsView: UIView!
    @IBOutlet weak var dailyDealsIMage: UIImageView!
    @IBOutlet weak var dailyDealsLabel: UILabel!
    
    
    
    @IBOutlet weak var clickHome: UIButton!
    @IBOutlet weak var clickMessage: UIButton!
    @IBOutlet weak var clickNotification: UIButton!
    @IBOutlet weak var clickAdd: UIButton!
    @IBOutlet weak var clickDailyDeals: UIButton!
    
    
    
    
    
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("FooterView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        homeLabel.text = "Home"
        messageLabel.text = "Message"
        notificationLBL.text = "Notification"
        addLBL.text = "Add"
        dailyDealsLabel.text = "Daily Deals"
       
    }
    
    
    func setFrame(ofLabel: UILabel, inView: UIView)
    {
        ofLabel.sizeToFit()
        ofLabel.textAlignment = .center
        ofLabel.center = inView.center
        
    }
    

}
