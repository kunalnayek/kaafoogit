//
//  GlobalDataClass.swift
//  Kaafoo
//
//  Created by esolz on 24/04/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import Foundation
import UIKit

class GlobalDataClass {
    
    static let shared = GlobalDataClass()
    init(){}
    
    var allCategoryData: NSArray!

    
    //MARK:- Set User data
    public func setUserData(category_data: NSArray){
        allCategoryData = category_data
    }
}
