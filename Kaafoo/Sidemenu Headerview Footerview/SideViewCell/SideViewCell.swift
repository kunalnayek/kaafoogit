//
//  SideViewCell.swift
//  Kaafoo
//
//  Created by admin on 14/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SideViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UserDefaults.standard.string(forKey: "langID") == "AR"{
            contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


}
