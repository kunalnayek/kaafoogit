//
//  AccountSearchListingVC.swift
//  Kaafoo
//
//  Created by priya on 16/07/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import GoogleMaps


class AccountSearchListingVC: GlobalViewController,GMSMapViewDelegate {
    
    
   
    @IBOutlet weak var allcategoriesbtn: UIButton!
    @IBOutlet weak var allcitiesBtn: UIButton!
    @IBOutlet weak var dispalymapBtn: UIButton!
    @IBOutlet weak var mapview: GMSMapView!
    @IBOutlet weak var accountlistview: UIView!
    @IBOutlet weak var citypickerview: UIPickerView!
    @IBOutlet weak var showcitypickerview: UIView!
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var showpickerview: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
     
    @IBOutlet weak var titleimg: UIImageView!
    
    @IBOutlet weak var QRcodeimg: UIImageView!
    @IBOutlet weak var accountLBL: UILabel!
    @IBOutlet weak var durationLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var pickerbuttonView: UIView!
    @IBOutlet weak var pickerviewOkBtn: UIButton!
    @IBOutlet weak var pickerviewCancelBtn: UIButton!
    
    
    @IBOutlet weak var citypickerBtnView: UIView!
    @IBOutlet weak var citypickerOkBtn: UIButton!
    @IBOutlet weak var citypickerCancleBtn: UIButton!
    
    
    
    var index : Int! = 0
    var markers = [GMSMarker]()
    let dispatchGroup = DispatchGroup()
    var MyCountry : String! = ""
    var allcountrylistArray : NSArray!
    var StateID : String! = ""
    var StateListArray : NSMutableArray! = NSMutableArray()
    var SelectedCityPickerRow : Int! = 0
    var ShowPicker : Bool! = false
    var cityrecordsArray = NSMutableArray()
    var citytitlestr : String! = ""
   
    
    
      var SearchBy : String! = ""
      var StartFrom : Int! = 0
      var StartValue : Int! = 0
      var PerLoad : Int! = 10
      var nextStart : String! = ""
      var scrollBegin : CGFloat!
      var scrollEnd : CGFloat!
      var recordsArray : NSMutableArray! = NSMutableArray()
      let LangID = UserDefaults.standard.string(forKey: "langID")
      let userID = UserDefaults.standard.string(forKey: "userID")
      let MyCountryName = UserDefaults.standard.string(forKey: "myCountryName")
      var userAccountTypeCommaSeparatedString : String! = ""
     
    
     var MapDetailsArray = [MapList]()
    
    
    @IBOutlet weak var accountlistingtableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        self.headerView.tapSearch.addTarget(self, action: #selector(self.HeaderSearch(sender:)), for: .touchUpInside)
         self.searchBar.delegate = self
         self.searchBar.isHidden = true
         self.accountlistview.isHidden = true
         self.mapview.delegate = self
        
        self.getListing()
      
        
       
       
    }
    
    @IBAction func pickerviewokbtnClicked(_ sender: Any) {
        
        
    }
    
    
    @IBAction func pickerviewcancelbtnClicked(_ sender: Any) {
    }
    
    
    @IBAction func citypickerokbtnClicked(_ sender: Any) {
        
        self.showcitypickerview.isHidden = true
        self.allcitiesBtn.setTitle("\((self.cityrecordsArray[SelectedCityPickerRow] as! NSDictionary)["city_name"]!)", for: .normal)
        citytitlestr =  self.allcitiesBtn.titleLabel!.text
        print("citytitlestr====",citytitlestr)
        self.getcityData()
    }
    
    
    @IBAction func citypickercancelbtnClicked(_ sender: Any) {
        
        self.showcitypickerview.isHidden = true
    }
    
    
    
    @IBAction func allcategoriesBtnClicked(_ sender: Any) {
    }
    
    @IBAction func allcitiesBtnClicked(_ sender: Any) {
        
         
        self.showcitypickerview.isHidden = false
             
        self.citypickerview.selectRow(0, inComponent: 0, animated: false)
        
          self.getcityData()
                
     


    }
    
    
     //MARK:- //Create ToolBar for PickerView
        
       
       
    
    //MARK:- //Append Data in Map
       
       func AppendMapData()
       {
           self.dispatchGroup.enter()
           
           for i in 0..<self.recordsArray.count
           {
               self.MapDetailsArray.append(MapList(displayname: "\((recordsArray[i] as! NSDictionary)["displayname"] ?? "")", displaytitle: "\((recordsArray[i] as! NSDictionary)["displayname"] ?? "")", displayduration: "\((recordsArray[i] as! NSDictionary)["displayname"] ?? "")", displayaccount: "\((recordsArray[i] as! NSDictionary)["displayname"] ?? "")"))
           }
           
           self.dispatchGroup.leave()
           
           self.dispatchGroup.notify(queue: .main, execute: {
               
               let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
               //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
               self.mapview.animate(to: cameraPosition)
               
               self.mapview.isMyLocationEnabled = true
               self.mapview.settings.myLocationButton = true
               
               
               for state in self.ProductOnMapArray {
//                   let state_marker = GMSMarker()
//                   state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
//                   state_marker.title = state.ProductName
//                   state_marker.snippet = "Hey, this is \(state.ProductName!)"
//                   state_marker.map = self.mapview
//                   state_marker.userData = state
//                   self.markers.append(state_marker)
                   
               }
           })
       }
       
    
    
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //        customInfoWindow?.removeFromSuperview()
       
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        
        self.index = 0
        self.accountlistview.isHidden = true
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
        //        let update = GMSCameraUpdate.zoom(by: 10)
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
        //        GmapView.moveCamera(update)
        mapView.animate(to: cameraPosition)
        CATransaction.commit()
    }
    
    func centerInMarker(marker: GMSMarker) {
        
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        mapview?.moveCamera(updateOne)
        mapview.transform = .identity
        
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = mapView.selectedMarker {
            selectedMarker.icon = GMSMarker.markerImage(with: nil)
        }
        mapView.selectedMarker = marker
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        
        self.accountlistview.isHidden = false
        
        let tempdict = marker.userData as! MapList
        
        self.dispatchGroup.enter()
        
       
      
        self.titleLBL.text = "\(tempdict.displayname ?? "")"
//        self.durationLBL.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
//        self.accountLBL.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
//        self.titleimg.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
//
        
//        let path101 = "\((self.recordsArray[indexPath.row] as! NSDictionary)["qrcode"]!)"
//        self.titleimg.sd_setImage(with: URL(string: path101))
               
             
        
        self.dispatchGroup.leave()
        
       
        return true
    }


    
    @IBAction func displaymapBtnClicked(_ sender: Any)
    {
        if mapview.isHidden == true
        {
            self.mapview.isHidden = false
            self.accountlistview.isHidden = true
          
        }
        else
        {
            self.mapview.isHidden = true
            self.accountlistview.isHidden = false
          
        }
    }
    
    
    
    @objc func HeaderSearch(sender : UIButton)
    {
      self.searchBar.isHidden = false
   
     
    
    }
    

    //MARK:- API Call Method
    
   

    func getListing()
    {
        
        let parameters = "lang_id=\(LangID!)&per_page=\(PerLoad!)&mycountry_name=\(MyCountryName!)&start_from=\(StartFrom!)&search_by=\(SearchBy!)&all_cat=&user_acc_type_search=\(userAccountTypeCommaSeparatedString!)&city=\(citytitlestr!)"
        
        
        print("parameters====",parameters)
        
        self.CallAPI(urlString: "app_all_users_list", param: parameters, completion: {
                   
                   self.globalDispatchgroup.leave()
                   
                   let infoarray = self.globalJson["data_info"] as! NSArray
                   
                   for i in 0..<infoarray.count
                   {
                       let tempdict = infoarray[i] as! NSDictionary
                       
                       self.recordsArray.add(tempdict)
                   }
            
                      self.nextStart = "\(self.globalJson["next_start_other"] ?? "")"
            
                   self.globalDispatchgroup.notify(queue: .main, execute: {
                       
                    DispatchQueue.main.async {
                   
                        self.AppendMapData()
                      
                    self.accountlistingtableView.dataSource = self
                    self.accountlistingtableView.delegate = self
                    self.accountlistingtableView.reloadData()
                    
                     SVProgressHUD.dismiss()
                }
            })

        })
    }
    
   
        
        
     
        
        //MARK:- //Dismiss Keyboard
        
        @objc func dismissKeyboard() {
            
            view.endEditing(true)
        }
        
    
    
    func getcityData()
    {
            dispatchGroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
            
            
            let url = URL(string: GLOBALAPI + "app_country_to_city")!       //change the url
            
            print("all country list URL : ", url)
            
            var parameters : String = ""
            
    //        let langID = UserDefaults.standard.string(forKey: "langID")
           // MyCountry = "99"
        
            let CountryId = UserDefaults.standard.string(forKey: "countryID")
            
            parameters = "lang_id=EN&mycountry_id=\(CountryId!)"
            
            print("Parameters are : " , parameters)
            
            let session = URLSession.shared
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = parameters.data(using: String.Encoding.utf8)
                
                
            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("all Country List Response: " , json)
                    
                    self.allcountrylistArray = json["info_array"] as? NSArray


                        for i in 0..<self.allcountrylistArray.count
                        {
                            self.StateListArray.add("\((self.allcountrylistArray[i] as! NSDictionary)["region_name"]!)")
                        }


                        for j in 0..<self.allcountrylistArray.count
                        {

                                for k in 0..<((self.allcountrylistArray[j] as! NSDictionary)["city_list"]! as! NSArray).count
                                {
                                    let tempdict = (((self.allcountrylistArray[j] as! NSDictionary)["city_list"]! as! NSArray)[k] as! NSDictionary)
                                    self.cityrecordsArray.add(tempdict)
                                    print("cityrecordsArray====",self.cityrecordsArray)
                                    
                                    
                                }
                          
                        }
                
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                            
                            self.citypickerview.delegate = self
                            
                            self.citypickerview.dataSource = self
                            
                            self.citypickerview.reloadAllComponents()
                        }
                        
                    }
                    
                } catch let error {
                    
                    print(error.localizedDescription)
                }
            })
            
            task.resume()
            
            
            
        }
    
    func getcategoryData()
    {
        
    }

}





// MARK:- // Tableview Delegate Methods

extension AccountSearchListingVC: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let obj = tableView.dequeueReusableCell(withIdentifier: "AccountListingTableViewCell") as! AccountListingTableViewCell

        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"]!)"
        obj.accountImage.sd_setImage(with: URL(string: path))
        
        let path1 = "\((self.recordsArray[indexPath.row] as! NSDictionary)["qrcode"]!)"
        obj.qrimage.sd_setImage(with: URL(string: path1))
        
        obj.title.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["displayname"]!)"
        obj.duration.text = "Member since:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["bus_regdate"]!)"
        obj.accountnumber.text = "Account Number:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["accotal_number"]!)"
        
       // let temp = "\(((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["whatsaap_link"] as! NSDictionary)["icon"]!))"
        
        //print("temp----",temp)
        
        if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["whatsaap_link"] as! NSDictionary)["link"]!)".isEmpty == false
        {
            
            let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["whatsaap_link"] as! NSDictionary)["icon"]!)"
                            
                              obj.socialmediaimg.sd_setImage(with: URL(string: path))

                              print("img---", obj.socialmediaimg.sd_setImage(with: URL(string: path)))
        }
        
        
       else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["google_link"] as! NSDictionary)["link"]!)".isEmpty == false
       {
            let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["google_link"] as! NSDictionary)["icon"]!)"
                   obj.googleimg.sd_setImage(with: URL(string: path))

        }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["facebook_link"] as! NSDictionary)["link"]!)".isEmpty == false
            {
                 let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["facebook_link"] as! NSDictionary)["icon"]!)"
                        obj.facebookimg.sd_setImage(with: URL(string: path))

             }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["twiter_link"] as! NSDictionary)["link"]!)".isEmpty == false
                       {
                            let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["twiter_link"] as! NSDictionary)["icon"]!)"
                                   obj.twitterimg.sd_setImage(with: URL(string: path))

                        }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["line_link"] as! NSDictionary)["link"]!)".isEmpty == false
                                  {
                                       let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["line_link"] as! NSDictionary)["icon"]!)"
                                              obj.smslinkimg.sd_setImage(with: URL(string: path))

                                   }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["instagram_link"] as! NSDictionary)["link"]!)".isEmpty == false
                                             {
                                                  let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["instagram_link"] as! NSDictionary)["icon"]!)"
                                                         obj.instagramimg.sd_setImage(with: URL(string: path))

                                              }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["wechat_link"] as! NSDictionary)["link"]!)".isEmpty == false
                                                       {
                                                            let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["wechat_link"] as! NSDictionary)["icon"]!)"
                                                                   obj.wechatimg.sd_setImage(with: URL(string: path))

                                                        }
            
            else if "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["sms_link"] as! NSDictionary)["link"]!)".isEmpty == false
            {
                 let path = "\((((recordsArray[indexPath.row] as! NSDictionary)["social_media"] as! NSDictionary)["sms_link"] as! NSDictionary)["icon"]!)"
                        obj.lineimg.sd_setImage(with: URL(string: path))

             }
        else
       {
            print("hide icon")
        }

       

        return obj
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 143
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["usertype"]!)".elementsEqual("B")
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            
            nav.sellerID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["id"]!)"
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            
            nav.sellerID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["id"]!)"
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
       
    }


}

// MARK:- // Searchbar Delegate Methods


extension AccountSearchListingVC : UISearchBarDelegate
{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("Writing is in progress")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchBar.searchTextField.text = ""
        
        self.searchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("Search is in Progress")
        
        self.SearchBy = self.searchBar.searchTextField.text
        
        self.recordsArray.removeAllObjects()
        
        self.getListing()
    }
}


// MARK:- // Scrollview Delegate MEthods

extension AccountSearchListingVC: UIScrollViewDelegate {

    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getListing()
            }
        }
    }

}



class AccountListingTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var accountImage: UIImageView!
    
    @IBOutlet weak var socialmediaimg: UIImageView!
    @IBOutlet weak var qrimage: UIImageView!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var accountnumber: UILabel!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var googleimg: UIImageView!
    
    @IBOutlet weak var facebookimg: UIImageView!
    @IBOutlet weak var twitterimg: UIImageView!
    @IBOutlet weak var lineimg: UIImageView!
    @IBOutlet weak var wechatimg: UIImageView!
    @IBOutlet weak var instagramimg: UIImageView!
    @IBOutlet weak var smslinkimg: UIImageView!
    
}

struct MapList
{
    var displayname : String?
    var displaytitle : String?
    var displayduration : String?
    var displayaccount : String?
   
    
    init(displayname : String , displaytitle : String , displayduration : String , displayaccount : String )
    {
        self.displayname = displayname
        self.displaytitle = displaytitle
        self.displayduration = displayduration
        self.displayaccount = displayaccount
       
        
        
    }
}

 //MARK:- // Pickerview Delegate MEthods

extension AccountSearchListingVC: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {

            return 1


    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
         return self.cityrecordsArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
       return ((self.cityrecordsArray[row] as! NSDictionary)["city_name"]!) as? String
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.SelectedCityPickerRow = row
    }
    
   
}


