//
//  KaafooBonusViewController.swift
//  Kaafoo
//
//  Created by priya on 01/06/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

class KaafooBonusViewController: GlobalViewController {
    
    @IBOutlet weak var WebView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetWebView()

        
    }
    
    func SetWebView()
    {
        
       
        let langID = UserDefaults.standard.string(forKey: "langID")
       
       
        let url = URL (string: "https://kaafoo.com/appcontrol/app_kaafoo_terms_condition_page?page_name=kaafoo_bonus"+"&"+langID!)
        
        print("url----",url)
        
        let requestObj = URLRequest(url: url!)
        
        self.WebView.loadRequest(requestObj)
    }

}
