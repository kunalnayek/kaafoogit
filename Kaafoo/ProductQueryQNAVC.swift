//
//  ProductQueryQNAVC.swift
//  Kaafoo
//
//  Created by esolz on 13/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProductQueryQNAVC: GlobalViewController {

    @IBOutlet weak var QnaQueryTV: UITableView!
    
    var reportID : String! = ""
    
    var msgID : String! = ""
    
    @IBAction func hideViewReportBtn(_ sender: UIButton) {
        
        self.ViewReport.isHidden = true
    }
    
    @IBOutlet weak var ReportView: Report!
    
    @IBOutlet weak var ViewReport: UIView!
    
    var FaqArray : NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.QnaQueryTV.separatorStyle = .none
        
        self.QnaQueryTV.delegate = self
        
        self.QnaQueryTV.dataSource = self
        
        self.QnaQueryTV.reloadData()
        
        self.ReportView.CrossBtnOutlet.addTarget(self, action: #selector(hideViewReport(sender:)), for: .touchUpInside)
        
        

        // Do any additional setup after loading the view.
    }
    
    @objc func hideViewReport(sender : UIButton)
    {
        self.ViewReport.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func ReplyBtnTarget(sender : UIButton)
    {
        self.ViewReport.isHidden = false
        
        print("Reply Button")
        
        self.msgID = "\((self.FaqArray[sender.tag] as! NSDictionary)["msg_id"] ?? "")"
        
        self.ReportView.SendBtnOutlet.addTarget(self, action: #selector(report(sender:)), for: .touchUpInside)
        
    }
    
    @objc func report(sender : UIButton)
    {
        if self.ReportView.ReportTxtView.text.isEmpty == true
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Reply text can not be empty", comment: ""))
        }
        else
        {
            let parameters = "lang_id=\(langID ?? "")&user_id=\(USERID ?? "")&msg_id=\(msgID ?? "")&product_id=\(reportID ?? "")&answer=\(self.ReportView.ReportTxtView.text ?? "")"
            
            self.CallAPI(urlString: "product-ques-ans-rply", param: parameters, completion: {
                
                self.globalDispatchgroup.leave()
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    
                    DispatchQueue.main.async {
                        
                        self.QnaQueryTV.reloadData()
                        
                        SVProgressHUD.dismiss()
                        
                        self.ReportView.isHidden = true
                        
                        self.customAlert(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: "\(self.globalJson["message"] ?? "")", completion: {
                            
                            self.navigationController?.popViewController(animated: false)
                            
                        })
                    }
                })
            })
        }
    }

}

class PQueryTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var QuestionView: UIView!
    
    @IBOutlet weak var ReplyView: UIView!
    
    @IBOutlet weak var AnswerView: UIView!
    
    @IBOutlet weak var ReplyBtnOutlet: UIButton!
    
    @IBOutlet weak var AnswerLabel: UILabel!
    
    @IBOutlet weak var QuestionLabel: UILabel!
    
    @IBOutlet weak var QuestionAsker: UILabel!
    
    @IBOutlet weak var QuestionDate: UILabel!
    
}

extension ProductQueryQNAVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.FaqArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pquery") as! PQueryTVC
        
        cell.ViewContent.layer.borderColor = UIColor.black.cgColor
        
        cell.ViewContent.layer.borderWidth = 1.0
        
        cell.ViewContent.layer.cornerRadius = 5.0
        
        cell.ReplyBtnOutlet.layer.cornerRadius = cell.ReplyBtnOutlet.frame.size.height / 2
        
        cell.selectionStyle = .none
        
        cell.ReplyBtnOutlet.setTitleColor(.white, for: .normal)
        
        cell.ReplyBtnOutlet.addTarget(self, action: #selector(ReplyBtnTarget(sender:)), for: .touchUpInside)
        
        cell.ReplyBtnOutlet.tag = indexPath.row
        
        if "\((self.FaqArray[indexPath.row] as! NSDictionary)["answer"] ?? "")".elementsEqual("")
        {
            cell.AnswerView.isHidden = true
            
            cell.ReplyView.isHidden = false
        }
        else
        {
            cell.AnswerView.isHidden = false
            
            cell.ReplyView.isHidden = true
            
            cell.AnswerLabel.text = "A : " + "\((self.FaqArray[indexPath.row] as! NSDictionary)["answer"] ?? "")"
        }
        
        cell.AnswerLabel.numberOfLines = 0
        
        cell.QuestionLabel.text = "Q : " + "\((self.FaqArray[indexPath.row] as! NSDictionary)["question"] ?? "")"
        
        cell.QuestionAsker.text = "\((self.FaqArray[indexPath.row] as! NSDictionary)["questioner_name"] ?? "")"
        
        cell.QuestionDate.text = "\((self.FaqArray[indexPath.row] as! NSDictionary)["questioner_date"] ?? "")"
        
        //Font
        
        cell.AnswerLabel.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionLabel.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionAsker.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionDate.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        //
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
