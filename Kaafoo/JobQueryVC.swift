//
//  JobQueryVC.swift
//  Kaafoo
//
//  Created by esolz on 17/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class JobQueryVC: GlobalViewController {

    var ListingArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var QueryListingTV: UITableView!
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.QueryListingTV.separatorStyle = .none
        
        //self.getListing()
        
        self.setLayer()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- //View Will appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.ListingArray.removeAllObjects()
        
        self.getListing()
    }
    
    //MARK:- //Set Layer for Job Query Design section
    
    func setLayer()
    {
        self.QueryListingTV.bounces = false
        
        self.sideMenuView.backgroundColor = UIColor(red: 65/255, green: 65/255, blue: 65/255, alpha: 0.85)
    }
    
    //MARK:- //Get Listing
    
    func getListing()
    {
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")"
        
        self.CallAPI(urlString: "app_job_question_query", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            let infoarray = self.globalJson["info_array"] as! NSArray
            
            for i in 0..<infoarray.count
            {
                let tempdict = infoarray[i] as! NSDictionary
                
                self.ListingArray.add(tempdict)
            }
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                DispatchQueue.main.async {
                    
                    self.QueryListingTV.delegate = self
                    
                    self.QueryListingTV.dataSource = self
                    
                    self.QueryListingTV.reloadData()
                    
                    SVProgressHUD.dismiss()
                }
            })
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class QueryJobTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var JobQueryName: UILabel!
    
    @IBOutlet weak var JobIV: UIImageView!
    
}

extension JobQueryVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.ListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "query") as! QueryJobTVC
        
        cell.JobQueryName.text = "Job Name : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
        
        cell.JobIV.image = UIImage(named: "notice")
        
        cell.ViewContent.layer.cornerRadius = 8.0
        
        cell.ViewContent.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        let textLayer = CATextLayer()
        
        textLayer.frame = cell.JobIV.bounds
        
        //textLayer.frame = CGRect(x: cell.JobIV.frame.origin.x - 5 , y: cell.JobIV.frame.origin.y, width: 20, height: 20)
        
        textLayer.string = String(((self.ListingArray[indexPath.row] as! NSDictionary)["qusn_ans"] as! NSArray).count)
        
        textLayer.alignmentMode = CATextLayerAlignmentMode.right
        
        textLayer.font = CGFont("Verdana" as CFString)!
        
        textLayer.fontSize = 12.0
        
        textLayer.foregroundColor = UIColor.black.cgColor
        
        cell.JobIV.layer.addSublayer(textLayer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "qnajobvc") as! JobQueryQNAVC
        
        navigate.QNAArray = ((self.ListingArray[indexPath.row] as! NSDictionary)["qusn_ans"] as! NSMutableArray)
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}
