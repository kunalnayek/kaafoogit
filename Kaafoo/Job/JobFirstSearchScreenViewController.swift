//
//  JobFirstSearchScreenViewController.swift
//  Kaafoo
//
//  Created by admin on 15/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class JobFirstSearchScreenViewController: GlobalViewController {


    @IBOutlet weak var pageTitle: UILabel!
    
    @IBOutlet weak var pageSubtitle: UILabel!
    
    @IBOutlet weak var openingVacanciesLBL: UILabel!
    
    @IBOutlet weak var employeesLBL: UILabel!
    
    @IBOutlet weak var vacanciesImageview: UIImageView!
    
    @IBOutlet weak var employeesImageview: UIImageView!
    
    @IBOutlet weak var viewOfPickerview: UIView!
    
    @IBOutlet weak var pagePicker: UIPickerView!
    
    @IBOutlet weak var searchStackview: UIStackView!
    
    @IBOutlet weak var searchByWordView: UIView!
    
    @IBOutlet weak var searchByWordTXT: UITextField!
    
    @IBOutlet weak var selectStateView: UIView!
    
    @IBOutlet weak var selectStateLBL: UILabel!
    
    @IBOutlet weak var state: UILabel!
    
    @IBOutlet weak var selectCityView: UIView!
    
    @IBOutlet weak var selectCityLBL: UILabel!
    
    @IBOutlet weak var city: UILabel!

    var typeID : String!

    var vacancyClicked : Bool! = false
    
    var employeesClicked : Bool! = false

    var stateArray = NSMutableArray()
    
    var cityArray = NSMutableArray()

    var pickerViewArray : NSArray!

    var regionID : String! = ""
    
    var countryID : String! = ""
    
    var cityID : String = ""

    // MARK:- // View Did Load

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.vacanciesImageview.image = UIImage(named: "emptySuccess")
        
        self.employeesImageview.image = UIImage(named: "emptySuccess")

        self.countryID = UserDefaults.standard.string(forKey: "countryID") ?? "99"

        self.countryRegionCity()

        self.FontSet()
        
        self.checkingBool()

        self.searchButtonOutlet.layer.cornerRadius = 5//self.searchButtonOutlet.frame.size.height / 2

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        let tap = UITapGestureRecognizer(target: self, action: #selector(closePicker(sender:)))
        
        self.viewOfPickerview.addGestureRecognizer(tap)

    }
    
    
    func checkingBool()
    {
        if vacancyClicked == true
        {
            vacanciesImageview.image = UIImage(named: "success")
        }
        else
        {
            vacanciesImageview.image = UIImage(named: "emptySuccess")
        }
        
        if employeesClicked == true
        {
            employeesImageview.image = UIImage(named: "success")
        }
        else
        {
            employeesImageview.image = UIImage(named: "emptySuccess")
        }
        
        
    }


    // MARK:- // Buttons

    // MARK:- // Select State Button

    @IBOutlet weak var selectStateButtonOutlet: UIButton!
    
    @IBAction func selectStateButton(_ sender: UIButton) {


        self.pickerViewArray = self.stateArray as NSArray

        self.pagePicker.reloadAllComponents()

        self.viewOfPickerview.isHidden = false

        self.view.bringSubviewToFront(self.viewOfPickerview)
    }


    // MARK:- // Select City Button

    @IBOutlet weak var selectCityButtonOutlet: UIButton!
    
    @IBAction func selectCityButton(_ sender: UIButton) {

        self.pickerViewArray = self.cityArray as NSArray

        self.pagePicker.reloadAllComponents()

        self.viewOfPickerview.isHidden = false

        self.view.bringSubviewToFront(self.viewOfPickerview)

    }



    // MARK:- // Search By Word Button

    @IBOutlet weak var searchByWordButtonOutlet: UIButton!
    
    @IBAction func searchByWordButton(_ sender: UIButton) {
    }





    // MARK:- // Opening Vacancies Button

    @IBOutlet weak var openingVacanciesButton: UIButton!
    
    @IBAction func tapOpeningVacancies(_ sender: UIButton) {
        
          vacanciesImageview.image = UIImage(named: "success")
        
        if vacanciesImageview.image == UIImage(named: "success")
        {
            employeesImageview.image = UIImage(named: "emptySuccess")
        }
        else
        {
            employeesImageview.image = UIImage(named: "emptySuccess")
        }
    }


    // MARK:- // Employees Button

    @IBOutlet weak var employeesButton: UIButton!
    
    @IBAction func tapEmployees(_ sender: Any) {
        
           employeesImageview.image = UIImage(named: "success")
        
        if employeesImageview.image == UIImage(named: "success")
        {
            vacanciesImageview.image = UIImage(named: "emptySuccess")
        }
        else
        {
            vacanciesImageview.image = UIImage(named: "emptySuccess")
        }

    }

    // MARK:- // Search Button

    @IBOutlet weak var searchButtonOutlet: UIButton!
    
    @IBAction func tapSearchButton(_ sender: Any) {

//        var jobCatArray = [String]()
//
//        if vacancyClicked == true
//        {
//            jobCatArray.append("1038")
//        }
//
//        if employeesClicked == true
//        {
//            jobCatArray.append("1039")
//        }
//
//        let jobCatString = jobCatArray.joined(separator: ",")
//
//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobListingVC") as! JobListingViewController
//
//        navigate.jobCatString = jobCatString
//
//        navigate.state = self.regionID
//
//        navigate.city = self.cityID
//
//        navigate.searchBy = self.searchByWordTXT.text ?? ""
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        
        self.searchByWordTXT.resignFirstResponder()
        
        
         //if vacancyClicked == false && employeesClicked == false
        if  vacanciesImageview.image == UIImage(named: "emptySuccess") && employeesImageview.image == UIImage(named: "emptySuccess")
         {
             self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Please select your preference", comment: ""))
        }
                
       

        else
         {
        
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newjob") as! NewJobListingViewController
        
            if vacanciesImageview.image == UIImage(named: "success")
            {
                navigate.jobCatString = "1038"
            }
            else if employeesImageview.image == UIImage(named: "success")
            {
                navigate.jobCatString = "1039"
            }
            self.navigationController?.pushViewController(navigate, animated: true)
        }

    }



    // MARK: - // JSON POST Method to get Country Region City List
    func countryRegionCity()
    {
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = "lang_id=\(langID!)&mycountry_id=\(countryID ?? "")&region_id=\(regionID ?? "")&city_id="

        self.CallAPI(urlString: "app_country_region_city", param: parameters, completion: {

            if self.countryID.isEmpty {
                //self.stateArray.removeAllObjects()
                self.cityArray = self.globalJson["info_array"] as! NSMutableArray
            }
            else {
                //self.cityArray.removeAllObjects()
                self.stateArray = self.globalJson["info_array"] as! NSMutableArray
                
                self.pickerViewArray = self.stateArray as NSArray
            }

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.pagePicker.delegate = self
                
                self.pagePicker.dataSource = self
                
                self.pagePicker.reloadAllComponents()

                SVProgressHUD.dismiss()
            }
        })
    }


    // MARK:- // Close Picker view

    @objc func closePicker(sender : UITapGestureRecognizer)
    {
        self.viewOfPickerview.isHidden = true

        self.view.sendSubviewToBack(self.viewOfPickerview)

    }


    // MARK:- // Set Font

    func FontSet()
    {
        self.pageTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "meet_potential_employees_and_find_interesting_vacancies", comment: "")
        
        self.pageSubtitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "connecting_employee_and_employees", comment: "")
        
        self.openingVacanciesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "searching_for_opening_vacancies", comment: "")
        
        self.employeesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "seraching_for_employee", comment: "")
        
        self.searchButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "search_by_word_or_state_or_city", comment: ""), for: .normal)
        
        self.selectStateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_state", comment: "")
        
        self.selectCityLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_city", comment: "")
        
    }

}



// MARK:- // Pickerview Delegate

extension JobFirstSearchScreenViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {

        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return self.pickerViewArray.count

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if self.countryID.isEmpty {
            
            return "\((self.pickerViewArray[row] as! NSDictionary)["city_name"] ?? "")"
        }
        else {
            
            return "\((self.pickerViewArray[row] as! NSDictionary)["region_name"] ?? "")"
        }

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if self.countryID.isEmpty {

            self.city.text = "\((self.pickerViewArray[row] as! NSDictionary)["city_name"] ?? "")"

            self.cityID = "\((self.pickerViewArray[row] as! NSDictionary)["city_id"] ?? "")"

            self.countryID = "99"


        }
        else {
            self.state.text = "\((self.pickerViewArray[row] as! NSDictionary)["region_name"] ?? "")"
            
            self.city.text = ""

            self.countryID = ""
            
            self.regionID = "\((self.pickerViewArray[row] as! NSDictionary)["region_id"] ?? "")"

            self.countryRegionCity()

            self.globalDispatchgroup.notify(queue: .main) {
                
                self.pickerViewArray = self.cityArray as? NSArray

                self.pagePicker.reloadAllComponents()
            }

        }

        self.viewOfPickerview.isHidden = true
        
        self.view.sendSubviewToBack(self.viewOfPickerview)

    }

}

