//
//  NewJobListingViewController.swift
//  Kaafoo
//
//  Created by esolz on 04/09/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import GoogleMaps
import GooglePlaces

class NewJobListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate {

    //MARK:- //Outlet Connections

    
    @IBOutlet weak var ListingCV: UICollectionView!
    
    @IBAction func FilterButton(_ sender: UIButton) {
        
        if self.filterView.isHidden == true
        {
            self.filterView.isHidden = false

            self.view.bringSubviewToFront(self.filterView)
        }
        else
        {
            self.filterView.isHidden = true

            self.view.sendSubviewToBack(self.filterView)
        }
        
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "categoryAdvancedSearchVC") as! CategoryAdvancedSearchViewController
//
//        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBOutlet weak var dataPickerView: UIPickerView!
    
    @IBAction func HidePickerViewBtn(_ sender: UIButton) {
        
        self.ShowPickerView.isHidden = true
    }
    @IBOutlet weak var HidePickerViewBtnOutlet: UIButton!
    
    @IBOutlet weak var ShowPickerView: UIView!
    
    @IBOutlet weak var FilterBtnOutlet: UIButton!
    
    @IBOutlet weak var LocalHeaderView: UIView!
    
    @IBOutlet weak var JobListingTableView: UITableView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var MapButtonOutlet: UIButton!
    
    @IBAction func MapButton(_ sender: UIButton) {
        
        if mapView.isHidden == true
        {
            self.mapView.isHidden = false
            
            self.JobListingTableView.isHidden = true
        }
    }
    @IBOutlet weak var ListGridButtonOutlet: UIButton!
    
    @IBAction func ListGridButton(_ sender: UIButton) {
        
        if JobListingTableView.isHidden == true
        {
            self.mapView.isHidden = true
            
            self.JobListingTableView.isHidden = false
        }
    
    }
    
    
     var allProductListingDictionary : NSDictionary!
    
    var localCategoryArray : NSMutableArray! = [["id" : " " , "name" : "All"]]
    
     var typeID : String! = ""
    
    var JobCatID : String! = ""
    
    var localSortCategoryArray = [["key" : "" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")] , ["key" : "T" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")] , ["key" : "N" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "newest_listed", comment: "")] , ["key" : "C" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "closing_soon", comment: "")]]
       
        var SortDict = [["key" : "" , "value" : "All"], ["key" : "T" , "value" : "Title"],["key" : "N" , "value" : "Newest Listed"] ,["key" : "C" , "value" : "Closing Soon"]]
    
//    var SortDict = [["key" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "") , "value" : ""],["key" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "") , "value" : "T"],["key" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "newly_listed", comment: "") , "value" : "N"],["key" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "closing_soon", comment: "") , "value" : "C"]]
    
    var sortType : String! = ""
    
    var JobListingStructArray = [JobListingStruct]()
    
    var JobInfoDict : NSDictionary!
    
    var JobDetailsArray : NSMutableArray! = NSMutableArray()
    
    var OptionDetailsArray : NSMutableArray! = NSMutableArray()
    
    var OptionValueArray : NSMutableArray! = NSMutableArray()
    
    var SelectedInt : Int!
    
    var startValue = 0
    
    var perLoad = 10
    
    var jobCatString : String! = ""
    
    var state : String! = ""
    
    var city : String! = ""
    
    var jobID : String! = ""
    
    var searchBy : String! = ""
    
    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    let MyCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    
    var ProductsOnMapArray = [MapListing]()
    
    var markers = [GMSMarker]()
    
    var SearchOptionOne : String! = ""
    
    var SearchOptionTwo : String! = ""
    
    var Mindist : String! = ""
    
    var MaxDist : String! = ""
    
    var index : Int!
    
    var isMarkerActive : Bool! = false
    
    var selectedMarker : GMSMarker!
    
     var optionvalId : String! = ""
     var optionvalName : String! = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setLayer()
        
        //self.ListingCV
        
        self.ListingCV.isHidden = true
        
        self.dataPickerView.delegate = self
        
        self.dataPickerView.dataSource = self
        
        self.dataPickerView.reloadAllComponents()
        
        self.setButtonSelector()
        
        self.mapView.isHidden = true
        
        self.mapView.delegate = self
        
        self.getListing()
        
        self.ProductsOnMapArray = []
        
        self.CreateFavouriteView(inview: self.view)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- //Set Layers for Configuring this page
    
    func setLayer()
    {
        self.JobListingTableView.bounces = false
    }
    
    func setButtonSelector()
    {
        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.filterButtonActionOne(sender:)), for: .touchUpInside)
        
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterButtonActionTwo(sender:)), for: .touchUpInside)
        
        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.filterViewCloseAction(sender:)), for: .touchUpInside)
    }
    
    @objc func filterButtonActionTwo(sender : UIButton)
    {
        print("filter Button Action One")
        
        print("jobcatid---",jobCatString)
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

               navigate.rootVC = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newjob") as! NewJobListingViewController

              // navigate.locationArray = self.JobInfoDict["filter_country_list"] as! NSArray
        
                navigate.rolearray = [sortClass(key: "M", value: "Marketing and PR", isClicked: false, totalNo: "nil"),sortClass(key: "I", value: "Information Technology", isClicked: false, totalNo: "nil"),sortClass(key: "E", value: "Engineering", isClicked: false, totalNo: "nil"),sortClass(key: "S", value: "Sales", isClicked: false, totalNo: "nil"),sortClass(key: "A", value: "Account and Auditing", isClicked: false, totalNo: "nil"),sortClass(key: "M", value: "Management", isClicked: false, totalNo: "nil"),sortClass(key: "H", value: "Human Resources and Recruitment", isClicked: false, totalNo: "nil"),sortClass(key: "T", value: "Teaching and Academics", isClicked: false, totalNo: "nil"),sortClass(key: "A", value: "Administration", isClicked: false, totalNo: "nil"),sortClass(key: "M", value: "Medical,Healthcare,and Nursing", isClicked: false, totalNo: "nil"),sortClass(key: "F", value: "Finance and Investment", isClicked: false, totalNo: "nil"),sortClass(key: "SS", value: "Support Services", isClicked: false, totalNo: "nil"),sortClass(key: "QC", value: "Quality Control", isClicked: false, totalNo: "nil"),sortClass(key: "L&T", value: "Logistics and Transportation", isClicked: false, totalNo: "nil"),sortClass(key: "M", value: "Maintenence,Repair and Technician", isClicked: false, totalNo: "nil"),sortClass(key: "H&T", value: "Hospitality and Tourism", isClicked: false, totalNo: "nil"),sortClass(key: "PP", value: "Purchasing and Procurement", isClicked: false, totalNo: "nil"),sortClass(key: "S", value: "Secretarial", isClicked: false, totalNo: "nil"),sortClass(key: "DCA", value: "Design,Creative and Arts", isClicked: false, totalNo: "nil"),sortClass(key: "CS", value: "Customer Service andCall Center", isClicked: false, totalNo: "nil"),sortClass(key: "ME", value: "Mechanical Engineering", isClicked: false, totalNo: "nil"),sortClass(key: "CB", value: "Construction and Building", isClicked: false, totalNo: "nil"),sortClass(key: "S", value: "Safety", isClicked: false, totalNo: "nil"),sortClass(key: "C", value: "Civil Engineering", isClicked: false, totalNo: "nil"),sortClass(key: "O&G", value: "Oil and Gas", isClicked: false, totalNo: "nil"),sortClass(key: "R&D", value: "Research and Development", isClicked: false, totalNo: "nil"),sortClass(key: "EE", value: "Electrical Engineering", isClicked: false, totalNo: "nil"),sortClass(key: "L", value: "Legal", isClicked: false, totalNo: "nil"),sortClass(key: "C", value: "Consulting", isClicked: false, totalNo: "nil"),sortClass(key: "M", value: "Manufacturing", isClicked: false, totalNo: "nil"),sortClass(key: "Cs", value: "Cleaning services", isClicked: false, totalNo: "nil"),sortClass(key: "Arc", value: "Architectture", isClicked: false, totalNo: "nil"),sortClass(key: "T&D", value: "Training and Development", isClicked: false, totalNo: "nil"),sortClass(key: "B&F", value: "Beauty and Fashion", isClicked: false, totalNo: "nil"),sortClass(key: "Sec", value: "Security", isClicked: false, totalNo: "nil"),sortClass(key: "C&E", value: "Chemical Engineering", isClicked: false, totalNo: "nil"),sortClass(key: "T", value: "Translation", isClicked: false, totalNo: "nil"),sortClass(key: "WJ", value: "Writing and Journalism", isClicked: false, totalNo: "nil"),sortClass(key: "CS", value: "Community Services", isClicked: false, totalNo: "nil"),sortClass(key: "FA", value: "Farming and Agriculture", isClicked: false, totalNo: "nil"),sortClass(key: "B", value: "Banking", isClicked: false, totalNo: "nil")]

               
                //navigate.rolearray = [sortClass(key: optionvalId, value: optionvalName, isClicked: false, totalNo: "nil")]
               
               navigate.genderarray = [sortClass(key: "M", value: "Male", isClicked: false, totalNo: "nil"),sortClass(key: "F", value: "Female", isClicked: false, totalNo: "nil"),sortClass(key: "N", value: "No preference", isClicked: false, totalNo: "nil")]
        
                navigate.jobcatId = jobCatString
               
               navigate.showDynamicType = false
               navigate.showLocation = false
               navigate.showCondition = false
               navigate.showType = false
               navigate.showFilter = false
               navigate.showPrice = false
               navigate.showDistance = true
                navigate.showRole = true
                navigate.showGender = true

               self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    @objc func filterButtonActionOne(sender : UIButton)
    {
        print("Sort Button Action Two")
        
        self.ShowPickerView.isHidden = false
        
        self.view.bringSubviewToFront(self.ShowPickerView)
    }
    
    @objc func filterViewCloseAction(sender : UIButton)
    {
        self.filterView.isHidden = true
    }
    
    func setAPIData()
    {
        self.JobListingTableView.delegate = self
        
        self.JobListingTableView.dataSource = self
        
        self.JobListingTableView.reloadData()
        
        self.ListingCV.delegate = self
        
        self.ListingCV.dataSource = self
        
        self.ListingCV.reloadData()
    }
    
    func setData()
    {
        for i in 0..<self.JobDetailsArray.count
        {
            self.JobListingStructArray.append(JobListingStruct(JobID: "\((self.JobDetailsArray[i] as! NSDictionary)["id"] ?? "")", CompanyName: "\((self.JobDetailsArray[i] as! NSDictionary)["company_name"] ?? "")", JobTitle: "\((self.JobDetailsArray[i] as! NSDictionary)["title"] ?? "")", CompanyLogo: "\((self.JobDetailsArray[i] as! NSDictionary)["company_logo"] ?? "")", JobAddress: "\((self.JobDetailsArray[i] as! NSDictionary)["location"] ?? "")", PostedDate: "\((self.JobDetailsArray[i] as! NSDictionary)["posted_date"] ?? "")", ClosedDate: "\((self.JobDetailsArray[i] as! NSDictionary)["closes_date"] ?? "")", SelectedOptions: ((self.JobDetailsArray[i] as! NSDictionary)["selected_opt"] as! NSArray), WatchListStatus: "\((self.JobDetailsArray[i] as! NSDictionary)["watchlist_status"] ?? "")", JobCatID: "\((self.JobDetailsArray[i] as! NSDictionary)["job_cat"] ?? "")"))
            
       
            
        }
    }
    
    func AppendMapData()
    {
        self.globalDispatchgroup.enter()
        
        for i in 0..<self.JobDetailsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((self.JobDetailsArray[i] as! NSDictionary)["company_name"] ?? "")", ProductLatitude: Double(("\((self.JobDetailsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((self.JobDetailsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((self.JobDetailsArray[i] as! NSDictionary)["company_logo"] ?? "")", CurrencySymbol: "\((self.JobDetailsArray[i] as! NSDictionary)["address"] ?? "")", StartPrice: "\((self.JobDetailsArray[i] as! NSDictionary)["closes_date"] ?? "")", ReservePrice: "\((self.JobDetailsArray[i] as! NSDictionary)["address"] ?? "")", ProductID: "\((self.JobDetailsArray[i] as! NSDictionary)["id"] ?? "")", SellerImage: "\((self.JobDetailsArray[i] as! NSDictionary)["posted_date"] ?? "")", WatchlistStatus: "\((self.JobDetailsArray[i] as! NSDictionary)["watchlist_status"] ?? "")"))
        }
        
        self.globalDispatchgroup.leave()
        
        self.globalDispatchgroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
            //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.mapView.animate(to: cameraPosition)
            
            self.mapView.isMyLocationEnabled = true
            
            self.mapView.settings.myLocationButton = true
            
            for state in self.ProductOnMapArray {
                
                let state_marker = GMSMarker()
                
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                
                state_marker.title = state.ProductName
                
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                
                state_marker.map = self.mapView
                
                state_marker.userData = state
                
                self.markers.append(state_marker)
                
            }
        })
    }
    
    //MARK:- // GMSMapView Delegate Methods
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //        customInfoWindow?.removeFromSuperview()
        
        self.index = 0
//        self.DetailsView.isHidden = true
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
        //        let update = GMSCameraUpdate.zoom(by: 10)
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
        //        GmapView.moveCamera(update)
        mapView.animate(to: cameraPosition)
    }
    
    func centerInMarker(marker: GMSMarker) {
        
        var bounds = GMSCoordinateBounds()
        
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        
        mapView?.moveCamera(updateOne)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = mapView.selectedMarker {
            
            selectedMarker.icon = GMSMarker.markerImage(with: nil)
        }
        mapView.selectedMarker = marker
        
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        return true
    }
    
    
    //MARK:- //UITableView Delegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return JobListingStructArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.SelectedInt = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "joblistingtvc") as! NewJobListingTVC
        
        cell.WatchListBtnOutlet.tag = indexPath.row
        
        cell.WatchlistedBtnOutlet.tag = indexPath.row
        
        cell.WatchListBtnOutlet.addTarget(self, action: #selector(self.JobWatchlist(sender:)), for: .touchUpInside)
        
        cell.WatchlistedBtnOutlet.addTarget(self, action: #selector(self.JobWatchlisted(sender:)), for: .touchUpInside)
        
        if self.jobCatString == "1038"
        {
            cell.JobType.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "open_vacancies", comment: "")
        }
        else
        {
            cell.JobType.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "job_wanted", comment: "")
        }
        
        cell.JobFeaturesCollectionView.backgroundColor = .white
        
        cell.JobName.text = "\(self.JobListingStructArray[indexPath.row].JobTitle ?? "")"
        
        cell.JobAddress.text = "\(self.JobListingStructArray[indexPath.row].JobAddress ?? "")"
        
        let path = "\(self.JobListingStructArray[indexPath.row].CompanyLogo ?? "")"
        
        cell.JobImage.sd_setImage(with: URL(string: path))
        
        cell.JobPoster.text = "\(self.JobListingStructArray[indexPath.row].CompanyName ?? "")"
        
        cell.JobPostedTime.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted", comment: "") + " : " + "\(self.JobListingStructArray[indexPath.row].PostedDate ?? "")"
        
        cell.JobClosingTime.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "closes", comment: "") + " : " + "\(self.JobListingStructArray[indexPath.row].ClosedDate ?? "")"
        
        cell.ShadowView.layer.cornerRadius = 8.0
        
        cell.ContentView.layer.cornerRadius = 8.0
        
        cell.JobName.textColor = .darkText
        
        cell.JobAddress.textColor = UIColorFromRGB(rgbValue: 0x7e7e7e)
        
        cell.JobType.textColor = UIColorFromRGB(rgbValue: 0x00c344)
        
        cell.JobPostedTime.textColor = UIColorFromRGB(rgbValue: 0x00c344)
        
        cell.JobPoster.textColor = UIColorFromRGB(rgbValue: 0x7e7e7e)
        
        cell.JobClosingTime.textColor = .red
        
        cell.JobName.font = UIFont(name: "Lato-Light", size: CGFloat(Get_fontSize(size: 18)))
        
        cell.JobType.font = UIFont(name: "Lato-Light", size: CGFloat(Get_fontSize(size: 16)))
        
        if "\(self.JobListingStructArray[indexPath.row].WatchListStatus ?? "")".elementsEqual("0")
        {
            cell.WatchListBtnOutlet.isHidden = false
            
            cell.WatchlistedBtnOutlet.isHidden = true
        }
        else if "\(self.JobListingStructArray[indexPath.row].WatchListStatus ?? "")".elementsEqual("1")
        {
            cell.WatchListBtnOutlet.isHidden = true
            
            cell.WatchlistedBtnOutlet.isHidden = false
        }
        else
        {
            cell.WatchListBtnOutlet.isHidden = false
            
            cell.WatchlistedBtnOutlet.isHidden = true
        }
        
        cell.selectionStyle = .none
        
        cell.JobFeaturesCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
//        cell.JobFeaturesCollectionView.contentOffset.x = cell.ContentView.frame.origin.x + CGFloat(8.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? NewJobListingTVC else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        //        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobdetails") as! NewJobDetailsViewController
        
        navigate.JobId = "\(self.JobListingStructArray[indexPath.row].JobID ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getListing()
    {
        
        print("MaxDist----",maxFilterDistance!)
        print("jobcatid-----",jobCatString)
        
        let parameters = "lang_id=\(langID!)&user_id=\(userID!)&start_value=\(startValue)&per_load=\(perLoad)&mycountry_name=\(MyCountryName!)&jobcatid=\(jobCatString!)&search_by=\(searchBy!)&state=\(state!)&city=\(city!)&min_dist=\(Mindist!)&max_dist=\(maxFilterDistance!)&search_option2=\(SearchOptionTwo!)&search_option1=\(SearchOptionOne!)&sort_search=\(sortType!)"
        
        self.CallAPI(urlString: "app_job_search_listing", param: parameters, completion: {
            
            self.JobInfoDict = self.globalJson["job_info"] as? NSDictionary
            
            print("jobinfodict------",self.JobInfoDict)
            
            self.JobDetailsArray = self.JobInfoDict["job_details"] as! NSMutableArray
            
            self.OptionDetailsArray = self.JobInfoDict["opt_details"] as! NSMutableArray
            
            //self.OptionValueArray = self.OptionDetailsArray!["opt_value"]as! NSMutableArray
            
             print("JobDetailsArray------",self.JobDetailsArray)
            
            print("OptionDetailsArray------",self.OptionDetailsArray)
            
             print("OptionValueArray------",self.OptionValueArray)
            
            for i in (0..<self.OptionDetailsArray.count)
            {
                self.OptionValueArray = ((self.OptionDetailsArray[i] as! NSDictionary)["opt_value"]!) as? NSMutableArray
                print("OptionValueArray======",self.OptionValueArray)
                
                self.optionvalId =  "\(((self.OptionValueArray[i] as! NSDictionary)["opt_value_id"]! ?? ""))"
                 print("optionvalId======",self.optionvalId)
                
                self.optionvalName =  "\(((self.OptionValueArray[i] as! NSDictionary)["opt_value_name"]! ?? ""))"
                print("opt_value_name======",self.optionvalName)
                 
            }
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                SVProgressHUD.dismiss()
                
                self.AppendMapData()
                
                self.setData()
            }
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.setAPIData()
            })
            
        })
    }
    
    @objc func JobWatchlist(sender : UIButton)
    {
        self.GlobalAddtoWatchlist(ProductID: "\(self.JobListingStructArray[sender.tag].JobID ?? "")", completion: {
            
        })
        
         self.JobListingStructArray = []
        
         self.ProductsOnMapArray = []
        
         self.getListing()
                    
    }
    
    @objc func JobWatchlisted(sender : UIButton)
    {
        self.GlobalRemoveFromWatchlist(ProductID: "\(self.JobListingStructArray[sender.tag].JobID ?? "")", completion: {
    
        })
           self.JobListingStructArray = []
        
           self.ProductsOnMapArray = []
        
           self.getListing()
            
    }
    
   
    }


class NewJobListingTVC : UITableViewCell
{
    
    @IBOutlet weak var WatchListBtnOutlet: UIButton!
    
    @IBOutlet weak var WatchlistedBtnOutlet: UIButton!
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var JobImage: UIImageView!
    
    @IBOutlet weak var JobName: UILabel!
    
    @IBOutlet weak var JobPoster: UILabel!
    
    @IBOutlet weak var JobType: UILabel!
    
    @IBOutlet weak var JobAddress: UILabel!
    
    @IBOutlet weak var JobFeaturesCollectionView: UICollectionView!
    
    @IBOutlet weak var JobPostedTime: UILabel!
    
    @IBOutlet weak var JobClosingTime: UILabel!
    
    @IBOutlet weak var ShadowView: ShadowView!
    
}

class JobListingOptionCVC : UICollectionViewCell
{
    
    @IBOutlet weak var OptionName: UILabel!
}


extension NewJobListingViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    //MARK:- //UICollectionView Delegate and DataSource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == self.ListingCV
        {
            return 1
        }
        else
        {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.ListingCV
        {
            return self.JobListingStructArray.count
        }
            
        else
        {
            return self.JobListingStructArray[SelectedInt].SelectedOptions.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.ListingCV
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "joblisting", for: indexPath) as! NewJobListingCVC
            
            cell.JobName.text = "\(self.JobListingStructArray[indexPath.row].JobTitle ?? "")"
            
            cell.JobAddress.text = "\(self.JobListingStructArray[indexPath.row].JobAddress ?? "")"
            
            cell.ListedTime.text = "\(self.JobListingStructArray[indexPath.row].PostedDate ?? "")"
            
            cell.ExpiredTime.text = "\(self.JobListingStructArray[indexPath.row].ClosedDate ?? "")"
            
            cell.JobPosterName.text = "\(self.JobListingStructArray[indexPath.row].CompanyName ?? "")"
            
            //cell.JobType.text = "\(self.JobListingStructArray[indexPath.row]. ?? "")"
            
            cell.JobType.text = "Job Type : Wanted"
            
            cell.ProductImage.sd_setImage(with: URL(string: "\(self.JobListingStructArray[indexPath.row].CompanyLogo ?? "")"))
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "joblistingcvc", for: indexPath) as! JobListingOptionCVC
            
            cell.OptionName.text = "\(((self.JobListingStructArray[SelectedInt].SelectedOptions[indexPath.row] as! NSDictionary)["value_name"] ?? ""))"
            
            cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == ListingCV
        {
            let padding : CGFloat! = 4
            
            let collectionViewSize = self.ListingCV.frame.size.width - padding
            
            return CGSize(width: collectionViewSize/2, height: 249 )
        }
        else
        {
            let size: CGSize = "\(((self.JobListingStructArray[SelectedInt].SelectedOptions[indexPath.row] as! NSDictionary)["value_name"] ?? ""))".size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
            
            return CGSize(width: size.width + 60, height: 35)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.ListingCV
        {
             return 0
        }
        else
        {
             return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.ListingCV
        {
            return 0
        }
        else
        {
            return 2
        }
    }
}

struct JobListingStruct
{
    var JobID : String?
    
    var CompanyName : String?
    
    var JobTitle : String?
    
    var CompanyLogo : String?
    
    var JobAddress : String?
    
    var PostedDate : String?
    
    var ClosedDate : String?
    
    var SelectedOptions : NSArray!
    
    var WatchListStatus : String?
    
    var JobCatID : String?
    
    
    
    
    
    init(JobID : String , CompanyName : String , JobTitle : String , CompanyLogo : String , JobAddress : String , PostedDate : String
        ,ClosedDate : String , SelectedOptions : NSArray , WatchListStatus : String , JobCatID : String )
    {
        self.JobID = JobID
        
        self.CompanyName = CompanyName
        
        self.JobTitle = JobTitle
        
        self.CompanyLogo = CompanyLogo
        
        self.JobAddress  = JobAddress
        
        self.PostedDate = PostedDate
        
        self.ClosedDate = ClosedDate
        
        self.SelectedOptions = SelectedOptions
        
        self.WatchListStatus = WatchListStatus
        
        self.JobCatID = JobCatID
        
       
    }
}

extension NewJobListingTVC {
    
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        JobFeaturesCollectionView.delegate = dataSourceDelegate
        
        JobFeaturesCollectionView.dataSource = dataSourceDelegate
        
        JobFeaturesCollectionView.tag = row
       
        JobFeaturesCollectionView.reloadData()
        //JobFeaturesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
    }
    

}

extension NewJobListingViewController : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.SortDict.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       // return "\((self.SortDict[row] as NSDictionary)["key"] ?? "")"
        
                   
        return "\((SortDict[row] as NSDictionary)["value"]!)"
                     
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
     
            self.sortType = "\((self.SortDict[row] as NSDictionary)["key"]!)"
            self.ShowPickerView.isHidden = true
            self.filterView.isHidden = true
            self.startValue = 0
            self.JobListingStructArray = []
          
            self.getListing()
       
    }
}


class NewJobListingCVC : UICollectionViewCell
{
    
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var ProductImage: UIImageView!
    
    @IBOutlet weak var JobName: UILabel!
    
    @IBOutlet weak var JobPosterName: UILabel!
    
    @IBOutlet weak var ListedTime: UILabel!
    
    @IBOutlet weak var ExpiredTime: UILabel!
    
    @IBOutlet weak var JobType: UILabel!
    
    @IBOutlet weak var JobAddress: UILabel!
    
    @IBOutlet weak var JobWatchlistButton: UIButton!
}

