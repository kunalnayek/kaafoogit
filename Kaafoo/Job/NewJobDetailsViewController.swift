//
//  NewJobDetailsViewController.swift
//  Kaafoo
//
//  Created by esolz on 26/09/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import Alamofire

class NewJobDetailsViewController: GlobalViewController {
    
    
    @IBOutlet weak var accountTypeImage: UIImageView!
    
    
    @IBOutlet weak var ApplyBtnOutlet: UIButton!
    
    var operationQueue : OperationQueue!
    
    var JobId : String! = "368"
    
    var JobTypeID : String! = "1"
    
    var JobDetailsDictionary : NSDictionary!
    
    var SelectedOptionArray : NSArray!
    
    var SelectedOptions : NSMutableArray! = NSMutableArray()
    
    var QuestionAnswerArray : NSMutableArray! = NSMutableArray()
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var OpenVacancies : Bool! = false
    
    var pdfstring : NSData!
    
    var tempData = Data()
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    
    @IBOutlet weak var LocationHeaderStaticLbl: UILabel!
    
    @IBOutlet weak var ListingStaticLbl: UILabel!
    
    @IBOutlet weak var QuestionsandAnswersStaticLbl: UILabel!
    
    @IBOutlet weak var MainScrollTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var MainScrollBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var FeaturesTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var MainScroll: UIScrollView!
    
    @IBOutlet weak var ScrollContentView: UIView!
    
    @IBOutlet weak var JobDetailsView: UIView!
    
    @IBOutlet weak var CredintialView: UIView!
    
    @IBOutlet weak var DescriptionHeaderView: UIView!
    
    @IBOutlet weak var DescriptionView: UIView!
    
    @IBOutlet weak var FeaturesTableView: UITableView!
    
    @IBOutlet weak var QuestionsAnswersHeaderView: UIView!
    
    @IBOutlet weak var QuestionsAnswerStackView: UIStackView!
    
    @IBOutlet weak var BrowseCategoryHeaderView: UIView!
    
    @IBOutlet weak var BreadCrumpView: UIView!
    
    @IBOutlet weak var PostedByStackView: UIStackView!
    
    @IBOutlet weak var JobImageView: UIImageView!
    
    @IBOutlet weak var JobName: UILabel!
    
    @IBOutlet weak var JobSellerName: UILabel!
    
    @IBOutlet weak var JobType: UILabel!
    
    @IBOutlet weak var JobLocation: UILabel!
    
    @IBOutlet weak var FeaturesCollectionView: UICollectionView!
    
    @IBOutlet weak var JobPostedDate: UILabel!
    
    @IBOutlet weak var JobClosingDate: UILabel!
    
    @IBOutlet weak var JobWatchlistBtnOutlet: UIButton!
    
    
    @IBOutlet weak var JobWatchlistedBtnOutlet: UIButton!
    
    @IBOutlet weak var JobViews: UILabel!
    
    @IBOutlet weak var JobWatchlistQuantity: UILabel!
    
    @IBOutlet weak var JobNumber: UILabel!
    
    
    @IBOutlet weak var DescriptionHeaderName: UILabel!
    
    @IBOutlet weak var JobDescription: UILabel!
    
    
    //Posted By StackViews Outlet
    @IBOutlet weak var PostedByHeaderView: UIView!
    
    @IBOutlet weak var PostedByDetailsView: UIView!
    
    @IBOutlet weak var LocationHeaderView: UIView!
    
    @IBOutlet weak var LoctionStackView: UIStackView!
    
    @IBOutlet weak var MyAccountView: UIView!
    
    @IBOutlet weak var PostedByStaticLbl: UILabel!
    
    @IBOutlet weak var JobFeedbackBack: UILabel!
    
    @IBOutlet weak var JobMemberSince: UILabel!
    
    @IBOutlet weak var JobRating: UILabel!
    
    @IBOutlet weak var JobPostedLocation: UILabel!
    
    @IBOutlet weak var JobContact: UILabel!
    
    
    @IBOutlet weak var JobDetailsQNATableView: UITableView!
    
    @IBOutlet weak var JobDetailsQNATableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var AllQuestionsHeaderView: UIView!
    
    @IBOutlet weak var AllQuestionsBtnOutlet: UIButton!
    
    @IBAction func AllQuestionBtn(_ sender: UIButton) {
    }
    @IBOutlet weak var NoQuestionAnswerStaticLbl: UILabel!
    
    @IBOutlet weak var BrowseCategoryStaticLbl: UILabel!
    
    @IBOutlet weak var BrowseCategory: UILabel!
    
    @IBAction func BrowseCategoryBtnOutlet(_ sender: UIButton) {
    }
    @IBOutlet weak var BrowseCategoryOutlet: UIButton!
    
    @IBOutlet weak var ReportImageView: UIImageView!
    
    @IBOutlet weak var ReportStaticLabel: UILabel!
    
    @IBOutlet weak var ReportBtnOutlet: UIButton!
    
    @IBAction func ReportBtn(_ sender: UIButton) {
    }
    
    @IBOutlet weak var JobSendMessageBtnOutlet: UIButton!
    
    @IBOutlet weak var JobViewAccountBtnOutlet: UIButton!
    
    @IBAction func JobSendMessageBtn(_ sender: UIButton) {
    }
    
    @IBAction func JobApplyBtn(_ sender: UIButton) {
        
        if self.JobApplyView.isHidden == true
        {
            self.tempData.removeAll()
            
            self.JobApplyView.isHidden = false
            
            self.view.bringSubviewToFront(self.JobApplyView)
        }
        else
        {
            self.JobApplyView.isHidden = true
            
            self.view.sendSubviewToBack(self.JobApplyView)
        }
    }
    
    @IBOutlet weak var JobApplyView: UIView!
    
    
    @IBOutlet weak var JobApplierName: UITextField!
    
    @IBOutlet weak var JobApplierEmail: UITextField!
    
    @IBOutlet weak var JobApplierPhone: UITextField!
    
    @IBOutlet weak var JobMessageTxtView: UITextView!
    
    @IBOutlet weak var ApplyBtn: UIButton!
    
    @IBOutlet weak var CancelBtn: UIButton!
    
    @IBOutlet weak var NameView: UIView!
    
    @IBOutlet weak var EmailView: UIView!
    
    @IBOutlet weak var PhoneView: UIView!
    
    @IBAction func ViewApplyBtn(_ sender: UIButton) {
        
        //self.clickDocumentPicker()
        
        if self.tempData.isEmpty == true
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_select_file", comment: ""))
        }
        else
        {
            if self.JobApplierName.text?.isEmpty == true
            {
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_name", comment: ""))
            }
            else if self.JobApplierEmail.text?.isEmpty == true
            {
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_email", comment: ""))
            }
            else if self.JobApplierPhone.text?.isEmpty == true
            {
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_phone_number", comment: ""))
            }
            else if self.JobMessageTxtView.text.isEmpty == true
            {
               self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_leave_your_message", comment: ""))
            }
            else
            {
                //do nothing
                print("Success")
                
                self.ApplyJob()
                
            }
        }
        
    }
    
    @IBAction func ViewCancelBtn(_ sender: UIButton) {
        
        self.clickDocumentPicker()
        
    }
    
    
    
    @IBAction func JobViewAccountBtn(_ sender: UIButton) {
        
        if "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["seller_type"]!)".elementsEqual("B") {
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            
            navigate.sellerID = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else {
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            
            navigate.sellerID = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    @IBOutlet weak var JobFollowBtnOutlet: UIButton!
    
    @IBOutlet weak var JobPosterName: UILabel!
    
    @IBAction func JobFollowBtn(_ sender: UIButton) {
        self.JobFollowBtnOutlet.isSelected = !self.JobFollowBtnOutlet.isSelected
        
        if self.JobFollowBtnOutlet.titleLabel?.text == "Follow"
        {
            self.followSeller()
            
            self.JobFollowBtnOutlet.setTitle("Unfollow", for: .normal)
        }
        else
        {
            self.unfollowSeller()
            
            self.JobFollowBtnOutlet.setTitle("Follow", for: .normal)
        }

    }
    @IBOutlet weak var JobPosterImage: UIImageView!
    
    @IBOutlet weak var JobPosterAccountStaticLbl: UILabel!
    
    
    var customView = UIView()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.JobFollowBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Follow", comment: ""), for: .normal)
        
        //self.job.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Follow", comment: ""), for: .normal)
               
        
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight))
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        
        self.setLanguage()
        
        self.setLayer()
        
        self.JobWatchlistBtnOutlet.addTarget(self, action: #selector(self.JobWatchlist(sender:)), for: .touchUpInside)
        
        self.JobWatchlistedBtnOutlet.addTarget(self, action: #selector(self.JobWatchlisted(sender:)), for: .touchUpInside)
        
        self.FeaturesCollectionView.backgroundColor = .white
        
        self.FeaturesTableView.isScrollEnabled = false
        
        self.getDetails()
        
        
        //self.MainScroll.frame.size.height = 0
       

        // Do any additional setup after loading the view.
    }
    
    func ApplyJob()
    {
     
        self.globalDispatchgroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
            }
            
            let langID = UserDefaults.standard.string(forKey: "langID")
            
            let parameters = [
                "lang_id" : "\(self.LangID!)" , "user_id" : "\(self.UserID!)" ,"job_type" : "\(self.JobTypeID!)","job_id" : "\(self.JobId!)","aplysend_msg" : "\(self.JobMessageTxtView.text ?? "")" , "aplysend_name" : "\(self.JobApplierName.text!)" ,"aplysend_phn" : "\(self.JobApplierPhone.text!)","aplysend_link" : "\(self.JobApplierEmail.text!)"]
            
            Alamofire.upload(
                multipartFormData: { MultipartFormData in
                    
                    for (key, value) in parameters {
                        MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                 
                 print("Tempdata",self.tempData ?? "")
                 
                MultipartFormData.append(self.tempData, withName: "job_upload_img[]", fileName: "iosPDF.pdf", mimeType: "application/pdf")
                    
            }, to: (GLOBALAPI + "app_user_send_message"))
             
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                     
                     print("response",response)
    //
                        print(response.request!)  // original URL request
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result)   // result of response serialization

                        if let JSON = response.result.value {
                            
                            print("PDF Response Response-----: \(JSON)")
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                             
                             self.displayToastMessage(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Successfully Send Job Offer", comment: ""))
                             
                             self.JobMessageTxtView.text = ""
                                
                                self.JobApplierName.text = ""
                 
                                self.JobApplierEmail.text = ""
                                
                                self.JobApplierPhone.text = ""
                                
                                self.JobMessageTxtView.text = ""
                                
                                self.JobApplyView.isHidden = true
                                
                                self.view.sendSubviewToBack(self.JobApplyView)
                                
                                
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                
                                self.globalDispatchgroup.leave()
                                
                                SVProgressHUD.dismiss()
                                self.customView.removeFromSuperview()
                             
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    
                    DispatchQueue.main.async {
                        
                        self.globalDispatchgroup.leave()
                        
                        SVProgressHUD.dismiss()
                        self.customView.removeFromSuperview()
                    }
                }
            }
        }
    
    func clickDocumentPicker(){

       let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
        
       documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        
       documentPicker.delegate = self
        
       present(documentPicker, animated: true, completion: nil)
        
       }

    func setLayer()
    {
        self.JobMessageTxtView.text = ""
        
        self.NameView.layer.cornerRadius = 5.0
        
        self.NameView.layer.borderWidth = 1.0
        
        self.EmailView.layer.cornerRadius = 5.0
        
        self.EmailView.layer.borderWidth = 1.0
        
        self.PhoneView.layer.cornerRadius = 5.0
        
        self.PhoneView.layer.borderWidth = 1.0
        
        self.JobMessageTxtView.layer.cornerRadius = 5.0
        
        self.JobMessageTxtView.layer.borderWidth = 1.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideApplyView(sender:)))
        
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideApplyView(sender : UITapGestureRecognizer)
    {
        self.JobApplyView.isHidden = true
        
        self.view.sendSubviewToBack(self.JobApplyView)
        
    }
    
    
    
    //MARK:- //MARK:- //Set Dynamic Language
       
       func setLanguage()
       {
        self.QuestionsandAnswersStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questionsAndAnswers", comment: "")
        
        self.AllQuestionsBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_all_questions_and_answers", comment: ""), for: .normal)
        
        self.BrowseCategoryStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "browseCategory", comment: "")
        
        self.PostedByStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")
        self.JobViewAccountBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)
        
        self.JobSendMessageBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
        
        self.LocationHeaderStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")
        
        self.DescriptionHeaderName.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
    self.ApplyBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Apply", comment: ""), for: .normal)
        
        self.ApplyBtnOutlet.titleLabel?.font = UIFont(name: "Lato-Regular", size: 18.0)
        
        self.JobApplierName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_name", comment: "")
        
        self.JobApplierEmail.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_email", comment: "")
        
        self.JobApplierPhone.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter_your_phone_number", comment: "")
        
    self.ApplyBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "apply", comment: ""), for: .normal)
        
       self.CancelBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload", comment: ""), for: .normal)
        
        
       }
       
    
    func setJobDetails()
    {
        
        if "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["profile_img"] ?? "")".elementsEqual("0")
        {
            self.JobWatchlistBtnOutlet.isHidden = false
            
            self.JobWatchlistedBtnOutlet.isHidden = true
        }
        else if "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["profile_img"] ?? "")".elementsEqual("1")
        {
            self.JobWatchlistBtnOutlet.isHidden = true
            
            self.JobWatchlistedBtnOutlet.isHidden = false
        }
        else
        {
            self.JobWatchlistBtnOutlet.isHidden = false
            
            self.JobWatchlistedBtnOutlet.isHidden = true
        }
        
        
        self.JobImageView.sd_setImage(with: URL(string: "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["profile_img"] ?? "")"))
        
        self.JobName.text = "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["company_name"] ?? "")"
        
        self.JobSellerName.text = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["display_name"] ?? "")"
        
        self.JobType.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "open_vacancies", comment: "")
        
        self.JobLocation.text = "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["job_location"] ?? "")"
        
        self.JobPostedDate.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted", comment: "") + " : " + "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["posted_date"] ?? "")"
        
        self.JobClosingDate.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "closes", comment: "") + " : " + "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["posted_date"] ?? "")"
        
        let descriptionText = "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["description"] ?? "")".removeWhitespace()
        
        //self.JobDescription.text = "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["description"] ?? "")"
        
        self.JobDescription.text = "\(descriptionText)"
        
        self.JobPosterName.text = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["display_name"] ?? "")"
        
        let imageURL = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["logo"] ?? "")"
        
        self.JobPosterImage.sd_setImage(with: URL(string: imageURL))
        
        self.JobFeedbackBack.text = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["positive_feedback"] ?? "")" + "%" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "positiveFeedback", comment: "")
        
        self.JobMemberSince.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "memberSince", comment: "") + " : " + "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["member_since"] ?? "")"
        
        self.JobPostedLocation.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "") + " : " + "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["location"] ?? "")"
        
        self.JobContact.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "") + " : " + "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["mobile"] ?? "")"
        
        self.JobRating.text = "\((self.JobDetailsDictionary["seller_info"] as! NSDictionary)["avg_rating"] ?? "")"
        
        //self.JobViews.text = "\(self.JobDetailsDictionary["count_view"] ?? "")"
        
    }
    
    @objc func JobWatchlist(sender : UIButton)
    {
        self.GlobalAddtoWatchlist(ProductID: "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["job_id"] ?? "")", completion: {
            
            self.JobWatchlistBtnOutlet.isHidden = true
            
            self.JobWatchlistedBtnOutlet.isHidden = false
        })
    }
    
    @objc func JobWatchlisted(sender : UIButton)
    {
        self.GlobalRemoveFromWatchlist(ProductID: "\((self.JobDetailsDictionary["job_info"] as! NSDictionary)["job_id"] ?? "")", completion: {
            
            self.JobWatchlistBtnOutlet.isHidden = false
            
            self.JobWatchlistedBtnOutlet.isHidden = true
            
        })
    }
    
    func setColorAndFonts()
    {
        self.DescriptionView.backgroundColor = .white
        
        self.JobDetailsView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.JobDetailsView.layer.borderWidth = 1.0
        
        self.JobDetailsView.layer.cornerRadius = 5
        
        self.JobDetailsView.clipsToBounds = true
        
        self.JobDetailsView.layer.masksToBounds = true
        
        self.JobPostedLocation.numberOfLines = 0
        
        self.DescriptionHeaderView.backgroundColor = .DeepWhite()
        
        self.QuestionsAnswersHeaderView.backgroundColor = .DeepWhite()
        
        self.BrowseCategoryHeaderView.backgroundColor = .DeepWhite()
        
        self.PostedByHeaderView.backgroundColor = .DeepWhite()
        
        self.LocationHeaderView.backgroundColor = .DeepWhite()
        
        self.JobPostedDate.textColor = .seaGreen()
        
        self.JobClosingDate.textColor = .darkred()
        
        self.JobType.textColor = .seaGreen()
        
        self.ReportBtnOutlet.setTitleColor(.blue, for: .normal)
        
        self.DescriptionHeaderName.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
        
        self.QuestionsandAnswersStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questions_&_answers", comment: "")
        
        self.BrowseCategoryStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "browseCategory", comment: "")
        
        self.PostedByStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")
        
        self.JobViews.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Views", comment: "") + " : " + "\(self.JobDetailsDictionary["count_view"] ?? "")"
        
        self.JobWatchlistQuantity.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: "") + " : " + "\(self.JobDetailsDictionary["count_watchlist"] ?? "")"
        
        self.LocationHeaderStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")
        
        self.ListingStaticLbl.text = "#" + LocalizationSystem.sharedInstance.localizedStringForKey(key: "listing", comment: "")
        self.JobViewAccountBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)
        self.JobSendMessageBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
        self.AllQuestionsBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_all_questions_and_answers", comment: ""), for: .normal)
        
        self.NoQuestionAnswerStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_questions_posted_yet", comment: "")
        
        if (langID?.elementsEqual("ar"))!
        {
            self.BrowseCategoryOutlet.setImage(UIImage(named: "right arrow"), for: .normal)
        }
        else
        {
            self.BrowseCategoryOutlet.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        }
        
        
    }
    
    func getDetails()
    {
        let parameter = "job_id=\(JobId!)&lang_id=\(LangID!)"
        
        self.CallAPI(urlString: "app_job_details", param: parameter, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.JobDetailsDictionary = self.globalJson["job_details"] as? NSDictionary
                
                self.SelectedOptionArray = ((self.JobDetailsDictionary["job_info"] as! NSDictionary)["select_otp"] as! NSArray)
                
//                let WantedDetailsArray = (self.JobDetailsDictionary["wanted_details"] as! NSArray)
//                for i in 0..<WantedDetailsArray.count
//                {
//                    let dict = WantedDetailsArray[i] as! NSDictionary
//                    self.SelectedOptionArray.adding(dict)
//                }
                
                if (self.JobDetailsDictionary["wanted_details"]! as! NSArray).count > 0
                {
                   // self.SelectedOptions = (((self.JobDetailsDictionary["wanted_details"] as! NSArray)[0] as! NSMutableArray))
                }
                else
                {
                    //do nothing
                }
                
                if self.LangID!.elementsEqual("AR")
                {
                    
                    self.accountTypeImage.image = UIImage(named: "button-2")
                    
                    
                }
                    
                else
                {
                    self.accountTypeImage.image = UIImage(named: "button-1")
                }
                
                if "\((self.JobDetailsDictionary["seller_info"]! as! NSDictionary)["seller_type"] ?? "")".elementsEqual("B")
                {
                     self.JobPosterAccountStaticLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_account", comment: "")
                }
                
                else
                {
                    self.JobPosterAccountStaticLbl
                         .text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "private_account", comment: "")
                }
                
                self.QuestionAnswerArray = (self.JobDetailsDictionary["qsn_ans"] as! NSMutableArray)
                
                DispatchQueue.main.async {
                    
                    self.setJobDetails()
                    
                    self.setColorAndFonts()
                    
                   self.FeaturesCollectionView.delegate = self
                    
                   self.FeaturesCollectionView.dataSource = self
                    
                   self.FeaturesCollectionView.reloadData()
                    
                   if self.QuestionAnswerArray.count == 0
                   {
                    DispatchQueue.main.async {
                        
                        self.JobDetailsQNATableView.isHidden = true
                        
                        self.AllQuestionsHeaderView.isHidden = true
                        
                        self.NoQuestionAnswerStaticLbl.isHidden = false
                        
                    }
                   }
                   else
                   {
                    DispatchQueue.main.async {
                        
                        self.JobDetailsQNATableView.isHidden = false
                        
                        self.AllQuestionsHeaderView.isHidden = false
                        
                        self.NoQuestionAnswerStaticLbl.isHidden = true
                    }
                    
                     self.JobDetailsQNATableView.delegate = self
                    
                     self.JobDetailsQNATableView.dataSource = self
                    
                     self.JobDetailsQNATableView.reloadData()
                    
                     self.SetTableheight(table: self.JobDetailsQNATableView, heightConstraint: self.JobDetailsQNATableViewHeightConstraint)
                    
                    
                    
                   }
                    
                    self.FeaturesTableView.delegate = self
                                       
                    self.FeaturesTableView.dataSource = self
                   
                    self.FeaturesTableView.reloadData()
                   
                    if self.SelectedOptions.count == 0
                    {
                       self.FeaturesTableHeightConstraint.constant = 0
                       
                       self.FeaturesTableView.isHidden = true
                    }
                    else
                    {
                       self.SetTableheight(table: self.FeaturesTableView, heightConstraint: self.FeaturesTableHeightConstraint)
                    }
                }
                SVProgressHUD.dismiss()
                self.customView.removeFromSuperview()
            })
        })
    }
    
    // MARK:- // Set Table Height
    
    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        
        var setheight: CGFloat  = 0
        
        table.frame.size.height = 3000
        
        for cell in table.visibleCells {
            
            setheight += cell.bounds.height
        }
        
        heightConstraint.constant = CGFloat(setheight)
    }
    
    //MARK:- //  JSON Post Method to Follow the Seller
    
    func followSeller()
    {
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&buss_seller_id=\("\((self.JobDetailsDictionary["seller_info"]! as! NSDictionary)["seller_id"] ?? "")")&lang_id=\(langID!)"
        
        self.CallAPI(urlString: "app_add_to_favourite", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                SVProgressHUD.dismiss()
                self.customView.removeFromSuperview()
                
                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")
                
            }
        }
    }
    
    
    
    // MARK:- // JSON Post method to Unfollow Seller
    
    
    func unfollowSeller() {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = "user_id=\(userID!)&lang_id=\(self.langID!)&remove_id=\("\((self.JobDetailsDictionary["seller_info"]! as! NSDictionary)["seller_id"] ?? "")")"
        
        self.CallAPI(urlString: "app_remove_my_favourite", param: parameters) {
            
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                SVProgressHUD.dismiss()
                self.customView.removeFromSuperview()
                
                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")
                
            }
        }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class FeaturesTVC : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var OptionName: UILabel!
    
    @IBOutlet weak var ValueName: UILabel!
    
    @IBOutlet weak var SeparatorView: UIView!
}

class FeaturesCCVC : UICollectionViewCell
{
    @IBOutlet weak var OptionName: UILabel!
}

extension NewJobDetailsViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.SelectedOptionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "features", for: indexPath) as! FeaturesCCVC
        
        cell.OptionName.text = "\((self.SelectedOptionArray[indexPath.row] as! NSDictionary)["value"] ?? "")"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let size: CGSize = "\((self.SelectedOptionArray[indexPath.row] as! NSDictionary)["value"] ?? "")".size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
        
        return CGSize(width: size.width + 60, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 2
    }
}

class JobDetailsQNATVC : UITableViewCell
{
    @IBOutlet weak var Question: UILabel!
    
    @IBOutlet weak var QuestionPoster: UILabel!
    
    @IBOutlet weak var QuestionPostingDate: UILabel!
    
    @IBOutlet weak var Answer: UILabel!
    
    @IBOutlet weak var AnswerPostingDate: UILabel!
    
    @IBOutlet weak var ReplyBtnOutlet: UIButton!
}

extension NewJobDetailsViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == self.JobDetailsQNATableView
        {
           return 1
        }
        else
        {
           return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.JobDetailsQNATableView
        {
            return self.QuestionAnswerArray.count
        }
        else if tableView == self.FeaturesTableView
        {
            return self.SelectedOptions.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.JobDetailsQNATableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "jobqna") as! JobDetailsQNATVC
            
            cell.Question.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["question"] ?? "")"
            
            cell.Answer.text = "\((self.QuestionAnswerArray[indexPath.row] as! NSDictionary)["answer"] ?? "")"
            
            return cell
        }
        else if tableView == self.FeaturesTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "features") as! FeaturesTVC
            
            cell.OptionName.text = "\((self.SelectedOptions[indexPath.row] as! NSDictionary)["type"] ?? "")"
            
            cell.ValueName.text = "\((self.SelectedOptions[indexPath.row] as! NSDictionary)["info"] ?? "")"
            
            cell.OptionName.textColor = UIColorFromRGB(rgbValue: 0xff7400)
            
            cell.SeparatorView.backgroundColor = .lightGray
            
            cell.selectionStyle = .none
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.JobDetailsQNATableView
        {
           return nil
        }
        else
        {
            let rect = CGRect(x: 0, y: 0, width: self.FeaturesTableView.frame.size.width, height: 50)
            
            let headerView = UIView(frame:rect)
            
            headerView.backgroundColor = UIColorFromRGB(rgbValue: 0xf9f9f9)
            
            let headerlabel = UILabel(frame: CGRect(x: 8, y: 14, width: 200, height: 24))
            
            headerlabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "job_details", comment: "")
            
            headerlabel.font = UIFont(name: "Lato-Bold", size: 16)
            
            headerView.addSubview(headerlabel)
            
            return headerView
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == self.FeaturesTableView
        {
           return 50
        }
        else
        {
            return 0
        }
    }
    
}

extension NewJobDetailsViewController : UIDocumentMenuDelegate,UIDocumentPickerDelegate
{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        print("do nothing")
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        do {

            let tempUrl = NSData(contentsOf: urls[0])
            
            let pdfString : String = tempUrl!.base64EncodedString(options: .endLineWithLineFeed)
            
            self.pdfstring = NSData(contentsOf: urls[0])
            
            self.tempData = Data(base64Encoded: pdfString, options: .ignoreUnknownCharacters)!
            
            //self.UploadToPdfServer()
            
        } catch _ {
            
            print("do nothing")
            
        }
        
    }
    
    
}
