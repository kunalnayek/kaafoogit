//
//  ServiceListingViewController.swift
//  Kaafoo
//
//  Created by esolz on 10/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Cosmos


class ServiceListingViewController: GlobalViewController {
    
    //Outlet Connections
    var SelectedPickerRowOne : Int! = 0
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBAction func SearchBarBtn(_ sender: UIButton) {
        
        self.SearchBar.isHidden = false
    }
    @IBOutlet weak var SearchBarBtnOutlet: UIButton!
    
    @IBAction func typeSortButton(_ sender: UIButton) {
        
        self.selectMaincategory = false
        
        if self.ShowPickerView.isHidden == true
        {
            self.CategoryPicker.selectRow(0, inComponent: 0, animated: true)
            
            self.ShowPickerView.isHidden = false
        }
        else
        {
            self.CategoryPicker.selectRow(0, inComponent: 0, animated: true)
            
            self.ShowPickerView.isHidden = true
        }
    }
    @IBOutlet weak var typeSortBtnOutlet: UIButton!
    
    @IBOutlet weak var SortingView: UIView!
    
    @IBOutlet weak var tapSelectionView: UIView!
    
    @IBOutlet weak var ListingtableView: UITableView!
    
    @IBOutlet weak var tapCollectionView: UICollectionView!
    
    @IBOutlet weak var ShowPickerView: UIView!
    
    @IBOutlet weak var CategoryPicker: UIPickerView!
    
    @IBOutlet weak var PickerHideBtnOutlet: UIButton!
    
    @IBAction func PickerHideBtn(_ sender: UIButton) {
        
        self.ShowPickerView.isHidden = true
    }
    
    @IBOutlet weak var SelectedCategoryName: UILabel!
    
    @IBOutlet weak var tapCategoryBtnOutlet: UIButton!
    
    @IBAction func tapCategoryBtn(_ sender: UIButton) {
        
        self.selectMaincategory = true
        
        self.CategoryPicker.reloadAllComponents()
        
        if self.ShowPickerView.isHidden == true
        {
            self.CategoryPicker.selectRow(0, inComponent: 0, animated: true)
            
            self.ShowPickerView.isHidden = false
        }
        else
        {
            self.CategoryPicker.selectRow(0, inComponent: 0, animated: true)
            
            self.ShowPickerView.isHidden = true
        }
    }
    
    var SelectedCategoryPickerRow : Int! = 0
    
    var SelectedSortPickerRow : Int! = 0
    
    var searchText : String! = ""
    
    var selectMaincategory : Bool! = false
    
    var SortsearchKey : String! = ""
    
    var ListingType : String! = "P"
    
    var PerLoad : Int! = 10
    
    var StartValue : Int! = 0
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let sellerID = UserDefaults.standard.string(forKey: "sellerID")
    
    
    let catID = UserDefaults.standard.string(forKey: "catID")
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var listingTypeArray : NSArray! = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "company_profile", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "listing", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")]
    
    var categoryDict = [["key" : "" , "value" : "All"],["key" : "LP" , "value" : "Lowest Price"],["key" : "HP" , "value" : "Highest Price"],["key" : "T" , "value" : "Title"],["key" : "N" , "value" : "Newly Listed"],["key" : "C" , "value" : "Closing Soon"],["key" : "AU" , "value" : "Auction"],["key" : "BN" , "value" : "Buy Now"]]
    
    var mainCategoryDict = [["key" : "" , "value" : "All"],["key" : "F" , "value" : "Food Court"],["key" : "S" , "value" : "Service"],["key" : "R" , "value" : "Real Estate"],["key" : "M" , "value" : "Marketplace"],["key" : "H" , "value" : "Holiday Accommodation"]]
    
    var SelectedCategoryID : String! = "9"
    
    
    /*
     9-Marketplace
     11-Services
     12-Jobs
     13-Food Court
     14-Happening
     15-Daily Rental
     16-Holiday Accommodatin
     */
    
    var CategoryIDArray = ["","13","11","999","9","16"]

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideSubviews(sender:)))
//        
//        self.view.addGestureRecognizer(tap)
        
        self.ConfigureDesign()
        
        self.CategoryPicker.delegate = self
        
        self.CategoryPicker.dataSource = self
        
        self.CategoryPicker.reloadAllComponents()
        
        self.tapCollectionView.showsVerticalScrollIndicator = false
        
        self.tapCollectionView.showsHorizontalScrollIndicator = false
        
        self.tapCollectionView.delegate = self
        
        self.tapCollectionView.dataSource = self
        
        self.tapCollectionView.reloadData()
        
        self.getServiceListing()
        
        self.SearchBar.delegate = self

        // Do any additional setup after loading the view.
    }
    
    @objc func hideSubviews(sender : UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    //MARK:- // Configure Pickerview Design
    
    func ConfigureDesign()
    {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = .default
        
        toolBar.isTranslucent = true
        
        toolBar.tintColor = .white
        
        toolBar.backgroundColor = .black
        
        toolBar.barTintColor = .black
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneClick))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        self.ShowPickerView.addSubview(toolBar)
        
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        toolBar.leadingAnchor.constraint(equalTo: self.CategoryPicker.leadingAnchor, constant: 0).isActive = true
        
        toolBar.trailingAnchor.constraint(equalTo: self.CategoryPicker.trailingAnchor, constant: 0).isActive = true
        
        toolBar.topAnchor.constraint(equalTo: self.CategoryPicker.topAnchor, constant: 0).isActive = true
        
        toolBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    //MARK:- // PickerView Done Button Functionality
    
    @objc func doneClick()
    {
        if self.selectMaincategory == true
        {
            self.SelectedCategoryName.text = "\((self.mainCategoryDict[SelectedCategoryPickerRow] as NSDictionary)["value"] ?? "")"
            
            self.SortsearchKey = "\((self.mainCategoryDict[SelectedCategoryPickerRow] as NSDictionary)["key"] ?? "")"
            
            print("SortsearchKey-----",self.SortsearchKey)
            
            self.SelectedCategoryID = "\(self.CategoryIDArray[SelectedCategoryPickerRow])"
            
            ServiceDetailsParameters.shared.setSelectedService(Service: "\((self.mainCategoryDict[SelectedCategoryPickerRow] as NSDictionary)["value"] ?? "")")
            
            self.recordsArray.removeAllObjects()

            self.getServiceListing()
            
            self.ShowPickerView.isHidden = true
        }
        else if selectMaincategory == false
        {
            self.SortsearchKey = "\((self.categoryDict[SelectedSortPickerRow] as NSDictionary)["key"] ?? "")"
            
            ServiceDetailsParameters.shared.setSelectedService(Service: "\((self.categoryDict[SelectedSortPickerRow] as NSDictionary)["value"] ?? "")")
            
            self.recordsArray.removeAllObjects()

            self.getServiceListing()
            
            self.ShowPickerView.isHidden = true
        }
        print("Done")
    }
    
    //MARK:- // Pickerview Cancel Button Functionality
    @objc func cancelClick()
    {
        print("Cancel")
        
        self.ShowPickerView.isHidden = true
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.SearchBar.isHidden = true
    }
    
    func getServiceListing()
    {
        
//        print("userid-----",UserID!)
//        print("userid-----",LangID!)
//         print("PerLoad-----",PerLoad!)
//         print("StartValue-----",StartValue!)
//         print("sellerID-----",sellerID!)
//         print("ListingType-----",ListingType!)
//         print("catID-----",catID!)
//         print("SortsearchKey-----",SortsearchKey!)
//         print("searchText-----",searchText!)
        
        let parameters = "login_id=\(UserID!)&lang_id=\(LangID!)&per_load=\(PerLoad!)&start_value=\(StartValue!)&seller_id=\(sellerID!)&listing_type=\(ListingType!)&category_id=\(self.catID ?? "")&sort_search_key=\(SortsearchKey ?? "")&search=\(searchText ?? "")"
        
        self.CallAPI(urlString: "app_service_user_details_main", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                print("GlobalJson",self.globalJson ?? "")
                
                let serviceArray = ((self.globalJson["info_array"] as! NSDictionary)["service_list"] as! NSArray)
                
                print("serviceArray-------",serviceArray)
                
                for i in 0..<(serviceArray.count)
                {
                    let tempDict = serviceArray[i] as! NSDictionary
                    
                    print("tempdict is : " , tempDict)
                    
                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                }
                DispatchQueue.main.async {
                    
                    self.ListingtableView.delegate = self
                    
                    self.ListingtableView.dataSource = self
                    
                    self.ListingtableView.reloadData()
                    
                    SVProgressHUD.dismiss()
                }
            })
        })
    }
    
    @objc func ProductWatchlist(sender : UIButton)
    {
        self.GlobalAddtoWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
            
            self.recordsArray.removeAllObjects()
            
            self.getServiceListing()
        })
    }
    
    @objc func ProductWatchlisted(sender : UIButton)
    {
        self.GlobalRemoveFromWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"] ?? "")", completion: {
            
            self.recordsArray.removeAllObjects()
            
            self.getServiceListing()
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ServiceListingViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceListingTableViewCell", for: indexPath) as! ServiceListingTableViewCell
        
//        cell.ContentView.layer.cornerRadius = 8.0
//        cell.ContentView.layer.masksToBounds = true
        let imageURL = "\((self.recordsArray[indexPath.row] as! NSDictionary)["service_image"] ?? "")"
        

       
        cell.starView.rating = Double("\((recordsArray[indexPath.row] as! NSDictionary)["reting_avg"] ?? "")")!
        
        cell.ServiceImage.sd_setImage(with: URL(string: imageURL))
        
        cell.ServiceName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
        
        cell.ServiceLocation.text = "Kolkata,West Bengal,India"
        
        cell.ServiceReviews.text = "Reviews" + "(" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["review_count"] ?? "")" + ")"
        
        cell.ServicePrice.text = "Per Hour: " + "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["service_price"] ?? "")"
        
        cell.ServiceWatchlistBtnOutlet.tag = indexPath.row
        
        cell.ServiceWatchlistedBtnOutlet.tag = indexPath.row
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"] ?? "")".elementsEqual("0")
        {
            cell.ServiceWatchlistBtnOutlet.isHidden = false
            
            cell.ServiceWatchlistedBtnOutlet.isHidden = true
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"] ?? "")".elementsEqual("1")
        {
            cell.ServiceWatchlistBtnOutlet.isHidden = true
            
            cell.ServiceWatchlistedBtnOutlet.isHidden = false
        }
        else
        {
            cell.ServiceWatchlistBtnOutlet.isHidden = false
            
            cell.ServiceWatchlistedBtnOutlet.isHidden = true
        }
        
        cell.ServiceWatchlistBtnOutlet.addTarget(self, action: #selector(self.ProductWatchlist(sender:)), for: .touchUpInside)
        
        cell.ServiceWatchlistedBtnOutlet.addTarget(self, action: #selector(self.ProductWatchlisted(sender:)), for: .touchUpInside)
         
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // print("nothing")
        self.SearchBar.isHidden = true
        
        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController
        
        obj.productID =   "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        
        obj.SID = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
}

//class ServiceListingTVC : UITableViewCell
//{
//
//
//}

class tapCollectionOneCVC : UICollectionViewCell
{
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var valueName: UILabel!
    
    @IBOutlet weak var SeparatorView: UIView!
    
    override var isSelected: Bool {
        
        didSet {
            
            if self.isSelected {
                
                valueName.textColor = .black
            }
            else
            {
                valueName.textColor = .gray
            }
        }
    }
}


extension ServiceListingViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.listingTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tap", for: indexPath) as! tapCollectionOneCVC
        
        cell.valueName.text = "\(self.listingTypeArray[indexPath.item] ?? "")"
        
        cell.valueName.textColor = .gray
        
        if indexPath.row == 1
        {
            cell.valueName.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGSize = "\(self.listingTypeArray[indexPath.row])".size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
        
        return CGSize(width: size.width + 60, height: 48)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tap", for: indexPath) as! tapCollectionOneCVC
        
        if indexPath.row == 0
        {
            cell.valueName.textColor = .black
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.SearchBar.isHidden = true
        
        if indexPath.item == 0
        {
             let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "ServiceVC") as! NewServiceProfileVC
            
             self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if indexPath.item == 1
        {
            //do nothing
        }
        else if indexPath.item == 2
        {
            let reviews = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "reviewsVC") as! ReviewsViewController
            
            self.navigationController?.pushViewController(reviews, animated: true)
        }
        else if indexPath.item == 3
        {
             let contact = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "contactVC") as! ContactViewController
            
             self.navigationController?.pushViewController(contact, animated: true)
        }
    }
}

extension ServiceListingViewController : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.selectMaincategory == true
        {
            return self.mainCategoryDict.count
        }
        else
        {
             return self.categoryDict.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.selectMaincategory == true
        {
             return "\((self.mainCategoryDict[row] as NSDictionary)["value"] ?? "")"
        }
        else
        {
            return "\((self.categoryDict[row] as NSDictionary)["value"] ?? "")"
        }

       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.selectMaincategory == true
        {
            self.SelectedCategoryPickerRow = row
        }
        else
        {
            self.SelectedSortPickerRow = row
        }
       
    }
}


extension ServiceListingViewController : UISearchBarDelegate
{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("Writing is in progress")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.SearchBar.searchTextField.text = ""
        
        self.SearchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("Search is in Progress")
        
        self.searchText = self.SearchBar.searchTextField.text
        
        self.recordsArray.removeAllObjects()
        
        self.getServiceListing()
    }
}

class ServiceListingTableViewCell: UITableViewCell {

@IBOutlet weak var ServiceImage: UIImageView!

@IBOutlet weak var ServiceName: UILabel!

@IBOutlet weak var ServiceReviews: UILabel!

@IBOutlet weak var ServicePrice: UILabel!

@IBOutlet weak var ServiceLocation: UILabel!

@IBOutlet weak var ServiceWatchlistBtnOutlet: UIButton!

@IBOutlet weak var ServiceWatchlistedBtnOutlet: UIButton!

@IBOutlet weak var starView: CosmosView!
    
@IBOutlet weak var ViewContent: UIView!
    
}
