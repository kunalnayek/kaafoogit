//
//  TermsandconditionsViewController.swift
//  Kaafoo
//
//  Created by priya on 02/06/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

class TermsandconditionsViewController: GlobalViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.SetWebView()
       
    }
    

    func SetWebView()
    {
        
       
        let langID = UserDefaults.standard.string(forKey: "langID")
       
       
        let url = URL (string: "https://kaafoo.com/appcontrol/app_kaafoo_terms_condition_page?page_name=terms_condition"+"&"+langID!)
        
        print("url----",url)
        
        let requestObj = URLRequest(url: url!)
        
        self.webView.loadRequest(requestObj)
    }


}
