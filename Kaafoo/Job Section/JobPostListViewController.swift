//
//  JobPostListViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 24/06/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class JobPostListViewController: GlobalViewController {

    
    @IBOutlet weak var jobPostListTable: UITableView!
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray = NSMutableArray()
    
    var jobListArray : NSArray!

    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getJobPostList()
        
    }
    
    // MARK:- // View will Appear
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.jobPostListTable.estimatedRowHeight = 400
        
        self.jobPostListTable.rowHeight = UITableView.automaticDimension
        
    }
    
    @objc func userlistBtnTarget(sender : UIButton)
    {
        let navigate = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "jobapplied") as! JobAppliedUsersVC
        
        navigate.JobID = "\((self.recordsArray[sender.tag] as! NSDictionary)["job_id"] ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    
    // MARK:- // JSON Post Method to Get Job Post List
    
    func getJobPostList() {
        
        var parameters : String!
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&start_value=\(startValue)&per_load=\(perLoad)"
        
        self.CallAPI(urlString: "app_user_job_list", param: parameters) {
            
            self.jobListArray = self.globalJson["info_array"] as? NSArray
            
            if self.jobListArray.count == 0
            {
                self.jobPostListTable.isHidden = true
                
                self.noDataFound(mainview: self.view)
            }
            else
            {
                for i in 0..<self.jobListArray.count
                    
                {
                    
                    let tempDict = self.jobListArray[i] as! NSDictionary
                    
                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                    
                }
                
                self.nextStart = "\(self.globalJson["next_start"]!)"
                
                self.startValue = self.startValue + self.jobListArray.count
                
                print("Next Start Value : " , self.startValue)
                
                
                DispatchQueue.main.async {
                    
                    self.globalDispatchgroup.leave()
                    
                    self.jobPostListTable.delegate = self
                    
                    self.jobPostListTable.dataSource = self
                    
                    self.jobPostListTable.reloadData()
                    
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    
}


// MARK:- // Tableview Delegates

extension JobPostListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobpostlist") as! jobPostListTVC
        
        cell.jobName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["name"] ?? "")"
        
        cell.closingDate.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time"] ?? "")"
        
        cell.topCategory.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["top_categorry"] ?? "")"
        
        cell.address.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["job_location"] ?? "")"
        
        cell.cellImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"] ?? "")"))
        
        cell.appliedUserList.tag = indexPath.row
        
        cell.appliedUserList.addTarget(self, action: #selector(self.userlistBtnTarget(sender:)), for: .touchUpInside)
        
        cell.appliedUserList.layer.cornerRadius = cell.appliedUserList.frame.size.height / 2
 
        
        // Set Font //////
        
        cell.jobName.font = UIFont(name: cell.jobName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        cell.closingDate.font = UIFont(name: cell.closingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.topCategory.font = UIFont(name: cell.topCategory.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.cellView.layer.cornerRadius = 8
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("Did Select Called")
        
    }
    
}




// MARK: - // Scrollview Delegates

extension JobPostListViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
//            print("next start : ",nextStart )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getJobPostList()
            }
        }
    }
}



// MARK:- // Job Post List Tableview Cell

class jobPostListTVC: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var jobName: UILabel!
    
    @IBOutlet weak var closingDate: UILabel!
    
    @IBOutlet weak var topCategory: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var appliedUserList: UIButton!
    
    
    
}
