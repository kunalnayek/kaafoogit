//
//  SecondViewController.swift
//  DemoCollectioView
//
//  Created by IOS-1 on 22/04/19.
//  Copyright © 2019 IOS-1. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import PushNotifications



class LatestHomeViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
//    var appDelegate: AppDelegate!
//    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var showpickerview: UIView!
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var titlelabel: UIButton!
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var searchingview: UIView!
    
    var clickedIndex : Int!
    
    var AutoScroll : Bool! = true
    
    var MyCountryId : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var watchListProductID : String! = ""
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    
    var Listdata : NSMutableArray! = NSMutableArray()
    
    var searchBy : String!
    
    //let searchBar = UISearchBar(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 35))
    
    var MainScrollView : UIScrollView = {
        
        let mainscrollview = UIScrollView()
        
        mainscrollview.showsHorizontalScrollIndicator = false
        
        mainscrollview.showsVerticalScrollIndicator = false
        
        mainscrollview.translatesAutoresizingMaskIntoConstraints = false
        
        return mainscrollview
    }()

    var ScrollContentView : UIView = {
        
        let scrollcontentview = UIView()
        
        scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        
        return scrollcontentview
    }()
    
    var mainStackView : UIStackView = {

        let mainstackview = UIStackView()
        
        mainstackview.axis = .vertical
        
        mainstackview.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        mainstackview.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
        mainstackview.spacing = 10

        //mainstackview.backgroundColor = .magenta
        mainstackview.translatesAutoresizingMaskIntoConstraints = false
        
        return mainstackview
        

    }()
    
    var DetailsView : UIView = {
        
        let detailsview = UIView()
        
        //detailsview.backgroundColor = .yellow
        
        detailsview.translatesAutoresizingMaskIntoConstraints = false
        
        return detailsview
    }()
    
    
    
    var classifiedView : UIView = {
        
        let classifiedview = UIView()
        
        //classifiedview.backgroundColor = .blue
        
        classifiedview.translatesAutoresizingMaskIntoConstraints = false
        
        return classifiedview
    }()
    
    var classifiedViewbutton : UIButton = {
        
        let classifiedViewbutton = UIButton()
        
        //classifiedview.backgroundColor = .blue
        
        classifiedViewbutton.translatesAutoresizingMaskIntoConstraints = false
        
        return classifiedViewbutton
    }()

    var footerAdvertisementView : UIView = {
        
        let footeradvertisementview = UIView()
        
        footeradvertisementview.translatesAutoresizingMaskIntoConstraints = false
        
        return footeradvertisementview
    }()


    @IBOutlet weak var sampleLBL: UILabel!

    let dispatchGroup = DispatchGroup()

    var mergedViewArray = [UIStackView]()
    
    var viewAllButtonArray = [UIButton]()

    var allDataDictionary : NSDictionary!

    var allDataArray = [dataStructure]()
    
    var allCategoriesArray : NSArray!
    
    var allAdvertisementArray : NSArray!
    
    var featureDict : NSDictionary!
    
    var allcategoryDict : NSDictionary!
    
    var footerArray : NSArray!
    
    var classifiedDict : NSDictionary!
    
    var productidstr : NSString!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        Listdata = ["Product","Account"]
        
         self.pickerview.delegate = self
         self.pickerview.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateWatchlist), name: NSNotification.Name(rawValue: "UpdateWatchlist"), object: nil)
        
        self.view.addSubview(footerView)
        
        self.MainScrollView.bringSubviewToFront(footerView)
        
        if isKeyPresentInUserDefaults(key: "userID") == false {
            UserDefaults.standard.set("", forKey: "userID")
        }

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        headerView.sideMenuButton.isUserInteractionEnabled = false
        self.DataFetch()
            

        self.dispatchGroup.notify(queue: .main) {

            self.createDataArrays()

            self.setPageLayout()
            
            SVProgressHUD.dismiss()
            self.headerView.sideMenuButton.isUserInteractionEnabled = true
        }
        

//        let options = PusherClientOptions(
//            host: .cluster("ap2")
//        )
//        let pusher = Pusher(key: "e53a9c4a375968423d66", options: options)
//        pusher.connect()
//
//        let myChannel = pusher.subscribe("my-notification-channel")
//
//        let _ = myChannel.bind(eventName: "my-noti-event", callback: { (data: Any?) -> Void in
//            if let data = data as? [String : AnyObject] {
//                if let message = data["message"] as? String {
//                    print(message)
//                }
//            }
//        })


        
       
        
        self.headerView.tapSearch.addTarget(self, action: #selector(self.HeaderSearch(sender:)), for: .touchUpInside)
               self.SearchBar.delegate = self
               self.SearchBar.isHidden = true
              

    }
    
    @IBAction func dropdownClicked(_ sender: Any)
    {
        showpickerview.isHidden = false
    }
    
    
    @objc func HeaderSearch(sender : UIButton)
       {
         self.SearchBar.isHidden = false
        self.searchingview.isHidden = false
        
       
       }
    
//       override func viewDidAppear(_ animated: Bool) {
//           for subView in searchBar.subviews  {
//               for subsubView in subView.subviews  {
//                   if let textField = subsubView as? UITextField {
//                       var bounds: CGRect
//                       bounds = textField.frame
//
//                       //2. Shorter textfield in height
//                       bounds.size.height = 28
//                       textField.bounds = bounds
//
//                       //3. Textfield to have less corner radious
//                       textField.layer.cornerRadius = 5 //Probably you can play with this and see the changes.
//                       textField.clipsToBounds = true
//                   }
//               }
//           }
//       }


    //MARK:- //Fetch Data From API
    func DataFetch()
    {

        self.dispatchGroup.enter()
        SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        let url = URL(string: GLOBALAPI + "app_api_homepage")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        
          let Countryname = UserDefaults.standard.string(forKey: "myCountryName")
          let CountryId = UserDefaults.standard.string(forKey: "countryID")
       // let CountryId = "99"
        
        print("countryid========",CountryId)

        print("Countryname========",Countryname)

        
        if isKeyPresentInUserDefaults(key: "userID") == true {
            
           if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "KUWAIT"
           {
               parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(245)"
           }
           else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "SAUDI ARABIA"
           {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(187)"
           }
           else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "INDIA"
           {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(99)"
           }
           else  if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "EGYPT"
           {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(63)"
            }
            else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "الكويت"
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(245)"
            }
            else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "المملكة العربية السعودية"
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(187)"
            }
            else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "الهند"
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(99)"
            }
            else  if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "مصر"
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(63)"
            }
            else if UserDefaults.standard.string(forKey: "countryID") != nil && Countryname == "السعودية"
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(187)"
            }
            
           
            else
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&mycountry_id=\(187)"
            }
            
        }
            
        else {
            parameters = "user_id="
        }
        
        

        print("homepageURL : ",url)
        
        print("Parameters are------------ : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)

        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json["info_array"] as? NSDictionary
                        
                        print("alldataDictionary---------",self.allDataDictionary)
                        
                        //let serviceArray = self.allDataDictionary["cat_product"] as! NSArray
                        
                       
                       
                        
//                        for i in 0..<(serviceArray.count)
//                        {
//                            let tempDict = serviceArray[i] as! NSDictionary
//
//                            print("tempdict is : " , tempDict)
//
//                           // self.recordsArray.(add(tempDict as! NSMutableDictionary)["product"]!)
//
//                             self.recordsArray.add((serviceArray[i] as! NSDictionary)["product"]!)
//
//                            print("recordarray---------",self.recordsArray)
//
//                        }

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                        }

                    }

                    else
                    {
                        print("Do nothing")

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()
    }



    // MARK:- // Create Data Arrays

    func createDataArrays() {

        self.featureDict = self.allDataDictionary["feature_product"] as? NSDictionary

        self.allCategoriesArray = self.allDataDictionary["cat_product"] as? NSArray
        print("allCategoriesArray----------",self.allCategoriesArray)
        
       


        self.allAdvertisementArray = self.allDataDictionary["advertisement"] as? NSArray

        self.footerArray = self.allDataDictionary["footer_logo"] as? NSArray

        self.classifiedDict = self.allDataDictionary["classified_list"] as? NSDictionary

        let tempArray = NSMutableArray()
        
        print("allcategoryarr----------",self.allCategoriesArray)
         print("allAdvertisementArray----------",self.allAdvertisementArray)


        for i in 0..<self.allAdvertisementArray.count {

            if i == 0 {
                tempArray.add(dataStructure(category: featureDict, advertisement: ((self.allAdvertisementArray[i] as! NSDictionary)["adv"] as! NSArray)))
            }
//

                
            else  {
               
            
              //  do {
                   // try
                        tempArray.add(dataStructure(category: self.allCategoriesArray[i-1] as! NSDictionary, advertisement: ((self.allAdvertisementArray[i] as! NSDictionary)["adv"] as! NSArray)))
//                } catch {
//                     print("priya")
//                }

//                 let tempArray = self.allDataDictionary["advertisement"] as! NSArray
//                print("temparray-------",tempArray)
                
               // tempArray.add(dataStructure(category: self.classifiedDict, advertisement: ((self.allAdvertisementArray[i] as! NSDictionary)["adv"] as! NSArray)))
               
                
            }
            
           

        }

        let tempCount = self.allAdvertisementArray.count - 1

        if self.allCategoriesArray.count > tempCount {
            for i in 0..<(self.allCategoriesArray.count - (self.allAdvertisementArray.count - 1)) {
                
                do {
                    try tempArray.add(dataStructure(category: self.allCategoriesArray[tempCount + i] as! NSDictionary, advertisement: []))
                } catch {
                    print("Priya sah")
                }
                
            }
        }


        self.allDataArray = tempArray.mutableCopy() as! [dataStructure]
        
      

    }



    // MARK:- // Collectionview Delegates



    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView.tag == 999 { // Classified
            return (self.classifiedDict["product"] as! NSArray).count
        }
        else {
            return ((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray).count
        }

    }



    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         var topCatID : String!
        


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath as IndexPath)

        for view in cell.subviews {
            view.removeFromSuperview()
        }

        cell.backgroundColor = .white
       

        let shadowView : ShadowView = {
            let shadowview = ShadowView()
            shadowview.translatesAutoresizingMaskIntoConstraints = false
            return shadowview
        }()

        cell.addSubview(shadowView)

        shadowView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor , padding: .init(top: 5, left: 5, bottom: 5, right: 5))


        let cellView : UIView = {
            let cellview = UIView()
            cellview.backgroundColor = .white
            cellview.layer.cornerRadius = 8
            cellview.translatesAutoresizingMaskIntoConstraints = false
           
            return cellview
        }()

        cell.addSubview(cellView)

        cellView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor , padding: .init(top: 5, left: 5, bottom: 5, right: 5))


        if collectionView.tag == 999 { // classified
            
            cellView.layer.borderWidth = 1.0
            cellView.layer.borderColor = UIColor.s_green().cgColor

            let tempdict = (self.classifiedDict["product"] as! NSArray)[indexPath.item] as! NSDictionary
            
            print("tempdict-----------",tempdict)
            
            
            let str = "\(tempdict["id"] ?? "")"
            
            print("str----",str)
            
            let celluniqueno : UILabel = {
                let celluniqueno = UILabel()
                celluniqueno.textAlignment = .left
                celluniqueno.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                celluniqueno.textColor = .darkGray
                celluniqueno.translatesAutoresizingMaskIntoConstraints = false
                return celluniqueno
            }()
             
            let cellTitle : UILabel = {
                let celltitle = UILabel()
                celltitle.textAlignment = .center
                celltitle.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //celltitle.backgroundColor = .red
                celltitle.translatesAutoresizingMaskIntoConstraints = false
                return celltitle
            }()

            let cellDescription : UILabel = {
                let celldescription = UILabel()
                celldescription.textAlignment = .center
                //celldescription.backgroundColor = .red
                celldescription.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                celldescription.numberOfLines = 0
                celldescription.translatesAutoresizingMaskIntoConstraints = false
                return celldescription
            }()

            let phoneNo : UILabel = {
                let phoneno = UILabel()
                phoneno.textAlignment = .center
                phoneno.backgroundColor = UIColor.s_green()
               
                phoneno.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                phoneno.translatesAutoresizingMaskIntoConstraints = false
                return phoneno
            }()

            cellView.addSubview(cellTitle)
            cellView.addSubview(cellDescription)
            cellView.addSubview(phoneNo)
            cellView.addSubview(celluniqueno)
            
            celluniqueno.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 25))
            //celluniqueno.backgroundColor = .green

            cellTitle.anchor(top: celluniqueno.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 25))
            //cellTitle.backgroundColor = .blue

            phoneNo.anchor(top: nil, leading: cellView.leadingAnchor, bottom: cellView.bottomAnchor, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 30))

            cellDescription.anchor(top: cellTitle.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 10, right: 10), size: .init(width: 0, height: 25))
           // cellDescription.backgroundColor = .red


            cellTitle.text = "\(tempdict["title"] ?? "")"
            celluniqueno.text = "\(tempdict["unique_no"] ?? "")"
            cellDescription.text = "\(tempdict["description"] ?? "")"
            phoneNo.text = "\(tempdict["phone_no"] ?? "")"




        }
        else {


            let tempDict = (((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.item] as! NSDictionary)

            self.recordsArray.add(tempDict as! NSMutableDictionary)
                           
            print("recordarray---------",self.recordsArray)
            
            
                       
                       
                       let str = "\(tempDict["seller_id"] ?? "")"
                       
                       print("str----",str)


            let cellImage : UIImageView = {
                let cellimage = UIImageView()
                cellimage.contentMode = .scaleAspectFill
                cellimage.clipsToBounds = true
                cellimage.translatesAutoresizingMaskIntoConstraints = false
                return cellimage
            }()

            cellView.addSubview(cellImage)

            cellImage.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 150))

            let cellTitle : UILabel = {
                let celltitle = UILabel()
                //celltitle.isEditable = false
                //celltitle.isScrollEnabled = false
                celltitle.textAlignment = .natural
                celltitle.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //celltitle.backgroundColor = .red
                celltitle.translatesAutoresizingMaskIntoConstraints = false
                return celltitle
            }()

            cellView.addSubview(cellTitle)

            cellTitle.anchor(top: cellImage.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))

            let price : UILabel = {
                let price = UILabel()
                //price.isEditable = false
               // price.textColor = .darkGray
                price.textColor = UIColor(red: 27, green: 165, blue: 117)
                //price.isScrollEnabled = false
                price.textAlignment = .natural
                price.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //price.backgroundColor = .green
                price.translatesAutoresizingMaskIntoConstraints = false
                return price
            }()

            cellView.addSubview(price)

            price.anchor(top: cellTitle.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))


            let reservedPrice : UILabel = {
                let reservedprice = UILabel()
                //reservedprice.isEditable = false
                reservedprice.textColor = .red
                //reservedprice.isScrollEnabled = false
                reservedprice.textAlignment = .natural
                reservedprice.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //reservedprice.backgroundColor = .orange
                reservedprice.translatesAutoresizingMaskIntoConstraints = false
                return reservedprice
            }()

            cellView.addSubview(reservedPrice)

            reservedPrice.anchor(top: price.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))
            
            if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("10")
            {
                let dailydealsicon : UIImageView = {
                                       let dailydealsicon = UIImageView()
                                       dailydealsicon.contentMode = .scaleAspectFill
                                       dailydealsicon.clipsToBounds = true
                                       dailydealsicon.translatesAutoresizingMaskIntoConstraints = false
                                       dailydealsicon.image = UIImage(named: "dealsicon")
                                      // addressImage.backgroundColor = .blue
                                       return dailydealsicon
                                   }()
                                   
                                   let dailydealslabel : UILabel = {
                                                       let dailydealslabel = UILabel()
                                                       //address.textColor = UIColor.seaGreen()
                                                       dailydealslabel.textColor = .black
                                                       dailydealslabel.textAlignment = .natural
                                                      
                                                       
                                                       //address.backgroundColor = .red
                                                       //address.text = "Kolkata"
                                                       dailydealslabel.translatesAutoresizingMaskIntoConstraints = false
                                                       return dailydealslabel
                                                   }()
                                   
                                 dailydealslabel.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))

                                                 cellView.addSubview(dailydealsicon)

                                                 dailydealsicon.anchor(top: reservedPrice.bottomAnchor, leading: reservedPrice.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 30, bottom: 5, right: 5), size: .init(width: 35, height: 35))



                                                 cellView.addSubview(dailydealslabel)

                                                 dailydealslabel.anchor(top: reservedPrice.bottomAnchor, leading: dailydealsicon.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 15, left: -10, bottom: 0, right: 0), size: .init(width: 50, height: 70))

                                                 dailydealslabel.text =  "Daily Deals"
                                                 dailydealslabel.numberOfLines = 0
                                                 //address.lineBreakMode = NSLineBreakMode.byWordWrapping
                                                 dailydealslabel.sizeToFit()
                               
                
                
            }
            
            else
            {
                
                //placeholder image
                
                let addressImage : UIImageView = {
                               let addressImage = UIImageView()
                               addressImage.contentMode = .scaleAspectFill
                               addressImage.clipsToBounds = true
                               addressImage.translatesAutoresizingMaskIntoConstraints = false
                               addressImage.image = UIImage(named: "placeholder")
                              // addressImage.backgroundColor = .blue
                               return addressImage
                           }()

                           cellView.addSubview(addressImage)

                           addressImage.anchor(top: reservedPrice.bottomAnchor, leading: reservedPrice.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 0, bottom: 5, right: 5), size: .init(width: 0, height: 15))
                
                //address label
                
                let address : UILabel = {
                    let address = UILabel()
                    //address.textColor = UIColor.seaGreen()
                    address.textColor = .black
                    address.textAlignment = .natural
                    address.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
                    
                    //address.backgroundColor = .red
                    //address.text = "Kolkata"
                    address.translatesAutoresizingMaskIntoConstraints = false
                    return address
                }()

                cellView.addSubview(address)

                address.anchor(top: reservedPrice.bottomAnchor, leading: addressImage.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: -20, left: 15, bottom: 0, right: 35), size: .init(width: 0, height: 70))
                
                //address.anchor(top: nil, leading: addressImage.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: -10, left: 15, bottom: 0, right: 35), size: .init(width: 0, height: 70))
                
                //address.centerXAnchor.constraint(equalTo: addressImage.centerXAnchor).isActive=true
                
                address.text =  "\(tempDict["address"] ?? "")"
                address.numberOfLines = 0
                //address.lineBreakMode = NSLineBreakMode.byWordWrapping
                address.sizeToFit()
            }
            
            
    
           
             UserDefaults.standard.set("\(tempDict["id"] ?? "")", forKey: "foodCourtSellerID")
            

            let likebutton : UIButton = {
                let likebutton = UIButton()
                likebutton.setImage(UIImage(named: "heart_red"),for: .normal)
                likebutton.translatesAutoresizingMaskIntoConstraints = false
                return likebutton
            }()

            cellView.addSubview(likebutton)

            likebutton.anchor(top: reservedPrice.bottomAnchor, leading: nil, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 10), size: .init(width: 0, height: 25))

            
            let unlikebutton : UIButton = {
                let unlikebutton = UIButton()
                unlikebutton.setImage(UIImage(named: "heart"),for: .normal)
                unlikebutton.translatesAutoresizingMaskIntoConstraints = false
                return unlikebutton
            }()

            cellView.addSubview(unlikebutton)

            unlikebutton.anchor(top: reservedPrice.bottomAnchor, leading: nil, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 10), size: .init(width: 0, height: 25))
            
            // working with watchlist status
            
            likebutton.tag = indexPath.row
                   
            unlikebutton.tag = indexPath.row
           
            if "\(tempDict["watchlist_status"] ?? "")".elementsEqual("1")
                
                {
                
                    likebutton.isHidden = false

                    unlikebutton.isHidden = true

                    likebutton.addTarget(self, action: #selector(remove(sender:)), for: .touchUpInside)
                   
            }
            else if "\(tempDict["watchlist_status"] ?? "")".elementsEqual("0")
            {
               likebutton.isHidden = true

               unlikebutton.isHidden = false
                
                unlikebutton.addTarget(self, action: #selector(add(sender:)), for: .touchUpInside)
   
            }
            else
            {
                print("entered")
                
               likebutton.isHidden = false

                unlikebutton.isHidden = true
            }
            
//             if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("10") {
                
                 
//                  dailydealslabel.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
//
//                 cellView.addSubview(dailydealsicon)
//
//                 dailydealsicon.anchor(top: reservedPrice.bottomAnchor, leading: reservedPrice.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 30, bottom: 5, right: 5), size: .init(width: 35, height: 35))
//
//
//
//                 cellView.addSubview(dailydealslabel)
//
//                 dailydealslabel.anchor(top: reservedPrice.bottomAnchor, leading: dailydealsicon.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 15, left: -10, bottom: 0, right: 0), size: .init(width: 50, height: 70))
//
//                 dailydealslabel.text =  "Daily Deals"
//                 dailydealslabel.numberOfLines = 0
//                 //address.lineBreakMode = NSLineBreakMode.byWordWrapping
//                 dailydealslabel.sizeToFit()
                 
            // }
            
//            else
//             {
                // cellView.willRemoveSubview(dailydealsicon)
                // cellView.willRemoveSubview(dailydealslabel)
          //  }
            
           
            

            // Image and Title Checking for Job and Others

            if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {

                cellImage.sd_setImage(with: URL(string: "\(tempDict["company_logo"] ?? "")"))

                cellTitle.text = "\(tempDict["company_name"] ?? "")"
                

            }
            else {

                cellImage.sd_setImage(with: URL(string: "\(tempDict["product_image"] ?? "")"))

                cellTitle.text = "\(tempDict["product_name"] ?? "")"

            }




            // Price and Reserved Price Checking

            if "\(tempDict["special_offer"] ?? "")".elementsEqual("0")
            {
                print("tempDict----------",tempDict)
                
                price.text = "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["start_price"] ?? "")"

                //price.textColor = .darkGray
                
                price.textColor = UIColor(red: 27, green: 165, blue: 117)

                reservedPrice.isHidden = true
            }
            else {
                 print("tempDict----------",tempDict)
                
                reservedPrice.text = "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["reserve_price"] ?? "")"

               // price.textColor = .darkGray
                
                price.textColor = UIColor(red: 27, green: 165, blue: 117)
                
                reservedPrice.textColor = .red

                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["start_price"] ?? "")")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                price.attributedText = attributeString

                reservedPrice.isHidden = false

            }
            
           // watchListProductID = "\(tempDict["product_id"] ?? "")" as String
           // print("watchListProductID-----------",watchListProductID)
            
        }



        return cell
    }
    
    //MARK:- // Updation of Watchlist Status Using NSNotification
      
      @objc func updateWatchlist()
      {
         //self.nextStart = "0"
          self.recordsArray.removeAllObjects()
          self.DataFetch()
      }
    
    
    @objc func add(sender: UIButton)
       {
           if userID?.isEmpty == true
           {
               let alert = UIAlertController(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
               let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
               alert.addAction(ok)
               self.present(alert, animated: true, completion: nil)
           }
           else
           {
               let alert = UIAlertController(title: "Warning", message: "Product is added to watchlist", preferredStyle: .alert)
               let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
               alert.addAction(ok)
               self.present(alert, animated: true, completion: {
              //  self.watchListProductID = "\(self.watchListProductID!)"
                self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                print("addwatchListProductID---------",self.watchListProductID)
                
                   self.addToWatchList {
                       //self.nextStart = "0"
                       //self.recordsArray.removeAllObjects()
                       self.DataFetch()

                   }
               })
           }
       }


    
    @objc func remove(sender: UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "warning", message: "Product is removed from Watchlist", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
               // self.watchListProductID = "\(self.watchListProductID!)"
                
                  self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                
                 print("removewatchListProductID---------",    self.watchListProductID)
                
                
                self.removeFromWatchList()
                    {
                       // self.nextStart = ""
                     //   self.recordsArray.removeAllObjects()
                        //self.nextStart = "0"
                        self.DataFetch()
                }
            })
        }
    }

    // MARK:- // JSON Post Method to Remove from Watchlist

       @objc func removeFromWatchList(completion : @escaping () -> ())

       {
           dispatchGroup.enter()
           DispatchQueue.main.async {
               SVProgressHUD.show()
           }
           let url = URL(string: GLOBALAPI + "app_remove_watchlist")!

           print("add from watchlist URL : ", url)

           var parameters : String = ""

           let userID = UserDefaults.standard.string(forKey: "userID")

           parameters = "user_id=\(userID!)&remove_product_id=\(watchListProductID!)&lang_id=\(langID!)"

           print("Parameters are : " , parameters)

           let session = URLSession.shared

           var request = URLRequest(url: url)
           request.httpMethod = "POST" //set http method as POST

           do {
               request.httpBody = parameters.data(using: String.Encoding.utf8)


           }

           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

               guard error == nil else {
                   return
               }

               guard let data = data else {
                   return
               }

               do {

                   if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                       print("remove from watchlist Response: " , json)
                       let alertmsg = "\(json["message"]!)"

                       if "\(json["response"] as! Bool)".elementsEqual("true")
                       {
                           DispatchQueue.main.async {
                               self.dispatchGroup.leave()
                               SVProgressHUD.dismiss()

                               let alert = UIAlertController(title: "Warning", message: alertmsg, preferredStyle: .alert)
                               let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                               alert.addAction(ok)
                               self.present(alert, animated: true, completion: nil)
                           }
                           completion()
                       }
                       else
                       {
                           DispatchQueue.main.async {
                               self.dispatchGroup.leave()
                               SVProgressHUD.dismiss()
                           }
                           let alert = UIAlertController(title: "Warning", message: alertmsg, preferredStyle: .alert)
                           let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                           alert.addAction(ok)
                           self.present(alert, animated: true, completion: nil)
                       }

                   }

               } catch let error {
                   print(error.localizedDescription)
               }
           })

           task.resume()



       }



       // MARK:- // JSON Post method to Remove from Watchlist


       func addToWatchList(completion : @escaping () -> ())

       {
           dispatchGroup.enter()

           DispatchQueue.main.async {
               SVProgressHUD.show()
           }



           let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url

           print("add from watchlist URL : ", url)

           var parameters : String = ""

           let userID = UserDefaults.standard.string(forKey: "userID")

           parameters = "user_id=\(userID!)&add_product_id=\(watchListProductID!)&lang_id=\(langID!)"

           print("Parameters are : " , parameters)

           let session = URLSession.shared

           var request = URLRequest(url: url)
           request.httpMethod = "POST" //set http method as POST

           do {
               request.httpBody = parameters.data(using: String.Encoding.utf8)


           }

           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

               guard error == nil else {
                   return
               }

               guard let data = data else {
                   return
               }

               do {

                   if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                       print("add to watchlist Response: " , json)

                       if "\(json["response"] as! Bool)".elementsEqual("true")
                       {
                           DispatchQueue.main.async {

                               self.dispatchGroup.leave()

                               SVProgressHUD.dismiss()

                           }

                           completion()
                       }
                       else
                       {
                           DispatchQueue.main.async {
                               self.dispatchGroup.leave()
                               SVProgressHUD.dismiss()
                           }
                       }



                   }

               } catch let error {
                   print(error.localizedDescription)
               }
           })

           task.resume()



       }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        

        var topCatID : String!
        
           print("topcatid------",topCatID)
        
        
                let tempdictcat = (self.classifiedDict["category"] as! NSDictionary)
        
                print("tempdictcat-----------",tempdictcat)
        
                let strcat = "\(tempdictcat["cat_id"] ?? "")"
        
                print("strcat----",strcat)

        
        if collectionView.tag == 999
        {
            let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "textad") as! NewTextAdvertisementViewController
                           self.navigationController?.pushViewController(nav, animated: true)
            
        }
        
        else
        {
        

        if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {
            topCatID = "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")"
            
            print("topcatid------",topCatID)
           
        }
        else {
            topCatID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["top_cat"] ?? "")"
            
             print("topcatid------",topCatID)
           
        }




        // Daily Deals
        if topCatID.elementsEqual("10") {

//            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailydealsd") as! DailyDealsDetailsViewController

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Marketplace
        else if topCatID.elementsEqual("9") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Services
        else if topCatID.elementsEqual("11") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController

            navigate.productID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Real Estate
        else if topCatID.elementsEqual("999") {

            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateDetailsVC") as! NewRealEstateDetailsViewController

            nav.productID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(nav, animated: true)

        }

            // Jobs
        else if topCatID.elementsEqual("12") {

           // let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobDetailsVC") as! JobDetailsViewController
            
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobdetails") as! NewJobDetailsViewController

            navigate.JobId = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Daily Rental
        else if topCatID.elementsEqual("15") {

             let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyrental") as! NewDailyRentalProductDetailsViewController

            nav.DailyRentalProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(nav, animated: true)

        }

            // Happening
        else if topCatID.elementsEqual("14") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController

            navigate.eventID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)


        }
        
            // Holiday Accomodation
        else if topCatID.elementsEqual("16") {
            
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
            
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }
        
            //Food Court
        else if topCatID.elementsEqual("13") {
            
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController
            
          
            //print("sellerid----","\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["id"] ?? "")")
            
           // UserDefaults.standard.set("\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"] ?? "")", forKey: "foodCourtSellerID")
            
            let tempval = (((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.item] as! NSDictionary)
            
            let str = "\(tempval["seller_id"] ?? "")"
                       
                       print("str----",str)
                       
                      // UserDefaults.standard.set("\(tempdict["id"] ?? "")"
                      // , forKey: "foodCourtSellerID")
            
             //UserDefaults.standard.set("\(str)", forKey: "foodCourtSellerName")

                  
            
            navigate.sellerID = str
           
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }
        }
       
            
          
        
            
//        let tempdictcat = (self.classifiedDict["category"] as! NSDictionary)
//
//        print("tempdictcat-----------",tempdictcat)
//
//        let strcat = "\(tempdictcat["cat_name"] ?? "")"
//
//        print("strcat----",strcat)


        

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView.tag == 999 { // classified
            let cellSize =  CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 180)

            return cellSize
        }
        else {
            let cellSize =  CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 310)

            return cellSize
        }



    }




    // MARK;- // Set Page Layout

    func setPageLayout() {

        self.view.addSubview(self.MainScrollView)
        self.MainScrollView.addSubview(self.ScrollContentView)
        self.MainScrollView.addSubview(self.showpickerview)
        self.ScrollContentView.addSubview(self.mainStackView)
        self.mainStackView.addArrangedSubview(self.DetailsView)
        self.mainStackView.addArrangedSubview(self.classifiedView)
        self.mainStackView.addArrangedSubview(self.footerAdvertisementView)

        let safeGuide = self.view.safeAreaLayoutGuide
//        self.MainScrollView.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor, bottom: safeGuide.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.MainScrollView.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor
            , bottom: safeGuide.bottomAnchor, trailing: self.view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 30, right: 0))

        self.ScrollContentView.anchor(top: self.MainScrollView.topAnchor, leading: self.MainScrollView.leadingAnchor, bottom: self.MainScrollView.bottomAnchor, trailing: self.MainScrollView.trailingAnchor, size: .init(width: self.FullWidth, height: 0))

        self.mainStackView.anchor(top: self.ScrollContentView.topAnchor, leading: self.ScrollContentView.leadingAnchor, bottom: self.ScrollContentView.bottomAnchor, trailing: self.ScrollContentView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 20, right: 0))




        for i in 0..<self.allDataArray.count {

            if i == 0 {
                createMergedView(yAnchor: self.DetailsView.topAnchor, isLastView: false, index: i)
            }
            else if i == (self.allDataArray.count - 1) {
                createMergedView(yAnchor: self.mergedViewArray[i-1].bottomAnchor, isLastView: true, index: i)
            }
            else {
                createMergedView(yAnchor: self.mergedViewArray[i-1].bottomAnchor, isLastView: false, index: i)
            }

        }


        // create classified view

        self.createClassifiedView(insideOfView: self.classifiedView, dataDict: self.classifiedDict)

        if (self.classifiedDict["product"] as! NSArray).count > 0 {
            self.classifiedView.isHidden = false
        }
        else {
            self.classifiedView.isHidden = true
        }



        // create Footer advertisement view

        createFooterAdvertisement(viewOnTop: self.footerAdvertisementView, dataArray: self.footerArray, keyString: "footer_image", contentMode: .scaleAspectFit)

    }


    // MARK:- // Create Merged View

    func createMergedView(yAnchor : NSLayoutYAxisAnchor , isLastView : Bool , index : Int)
    {


        let mergedView : UIStackView = {

            let mergedview = UIStackView()
            mergedview.axis = .vertical
            mergedview.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
            mergedview.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
            mergedview.spacing = 10

            //mergedview.backgroundColor = .magenta
            mergedview.translatesAutoresizingMaskIntoConstraints = false
            return mergedview

        }()

        self.mergedViewArray.append(mergedView)

        let viewOfListCollectionview : UIView = {

            let viewoflistcollectionview = UIView()
            //viewoflistcollectionview.backgroundColor = .black
            viewoflistcollectionview.translatesAutoresizingMaskIntoConstraints = false
            return viewoflistcollectionview

        }()

        let viewOfAdvertisementview : UIView = {

            let viewOfAdvertisementview = UIView()
            //viewOfAdvertisementview.backgroundColor = .orange
            viewOfAdvertisementview.translatesAutoresizingMaskIntoConstraints = false
            return viewOfAdvertisementview

        }()

        self.DetailsView.addSubview(mergedView)

        mergedView.addArrangedSubview(viewOfAdvertisementview)
        mergedView.addArrangedSubview(viewOfListCollectionview)

        if isLastView == true {
            mergedView.anchor(top: yAnchor, leading: self.DetailsView.leadingAnchor, bottom: self.DetailsView.bottomAnchor, trailing: self.DetailsView.trailingAnchor)
        }
        else {
            mergedView.anchor(top: yAnchor, leading: self.DetailsView.leadingAnchor, bottom: nil, trailing: self.DetailsView.trailingAnchor)
        }

        self.createAdvertisement(viewOnTop: viewOfAdvertisementview , dataArray: self.allDataArray[index].advertisement!, keyString: "image", contentMode: .scaleAspectFill)

        self.createListCollectionview(viewOnTop: viewOfListCollectionview, isLastView: false, dataDict: self.allDataArray[index].category!, index: index)

        if ((self.allDataArray[index].category)!["product"] as! NSArray).count == 0 {
            viewOfListCollectionview.isHidden = true
        }
        else {
            viewOfListCollectionview.isHidden = false
        }

        if ((self.allDataArray[index].advertisement)!).count == 0 {
            viewOfAdvertisementview.isHidden = true
        }
        else {
            viewOfAdvertisementview.isHidden = false
        }

    }



    // MARK:- // Create Advertisement

    func createAdvertisement(viewOnTop : UIView , dataArray : NSArray , keyString : String , contentMode : UIView.ContentMode) {

        var imageviewArray = [UIImageView]()

        /*
         let shadowView : ShadowView = {
         let shadowview = ShadowView()
         shadowview.translatesAutoresizingMaskIntoConstraints = false
         return shadowview
         }()

         viewOnTop.addSubview(shadowView)

         shadowView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))
         */
        let scrollView : UIScrollView = {
            let scrollview = UIScrollView()
            scrollview.isPagingEnabled = true
            scrollview.layer.cornerRadius = 8
            scrollview.showsVerticalScrollIndicator = false
            scrollview.showsHorizontalScrollIndicator = false
            scrollview.translatesAutoresizingMaskIntoConstraints = false
            //scrollview.backgroundColor = .red
            return scrollview
        }()


        let scrollContentview : UIView = {
            let scrollcontentview = UIView()
            scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
            //scrollcontentview.backgroundColor = .green
            return scrollcontentview
        }()


        viewOnTop.addSubview(scrollView)
        scrollView.addSubview(scrollContentview)


        scrollView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))

        scrollContentview.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, trailing: scrollView.trailingAnchor, size: .init(width: 0, height: 200))

        for i in 0..<dataArray.count {

            let advertisementImageview : UIImageView = {

                let advertisementimageview = UIImageView()
                advertisementimageview.translatesAutoresizingMaskIntoConstraints = false
                advertisementimageview.contentMode = contentMode
                return advertisementimageview
            }()

            imageviewArray.append(advertisementImageview)

            scrollContentview.addSubview(advertisementImageview)

            advertisementImageview.sd_setImage(with: URL(string: "\((dataArray[i] as! NSDictionary)[keyString] ?? "")"))


            advertisementImageview.topAnchor.constraint(equalTo: scrollContentview.topAnchor, constant: 0).isActive = true
            advertisementImageview.bottomAnchor.constraint(equalTo: scrollContentview.bottomAnchor, constant: 0).isActive = true
            advertisementImageview.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -20).isActive = true
            if dataArray.count == 1
            {

                advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
            }
            else
            {

                if i == 0 {
                    advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                }
                else if i == (dataArray.count - 1)
                {
                    advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                    advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                }
                else {
                    advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                }
            }



        }


        // Auto Sctoll
        //self.startAutoScroll(scrollView: scrollView)
        
        //scrollView.contentOffset.x = 0
        
        self.animateScrollView(scrollView: scrollView)

    }





    // MARK:- // Create Footer Advertisement

    func createFooterAdvertisement(viewOnTop : UIView , dataArray : NSArray , keyString : String , contentMode : UIView.ContentMode) {

        var imageviewArray = [UIImageView]()

        /*
         let shadowView : ShadowView = {
         let shadowview = ShadowView()
         shadowview.translatesAutoresizingMaskIntoConstraints = false
         return shadowview
         }()

         viewOnTop.addSubview(shadowView)

         shadowView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))
         */
        let scrollView : UIScrollView = {
            let scrollview = UIScrollView()
            scrollview.isPagingEnabled = true
            scrollview.layer.cornerRadius = 8
            scrollview.contentInsetAdjustmentBehavior = .never
            scrollview.showsVerticalScrollIndicator = false
            scrollview.showsHorizontalScrollIndicator = false
            scrollview.translatesAutoresizingMaskIntoConstraints = false
            //scrollview.backgroundColor = .red
            return scrollview
        }()


        let scrollContentview : UIView = {
            let scrollcontentview = UIView()
            scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
            //scrollcontentview.backgroundColor = .green
            return scrollcontentview
        }()


        viewOnTop.addSubview(scrollView)
        scrollView.addSubview(scrollContentview)


        scrollView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))

        scrollContentview.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, trailing: scrollView.trailingAnchor, size: .init(width: 0, height: 200))

        for i in 0..<dataArray.count {

            let advertisementImageview : UIImageView = {

                let advertisementimageview = UIImageView()
                advertisementimageview.translatesAutoresizingMaskIntoConstraints = false
                advertisementimageview.contentMode = contentMode
                advertisementimageview.clipsToBounds = true
                return advertisementimageview
            }()

            imageviewArray.append(advertisementImageview)

            scrollContentview.addSubview(advertisementImageview)

            advertisementImageview.sd_setImage(with: URL(string: "\((dataArray[i] as! NSDictionary)[keyString] ?? "")"))


            advertisementImageview.topAnchor.constraint(equalTo: scrollContentview.topAnchor, constant: 0).isActive = true
            advertisementImageview.bottomAnchor.constraint(equalTo: scrollContentview.bottomAnchor, constant: 0).isActive = true
            advertisementImageview.widthAnchor.constraint(equalToConstant: (self.FullWidth - 40) / 3).isActive = true

            if dataArray.count == 1
            {

                advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
            }
            else
            {
                if i % 3 == 0 && i != 0 {
                    if i == (dataArray.count - 1) {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                        advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                    }
                    else {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                    }
                }
                else {
                    if i == 0 {
                        advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                    }
                    else if i == (dataArray.count - 1)
                    {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 10).isActive = true
                        advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                    }
                    else {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 10).isActive = true
                    }
                }


            }



        }


        // Auto Sctoll
        //self.startAutoScroll(scrollView: scrollView)
        
        //scrollView.contentOffset.x = 0
        
        self.animateScrollView(scrollView: scrollView)

    }






    // MARK:- // Create Listing CollectionVIew

    func createListCollectionview(viewOnTop : UIView ,isLastView : Bool , dataDict : NSDictionary , index : Int) {



        let HeaderView : UIView = {

            let headerview = UIView()
            //headerview.backgroundColor = .cyan
            headerview.translatesAutoresizingMaskIntoConstraints = false
            return headerview

        }()

        viewOnTop.addSubview(HeaderView)


        HeaderView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: nil, trailing: viewOnTop.trailingAnchor)


        let headerLabel : UILabel = {

            let headerlabel = UILabel()
            //headerlabel.backgroundColor = .green
            headerlabel.font = UIFont(name: self.sampleLBL.font.fontName, size: 16)
            headerlabel.translatesAutoresizingMaskIntoConstraints = false
            return headerlabel


        }()

        HeaderView.addSubview(headerLabel)

       // headerLabel.text = "\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")"
        headerLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")", comment: "")
        
        
        //        headerLabel.anchor(top: HeaderView.topAnchor, leading: HeaderView.leadingAnchor, bottom: HeaderView.bottomAnchor, trailing: nil, size: .init(width: 100, height: 0))

        let ViewAllBtn : UIButton = {
            let viewallbtn = UIButton()
            viewallbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAll", comment: ""), for: .normal)
            viewallbtn.setTitleColor(.black, for: .normal)
            viewallbtn.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: 14)
            viewallbtn.addTarget(self, action: #selector(LatestHomeViewController.viewAllButton(sender:)), for: .touchUpInside)
            viewallbtn.translatesAutoresizingMaskIntoConstraints = false
            return viewallbtn
        }()

        HeaderView.addSubview(ViewAllBtn)

        self.viewAllButtonArray.append(ViewAllBtn)

        ViewAllBtn.tag = index

        ViewAllBtn.anchor(top: HeaderView.topAnchor, leading: nil, bottom: HeaderView.bottomAnchor, trailing: HeaderView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 10), size: .init(width: 100, height: 30))


        headerLabel.topAnchor.constraint(equalTo: HeaderView.topAnchor, constant: 0).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: HeaderView.bottomAnchor, constant: 0).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: HeaderView.leadingAnchor, constant: 15).isActive = true
        headerLabel.trailingAnchor.constraint(lessThanOrEqualTo: ViewAllBtn.leadingAnchor, constant: -10).isActive = true


        ///////

        

        let listingCollectionView : UICollectionView = {

            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

            let width = (UIScreen.main.bounds.size.width - 10) / 2.1
            layout.itemSize = CGSize(width: floor(width), height: 250)
            
            /*Dynamic Collection View Code*/
            
//            layout.itemSize = UICollectionViewFlowLayout.automaticSize
//
//            layout.estimatedItemSize = CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 250)
            
            
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal

            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)

            listingcollectionview.backgroundColor = .white
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false

            return listingcollectionview

        }()

        listingCollectionView.tag = index


        self.DetailsView.addSubview(listingCollectionView)

        listingCollectionView.anchor(top: HeaderView.bottomAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15), size: .init(width: 0, height: 310))

        listingCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")



        listingCollectionView.dataSource = self
        listingCollectionView.delegate = self


        // View all Visibility

        if "\((dataDict["category"] as! NSDictionary)["view_all"] ?? "")".elementsEqual("1") {
            ViewAllBtn.isHidden = false
        }
        else {
            ViewAllBtn.isHidden = true
        }

    }




    // MARK:- // View All Button Action

    @objc func viewAllButton(sender: UIButton) {

        // Daily Deals
        if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("10") {

            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailydeals") as! DailyDealsListingViewController

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Marketplace
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("9") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController

            navigate.typeID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Services
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("11") {

            //            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicesMainVC") as! ServicesMainViewController
            //
            //            navigate.typeID = ""
            //            navigate.categoryNameToShow = "All"
            //
            //            self.navigationController?.pushViewController(navigate, animated: true)
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicemain") as! ServiceMainLatestViewController

            //            navigate.typeID = typeID
            //            navigate.categoryNameToShow = categoryName

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Food Court
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("13") {

//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController
            navigate.searchCategoryID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }


            // Real Estate
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("999") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "realestate") as! RealEstateViewController

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Jobs
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobFirstSearchScreenVC") as! JobFirstSearchScreenViewController

            navigate.typeID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Daily Rental
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("15") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalSearchVC") as! DailyRentalSearchViewController

            navigate.productID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }
            //MARK:- //Holiday Accommodation
            
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("16") {
            
//            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
//
//            navigate.ProductID = ""
//
//            self.navigationController?.pushViewController(navigate, animated: true)
            
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "holidayAccomodationSearchVC") as! HolidayAccomodationSearchViewController

                   self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        

            // Happening
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("14") {

            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningsearch") as! HappeningSearchViewController

            self.navigationController?.pushViewController(nav, animated: true)

        }

        else {

        }







    }




    // MARK:- // Create Classified View

    func createClassifiedView(insideOfView : UIView, dataDict : NSDictionary) {



        let HeaderView : UIView = {

            let headerview = UIView()
            //headerview.backgroundColor = .cyan
            headerview.translatesAutoresizingMaskIntoConstraints = false
            return headerview

        }()

        insideOfView.addSubview(HeaderView)


        HeaderView.anchor(top: insideOfView.topAnchor, leading: insideOfView.leadingAnchor, bottom: nil, trailing: insideOfView.trailingAnchor)


        let headerLabel : UILabel = {

            let headerlabel = UILabel()
            //headerlabel.backgroundColor = .green
            headerlabel.font = UIFont(name: self.sampleLBL.font.fontName, size: 16)
            headerlabel.translatesAutoresizingMaskIntoConstraints = false
            return headerlabel


        }()

        HeaderView.addSubview(headerLabel)

        //headerLabel.text = "\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")"
        headerLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key:"\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")", comment: "")

        //        headerLabel.anchor(top: HeaderView.topAnchor, leading: HeaderView.leadingAnchor, bottom: HeaderView.bottomAnchor, trailing: nil, size: .init(width: 100, height: 0))

        let ViewAllBtn : UIButton = {
            let viewallbtn = UIButton()
            //viewallbtn.setTitle("View All", for: .normal)
             viewallbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAll", comment: ""), for: .normal)
            viewallbtn.setTitleColor(.black, for: .normal)
            viewallbtn.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: 14)
            viewallbtn.translatesAutoresizingMaskIntoConstraints = false
          viewallbtn.addTarget(self, action: #selector(self.viewBtnTarget(sender:)), for: .touchUpInside)
            return viewallbtn
        }()

        HeaderView.addSubview(ViewAllBtn)

        ViewAllBtn.anchor(top: HeaderView.topAnchor, leading: nil, bottom: HeaderView.bottomAnchor, trailing: HeaderView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 10), size: .init(width: 100, height: 30))


        headerLabel.topAnchor.constraint(equalTo: HeaderView.topAnchor, constant: 0).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: HeaderView.bottomAnchor, constant: 0).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: HeaderView.leadingAnchor, constant: 15).isActive = true
        headerLabel.trailingAnchor.constraint(lessThanOrEqualTo: ViewAllBtn.leadingAnchor, constant: -10).isActive = true


        ///////



        let listingCollectionView : UICollectionView = {

            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

            let width = (UIScreen.main.bounds.size.width - 10) / 2.1
            layout.itemSize = CGSize(width: floor(width), height: 180)
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal

            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)

            listingcollectionview.backgroundColor = .white
            // listingcollectionview.backgroundColor = .red
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false

            return listingcollectionview

        }()

        self.classifiedView.addSubview(listingCollectionView)

        listingCollectionView.anchor(top: HeaderView.bottomAnchor, leading: insideOfView.leadingAnchor, bottom: insideOfView.bottomAnchor, trailing: insideOfView.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15), size: .init(width: 0, height: 180))

        listingCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")

        listingCollectionView.tag = 999

        listingCollectionView.dataSource = self
        listingCollectionView.delegate = self



    }

    @objc func viewBtnTarget(sender : UIButton)
       {
          
            let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "textad") as! NewTextAdvertisementViewController
                self.navigationController?.pushViewController(nav, animated: true)
       }
       

    // MARK:- // Auto Scroll Function

//    private func startAutoScroll(scrollView : UIScrollView)
//    {
//
//        let tempScroll = scrollView
//
//        tempScroll.alwaysBounceVertical = false
//
//        tempScroll.alwaysBounceHorizontal = false
//
//        var timer: Timer?
//
//        timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true, block: {_ in
//
//            UIView.animate(withDuration: 2.5, delay: 2.5, options: .repeat, animations: {
//
//                    if scrollView.contentOffset.x == (scrollView.contentSize.width - self.FullWidth)
//                    {
//                        scrollView.contentOffset.x = 0
//
//                        //self.startAutoScroll(scrollView: tempScroll)
//                    }
//                    else
//                    {
//                        scrollView.contentOffset.x = scrollView.contentOffset.x + self.FullWidth
//                    }
//
//                }, completion: { (status) in
//                    self.startAutoScroll(scrollView: tempScroll)
//                })
//            })
//    }
    
    private func animateScrollView(scrollView : UIScrollView) {
        
            let scrollWidth = scrollView.bounds.width
        
            let currentXOffset = scrollView.contentOffset.x

            let lastXPos = currentXOffset + scrollWidth
        
        var Autotimer : Timer?
        
         Autotimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: {_ in
            UIView.animate(withDuration: 5, delay: 2.5, options: .repeat, animations: {
                
                if lastXPos != scrollView.contentSize.width {
                    
                    //print("Scroll")
                    
                    if lastXPos > scrollView.contentSize.width
                    {
                        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                        
                        Autotimer?.invalidate()
                        
                        Autotimer = nil
                        
                        scrollView.contentOffset.x = 0
                        
                        self.animateScrollView(scrollView: scrollView)
                    }
                    else if lastXPos <= scrollView.contentSize.width
                    {
                        scrollView.setContentOffset(CGPoint(x: lastXPos, y: 0), animated: false)
                        
                        scrollView.contentOffset.x = lastXPos
                        
                        //self.animateScrollView(scrollView: scrollView)
                    }
                    
                }
                else {
                    
                   // print("Scroll to start")
                    
                    scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                    
                    Autotimer?.invalidate()
                    
                    Autotimer = nil
                    
                    self.animateScrollView(scrollView: scrollView)
                    
                }
                
            }, completion: { (status) in
                
                //print("nothing")
                
                if self.AutoScroll == true
                {
                    self.animateScrollView(scrollView: scrollView)
                    
                    self.AutoScroll = false
                }
                else
                {
//                    self.AutoScroll = true
//
//                    Autotimer?.invalidate()
//
//                    Autotimer = nil
                }
                
                
            })
            
        })
        }
    
    
   
//    func scheduledTimerWithTimeInterval(){
//            // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
//            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.animateScrollView), userInfo: nil, repeats: true)
//        }
    
}
class dataStructure
{
    var category : NSDictionary?
    
    var advertisement : NSArray?

    init(category: NSDictionary , advertisement: NSArray) {
        
        self.category = category
        
        self.advertisement = advertisement
        
    }

}

extension LatestHomeViewController : UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Searching is in progress")
        view.endEditing(true)
        
        self.searchBy = self.SearchBar.searchTextField.text
        print("searchBy=====",self.searchBy!)
        
        if textLabel.text == "Product"
      {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController

        navigate.typeID = ""
        navigate.SearchBy = searchBy

        self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            searchingview.isHidden = true
            
            print("Account")
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "AccountSearchListingVC") as! AccountSearchListingVC
            navigate.SearchBy = searchBy

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.SearchBar.searchTextField.text = ""
        self.SearchBar.isHidden = true
         self.searchingview.isHidden = true
        view.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("nothing")
    }  
}



// MARK:- // Pickerview Delegate MEthods

extension LatestHomeViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       
            return 1
       

    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
        return self.Listdata.count
          
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
     
                   
        return self.Listdata[row] as? String
              
        }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          
        
        textLabel.text = self.Listdata[row] as! String
          self.showpickerview.isHidden = true
        
        
      

    }
   
     
}

