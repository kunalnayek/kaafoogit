//
//  AllQnaViewController.swift
//  Kaafoo
//
//  Created by admin on 31/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class AllQnaViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var allQNATitleLbl: UILabel!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var crossImageView: UIImageView!
    
    
    @IBOutlet weak var allQNATableView: UITableView!
    
    var nameOfProduct : String!
    
    var questionAnswerArray : NSMutableArray! = NSMutableArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        productName.text = nameOfProduct
        
        allQNATableView.delegate = self
        allQNATableView.dataSource = self
        self.allQNATableView.rowHeight = UITableView.automaticDimension
        self.allQNATableView.estimatedRowHeight = UITableView.automaticDimension

        
    }
    
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionAnswerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "qnaCell")
        
        cell?.textLabel?.text = "\((questionAnswerArray[indexPath.row] as! NSMutableDictionary)["question"]!)"
        
        
        if "\((questionAnswerArray[indexPath.row] as! NSMutableDictionary)["answer"]!)".isEmpty
        {
            cell?.detailTextLabel?.text = "Answer:- No Reply Yet"
        }
        else
        {
            cell?.detailTextLabel?.text = "Answer:- " + "\((questionAnswerArray[indexPath.row] as! NSMutableDictionary)["answer"]!)"
        }
        
        //Font
        
        cell?.textLabel?.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell?.detailTextLabel?.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        
        
        return cell!
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        let cellHeight = ((50.0/568.0)*FullHeight)
//        return cellHeight
//
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    


}
